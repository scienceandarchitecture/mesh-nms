package com.benison.spnms.valueObject;

import com.meshdynamics.util.IpAddress;

public class RadiusConfigurationVO {

	private short  radiusCipherCCMP;
	private short  radiusCipherTKIP;
	private short  radiusMode;
	private short  wpaEnterpriseDecidesVlan;
	private int    radiusGroupKeyRenewal;
	private long   radiusPort;
	private String radiusKey;
	private String serverIpAddress;

	public short getCipherCCMP() {
		return radiusCipherCCMP;
	}

	public short getCipherTKIP() {
		return radiusCipherTKIP;
	}

	public int getGroupKeyRenewal() {
		return radiusGroupKeyRenewal;
	}

	public String getKey() {
		return radiusKey;
	}

	public short getMode() {
		return radiusMode;
	}

	public long getPort() {
		return radiusPort;
	}

	public String getServerIPAddress() {
		return serverIpAddress;
	}

	public void setCipherCCMP(short ccmp) {
		radiusCipherCCMP = ccmp;
	}

	public void setCipherTKIP(short tkip) {
		radiusCipherTKIP = tkip;
	}

	public void setGroupKeyRenewal(int grpKeyRenew) {
		radiusGroupKeyRenewal = grpKeyRenew;
	}

	public void setKey(String key) {
		radiusKey = key;
	}

	public void setMode(short mode) {
		radiusMode = mode;
	}

	public void setPort(long port) {
		radiusPort = port;
	}

	public void setServerIPAddress(String ip) {
		serverIpAddress = ip;
	}
	
	public short getWPAEnterpriseDecidesVlan() {
		return wpaEnterpriseDecidesVlan;
	}

	public void setWPAEnterpriseDecidesVlan(short decision) {
		wpaEnterpriseDecidesVlan = decision;
	}
}
