package com.benison.spnms.valueObject;

public class InterfaceAdvanceInfo {
	
	private byte channelBandwidth;
	private byte secondaryChannelPosition;
	private byte guardInterval_20;
	private byte guardInterval_40;
	private byte frameAggregation;
	private byte MaxAMPDU;
	private byte MaxAMSDU;
	private byte txSTBC;
	private byte rxSTBC;
	private byte ldpc;
	private byte gfMode;
	private byte coexistence;
	private short  MaxMPDU;
	private byte   maxAMPDUEnabled;
	
	public byte getChannelBandwidth() {
		return channelBandwidth;
	}
	public void setChannelBandwidth(byte channelBandwidth) {
		this.channelBandwidth = channelBandwidth;
	}
	public byte getSecondaryChannelPosition() {
		return secondaryChannelPosition;
	}
	public void setSecondaryChannelPosition(byte secondaryChannelPosition) {
		this.secondaryChannelPosition = secondaryChannelPosition;
	}
	
	public byte getGuardInterval_20() {
		return guardInterval_20;
	}
	public void setGuardInterval_20(byte guardInterval_20) {
		this.guardInterval_20 = guardInterval_20;
	}
	public byte getGuardInterval_40() {
		return guardInterval_40;
	}
	public void setGuardInterval_40(byte guardInterval_40) {
		this.guardInterval_40 = guardInterval_40;
	}
	public byte getFrameAggregation() {
		return frameAggregation;
	}
	public void setFrameAggregation(byte frameAggregation) {
		this.frameAggregation = frameAggregation;
	}
	public byte getMaxAMPDU() {
		return MaxAMPDU;
	}
	public void setMaxAMPDU(byte maxAMPDU) {
		MaxAMPDU = maxAMPDU;
	}
	public byte getMaxAMSDU() {
		return MaxAMSDU;
	}
	public void setMaxAMSDU(byte maxAMSDU) {
		MaxAMSDU = maxAMSDU;
	}
	public byte getTxSTBC() {
		return txSTBC;
	}
	public void setTxSTBC(byte txSTBC) {
		this.txSTBC = txSTBC;
	}
	public byte getRxSTBC() {
		return rxSTBC;
	}
	public void setRxSTBC(byte rxSTBC) {
		this.rxSTBC = rxSTBC;
	}
	public byte getLdpc() {
		return ldpc;
	}
	public void setLdpc(byte ldpc) {
		this.ldpc = ldpc;
	}
	public byte getGfMode() {
		return gfMode;
	}
	public void setGfMode(byte gfMode) {
		this.gfMode = gfMode;
	}
	public byte getCoexistence() {
		return coexistence;
	}
	public void setCoexistence(byte coexistence) {
		this.coexistence = coexistence;
	}
	public short getMaxMPDU() {
		return MaxMPDU;
	}
	public void setMaxMPDU(short maxMPDU) {
		MaxMPDU = maxMPDU;
	}
	public byte getMaxAMPDUEnabled() {
		return maxAMPDUEnabled;
	}
	public void setMaxAMPDUEnabled(byte maxAMPDUEnabled) {
		this.maxAMPDUEnabled = maxAMPDUEnabled;
	}	
	
}
