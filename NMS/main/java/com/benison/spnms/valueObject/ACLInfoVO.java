package com.benison.spnms.valueObject;

import com.meshdynamics.util.MacAddress;

public class ACLInfoVO {
	
	private short		_80211eCategoryIndex;
	private	short		allowEntry;
	private short		_80211eCategoryEnabled;
	private String	staAddress;
	private	int			vlanTag;
	
	public short get_80211eCategoryIndex() {
		return _80211eCategoryIndex;
	}
	public void set_80211eCategoryIndex(short _80211eCategoryIndex) {
		this._80211eCategoryIndex = _80211eCategoryIndex;
	}
	public short getAllowEntry() {
		return allowEntry;
	}
	public void setAllowEntry(short allowEntry) {
		this.allowEntry = allowEntry;
	}
	public short get_80211eCategoryEnabled() {
		return _80211eCategoryEnabled;
	}
	public void set_80211eCategoryEnabled(short _80211eCategoryEnabled) {
		this._80211eCategoryEnabled = _80211eCategoryEnabled;
	}
	public String getStaAddress() {
		return staAddress;
	}
	public void setStaAddress(String staAddress) {
		this.staAddress = staAddress;
	}
	public int getVlanTag() {
		return vlanTag;
	}
	public void setVlanTag(int vlanTag) {
		this.vlanTag = vlanTag;
	}	
}
