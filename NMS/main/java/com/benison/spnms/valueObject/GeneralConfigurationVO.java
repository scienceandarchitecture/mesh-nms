package com.benison.spnms.valueObject;



public class GeneralConfigurationVO {
	
	private int 							countryCode;
	private String						    dsMacAddress;
	private String							description;
	private String 							model;
	private String							latitude;
	private String							longitude;
	private String 							nodeName;
	private int 							regDomain;
	private short 							firmwareMajorVersion;
	private short 							firmwareMinorVersion;
	private short 							firmwareVariantVersion;
	private String 							preferredParent;
	private	boolean							fipsEnabled;
	private String	                        gateWay;
	private String   	                    hostName;
	private String	                        subnetMask;
	private String	                        ipAddress;
	private short                           heartbeatInterval;
	private short                           mobilityMode;
	private String                          essid;
	private String 				            signalMap;
	private short 					        heartbeatMissCount;
	private short 					        hopCost;
	private short 				     	    maxAllowableHops;
	private short 					        lasi;
	private short 					        crThreshold;
	private short  					        interfaceCount;
	private short					        fcc;
	private short					        etsi;
	private long					        dsTxRate;
	private short					        dsTxPower;
	private long 					        rtsThreshold;
	private long 					        fragThreshold;
	private long 					        beaconInterval;
	private short 					        dynamicChannelAlloc;
	private short        			        stayAwakeCount;
	private short        			        bridgeAgeingTime;
	
	public String getGateWay() {
		return gateWay;
	}
	public void setGateWay(String gateWay) {
		this.gateWay = gateWay;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getSubnetMask() {
		return subnetMask;
	}
	public void setSubnetMask(String subnetMask) {
		this.subnetMask = subnetMask;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public int getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}
	public String getDsMacAddress() {
		return dsMacAddress;
	}
	public void setDsMacAddress(String dsMacAddress) {
		this.dsMacAddress = dsMacAddress;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getNodeName() {
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	public int getRegDomain() {
		return regDomain;
	}
	public void setRegDomain(int regDomain) {
		this.regDomain = regDomain;
	}
	public short getFirmwareMajorVersion() {
		return firmwareMajorVersion;
	}
	public void setFirmwareMajorVersion(short firmwareMajorVersion) {
		this.firmwareMajorVersion = firmwareMajorVersion;
	}
	public short getFirmwareMinorVersion() {
		return firmwareMinorVersion;
	}
	public void setFirmwareMinorVersion(short firmwareMinorVersion) {
		this.firmwareMinorVersion = firmwareMinorVersion;
	}
	public short getFirmwareVariantVersion() {
		return firmwareVariantVersion;
	}
	public void setFirmwareVariantVersion(short firmwareVariantVersion) {
		this.firmwareVariantVersion = firmwareVariantVersion;
	}
	public String getPreferredParent() {
		return preferredParent;
	}
	public void setPreferredParent(String preferredParent) {
		this.preferredParent = preferredParent;
	}
	public boolean isFipsEnabled() {
		return fipsEnabled;
	}
	public void setFipsEnabled(boolean fipsEnabled) {
		this.fipsEnabled = fipsEnabled;
	}
	public short getHeartbeatInterval() {
		return heartbeatInterval;
	}
	public void setHeartbeatInterval(short heartbeatInterval) {
		this.heartbeatInterval = heartbeatInterval;
	}
	public short getMobilityMode() {
		return mobilityMode;
	}
	public void setMobilityMode(short mobilityMode) {
		this.mobilityMode = mobilityMode;
	}
	public String getEssid() {
		return essid;
	}
	public void setEssid(String essid) {
		this.essid = essid;
	}
	public String getSignalMap() {
		return signalMap;
	}
	public void setSignalMap(String signalMap) {
		this.signalMap = signalMap;
	}
	public short getHeartbeatMissCount() {
		return heartbeatMissCount;
	}
	public void setHeartbeatMissCount(short heartbeatMissCount) {
		this.heartbeatMissCount = heartbeatMissCount;
	}
	public short getHopCost() {
		return hopCost;
	}
	public void setHopCost(short hopCost) {
		this.hopCost = hopCost;
	}
	public short getMaxAllowableHops() {
		return maxAllowableHops;
	}
	public void setMaxAllowableHops(short maxAllowableHops) {
		this.maxAllowableHops = maxAllowableHops;
	}
	public short getLasi() {
		return lasi;
	}
	public void setLasi(short lasi) {
		this.lasi = lasi;
	}
	public short getCrThreshold() {
		return crThreshold;
	}
	public void setCrThreshold(short crThreshold) {
		this.crThreshold = crThreshold;
	}
	public short getInterfaceCount() {
		return interfaceCount;
	}
	public void setInterfaceCount(short interfaceCount) {
		this.interfaceCount = interfaceCount;
	}
	public short getFcc() {
		return fcc;
	}
	public void setFcc(short fcc) {
		this.fcc = fcc;
	}
	public short getEtsi() {
		return etsi;
	}
	public void setEtsi(short etsi) {
		this.etsi = etsi;
	}
	public long getDsTxRate() {
		return dsTxRate;
	}
	public void setDsTxRate(long dsTxRate) {
		this.dsTxRate = dsTxRate;
	}
	public short getDsTxPower() {
		return dsTxPower;
	}
	public void setDsTxPower(short dsTxPower) {
		this.dsTxPower = dsTxPower;
	}
	public long getRtsThreshold() {
		return rtsThreshold;
	}
	public void setRtsThreshold(long rtsThreshold) {
		this.rtsThreshold = rtsThreshold;
	}
	public long getFragThreshold() {
		return fragThreshold;
	}
	public void setFragThreshold(long fragThreshold) {
		this.fragThreshold = fragThreshold;
	}
	public long getBeaconInterval() {
		return beaconInterval;
	}
	public void setBeaconInterval(long beaconInterval) {
		this.beaconInterval = beaconInterval;
	}
	public short getDynamicChannelAlloc() {
		return dynamicChannelAlloc;
	}
	public void setDynamicChannelAlloc(short dynamicChannelAlloc) {
		this.dynamicChannelAlloc = dynamicChannelAlloc;
	}
	public short getStayAwakeCount() {
		return stayAwakeCount;
	}
	public void setStayAwakeCount(short stayAwakeCount) {
		this.stayAwakeCount = stayAwakeCount;
	}
	public short getBridgeAgeingTime() {
		return bridgeAgeingTime;
	}
	public void setBridgeAgeingTime(short bridgeAgeingTime) {
		this.bridgeAgeingTime = bridgeAgeingTime;
	}
    
	
}

