package com.benison.spnms.valueObject;

public class StationInfo {
  private String staMacAddress;
  private long   dwnlinkTransmitRate;
  private long   signal;
  private String essid;
  private String bssid;
  private long   activityTime;
  private long   bytesReceived;
  private long   bytesSent;

	public String getStaMacAddress() {
		return staMacAddress;
	}

	public void setStaMacAddress(String staMacAddress) {
		this.staMacAddress = staMacAddress;
	}

	public long getDwnlinkTransmitRate() {
		return dwnlinkTransmitRate;
	}

	public void setDwnlinkTransmitRate(long dwnlinkTransmitRate) {
		this.dwnlinkTransmitRate = dwnlinkTransmitRate;
	}

	public long getSignal() {
		return signal;
	}

	public void setSignal(long signal) {
		this.signal = signal;
	}

	public String getEssid() {
		return essid;
	}

	public void setEssid(String essid) {
		this.essid = essid;
	}

	public String getBssid() {
		return bssid;
	}

	public void setBssid(String bssid) {
		this.bssid = bssid;
	}

	public long getActivityTime() {
		return activityTime;
	}

	public void setActivityTime(long activityTime) {
		this.activityTime = activityTime;
	}

	public long getBytesReceived() {
		return bytesReceived;
	}

	public void setBytesReceived(long bytesReceived) {
		this.bytesReceived = bytesReceived;
	}

	public long getBytesSent() {
		return bytesSent;
	}

	public void setBytesSent(long bytesSent) {
		this.bytesSent = bytesSent;
	}
  
}
