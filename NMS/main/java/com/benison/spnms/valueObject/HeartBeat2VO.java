package com.benison.spnms.valueObject;

public class HeartBeat2VO {
	private    String        dsMacAddress; 	
	private    long 		 sequenceNumber;
	private    long 		 uplinkTransmitRate;
	private    long 		 dwnlinkTransmitRate;
	private    String 		 rebootRequired;
	private    long		     configSeqNumber;
	private    short		 capabilities;				
	private    short		 cmdcapabilities;
	private    short		 configcapabilities;

	public String getDsMacAddress() {
		return dsMacAddress;
	}

	public void setDsMacAddress(String dsMacAddress) {
		this.dsMacAddress = dsMacAddress;
	}

	public long getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(long sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public long getUplinkTransmitRate() {
		return uplinkTransmitRate;
	}

	public void setUplinkTransmitRate(long uplinkTransmitRate) {
		this.uplinkTransmitRate = uplinkTransmitRate;
	}

	public long getDwnlinkTransmitRate() {
		return dwnlinkTransmitRate;
	}

	public void setDwnlinkTransmitRate(long dwnlinkTransmitRate) {
		this.dwnlinkTransmitRate = dwnlinkTransmitRate;
	}
	
	public String getRebootRequired() {
		return rebootRequired;
	}

	public void setRebootRequired(String rebootRequired) {
		this.rebootRequired = rebootRequired;
	}

	public long getConfigSeqNumber() {
		return configSeqNumber;
	}

	public void setConfigSeqNumber(long configSeqNumber) {
		this.configSeqNumber = configSeqNumber;
	}

	public short getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(short capabilities) {
		this.capabilities = capabilities;
	}

	public short getCmdcapabilities() {
		return cmdcapabilities;
	}

	public void setCmdcapabilities(short cmdcapabilities) {
		this.cmdcapabilities = cmdcapabilities;
	}

	public short getConfigcapabilities() {
		return configcapabilities;
	}

	public void setConfigcapabilities(short configcapabilities) {
		this.configcapabilities = configcapabilities;
	}
	
}
