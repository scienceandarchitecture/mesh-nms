package com.benison.spnms.valueObject;

public class PSKConfigurationVO {

	private short    pskCipherCCMP;
	private short    pskCipherTKIP;
	private short[]  pskKey;
	private short    pskMode;
	private int      pskKeyLength;
	private int      pskGroupKeyRenewal;
	private String   pskPassPhrase;

	public short getPSKCipherCCMP() {
		return pskCipherCCMP;
	}

	public short getPSKCipherTKIP() {
		return pskCipherTKIP;
	}

	public int getPSKGroupKeyRenewal() {
		return pskGroupKeyRenewal;
	}

	public short[] getPSKKey() {
		return pskKey;
	}

	public int getPSKKeyLength() {
		return pskKeyLength;
	}

	public short getPSKMode() {
		return pskMode;
	}

	public String getPSKPassPhrase() {
		return pskPassPhrase;
	}

	public void setPSKCipherCCMP(short psk) {
		this.pskCipherCCMP = psk;
	}

	public void setPSKCipherTKIP(short psk) {
		this.pskCipherTKIP = psk;
	}

	public void setPSKGroupKeyRenewal(int grpKeyRenew) {
		this.pskGroupKeyRenewal = grpKeyRenew;
	}

	public void setPSKKey(short[] key) {
		pskKey = new short[pskKeyLength];
		for (int i = 0; i < pskKey.length; i++)
			pskKey[i] = key[i];
	}

	public void setPSKKeyLength(int keyLength) {
		pskKeyLength = keyLength;
	}

	public void setPSKMode(short mode) {
		pskMode = mode;
	}

	public void setPSKPassPhrase(String passphrase) {
		pskPassPhrase = passphrase;
	}
}
