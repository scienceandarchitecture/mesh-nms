package com.benison.spnms.valueObject;

import java.io.Serializable;

public class VlanVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String vlanName;
	private String essid;
	private short tag;
	private short dot11eEnabled;
	private short dot11eCategory;
	private short dot1pPriority;
	private short securityType;
	private long rtsThreshold;
	private long fragThreshold;
	private long beaconInt;
	private short serviceType;
	private short txPower;
	private long txRate;
	private String ipAddress;
	private Object securityInfo;


	public String getVlanName() {
		return vlanName;
	}

	public void setVlanName(String vlanName) {
		this.vlanName = vlanName;
	}

	public String getEssid() {
		return essid;
	}

	public void setEssid(String essid) {
		this.essid = essid;
	}

	public short getTag() {
		return tag;
	}

	public void setTag(short tag) {
		this.tag = tag;
	}

	public short getDot11eEnabled() {
		return dot11eEnabled;
	}

	public void setDot11eEnabled(short dot11eEnabled) {
		this.dot11eEnabled = dot11eEnabled;
	}

	public short getDot11eCategory() {
		return dot11eCategory;
	}

	public void setDot11eCategory(short dot11eCategory) {
		this.dot11eCategory = dot11eCategory;
	}

	public short getDot1pPriority() {
		return dot1pPriority;
	}

	public void setDot1pPriority(short dot1pPriority) {
		this.dot1pPriority = dot1pPriority;
	}

	public short getSecurityType() {
		return securityType;
	}

	public void setSecurityType(short securityType) {
		this.securityType = securityType;
	}

	public Object getSecurityInfo() {
		return securityInfo;
	}

	public void setSecurityInfo(Object securityInfo) {
		this.securityInfo = securityInfo;
	}

	public long getRtsThreshold() {
		return rtsThreshold;
	}

	public void setRtsThreshold(long rtsThreshold) {
		this.rtsThreshold = rtsThreshold;
	}

	public long getFragThreshold() {
		return fragThreshold;
	}

	public void setFragThreshold(long fragThreshold) {
		this.fragThreshold = fragThreshold;
	}

	public long getBeaconInt() {
		return beaconInt;
	}

	public void setBeaconInt(long beaconInt) {
		this.beaconInt = beaconInt;
	}

	public short getServiceType() {
		return serviceType;
	}

	public void setServiceType(short serviceType) {
		this.serviceType = serviceType;
	}

	public short getTxPower() {
		return txPower;
	}

	public void setTxPower(short txPower) {
		this.txPower = txPower;
	}

	public long getTxRate() {
		return txRate;
	}

	public void setTxRate(long txRate) {
		this.txRate = txRate;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

}
