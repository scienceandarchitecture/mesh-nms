package com.benison.spnms.valueObject;



public class InterfaceVO {
	
	private String     macAddress;
	private short      ackTimeout;
	private long       beaconInterval;
	private short      channel;
	private short      bondingType;
	private short[]    dcaList;
	private short      dcaListCount;
	private short      dcaEnabled;
	private String     essid;
	private short      essidHidden;
	private long       fragThreshold;
	private short      mediumSubType;
	private short      mediumType;
	private String     interfaceName;
	private long       rtsThreshold;
	private short      serviceType;
	private short      txPower;
	private long       txRate;
	private short      usageType;
	private short      operatingChannel;
	private short      dot11eEnabled;
	private short      dot11eCategory;
	private short 	   appCtrl;
	
	private InterfaceAdvanceInfo      interfaceAdvanceInfo;
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	public short getAckTimeout() {
		return ackTimeout;
	}
	public void setAckTimeout(short ackTimeout) {
		this.ackTimeout = ackTimeout;
	}
	public long getBeaconInterval() {
		return beaconInterval;
	}
	public void setBeaconInterval(long beaconInterval) {
		this.beaconInterval = beaconInterval;
	}
	public short getChannel() {
		return channel;
	}
	public void setChannel(short channel) {
		this.channel = channel;
	}
	public short getBondingType() {
		return bondingType;
	}
	public void setBondingType(short bondingType) {
		this.bondingType = bondingType;
	}
	
	public short getDcaListCount() {
		return dcaListCount;
	}
	public void setDcaListCount(short dcaListCount) {
		this.dcaListCount = dcaListCount;
	}
	
	public short[] getDcaList() {
		return dcaList;
	}
	public void setDcaList(short[] dcaList) {
		this.dcaList = dcaList;
	}
	public short getDcaEnabled() {
		return dcaEnabled;
	}
	public void setDcaEnabled(short dcaEnabled) {
		this.dcaEnabled = dcaEnabled;
	}
	public String getEssid() {
		return essid;
	}
	public void setEssid(String essid) {
		this.essid = essid;
	}
	public short getEssidHidden() {
		return essidHidden;
	}
	public void setEssidHidden(short essidHidden) {
		this.essidHidden = essidHidden;
	}
	public long getFragThreshold() {
		return fragThreshold;
	}
	public void setFragThreshold(long fragThreshold) {
		this.fragThreshold = fragThreshold;
	}
	public short getMediumSubType() {
		return mediumSubType;
	}
	public void setMediumSubType(short mediumSubType) {
		this.mediumSubType = mediumSubType;
	}
	public short getMediumType() {
		return mediumType;
	}
	public void setMediumType(short mediumType) {
		this.mediumType = mediumType;
	}
	public String getInterfaceName() {
		return interfaceName;
	}
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}
	public long getRtsThreshold() {
		return rtsThreshold;
	}
	public void setRtsThreshold(long rtsThreshold) {
		this.rtsThreshold = rtsThreshold;
	}
	public short getServiceType() {
		return serviceType;
	}
	public void setServiceType(short serviceType) {
		this.serviceType = serviceType;
	}
	public short getTxPower() {
		return txPower;
	}
	public void setTxPower(short txPower) {
		this.txPower = txPower;
	}
	public long getTxRate() {
		return txRate;
	}
	public void setTxRate(long txRate) {
		this.txRate = txRate;
	}
	public short getUsageType() {
		return usageType;
	}
	public void setUsageType(short usageType) {
		this.usageType = usageType;
	}
	public short getOperatingChannel() {
		return operatingChannel;
	}
	public void setOperatingChannel(short operatingChannel) {
		this.operatingChannel = operatingChannel;
	}
	public short getDot11eEnabled() {
		return dot11eEnabled;
	}
	public void setDot11eEnabled(short dot11eEnabled) {
		this.dot11eEnabled = dot11eEnabled;
	}
	public short getDot11eCategory() {
		return dot11eCategory;
	}
	public void setDot11eCategory(short dot11eCategory) {
		this.dot11eCategory = dot11eCategory;
	}
	
	public InterfaceAdvanceInfo getInterfaceAdvanceInfo() {
		return interfaceAdvanceInfo;
	}
	public void setInterfaceAdvanceInfo(InterfaceAdvanceInfo interfaceAdvanceInfo) {
		this.interfaceAdvanceInfo = interfaceAdvanceInfo;
	}
	public short getAppCtrl() {
		return appCtrl;
	}
	public void setAppCtrl(short appCtrl) {
		this.appCtrl = appCtrl;
	}
}
