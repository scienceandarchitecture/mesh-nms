package com.benison.spnms.valueObject;

public class HeartBeatVO {
	private    String    dsMacAddress;			
	private    long      sequenceNumber;			
	private    long      cumulativeTollCost; 		
	private    short     hopCount;				
	private    String    rootBssid;			
	private    long      cumulativeRSSI;			
	private    short     healthIndex;			
	private    short     heartbeatInterval; 		
	private    long      bitRate;	 				
	private    short     staCount; 				
	private    double    boardTemperature;		
	private    String    parentBssid;				
	private    short     apCount;				
	private    long      txPackets;				
	private    long      rxPackets;				
	private    long      txBytes;					
	private    long      rxBytes;

	public String getDsMacAddress() {
		return dsMacAddress;
	}

	public void setDsMacAddress(String dsMacAddress) {
		this.dsMacAddress = dsMacAddress;
	}

	public long getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(long sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public long getCumulativeTollCost() {
		return cumulativeTollCost;
	}

	public void setCumulativeTollCost(long cumulativeTollCost) {
		this.cumulativeTollCost = cumulativeTollCost;
	}

	public short getHopCount() {
		return hopCount;
	}

	public void setHopCount(short hopCount) {
		this.hopCount = hopCount;
	}

	public String getRootBssid() {
		return rootBssid;
	}

	public void setRootBssid(String rootBssid) {
		this.rootBssid = rootBssid;
	}

	public long getCumulativeRSSI() {
		return cumulativeRSSI;
	}

	public void setCumulativeRSSI(long cumulativeRSSI) {
		this.cumulativeRSSI = cumulativeRSSI;
	}

	public short getHealthIndex() {
		return healthIndex;
	}

	public void setHealthIndex(short healthIndex) {
		this.healthIndex = healthIndex;
	}

	public short getHeartbeatInterval() {
		return heartbeatInterval;
	}

	public void setHeartbeatInterval(short heartbeatInterval) {
		this.heartbeatInterval = heartbeatInterval;
	}

	public long getBitRate() {
		return bitRate;
	}

	public void setBitRate(long bitRate) {
		this.bitRate = bitRate;
	}

	public short getStaCount() {
		return staCount;
	}

	public void setStaCount(short staCount) {
		this.staCount = staCount;
	}

	public double getBoardTemperature() {
		return boardTemperature;
	}

	public void setBoardTemperature(double boardTemperature) {
		this.boardTemperature = boardTemperature;
	}

	public String getParentBssid() {
		return parentBssid;
	}

	public void setParentBssid(String parentBssid) {
		this.parentBssid = parentBssid;
	}

	public short getApCount() {
		return apCount;
	}

	public void setApCount(short apCount) {
		this.apCount = apCount;
	}

	public long getTxPackets() {
		return txPackets;
	}

	public void setTxPackets(long txPackets) {
		this.txPackets = txPackets;
	}

	public long getRxPackets() {
		return rxPackets;
	}

	public void setRxPackets(long rxPackets) {
		this.rxPackets = rxPackets;
	}

	public long getTxBytes() {
		return txBytes;
	}

	public void setTxBytes(long txBytes) {
		this.txBytes = txBytes;
	}

	public long getRxBytes() {
		return rxBytes;
	}

	public void setRxBytes(long rxBytes) {
		this.rxBytes = rxBytes;
	}

}
