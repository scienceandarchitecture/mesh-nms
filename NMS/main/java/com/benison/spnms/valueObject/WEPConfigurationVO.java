package com.benison.spnms.valueObject;

public class WEPConfigurationVO {

	private short[][] key;
	private short keyIndex;
	private short keyStrength;
	private String passphrase;
	
	public short[] getKey(int keyIndex) {
		return key[keyIndex];
	}

	public short getKeyIndex() {
		return keyIndex;
	}

	public short getKeyStrength() {
		return keyStrength;
	}

	public String getPassPhrase() {
		return passphrase;
	}

	public void setKey(int keyIndex, short[] key0) {
		key[keyIndex]= key0;
	}

	public void setKeyIndex(short keyIndex) {
		this.keyIndex = keyIndex;
	}

	public void setKeyStrength(short strength) {
		this.keyStrength = strength;
	}

	public void setPassPhrase(String passphrase) {
		this.passphrase = passphrase;
		
	}
}
