package com.benison.spnms.controller;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.benison.spnms.common.ModelVOHelper;
import com.benison.spnms.common.NMSHelper;
import com.benison.spnms.common.SingletonObject;
import com.benison.spnms.common.Status;
import com.benison.spnms.valueObject.ACLInfoVO;
import com.benison.spnms.valueObject.GeneralConfigurationVO;
import com.benison.spnms.valueObject.HeartBeat2VO;
import com.benison.spnms.valueObject.HeartBeatVO;
import com.benison.spnms.valueObject.InterfaceAdvanceInfo;
import com.benison.spnms.valueObject.InterfaceVO;
import com.benison.spnms.valueObject.PSKConfigurationVO;
import com.benison.spnms.valueObject.RadiusConfigurationVO;
import com.benison.spnms.valueObject.StationInfo;
import com.benison.spnms.valueObject.VlanVO;
import com.benison.spnms.valueObject.WEPConfigurationVO;
import com.meshdynamics.meshviewer.MeshViewer;
import com.meshdynamics.meshviewer.configuration.I80211eCategoryConfiguration;
import com.meshdynamics.meshviewer.configuration.IACLConfiguration;
import com.meshdynamics.meshviewer.configuration.IACLInfo;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IEffistreamConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.IPSKConfiguration;
import com.meshdynamics.meshviewer.configuration.IRadiusConfiguration;
import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.IStaConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IWEPConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.ACLInfo;
import com.meshdynamics.meshviewer.configuration.impl.C80211eCategoryInfo;
import com.meshdynamics.meshviewer.configuration.impl.EffistreamAction;
import com.meshdynamics.meshviewer.configuration.impl.EffistreamRule;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceAdvanceConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceInfo;
import com.meshdynamics.meshviewer.configuration.impl.PSKConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.RadiusConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.VLANInfo;
import com.meshdynamics.meshviewer.configuration.impl.WEPConfiguration;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.meshviewer.util.TransmitRateTable;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.MacAddress;

@RestController
@RequestMapping
public class MDNMSController {
	final static Logger logger=Logger.getLogger(MDNMSController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET )
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.");
				
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return model.toString();
	}
	
@RequestMapping(value="/getInterface",method=RequestMethod.GET,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
public @ResponseBody InterfaceVO getInterfaceData(@RequestParam("interfaceName") String interfacename,@RequestParam("networkName") String networkName,@RequestParam("macAddress") String macAddress){
	       logger.info("Get interface from Accesspoint By using networkName:"+networkName+" and macAddress "+macAddress);
	       InterfaceVO interfaceVO=new InterfaceVO();
	      try {		
		     AccessPoint accesspoint=NMSHelper.getNMSConfiguration(networkName,macAddress);
		     if(accesspoint == null){
		    	 return null;
		     }
		if(accesspoint.canConfigure()){
			     IConfiguration configuration=accesspoint.getConfiguration();
			     IInterfaceConfiguration interfaceConfiguration=configuration.getInterfaceConfiguration();
			     InterfaceInfo inter=(InterfaceInfo) interfaceConfiguration.getInterfaceByName(interfacename);
			if(inter != null){
				    logger.info("successfully fetched interface data from Accesspoint ");
			    	ModelVOHelper.interfaceToInterfaceVO(inter, interfaceVO);
			}
		}
	} catch (Exception e) {
		// TODO: handle exception
	    System.out.println(e.getMessage());
		return new InterfaceVO();
	}
	return interfaceVO;
}

@RequestMapping(value="/updateInterface",method=RequestMethod.PUT,consumes=MediaType.APPLICATION_JSON_VALUE)
public @ResponseBody Status  updateInterface(@RequestBody String jsonValue,@RequestParam("networkName") String networkName,@RequestParam("macAddress") String macAddress){
	
	logger.info("update interface in MDNMS controller ");	
	Status status=new Status();
	status.setMessage("still interface data not recieved or enter correct interface name");
	boolean flag=false;
	try {
		if(jsonValue == null || jsonValue.length() <1){			
			return new Status(1,"requestbody empty");
		}
		JSONObject obj=NMSHelper.pasrseString(jsonValue);
		
		AccessPoint accesspoint=NMSHelper.getNMSConfiguration(networkName, macAddress);
		if(accesspoint == null){
			status.setCode(NMSHelper.NODE_NOT_EXIST);
			status.setMessage("verify MacAddress and Network details");
			status.setRebootRequired("NO");
			status.setResponseData("");
			return status;
		}
		if(accesspoint.canConfigure()){
			IConfiguration configuration=accesspoint.getConfiguration();
			IInterfaceConfiguration interfaceConfiguration=configuration.getInterfaceConfiguration();
			InterfaceInfo inter=(InterfaceInfo) interfaceConfiguration.getInterfaceByName((String) obj.get("interfaceName"));
			IConfigStatusHandler statusHandler	= accesspoint.getUpdateStatusHandler();
			//check valid medium Type
			if(obj.get("mediumSubType") != null){
			boolean mflag=false;
			short val=(short)(long)obj.get("mediumSubType");
			String freq=ModelVOHelper.verifyMediumSubType(val,(String) obj.get("interfaceName"));
			if(freq != null){
				 if(freq.equalsIgnoreCase(ModelVOHelper.FREQ_FIVE)){	
				 if(inter.getUsageType() == Mesh.PHY_USAGE_TYPE_DS)
					mflag=true;				
			}
			if(freq.equalsIgnoreCase(ModelVOHelper.FREQ_TWO)){	
				 if(inter.getUsageType() == Mesh.PHY_USAGE_TYPE_WM)
					mflag =true;				
			}
			}
			if(!mflag){
				status.setCode(NMSHelper.NOT_SUPPORTED);
				status.setMessage("mediumSubType is not valid value");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
			}
			}
			//check Tx rates based On Type
			if(obj.get("mediumSubType") != null){
				boolean txflag=false;
				short val=(short)(long)obj.get("mediumSubType");
				 Vector<String> vector=null;
				if(val == Mesh.PHY_SUB_TYPE_802_11_24GHz_N ||
				   val == Mesh.PHY_SUB_TYPE_802_11_5GHz_N ||
				   val == Mesh.PHY_SUB_TYPE_802_11_AN ||
				   val == Mesh.PHY_SUB_TYPE_802_11_AC ||
				   val == Mesh.PHY_SUB_TYPE_802_11_ANAC ||
				   val == Mesh.PHY_SUB_TYPE_802_11_BGN){
					InterfaceAdvanceConfiguration advanceInfo=inter.getInterfaceAdvanceConfiguration();
				    byte ifBandwidth=advanceInfo.getChannelBandwidth();
				   if(ifBandwidth == 2)
				    	vector=TransmitRateTable.getDefaultRateTable_AdvanceSettings(val, ifBandwidth, advanceInfo.getGuardInterval_20());
				   else if(ifBandwidth ==1 || ifBandwidth==0)
				    	vector=TransmitRateTable.getDefaultRateTable_AdvanceSettings(val, ifBandwidth, advanceInfo.getGuardInterval_40());
				    else if(ifBandwidth == -1)
				    	vector=TransmitRateTable.getDefaultRateTable_AdvanceSettings(val, ifBandwidth, advanceInfo.getGuardInterval_80());
					
				}else{
				      vector=TransmitRateTable.getDefaultRateTable(val, 20);
				     }
				if(obj.get("txRate") != null && vector != null){
					long txrate=(long)obj.get("txRate");
					for(String strtxrate:vector){
						if(txrate == Integer.parseInt(strtxrate)){
							txflag=true;
						}
						if(txrate == 0)
							flag=true;
					}
					if(!txflag){
						status.setCode(NMSHelper.NOT_SUPPORTED);
						status.setMessage("Txrate is not valid value");
						status.setRebootRequired("NO");
						status.setResponseData("");
						return status;
					}
				}
				}
			if(obj.get("advanceInfo") != null){
				short val=(short)(long)obj.get("mediumSubType");
				if(!(val == Mesh.PHY_SUB_TYPE_802_11_24GHz_N ||
					val == Mesh.PHY_SUB_TYPE_802_11_5GHz_N ||
					val == Mesh.PHY_SUB_TYPE_802_11_AN ||
					val == Mesh.PHY_SUB_TYPE_802_11_AC ||
					val == Mesh.PHY_SUB_TYPE_802_11_ANAC ||
					val == Mesh.PHY_SUB_TYPE_802_11_BGN)){
					status.setCode(NMSHelper.NOT_SUPPORTED);
					status.setMessage("HT and VHT Capabilities Not supporting");
					status.setRebootRequired("NO");
					status.setResponseData("");
					return status;
					}
				}
			if(inter != null){
				flag = ModelVOHelper.interfaceVOTOInterface(obj, inter, statusHandler);
			}
			//check for update 
			if(statusHandler.isAnyPacketChanged()){
				accesspoint.updateChangedConfiguration();
				boolean response=NMSHelper.wiatForResponse(statusHandler,PacketFactory.AP_CONFIGURATION_INFO);
				if(response){
				logger.info("interfacedata updated successfully");	
				if(!statusHandler.getResponseStatus()){
				 if(flag)				
					status.setRebootRequired("YES");			  
			     else
					 status.setRebootRequired("NO");
							 
				status.setCode(NMSHelper.OK);				    
				status.setMessage("interfacedata updated successfully");
				
				}else{
					statusHandler.setResponseStatus(false);
					status.setCode(NMSHelper.VALIDATION);
					status.setRebootRequired("NO");
					status.setMessage("Validation failure in case of interface data.");
				}}else{
					status.setCode(NMSHelper.RESPONSE_TIMEOUT);
					status.setRebootRequired("NO");
					status.setMessage("interfacedata sent To Device,Not received response.");	}
				}else{
					status.setCode(NMSHelper.NO_CHANGE);
					status.setRebootRequired("NO");
					status.setMessage("NO Changes in interfacedata.");	
					}//if any change 		
		}else{
		status.setCode(NMSHelper.NODE_NOT_RECEIVED);
		status.setMessage("Node Data Not yet received");
		status.setRebootRequired("NO");
		status.setResponseData("");
  		return status;
		}// if end
	}catch (Exception e) {
		   	status.setCode(NMSHelper.INTERNAL_SERVER_ERROR);
			status.setRebootRequired("NO");
			status.setMessage("interface data updation failure");
			return status;
	}
	return status;
}

	/*
	 *  Vlan configuration  
	 */
	@RequestMapping(value = "/getVlanByName", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody VlanVO getVlanDataByName(@RequestParam("vlanName") String vlanName,
			@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress) {
		logger.info("getVlanDataByName method started");
		VLANInfo vlanInfo = null;
		VlanVO vlanVO = new VlanVO();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if (accesspoint.canConfigure()) {
				IConfiguration configuration = accesspoint.getConfiguration();
				IVlanConfiguration vlanConfiguration = configuration.getVlanConfiguration();
				vlanInfo = (VLANInfo) vlanConfiguration.getVlanInfoByName(vlanName);
				if (vlanInfo != null) {
					ModelVOHelper.vlanInfoToVlanVO(vlanInfo, vlanVO);
					logger.info("successfully fetch the information from AccessPoint using Vlan Name");
					return vlanVO;
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("while getting the Vlan information using vlanName facing problem" + e.getLocalizedMessage());
			return null;
		}
		logger.info("getVlanDataByName method ended");
		return vlanVO;
	}
	
	@RequestMapping(value = "/getVlanByEssid", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody VlanVO getVlanDataByEssId(@RequestParam("essid") String essID,
			@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress) {
		logger.info(" getVlanDataByEssId method started. ");
		VLANInfo vlanInfo = null;
		VlanVO vlanVO = new VlanVO();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if (accesspoint.canConfigure()) {
				IConfiguration configuration = accesspoint.getConfiguration();
				IVlanConfiguration vlanConfiguration = configuration.getVlanConfiguration();
				vlanInfo = (VLANInfo) vlanConfiguration.getVlanInfoByEssid(essID);
				if (vlanInfo != null) {
					ModelVOHelper.vlanInfoToVlanVO(vlanInfo, vlanVO);
					logger.info("successfully fetch the information from AccessPoint using Vlan EssId");
					return vlanVO;
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
			logger.error("while getting the Vlan information using essid facing problem" + e.getLocalizedMessage());
			return null;
		}
		logger.info(" getVlanDataByEssId method end. ");
		return vlanVO;
	}
	/***
	 * 
	 * @param jsonString
	 * @param networkName
	 * @param macAddress
	 * @return
	 */
	@RequestMapping(value = "/updateVlan", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status updateVlan(@RequestBody String jsonString,
			@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress) {
		logger.info("addOrUpdateVlan method started");
		Status status = new Status();
		short statusChanges;
		try {
			if (jsonString == null || jsonString.length() < 1) {
				return new Status(1, "requestbody empty");
			}
			JSONObject jsonObject = NMSHelper.pasrseString(jsonString);
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if(accesspoint == null){
				status.setCode(NMSHelper.NODE_NOT_EXIST);
				status.setMessage("verify MacAddress and Network details");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
			}
			if (accesspoint.canConfigure()) {
				IConfiguration configuration = accesspoint.getConfiguration();
				IVlanConfiguration vlanConfiguration = configuration.getVlanConfiguration();
				IConfigStatusHandler statusHandler = accesspoint.getUpdateStatusHandler();
				if((String) jsonObject.get("vlanName") != null){
				VLANInfo vlanInfo = (VLANInfo) vlanConfiguration.getVlanInfoByName((String) jsonObject.get("vlanName"));
				if (vlanInfo != null) {
					VLANInfo tempVlanInfo = new VLANInfo();
					vlanInfo.copyTo(tempVlanInfo);
					ModelVOHelper.jsonToVlanObject(jsonObject, tempVlanInfo);
					tempVlanInfo.copyTo(vlanInfo);
					statusChanges = tempVlanInfo.compare(vlanInfo, statusHandler);
					if (statusChanges >= 1) {
						accesspoint.updateChangedConfiguration();
						boolean flag=NMSHelper.wiatForResponse(statusHandler,PacketFactory.AP_VLAN_CONFIGURATION_INFO);
						if(flag){
						logger.info("vlan information updated successfully");
						status.setCode(NMSHelper.OK);
						status.setRebootRequired("YES");
						status.setMessage("vlan information updated  successfully");
						}else{
							status.setCode(NMSHelper.RESPONSE_TIMEOUT);
							status.setRebootRequired("NO");
							status.setMessage("Vlan info sent To Device,Not received response.");
						}
					}
				}else{
					VLANInfo vlan=(VLANInfo)vlanConfiguration.addVlanInfo((String) jsonObject.get("vlanName"));
					VLANInfo tempVlanInfo = new VLANInfo();
					vlan.copyTo(tempVlanInfo);
					ModelVOHelper.jsonToVlanObject(jsonObject, tempVlanInfo);	
					tempVlanInfo.copyTo(vlan);	
					   statusChanges = tempVlanInfo.compare(vlan, statusHandler);
					   statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
					if (statusChanges >= 1) {
						accesspoint.updateChangedConfiguration();
						boolean flag=NMSHelper.wiatForResponse(statusHandler,PacketFactory.AP_VLAN_CONFIGURATION_INFO);
						if(flag){
						logger.info("vlan information added successfully");
						status.setCode(NMSHelper.CREATED);
						status.setRebootRequired("YES");
						status.setMessage("vlan information added  successfully");
						}else{
							status.setCode(NMSHelper.RESPONSE_TIMEOUT);
							status.setRebootRequired("NO");
							status.setMessage("Vlan info sent To Device,Not received response.");
						}
					}
				}
			 
			}
		}else{
			status.setCode(NMSHelper.NODE_NOT_RECEIVED);
			status.setMessage("Node Data Not yet received");
			status.setRebootRequired("NO");
			status.setResponseData("");
			return status;
			}} catch (Exception e) {
			logger.error("Updation filed : " + e.getLocalizedMessage());
			status.setCode(NMSHelper.INTERNAL_SERVER_ERROR);
			status.setRebootRequired("NO");
			status.setMessage("vlan information updation failure");
			return status;
		}
		return status;
	}
	/***
	 * 
	 * @param vlanName
	 * @param networkName
	 * @param macAddress
	 * @return
	 */
	@RequestMapping(value = "/deleteVlan", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status DeleteVlanDataByName(@RequestParam("vlanName") String vlanName,
			@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress) {
		logger.info("delete Vlan by Name");
		Status status=new Status();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if(accesspoint == null){
				status.setCode(NMSHelper.NODE_NOT_EXIST);
				status.setMessage("verify MacAddress and Network details");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
			}
			if (accesspoint.canConfigure()) {
				IConfiguration configuration = accesspoint.getConfiguration();
				IVlanConfiguration vlanConfiguration = configuration.getVlanConfiguration();
				vlanConfiguration.deleteVlanInfo(vlanName);
				IConfigStatusHandler statusHandler = accesspoint.getUpdateStatusHandler();
				statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
				accesspoint.updateChangedConfiguration();
				boolean flag=NMSHelper.wiatForResponse(statusHandler,PacketFactory.AP_VLAN_CONFIGURATION_INFO);
				if(flag){
				status.setCode(NMSHelper.VLAN_REMOVED);
				status.setRebootRequired("YES");
				status.setMessage("Vlan data deleted successfully");
				}else{
					status.setCode(NMSHelper.RESPONSE_TIMEOUT);
					status.setRebootRequired("NO");
					status.setMessage("Vlan info sent To Device,Not received response.");
				}
			}else{
				status.setCode(NMSHelper.NODE_NOT_RECEIVED);
				status.setMessage("Node Data Not yet received");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
				}
		} catch (Exception e) {
			logger.error("while getting the Vlan information using vlanName facing problem" + e.getLocalizedMessage());
			status.setCode(NMSHelper.INTERNAL_SERVER_ERROR);
			status.setRebootRequired("NO");
			status.setMessage("Valan data not deleted ");
			return status;
		}
		logger.info("delete VlanDataByName method ended");
		return status;
	}
	// testing for json file
	@RequestMapping(value = "/updateJson", method = RequestMethod.PUT,consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status updateJson(@RequestBody String jsonString) throws Exception{
		System.out.println("update Json data method.................");
		Status status = new Status();
		VlanVO vlanVO = new VlanVO();
		if (jsonString == null || jsonString.length() < 1) {
			return new Status(1, "requestbody empty");
		}
		JSONObject jsonObject = NMSHelper.pasrseString(jsonString);
		VLANInfo tempVlanInfo = new VLANInfo();
		ModelVOHelper.jsonToVlanObject(jsonObject, tempVlanInfo);
		ModelVOHelper.vlanInfoToVlanVO(tempVlanInfo, vlanVO);
		status.setMessage("successfully get the information");
		status.setResponseData(vlanVO);
		return status;
	}
	
	/*
	 *  ACL configuration  
	 *  AddACLInfo using MAC Address
	 */
	@RequestMapping(value = "/addACLInfo", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status addACLInfo(@RequestBody String jsonString, @RequestParam("networkName") String networkName,
			@RequestParam("macAddress") String macAddress) {
		logger.info("Acl addOrUpdate method execution started ");
		Status status = new Status();
		try {
			if(jsonString == null || jsonString.length()<1){
				return new Status(1, "requestbody empty");
			}
			JSONObject jsonObject = NMSHelper.pasrseString(jsonString);
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if(accesspoint == null){
				status.setCode(NMSHelper.NODE_NOT_EXIST);
				status.setMessage("verify MacAddress and Network details");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
			}
			if (accesspoint.canConfigure()) {
 				IConfiguration configuration = accesspoint.getConfiguration();
				IACLConfiguration aclConfiguration = configuration.getACLConfiguration();
				IConfigStatusHandler statusHandler	= accesspoint.getUpdateStatusHandler();
				MacAddress _macAddress = NMSHelper.stringToMacAddress((String)jsonObject.get("MacAddress"));
				if(MeshValidations.isMacAddress((String)jsonObject.get("MacAddress"))){
					if(!aclConfiguration.containsAclEntryWithStaAddr(_macAddress)){
				     ACLInfo aclInfo = (ACLInfo) aclConfiguration.addAclInfo(_macAddress);
				     ModelVOHelper.jsonToACLObject(jsonObject, aclInfo);	
				    statusHandler.packetChanged(PacketFactory.ACL_INFO_PACKET);
				if (aclInfo != null) {
					accesspoint.updateChangedConfiguration();
					boolean flag=NMSHelper.wiatForResponse(statusHandler,PacketFactory.ACL_INFO_PACKET);
					logger.info("Access Control List(ACL) added successfully into accesspoint");
					if(flag){
					status.setCode(NMSHelper.CREATED);
					status.setRebootRequired("NO");
					status.setMessage("Access Control List(ACL) added successfully");
					}else{
						status.setCode(NMSHelper.RESPONSE_TIMEOUT);
						status.setRebootRequired("NO");
						status.setMessage("ACL info sent To Device,Not received response.");
					}
				}else{
					logger.error("ACL not added using given macAddress");
					status.setCode(NMSHelper.NOT_ADDED);
					status.setRebootRequired("NO");
					status.setMessage("ACL not added using given macAddress");
				}}else{
					status.setCode(NMSHelper.EXIST);
					status.setRebootRequired("NO");
					status.setMessage("ACL Entry is already exist");
				}}else{
					status.setCode(NMSHelper.BAD_REQUEST);
					status.setRebootRequired("NO");
					status.setMessage("inValid  macAddress");
				}}else{
					status.setCode(NMSHelper.NODE_NOT_RECEIVED);
					status.setMessage("Node Data Not yet received");
					status.setRebootRequired("NO");
					status.setResponseData("");
					return status;
					}
		} catch (Exception e) {
			logger.error(" Access Control List(ACL) not added "+e.getLocalizedMessage());
			status.setCode(NMSHelper.INTERNAL_SERVER_ERROR);
			status.setRebootRequired("NO");
			status.setMessage("Some Internal Problem");
			return status;
		}
		logger.info("Acl addOrUpdate method execution ended ");
		return status;
	}
	
	/*
	 *  ACL configuration  
	 *  Check the ACL Configuration infos whether the information available or not using MacAddress
	 */
	@RequestMapping(value = "/getACLInfo", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ACLInfoVO getACLInfoUsingMacAddress(@RequestParam("aclMacAddress") String aclMacAddress, @RequestParam("networkName") String networkName,
			@RequestParam("macAddress") String macAddress) {
		logger.info("ACL getACLInfoUsingMacAddress started");
		ACLInfoVO aclInfoVO=new ACLInfoVO();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if (accesspoint.canConfigure()) {
				IConfiguration configuration = accesspoint.getConfiguration();
				IACLConfiguration aclConfiguration = configuration.getACLConfiguration();
				MacAddress _macAddress = NMSHelper.stringToMacAddress(aclMacAddress);
				IACLInfo	aclInfo = aclConfiguration.aclEntryWithStaAddr(_macAddress);
				if (aclInfo != null) {
					aclInfoVO.set_80211eCategoryEnabled(aclInfo.getEnable80211eCategory());
					aclInfoVO.set_80211eCategoryIndex(aclInfo.get80211eCategoryIndex());
					aclInfoVO.setAllowEntry(aclInfo.getAllow());
					aclInfoVO.setStaAddress(aclInfo.getStaAddress().toString());
					aclInfoVO.setVlanTag(aclInfo.getVLANTag());
				}
			}
		} catch (Exception e) {
			logger.error("  "+e.getMessage());			
			return null;
		}
		logger.info("ACL getACLInfoUsingMacAddress ended");
		return aclInfoVO;
	}
	
	/*
	 *  ACL configuration  
	 *  Delete the ACL Configuration infos
	 */
	@RequestMapping(value = "/deleteACLInfo", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status deleteACLInfo(@RequestBody String jsonString,
			@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress) {
		logger.info("DeleteACLInfo  of ACL method started");
		Status status = new Status();
		try {
			if (jsonString == null || jsonString.length() < 1) {
				return new Status(1, "requestbody empty");
			}
			JSONObject jsonObj = NMSHelper.pasrseString(jsonString);
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if(accesspoint == null){
				status.setCode(NMSHelper.NODE_NOT_EXIST);
				status.setMessage("verify MacAddress and Network details");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
			}
			if (accesspoint.canConfigure()) {
				IConfiguration configuration = accesspoint.getConfiguration();
				IACLConfiguration aclConfiguration = configuration.getACLConfiguration();
				IConfigStatusHandler statusHandler	= accesspoint.getUpdateStatusHandler();
				MacAddress _macAddress = NMSHelper.stringToMacAddress((String)jsonObj.get("MacAddress"));
				if(aclConfiguration.containsAclEntryWithStaAddr(_macAddress)){
				   ACLInfo aclInfo = new ACLInfo();
				   ModelVOHelper.jsonToACLObject(jsonObj, aclInfo);
				   ACLInfo matchACLInfo=(ACLInfo) aclConfiguration.getACLInfo(aclInfo);
				   aclConfiguration.deleteAclInfo(matchACLInfo);
				   statusHandler.packetChanged(PacketFactory.ACL_INFO_PACKET);
				   accesspoint.updateChangedConfiguration();
				   boolean flag=NMSHelper.wiatForResponse(statusHandler,PacketFactory.ACL_INFO_PACKET);
				   if(flag){
					   logger.info("ACLInfo deleted successfully");
					    status.setCode(NMSHelper.OK);
						status.setRebootRequired("NO");
						status.setMessage("ACLInfo deleted successfully");
				   }else{
						status.setCode(NMSHelper.RESPONSE_TIMEOUT);
						status.setRebootRequired("NO");
						status.setMessage("ACL info sent To Device,Not received response.");
					}
				}else{
					status.setCode(NMSHelper.NOT_EXIST);
					status.setRebootRequired("NO");
					status.setMessage("ACL Not Exist...");
					logger.info("ACL Not Exist...");
					return status;
				}
				
			}else{
				status.setCode(NMSHelper.NODE_NOT_RECEIVED);
				status.setMessage("Node Data Not yet received");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
				}
		} catch (Exception e) {
			logger.error("ACLInfo not deleted  "+e.getMessage());
			status.setCode(NMSHelper.INTERNAL_SERVER_ERROR);
			status.setRebootRequired("NO");
			status.setMessage("Some internal problem ");
		}
		logger.info("Delete ACLInfo  of ACL method ended");
		return status;
	}
	
	/*
	 *  EffiStreamRules
	 *  addEffistream Rules using criteriaId
	 */
	
	@RequestMapping(value = "/addEffiStreamRule", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status addEffiStreamRule(@RequestParam("criteriaId") String criteriaId,
			@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress) {
		logger.info("AddRule of EffiStream method started");
		Status status = new Status();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if (accesspoint.canConfigure()) {
				IConfiguration configuration = accesspoint.getConfiguration();
				IEffistreamConfiguration effistreamConfiguration = configuration.getEffistreamConfiguration();
				EffistreamRule effistreamRule = (EffistreamRule) effistreamConfiguration.getRulesRoot();
				EffistreamRule rules = new EffistreamRule(effistreamRule, Short.valueOf(criteriaId));
				if (effistreamRule.addRule(rules)) {
					accesspoint.updateChangedConfiguration();
					logger.info("EffiStreamRule added successfully into accesspoint");
					status.setCode(1);
					status.setMessage("EffiStreamRule added successfully");
				}
			}
		} catch (Exception e) {
			logger.error("EffistreamRule not added successfully  " + e.getLocalizedMessage());
			status.setCode(0);
			status.setMessage("EffistreamRule not added successfully");
		}
		logger.info("AddRule of EffiStream method ended");
		return status;
	}
	
	/*
	 *  EffiStreamRules
	 *  create Action using criteriaId
	 */
	@RequestMapping(value = "/addEffiStreamAction", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status addEffiStreamAction(@RequestParam("actionType") String actionType,
			@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress) {
		logger.info("AddAction of EffiStream method started");
		Status status = new Status();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if (accesspoint.canConfigure()) {
				IConfiguration configuration = accesspoint.getConfiguration();
				IEffistreamConfiguration effistreamConfiguration = configuration.getEffistreamConfiguration();
				EffistreamRule effistreamRule = (EffistreamRule) effistreamConfiguration.getRulesRoot();
				EffistreamAction effistreamAction = (EffistreamAction) effistreamRule.addAction(Integer.valueOf(actionType));
				if (effistreamAction != null) {
					accesspoint.updateChangedConfiguration();
					logger.info("AddAction of EffiStreamRule added successfully");
					status.setCode(1);
					status.setMessage("AddAction of EffiStreamRule added successfully");
				}
			}
		} catch (Exception e) {
			logger.error("AddAction of EffistreamRule not done  " + e.getLocalizedMessage());
			status.setCode(0);
			status.setMessage("AddAction of EffistreamRule not done");
		}
		logger.info("AddAction of EffiStream method ended");
		return status;
	}
	
	@RequestMapping(value = "/deleteEffiStreamRule", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status deleteEffiStreamRule(@RequestParam("strIdentifier") String strIdentifier,
			@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress) {
		logger.info("deleteRule of EffiStream method started");
		Status status = new Status();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if (accesspoint.canConfigure()) {
				IConfiguration configuration = accesspoint.getConfiguration();
				IEffistreamConfiguration effistreamConfiguration = configuration.getEffistreamConfiguration();
				EffistreamRule effistreamRule = (EffistreamRule) effistreamConfiguration.getRulesRoot();
				if (effistreamRule.deleteRule(strIdentifier)) {
					accesspoint.updateChangedConfiguration();
					logger.info("EffiStreamRule deleted successfully");
					status.setCode(1);
					status.setMessage("EffiStreamRule deleted successfully");
				}
			}
		} catch (Exception e) {
			logger.error("while delete Effistream getting  exception  " + e.getLocalizedMessage());
			status.setCode(2);
			status.setMessage("while delete Effistream getting  exception please try again later");
		}
		logger.info("deleteRule of EffiStream method ended");
		return status;
	}
	
	/*
	 * I80211eCategoryConfiguration
	 * getCategoryInfoByName(categoryName)
	 */
	
	@RequestMapping(value="/getCategoryInfo",method=RequestMethod.GET,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public C80211eCategoryInfo getCategoryInfo(@RequestParam("categoryName") String categoryName,@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress){
		logger.info("getCategoryInfo() method started");
		C80211eCategoryInfo categoryInfo = new C80211eCategoryInfo();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if (accesspoint.canConfigure()) {
				IConfiguration configuration = accesspoint.getConfiguration();
				I80211eCategoryConfiguration categoryConfiguration = configuration.get80211eCategoryConfiguration();
				categoryInfo =  (C80211eCategoryInfo) categoryConfiguration.getCategoryInfoByName(categoryName);
				if(categoryInfo != null){
					logger.info("Successfully fetch the information using categoryName");
					return categoryInfo;
				}else{
					logger.info("information not available using given categoryName");
				}
			}
		} catch (Exception e) {
			logger.error(" failed to get the information "+e.getLocalizedMessage());
		}
		logger.info("getCategoryInfo() method ended");
		return categoryInfo;
	}
	
	/*
	 *  General Configuration
	 *  
	 */
	@RequestMapping(value = "/getGeneralConfiguration", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody GeneralConfigurationVO getGeneralConfiguration(@RequestParam("networkName") String networkName,
			@RequestParam("macAddress") String macAddress) {
		logger.info("getGeneralConfiguration() method started");
		GeneralConfigurationVO generalConfigurationVO=new GeneralConfigurationVO();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if (accesspoint.canConfigure()) {
				IConfiguration configuration = accesspoint.getConfiguration();
				generalConfigurationVO=ModelVOHelper.getGeneralConfiguration(generalConfigurationVO,configuration);
				}
		} catch (Exception e) {
			return null;
		}
		logger.info("getGeneralConfiguration() method ended");
		return generalConfigurationVO;
	}

	@RequestMapping(value = "/updateGeneralConfiguration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status updateGeneralConfiguration(@RequestBody String jsonString,
			@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress) {
		logger.info("updateGeneralConfiguration() method started");
		Status status = new Status();
		boolean flag=false;
		try {
			if (jsonString == null || jsonString.length() < 1) {
				return new Status(1, "requestbody empty");
			}
			JSONObject jsonObj = NMSHelper.pasrseString(jsonString);
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if(accesspoint == null){
				status.setCode(NMSHelper.NODE_NOT_EXIST);
				status.setMessage("verify MacAddress and Network details");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
			}
			if (accesspoint.canConfigure()) {
			IConfigStatusHandler statusHandler	= accesspoint.getUpdateStatusHandler();
			IConfiguration  configuration=accesspoint.getConfiguration();
			flag=ModelVOHelper.jsonToGeneralConfiguration(jsonObj, configuration,statusHandler);
			if (statusHandler.isAnyPacketChanged()) {
				accesspoint.updateChangedConfiguration();
				boolean response=NMSHelper.wiatForResponse(statusHandler,PacketFactory.AP_CONFIGURATION_INFO);
				if(response){
					if(!statusHandler.getResponseStatus()){
					if(!flag){
				status.setCode(NMSHelper.OK);
				status.setRebootRequired("NO");
				status.setMessage("GeneralConfiguration updated successfully");
				}else{
					status.setCode(NMSHelper.OK);
					status.setRebootRequired("YES");
					status.setMessage("GeneralConfiguration updated successfully");
				}}else{
					statusHandler.setResponseStatus(false);
					status.setCode(NMSHelper.VALIDATION);
					status.setRebootRequired("NO");
					status.setMessage("Validation failure ..");
				}}else{
						status.setCode(NMSHelper.RESPONSE_TIMEOUT);
						status.setRebootRequired("NO");
						status.setMessage("General Configuration sent To Device,Not received response.");
					}
				
			} else {
				status.setCode(NMSHelper.NO_CHANGE);
				status.setMessage("GeneralConfiguration not updated successfully");
				status.setRebootRequired("NO");
			}
			}
			} catch (Exception e) {
				status.setCode(NMSHelper.INTERNAL_SERVER_ERROR);
				status.setRebootRequired("NO");
				status.setMessage("Some internal problem ");
				return status;
		}
		logger.info("updateGeneralConfiguration() method ended");
		return status;
	}
	
	/***
	 * 
	 * @param jsonString
	 * @param networkName
	 * @param macAddress
	 * @return
	 */
	@RequestMapping(value="/moveNode",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status moveNode(@RequestBody String jsonString,@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress){
		logger.info("Move Node from one Network to another network");
		Status status=new Status();
		try {
			if (jsonString == null || jsonString.length() < 1) {
				return new Status(1, "requestbody empty");
			}
			JSONObject jsonObj = NMSHelper.pasrseString(jsonString);
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if(accesspoint == null){
				status.setCode(NMSHelper.NODE_NOT_EXIST);
				status.setMessage("verify MacAddress and Network details");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
			}
			String newNetworkId=(String) jsonObj.get("newNetworkId");
			String newNetworkKey=(String) jsonObj.get("newNetworkKey");
			if(accesspoint.changeNetwork(newNetworkId, newNetworkKey)){
				Map<String,MeshViewer> map=SingletonObject.getInstance();
				if(map.containsKey("mesh")){
					  MeshViewer meshViewer =map.get("mesh");
					  MeshNetwork meshNetwork =meshViewer.getMeshNetworkByName(newNetworkId);
					  if(meshNetwork == null){
						meshViewer.createMeshNetwork(newNetworkId,newNetworkKey,(byte) 0);
						}
					}
			}
					
			status.setCode(NMSHelper.CREATED);
			status.setRebootRequired("YES");
			status.setMessage("Move Node Successfull");
		} catch (Exception e) {
			status.setCode(NMSHelper.INTERNAL_SERVER_ERROR);
			status.setRebootRequired("NO");
			status.setMessage("Move node is failure.... ");
		}
		return status;
	}
	
	/*
	 * @Security
	 * getSecurityStatus
	 */
	
	@RequestMapping(value = "/getSecurityStatus", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Status getSecurityStatus(@RequestParam("name") String interfaceName,
			@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress){
		logger.info("securityStatus() method started");
		Status status = new Status();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if(accesspoint == null){
				status.setCode(NMSHelper.NODE_NOT_EXIST);
				status.setMessage("verify MacAddress and Network details");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
			}
			if (accesspoint.canConfigure()) {
				IConfiguration configuration=accesspoint.getConfiguration();
				IInterfaceConfiguration iinterfaceConfiguration=configuration.getInterfaceConfiguration();
				IInterfaceInfo interfaceInfo=iinterfaceConfiguration.getInterfaceByName(interfaceName);
				ISecurityConfiguration securityConfiguration =interfaceInfo.getSecurityConfiguration();
				int securityType = securityConfiguration.getEnabledSecurity();
				switch (securityType) {
				case 1:
					logger.info("security not provided given key");
					status.setCode(1);
					status.setRebootRequired("NO");
					status.setMessage("security not provided given key");
					break;
				case 2:
					logger.info("Trying to get the WEP Security ");
					IWEPConfiguration webConfiguration = securityConfiguration.getWEPConfiguration();
					if(webConfiguration != null){
						WEPConfigurationVO wepConfigurationVO=ModelVOHelper.getWEPConfigurationVO(webConfiguration);
						status.setCode(1);
						status.setRebootRequired("NO");
						status.setResponseData(wepConfigurationVO);
						status.setMessage("WEP security information received successfully");
						logger.info("WEP security information received successfully");
					}else{
						status.setCode(2);
						status.setRebootRequired("NO");
						status.setMessage("WEP security information not received");
						logger.info("sent the web security information through status");
					}
					break;
				case 4:
					logger.info("Trying to get the WPA Personal Security ");
					IPSKConfiguration pskConfiguration = securityConfiguration.getPSKConfiguration();
					if(pskConfiguration != null){
						PSKConfigurationVO pskConfigurationVO=ModelVOHelper.getPSKConfigurationVO(pskConfiguration);
						status.setCode(1);
						status.setRebootRequired("NO");
						status.setMessage("WPA personal security information received successfully");
						status.setResponseData(pskConfigurationVO);
						logger.info("WPA personal security information received successfully");
					}else{
						status.setCode(2);
						status.setRebootRequired("NO");
						status.setMessage("WPA perstatussonal security information not received");
						logger.info("WPA personal security information not received");
					}
					break;
				case 8:
					logger.info("Trying to get the WPA Enterprise Security ");
					IRadiusConfiguration radiusConfiguration = securityConfiguration.getRadiusConfiguration();
					if(radiusConfiguration != null){
						RadiusConfigurationVO radiusConfigurationVO=ModelVOHelper.getRadiusConfigurationVO(radiusConfiguration);
						status.setCode(1);
						status.setRebootRequired("NO");
						status.setMessage("WPA Enterprise security information recevied successfully");
						status.setResponseData(radiusConfigurationVO);
						logger.info("WPA Enterprise security information recevied successfully");
					}else{
						status.setCode(2);
						status.setRebootRequired("NO");
						status.setMessage("WPA Enterprise security information not received");
						logger.info("WPA Enterprise security information not received");
					}
					break;

				default:
					logger.info("default swith statement executing");
					status.setCode(2);
					status.setRebootRequired("NO");
					status.setMessage("There is no security key enable using given enableSecurityKey");
					break;
				}
			}
		} catch (Exception e) {
			logger.error("failed to check the securityStatus "+e.getMessage());
			status.setCode(2);
			status.setRebootRequired("NO");
			status.setMessage("failed to check the security Status");
			return status;
		}
		logger.info("securityStatus() method ended");
		return status;	
	}
	/***
	 * 
	 * @param jsonString
	 * @param networkName
	 * @param macAddress
	 * @return
	 */
	
	@RequestMapping(value = "/updateSecurity", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Status updateSecurity(@RequestBody String jsonString,@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress){
		logger.info("updateSecurity() method started ");
		Status status = new Status();
		
		try {
			if (jsonString == null || jsonString.length() < 1) {
				return new Status(1, "requestbody empty");
			}
			JSONObject jsonObj = NMSHelper.pasrseString(jsonString);
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if(accesspoint == null){
				status.setCode(NMSHelper.NODE_NOT_EXIST);
				status.setMessage("verify MacAddress and Network details");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
			}
			if (accesspoint.canConfigure()) {
				IConfigStatusHandler statusHandler = accesspoint.getUpdateStatusHandler();
				IConfiguration configuration=accesspoint.getConfiguration();
				IInterfaceConfiguration iinterfaceConfiguration=configuration.getInterfaceConfiguration();
				String interfaceName=(String) jsonObj.get("interfaceName");
				IInterfaceInfo interfaceInfo=null;
				if(interfaceName.equals("wlan0") || interfaceName.equals("wlan2")){
				interfaceInfo=iinterfaceConfiguration.getInterfaceByName((String) jsonObj.get("interfaceName"));
				
				}else{
					status.setCode(2);
					status.setMessage("No Security for  Interface :"+interfaceName);
				}
				ISecurityConfiguration securityConfiguration =interfaceInfo.getSecurityConfiguration();
				int enabledSecurity = (int)(long)jsonObj.get("enabledSecurity");
				int security=securityConfiguration.getEnabledSecurity();
				/*if(enabledSecurity == security){
					status.setCode(2);
					status.setMessage("NO change in security :");
					status.setRebootRequired("NO");
					return status;
				}*/
				if(interfaceInfo != null){
					switch (enabledSecurity) {
					case 1:	
						securityConfiguration.setEnabledSecurity(enabledSecurity);
						statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
						statusHandler.securityChanged(true);
						if(statusHandler.isAnyPacketChanged()){
							accesspoint.updateChangedConfiguration();
							status.setCode(1);
							status.setRebootRequired("YES");
							status.setMessage("NO Security updated successfully.");
							}
						break;
					case 2:	
						WEPConfiguration wepConfiguration = ModelVOHelper.updateWEPSecurity(jsonObj, securityConfiguration);
						securityConfiguration.setEnabledSecurity(enabledSecurity);
						statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
						statusHandler.securityChanged(true);
						if(statusHandler.isAnyPacketChanged()){
							accesspoint.updateChangedConfiguration();
							logger.info("WEP Security updated Successfully");
							status.setCode(1);
							status.setRebootRequired("YES");
							status.setMessage("WEP Security updated successfully.");
							status.setResponseData(wepConfiguration);
						}
						break;
					case 4:
						PSKConfiguration pskConfiguration = ModelVOHelper.updateWPAPersonalSecurity(jsonObj, securityConfiguration,interfaceInfo.getEssid());
						if(pskConfiguration == null){
							status.setCode(1);
							status.setRebootRequired("NO");
							status.setMessage("pskCipherCCMP & pskCipherTKIP Both are not Same and different values Always.");
							return status;
						}
						securityConfiguration.setEnabledSecurity(enabledSecurity);
						statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
						statusHandler.securityChanged(true);
						if(statusHandler.isAnyPacketChanged()){
							accesspoint.updateChangedConfiguration();
							logger.info("WPA Personal Security updated successfully.");
							status.setCode(1);
							status.setRebootRequired("YES");
							status.setMessage("WPA Personal Security updated successfully.");
							status.setResponseData(pskConfiguration);
						}
						break;
					case 8:	
						RadiusConfiguration radiusConfiguration = ModelVOHelper.updateWPAEnterpriseSecurity(jsonObj,
								securityConfiguration);
						securityConfiguration.setEnabledSecurity(enabledSecurity);
						statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
						statusHandler.securityChanged(true);
						if(statusHandler.isAnyPacketChanged()){
							accesspoint.updateChangedConfiguration();
							logger.info("WPA Enterprise Security updated successfully.");
							status.setCode(1);
							status.setRebootRequired("YES");
							status.setMessage("WPA Enterprise Security updated successfully.");
							status.setResponseData(radiusConfiguration);
						}					
						break;
					default:
						status.setCode(1);
						status.setRebootRequired("NO");
						status.setMessage("unknown security type.");
						break;
					}
				
				
				}}} catch (Exception e) {
		    e.printStackTrace();
			logger.error("failed to update the security "+e.getMessage());
			status.setCode(2);
			status.setRebootRequired("NO");
			status.setMessage("failed to update the security");
			return status;
		}
		logger.info("updateSecurity() method ended");
		return status;
	}		
	/***
	 * 
	 * @param jsonString
	 * @return
	 */
	@RequestMapping(value="/createNetwork",method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status creatNetwork(@RequestBody String jsonString){
		logger.info("create network ");
		Status status=new Status();
		try {
			if (jsonString == null || jsonString.length() < 1) {
				return new Status(2, "requestbody empty");
			}
			JSONObject jsonObject=NMSHelper.pasrseString(jsonString);
			String networkId=(String) jsonObject.get("networkId");
			String networkKey=(String) jsonObject.get("networkKey");
			if(networkKey.equals("") || networkKey.length()>16){
				return new Status(2, "Network key length cannot be more than 16 characters");
			}
			Map<String,MeshViewer> map=SingletonObject.getInstance();
			if(map.containsKey("mesh")){
				  MeshViewer meshViewer =map.get("mesh");
				  MeshNetwork meshNetwork =meshViewer.getMeshNetworkByName(networkId);
				  if(meshNetwork == null){
					   meshNetwork=meshViewer.createMeshNetwork(networkId,networkKey,(byte) 0);
					   if(meshNetwork != null){
							  status.setCode(1);
							  status.setRebootRequired("NO");
							  status.setMessage("MeshNetwork created successfully");
						      }else{
							  status.setCode(0);
							  status.setRebootRequired("NO");
							  status.setMessage("MeshNetwork not created ");
						  }
					 }else{
						  status.setCode(2);
						  status.setRebootRequired("NO");
						  status.setMessage("MeshNetwork already created ");
					 }				 
				}
			} catch (Exception e) {
				status.setCode(0);
				status.setRebootRequired("NO");
			    status.setMessage("some internal problem");
			    return status;
		}
		return status;		
	}
/***
 * 
 * @param networkName
 * @param macAddress
 * @return
 */
	
	@RequestMapping(value="/reboot",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status rebootRequest(@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress){
		logger.info("node rebooting ...");
		Status status=new Status();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if(accesspoint == null){
				status.setCode(NMSHelper.NODE_NOT_EXIST);
				status.setMessage("verify MacAddress and Network details");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
			}
			accesspoint.reboot();
			status.setCode(1);
			status.setRebootRequired("NO");
			status.setMessage("Node rebooting.. wait....");
			
		} catch (Exception e) {
			status.setCode(0);
			status.setRebootRequired("NO");
		    status.setMessage("some internal problem");
		    return status;
		}
		return status;
	}
	/***
	 * @param networkName
	 * @param macAddress
	 * @return
	 */
	@RequestMapping(value="/stationInfo",method=RequestMethod.GET,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<StationInfo> getStationInfo(@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress){
		logger.info("get Stations  info under Node");
		List<StationInfo> station=new ArrayList<StationInfo>();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
		    IRuntimeConfiguration runTimeConfiguration=accesspoint.getRuntimeApConfiguration();
		    Enumeration<IStaConfiguration> stationList=runTimeConfiguration.getStaList();
		    if(stationList != null){
		    	while(stationList.hasMoreElements()){
		    		IStaConfiguration stationConfiguration=stationList.nextElement();
		    		StationInfo info=new StationInfo();
		    		info.setStaMacAddress(stationConfiguration.getStaMacAddress().toString());
		    		info.setDwnlinkTransmitRate(stationConfiguration.getBitRate());
		    		info.setSignal(stationConfiguration.getSignal());
		    		info.setEssid(stationConfiguration.getParentESSID());
		    		//info.setBssid(stationConfiguration.get);
		    		info.setActivityTime(stationConfiguration.getActiveTime());
		    		info.setBytesReceived(stationConfiguration.getBytesReceived());
		    		info.setBytesSent(stationConfiguration.getBytesSent());
		    		station.add(info);
		    		}
		    }
		} catch (Exception e) {
			// TODO: handle exception
			logger.info("some internal problem ");
			return null;
		}
		logger.info("station info received successfully ");;
		return station;
	}
	/***
	 * @param networkName
	 * @param macAddress
	  * @return
	 */
	@RequestMapping(value="/getHeartBeat",method=RequestMethod.GET,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody HeartBeatVO getHeartBeatInfo(@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress){
		logger.info("Get HeartBeat Info of :"+macAddress);
		HeartBeatVO heartBeatVO=new HeartBeatVO();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
		    IRuntimeConfiguration runTimeConfiguration=accesspoint.getRuntimeApConfiguration();
		    ModelVOHelper.getHeartBeat(runTimeConfiguration, heartBeatVO);
		    heartBeatVO.setDsMacAddress(accesspoint.getDSMacAddress());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return heartBeatVO;
	}
	@RequestMapping(value="/heartBeat2",method=RequestMethod.GET,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody HeartBeat2VO getHeartBeat2Info(@RequestParam("networkName") String networkName, @RequestParam("macAddress") String macAddress){
		logger.info("Get HeartBeat2 Info of :"+macAddress);
		HeartBeat2VO heartBeat2VO=new HeartBeat2VO();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
		    IRuntimeConfiguration runTimeConfiguration=accesspoint.getRuntimeApConfiguration();
		    ModelVOHelper.getHeartBeat2Info(runTimeConfiguration, heartBeat2VO);
		    heartBeat2VO.setDsMacAddress(accesspoint.getDSMacAddress());
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		return heartBeat2VO;
	}
	/***
	 * @return
	 */
	@RequestMapping(value="/advanceInfo",method=RequestMethod.GET,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status getAdvanceInfo(@RequestParam("name") String interfaceName,@RequestParam("networkName") String networkName,@RequestParam("macAddress") String macAddress){
		Status status=new Status();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if(accesspoint.canConfigure()){
			     IConfiguration configuration=accesspoint.getConfiguration();
			     IInterfaceConfiguration interfaceConfiguration=configuration.getInterfaceConfiguration();
			     InterfaceInfo inter=(InterfaceInfo) interfaceConfiguration.getInterfaceByName(interfaceName);
			if(inter != null){
				if(inter.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_24GHz_N ||
						inter.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_5GHz_N ||
						inter.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_AN ||
						inter.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_AC ||
						inter.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_ANAC ||
						inter.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_BGN){
				        InterfaceAdvanceConfiguration advanceInfo=inter.getInterfaceAdvanceConfiguration();
					InterfaceAdvanceInfo advanceInfoVO=ModelVOHelper.getAdvanceInfo(advanceInfo);
					status.setCode(1);
					status.setMessage("Interface AdvanceInfo received successfully");
					status.setRebootRequired("NO");
					status.setResponseData(advanceInfoVO);
				}else{
							status.setCode(1);
							status.setMessage("Interface Don't have AdvanceInfo ");
							status.setRebootRequired("NO");
							}
			}}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			status.setCode(0);
			status.setMessage("Some Internal Problem");
			status.setRebootRequired("NO");
			return status;
		}
		return status;
	}
	/***
	 * @param jsonValue
	 * @param networkName
	 * @param macAddress
	name=wlan2&networkName=default&macAddress=30:14:4A:EA:88:7D * @return
	 */
	@RequestMapping(value="/updateAdvanceInfo",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status updateAdvanceInfo(@RequestBody String jsonValue,@RequestParam("networkName") String networkName,@RequestParam("macAddress") String macAddress){
	 Status status=new Status();
		try {
			if (jsonValue == null || jsonValue.length() < 1) {
				return new Status(2, "requestbody empty");
			}
			JSONObject jsonObject=NMSHelper.pasrseString(jsonValue);
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if(accesspoint == null){
				status.setCode(NMSHelper.NODE_NOT_EXIST);
				status.setMessage("verify MacAddress and Network details");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
			}
			if(accesspoint.canConfigure()){
			     IConfiguration configuration=accesspoint.getConfiguration();
			     IInterfaceConfiguration interfaceConfiguration=configuration.getInterfaceConfiguration();
			     IConfigStatusHandler statusHandler=accesspoint.getUpdateStatusHandler();
			     InterfaceInfo inter=(InterfaceInfo) interfaceConfiguration.getInterfaceByName((String) jsonObject.get("interfaceName"));
				if(inter != null){
					if(inter.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_24GHz_N ||
							inter.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_5GHz_N ||
							inter.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_AN ||
							inter.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_AC ||
							inter.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_ANAC ||
							inter.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_BGN){
						    InterfaceAdvanceConfiguration info=new InterfaceAdvanceConfiguration();					
					        InterfaceAdvanceConfiguration advanceInfo=inter.getInterfaceAdvanceConfiguration();
					        advanceInfo.copyTo(info);					        
					        ModelVOHelper.updateAdvanceInfo(jsonObject,info,statusHandler);
					        info.copyTo(advanceInfo);
					       short config=info.compare(advanceInfo, statusHandler);
					       if(config >= 1){
					    	  accesspoint.updateChangedConfiguration();
					    	  boolean flag=NMSHelper.wiatForResponse(statusHandler, PacketFactory.AP_CONFIGURATION_INFO);
					    	  if(flag){
					    		      status.setCode(1);
									  status.setMessage("Interface AdvanceInfo updated  successfully");
									  status.setRebootRequired("YES");
					    	  }else{
					    		  status.setCode(1);
								  status.setMessage("Interface AdvanceInfo updated  successfully");
								  status.setRebootRequired("NO");
					    	  }
					       }
				}else{
					  status.setCode(1);
					  status.setMessage("There is NO AdvanceInfo Protocol set");
					  status.setRebootRequired("NO");
				}
			}}
		} catch (Exception e) {
			status.setCode(0);
			status.setMessage("Some Internal Problem");
			status.setRebootRequired("NO");
			return status;
		}
		return status;
		
	}
	
	/***
	 * 
	 * @param networkName
	 * @param macAddress
	 * @return
	 */
	@RequestMapping(value="/firmwareupgrade",method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status firmwareUpgrade(@RequestParam("networkName") String networkName,@RequestParam("macAddress") String macAddress,
			@RequestParam("deviceType") String deviceType,@RequestParam("version") String version){
		logger.info("firmware download and upgrade started wait. it will take time");
		Status status=new Status();
		try {
			AccessPoint accesspoint = NMSHelper.getNMSConfiguration(networkName, macAddress);
			if(accesspoint == null){
				status.setCode(NMSHelper.NODE_NOT_EXIST);
				status.setMessage("verify MacAddress and Network details");
				status.setRebootRequired("NO");
				status.setResponseData("");
				return status;
			}
			if(accesspoint.canConfigure()){
			String fileName=getUpdateFileName(macAddress,deviceType);
			boolean flag=true;
			if(!verifyUpdateFilePresent(fileName))
			    flag=accesspoint.buildImage(macAddress,null,deviceType,version);
			
			if(flag){
				logger.info("firmware  file downloaded successfully");
			accesspoint.firmwareUpgrade();
			status.setCode(NMSHelper.OK);
			status.setMessage("firmware upgrade started ");
			status.setRebootRequired("NO");
			status.setResponseData("");
			}else{
				status.setCode(NMSHelper.NODE_NOT_EXIST);
				status.setMessage("firmware file not downloaded.please ur server details");
				status.setRebootRequired("NO");
				status.setResponseData("");
			}
			}else{
				status.setCode(NMSHelper.NODE_NOT_RECEIVED);
				status.setMessage("Node Data Not yet received");
				status.setRebootRequired("NO");
				status.setResponseData("");				
				}
			} catch (Exception e) {
				
			status.setCode(NMSHelper.INTERNAL_SERVER_ERROR);
			status.setMessage("Some Internal Problem");
			status.setRebootRequired("NO");
			return status;
		}
		return status;
	}
	
	private String getUpdateFileName(String strMacAddress,String deviceType) {
		String fileName = strMacAddress.replace(':', '_');
		fileName += ".bin";
		if(deviceType.startsWith("CNS"))
			fileName = MFile.getUpdatesPath() + "/cns3xxx_md_3.0.1_"+fileName;
			else
				fileName = MFile.getUpdatesPath() + "/imx_md_3.0.1_"+fileName;
		return fileName;
	}
	
private boolean verifyUpdateFilePresent(String fileName) {		
		File f = new File(fileName);
		if(f.exists() == false)
			return false;
		
		return true;
	}
}
  