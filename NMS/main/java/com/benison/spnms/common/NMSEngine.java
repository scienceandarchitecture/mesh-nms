package com.benison.spnms.common;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.meshdynamics.api.NMS;
import com.meshdynamics.meshviewer.MeshViewer;

public class NMSEngine extends TimerTask{
	private static Timer timer = null;
	long PERIOD=0;
	private  NMS nms ;
	Map<String,MeshViewer> map=null;
	public NMSEngine(){
		 if (timer != null) {
	            timer.cancel();
	            timer = null;
	        }
		 try {
				PERIOD = 3600000; //Integer.parseInt(NMSProperties.getValue("stationUpTime"));
			  } catch (Exception e) {
				e.printStackTrace();
			 }
		    timer = new Timer();
	        timer.schedule(this, 0, PERIOD);
	       //nms = NMSFactory.createNMS(); 
	        nms=NMS.getInstance();
			boolean flag=false;
			MeshViewer meshViewer =MeshViewer.getInstance();
//			System.out.println(meshViewer.hashCode());
			map=SingletonObject.getInstance();
			if(meshViewer.isRunning() == false){
				meshViewer.startViewer();
				flag=true;
			}
			map.put("mesh", meshViewer);
		//	 nms.start();
	        
		 System.out.println("NMS created");
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		if(map != null && map.size()>0)
		if(map.containsKey("mesh")){
			
		}else{
			nms=NMS.getInstance();
			MeshViewer meshViewer =MeshViewer.getInstance();
			map=SingletonObject.getInstance();
			if(meshViewer.isRunning() == false){
				meshViewer.startViewer();				
			}
			map.put("mesh", meshViewer);
		}
		System.out.println("NMS is alive");
	}

	public NMS getNms() {
		return nms;
	}

	public void setNms(NMS nms) {
		this.nms = nms;
	}

}
