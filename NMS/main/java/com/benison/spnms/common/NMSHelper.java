package com.benison.spnms.common;

import java.io.IOException;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.meshdynamics.meshviewer.MeshViewer;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

public class NMSHelper {
	
	public static final int OK 					= 	200;
	public static final int CREATED 			= 	201;
	public static final int NODE_NOT_EXIST		=	202;
	public static final int EXIST               =   203;
	public static final int NOT_EXIST           =   204;
	public static final int NO_CHANGE           =   205;
	public static final int NODE_NOT_RECEIVED	=	206;
	public static final int VLAN_REMOVED     	=	208;
	public static final int VALIDATION          =   209;
		
	public static final int UNAUTHORISED		=	401;
	public static final int UNSUPPORTED_MEDIA	=	415;
	public static final int RESPONSE_TIMEOUT    =  	440;
	public static final int BAD_REQUEST			=	400;
	public static final int INVALID_TOKEN		=	498;
	
	public static final int INTERNAL_SERVER_ERROR=	500;
	public static final int NOT_IMPLEMENTED		=	501;
	public static final int NOT_SUPPORTED		=	304;
	public static final int NOT_ADDED           =   305;

	public static JSONObject pasrseString(String stringValue) throws Exception {
		JSONObject jsonObject = (JSONObject) new JSONParser().parse(stringValue);
		return jsonObject;
	}

	public static AccessPoint getNMSConfiguration(String  networkId, String macAddress ){
		
		AccessPoint accessPoint=null;
		Map<String,MeshViewer> map=SingletonObject.getInstance();
		if(map.containsKey("mesh")){
		MeshViewer meshViewer =map.get("mesh");
		MeshNetwork meshNetwork =meshViewer.getMeshNetworkByName(networkId);
		if(meshNetwork == null){
			meshViewer.createMeshNetwork(networkId,networkId,(byte) 0);
			}
		  accessPoint= meshNetwork.getAccessPoint(NMSHelper.stringToMacAddress(macAddress));
		  if(accessPoint == null){
			meshNetwork.addAccessPointFromPacket(NMSHelper.stringToMacAddress(macAddress));
			accessPoint=meshNetwork.getAccessPoint(NMSHelper.stringToMacAddress(macAddress));
		  }	
	      //NMS.InterfaceConfiguration nmsInterfaceConfiguration= new NMS.InterfaceConfiguration();
		
		}		
		return accessPoint;
	}

	public static MacAddress stringToMacAddress(String macAddress){
		MacAddress ms=new MacAddress();
		ms.setBytes(macAddress);
		return ms;
	}
	
	public static IpAddress stringToIpAddress(String macAddress){
		IpAddress address=new IpAddress();
		address.setBytes(macAddress);
		return address;
	}
	/***
	 * @param statusHandler
	 * @param packetType
	 * @return
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public synchronized static boolean wiatForResponse(IConfigStatusHandler statusHandler,int packetType) throws IOException, InterruptedException{
		
		/*String waitingTime = MDNMSProperties.getValue("waitingTime");
		String waitCount = MDNMSProperties.getValue("waitCount");*/
		String waitingTime ="2000";
		String waitCount = "10";
		int waitTime = 0;
		int counter = 0;
		int count = 0;
		if (waitingTime != null)
			waitTime = Integer.parseInt(waitingTime);
		boolean flag = false;
		if (waitCount != null)
			counter = Integer.parseInt(waitCount);
		while (true) {
			count++;
			if (statusHandler.isPacketChanged(packetType) == false) {
				flag = true;
				break;
			}
			if (count > counter) {
				flag = false;
				break;
			}
			Thread.sleep(waitTime);
		}
		return true;
	}
}
