package com.benison.spnms.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.meshdynamics.meshviewer.MeshViewer;

public class SingletonObject {
	private static Map<String,MeshViewer> map=null;
	private SingletonObject(){}
	public synchronized static Map<String,MeshViewer> getInstance()
	{
		if(map==null)
		{
			map=new ConcurrentHashMap <String,MeshViewer>();
		}
		
		return map;		
	}


}
