package com.benison.spnms.common;

import java.util.Map;

import com.meshdynamics.api.impl.NetworkImplementation;
import com.meshdynamics.api.impl.NodeImplementation;
import com.meshdynamics.meshviewer.MeshViewer;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.util.MacAddress;

public class NodeImplementationFactory {


	public static NodeImplementation getNMSDetails(String networkName, String macAddress) {
		Map<String,MeshViewer> map=SingletonObject.getInstance();
		AccessPoint accessPoint = null;
		NetworkImplementation network = null;
		if(map.containsKey("mesh")){
			MeshViewer meshViewer =map.get("mesh");
			MeshNetwork meshNetwork = meshViewer.getMeshNetworkByName(networkName);
			if(meshNetwork == null){
				meshViewer.createMeshNetwork(networkName, networkName, (byte) 0);
			}
			MacAddress stringToMacAddress = NMSHelper.stringToMacAddress(macAddress);
			accessPoint = meshNetwork.getAccessPoint(stringToMacAddress);
			if(accessPoint == null){
				meshNetwork.addAccessPointFromPacket(NMSHelper.stringToMacAddress(macAddress));
				accessPoint=meshNetwork.getAccessPoint(NMSHelper.stringToMacAddress(macAddress));
			  }	
			//network = new NetworkImplementation(meshNetwork);
		}
		NodeImplementation implementation =null; //new NodeImplementation(network, accessPoint);
		return implementation;
	}

}
