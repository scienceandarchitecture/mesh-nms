package com.benison.spnms.common;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.benison.spnms.valueObject.GeneralConfigurationVO;
import com.benison.spnms.valueObject.HeartBeat2VO;
import com.benison.spnms.valueObject.HeartBeatVO;
import com.benison.spnms.valueObject.InterfaceAdvanceInfo;
import com.benison.spnms.valueObject.InterfaceVO;
import com.benison.spnms.valueObject.PSKConfigurationVO;
import com.benison.spnms.valueObject.RadiusConfigurationVO;
import com.benison.spnms.valueObject.VlanVO;
import com.benison.spnms.valueObject.WEPConfigurationVO;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;
import com.meshdynamics.meshviewer.configuration.INetworkConfiguration;
import com.meshdynamics.meshviewer.configuration.IPSKConfiguration;
import com.meshdynamics.meshviewer.configuration.IRadiusConfiguration;
import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.meshviewer.configuration.IWEPConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.ACLInfo;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceAdvanceConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceInfo;
import com.meshdynamics.meshviewer.configuration.impl.PSKConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.RadiusConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.VLANInfo;
import com.meshdynamics.meshviewer.configuration.impl.WEPConfiguration;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.HexHelper;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;
import com.meshdynamics.util.RSN_PSK;
import com.meshdynamics.util.WEP;

public class ModelVOHelper {
	private final static short	 KEY1_INDEX	  				= 0;
	private final static short	 KEY2_INDEX					= 1;
	private final static short	 KEY3_INDEX 				= 2 ;
	private final static short	 KEY4_INDEX					= 3;
	private final static int	 KEY_STRENGTH_64_BIT_INDEX	= 0;
	private final static int	 KEY_STRENGTH_128_BIT_INDEX	= 1;
	public static final short	 KEY_STRENGTH_64_BIT	= 40;
	public static final short	 KEY_STRENGTH_128_BIT	= 104;
	public static final int[]    subTypesFor5GHZ               = {1,10,11,13,14};
	public static final int[]    subTypesFor24GHZ              = {2,10,3,4,12};
	public static final String   FREQ_FIVE					  = "5GHz";
	public static final String   FREQ_TWO					  = "2.4GHz";

	public static void interfaceToInterfaceVO(InterfaceInfo inter,InterfaceVO interfaceVO){
		interfaceVO.setMacAddress(inter.getMacAddress().toString());
		interfaceVO.setAckTimeout(inter.getAckTimeout());
		interfaceVO.setBeaconInterval(inter.getBeaconInterval());
		interfaceVO.setChannel(inter.getChannel());
		interfaceVO.setBondingType(inter.getChannelBondingType());
		interfaceVO.setDcaListCount(inter.getDcaListCount());
		interfaceVO.setDcaList(inter.getDcaList());
		interfaceVO.setDcaEnabled(inter.getDot11eEnabled());
		interfaceVO.setEssid(inter.getEssid());
		interfaceVO.setEssidHidden(inter.getEssidHidden());
		interfaceVO.setFragThreshold(inter.getFragThreshold());
		interfaceVO.setMediumSubType(inter.getMediumSubType());
		short val=inter.getMediumSubType();
		if(val == Mesh.PHY_SUB_TYPE_802_11_24GHz_N ||
				val == Mesh.PHY_SUB_TYPE_802_11_5GHz_N ||
				val == Mesh.PHY_SUB_TYPE_802_11_AN   ||
				val == Mesh.PHY_SUB_TYPE_802_11_AC   ||
				val == Mesh.PHY_SUB_TYPE_802_11_ANAC ||
				val == Mesh.PHY_SUB_TYPE_802_11_BGN){
			InterfaceAdvanceConfiguration advanceInfo=inter.getInterfaceAdvanceConfiguration();
			interfaceVO.setInterfaceAdvanceInfo(getAdvanceInfo(advanceInfo));			
		}else{
			interfaceVO.setInterfaceAdvanceInfo(null);
		}
		interfaceVO.setMediumType(inter.getMediumType());
		interfaceVO.setInterfaceName(inter.getName());
		interfaceVO.setRtsThreshold(inter.getRtsThreshold());
		interfaceVO.setServiceType(inter.getServiceType());
		interfaceVO.setTxPower(inter.getTxPower());
		interfaceVO.setTxRate(inter.getTxRate());
		interfaceVO.setUsageType(inter.getUsageType());
		interfaceVO.setOperatingChannel(inter.getOperatingChannel());
		interfaceVO.setDot11eCategory(inter.getDot11eCategory());
		interfaceVO.setDot11eEnabled(inter.getDot11eEnabled());		
	}
	
	public static boolean  interfaceVOTOInterface(JSONObject jsonObject,InterfaceInfo inter,IConfigStatusHandler statusHandler){
		boolean flag=false;
		if(jsonObject.get("macAddress") != null){
		inter.setMacAddress(NMSHelper.stringToMacAddress((String) jsonObject.get("macAddress")));
		}
		if(jsonObject.get("ackTimeout") != null){
			short val=(short)(long)jsonObject.get("ackTimeout");
		      if(inter.getAckTimeout() != val){
		           inter.setAckTimeout(val);
		           statusHandler.packetChanged(PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET);
		     }
		}
		if(jsonObject.get("beaconInterval") != null){			
			long val=(long)jsonObject.get("beaconInterval");
		        if(inter.getBeaconInterval() != val){
		           inter.setBeaconInterval(val);
		           statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		       }
		   }
		if(jsonObject.get("channel") != null){			
			short val=(short)(long)jsonObject.get("channel");
		    if(inter.getChannel() != val){
		        inter.setChannel(val);
		        statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		}
		}
		if(jsonObject.get("bondingType") != null){			
			short val=(short)(long)jsonObject.get("bondingType");
	    	if(inter.getChannelBondingType() != val){
		    inter.setChannelBondingType(val);
		     statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		}
		}
		if(jsonObject.get("essid") != null){
			if(!inter.getEssid().equals((String)jsonObject.get("essid"))){
		       inter.setEssid((String)jsonObject.get("essid"));
		       statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		   }
		}
		if(jsonObject.get("essidHidden") != null){
			    short val=(short)(long)jsonObject.get("essidHidden");
				if(inter.getEssidHidden() != val){
		            inter.setEssidHidden(val);
		            statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		    }
		}
		if(jsonObject.get("fragThreshold") != null){
			long val=(long)jsonObject.get("fragThreshold");
			if(inter.getFragThreshold() != val){
		         inter.setFragThreshold(val);
		         statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		      }
		}
		if(jsonObject.get("mediumSubType") != null){
			short val=(short)(long)jsonObject.get("mediumSubType");
			if(val == Mesh.PHY_SUB_TYPE_802_11_24GHz_N ||
					val == Mesh.PHY_SUB_TYPE_802_11_5GHz_N ||
					val == Mesh.PHY_SUB_TYPE_802_11_AN ||
					val == Mesh.PHY_SUB_TYPE_802_11_AC ||
					val == Mesh.PHY_SUB_TYPE_802_11_ANAC ||
					val == Mesh.PHY_SUB_TYPE_802_11_BGN){
				if(jsonObject.get("advanceInfo") != null){
				JSONObject getSth = (JSONObject)jsonObject.get("advanceInfo");
		    	flag=updateAdvanceInfo(getSth,inter.getInterfaceAdvanceConfiguration(),statusHandler);
				}
			}
		    if(inter.getMediumSubType() != val){	
		    	flag=true;
		    	inter.setMediumSubType(val);
		        statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		   }
		}
		if(jsonObject.get("mediumType") != null){
			short val=(short)(long)jsonObject.get("mediumType");
		if(inter.getMediumType() != val){
		     inter.setMediumType(val);
		      statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		     }
		}
		//inter.setName(interfaceVO.getName());
		if(jsonObject.get("rtsThreshold") != null){			
			long val=(long)jsonObject.get("rtsThreshold");
		    if(inter.getRtsThreshold() != val){
		         inter.setRtsThreshold(val);
		         statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		       }
		    }
		if(jsonObject.get("serviceType") != null){
			short val=(short)(long)jsonObject.get("serviceType");		
		if(inter.getServiceType() != val){
		        inter.setServiceType(val);
		        statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		     }
		}
		if(jsonObject.get("txPower") != null){
			short val=(short)(long)jsonObject.get("txPower");
		if(inter.getTxPower() != val){
		      inter.setTxPower(val);
		      statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		    }
		}
		if(jsonObject.get("txRate") != null){
			long val=(long)jsonObject.get("txRate");
		if(inter.getTxRate() != val){
		         inter.setTxRate(val);
		         statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		}
		}
		if(jsonObject.get("usageType") != null){
			short val=(short)(long)jsonObject.get("usageType");
		if(inter.getUsageType() != val){
		      inter.setUsageType(val);
		      statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
	    	}
		}
		if(jsonObject.get("operatingChannel") != null){
			short val=(short)(long)jsonObject.get("operatingChannel");		
		       if(inter.getOperatingChannel() != val){
		        inter.setOperatingChannel(val);
		       statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		    }
		}
		if(jsonObject.get("dot11eEnabled") != null){
			short val=(short)(long)jsonObject.get("dot11eEnabled");	
			if(inter.getDot11eCategory() != val){
		             inter.setDot11eCategory(val);
		            statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		       }
			}
		if(jsonObject.get("dot11eCategory") != null){
			short val=(short)(long)jsonObject.get("dot11eCategory");		
		if(inter.getDot11eEnabled() != val){
		       inter.setDot11eEnabled(val);	
		       statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		    }
		}
		if(jsonObject.get("dcaList") != null){
		JSONArray jArray = (JSONArray) jsonObject.get("dcaList");
	     if(jArray.size() >0){
	    	 inter.setDcaListCount((short)jArray.size());
	    	 for(int i=0;i<jArray.size();i++){
	    		 inter.setDcaChannel(i,(short)(long)jArray.get(i));	    		
	    	 }	    	
	    	 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
	     }
		}
		return flag;
	}

	/*private static void verifyMediumSubType(short val, String name) {
		if(name.equals("wlan1") || name.equals("wlan0")){
			if(val == Mesh.)
		}
		
	}*/

	public static String verifyMediumSubType(short val, String name) {
		if(val ==Mesh.PHY_SUB_TYPE_802_11_A       ||
				val ==Mesh.PHY_SUB_TYPE_802_11_AN ||
				val ==Mesh.PHY_SUB_TYPE_802_11_AC ||
				val ==Mesh.PHY_SUB_TYPE_802_11_5GHz_N ||
				val ==Mesh.PHY_SUB_TYPE_802_11_ANAC){			
			return FREQ_FIVE;
		}else if(val == Mesh.PHY_SUB_TYPE_802_11_B ||
				 val == Mesh.PHY_SUB_TYPE_802_11_B_G ||
			     val == Mesh.PHY_SUB_TYPE_802_11_G ||
			     val == Mesh.PHY_SUB_TYPE_802_11_24GHz_N ||
				 val == Mesh.PHY_SUB_TYPE_802_11_BGN){
				return FREQ_TWO;
			}
		return null;
		}
		
	

	public static void vlanInfoToVlanVO(VLANInfo vlanInfo, VlanVO vlanVO) {
		vlanVO.setVlanName(vlanInfo.getName());
		vlanVO.setEssid(vlanInfo.getEssid());
		vlanVO.setTag((short) vlanInfo.getTag());
		vlanVO.setDot11eEnabled(vlanInfo.get80211eEnabled());
		vlanVO.setDot11eCategory(vlanInfo.get80211eCategoryIndex());
		vlanVO.setDot1pPriority(vlanInfo.get8021pPriority());
		vlanVO.setSecurityType(vlanInfo.getServiceType());
		vlanVO.setFragThreshold(vlanInfo.getfragThreshold());
		vlanVO.setRtsThreshold(vlanInfo.getRtsThreshold());
		vlanVO.setTxPower(vlanInfo.getTxPower());
		vlanVO.setTxRate(vlanInfo.getTxRate());
		vlanVO.setIpAddress(vlanInfo.getIpAddress().toString());
		//vlanVO.setSecurityInfo(vlanInfo.getSecurityConfiguration());
	}

	public static void jsonToVlanObject(JSONObject jsonObject, IVlanInfo tempInfo) {
	
		String ipAddress = null;
		
		tempInfo.setName((String) jsonObject.get("vlanName"));
		tempInfo.setEssid((String) jsonObject.get("essid"));
		tempInfo.setTag((int)(long)jsonObject.get("tag"));
		tempInfo.set80211eCategoryIndex((short)(long) jsonObject.get("dot11eCategory"));
		tempInfo.set80211eEnabled((short)(long) jsonObject.get("dot11eEnabled"));
		tempInfo.set8021pPriority((short)(long) jsonObject.get("dot1pPriority"));
		tempInfo.setFragThreshold((long) jsonObject.get("fragThreshold"));
		tempInfo.setRtsThreshold((long) jsonObject.get("rtsThreshold"));
		tempInfo.setBeaconInterval((long) jsonObject.get("beaconInt"));
		tempInfo.setServiceType((short)(long) jsonObject.get("serviceType"));
		tempInfo.setTxPower((short)(long) jsonObject.get("txPower"));
		tempInfo.setTxRate((long) jsonObject.get("txRate"));
		ipAddress = (String)jsonObject.get("ipAddress");
		IpAddress address = NMSHelper.stringToIpAddress(ipAddress);
		tempInfo.setIpAddress(address);
	}

	public static void jsonToACLObject(JSONObject jsonObj, ACLInfo aclInfo) {
		String macAddress = null;
		aclInfo.set80211eCategoryIndex((short)(long)jsonObj.get("_80211eCategoryIndex"));
		aclInfo.setAllow((short)(long)jsonObj.get("allowEntry"));
		aclInfo.setEnable80211eCategory((short)(long)jsonObj.get("_80211eCategoryEnabled"));
		aclInfo.setVLANTag((int)(long)jsonObj.get("vlanTag"));
		
		macAddress = (String)jsonObj.get("MacAddress");
		MacAddress address = NMSHelper.stringToMacAddress(macAddress);
		aclInfo.setStaAddress(address);
	}

	public static  boolean jsonToGeneralConfiguration(JSONObject jsonObj, IConfiguration configuration,IConfigStatusHandler statusHandler) {
		boolean flag=false;
		try {
			
			if(jsonObj.get("nodeName") != null){
				String val=(String) jsonObj.get("nodeName");
				if(!configuration.getNodeName().equalsIgnoreCase(val)){
					 configuration.setNodeName((String) jsonObj.get("nodeName"));
			         statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			      }
			}
			
			if(jsonObj.get("countryCode") != null){
				int val=(int)(long) jsonObj.get("countryCode");
				if(configuration.getCountryCode() !=val){
					 flag=true;
					 configuration.setCountryCode((int)(long) jsonObj.get("countryCode"));
			         statusHandler.packetChanged(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET);
			      }
			}
			if(jsonObj.get("preferedParent") != null){
				String val=(String) jsonObj.get("preferedParent");
				if(!configuration.getPreferedParent().equalsIgnoreCase(val)){
					 flag=true;
					 configuration.setPreferedParent((String) jsonObj.get("preferedParent"));
			         statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			      }
			}
			if(jsonObj.get("description") != null){
				String val=(String) jsonObj.get("description");
				if(!configuration.getDescription().equalsIgnoreCase(val)){
					 configuration.setDescription((String) jsonObj.get("description"));
			         statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			      }
			}
			if( jsonObj.get("model") != null){
				String val=(String) jsonObj.get("model");
				if(!configuration.getHardwareModel().equalsIgnoreCase(val)){
					 configuration.setHardwareModel((String) jsonObj.get("model"));
			         statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			      }
			}
			if( jsonObj.get("latitude") != null){
				String val=(String) jsonObj.get("latitude");
				if(!configuration.getLatitude().equalsIgnoreCase(val)){
					 configuration.setLatitude((String) jsonObj.get("latitude"));
			         statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			      }
			}
			if( jsonObj.get("longitude") != null){
				String val=(String) jsonObj.get("longitude");
				if(!configuration.getLongitude().equalsIgnoreCase(val)){
					 configuration.setLongitude((String) jsonObj.get("longitude"));
			         statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			      }
			}
			INetworkConfiguration networkConfiguration=configuration.getNetworkConfiguration();
			if(jsonObj.get("hostname") != null){
				String val=(String) jsonObj.get("hostname");
				if(!networkConfiguration.getHostName().equalsIgnoreCase(val)){
					 networkConfiguration.setHostName((String) jsonObj.get("hostname"));
			         statusHandler.packetChanged(PacketFactory.AP_IP_CONFIGURATION_INFO);
			      }
			}
			if(jsonObj.get("ipAddress") != null){
				if(!networkConfiguration.getHostName().equals(NMSHelper.stringToIpAddress((String)jsonObj.get("ipAddress")))){
					 networkConfiguration.setIpAddress(NMSHelper.stringToIpAddress((String)jsonObj.get("ipAddress")));
			         statusHandler.packetChanged(PacketFactory.AP_IP_CONFIGURATION_INFO);
			      }
			}
			
			if(jsonObj.get("subnetMask") != null){
				if(!networkConfiguration.getHostName().equals(NMSHelper.stringToIpAddress((String) jsonObj.get("subnetMask")))){
					 networkConfiguration.setSubnetMask(NMSHelper.stringToIpAddress((String) jsonObj.get("subnetMask")));
			         statusHandler.packetChanged(PacketFactory.AP_IP_CONFIGURATION_INFO);
			      }
			}
			if(jsonObj.get("gateWay") != null){
				if(!networkConfiguration.getGateway().equals(NMSHelper.stringToIpAddress((String) jsonObj.get("gateWay")))){
					  networkConfiguration.setGateway(NMSHelper.stringToIpAddress((String) jsonObj.get("gateWay")));
			          statusHandler.packetChanged(PacketFactory.AP_IP_CONFIGURATION_INFO);
			      }
			}
			IMeshConfiguration mesh=configuration.getMeshConfiguration();
			if(jsonObj.get("heartbeatInterval") != null){
				short val=(short)(long)jsonObj.get("heartbeatInterval");
				if(mesh.getHeartbeatInterval() != val){
					  mesh.setHeartbeatInterval((short)(long) jsonObj.get("heartbeatInterval"));
			          statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			      }
			}
			if(jsonObj.get("mobilityMode") != null){
				short val=(short)(long)jsonObj.get("mobilityMode");
				if(mesh.getMobilityMode() != val){
					mesh.setMobilityMode((short)(long) jsonObj.get("MobilityMode"));
			          statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			      }
			}
			
			if(jsonObj.get("essid") != null){
				String val=(String)jsonObj.get("essid");
				 if(!mesh.getEssid().equals(val)){
					   mesh.setEssid(val);
			          statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
				 }
			}
			
			//configuration.d(short)(long) jsonObj.get("dfsRequired");
			
			//configuration.setRegulatoryDomain((int) jsonObj.get("regulatoryDomain"));
			//configuration.(short)(long) jsonObj.get("options");
			//configuration.setd (short)(long) jsonObj.get("dynamicChannelAllocation");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	public static RadiusConfiguration updateWPAEnterpriseSecurity(JSONObject jsonObj,
			ISecurityConfiguration securityConfiguration) {
		RadiusConfiguration radiusConfiguration = new RadiusConfiguration();
		String ipAddress = null;
		radiusConfiguration.setCipherCCMP((short)(long) jsonObj.get("radiusCipherCCMP"));
		radiusConfiguration.setCipherTKIP((short)(long) jsonObj.get("radiusCipherTKIP"));
		radiusConfiguration.setGroupKeyRenewal((int)(long) jsonObj.get("radiusGroupKeyRenewal"));
		radiusConfiguration.setKey((String) jsonObj.get("radiusKey"));
		radiusConfiguration.setMode((short)(long) jsonObj.get("radiusMode"));
		ipAddress = (String) jsonObj.get("serverIpAddress");		
		IpAddress address = NMSHelper.stringToIpAddress(ipAddress);
		radiusConfiguration.setServerIPAddress(address);
		radiusConfiguration.setPort((long) jsonObj.get("radiusPort"));
		securityConfiguration.setRadiusConfiguration(radiusConfiguration);
		return radiusConfiguration;
	}

	public static PSKConfiguration updateWPAPersonalSecurity(JSONObject jsonObj,
			ISecurityConfiguration securityConfiguration,String essid) {
		
		PSKConfiguration pskConfiguration = new PSKConfiguration();
		if((short)(long) jsonObj.get("pskCipherCCMP") == (short)(long) jsonObj.get("pskCipherTKIP")){
			return null;
		}
		pskConfiguration.setPSKCipherCCMP((short)(long) jsonObj.get("pskCipherCCMP"));
		pskConfiguration.setPSKCipherTKIP((short)(long) jsonObj.get("pskCipherTKIP"));
		pskConfiguration.setPSKGroupKeyRenewal((int)(long) jsonObj.get("pskGroupKeyRenewal"));		
		pskConfiguration.setPSKKeyLength((int)(long) jsonObj.get("pskKeyLength"));
		pskConfiguration.setPSKMode((short)(long) jsonObj.get("pskMode"));
		pskConfiguration.setPSKPassPhrase((String) jsonObj.get("pskPassPhrase"));
	    String key=keyGenerate((String) jsonObj.get("pskPassPhrase"),essid);
	    short[] keyhex=HexHelper.stringToHex(key);
	    pskConfiguration.setPSKKey(keyhex);
		securityConfiguration.setPSKConfiguration(pskConfiguration);
		return pskConfiguration;
	}

	private static String keyGenerate(String genstr,String essId) {

    RSN_PSK 	pskKey 	= new RSN_PSK();				
	byte[] 		data 	= null; 		
	short [] 	shkey 	= new short[32];
	data = pskKey.PSKKeyGenerate(genstr, essId, essId.length());
	if(data == null) {
		return null;
	}
	
	for(int i = 0;i < 32;i++) {  				
		shkey[i] = data[i];  				
	}	
	String key = HexHelper.toHex(shkey);
	return key;
	}

	public static WEPConfiguration updateWEPSecurity(JSONObject jsonObj, ISecurityConfiguration securityConfiguration) {
		
		String 	key 		= ""; 
  		String 	key1 		= "";
  		String 	key2		= "";
  		String 	key3		= "";
  		String 	key4 		= "";
		WEPConfiguration configuration = new WEPConfiguration();
		configuration.setKeyIndex((short)(long) jsonObj.get("keyIndex"));
		configuration.setPassPhrase((String)jsonObj.get("passphrase"));
		WEP  wep		= new WEP();
  		String passphrase=(String)jsonObj.get("passphrase");
	    if((int)(long)jsonObj.get("keyStrength") == KEY_STRENGTH_128_BIT_INDEX ){  						
		char[] data 	= new char[13];
		int ret 	= wep.keyGenerate(passphrase,passphrase.length(), (int)(long)jsonObj.get("keyStrength"), data);
		if(ret == -1) {
			return null;
		}  			
		for(int index = 0; index < 13; index++) {  				
			key += data[index];  				
		}  	
		
		key1 = HexHelper.toHexString(key);
		key2 = HexHelper.toHexString(key);
		key3 = HexHelper.toHexString(key);
		key4 = HexHelper.toHexString(key);
		configuration.setKeyStrength(KEY_STRENGTH_128_BIT);
	}else if ((int)(long)jsonObj.get("keyStrength") == KEY_STRENGTH_64_BIT_INDEX) {
		
		char[] data	= new char[20];
		int ret 	= wep.keyGenerate(passphrase,passphrase.length(), (int)(long)jsonObj.get("keyStrength"), data);
		if(ret == -1) {
			return null;
		}
		
		for(int index = 0;index < 20;index++) {  				
			key	+= data[index];  				
		}  		
		
		key1 = key.substring(0,   5);
		key2 = key.substring(5,  10);
		key3 = key.substring(10, 15);
		key4 = key.substring(15);
		
		key1 = HexHelper.toHexString(key1);
		key2 = HexHelper.toHexString(key2);
		key3 = HexHelper.toHexString(key3);
		key4 = HexHelper.toHexString(key4);
		configuration.setKeyStrength(KEY_STRENGTH_64_BIT);
	}
	    configuration.setKey(KEY1_INDEX, HexHelper.stringToHex(key1));
	    configuration.setKey(KEY2_INDEX, HexHelper.stringToHex(key2));
	    configuration.setKey(KEY3_INDEX, HexHelper.stringToHex(key3));
	    configuration.setKey(KEY4_INDEX, HexHelper.stringToHex(key4));
		securityConfiguration.setWEPConfiguration(configuration);
		return configuration;
	}
	
	
	public static  GeneralConfigurationVO getGeneralConfiguration(GeneralConfigurationVO generalConfigurationVO,IConfiguration configuration){
		generalConfigurationVO.setCountryCode(configuration.getCountryCode());
		generalConfigurationVO.setDescription(configuration.getDescription());
		generalConfigurationVO.setDsMacAddress(configuration.getDSMacAddress().toString());
		generalConfigurationVO.setFirmwareMajorVersion(configuration.getFirmwareMajorVersion());
		//generalConfigurationVO.setFipsEnabled(configuration.getMeshConfiguration().getf);
		generalConfigurationVO.setLatitude(configuration.getLatitude());
		generalConfigurationVO.setLongitude(configuration.getLongitude());
		generalConfigurationVO.setModel(configuration.getHardwareModel());
		generalConfigurationVO.setNodeName(configuration.getNodeName());
		generalConfigurationVO.setPreferredParent(configuration.getPreferedParent());
		generalConfigurationVO.setRegDomain(configuration.getRegulatoryDomain());
		generalConfigurationVO.setGateWay(configuration.getNetworkConfiguration().getGateway().toString());
		generalConfigurationVO.setHostName(configuration.getNetworkConfiguration().getHostName());
		generalConfigurationVO.setIpAddress(configuration.getNetworkConfiguration().getIpAddress().toString());
		generalConfigurationVO.setSubnetMask(configuration.getNetworkConfiguration().getSubnetMask().toString());
		generalConfigurationVO.setHeartbeatInterval(configuration.getMeshConfiguration().getHeartbeatInterval());
		generalConfigurationVO.setMobilityMode(configuration.getMeshConfiguration().getMobilityMode());
		generalConfigurationVO.setEssid(configuration.getMeshConfiguration().getEssid());
		generalConfigurationVO.setSignalMap(configuration.getMeshConfiguration().getSignalMap().toString());
		generalConfigurationVO.setHeartbeatMissCount(configuration.getMeshConfiguration().getHeartbeatMissCount());
		generalConfigurationVO.setHopCost(configuration.getMeshConfiguration().getHopCost());
		generalConfigurationVO.setMaxAllowableHops(configuration.getMeshConfiguration().getMaxAllowableHops());
		//generalConfigurationVO.setLasi(configuration.getMeshConfiguration().);
		//generalConfigurationVO.setCrThreshold((short) configuration.getMeshConfiguration().get);
		//generalConfigurationVO.setInterfaceCount(configuration.getMeshConfiguration().get);
		generalConfigurationVO.setFcc(configuration.getMeshConfiguration().getFCCCertifiedOperation());
		generalConfigurationVO.setEtsi(configuration.getMeshConfiguration().getETSICertifiedOperation());
		generalConfigurationVO.setDsTxRate(configuration.getMeshConfiguration().getDSTxRate());
		generalConfigurationVO.setDsTxPower(configuration.getMeshConfiguration().getDSTxPower());
		generalConfigurationVO.setRtsThreshold(configuration.getMeshConfiguration().getRTSThreshold());
		generalConfigurationVO.setFragThreshold(configuration.getMeshConfiguration().getFragThreshold());
		generalConfigurationVO.setBeaconInterval(configuration.getMeshConfiguration().getBeaconInterval());
		generalConfigurationVO.setBridgeAgeingTime(configuration.getMeshConfiguration().getBridgeAgeingTime());
		generalConfigurationVO.setDynamicChannelAlloc(configuration.getMeshConfiguration().getDynamicChannelAllocation());
		generalConfigurationVO.setStayAwakeCount(configuration.getMeshConfiguration().getStayAwakeCount());
		
		return generalConfigurationVO;		
	}
	
	public static WEPConfigurationVO getWEPConfigurationVO(IWEPConfiguration WEPConfiguration){
		WEPConfigurationVO wepConfigurationVO=new WEPConfigurationVO();
		//wepConfigurationVO.setKey(WEPConfiguration.getKey(keyIndex), key0);
		wepConfigurationVO.setKeyIndex(WEPConfiguration.getKeyIndex());
		wepConfigurationVO.setKeyStrength(WEPConfiguration.getKeyStrength());
		wepConfigurationVO.setPassPhrase(WEPConfiguration.getPassPhrase());		
		return wepConfigurationVO;
	}
	
	public static PSKConfigurationVO getPSKConfigurationVO(IPSKConfiguration PSkConfiguration){
		PSKConfigurationVO pskConfigurationVO=new PSKConfigurationVO();
		pskConfigurationVO.setPSKCipherCCMP(PSkConfiguration.getPSKCipherCCMP());
		pskConfigurationVO.setPSKCipherTKIP(PSkConfiguration.getPSKCipherTKIP());
		pskConfigurationVO.setPSKGroupKeyRenewal(PSkConfiguration.getPSKGroupKeyRenewal());
		pskConfigurationVO.setPSKKeyLength(PSkConfiguration.getPSKKeyLength());
		pskConfigurationVO.setPSKKey(PSkConfiguration.getPSKKey());
		pskConfigurationVO.setPSKMode(PSkConfiguration.getPSKMode());
		pskConfigurationVO.setPSKPassPhrase(PSkConfiguration.getPSKPassPhrase());
				
		return pskConfigurationVO;
	}
	
	public static RadiusConfigurationVO getRadiusConfigurationVO(IRadiusConfiguration radiusConfiguration){
		RadiusConfigurationVO radiusConfigurationVO=new RadiusConfigurationVO();
		radiusConfigurationVO.setCipherCCMP(radiusConfiguration.getCipherCCMP());
		radiusConfigurationVO.setCipherTKIP(radiusConfiguration.getCipherTKIP());
		radiusConfigurationVO.setGroupKeyRenewal(radiusConfiguration.getGroupKeyRenewal());
		radiusConfigurationVO.setKey(radiusConfiguration.getKey());
		radiusConfigurationVO.setMode(radiusConfiguration.getMode());
		radiusConfigurationVO.setPort(radiusConfiguration.getPort());
		radiusConfigurationVO.setServerIPAddress(radiusConfiguration.getServerIPAddress().toString());
		radiusConfigurationVO.setWPAEnterpriseDecidesVlan(radiusConfiguration.getWPAEnterpriseDecidesVlan());		
		return radiusConfigurationVO;
	}
	
	public static HeartBeatVO getHeartBeat(IRuntimeConfiguration runTimeConfiguration,HeartBeatVO heartBeatVO){
		
		heartBeatVO.setSequenceNumber(runTimeConfiguration.getConfigSeqNumber());
		heartBeatVO.setCumulativeTollCost(runTimeConfiguration.getVoltage());
		heartBeatVO.setHopCount(runTimeConfiguration.getHopCount());
		//heartBeatVO.setRootBssid(runTimeConfiguration.getRootBssid().toString());
		heartBeatVO.setHealthIndex((short) runTimeConfiguration.getBoardCPUUsage());
		heartBeatVO.setHeartbeatInterval(runTimeConfiguration.getHeartbeatInterval());
		heartBeatVO.setBitRate(runTimeConfiguration.getBitRate());
		heartBeatVO.setStaCount(runTimeConfiguration.getStaCount());
		heartBeatVO.setBoardTemperature(runTimeConfiguration.getBoardTemperature());
		heartBeatVO.setParentBssid(runTimeConfiguration.getParentBssid().toString());
		heartBeatVO.setApCount((short) runTimeConfiguration.getKnownApCount());
		heartBeatVO.setTxPackets(runTimeConfiguration.getTxPackets());
		heartBeatVO.setRxPackets(runTimeConfiguration.getRxPackets());
		heartBeatVO.setTxBytes(runTimeConfiguration.getTxBytes());
		heartBeatVO.setRxBytes(runTimeConfiguration.getRxBytes());
	
		return heartBeatVO;		
	}
	public static  void getHeartBeat2Info(IRuntimeConfiguration runTimeConfiguration,HeartBeat2VO heartBeat2VO){
		heartBeat2VO.setSequenceNumber(runTimeConfiguration.getHeartbeat2SequenceNumber());
		heartBeat2VO.setUplinkTransmitRate(runTimeConfiguration.getUplinkTransmitRate());
		heartBeat2VO.setDwnlinkTransmitRate(runTimeConfiguration.getDwnlinkTransmitRate());
		if(runTimeConfiguration.isRebootRequired() == true)
		    heartBeat2VO.setRebootRequired("YES");
		else
			heartBeat2VO.setRebootRequired("NO");
		heartBeat2VO.setConfigSeqNumber(runTimeConfiguration.getConfigSeqNumber());
		//heartBeat2VO.setCapabilities(runTimeConfiguration.isc);
		//heartBeat2VO.setConfigcapabilities(configcapabilities);
		//heartBeat2VO.setCmdcapabilities(cmdcapabilities);
	}
	public static InterfaceAdvanceInfo getAdvanceInfo(InterfaceAdvanceConfiguration advanceInfo){
		InterfaceAdvanceInfo advanceInfoVO=new InterfaceAdvanceInfo();
		
		advanceInfoVO.setChannelBandwidth(advanceInfo.getChannelBandwidth());
		advanceInfoVO.setSecondaryChannelPosition(advanceInfo.getSecondaryChannelPosition());
		advanceInfoVO.setGuardInterval_20(advanceInfo.getGuardInterval_20());
		advanceInfoVO.setGuardInterval_40(advanceInfo.getGuardInterval_40());
		advanceInfoVO.setFrameAggregation(advanceInfo.getFrameAggregation());
		advanceInfoVO.setMaxAMPDU(advanceInfo.getMaxAMPDU());
		advanceInfoVO.setMaxAMSDU(advanceInfo.getMaxAMSDU());
		advanceInfoVO.setMaxMPDU(advanceInfo.getMaxMPDU());
		advanceInfoVO.setCoexistence(advanceInfo.getCoexistence());
		advanceInfoVO.setGfMode(advanceInfo.getGFMode());
		advanceInfoVO.setLdpc(advanceInfo.getLDPC());
		advanceInfoVO.setRxSTBC(advanceInfo.getRxSTBC());
		advanceInfoVO.setTxSTBC(advanceInfo.getTxSTBC());	
		
		return advanceInfoVO;
		
	}
	
	public static boolean updateAdvanceInfo(JSONObject jsonObject,InterfaceAdvanceConfiguration advanceInfo,IConfigStatusHandler statusHandler){
		boolean flag=false;
		if(jsonObject.get("channelBandwidth") != null){
			flag=true;
			byte val=(byte)(long)jsonObject.get("channelBandwidth");
			if(val == 1 || val == 0){
			if(jsonObject.get("secondaryChannelPosition") != null){
				byte secondaryChannel=(byte)(long)jsonObject.get("secondaryChannelPosition");
				if(secondaryChannel != advanceInfo.getSecondaryChannelPosition()){
					advanceInfo.setSecondaryChannelPosition(secondaryChannel);
					 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
				}
			}
			if(jsonObject.get("guardInterval_40") != null){
				byte guardInterval_40=(byte)(long)jsonObject.get("guardInterval_40");
				if(guardInterval_40 != advanceInfo.getGuardInterval_40()){
					advanceInfo.setGuardInterval_40(guardInterval_40);
					 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
					}
				}
			}
			if(val == 2){
				if(jsonObject.get("guardInterval_20") != null){
					byte guardInterval_20=(byte)(long)jsonObject.get("guardInterval_20");
					if(guardInterval_20 != advanceInfo.getGuardInterval_20()){
						advanceInfo.setGuardInterval_20(guardInterval_20);
						 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
						}
					}
				
			}
			if(val != advanceInfo.getChannelBandwidth()){
				advanceInfo.setChannelBandwidth(val);
				statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			}	
		}
		
		if(jsonObject.get("maxAMPDU") != null){
			flag=true;
			byte MaxAMPDU=(byte)(long)jsonObject.get("maxAMPDU");
			if(MaxAMPDU != advanceInfo.getMaxAMPDU()){
				advanceInfo.setMaxAMPDU(MaxAMPDU);;
				 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
				}
		}
		if(jsonObject.get("maxAMSDU") != null){
			flag=true;
			byte MaxAMSDU=(byte)(long)jsonObject.get("maxAMSDU");
			if(MaxAMSDU != advanceInfo.getMaxAMSDU()){
				advanceInfo.setMaxAMSDU(MaxAMSDU);;
				 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
				}
		}
		if(jsonObject.get("txSTBC") != null){
			flag=true;
			byte txSTBC=(byte)(long)jsonObject.get("txSTBC");
			if(txSTBC != advanceInfo.getTxSTBC()){
				advanceInfo.setTxSTBC(txSTBC);
				 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			}
		}
		if(jsonObject.get("rxSTBC") != null){
			flag=true;
			byte rxSTBC=(byte)(long)jsonObject.get("rxSTBC");
			if(rxSTBC != advanceInfo.getRxSTBC()){
				advanceInfo.setRxSTBC(rxSTBC);
				 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			}
		}
		if(jsonObject.get("ldpc") != null){
			flag=true;
			byte ldpc=(byte)(long)jsonObject.get("ldpc");
			if(ldpc != advanceInfo.getLDPC()){
				advanceInfo.setLDPC(ldpc);
				 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			}
		}
		if(jsonObject.get("gfMode") != null){
			flag=true;
			byte gfMode=(byte)(long)jsonObject.get("gfMode");
			if(gfMode != advanceInfo.getGFMode()){
				advanceInfo.setGFMode(gfMode);
				 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			}
		}
		if(jsonObject.get("coexistence") != null){
			flag=true;
			byte coexistence=(byte)(long)jsonObject.get("coexistence");
			if(coexistence != advanceInfo.getCoexistence()){
				advanceInfo.setCoexistence(coexistence);
				 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			}
		}
		if(jsonObject.get("maxMPDU") != null){
			flag=true;
			byte MaxMPDU=(byte)(long)jsonObject.get("maxMPDU");
			if(MaxMPDU != advanceInfo.getMaxMPDU()){
				advanceInfo.setMaxMPDU(MaxMPDU);
				 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			}

		}
		if(jsonObject.get("maxAMPDUEnabled") != null){
			flag=true;
			byte maxAMPDUEnabled=(byte)(long)jsonObject.get("maxAMPDUEnabled");
			if(maxAMPDUEnabled != advanceInfo.getMaxAMPDUEnabled()){
				advanceInfo.setMaxAMPDUEnabled(maxAMPDUEnabled);
				 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			}

		}
		return flag;
		
	}
}
