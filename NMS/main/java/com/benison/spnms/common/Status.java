package com.benison.spnms.common;

public class Status {
	private int code;
	private String message;
	private String rebootRequired;
	private Object responseData;

	public Status() {
	}

	public Status(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResponseData() {
		return responseData;
	}

	public void setResponseData(Object responseData) {
		this.responseData = responseData;
	}

	public String getRebootRequired() {
		return rebootRequired;
	}

	public void setRebootRequired(String rebootRequired) {
		this.rebootRequired = rebootRequired;
	}
	
	

}
