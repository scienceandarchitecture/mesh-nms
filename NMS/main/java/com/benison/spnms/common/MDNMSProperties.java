package com.benison.spnms.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MDNMSProperties {
	public static String getValue(String attribute) throws IOException {
		String result = "";
		try {
			InputStream in = MDNMSProperties.class.getResourceAsStream("/MDProperties.properties");
			Properties properties = new Properties();
			properties.load(in);
			result = properties.getProperty(attribute);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

}
