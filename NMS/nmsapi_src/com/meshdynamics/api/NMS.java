/*
 * Created Date: Jul 10, 2008
 * Author : Sri
 */
package com.meshdynamics.api;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

import com.meshdynamics.api.impl.NMSFactory;
import com.meshdynamics.api.impl.ObjectNotationParser;

/**
 * NMS is the primary class for using the <b>Meshdynamics Network Management
 * System (NMS) API</b>.
 * <p>
 * It is a singleton class defining classes, interfaces and constants to be used
 * for accessing the NMS information<br>
 * <p>
 * All clients of the NMS API need to obtain a reference to the singleton
 * instance of the NMS object by calling the <code>NMS.getInstance()</code>
 * method.
 *  
 */

public abstract class NMS {
    
    /**
     * Defines all Node level fields used by a {@link NMS.Node}.
     * 
     * @see NMS.Node#getGeneralConfiguration
     * @see NMS.Node#setGeneralConfiguration
     */

    public static class GeneralConfiguration {

        /**
         * User-defined name of the node
         */

        public String nodeName;

        /**
         * User-defined description for the node
         */

        public String nodeDescription;
        
        /**
         * The model identifier for the node.
         * <p>
         * NOTE: This field is read-only and will be ignored in calls to {@link NMS.Node#setGeneralConfiguration}.
         */
        
        public String model;
        
        /**
         * Latitude coordinate of the node in decimal format.
         * <p>
         * Coordinates South of the equator are represented by a negative number
         */

        public String gpsLatitude;

        /**
         * Longitude coordinate of the node in decimal format.
         * <p>
         * Coordinates West of the meridian are represented by a negative number
         */

        public String gpsLongitude;

        /**
         * The network host-name for the node.
         */

        public String hostName;

        /**
         * The ip-address for the node in dotted decimal form.
         */

        public String ipAddress;

        /**
         * The subnet-mask for the node in dotted decimal form.
         */

        public String subnetMask;

        /**
         * The ip-address of the default gateway in dotted decimal form.
         */

        public String gatewayIpAddress;

        /**
         * The MAC address of the preferred parent's downlink radio.
         */

        public String preferredParent;

        /**
         * The heartbeat interval for the node.
         */

        public short heartbeatInterval;

        /**
         * The node's mobilty mode. 
         * <p>
         * A non-zero value indicates that the node is configured for mobility.
         */

        public short mobilityMode;

        /**
         * The combination of run-time options enabled on the node.
         * 
         * @see NMS#OPTION_ADHOC
         * @see NMS#OPTION_ADHOC_DHCP
         * @see NMS#OPTION_ADHOC_INFRA_BEGIN
         * @see NMS#OPTION_ADHOC_SECTORED
         * @see NMS#OPTION_FORCED_ROOT
         * @see NMS#OPTION_IGMP
         * @see NMS#OPTION_LOCATION
         * @see NMS#OPTION_SIP
         */

        public short options;

        /**
         * The node level flag determining whether downlink interfaces shall use
         * the Dynamic Channel Allocation scheme.
         * <p>
         * A value of 0 will turn off the dynamic channel allocation scheme even
         * if it is turned on for individual downlink interfaces.
         */

        public short dynamicChannelAllocation;
        
        /**
         * The operating country code for the node.
         * <p>
         * A value of 0 indicates the default country code. 
         */
        
        public int countryCode;
        
        /**
         * The operating regulatory domain for the node.
         * 
         * @see NMS#REG_DOMAIN_CODE_NONE
         * @see NMS#REG_DOMAIN_CODE_CUSTOM
         * @see NMS#REG_DOMAIN_CODE_FCC
         * @see NMS#REG_DOMAIN_CODE_ETSI
         */
        
        public int regulatoryDomain;
        
        /**
         * Specifies whether Dynamics Frequency Selection and RADAR detection
         * is required for the {@code regulatoryDomain}.
         */
        
        public short dfsRequired;
    }

    /**
     * Defines the information used by the IEEE 802.11 <b>Wired Equivalent
     * Privacy</b> (WEP) setting by a Node's downlink interface.
     * 
     * @see NMS.InterfaceConfiguration#securityType
     * @see NMS.InterfaceConfiguration#securityInfo
     */

    public static class WEPSecurity {

        /**
         * Default constructor.
         */
        
        public WEPSecurity() {            
            keyIndex    = 0;
            wepKeys     = new ObjectArray();
        }
        
        /**
         * Package protected constructor to create from a parsed {@code ObjectNotationParser.Object}
         * 
         * @param notationObject the parsed {@code ObjectNotationParser.Object}
         */
        
        WEPSecurity(ObjectNotationParser.Object notationObject) {
            
            /**
             * The primary use of this constructor is to lessen the
             * amount of code written in scripts
             */            
            
            keyIndex    = 0;
            wepKeys     = new ObjectArray();
            
            try {
                
                String[]    keys;
                
                keyIndex    = ((Number)notationObject.getValue("keyIndex")).shortValue();
                keys        = (String[])notationObject.getValue("wepKeys");
                
                for(int i = 0; i < keys.length; i++)
                    wepKeys.add(keys[i]);
                
            } catch(Exception e) {
                
                /**
                 * Silently ignore 
                 */
                
            }
            
        }
        
        @Override
        public String toString() {
            return toObjectNotation();
        }
        
        /**
         * Returns a string containing the object notation representation
         * of the {@code WEPSecurity} object
         * 
         * @return the object notation string
         */
        
        public String toObjectNotation() {
            
            ByteArrayOutputStream   byteArrayStream;
            PrintStream             printStream;
            
            byteArrayStream = new ByteArrayOutputStream();
            printStream     = new PrintStream(byteArrayStream);
            
            printStream.printf("{ keyIndex : %d, wepKeys : %s }",
                               keyIndex,
                               wepKeys.toObjectNotation());
                        
            return byteArrayStream.toString();
        }

        /**
         * The index of the key used for transmitting packets.
         * <p>
         * For WEP-40 the valid values are 0-3.
         * <p>
         * For WEP-104 the value is ignored.
         */

        public short        keyIndex;
        
        /**
         * An array of upto 4 WEP keys formatted as hexadecimal strings.
         * <p>
         * When using WEP-40 the array shall contain 4 entries of 10 hexadecimal
         * digits.
         * <p>
         * For WEP-104 the array shall contain 1 entry of 26 hexadecimal digits
         */        
        
        public ObjectArray  wepKeys;

    }

    /**
     * Defines the information used for the Wifi Protected Access (WPA) security
     * setting by a node's downlink interface.
     * 
     * @see NMS.InterfaceConfiguration#securityType
     * @see NMS.InterfaceConfiguration#securityInfo 
     */

    public static class WPAPersonalSecurity {
        
        /**
         * Default constructor
         */
        
        public WPAPersonalSecurity() {
            preSharedKey    = "";
            cipherType      = NMS.CIPHER_CCMP;
        }
        
        @Override
        public String toString() {
            return toObjectNotation();
        }
        
        /**
         * Returns a string containing the object notation representation
         * of the {@code WPAPersonalSecurity} object
         * 
         * @return the object notation string
         */
        
        public String toObjectNotation() {
            
            ByteArrayOutputStream   byteArrayStream;
            PrintStream             printStream;
            
            byteArrayStream = new ByteArrayOutputStream();
            printStream     = new PrintStream(byteArrayStream);
            
            printStream.printf("{ preSharedKey : \"%s\", cipherType : %d }",
                               preSharedKey,
                               cipherType);
                        
            return byteArrayStream.toString();
        }        

        /**
         * Package protected constructor used to initialize
         * from a {@code ObjectNotationParser.Object} 
         */
        
        WPAPersonalSecurity(ObjectNotationParser.Object notationObject) {
            
            /**
             * The primary use of this constructor is to lessen the
             * amount of code written in scripts
             */
            
            preSharedKey    = "";
            cipherType      = NMS.CIPHER_CCMP;              
                        
            try {                                
                
                preSharedKey    = (String)notationObject.getValue("preSharedKey");
                
                if(preSharedKey == null) {
                    preSharedKey    = "";
                }                
                
                cipherType      = ((Number)notationObject.getValue("cipherType")).shortValue();
                
            } catch(Exception e) {   
                
                /**
                 * Silently ignore exceptions
                 */
              
            }
        }
        
        /**
         * The 256-bit pre-shared key (PSK) formatted as a hexidecimal string.
         * <p>
         * The string shall consist of 64 hexadecimal digits.
         */

        public String preSharedKey;

        /**
         * Defines the encryption mechanism to be used.
         * 
         * @see NMS#CIPHER_CCMP
         * @see NMS#CIPHER_TKIP
         */

        public short cipherType;
    }

    /**
     * Defines the information used for the Wifi Protected Access security
     * setting by a Node's downlink interface in an enterprise environment.
     * 
     * @see NMS.InterfaceConfiguration#securityType
     * @see NMS.InterfaceConfiguration#securityInfo
     */

    public static class WPAEnterpriseSecurity {
        
        /**
         * Default constructor
         */
        
        public WPAEnterpriseSecurity() {
            radiusServerIp      = "0.0.0.0";
            radiusServerPort    = 1812;
            radiusServerSecret  = "";
            cipherType          = NMS.CIPHER_CCMP;
        }
        
        @Override
        public String toString() {
            return toObjectNotation();
        }
        
        /**
         * Returns a string containing the object notation representation
         * of the {@code WPAEnterpriseSecurity} object.
         * 
         * @return the object notation string
         */
        
        public String toObjectNotation() {
            
            ByteArrayOutputStream   byteArrayStream;
            PrintStream             printStream;
            
            byteArrayStream = new ByteArrayOutputStream();
            printStream     = new PrintStream(byteArrayStream);
            
            printStream.printf("{ radiusServerIp : %s, radiusServerPort : %d\n"
                              + "radiusServerSecret : \"%s\", cipherType : %d }",
                              radiusServerIp,radiusServerPort,
                              radiusServerSecret, cipherType);
                        
            return byteArrayStream.toString();
        }         
        
        /**
         * Package protected constructor used to intialize from
         * a {@code ObjectNotationParser.Object}
         */
        
        WPAEnterpriseSecurity(ObjectNotationParser.Object notationObject) {
            
            /**
             * The primary use of this constructor is to lessen the
             * amount of code written in scripts
             */            
            
            radiusServerIp      = "0.0.0.0";
            radiusServerPort    = 1812;
            radiusServerSecret  = "";
            cipherType          = NMS.CIPHER_CCMP;            
            
            try {
                
                radiusServerIp    = (String)notationObject.getValue("radiusServerIp");
                
                if(radiusServerIp == null) {
                    radiusServerIp    = "0.0.0.0";
                }                    
                
                radiusServerSecret    = (String)notationObject.getValue("radiusServerSecret");
                
                if(radiusServerSecret == null) {
                    radiusServerSecret    = "";
                }     
                
                cipherType          = ((Number)notationObject.getValue("cipherType")).shortValue();                
                radiusServerPort    = ((Number)notationObject.getValue("radiusServerPort")).shortValue();
                
                
            } catch(Exception e) {
                
                /**
                 * Silently ignore exceptions
                 */
                
            }
            
        }

        /**
         * IP-address of the RADIUS server
         */

        public String radiusServerIp;

        /**
         * The UDP port used by the RADIUS server
         */

        public short radiusServerPort;

        /**
         * The secret key used to authenticate RADIUS packets sent by the node
         */

        public String radiusServerSecret;

        /**
         * Defines the encryption mechanism to be used.
         * 
         * @see NMS#CIPHER_CCMP
         * @see NMS#CIPHER_TKIP
         */

        public short cipherType;
    }
 public static class InterfaceAdvanceInfo {
        
        /**
         * Default constructor.
         */
        
        public InterfaceAdvanceInfo() {
             internalInitializeDefaults();
             }
        public InterfaceAdvanceInfo(ObjectNotationParser.Object notationObject) {
        	 /**
             * The primary use of this constructor is to minimize
             * the amount of code in scripts
             */                                
               super();            
            if(notationObject == null)
                return;
            
            try {    
                internalSetChannelBandwidth((Number)notationObject.getValue("channelBandwidth"));
                internalSetSecondaryChannelPosition((Number)notationObject.getValue("secondaryChannelPosition"));
                internalSetGuardInterval_20((Number)notationObject.getValue("guardInterval_20"));
                internalSetGuardInterval_40((Number)notationObject.getValue("guardInterval_40"));
                internalSetGuardInterval_80((Number)notationObject.getValue("guardInterval_80"));
                internalSetMaxAMPDUEnabled((Number)notationObject.getValue("maxAMPDUEnabled"));
                internalSetMaxAMPDU((Number)notationObject.getValue("MaxAMPDU"));
                internalSetMaxAMSDU((Number)notationObject.getValue("MaxAMSDU"));
                internalSetTxSTBC((Number)notationObject.getValue("txSTBC"));
                internalSetrxSTBC((Number)notationObject.getValue("rxSTBC"));
                internalSetLDPC((Number)notationObject.getValue("ldpc"));
                internalSetGfMode((Number)notationObject.getValue("gfMode"));
                internalSetCoexistence((Number)notationObject.getValue("coexistence"));
                internalSetMaxMPDU((Number)notationObject.getValue("MaxMPDU"));
                                          
            } catch(Exception e) {
                
                /**
                 * Silently ignore exceptions.
                 */
                e.printStackTrace();
            }            
        }		
		private void internalSetChannelBandwidth(Number value) {
		if(value != null)	
			channelBandwidth=value.byteValue();
		}
		private void internalSetSecondaryChannelPosition(Number value) {
			if(value != null)	
				secondaryChannelPosition=value.byteValue();
			
		}
		/*private void internalSetPreamble(Number value) {
			if(value != null)	
				preamble=value.byteValue();
			
		}*/
		private void internalSetGuardInterval_20(Number value) {
			if(value != null)	
				guardInterval_20=value.byteValue();
			
		}
		private void internalSetGuardInterval_40(Number value) {
			if(value != null)	
				guardInterval_40=value.byteValue();			
		}
		private void internalSetGuardInterval_80(Number value) {
			if(value != null)	
				guardInterval_80=value.byteValue();			
		}
		private void internalSetMaxAMPDUEnabled(Number value) {
			if(value != null)	
				maxAMPDUEnabled=value.byteValue();	
			
		}
		private void internalSetMaxAMPDU(Number value) {
			if(value != null)	
				MaxAMPDU=value.byteValue();	
			
		}
		private void internalSetMaxAMSDU(Number value) {
			if(value != null)	
				MaxAMSDU=value.byteValue();	
			
		}
		private void internalSetTxSTBC(Number value) {
			if(value != null)	
				txSTBC=value.byteValue();			
		}
		private void internalSetrxSTBC(Number value) {
			if(value != null)	
				rxSTBC=value.byteValue();
			
		}
		private void internalSetLDPC(Number value) {
			if(value != null)	
				ldpc=value.byteValue();	
			
		}
		private void internalSetGfMode(Number value) {
			if(value != null)	
				gfMode=value.byteValue();	
			
		}
		private void internalSetCoexistence(Number value) {
			if(value != null)	
				coexistence=value.byteValue();				
		}
		private void internalSetMaxMPDU(Number value) {
			if(value != null)	
				MaxMPDU=value.shortValue();	
		}
		private void internalInitializeDefaults() {
			
			channelBandwidth         = 1;
			secondaryChannelPosition = 1;
			guardInterval_20         = 2;
			guardInterval_40         = 2;
			guardInterval_80         = 2;
			maxAMPDUEnabled          = 1;
			MaxAMPDU                 = 32;
			MaxAMSDU                 = 0;
			txSTBC                   = 1;
			rxSTBC                   = 0;
			ldpc                     = 0;
			gfMode                   = 0;
			coexistence              = 0;
			MaxMPDU                  = 3895;
		}
		 @Override
	        public String toString() {            
	            return toObjectNotation();
	        }
		private String toObjectNotation() {
			    ByteArrayOutputStream   byteArrayStream;
	            PrintStream             printStream;
	            
	            byteArrayStream = new ByteArrayOutputStream();
	            printStream     = new PrintStream(byteArrayStream);
	            printStream.printf("{channelBandwidth : %d, secondaryChannelPosition : %d,\n"
                        + "guardInterval_20 : %d, guardInterval_40 : %d,guardInterval_80 : %d, frameAggregation : %d,\n"
                        + "MaxAMPDU : %d, MaxAMSDU : %d, txSTBC : %d,\n"
                        + "rxSTBC : %d, ldpc : %d,gfMode : %d,coexistence : %d, MaxMPDU : %d,\n"
                        + "maxAMPDUEnabled : %d}",                              
                        channelBandwidth, secondaryChannelPosition,guardInterval_20, guardInterval_40, guardInterval_80,
                        frameAggregation,MaxAMPDU, MaxAMSDU, txSTBC,
                        rxSTBC, ldpc, gfMode,coexistence, MaxMPDU, maxAMPDUEnabled);
			return      byteArrayStream.toString();
		}
		public byte   channelBandwidth;
		public byte   secondaryChannelPosition;
		public byte   guardInterval_20;
		public byte   guardInterval_40;
		public byte   guardInterval_80;
		public byte   frameAggregation;
		public byte   MaxAMPDU;
		public byte   MaxAMSDU;
		public byte   txSTBC;
		public byte   rxSTBC;
		public byte   ldpc;
		public byte   gfMode;
		public byte   coexistence;
		public short  MaxMPDU;
		public byte   maxAMPDUEnabled;
 }
    /**
     * Defines the interface level settings for a {@link NMS.Node}.
     * 
     * @see NMS.Node#getInterfaces
     * @see NMS.Node#getInterfaceConfigurationByName
     */

    public static class InterfaceConfiguration {
        
        /**
         * Default constructor.
         */
        
        public InterfaceConfiguration() {
            internalInitializeDefaults();
        }
        
        /**
         * Initializes the configuration from the object notation string.
         * 
         * @param objectNotation the object notation string
         */        
        
        public InterfaceConfiguration(String objectNotation) {
            
            /**
             * The primary use of this constructor is to minimize
             * the amount of code in scripts
             */
            
            ObjectNotationParser.Object parsedObject;
            
            internalInitializeDefaults();
            
            parsedObject = ObjectNotationParser.parse(objectNotation);
            
            if(parsedObject == null)
                return;
            
            try {            
                
                internalSetPhySubType((Number)parsedObject.getValue("phySubType"));
                internalSetUsageType((Number)parsedObject.getValue("usageType"));
                internalSetIdentifier((Number)parsedObject.getValue("identifier"));
                internalSetEssid((String)parsedObject.getValue("essid"));
                internalSetHideEssid((Number)parsedObject.getValue("hideEssid"));
                internalSetMaximumTransmitRate((Number)parsedObject.getValue("maxTransmitRate"));
                internalSetTransmitPower((Number)parsedObject.getValue("transmitPower"));
                internalSetAckTimeout((Number)parsedObject.getValue("ackTimeout"));
                internalSetAllowClientConnection((Number)parsedObject.getValue("allowClientConnection"));
                internalSetFragThreshold((Number)parsedObject.getValue("fragThreshold"));
                internalSetRtsThreshold((Number)parsedObject.getValue("rtsThreshold"));
                internalSetDynamicChannelAllocation((Number)parsedObject.getValue("dynamicChannelAllocation"));
                internalSetManualChannel((Number)parsedObject.getValue("manualChannel"));
                internalSetDcaList(parsedObject.getValue("dcaList"));
                internalSetSecurityType((Number)parsedObject.getValue("securityType"));
                internalSetSecurityInfo(parsedObject.getValue("securityInfo"));
                internalSetAdvanceInfo(parsedObject.getValue("advanceInfo"));                                
            } catch(Exception e) {
                
                /**
                 * Silently ignore exceptions.
                 */
                
            }            
            
        }
        
        private void internalSetAdvanceInfo(Object value) {
			if(NMS.PHY_SUB_TYPE_802_11_N ==phySubType || 
					NMS.PHY_SUB_TYPE_802_11_AC ==phySubType ||
					NMS.PHY_SUB_TYPE_802_11_AN ==phySubType ||
					NMS.PHY_SUB_TYPE_802_11_ANAC ==phySubType ||
					NMS.PHY_SUB_TYPE_802_11_BGN ==phySubType
					){
				   advanceInfo=new InterfaceAdvanceInfo((ObjectNotationParser.Object)value);
			}else{
				advanceInfo=null;
			}			
		}

		@Override
        public String toString() {            
            return toObjectNotation();
        }
        
        /**
         * Returns a string containing the object notation representation 
         * for the interface.
         * 
         * @return string containing object notation representation of the interface
         */
        
        public String toObjectNotation() {
            
            ByteArrayOutputStream   byteArrayStream;
            PrintStream             printStream;
            
            byteArrayStream = new ByteArrayOutputStream();
            printStream     = new PrintStream(byteArrayStream);
            
            printStream.printf("{phySubType : %d, usageType : %d, essid : \"%s\",\n"
                              + "ackTimeout : %d, hideEssid : %d, maxTransmitRate : %d,\n"
                              + "transmitPower : %d, allowClientConnection : %d, fragThreshold : %d,\n"
                              + "rtsThreshold : %d, dynamicChannelAllocation : %d, manualChannel : %d,\n"
                              + "dcaList : %s, securityType : %d, identifier : %d\n",                              
                               phySubType, usageType, essid,
                               ackTimeout, hideEssid, maxTransmitRate, 
                               transmitPower, allowClientConnection, fragThreshold,
                               rtsThreshold, dynamicChannelAllocation, manualChannel,
                               dcaList.toString(), securityType, identifier);
            
            if(securityInfo != null) {
                printStream.printf(",securityInfo : %s}",securityInfo.toString());                
            }/* else {
                printStream.printf("}");
            }*/
              if(advanceInfo != null) {
                printStream.printf(",advanceInfo : %s}",advanceInfo.toString());                
            } else {
                printStream.printf("}");
            }
            return byteArrayStream.toString();            
            
        } 
        
        private void internalSetIdentifier(Number value) {
            if(value != null)
                identifier = value.shortValue();
        }        
                
        private void internalSetPhySubType(Number value) {
            if(value != null)
                phySubType = value.shortValue();
        }
        
        private void internalSetUsageType(Number value) {
            if(value != null)
                usageType = value.shortValue();
        }
     
        private void internalSetEssid(String value) {
            if(value != null)
                essid = value;
        }   
        
        private void internalSetHideEssid(Number value) {
            if(value != null)
                hideEssid = value.shortValue();
        }    
        
        private void internalSetMaximumTransmitRate(Number value) {
            if(value != null)
                maxTransmitRate = value.shortValue();
        }   
        
        private void internalSetTransmitPower(Number value) {
            if(value != null)
                transmitPower = value.shortValue();
        } 
        
        private void internalSetAckTimeout(Number value) {
            if(value != null)
                ackTimeout = value.shortValue();
        }  
        
        private void internalSetAllowClientConnection(Number value) {
            if(value != null)
                allowClientConnection = value.shortValue();
        }    
        
        private void internalSetFragThreshold(Number value) {
            if(value != null)
                fragThreshold = value.shortValue();
        }  
        
        private void internalSetRtsThreshold(Number value) {
            if(value != null)
                rtsThreshold = value.shortValue();
        }   
        
        private void internalSetDynamicChannelAllocation(Number value) {
            if(value != null)
                dynamicChannelAllocation = value.shortValue();
        }  
        
        private void internalSetManualChannel(Number value) {
            if(value != null)
                manualChannel = value.shortValue();
            else {
                switch(phySubType) {
                case NMS.PHY_SUB_TYPE_802_11_AN:
                case NMS.PHY_SUB_TYPE_802_11_AC:
                case NMS.PHY_SUB_TYPE_802_11_A:
                case NMS.PHY_SUB_TYPE_802_11_ANAC:
                     manualChannel   = 52;
                     break;
                case NMS.PHY_SUB_TYPE_802_11_B:
                case NMS.PHY_SUB_TYPE_802_11_BG:
                case NMS.PHY_SUB_TYPE_802_11_G:
                case NMS.PHY_SUB_TYPE_802_11_BGN:
                    manualChannel   = 6;
                    break;
                case NMS.PHY_SUB_TYPE_802_11_PSQ:
                    manualChannel   = 5;
                    break;
                case NMS.PHY_SUB_TYPE_802_11_PSH:
                    manualChannel   = 10;
                    break;
                case NMS.PHY_SUB_TYPE_802_11_PSF:
                    manualChannel   = 20;
                    break;
                }
            }
        }    
        
        private void internalSetDcaList(Object value) {
            if(value != null) {
                dcaList = new ShortArray((Number [])value);
            } else {
                if(usageType == NMS.USAGE_TYPE_DOWNLINK
                || usageType == NMS.USAGE_TYPE_SCANNER) {
                    switch(phySubType) {
                    case NMS.PHY_SUB_TYPE_802_11_A:
                        dcaList = new ShortArray((short)52,(short)60,(short)149,(short)157,(short)165);
                        break;
                    case NMS.PHY_SUB_TYPE_802_11_B:
                    case NMS.PHY_SUB_TYPE_802_11_BG:
                    case NMS.PHY_SUB_TYPE_802_11_G:
                        dcaList = new ShortArray((short)1,(short)6,(short)11);
                        break;
                    case NMS.PHY_SUB_TYPE_802_11_PSQ:
                        dcaList = new ShortArray((short)5,(short)25,(short)45,(short)65, (short)85);
                        break;
                    case NMS.PHY_SUB_TYPE_802_11_PSH:
                        dcaList = new ShortArray((short)10,(short)40,(short)70);
                        break;
                    case NMS.PHY_SUB_TYPE_802_11_PSF:
                        dcaList = new ShortArray((short)20,(short)80);
                        break;
                    }
                } else {
                    dcaList = new ShortArray(0);
                }
            }
        }
        
        private void internalSetSecurityType(Number value) {
            if(value != null)
                securityType = value.shortValue();            
        }
        
        private void internalSetSecurityInfo(Object value) {
            
            if(value != null) {                
                switch(securityType) {
                case NMS.SECURITY_TYPE_WEP_40:
                case NMS.SECURITY_TYPE_WEP_104:
                    securityInfo = new WEPSecurity((ObjectNotationParser.Object)value);
                    break;
                case NMS.SECURITY_TYPE_WPA_PERSONAL:
                case NMS.SECURITY_TYPE_WPA2_PERSONAL:
                    securityInfo = new WPAPersonalSecurity((ObjectNotationParser.Object)value);
                    break;
                case NMS.SECURITY_TYPE_WPA_ENTERPRISE:
                case NMS.SECURITY_TYPE_WPA2_ENTERPRISE:
                    securityInfo = new WPAEnterpriseSecurity((ObjectNotationParser.Object)value);
                    break;
                default:
                    securityType    = NMS.SECURITY_TYPE_NONE;
                    securityInfo    = null;                        
                }                                
                
            } else {
                securityType    = NMS.SECURITY_TYPE_NONE;
                securityInfo    = null;
            }
            
        }
        
        private void internalInitializeDefaults() {
            name                        = "wlanX";
            macAddress                  = "00:00:00:00:00:00";
            identifier                  = -1;
            usageType                   = NMS.USAGE_TYPE_UPLINK;
            phyType                     = NMS.PHY_TYPE_ETHERNET;
            phySubType                  = NMS.PHY_SUB_TYPE_IGNORE;
            essid                       = "";
            hideEssid                   = 0;
            maxTransmitRate             = 0;
            transmitPower               = 100;
            ackTimeout                  = 50;
            allowClientConnection       = 1;
            fragThreshold               = 2346;
            rtsThreshold                = 2347;
            dynamicChannelAllocation    = 0;
            manualChannel               = 0;
            dcaList                     = new ShortArray(0);
            securityType                = NMS.SECURITY_TYPE_NONE;
            securityInfo                = null;
            operatingChannel            = 0;   
            advanceInfo                 = null;
        }                
        


        /**
         * The name of the interface.
         */

        public String name;

        /**
         * The MAC address of the interface.
         * <p>
         * NOTE: The value of this field will be ignored in calls to {@link NMS.Node#setInterfaceConfiguration}.
         */

        public String macAddress;
        
        /**
         * The identifier for the interface.
         * 
         * <p>
         * The interfaces of a node are identified according to the
         * {@code usageType} and {@code phySubType} fields.
         * <p>
         * e.g. For a node with two 802.11a downlinks and a 802.11g downlink,
         * the first downlink shall have an identifier of 0, while the 2nd will have 1.
         * <p>
         * The 802.11g downlink will have an identifier of 0.
         * <p>
         * NOTE: The value of this field will be ignored in calls to {@link NMS.Node#setInterfaceConfiguration}.
         */
        
        public short  identifier;
        
        /**
         * Defines the role in which the interface is used during the node's
         * operation.
         * <p>
         * NOTE: The value of this field will be ignored in calls to {@link NMS.Node#setInterfaceConfiguration}.
         * 
         * @see NMS#USAGE_TYPE_DOWNLINK
         * @see NMS#USAGE_TYPE_UPLINK
         * @see NMS#USAGE_TYPE_SCANNER
         */

        public short usageType;

        /**
         * Defines the Physical layer used by the interface.
         * <p>
         * NOTE: The value of this field will be ignored in calls to {@link NMS.Node#setInterfaceConfiguration}.
         * @see NMS#PHY_TYPE_ETHERNET
         * @see NMS#PHY_TYPE_802_11
         */

        public short phyType;

        /**
         * Defines the physical layer sub-type used by the interface.
         * 
         * @see NMS#PHY_SUB_TYPE_IGNORE
         * @see NMS#PHY_SUB_TYPE_802_11_A
         * @see NMS#PHY_SUB_TYPE_802_11_B
         * @see NMS#PHY_SUB_TYPE_802_11_G
         * @see NMS#PHY_SUB_TYPE_802_11_BG
         * @see NMS#PHY_SUB_TYPE_802_11_PSQ
         * @see NMS#PHY_SUB_TYPE_802_11_PSH
         * @see NMS#PHY_SUB_TYPE_802_11_PSF
         */

        public short phySubType;

        /**
         * The ESSID used in 802.11 beacons and 802.11 probe-response packets
         * transmitted by the downlink interface.
         * <p>
         * This field is ignored for 802.11 uplink, scanner interfaces.
         * <p>
         * For ETHERNET downlinks, this field specifies the VLAN configuration
         * for the ethernet port :
         * <p>
         * <ul>
         *      <li>ESSID of a VLAN - only allows the specified VLAN</li>
         *      <li>MD-PRIV-SSID-NO-VLAN - No VLANs allowed.</li>
         *      <li>Other - All VLANs allowed</li>
         * </ul>
         */

        public String essid;

        /**
         * When non-zero causes the ESSID field of 802.11 beacons and broadcast
         * probe-responses to contain an empty string.
         * <p>
         * This field is ignored for 802.11 uplink, scanner interfaces and by
         * all ethernet interfaces.
         */

        public short hideEssid;

        /**
         * The maximum transmit rate for the interface.
         * <p>
         * When set to {@code 0}, the interface uses all the transmit rates defined by
         * the physical layer sub-type.
         * <p>
         * This field is ignored for ethernet interfaces.
         */

        public int maxTransmitRate;

        /**
         * The transmit power for the interface.
         * <p>
         * This field is ignored for ethernet interfaces.
         */

        public int transmitPower;

        /**
         * The timeout in multiples of {@code 10} micro-seconds for the 802.11 ACK frame. 
         * <p>
         * Transmissions with the ACK frame not arriving within the ackTimeout value are
         * considered erroneous and are retried.
         * <p>
         * This field is ignored for ethernet interfaces.
         */

        public int ackTimeout;

        /**
         * When set to {@code 0}, does not allow standard 802.11 clients to connect to
         * the interface, and only allows child mesh nodes to connect to the
         * interface.
         * <p>
         * This field is ignored for ethernet interfaces.
         */

        public short allowClientConnection;

        /**
         * The 802.11 fragmentation threshold for the interface. 
         * <p>
         * All packets larger than the fragThreshold shall be fragmented.
         * <p>
         * This field is ignored for ethernet interfaces.
         */

        public int fragThreshold;

        /**
         * The 802.11 RTS threshold for the interface. 
         * <p>
         * All packets larger than the rtsThreshold shall be preceeded 
         * by the standard 802.11 RTS/CTS mechanism to ensure error free reception.
         * <p>
         * This field is ignored for ethernet interfaces.
         */

        public int rtsThreshold;

        /**
         * When set to {@code 0}, disables dynamic channel allocation and forces the
         * interface to use the channel specified by {@code manualChannel}.
         * <p>
         * When set to a non-zero value, the interface chooses the best channel
         * from the {@code dcaList} for operation.
         * <p>
         * This field is ignored for 802.11 uplink scanner and ethernet
         * interfaces.
         * 
         * @see #dcaList
         * @see #manualChannel
         */

        public short dynamicChannelAllocation;

        /**
         * The channel to be used when {@code dynamicChannelAllocation} is set
         * to 0.
         * <p>
         * This field is ignored for 802.11 uplink scanner and ethernet
         * interfaces.
         * 
         * @see #dynamicChannelAllocation
         */

        public short manualChannel;

        /**
         * When {@code dynamicChannelAllocation} is non-zero, downlink
         * interfaces choose the best channel from the integers specified in
         * this array.
         * <p>
         * For uplink interfaces, if the list is empty, all channels shall be
         * scanned. If the list is non-empty only the channels specified in the
         * list will be scanned for parent selection.
         * <p>
         * NOTE: The list must not be empty for uplink interfaces if the node is
         * in disjoint-adhoc mode.
         * <p>
         * For scanner interfaces, the list determines the channels that will be
         * scanned for detecting prospective parent nodes. <br>
         * <p> 
         * This field is ignored for ethernet interfaces.
         * 
         * @see #dynamicChannelAllocation
         */

        public ShortArray   dcaList;

        /**
         * The encryption/authentication scheme used to secure connections on
         * the interface. 
         * <p>
         * This field is ignored for 802.11 uplink scanner and
         * ethernet interfaces.
         * 
         * @see NMS#SECURITY_TYPE_NONE
         * @see NMS#SECURITY_TYPE_WEP_104
         * @see NMS#SECURITY_TYPE_WEP_40
         * @see NMS#SECURITY_TYPE_WPA2_ENTERPRISE
         * @see NMS#SECURITY_TYPE_WPA2_PERSONAL
         * @see NMS#SECURITY_TYPE_WPA_ENTERPRISE
         * @see NMS#SECURITY_TYPE_WPA_PERSONAL
         */

        public short securityType;

        /**
         * Opaque object containing the security settings for the interface.
         * <p>
         * The field represents a {@code NMS.WEPSecurity} object if {@code securityType}
         * is {@code NMS.SECURITY_TYPE_WEP_104} or {@code NMS.SECURITY_TYPE_WEP_40}.
         * <p>
         * The field represents a {@code NMS.WPAPersonalSecurity} object if {@code securityType}
         * is {@code NMS.SECURITY_TYPE_WPA2_PERSONAL} or {@code NMS.SECURITY_TYPE_WPA_PERSONAL}.
         * <p>
         * The field represents a {@code NMS.WPAEnterpriseSecurity} object if {@code securityType}
         * is {@code NMS.SECURITY_TYPE_WPA2_ENTERPRISE} or {@code NMS.SECURITY_TYPE_WPA_ENTERPRISE}.
        * <p>
         * This field is ignored for 802.11 uplink scanner and ethernet
         * interfaces.
         * 
         * @see #securityType
         * @see NMS.WEPSecurity
         * @see NMS.WPAPersonalSecurity
         * @see NMS.WPAEnterpriseSecurity
         */

        public Object securityInfo;

        /**
         * The current operating channel for the interface.
         */

        public short operatingChannel;
        /***
         * HT & VHT Info
         */
        public Object advanceInfo;
    }
    
    
    /**
     * Defines the settings for a Virtual-LAN in a {@link NMS.Node}.
     */
    
    public static class VlanConfiguration {
        
        /**
         * Default constructor.
         */
        
        public VlanConfiguration() {
            
            internalInitializeDefaults();
            
        }
        
        /**
         * Creates a VlanConfiguration object from a object notation string.
         * 
         * @param objectNotation the object notation string
         */
        
        public VlanConfiguration(String objectNotation) {
            
            /**
             * The primary use of this constructor is to minimize
             * the amount of code in scripts
             */
            
            ObjectNotationParser.Object parsedObject;
            
            internalInitializeDefaults();
            
            parsedObject = ObjectNotationParser.parse(objectNotation);
            
            if(parsedObject == null)
                return;
            
            try {            
                
                internalSetTag((Number)parsedObject.getValue("tag"));
                internalSetName((String)parsedObject.getValue("name"));
                internalSetEssid((String)parsedObject.getValue("essid"));                
                internalSetDot11eEnabled((Number)parsedObject.getValue("dot11eEnabled"));
                internalSetDot11eCategory((Number)parsedObject.getValue("dot11eCategory"));
                internalSetDot1pPriority((Number)parsedObject.getValue("dot1pPriority"));
                internalSetSecurityType((Number)parsedObject.getValue("securityType"));
                internalSetSecurityInfo(parsedObject.getValue("securityInfo"));    
                                                
            } catch(Exception e) {
                
                /**
                 * Silently ignore exceptions.
                 */
                
            }                        
        }
     	@Override
        public String toString() {
            return toObjectNotation();
        }
        
        /**
         * Returns a string containing the object notation representation
         * of the VlanConfiguration object.
         * 
         * @return the object notation string
         */
        
        public String toObjectNotation() {
            
            ByteArrayOutputStream   byteArrayStream;
            PrintStream             printStream;
            
            byteArrayStream = new ByteArrayOutputStream();
            printStream     = new PrintStream(byteArrayStream);
                        
            printStream.printf( "{ tag : %d, name : \"%s\", essid : \"%s\","
                               + " dot11eEnabled : %d, dot11eCategory : %d, dot1pPriority: %d,\n"
                               + "securityType : %d\n",
                               tag, name, essid, 
                               dot11eEnabled, dot11eCategory, dot1pPriority,
                               securityType); 
            
            if(securityInfo != null) {
                printStream.printf(",securityInfo : %s}",securityInfo.toString());                
            } else {
                printStream.printf("}");
            }
                        
            return byteArrayStream.toString();
        }        
        
        private void internalSetName(String value) {
            if(value != null) {
                name    = value;
            } else {
                name    = String.format("vlan%d",tag);
            }
        }
        
        private void internalSetEssid(String value) {
            if(value != null) {
                essid    = value;
            } else {
                essid    = String.format("essid-vlan%d",tag);
            }            
        }
        
        private void internalSetTag(Number value) {
            if(value != null) {
                tag = value.shortValue();
            } else {
                tag = 4094;
            }
        }
        
        private void internalSetDot11eEnabled(Number value) {
            if(value != null) {
                dot11eEnabled   = value.shortValue();
            } else {
                dot11eEnabled   = 0;
            }
        }
        
        private void internalSetDot11eCategory(Number value) {
            if(value != null) {
                dot11eCategory   = value.shortValue();
            } else {
                dot11eCategory  = 0;
                dot11eEnabled   = 0;
            }            
        }

        private void internalSetDot1pPriority(Number value) {
            if(value != null) {
                dot1pPriority   = value.shortValue();
            } else {
                dot1pPriority  = 0;
            }            
        }
        
        private void internalSetSecurityType(Number value) {
            if(value != null) {
                securityType   = value.shortValue();
            } else {
                securityType  = NMS.SECURITY_TYPE_NONE;;
                securityInfo  = null;
            }            
        }
        
        private void internalSetSecurityInfo(Object value) {
            
            if(value != null) {                
                switch(securityType) {
                case NMS.SECURITY_TYPE_WEP_40:
                case NMS.SECURITY_TYPE_WEP_104:
                    securityInfo = new WEPSecurity((ObjectNotationParser.Object)value);
                    break;
                case NMS.SECURITY_TYPE_WPA_PERSONAL:
                case NMS.SECURITY_TYPE_WPA2_PERSONAL:
                    securityInfo = new WPAPersonalSecurity((ObjectNotationParser.Object)value);
                    break;
                case NMS.SECURITY_TYPE_WPA_ENTERPRISE:
                case NMS.SECURITY_TYPE_WPA2_ENTERPRISE:
                    securityInfo = new WPAEnterpriseSecurity((ObjectNotationParser.Object)value);
                    break;
                default:
                    securityType    = NMS.SECURITY_TYPE_NONE;
                    securityInfo    = null;                        
                }                                
                
            } else {
                securityType    = NMS.SECURITY_TYPE_NONE;
                securityInfo    = null;
            }            
        }
        
        private void internalInitializeDefaults() {
            name            = "vlan";
            essid           = "";
            tag             = 4094;
            dot11eEnabled   = 0;
            dot11eCategory  = 0;
            dot1pPriority   = 0;
            securityType    = NMS.SECURITY_TYPE_NONE;
            securityInfo    = null;          
        }
        
        /**
         * The friendly name for the VLAN.
         */
        
        public String   name;
        
        /**
         * The ESSID used in 802.11 probe-response packets.
         */
        
        public String   essid;
        
        /**
         * The IEEE 802.1q tag for the VLAN.
         */
        
        public short    tag;
        
        /**
         * Non-zero if IEEE 802.11e based QOS is enabled for the VLAN.
         */
        
        public short    dot11eEnabled;
        
        /**
         * The IEEE 802.11e access category to be used for packets for the VLAN.
         * <p>
         * Ignored if {@code dot11eEnabled} is {@code 0}.
         */
        
        public short    dot11eCategory;
        
        /**
         * The IEEE 802.1p bridge priority for the VLAN.
         */
        
        public short    dot1pPriority;
        
        /**
         * The encryption/authentication scheme used to secure connections on
         * the VLAN.
         * 
         * @see NMS#SECURITY_TYPE_NONE
         * @see NMS#SECURITY_TYPE_WEP_104
         * @see NMS#SECURITY_TYPE_WEP_40
         * @see NMS#SECURITY_TYPE_WPA2_ENTERPRISE
         * @see NMS#SECURITY_TYPE_WPA2_PERSONAL
         * @see NMS#SECURITY_TYPE_WPA_ENTERPRISE
         * @see NMS#SECURITY_TYPE_WPA_PERSONAL
         */
        
        public short    securityType;
        
        /**
         * Opaque object containing the security settings for the VLAN.
         * <p>
         * The field represents a {@code NMS.WEPSecurity} object if {@code securityType}
         * is {@code NMS.SECURITY_TYPE_WEP_104} or {@code NMS.SECURITY_TYPE_WEP_40}.
         * <p>
         * The field represents a {@code NMS.WPAPersonalSecurity} object if {@code securityType}
         * is {@code NMS.SECURITY_TYPE_WPA2_PERSONAL} or {@code NMS.SECURITY_TYPE_WPA_PERSONAL}.
         * <p>
         * The field represents a {@code NMS.WPAEnterpriseSecurity} object if {@code securityType}
         * is {@code NMS.SECURITY_TYPE_WPA2_ENTERPRISE} or {@code NMS.SECURITY_TYPE_WPA_ENTERPRISE}.
        * <p>
         * This field is ignored for 802.11 uplink scanner and ethernet
         * interfaces.
         * 
         * @see #securityType
         * @see NMS.WEPSecurity
         * @see NMS.WPAPersonalSecurity
         * @see NMS.WPAEnterpriseSecurity
         */       
        
        public Object   securityInfo;
    }
    
    /**
     * Defines the Access Control List configuration for a node.
     */
    
    public static class ACLConfiguration {     
        
        /**
         * Default constructor, initializes the object with an empty entries array
         * and sets {@code whiteList} to {@code 0}.
         */
        
        public ACLConfiguration() {
            internalInitializeDefaults();
        }

        /**
         * Constructs the {@code ACLConfiguration} from a object notation string.
         * 
         * @param objectNotation
         */
        
        public ACLConfiguration(String objectNotation) {
        
            ObjectNotationParser.Object parsedObject;
                        
            internalInitializeDefaults();
            
            parsedObject = ObjectNotationParser.parse(objectNotation);
            
            if(parsedObject == null)
                return;
            
            try {            
                
                ObjectNotationParser.Object[] objectEntries;
                
                whiteList       = ((Number)parsedObject.getValue("whiteList")).shortValue();
                objectEntries   = (ObjectNotationParser.Object[])parsedObject.getValue("entries");
                
                for(int i = 0; i < objectEntries.length; i++) {                    
                    addEntry(new ACLEntry(objectEntries[i]));                    
                }                
                
            } catch(Exception e) {
                
                /**
                 * Silently ignore exceptions.
                 */
                
            }                                        
        }
        
        @Override
        public String toString() {
            return toObjectNotation();
        }
        
        /**
         * Returns a string containing the object notation representation
         * of the ACLConfiguration object.
         * 
         * @return the object notation string
         */
        
        public String toObjectNotation() {
            
            ByteArrayOutputStream   byteArrayStream;
            PrintStream             printStream;
            
            byteArrayStream = new ByteArrayOutputStream();
            printStream     = new PrintStream(byteArrayStream);
            
            printStream.printf("{ whiteList : %d, \n"
                              + "entries : %s}",
                              whiteList,
                              entries.toObjectNotation());
                        
            return byteArrayStream.toString();
        }         
        
        private void internalInitializeDefaults() {
            whiteList   = 0;
            entries     = new ObjectArray();            
        }
        
        /**
         * Adds the entry into the entries array.
         * 
         * @param entry the entry to be added
         */
        
        public void addEntry(ACLEntry entry) {
            entries.add(entry);
        }        
        
        /**
         * Defines whether the ACL configuration entries specify a 'white-list'.
         * <p>
         * If non-zero, the entries are used as a white-list i.e clients that are not
         * in the list shall be rejected. 
         */
        
        public short        whiteList;
        
        /**
         * The array of {@link NMS.ACLEntry} objects.
         */
        
        public ObjectArray  entries;        
    }
    
    /**
     * Defines  an Access Control List entry.
     */
    
    public static class ACLEntry {    
        
        /**
         * Default constructor.
         */

        public ACLEntry() {
            internalInitializeDefaults();
        }        
        
        /**
         * Package protected constructor to create {@code ACLEntry} from 
         * a {@code ObjectNotation.Object}
         */
        
        ACLEntry(ObjectNotationParser.Object notationObject) throws Exception {
            
            internalInitializeDefaults();
            
            macAddress      = (String)notationObject.getValue("macAddress");
            
            try {
                block   = ((Number)notationObject.getValue("block")).shortValue();
            } catch(Exception e) {
                block   = 0;
            }
            
            try {
                vlanTag = ((Number)notationObject.getValue("vlanTag")).shortValue();
            } catch(Exception e) {
                vlanTag = INVALID_VLAN;
            }
            
            try {            
                dot11eEnabled   = ((Number)notationObject.getValue("dot11eEnabled")).shortValue();
            } catch(Exception e) {
                dot11eEnabled   = 0;
            }
            
            try {            
                dot11eCategory  = ((Number)notationObject.getValue("dot11eCategory")).shortValue();
            } catch(Exception e) {
                dot11eCategory  = 0;
                dot11eEnabled   = 0;
            }
            
        }
        
        @Override
        public String toString() {
            return toObjectNotation();
        }
        
        /**
         * Returns a string containing the object notation representation
         * of the {@code ACLEntry} object.
         * 
         * @return the object notation string
         */
        
        public String toObjectNotation() {
            
            ByteArrayOutputStream   byteArrayStream;
            PrintStream             printStream;
            
            byteArrayStream = new ByteArrayOutputStream();
            printStream     = new PrintStream(byteArrayStream);
            
            printStream.printf("{ macAddress : \"%s\", block : %d, vlanTag : %d,\n"
                              + "dot11eEnabled : %d, dot11eCategory : %d}",
                              macAddress,block,vlanTag,
                              dot11eEnabled, dot11eCategory);
                        
            return byteArrayStream.toString();
        }         
        
        private void internalInitializeDefaults() {
            macAddress      = "00:00:00:00:00:00";
            vlanTag         = INVALID_VLAN;
            dot11eEnabled   = 0;
            dot11eCategory  = 0;
            block           = 0;            
        }        
        
        /**
         * The MAC-address of the device.
         */
               
        public String   macAddress;
        
        /**
         * The IEEE 802.1q VLAN tag to be used when the device associates.
         * <p>
         * Setting this value to {@code ACLEntry.INVALID_VLAN} will put the
         * device on the default VLAN.
         */
        
        public short    vlanTag;
        
        /**
         * Set to non-zero of {@code dot11eCategory} is valid.
         */
        
        public short    dot11eEnabled;
        
        /**
         * The IEEE 802.11e access category for the device.
         * <p>
         * NOTE: This field is ignored if {@code dot11eEnabled} is {@code 0}.
         */
        
        public short    dot11eCategory;
        
        /**
         * Set to non-zero to block the device.
         */
        
        public short    block;
        
        /**
         * Constant specifying the default VLAN.
         */
        
        public static final short INVALID_VLAN = (short)(-1);
    }
    
    /**
     * Defines a Effistream QoS rule.
     */
    
    public static class EffistreamRule {
        
        /**
         * Default constructor typically used to create the 'ROOT' object
         * for the rules. 
         */
        
        public EffistreamRule() {
            this.matchId        = NMS.EFFISTREAM_MATCH_IGNORE;
            this.matchCriteria  = null;
            this.firstChild     = null;
            this.nextSibling    = null;
            this.parent         = null;         
        }        
        
        /**
         * Use this constructor to create a rule without specifying
         * child rules.
         * 
         * @param matchId the match identifier for the rule see {@link #matchId}
         * @param matchCriteria the criteria for a match see {@link #matchCriteria}
         */        
        
        public EffistreamRule(short matchId, String matchCriteria) {
            this.matchId        = matchId;
            this.matchCriteria  = matchCriteria;
            this.firstChild     = null;
            this.nextSibling    = null;
            this.parent         = null;
        }
        
        /**
         * Use this constructor to create a rule directly specifying the first child.
         * <p>
         * e.g. {@code rule = new EffistreamRule(NMS.EFFISTREAM_MATCH_ETH_TYPE,"2048",new EffistreamRule(NMS.EFFISTREAM_MATCH_IP_SRC,"192.168.45.6",0,0,3,36,0)))}<br>
         *   
         * @param matchId the match identifier for the rule see {@link #matchId}
         * @param matchCriteria the criteria for a match see {@link #matchCriteria}
         * @param child the first child rule {@link #firstChild}
         */  
        
        public EffistreamRule(short matchId, String matchCriteria, EffistreamRule child) {            
            
            this.matchId        = matchId;
            this.matchCriteria  = matchCriteria;
            this.firstChild     = child;
            this.nextSibling    = null;
            this.parent         = null;            
            
            if(child.parent != null)
                child.parent    = this;
       }        
        
        /**
         * Use this constructor to create a leaf-level rule object.
         *  
         * @param matchId  the match identifier for the rule see {@link #matchId}
         * @param matchCriteria  the criteria for a match see {@link #matchCriteria}
         * @param actionNoAck see {@link #actionNoAck}
         * @param actionDropPacket see {@link #actionDropPacket}
         * @param actionDot11eCategory see {@link #actionDot11eCategory}
         * @param actionBitRate see {@link #actionBitRate}
         * @param actionQueuedRetry see {@link #actionQueuedRetry}
         */
        
        public EffistreamRule(short     matchId,
                              String    matchCriteria,
                              short     actionNoAck,
                              short     actionDropPacket,
                              short     actionDot11eCategory,
                              short     actionBitRate,
                              short     actionQueuedRetry) {
            
            this.matchId                = matchId;
            this.matchCriteria          = matchCriteria;
            this.firstChild             = null;
            this.nextSibling            = null;
            this.parent                 = null;
            this.actionNoAck            = actionNoAck;
            this.actionDropPacket       = actionDropPacket;
            this.actionDot11eCategory   = actionDot11eCategory;
            this.actionBitRate          = actionBitRate;
            this.actionQueuedRetry      = actionQueuedRetry;
        }        
 
        /**
         * Adds a child rule to the rule object.
         * <p>
         * The child rule is added to the tail of the siblings list
         * 
         * @param child the child rule to add
         */
        
        public void addChild(EffistreamRule child) {
            
            EffistreamRule  children;
            EffistreamRule  lastChild;
            
            lastChild = null;
            
            for(children = firstChild; children != null; children = children.nextSibling) {
                lastChild = children;
            }
            
            if(lastChild != null) {
                lastChild.nextSibling   = child;
            } else {
                firstChild  = child;
            }
            
            child.parent = this;
        }
        
        /**
         * Converts a EffistreamRule object hierarchy to a XML based
         * string.
         * 
         * @return xml based effistream rule hierarchy
         */
        
        public String toXmlSpec() {
            
            ByteArrayOutputStream   byteArrayStream;
            PrintStream             printStream;
            
            byteArrayStream = new ByteArrayOutputStream();
            printStream     = new PrintStream(byteArrayStream);             
            
            internalToXml(printStream);            
            
            return byteArrayStream.toString();
        }
        
      /**
       * Returns a EffistreamRule object hierarchy based on a XML based
       * input.
       * 
       * @param xmlSpec the XML input string
       * @return a EffistreamRule object hierarchy
       * 
       * @throws Exception
       */
      
      public static EffistreamRule fromXmlSpec(String xmlSpec) {
          
          DocumentBuilderFactory  factory;
          DocumentBuilder         builder;
          Document                document;
          ByteArrayInputStream    inputStream;
          
          try {
              
              inputStream = new ByteArrayInputStream(xmlSpec.getBytes());
              factory     = DocumentBuilderFactory.newInstance(); 
              
              builder     = factory.newDocumentBuilder();
              
              document    = builder.parse(inputStream);
              
              if(document == null)
                  return null;                            
              
              return internalProcessXMLNode(document.getChildNodes().item(0));
              
          } catch(Exception e) {
              e.printStackTrace();
          }
     
          return null;
      }
      
      @Override      
      public String toString() {
          return toXmlSpec();
      }
      
      /**
       * Internal function that recursively prints the xml stream to the
       * printStream object.
       */
      
      private void internalToXml(PrintStream printStream) {

          EffistreamRule          child;

          if(matchId == NMS.EFFISTREAM_MATCH_IGNORE) {               
              printStream.print("<effroot>\n");                
          } else {

              printStream.printf("<eff id=\"%d\" ct=\"%s\" ",matchId,matchCriteria);

              if(firstChild != null) {
                  printStream.print(">\n");
              } else {
                  printStream.printf("br=\"%d\" ac=\"%d\" dr=\"%d\" na=\"%d\" qr=\"%d\" />\n",
                                     actionBitRate,
                                     actionDot11eCategory,
                                     actionDropPacket,
                                     actionNoAck,
                                     actionQueuedRetry);
                  return;
              }
          }

          for(child = firstChild; child != null; child = child.nextSibling) {
              child.internalToXml(printStream);
          }      
          
          if(matchId == NMS.EFFISTREAM_MATCH_IGNORE) {
              printStream.print("</effroot>\n");
          } else {
              printStream.print("</eff>\n");
          }
      }

      /**
       * Internal function that recursively processes a XML dom node into a
       * EffistreamRule object hierarchy.
       */
      
      private static EffistreamRule internalProcessXMLNode(org.w3c.dom.Node xmlNode) throws Exception {

          String                          nodeName;
          String                          nodeValue;
          EffistreamRule                  rule;
          EffistreamRule                  childRule;
          org.w3c.dom.NamedNodeMap        attributes;
          org.w3c.dom.Node                attrNode;
          org.w3c.dom.Node                childNode;
          org.w3c.dom.NodeList            nodeList;
                    
          nodeName    = xmlNode.getNodeName();
          rule        = null;

          if(EFFISTREAM_XML_ROOT_NODE.equalsIgnoreCase(nodeName)) {
              rule = new EffistreamRule();
          } else if(EFFISTREAM_XML_NODE.equalsIgnoreCase(nodeName)) {                

              if(!xmlNode.hasAttributes())
                  throw new Exception("eff node found with no attributes");

              rule        = new EffistreamRule();              
              attributes  = xmlNode.getAttributes();               

              for(int i = 0; i < attributes.getLength(); i++) {                                    
                  
                  attrNode    = attributes.item(i);
                                  
                  nodeName    = attrNode.getNodeName();
                  nodeValue   = attrNode.getNodeValue();                                                      

                  if(nodeName.equalsIgnoreCase(EFFISTREAM_XML_ID_ATTR)) { 
                      rule.matchId = Short.parseShort(nodeValue);
                  } else if(nodeName.equalsIgnoreCase(EFFISTREAM_XML_CT_ATTR)) {
                      rule.matchCriteria = new String(nodeValue);
                  } else if(nodeName.equalsIgnoreCase(EFFISTREAM_XML_BR_ATTR)) {
                      rule.actionBitRate = Short.parseShort(nodeValue);
                  } else if(nodeName.equalsIgnoreCase(EFFISTREAM_XML_AC_ATTR)) {
                      rule.actionDot11eCategory = Short.parseShort(nodeValue);
                  } else if(nodeName.equalsIgnoreCase(EFFISTREAM_XML_DR_ATTR)) {
                      rule.actionDropPacket = Short.parseShort(nodeValue);
                  } else if(nodeName.equalsIgnoreCase(EFFISTREAM_XML_NA_ATTR)) {
                      rule.actionNoAck = Short.parseShort(nodeValue);
                  } else if(nodeName.equalsIgnoreCase(EFFISTREAM_XML_QR_ATTR)) {
                      rule.actionQueuedRetry = Short.parseShort(nodeValue);
                  }
              }    
              
              if(rule.matchId == NMS.EFFISTREAM_MATCH_IGNORE)
                  throw new Exception("eff node found with invalid 'id' attribute");
              
          } else {
              throw new Exception("Invalid node name '" + nodeName + "'");
          }

          /**
           * Process child nodes recursively
           */

          nodeList = xmlNode.getChildNodes();

          for(int i = 0; i < nodeList.getLength(); i++) {
              childNode   = nodeList.item(i);
              if(childNode.getNodeType() != org.w3c.dom.Node.ELEMENT_NODE)
                  continue;
              childRule   = internalProcessXMLNode(childNode);                
              rule.addChild(childRule);
          }                        

          return rule;
        }
      
        
        /**
         * Specifies the match identifier for the rule.
         * <p>
         * This can be one of {@link NMS#EFFISTREAM_MATCH_ETH_DST},{@link NMS#EFFISTREAM_MATCH_ETH_SRC},
         * {@link NMS#EFFISTREAM_MATCH_ETH_TYPE},{@link NMS#EFFISTREAM_MATCH_IGNORE},
         * {@link NMS#EFFISTREAM_MATCH_IP_DIFFSRV},{@link NMS#EFFISTREAM_MATCH_IP_DST},
         * {@link NMS#EFFISTREAM_MATCH_IP_PROTO},{@link NMS#EFFISTREAM_MATCH_IP_SRC},
         * {@link NMS#EFFISTREAM_MATCH_IP_TOS},{@link NMS#EFFISTREAM_MATCH_RTP_LENGTH},
         * {@link NMS#EFFISTREAM_MATCH_RTP_VERSION},{@link NMS#EFFISTREAM_MATCH_TCP_DST_PORT},
         * {@link NMS#EFFISTREAM_MATCH_TCP_LENGTH},{@link NMS#EFFISTREAM_MATCH_TCP_SRC_PORT},
         * {@link NMS#EFFISTREAM_MATCH_UDP_DST_PORT},{@link NMS#EFFISTREAM_MATCH_UDP_LENGTH},
         * {@link NMS#EFFISTREAM_MATCH_UDP_SRC_PORT}.
         */
        
        public short            matchId;
        
        /**
         * Specifies the match criteria for the rule. 
         * <p>
         * Depending on the value of {@code matchId} this field contains either a MAC address,
         * an IP address, a 32-bit integer or a range of 32-bit integers all formatted as a string.
         * <p>
         * For more information on the format refer to the match identifiers :
         * <p>
         * {@link NMS#EFFISTREAM_MATCH_ETH_DST},{@link NMS#EFFISTREAM_MATCH_ETH_SRC},
         * {@link NMS#EFFISTREAM_MATCH_ETH_TYPE},{@link NMS#EFFISTREAM_MATCH_IGNORE},
         * {@link NMS#EFFISTREAM_MATCH_IP_DIFFSRV},{@link NMS#EFFISTREAM_MATCH_IP_DST},
         * {@link NMS#EFFISTREAM_MATCH_IP_PROTO},{@link NMS#EFFISTREAM_MATCH_IP_SRC},
         * {@link NMS#EFFISTREAM_MATCH_IP_TOS},{@link NMS#EFFISTREAM_MATCH_RTP_LENGTH},
         * {@link NMS#EFFISTREAM_MATCH_RTP_VERSION},{@link NMS#EFFISTREAM_MATCH_TCP_DST_PORT},
         * {@link NMS#EFFISTREAM_MATCH_TCP_LENGTH},{@link NMS#EFFISTREAM_MATCH_TCP_SRC_PORT},
         * {@link NMS#EFFISTREAM_MATCH_UDP_DST_PORT},{@link NMS#EFFISTREAM_MATCH_UDP_LENGTH},
         * {@link NMS#EFFISTREAM_MATCH_UDP_SRC_PORT}
         */        
        
        public String           matchCriteria;                
        
        /**
         * When non-zero specifies that the packets will be sent without acknowledgement.
         * <p>
         * This field is only valid for leaf-level rules.
         */
        
        public short            actionNoAck;
        
        /**
         * Specifies that the packets will be dropped.
         * <p>
         * This field is only valid for leaf-level rules.
         */
        
        public short            actionDropPacket;
        
        /**
         * Specifies that the IEEE 802.11e category.
         * <p>
         * This field is only valid for leaf-level rules.
         */
        
        public short            actionDot11eCategory;

        /**
         * Specifies that the transmit rate.<br>
         * This field is only valid for leaf-level rules.
         */        
        
        public short            actionBitRate;
        
        /**
         * Specifies that the transmit rate.
         * <p>
         * This field is only valid for leaf-level rules.
         */        
        
        public short            actionQueuedRetry;
        
        /**
         * Reference to the parent rule object.
         */
        
        public EffistreamRule   parent;
        
        /**
         * Reference to the next sibling rule object.
         */
        
        public EffistreamRule   nextSibling;
        
        /**
         * Reference to the next child rule object.
         * <p>
         * When {@code null}, the rule is a leaf-level rule.
         */
        
        public EffistreamRule   firstChild;    
                
        private static final String EFFISTREAM_XML_NODE         = "eff";
        private static final String EFFISTREAM_XML_ROOT_NODE    = "effroot";
        private static final String EFFISTREAM_XML_ID_ATTR      = "id";
        private static final String EFFISTREAM_XML_CT_ATTR      = "ct";
        private static final String EFFISTREAM_XML_BR_ATTR      = "br";
        private static final String EFFISTREAM_XML_AC_ATTR      = "ac";
        private static final String EFFISTREAM_XML_DR_ATTR      = "dr";
        private static final String EFFISTREAM_XML_NA_ATTR      = "na";
        private static final String EFFISTREAM_XML_QR_ATTR      = "qr";        
        

    }        
    
    /**
     * Defines the properties of all neighbor nodes detected by a {@link NMS.Node} 
     * 
     * @see NMS.Node#getNeighborNodes()
     */

    public static interface NeighborNode {
        
        /**
         * Returns a reference to the {@code NMS.Node} object representing the neighbor.
         * 
         * @return a reference to the {@link NMS.Node} object representing the neighbor
         */
        
        public Node getNode();
        
        /**
         * Returns the RSSI of the neighbor's first downlink signal as seen by the uplink.
         * 
         * @return signal RSSI 
         * @see #getUplinkSignal(int)
         */

        public int getUplinkSignal();

        /**
         * Returns the transmit rate from the uplink to the neighbor's first downlink.
         * 
         * @return transmit rate
         * @see #getUplinkTxBitRate(int)
         */        
        
        public int getUplinkTxBitRate();

 
        /**
         * Returns the RSSI as seen by the uplink from the specific downlink of the neighbor.
         * 
         * @param downlinkIndex the index of the neighbor's downlink 
         * @return signal RSSI 
         */        
        
        public int getUplinkSignal(int downlinkIndex);

        /**
         * Returns the transmit rate from the uplink to the specfic downlink of the neighbor.
         * 
         * @param downlinkIndex the index of the neighbor's downlink
         * @return transmit rate
         */    
        
        public int getUplinkTxBitRate(int downlinkIndex);
        
        /**
         * Returns the number of downlink radios seen by the node.
         * 
         * @return downlink count
         */

        public int getDownlinkCount();
    }
    
    /**
     * Defines the properties of all devices connected to a {@link NMS.Node}
     * 
     * @see NMS.Node#getConnectedDevices()
     */    

    public static interface ConnectedDevice {
        
        /**
         * Returns the MAC address of the device formatted as a string.
         * 
         * @return MAC address
         */        
        
        public String getMacAddress();

        /**
         * Returns the RSSI of the packets from the device to the node.
         * 
         * @return signal RSSI
         */
        
        public int getRxSignal();

        /**
         * Returns the transmit rate of packets from the node to the device.
         * 
         * @return transmit rate
         */
        
        public int getTxBitRate();
    }
    
    /**
     * The {@code NetworkListener} interface is used to receive
     * events on a mesh network.
     * 
     * @see NMS.Network#addListener
     */

    public static interface NetworkListener {
        
        /**
         * This method is called when an event occurs on the network.
         * 
         * @param event the code specifying the event that occurred. It can be one of the following:
         * <p>
         *              {@link NMS#EVENT_NODE_DEAD},{@link NMS#EVENT_NODE_HEARTBEAT},
         *              {@link NMS#EVENT_NODE_HEARTBEAT_MISS},{@link NMS#EVENT_NODE_SCAN}
         *              
         * @param network the network on which the event occurred
         * @param node the node for which the event occurred
         * @return Currently the return value is ignored and must be set to 0
         */
        
        public int onEvent(int event, Network network, Node node);
        
    }
              
    /**
     * The {@code Node} interface defines all the properties and actions
     * that can be carried out on a mesh node.
     */

    public static interface Node {
        
        /**
         * Returns the MAC address of the node formatted as a string.
         * 
         * @return MAC address
         */

        public String getUnitMacAddress();

        /**
         * Returns the sequence number of the last heartbeat received from the node.
         * 
         * @return heartbeat sequence number
         */
        
        public long getHeartbeatSqnr();

        /**
         * Returns whether the node is mobile or stationary.
         * 
         * @return {@code true} if the node is mobile, {@code false} otherwise
         */
        
        public boolean isMobile();
        
        /**
         * Returns whether the remote or local.
         * 
         * @return {@code true} if node is remote, {@code false} otherwise
         */
        
        public boolean isRemote();
        

        /**
         * Returns the amount of free RAM in Mega-bytes.
         * 
         * @return free RAM in Mega-bytes
         */
        
        public short getFreeRAM();

        /**
         * Returns the current input voltage to the node.
         * 
         * @return node input voltage
         */
        
        public short getInputVoltage();

        /**
         * Returns the 'Tree Link Rate' for the node. 
         * <p>
         * The 'Tree Link Rate' is the lowest rate in the path from the node to the ROOT.
         * 
         * @return the 'Tree Link Rate'
         */
        
        public short getTreeLinkRate();
        
        /**
         * Returns the current hop level for the node.
         * 
         * @return the number of hops away from the ROOT.
         */

        public short getHopCount();
        
        /**
         * Returns the current average CPU usage for the node.
         * 
         * @return the average cpu usage as a percentage
         */

        public short getCpuUsage();
        
        /**
         * Returns the current node enclosure temperature.
         * 
         * @return the current temperature inside the node enclosure in Celcius.
         */

        public short getTemperature();
        
        /**
         * Returns the signal RSSI in packets received by the parent's downlink interface
         * from this node's uplink.
         * 
         * @return the signal RSSI received by the parent's downlink interface.
         */

        public int getParentDownlinkSignal();
        
        /**
         * Returns the transmit rate used by the parent for packet's transmitted to this {@code Node}.
         * 
         * @return the transmit rate for packets transmitted by parent's downlink.
         */        

        public int getParentDownlinkTxBitRate();
        
        /**
         * Returns the MAC-address of the parent's downlink on which this {@code Node} is connected.
         * 
         * @return MAC-address of parent's downlink interface
         */

        public String getParentBssid();
        
        /**
         * Returns the current operational latitude coordinate in decimal format.
         * <p>
         * Coordinates South of the equator are represented by a negative number.
         * 
         * @return the current operational latitude coordinate 
         */

        public String getGpsCurrentLatitude();
        
        /**
         * Returns the current operational longitude coordinate in decimal format.
         * <p>
         * Coordinates West of the meridian are represented by a negative number.
         * 
         * @return the current operational longitude coordinate 
         */        

        public String getGpsCurrentLongitude();
        
        /**
         * Returns the current operational speed in Km/Hr.
         * 
         * @return the current operational speed
         */

        public short getGpsSpeed();
        
        /**
         * Returns the current operational altitude in meters.
         * 
         * @return the the current operational altitude in meters
         */

        public short getGpsAltitude();
        
        /**
         * Returns the major firmware version for the {@code Node}.
         * 
         * @return the major firmware version.
         */

        public short getFirmwareVersionMajor();
        
        /**
         * Returns the minor firmware version for the {@code Node}.
         * 
         * @return the minor firmware version.
         */        

        public short getFirmwareVersionMinor();
        
        /**
         * Returns the firmware version variant for the {@code Node}.
         * 
         * @return the firmware version variant.
         */             

        public short getFirmwareVersionVariant();
        
        /**
         * Returns non-zero if this {@code Node} can be communicated with using
         * IP.
         * 
         * @return {@code 0} if node is not IP-reachable.
         * @see NMS.GeneralConfiguration#ipAddress
         */

        public short isIpReachable();
        
        /**
         * Returns non-zero if a 'REBOOT' is required for the {@code Node}.
         * 
         * @return {@code 0} if the changes to the {@code Node}'s configuration
         *                   dot not require a reboot.
         *         {@code non-zero} if a reboot is required.
         *                 
         */

        public short rebootRequired();
        
        /**
         * Returns an {@code Enumeration} of nodes that this {@code Node} sees
         * as neighbors.
         * <p>
         * Neighbor nodes are pottential parent nodes, and are connected to, in the event
         * of a link failure.
         * 
         * @return {@code Enumeration} of {@code NeighborNode} objects
         */
        

        public Enumeration<NeighborNode> getNeighborNodes();
        
        /**
         * Returns an {@code Enumeration} of devices that are connected to this {@code Node}.
         * <p>
         * This method returns standard client devices and child mesh nodes.
         * 
         * @return {@code Enumeration} of {@code ConnectedDevice} objects
         */

        public Enumeration<ConnectedDevice> getConnectedDevices();
        
        /**
         * Returns the node level configuration of the {@code Node}.
         * 
         * @return the node level configuration of the {@code Node}
         */

        public GeneralConfiguration getGeneralConfiguration();
        
        /**
         * Returns an {@code Enumeration} of all interfaces in the {@code Node}.
         * 
         * @return {@code Enumeration} of {@code InterfaceConfiguration} objects
         */
        

        public Enumeration<InterfaceConfiguration> getInterfaces();

        /**
         * Returns an {@code Enumeration} of all VLANS in the {@code Node}.
         * 
         * @return {@code Enumeration} of {@code VlanConfiguration} objects
         */
        
        public Enumeration<VlanConfiguration> getVlans();
        
        /**
         * Returns the configuration of the specified interface.
         * 
         * @param name the name of the interface
         * @return {@code InterfaceConfiguration} object for the interface
         */

        public InterfaceConfiguration getInterfaceConfigurationByName(String name);
        
        /**
         * Returns the configuration of the specified VLAN.
         * 
         * @param tag the VLAN identifier
         * @return {@code VlanConfiguration} object for the VLAN
         */

        public VlanConfiguration getVlanConfigurationByTag(short tag);
        
        /**
         * Returns the Effistream<sup>TM</sup> rule hierarchy for the {@code Node}.
         * 
         * @return {@code EffistreamRule} object hierachy
         */
        
        public EffistreamRule getEffistreamRules();
        
        /**
         * Returns the Access Control List configuration for the {@code Node}.
         * 
         * @return {@code ACLConfiguration} object
         */
        
        public ACLConfiguration getACLConfiguration();
        
        /**
         * REBOOT's the {@code Node}.
         */

        public void reboot();
        
        /**
         * Restore's the {@code Node} to factory configuration.
         * 
         * @return {@code 0} on success
         */

        public int restoreDefaults();
        
        /**
         * Executes a Meshdynamics MeshCommand<sup>TM</sup> on the {@code Node}.
         * 
         * @param command the Meshdynamics MeshCommand<sup>TM</sup> to execute
         * @return the result of the command
         */

        public String executeCommand(String command);
        
        /**
         * Upgrades the firmware of the {@code Node}.
         * <p>
         * The firmware file must be one that is created specifically for
         * the MAC address of the {@code Node}.
         * <p>
         * @param firmwareFilePath the path to the firmware upgrade file.
         * @return {@code 0} on success 
         */
        
        public int upgradeFirmware(String firmwareFilePath);
        
        /**
         * Provides network performance information to the {@code Node}.
         * <p>
         * The performance test is run from the host to the {@code Node} and hence
         * will reflect the network performance of all links along the path.
         * 
         * @param recordCount the number of performanc records to be run
         * @param type the type of the performance run, can be one of
         *             {@link NMS#PERFORMANCE_TYPE_SINGLE}, 
         *             {@link NMS#PERFORMANCE_TYPE_DUAL_INDIVIDUAL},
         *             {@link NMS#PERFORMANCE_TYPE_DUAL_SIMULTANEOUS}
         * @param protocol the protocol to be used, can be one of 
         *                 {@link NMS#PERFORMANCE_PROTOCOL_TCP},
         *                 {@link NMS#PERFORMANCE_PROTOCOL_UDP}.
         * @param udpBandWidth when using {@code PERFORMANCE_PROTOCOL_UDP}, the bandwidth in Kbps.
         * 
         * @return the result of the performance test
         */
        
        public String runPerformanceTest(int recordCount, short type, short protocol, int udpBandWidth);
        
        /**
         * Updates the node level configuration for the {@code Node}.
         * <p>
         * If {@code beginConfigurationUpdate} has been called prior to this method,
         * the updated configuration will be sent upon a call to the method {@code commitConfigurationUpdate}.
         * <p>
         * If {@code beginConfigurationUpdate} has not been called prior to this method,
         * the configuration is sent immedietly.
         * 
         * @param configuration the node level configuration 
         * @return {@code 0} upon success
         */

        public int setGeneralConfiguration(GeneralConfiguration configuration);
        
        /**
         * Updates the interface configuration for the {@code Node}.
         * <p>
         * The interface is specified by the {@code name} field of the {@code InterfaceConfiguration} object.
         * <p>
         * If {@code beginConfigurationUpdate} has been called prior to this method,
         * the updated configuration will be sent upon a call to the method {@code commitConfigurationUpdate}.
         * <p>
         * If {@code beginConfigurationUpdate} has not been called prior to this method,
         * the configuration is sent immedietly.
         * 
         * @param configuration the configuration for the interface 
         * @return {@code 0} upon success
         */

        public int setInterfaceConfiguration(InterfaceConfiguration configuration);     
        
        /**
         * Updates the Effistream<sup>TM</sup> rule hierarchy for the {@code Node}.
         * <p>
         * If {@code beginConfigurationUpdate} has been called prior to this method,
         * the updated configuration will be sent upon a call to the method 
         * {@code commitConfigurationUpdate}.
         * <p>
         * If {@code beginConfigurationUpdate} has not been called prior to this method,
         * the configuration is sent immedietly.
         * 
         * @param rules the Effistream<sup>TM</sup> rule hierarchy
         * @return {@code 0} upon success
         */
                
        public int setEffistreamRules(EffistreamRule rules);
        
        /**
         * Adds the specified VLAN to the {@code Node}.
         * <p>
         * If {@code beginConfigurationUpdate} has been called prior to this method,
         * the updated configuration will be sent upon a call to the method 
         * {@code commitConfigurationUpdate}.
         * <p>
         * If {@code beginConfigurationUpdate} has not been called prior to this method,
         * the configuration is sent immedietly.
         * 
         * @param configuration the {@code VlanConfiguration} object
         * @return {@code 0} upon success
         */
        
        public int addVlan(VlanConfiguration configuration);
        
        /**
         * Sets the configuration of an existing VLAN in the {@code Node}.
         * <p>
         * The {@code essid} and {@code tag} fields of the {@code VlanConfiguration} object
         * are used to identify the existing VLAN.
         * <p>
         * If no existing VLAN exisits, the method returns an error.
         * <p>
         * If {@code beginConfigurationUpdate} has been called prior to this method,
         * the updated configuration will be sent upon a call to the method 
         * {@code commitConfigurationUpdate}.
         * <p>
         * If {@code beginConfigurationUpdate} has not been called prior to this method,
         * the configuration is sent immedietly.

         * @param configuration the {@code VlanConfiguration} object
         * @return {@code 0} upon success
         */
        
        public int setVlanConfiguration(VlanConfiguration configuration);
        
        /**
         * Removes the specified VLAN from the {@code Node}.
         * <p>
         * The tag field is used to identify the VLAN.
         * <p>
         * If no existing VLAN exisits, the method returns an error.
         * <p>
         * If {@code beginConfigurationUpdate} has been called prior to this method,
         * the updated configuration will be sent upon a call to the method 
         * {@code commitConfigurationUpdate}.
         * <p>
         * If {@code beginConfigurationUpdate} has not been called prior to this method,
         * the configuration is sent immedietly.
         * 
         * @param tag the tag to identify the existing VLAN
         * @return  {@code 0} upon success
         */
        
        public int removeVlan(short tag);
        
        /**
         * Sets the {@code Node}'s VLAN list from a {@code ObjectArray}.
         * <p>
         * This method delete's all existing VLANs and adds all VLANs in the {@code ObjectArray}.
         * <p>
         * If {@code beginConfigurationUpdate} has been called prior to this method,
         * the updated configuration will be sent upon a call to the method 
         * {@code commitConfigurationUpdate}.
         * <p>
         * If {@code beginConfigurationUpdate} has not been called prior to this method,
         * the configuration is sent immedietly.
         * 
         * @param vlans {@code ObjectArray} containing {@code VlanConfiguration} objects
         * @return {@code 0} upon success
         */
        
        public int setVlans(ObjectArray vlans);
        
        /**
         * Sets the {@code Node}'s Access Control List configuration.
         * <p>
         * This method delete's all existing entries from the ACL configuration and
         * sets the {@code Node}'s Access Control List configuration as specified by the
         * {@code ACLConfiguration} object.
         * <p>
         * If {@code beginConfigurationUpdate} has been called prior to this method,
         * the updated configuration will be sent upon a call to the method 
         * {@code commitConfigurationUpdate}.
         * <p>
         * If {@code beginConfigurationUpdate} has not been called prior to this method,
         * the configuration is sent immedietly.
         * 
         * @param configuration the {@code ACLConfiguration} object
         * @return {@code 0} upon success
         */
        
        public int setACLConfiguration(ACLConfiguration configuration);
        
        /**
         * Generates a configuration macro script for the {@code Node}.
         * 
         * @param scriptLanguage the scripting lanugage to use
         * @return string containing the configuration macro script
         */
        
        public String generateConfigMacro(String scriptLanguage);

        /**
         * Starts a configuration transaction bracket.
         * <p>
         * After a call to this method, calls that update the {@code Node}'s configuration
         * are be not be sent immedietly, but are deferred until a call to
         * {@code commitConfigurationUpdate}.
         * <p>
         * The configuration transaction bracket can be closed by a call to {@code commitConfigurationUpdate}
         * or to {@code cancelConfigurationUpdate}.
         * 
         * @return {@code 0} upon success
         */
        
        public int beginConfigurationUpdate();
        
        /**
         * Closes the current configuration transaction bracket without sending the configuration update.
         * 
         * @return {@code 0} upon success
         */
        
        public int cancelConfigurationUpdate();
        
        /**
         * Closes the current configuration transaction bracket and sends the updated configuration to
         * the {@code Node}.
         * 
         * @return {@code 0} upon success
         */
        
        public int commitConfigurationUpdate();
    }
    
    /**
     * The {@code Network} interface defines all properties and actions 
     * associated with a mesh network.
     * <p>
     * A mesh network is a community of mesh nodes that can :<br><br>
     * <ul>
     *      <li>Communicate with each other using a common security parameters.<br></li>
     *      <li>Be managed as a single entity.</li>
     * </ul>
     * 
     */

    public static interface Network {
        
        /**
         * Returns the name of the mesh network.
         * 
         * @return {@code String} object containing the name of the mesh network
         */
        
        public String getName();
        
        /**
         * Returns an {@code Enumeration} of all mesh nodes in the network. 
         * 
         * @return {@code Enumeration} of all mesh nodes in the network.
         * @see NMS.Node
         */

        public Enumeration<Node> getNodes();
        
        /**
         * Deletes the specified node from the mesh network.
         * 
         * @param node the node to be deleted
         * @return {@code 0} if successful 
         */

        public int deleteNode(Node node);
        
        /**
         * Adds the specified {@code NetworkListener} callback hook to the mesh network.
         * <p>
         * The {@code NetworkListener} callback hook enables the caller to receive information
         * on the events that occur in the mesh network.
         *  
         * @param networklistener the {@code NetworkListener} callback hook to be added
         * @return {@code 0} if successful
         * @see NMS.NetworkListener
         */

        public int addListener(NetworkListener networklistener);
        
        /**
         * Removes the specified {@code NetworkListener} callback hook from the mesh network.
         * <p>
         * If successful, the caller will no longer be able to receive information on the events 
         * that occur in the mesh network.
         *
         * @param networklistener the {@code NetworkListener} callback hook to be removed
         * @return {@code 0} if successful
         * @see NMS.NetworkListener
         */

        public int removeListener(NetworkListener networklistener);
        
        /**
         * Returns the {@code Node} object representing the specified MAC-address.
         * 
         * @param macAddress the mesh node's unit MAC-address to be searched
         * @return {@code Node} object representing the specified MAC-address.
         * @see NMS.Node
         */

        public Node getNodeByMacAddress(String macAddress);
        
        /**
         * Blocks the calling thread until all the nodes specified in {@code macAddresses} parameter
         * are fully detected and configurable.
         * <p>
         * @param macAddresses A string containing comma-seperated list of MAC-addresses to detect
         * @param timeout the number of milli-seconds to block until nodes get detected
         * @return {@code 0} if successful or negative integer if a timeout occurs.
         */
        
        public int waitForNodeDetect(String macAddresses, long timeout);
    }
    
    /**
     * The {@code Thread} class provides multi-threading functionality to scripting platforms.
     */
        
    public static class Thread extends java.lang.Thread {
        
        /**
         * The {@code Runnable} interface is implemented by any class whose instances are executed by
         * a thread.
         * <p>
         * The interface defines a single method {@code run} that represents the running thread.
         * 
         * @see NMS.Thread
         */
        
        public static interface Runnable {
            
            /**
             * The {@code run} method implements the logic for the thread.
             */
            
            public void run();        
        }        
        
        /**
         * Default constructor
         * @param runnable the reference to an object implementing the {@code Runnable} interface
         */
        
        public Thread(Runnable runnable) {
            this.runnable   = runnable;
        }
        
        /**
         * The {@code sleep} method blocks the calling thread for the specified number of
         * milli-seconds.<br><br>
         * 
         * Since it is a static method, the calling thread does not have to be an instance of 
         * the {@code NMS.Thread} class.
         * 
         * @param milliSeconds the number of milli-seconds to block
         */
        
        public static void sleep(long milliSeconds) {
            
            try {
                java.lang.Thread.sleep(milliSeconds);
            } catch (InterruptedException e) {
                
            }
        }             
        
        /**
         * Starts the thread.
         */
        
        @Override
        public void start() {
            super.start();
        }
                
        @Override
        public void run() {
            
            if(runnable != null) {
                runnable.run();
            }
                        
        }
        
        private Runnable runnable;
    }
    
    
    
    /**
     * Defines an array of short integers.
     */

    public static class ShortArray {
        
        /**
         * Package protected constructor.
         * 
         * @param numbers array of Number objects
         */
        
        ShortArray(Number[] numbers) {
            array = new short[numbers.length];
            for(int i = 0; i < numbers.length; i++) {
                array[i] = numbers[i].shortValue();
            }
        }
        
        /**
         * Constructs {@code ShortArray} object with specified number of elements.
         * 
         * @param length the number of elements
         */
        
        public ShortArray(int length) {            
            array   = new short[length];
        }
        
        /**
         * Constructs {@code ShortArray} object with the specified elements.
         * 
         * @param numbers variable argument list of short inetegers
         */
        
        public ShortArray(short ... numbers) {
            set(numbers);            
        }
        
        /**
         * Constructs {@code ShortArray} object from a comma seperated list of numbers.
         * 
         * @param values string containing comma seperated list of numbers
         */
        
        public ShortArray(String values) {
            set(values);
        }
        
        /**
         * Set the elements of the {@code ShortArray} to the specified variable
         * argument list of numbers.
         * 
         * @param numbers variable argument list of short inetegers
         */
                
        public void set(short ... numbers) {
            
            array = new short[numbers.length];
            
            System.arraycopy(numbers,0, array, 0, numbers.length);            
        }
        
        /**
         * Set the elements of the {@code ShortArray} from a comma seperated list
         * of numbers.
         * 
         * @param values string specifying comma seperated list of values
         */
        
        public void set(String values) {
            
            StringTokenizer st;
            
            st      = new StringTokenizer(values,",");
            array   = new short[st.countTokens()];            
            
            for(int i = 0; st.hasMoreElements(); i++) {
                array[i] = Short.parseShort(st.nextToken());
            }        
            
        }
        
        /**
         * Set the value at specified index.
         * 
         * @param index the index
         * @param value the value
         */
        
        public void set(int index, short value) {                
            array[index] = value;            
        }
        
        /**
         * Retrieve the value at the specified index.
         * 
         * @param index the index
         * @return the value at the specified index
         */
        
        public short get(int index) {
            return array[index];
        }
        
        /**
         * Retrieve the number of elements in the {@code ShortArray}.
         * 
         * @return the number of elements
         */

        public int length() {
            return array.length;
        }
               
        @Override
        public String toString() {            
            return toObjectNotation();
        }
        
        
        /**
         * Returns a string containing the object notation representation 
         * for the {@code ShortArray}.
         * 
         * @return string containing object notation representation
         */
        
        public String toObjectNotation() {
            
            ByteArrayOutputStream   byteArrayStream;
            PrintStream             printStream;
            
            byteArrayStream = new ByteArrayOutputStream();
            printStream     = new PrintStream(byteArrayStream);
            
            printStream.print("[");
            
            for(int i = 0; i < array.length; i++) {
                
                if(i != 0)
                    printStream.print(",");
                
                printStream.printf("%d",array[i]);
            }
            
            printStream.print("]");
            
            return byteArrayStream.toString();
        }
        
        private short[] array;
    }
    
    /**
     * The Hashtable class provides an implementation of a Hashtable
     * of generic 'Object' keys and generic 'Object' values.
     */
    
	public static class Hashtable {
	
		/**
		 * Default constructor.
		 */
		
		public Hashtable() {
			hashTable = new java.util.Hashtable<Object, Object>();
			
		}
		
		/**
		 * Retrieves the value for the specified key.
		 * 
		 * @param key the key for which the value is to be retrieved
		 * @return the value
		 */
		
		public Object get(Object key) {
			return hashTable.get(key);
		}
		
		/**
		 * Inserts the specified value for the specified key into the hashtable.
		 * 
		 * @param key the key for which the value is to be inserted
		 * @param value the value to be inserted
		 */
		
		public void put(Object key, Object value) {
			hashTable.put(key, value);
		}
		
		/**
		 * Removes the specified key from the hashtable.
		 */
		
		public void remove(Object key) {
			hashTable.remove(key);
		}
		
		/**
		 * Clears the hashtable.
		 */
		
		public void clear() {
			hashTable.clear();
		}
		
		/**
		 * Returns an {@code Enumeration} of all the keys in the hashtable.
		 * 
		 * @return {@code Enumeration} object for the keys
		 */
		
		public Enumeration<Object> keys() {
			return hashTable.keys();
		}
    	
    	private java.util.Hashtable<Object,Object> hashTable; 
    	
    }
    
    /**
     * The ObjectArray class provides an interface to a growable array
     * that stores object references.
     */
    
    public static class ObjectArray {
        
        /**
         * Default constructor to create the array with 0 elements.
         */
        
        public ObjectArray() {
            array = new Vector<Object>();
            array.setSize(0);            
        }

        /**
         * Constructor to create the array with specified number of elements
         * initialized to null.
         */
        
        public ObjectArray(int length) {            
            array = new Vector<Object>();
            array.setSize(length);
        }
        
        /**
         * Set the object reference at the specified index.
         * 
         * @param index the index
         * @param value the object reference
         */
        
        public void set(int index, Object value) {
        	try {
        		array.set(index, value);
        	}catch(Exception e) {
        		
        	}
        }
        
        /**
         * Retrieves the object reference at the specified index.
         * 
         * @param index the index
         * @return the object reference
         */
        
        public Object get(int index) {
        	try {
        		return array.get(index);
        	} catch(Exception e) {
        		return null;
        	}
        }
        
        /**
         * Retrieve the number of elements in the {@code ObjectArray}.
         * 
         * @return the number of elements
         */        

        public int length() {
            return array.size();
        }        
        
        /**
         * Removes the element at the specified index.
         * 
         * @param index the index of the element to be removed.
         */
        
        public void removeAt(int index) {
        	
        	try {
        		array.removeElementAt(index);
        	} catch(Exception e) {
        		
        	}
        	
        }
        
        /**
         * Add a object reference to the end of the array and increase
         * the length by 1.
         * 
         * @param value the object reference to be added
         */
        
        public void add(Object value) {
            array.add(value);
        }
        
        /**
         * Removes all elements in the array and sets the number of elements 
         * to 0.
         */
        
        public void clear() {
            array.clear();
        }
        
        @Override
        public String toString() {
            return toObjectNotation();
        }
        
        /**
         * Returns a string containing the object notation representation 
         * for the {@code ObjectArray}.
         * 
         * @return string containing object notation
         */        
        
        public String toObjectNotation() {
            
            ByteArrayOutputStream   byteArrayStream;
            PrintStream             printStream;
            Object                  elem;
            
            byteArrayStream = new ByteArrayOutputStream();
            printStream     = new PrintStream(byteArrayStream);
            
           printStream.print("[");
            
            for(int i = 0; i < array.size(); i++) {
                
                if(i != 0)
                    printStream.print(",");
                
                elem = array.get(i);
                
                if(elem instanceof String) {                
                    printStream.printf("\"%s\"",elem);
                } else {
                    printStream.printf("%s",elem.toString());
                }
                
            }
            
            printStream.print("]");            
            
            return byteArrayStream.toString();
        }        
        
        private Vector<Object> array;
    }
    
    /**
     * Returns a reference to the singleton instance of the {@code NMS} class.
     * 
     * @return reference to the {@code NMS} instance
     */

    public static NMS getInstance() {

        if (singletonInstance == null)
            singletonInstance = NMSFactory.createNMS();

        return singletonInstance;
    }
    
    public void internalSetRxSTBC(Number value) {
		// TODO Auto-generated method stub
		
	}

	/**
     * Un-initializes the singleton instance of the {@code NMS} class. 
     */
    
    public static void unInitializeInstance() {
        
        if(singletonInstance != null) {
            
            singletonInstance.unInitialize();
            
            singletonInstance = null;
        }
    }
    
    /**
     * This utility method converts a hexadecimal string into a byte array.
     * 
     * @param hexString the hexadecimal string
     * @return byte array containing the byte representation of the hexadecimal string
     * @see NMS#bytesToHexString
     */

    public static byte[] hexStringToBytes(String hexString) {

        byte[] bytes;
        int i;
        String token;

        bytes = new byte[hexString.length() / 2];

        for (i = 0; i < bytes.length; i++) {
            token = hexString.substring(i * 2, (i * 2) + 2);
            bytes[i] = (byte) Integer.parseInt(token, 16);
        }

        return bytes;
    }
    
    /**
     * This utility method converts a byte array to a hexadecimal string. 
     * 
     * @param bytes the byte array to be converted.
     * @return hexadecimal string
     * @see NMS#hexStringToBytes
     */    

    public static String bytesToHexString(byte[] bytes) {

        int i;
        int b;
        String strBytes;
        String strValue;

        strBytes = "";

        for (i = 0; i < bytes.length; i++) {

            b = bytes[i];
            b &= 0xFF;
            strValue = Integer.toHexString(b);

            if (b >= 0 && b < 16)
                strValue = "0" + strValue;

            strBytes = strBytes + strValue.toUpperCase();

        }

        return strBytes;

    }
    
    /**
     * This utility method converts a byte representation of MAC-address to 
     * a string where the individual bytes are seperated by
     * a ':' character.
     * 
     * @param macAddress byte array containing the MAC address
     * @return string representation of the MAC address
     * @see NMS#macAddressHexStringToBytes
     */

    public static String macAddressBytesToHexString(byte[] macAddress) {

        int i;
        int b;
        String strMacAddress;
        String strValue;

        strMacAddress = "";

        for (i = 0; i < 6; i++) {

            b = macAddress[i];
            b &= 0xFF;
            strValue = Integer.toHexString(b);

            if (b >= 0 && b < 16)
                strValue = "0" + strValue;

            strMacAddress = strMacAddress + strValue.toUpperCase();

            if (i != 5)
                strMacAddress = strMacAddress + ":";
        }

        return strMacAddress;
    }
    
    /**
     * This utlity method converts a byte representation of IP-address to
     * a dotted decimal dormat string.
     * 
     * @param ipAddress the byte array containing the IP-address
     * @return dotted decimal format string representation of the IP-address
     * @see NMS#ipAddressStringToBytes
     */

    public static String ipAddressBytesToString(byte ipAddress[]) {

        String strIpAddress;
        int i;
        int b;
        String strValue;

        strIpAddress = "";

        for (i = 0; i < 4; i++) {

            b = ipAddress[i];
            b &= 0xFF;

            strValue = Integer.toString(b);

            strIpAddress = strIpAddress + strValue;

            if (i != 3)
                strIpAddress = strIpAddress + ".";
        }

        return strIpAddress;
    }
    
    /**
     * This utility method converts a string repsentation of MAC-address to an array of bytes.
     * 
     * @param macAddress the string representation of the MAC-address.
     * @return byte array containing the MAC-address
     * @see NMS#macAddressBytesToHexString
     */

    public static byte[] macAddressHexStringToBytes(String macAddress) {

        byte[] macAddressBytes;
        StringTokenizer st;
        int i;
        String token;

        macAddressBytes = new byte[6];
        st = new StringTokenizer(macAddress, ":");

        for (i = 0; st.hasMoreTokens() && i < 6;) {
            token = st.nextToken();
            macAddressBytes[i++] = (byte) Integer.parseInt(token, 16);
        }

        return macAddressBytes;
    }
    
    /**
     * This utility method converts a dotted-decimal format string IP-address to an array of bytes.
     * 
     * @param ipAddress the dotted-decimal string IP-address.
     * @return byte array containing the IP-address
     * @see NMS#ipAddressBytesToString
     */

    public static byte[] ipAddressStringToBytes(String ipAddress) {

        byte[] ipAddressBytes;
        StringTokenizer st;
        int i;
        String token;

        ipAddressBytes = new byte[4];
        st = new StringTokenizer(ipAddress, ".");

        for (i = 0; st.hasMoreTokens() && i < 6;) {
            token = st.nextToken();
            ipAddressBytes[i++] = (byte) Integer.parseInt(token);
        }

        return ipAddressBytes;
    }
    
    /**
     * Starts the node detection and event generation processes for the {@code NMS} object.
     * @return {@code 0} on success
     */

    public abstract int start();

    /**
     * Stops the node detection and event generation processes for the {@code NMS} object.
     * @return {@code 0} on success
     */    
    
    public abstract int stop();

    /**
     * Starts the Meshdynamics Management Gateway client for remote management.
     * <p>
     * The Meshdynamics Management Gateway client connects to a Meshdynamics Management
     * Gateway server using the HTTP protocol.
     * 
     * @param mode the client mode, can be one of {@link NMS#MG_CLIENT_MODE_FORWARDER} or
     *             {@link NMS#MG_CLIENT_MODE_REMOTE_MANAGER}
     * @param server the IP address or host name of the Meshdynamics Management Gateway server
     * @param port the port on which the Meshdynamics Management Gateway server listens
     * @param useSSL set to {@code true} if a SSL connection is to be used
     * @param userName the account user-name at the  Meshdynamics Management Gateway server
     * @param password the account password
     * @param ignoreLocalPackets local incoming packets will be ignored in {@link NMS#MG_CLIENT_MODE_REMOTE_MANAGER} mode
     * 
     * @return {@code 0} on success
     */

    public abstract int startMGClient(short     mode,
                                      String    server,
                                      int       port,
                                      boolean   useSSL,
                                      String    userName,
                                      String    password,
                                      boolean	ignoreLocalPackets);
    
    
    /**
     * Stops the Meshdynamics Management Gateway client for remote management.
     * 
     * @return {@code 0} on success
     */
    
    public abstract int stopMGClient();
    
    /**
     * Opens the specified mesh network.
     * 
     * @param networkName the mesh network identifier
     * @param networkKey the mesh network key
     * @param networkType the network type ({@code NMS.NETWORK_TYPE_REGULAR} or {@code NMS.NETWORK_TYPE_FIPS_140_2}).
     *                    For {@code NMS.NETWORK_TYPE_FIPS_140_2} the {@code networkKey} specifies a 128-bit hexstring.
     * @return reference to the {@code Network} object or {@code null} on failure 
     */

    public abstract Network openNetwork(String networkName, String networkKey, int networkType);
    
    /**
     * Closes the specified network.
     * 
     * @param network the mesh network to be closed
     * @return {@code 0} on success
     */

    public abstract int closeNetwork(Network network);
    
    /**
     * Returns an {@code Enumeration} of all open {@code Network} objects.
     * 
     * @return {@code Enumeration} of all open {@code Network} objects.
     */

    public abstract Enumeration<Network> getOpenNetworks();
    
    /**
     * Returns a reference to a {@code Network} object with the specified identifier.
     * 
     * @param networkName the mesh network identifier
     * @return reference to the {@code Network} object or {@code null} on failure
     */
    
    public abstract Network getNetworkByName(String networkName);
    
   /**
    * Prints the specified string to the standard output stream.
    * 
    * @param str the string to be printed
    */
    
    public abstract void stdOutPrintln(String str);

    /**
     * Prints the specified string to the error output stream.
     * 
     * @param str the string to be printed
     */

    public abstract void stdErrPrintln(String str);
    
    /**
     * Un-initializes the {@code NMS} instance.
     */

    protected abstract void unInitialize();
    
    /**
     * Protected default constructor to be used by derived classes.
     */
    
    protected NMS() {

    }
    
    /**
     * The singleton instance of the {@code NMS} class.
     */

    private static NMS singletonInstance = null;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about an UPLINK interface.
     * 
     * @see InterfaceConfiguration#usageType
     */

    public static final short USAGE_TYPE_UPLINK = 0;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a DOWNLINK interface.
     * 
     * @see InterfaceConfiguration#usageType
     */    
    
    public static final short USAGE_TYPE_DOWNLINK = 1;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a SCANNER interface.
     * 
     * @see InterfaceConfiguration#usageType
     */ 
    
    public static final short USAGE_TYPE_SCANNER = 2;

    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about an ETHERNET interface.
     * 
     * @see InterfaceConfiguration#phyType
     */    
    
    public static final short PHY_TYPE_ETHERNET = 0;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a IEEE 802.11 wireless interface.
     * 
     * @see InterfaceConfiguration#phyType
     */ 
    
    public static final short PHY_TYPE_802_11 = 1;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about an ETHERNET interface.<br>
     * For interfaces with a {@code phyType} value of {@code PHY_TYPE_ETHERNET}, the
     * {@code phySubType} shall be {@code PHY_SUB_TYPE_IGNORE}.
     * @see InterfaceConfiguration#phyType
     * @see InterfaceConfiguration#phySubType
     */     

    public static final short PHY_SUB_TYPE_IGNORE = 0;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a IEEE 802.11a interface.<br>
     * @see InterfaceConfiguration#phySubType
     */       
    
    public static final short PHY_SUB_TYPE_802_11_A = 1;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a IEEE 802.11b interface.<br>
     * @see InterfaceConfiguration#phySubType
     */     
    
    public static final short PHY_SUB_TYPE_802_11_B = 2;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a IEEE 802.11g interface.<br>
     * @see InterfaceConfiguration#phySubType
     */       
    
    public static final short PHY_SUB_TYPE_802_11_G = 3;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a mixed mode IEEE 802.11b/g interface.<br>
     * @see InterfaceConfiguration#phySubType
     */  
    
    public static final short PHY_SUB_TYPE_802_11_BG = 4;
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a IEEE 802.11n interface.<br>
     * @see InterfaceConfiguration#phySubType
     */       
     public static final int 	PHY_SUB_TYPE_802_11_N = 10;
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a IEEE 802.11ac interface.<br>
     * @see InterfaceConfiguration#phySubType
     */   
	public static final int 	PHY_SUB_TYPE_802_11_AC	= 11;
	  /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a IEEE 802.11bgn interface.<br>
     * @see InterfaceConfiguration#phySubType
     */   
	public static final int 	PHY_SUB_TYPE_802_11_BGN	 = 12;
	  /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a IEEE 802.11an interface.<br>
     * @see InterfaceConfiguration#phySubType
     */   
	public static final int 	PHY_SUB_TYPE_802_11_AN = 13;
	  /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a IEEE 802.11anac interface.<br>
     * @see InterfaceConfiguration#phySubType
     */   
	public static final int 	PHY_SUB_TYPE_802_11_ANAC = 14;
   
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a 5 MHz channel-width 4.9GHz interface.<br>
     * @see InterfaceConfiguration#phySubType
     */      
    
    public static final short PHY_SUB_TYPE_802_11_PSQ = 5;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a 10 MHz channel-width 4.9GHz interface.<br>
     * @see InterfaceConfiguration#phySubType
     */ 
    
    public static final short PHY_SUB_TYPE_802_11_PSH = 6;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * information about a 20 MHz channel-width 4.9GHz interface.<br>
     * @see InterfaceConfiguration#phySubType
     */ 
    
    public static final short PHY_SUB_TYPE_802_11_PSF = 7;
    
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * no security parameters.<br><br>
     * With this setting the {@code securityInfo} field of the {@code InterfaceConfiguration}
     * is ignored and set to {@code null}.
     * @see InterfaceConfiguration#securityType
     */     

    public static final short SECURITY_TYPE_NONE = 0;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * security parameters for IEEE 802.11 WEP encryption using a 40-bit key.<br><br>
     * With this setting the {@code securityInfo} field of the {@code InterfaceConfiguration}
     * references a {@code NMS.WEPSecurity} object.
     * @see InterfaceConfiguration#securityType
     * @see NMS.WEPSecurity
     */       
    
    public static final short SECURITY_TYPE_WEP_40 = 1;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * security parameters for IEEE 802.11 WEP encryption using a 104-bit key.<br><br>
     * With this setting the {@code securityInfo} field of the {@code InterfaceConfiguration}
     * references a {@code NMS.WEPSecurity} object.
     * @see InterfaceConfiguration#securityType
     * @see NMS.WEPSecurity
     */      
    
    public static final short SECURITY_TYPE_WEP_104 = 2;
    
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * security parameters for Wifi Protected Access encryption using a pre-shared key.<br><br>
     * With this setting the {@code securityInfo} field of the {@code InterfaceConfiguration}
     * references a {@code NMS.WPAPersonalSecurity} object.
     * @see InterfaceConfiguration#securityType
     * @see NMS.WPAPersonalSecurity 
     */      
    
    public static final short SECURITY_TYPE_WPA_PERSONAL = 3;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * security parameters for Wifi Protected Access encryption using a RADIUS server.<br><br>
     * With this setting the {@code securityInfo} field of the {@code InterfaceConfiguration}
     * references a {@code NMS.WPAEnterpriseSecurity} object.
     * @see InterfaceConfiguration#securityType
     * @see NMS.WPAEnterpriseSecurity 
     */ 
    
    public static final short SECURITY_TYPE_WPA_ENTERPRISE = 4;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * security parameters for Wifi Protected Access 2 encryption using a pre-shared key.<br><br>
     * With this setting the {@code securityInfo} field of the {@code InterfaceConfiguration}
     * references a {@code NMS.WPAPersonalSecurity} object.
     * @see InterfaceConfiguration#securityType
     * @see NMS.WPAPersonalSecurity 
     */       
    
    public static final short SECURITY_TYPE_WPA2_PERSONAL = 5;
    
    /**
     * Specifies that the {@code InterfaceConfiguration} object contains
     * security parameters for Wifi Protected Access 2 encryption using a RADIUS server.<br><br>
     * With this setting the {@code securityInfo} field of the {@code InterfaceConfiguration}
     * references a {@code NMS.WPAEnterpriseSecurity} object.
     * @see InterfaceConfiguration#securityType
     * @see NMS.WPAEnterpriseSecurity 
     */     
    
    public static final short SECURITY_TYPE_WPA2_ENTERPRISE = 6;

    /**
     * Specifies AES-CCMP encryption to be used for WPA/WPA2 Personal/Enterprise security options.
     * @see InterfaceConfiguration#securityType
     * @see NMS.WPAPersonalSecurity#cipherType
     * @see NMS.WPAEnterpriseSecurity#cipherType
     */
    
    public static final short CIPHER_CCMP = 1;
    
    /**
     * Specifies TKIP encryption to be used for WPA/WPA2 Personal/Enterprise security options.
     * @see InterfaceConfiguration#securityType
     * @see NMS.WPAPersonalSecurity#cipherType
     * @see NMS.WPAEnterpriseSecurity#cipherType
     */    
    
    public static final short CIPHER_TKIP = 2;
    
    /**
     * Specifies that a heartbeat was received from a node in the mesh network.<br>
     * @see NetworkListener#onEvent
     */      

    public static final int EVENT_NODE_HEARTBEAT = 1;
    
    /**
     * Specifies that a node's heartbeat was missed in the mesh network.<br>
     * @see NetworkListener#onEvent
     */    
    
    public static final int EVENT_NODE_HEARTBEAT_MISS = 2;
    
    /**
     * Specifies that a node in unreachable in the mesh network.<br>
     * @see NetworkListener#onEvent
     */    
    
    public static final int EVENT_NODE_DEAD = 3;
    
    /**
     * Specifies that a node is conducting dynamic channel allocation scan.<br>
     * @see NetworkListener#onEvent
     */  
    
    public static final int EVENT_NODE_SCAN = 4;
    
    /**
     * Specifies that a network was closed.<br>
     * @see NetworkListener#onEvent
     */  
 
    public static final int EVENT_NETWORK_CLOSE = 5;
    
    /**
     * Specifies that a {@code Node} has the IGMP multicast optimization
     * option turned on.
     * @see GeneralConfiguration#options
     */

    public static final short OPTION_IGMP = 0x01;
    
    /**
     * Specifies that a {@code Node} has the Disjoint Adhoc feature
     * option turned on.
     * @see GeneralConfiguration#options
     */
    
    public static final short OPTION_ADHOC = 0x02;
    
    /**
     * Specifies that a {@code Node} has the Forced Root feature
     * option turned on.
     * @see GeneralConfiguration#options
     */
    
    public static final short OPTION_FORCED_ROOT = 0x04;
    
    /**
     * Specifies that a {@code Node} has the 'Begin in infrastructure'
     * option turned on for the Disjoint Adhoc feature.
     * @see GeneralConfiguration#options
     */
    
    public static final short OPTION_ADHOC_INFRA_BEGIN = 0x08;
    
    /**
     * Specifies that a {@code Node} has the DHCP server option turned on.
     * @see GeneralConfiguration#options
     */
    
    public static final short OPTION_ADHOC_DHCP = 0x10;
    
    /**
     * Specifies that a {@code Node} has the 802.11 PROBE request based
     * location tracking turned on.
     * @see GeneralConfiguration#options
     */    
    
    public static final short OPTION_LOCATION = 0x20;
    
    /**
     * Specifies that a {@code Node} has the 'SECTORED arbitration'
     * option turned on for the Disjoint Adhoc feature.
     * @see GeneralConfiguration#options
     */
    
    public static final short OPTION_ADHOC_SECTORED = 0x40;
    
    /**
     * Specifies that a {@code Node} has the 'SIP PHONE SYSTEM'
     * option turned on.
     * @see GeneralConfiguration#options
     */
    
    public static final short OPTION_SIP = 0x80;
    
    /**
     * Specifies that the mesh network is a regular network.
     * @see NMS#openNetwork 
     */

    public static final short NETWORK_TYPE_REGULAR = 1;
    
    /**
     * Specifies that the mesh network is a FIPS 140-2 secure network.
     * @see NMS#openNetwork 
     */
    
    public static final short NETWORK_TYPE_FIPS_140_2 = 2;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code used at the ROOT
     * level.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing an integer.<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */
        
    public static final short EFFISTREAM_MATCH_IGNORE       = 0;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the ETHERNET
     * type field.
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */    
    
    public static final short EFFISTREAM_MATCH_ETH_TYPE     = 1;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the ETHERNET
     * destination address field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing a MAC-address.<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */   
    
    public static final short EFFISTREAM_MATCH_ETH_DST      = 2;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the ETHERNET
     * source address field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing a MAC-address.<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */     
    
    public static final short EFFISTREAM_MATCH_ETH_SRC      = 3;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the IP
     * Type-of-Service field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing an integer.<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */ 
    
    public static final short EFFISTREAM_MATCH_IP_TOS       = 4;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the IP
     * Diffrentiated services field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing an integer.<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */     
    
    public static final short EFFISTREAM_MATCH_IP_DIFFSRV   = 5;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the IP
     * source address field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing a IP-address.<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */
    
    public static final short EFFISTREAM_MATCH_IP_SRC       = 6;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the IP
     * destination address field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing a IP-address.<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */
    
    public static final short EFFISTREAM_MATCH_IP_DST       = 7;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the IP
     * protocol field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing an integer.<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */    
    
    public static final short EFFISTREAM_MATCH_IP_PROTO     = 8;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the UDP
     * source port field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing a range (two integers seperated by a {@code :}).<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */   
    
    public static final short EFFISTREAM_MATCH_UDP_SRC_PORT = 9;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the UDP
     * destination port field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing a range (two integers seperated by a {@code :}).<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */   
    
    public static final short EFFISTREAM_MATCH_UDP_DST_PORT = 10;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the UDP
     * datagram length.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing a range (two integers seperated by a {@code :}).<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */  
    
    public static final short EFFISTREAM_MATCH_UDP_LENGTH   = 11;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the TCP
     * source port field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing a range (two integers seperated by a {@code :}).<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */ 
    
    public static final short EFFISTREAM_MATCH_TCP_SRC_PORT = 12;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the TCP
     * destination port field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing a range (two integers seperated by a {@code :}).<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */     
    
    public static final short EFFISTREAM_MATCH_TCP_DST_PORT = 13;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the TCP
     * segment length.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing a range (two integers seperated by a {@code :}).<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */      
    
    public static final short EFFISTREAM_MATCH_TCP_LENGTH   = 14;    
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the RTP
     * version field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing an integer.<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */  
    
    public static final short EFFISTREAM_MATCH_RTP_VERSION  = 15;    
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the RTP
     * payload code field.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing an integer.<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */  
    
    public static final short EFFISTREAM_MATCH_RTP_PAYLOAD  = 16;
    
    /**
     * Specifies a Effistream<sup>TM</sup> match code for the RTP
     * data length.<br>
     * The {@code matchCriteria} of the {@code EffistreamRule} specifies
     * a string containing a range (two integers seperated by a {@code :}).<br>
     * @see EffistreamRule#matchId
     * @see EffistreamRule#matchCriteria
     */  
    
    public static final short EFFISTREAM_MATCH_RTP_LENGTH   = 17;
    
    /**
     * Specifies usage of TCP protocol for running performance tests on a 
     * {@code Node}.
     * 
     * @see NMS.Node#runPerformanceTest
     */
    
    public static final short PERFORMANCE_PROTOCOL_TCP      = 1;
    
    /**
     * Specifies usage of UDP protocol for running performance tests on a 
     * {@code Node}.
     * 
     * @see NMS.Node#runPerformanceTest(int, short, short, int)
     */    
    
    public static final short PERFORMANCE_PROTOCOL_UDP      = 2;
    
    /**
     * Specifies that performance tests on a {@code Node} be run in the direction
     * {@code Host} -> {@code Node}.
     * 
     * @see NMS.Node#runPerformanceTest(int, short, short, int)
     */    
    
    public static final short PERFORMANCE_TYPE_SINGLE           = 1;
    
    /**
     * Specifies that performance tests on a {@code Node} be run in the direction
     * {@code Host} -> {@code Node} and then {@code Node} -> {@code Host}.
     * 
     * @see NMS.Node#runPerformanceTest(int, short, short, int)
     */     
    
    public static final short PERFORMANCE_TYPE_DUAL_INDIVIDUAL  = 2;
    
    /**
     * Specifies that performance tests on a {@code Node} be run in the direction
     * {@code Host} -> {@code Node} and {@code Node} -> {@code Host} simultaneously.
     * 
     * @see NMS.Node#runPerformanceTest(int, short, short, int)
     */     
    
    public static final short PERFORMANCE_TYPE_DUAL_SIMULTANEOUS    = 3;
    
    /**
     * Specifies that the Meshdynamics Management Gateway client operates as
     * a packet forwader, forwarding all management packets from the {@code Node}'s 
     * to the server.
     * 
     * @see NMS#startMGClient
     */
    public static final short MG_CLIENT_MODE_FORWARDER = 1;
    
    /**
     * Specifies that the Meshdynamics Management Gateway client operates as
     * a remote manager, receiving management packets from remote sites.
     * 
     * @see NMS#startMGClient
     */    
    
    public static final short MG_CLIENT_MODE_REMOTE_MANAGER = 2;
    
    /**
     * Specifies the default country code for node operation.
     */
    
    public static final short COUNTRY_CODE_DEFAULT      = 0;
    
    /**
     * Specifies the use of custom channels. 
     * <p>
     * This is only allowed via the use of the Meshdynamics RF-Editor API.
     */
    
    public static final short COUNTRY_CODE_CUSTOM       = 1;
    
    /**
     * Specifies a NULL regulatory domain for node operation.
     */
    
    public static final short REG_DOMAIN_CODE_NONE      = 0;
    
    /**
     * Specifies the FCC regulatory domain for node operation.
     */
    
    public static final short REG_DOMAIN_CODE_FCC       = 1;

    /**
     * Specifies the ETSI regulatory domain for node operation.
     */

    public static final short REG_DOMAIN_CODE_ETSI      = 2;
    
    /**
     * Speciies the custom regulatory domain for node operation.
     * <p>
     * This is only allowed via the use of the Meshdynamics RF-Editor API.
     */    
    
    public static final short REG_DOMAIN_CODE_CUSTOM    = 3;
        
}
