/*
 * Created Date: Jul 11, 2008
 * Author : Sri
 */

package com.meshdynamics.api.impl;

import com.meshdynamics.api.NMS;

@SuppressWarnings("unchecked")

public final class NMSFactory {

    public static interface NMSCreator {
        public NMS create();
    }


    private NMSFactory() {
        
    }

    public static void setCreator(NMSCreator nmsCreator) {
        NMSFactory.nmsCreator = nmsCreator;
    }

    public static NMS createNMS() {
        if(nmsCreator == null) {
            System.err.println("NMSFactory: Cannot instantiate NMS as creators have been setup.");
        }
        return nmsCreator.create();
    }

    private static NMSCreator nmsCreator = null;
    
    static {
        
        /**
         * This block will execute when the classloader loads NMSFactory. Since NMSFactory 
         * is referenced by the NMS class in the getInstance method, this will be loaded
         * before getInstance is executed.
         *  
         * Here we try to create an instance of com.meshdynamics.api.impl.NMSCreator
         * If we succeed, we set that as the default nmsCreator, until someone
         * calls setCreator to overwrite it. 
         * 
         * If we do not succeed, we silently ignore.
         */
        
        Class c;
        
        try {
            c = Class.forName("com.meshdynamics.api.impl.NMSCreator");
        } catch (ClassNotFoundException e) {
            c = null;
        }
        
        if(c != null) {
            try {
                nmsCreator = (NMSCreator)c.newInstance();
            } catch (InstantiationException e) {
                nmsCreator = null;
            } catch (IllegalAccessException e) {
                nmsCreator = null;                
            }
            c = null; /* Release reference to the Class object */
        }        
    }
    
}
