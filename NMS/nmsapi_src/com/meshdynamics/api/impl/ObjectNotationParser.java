/*
 * Created Date: Jul 19, 2008
 * Author : Sri
 */
package com.meshdynamics.api.impl;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Vector;

public final class ObjectNotationParser {
    
    public static Object parse(String objectNotation) {
        
        Tokenizer   tokenizer;
        String      token;
        
        tokenizer   = new Tokenizer(objectNotation);
        
        token       = tokenizer.getToken();
        
        if(token.isEmpty())
            return null;
        
        switch(token.charAt(0)) {
        case '{':     
            return internalParseObject(tokenizer,true);
        case ':':
        case '}':
        case '[':
        case ']':
        case ',':
        case '\"':
            return null;
        default:
            tokenizer.revert();
            return internalParseObject(tokenizer,false);
        }
                             
    }    
    
    private ObjectNotationParser() {
        
    }
    
    private static java.lang.Object internalParseString(Tokenizer tokenizer) {
        
        String  token;
        String  closingQuote;
        
        token           = tokenizer.getToken('\"');
        closingQuote    = tokenizer.getToken();
        
        if(closingQuote.charAt(0) != '\"')
            return null;
        
        return new String(token);
        
    }
    
    private static java.lang.Object internalParseNumeric(Tokenizer tokenizer) {
        
        String              token;
        java.lang.Object    object;
        
        token = tokenizer.getToken();
        
        if(token.indexOf('.') != -1) {
            try {
                object  = Double.valueOf(token);
            } catch(Exception e) {
                return null;
            }
        } else {
            
            try {            
                if(token.startsWith("0x")) {
                    object  = Integer.valueOf(token.substring(2),16);
                } else if(token.startsWith("0")) {
                    object  = Integer.valueOf(token,8);
                } else {
                    object  = Integer.valueOf(token,10);
                }
            } catch(Exception e) {
                return null;
            }            
        }
        
        return object;
    }
    
    private static java.lang.Object internalParseArray(Tokenizer tokenizer) {
        
        java.lang.Object            element;
        String                      token;
        Vector<java.lang.Object>    theArray;
        boolean                     isString;        
        boolean                     isObject;
        
        isString    = false;
        isObject    = false;
        token       = tokenizer.getToken();
        
        if(token.isEmpty()) {
            return null;
        }
        
        if(token.charAt(0) == '\"') {
            isString    = true;
        } else if(token.charAt(0) == '{') {
            isObject    = true;
        } else {
            tokenizer.revert();
        }
        
        theArray = new Vector<java.lang.Object>();
        
        do {
            
            if(isString) {                
                element = internalParseString(tokenizer);
            } else if(isObject) {
                element = internalParseObject(tokenizer, true);                
            } else {                
                element = internalParseNumeric(tokenizer);
            }
            
            if(element == null)
                break;
            
            theArray.add(element);
            
            token = tokenizer.getToken();
            
            if(token.isEmpty())
                break;
            
            if(token.charAt(0) == ',') {
                
                if(isString) {                                        
                    token = tokenizer.getToken();                    
                    if(token.charAt(0) != '\"')
                        break;
                } else if(isObject) {                    
                    token = tokenizer.getToken();                    
                    if(token.charAt(0) != '{')
                        break; 
                }
                
                continue;
            }
            
            if(token.charAt(0) == ']')
                break;
            
        }while(true);
        
        if(isString) {
            
            String[] returnArray;
            
            returnArray = new String[theArray.size()];
            
            for(int i = 0; i < returnArray.length; i++)
                returnArray[i] = (String)theArray.get(i);
            
            return returnArray;
            
        } else if(isObject) {
            
            Object[] returnArray;
            
            returnArray = new Object[theArray.size()];
            
            for(int i = 0; i < returnArray.length; i++)
                returnArray[i] = (Object)theArray.get(i);            
            
            return returnArray;
            
        } else {
            
            Number[]    returnArray;
            
            returnArray = new Number[theArray.size()];
            
            for(int i = 0; i < returnArray.length; i++)
                returnArray[i] = (Number)theArray.get(i);
            
            return returnArray;                        
        }
        
    }
    
    private static Object internalParseObject(Tokenizer tokenizer, boolean openBraceFound) {   
        
        final int _STATE_BEFORE_FIELD_NAME  = 0;
        final int _STATE_AFTER_FIELD_NAME   = 1;
        final int _STATE_BEFORE_FIELD_VALUE = 2;
        final int _STATE_AFTER_FIELD_VALUE  = 3;
        
        String              token;
        int                 state;
        boolean             breakLoop;
        Object              object;
        String              fieldName;
        java.lang.Object    fieldValue;
        
        state       = _STATE_BEFORE_FIELD_NAME;
        breakLoop   = false;
        object      = new Object();
        fieldName   = null;
        fieldValue  = null;
        
        while(true) {
            
            token = tokenizer.getToken();
            
            switch(state) {
            case _STATE_BEFORE_FIELD_NAME:
                
                if(token.isEmpty() || token.charAt(0) == '}') {
                    breakLoop = true;
                    break;
                }
                
                fieldName   = token;
                state       = _STATE_AFTER_FIELD_NAME;
                
                break;
            case _STATE_AFTER_FIELD_NAME:
                
                /**
                 * We expect a ':' token after the field name. If not
                 * we return the object in the current state
                 */
                
                if(token.isEmpty() || token.charAt(0) != ':') {
                    return object;
                }
                
                state = _STATE_BEFORE_FIELD_VALUE;
                
                break;
            case _STATE_BEFORE_FIELD_VALUE:
                
                if(token.isEmpty()) {
                    return object;
                }
                
                fieldValue = null;
                
                switch(token.charAt(0)) {
                case '{':
                    fieldValue  = internalParseObject(tokenizer, true);
                    break;
                case '[':
                    fieldValue  = internalParseArray(tokenizer); 
                    break;
                case '\"':
                    fieldValue  = internalParseString(tokenizer);
                    break;
                default:
                    tokenizer.revert();
                    fieldValue  = internalParseNumeric(tokenizer);
                }
                
                /**
                 * If the value cannot be parsed, return the object as it is
                 */
                
                if(fieldValue == null) {
                    return object;
                }
                
                object.setValue(fieldName,fieldValue);
                
                state = _STATE_AFTER_FIELD_VALUE;
                                
                break;
            case _STATE_AFTER_FIELD_VALUE:
                
                if(token.isEmpty() || token.charAt(0) != ',') {
                    return object;
                }                
                
                state = _STATE_BEFORE_FIELD_NAME;
                
                break;
            }     
            
            if(breakLoop)
                break;
            
        }
        
        return object;
        
    }
    

    
    public static class Object {
        
        Object() {
            valueMap = new Hashtable<String,java.lang.Object>();
        }                
        
        public java.lang.Object getValue(String name) {                        
            return valueMap.get(name);
        }
        
        void setValue(String name, java.lang.Object value) {
            valueMap.put(name,value);
        }
        
        private Hashtable<String,java.lang.Object>    valueMap; 
    }    

    private static class Tokenizer {

        Tokenizer(String input) {
            
            this.input              = input;
            this.currentPosition    = 0;
            this.positionStack      = new LinkedList<Integer>();
            
        }
        
        String getToken(final char untilChar) {
            
            StringBuffer    tokenBuffer;
            char            c;
            boolean         escape;
            
            tokenBuffer = new StringBuffer(); 
            escape      = false;   
            
            if(currentPosition < input.length()) {
                positionStack.push(currentPosition);
            }
            
            while(currentPosition < input.length()) {
                
                c = input.charAt(currentPosition);
                
                if(escape) {
                    tokenBuffer.append(c);
                    ++currentPosition;
                    escape = false;                    
                    continue;
                } else {
                    if(c == '\\') {
                        escape = true;
                        ++currentPosition;
                        continue;
                    }
                }
                
                if(untilChar == c) 
                    break;
                
                tokenBuffer.append(c);
                ++currentPosition;
                
            }
            
            return tokenBuffer.toString();
            
        }
        
        String getToken() {
            
            StringBuffer    tokenBuffer;
            boolean         seenNonOperatorChars;
            boolean         found;
            char            c;
                        
            tokenBuffer             = new StringBuffer();
            seenNonOperatorChars    = false;
            found                   = false;
            
            if(currentPosition < input.length()) {
                positionStack.push(currentPosition);
            }            
            
            while(currentPosition < input.length()) {
                
                c = input.charAt(currentPosition);
                
                switch(c) {
                case ',':
                case ':':
                case '{':
                case '}':
                case '[':
                case ']':
                case '\"':
                    if(seenNonOperatorChars) {
                        found = true;
                    } else {
                        tokenBuffer.append(c);
                        found = true;
                        ++currentPosition;
                    }
                    break;
                case ' ':
                case '\n':
                case '\t':
                    ++currentPosition;
                    break;
                default:
                    seenNonOperatorChars = true;
                    tokenBuffer.append(c);
                    ++currentPosition;
                }
                
                if(found)
                    break;
                
            }
            
            return tokenBuffer.toString();            
        }
        
        void revert() {
            if(!positionStack.isEmpty())
                currentPosition = positionStack.pop();
        }

        private LinkedList<Integer> positionStack;
        private String              input;
        private int                 currentPosition;
    }
}
