
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RadioNoise.js
 * Comments : Network Viewer 10.1 extension for showing radio noise
 * Created  : 9/9/2008
 * Copyright (c) 2008 MeshDynamics, Inc. All rights reserved.
 ********************************************************************************/

/**
 * The RadioNoise.js extension script queries selected nodes
 * for the noise floor information for each radio.
 *
 * It publishes the information into a custom status-tab 
 * of the Network Viewer
 *
 * The information is also made available to the generic Health Checker script
 */

/**
 * ----------------------------------------------
 * Insert our extension into the Network Viewer
 * ----------------------------------------------
 */
 
var nmsExt
var extension;

extension	= new Extension();
nmsExt 		= new NMSUI.Extension(extension);

nmsui.addExtension(nmsExt);

propInit();

function propInit() {

	nmsui.createPropertySection(nmsExt, "RadioNoise", "Noise Floor");
	nmsui.addProperty("RadioNoise", "wlan0", "");
	nmsui.addProperty("RadioNoise", "wlan1", "");
	nmsui.addProperty("RadioNoise", "wlan2", "");
	nmsui.addProperty("RadioNoise", "wlan3", "");
	
}

/**
 * ------------------------------------------------------------
 * Processing thread class - implements NMS.Thread.Runnable
 * ------------------------------------------------------------
 */

function ProcessingThread(extension) {

	this.extension	= extension;
	this.quit		= false;

}

ProcessingThread.prototype.run = function() {

	stdOut.println("RadioNoise : Processing thread started.");

	while(true) {

		NMS.Thread.sleep(2000);

		if(this.quit)
			break;

		this.threadExecute();

	}

	stdOut.println("RadioNoise : Processing thread stopped");
}

ProcessingThread.prototype.threadExecute = function() {

	var	nodeData;
	var	network;
	var	node;
	var	output;
	var	j;
	var	commands;
	var	nodeId;
	
	commands	= [
					"cat /proc/net/atheros/wlan0/noise",
					"cat /proc/net/atheros/wlan1/noise",
					"cat /proc/net/atheros/wlan2/noise",
					"cat /proc/net/atheros/wlan3/noise"
			      ];
			     
	for(nodeId in this.extension.nodes) {     			     	   	

		nodeData = this.extension.nodes[nodeId];

		if(!nodeData.enabled) {
			nodeData.disable();
			continue;
		}
		
		network	= nms.getNetworkByName(nodeData.networkId);
		
		if(network == null) {
			continue;
		}			
		
			
		node	= network.getNodeByMacAddress(nodeData.nodeId);
		
		if(node == null) {
			continue;	
		}			
											
		for(j = 0; j < commands.length; j++) {	
		
			output = node.executeCommand(commands[j]);
		
			nodeData.setNoiseValue(j, output);						
		}
		
		nodeData.updateStatusTab();
				
	}	

}

/**
 * ----------------------------------------------
 * Node data class
 * ----------------------------------------------
 */

function NodeData(networkId, nodeId) {

	var	i;
		
	this.networkId		= networkId;
	this.nodeId			= nodeId;
	this.noiseVal		= new Array(4);
	this.enabled		= false;
	this.statusTabRow	= null;
	
	for(i = 0; i < this.noiseVal.length; i++) {	
		this.noiseVal[i] = "Disabled";		
	}
	
}

NodeData.prototype.disable = function() {

	var	i;
	var hcProperties;
	
	hcProperties	= nmsui.getProperty(this.networkId, this.nodeId, "HealthChecker.Properties");
	
	for(i = 0; i < this.noiseVal.length; i++) {
		this.noiseVal[i]	= "Disabled";
		if(hcProperties != null)		
			hcProperties.remove("RadioNoise-wlan-" + i);		
	}
	
	this.updateStatusTab();

}

NodeData.prototype.setNoiseValue = function(index,value) {

	var	lookFor;
	var	start;
	var	end;
	var nVal;
	var hcProperties;
	
	hcProperties	= nmsui.getProperty(this.networkId, this.nodeId, "HealthChecker.Properties");
	
	lookFor	= "Noise Floor : ";
	
	start	= value.indexOf(lookFor);
	
	if(start == -1) {
	
		start	= value.indexOf("cat");
		
		if(start != -1) {
			this.noiseVal[index] = "--";				
		} else {
			this.noiseVal[index] = "Not Available";
		}		
		
		hcProperties.put("RadioNoise-wlan-" + index,
		                 "wlan" + index + " Noise floor " + this.noiseVal[index] + "|g|");		
		
		return;
			
	}
	
	end	= value.indexOf(",");
	
	this.noiseVal[index] = value.substr(start + lookFor.length, 
	                                    end - (start + lookFor.length));
	                                    
	                                    	                                    
		                                    
	nVal = parseInt(this.noiseVal[index]);
	
	if(nVal > -93) {
		hcProperties.put("RadioNoise-wlan-" + index,
		                 "wlan" + index + " Noise floor" + this.noiseVal[index] + "dBm (Slightly high)|y|");	
	
	} else if(nVal > -85) {
		hcProperties.put("RadioNoise-wlan-" + index,
		                 "wlan" + index + " Noise floor" + this.noiseVal[index] + "dBm (High)|r|");	
	} else {
		hcProperties.put("RadioNoise-wlan-" + index,
		                 "wlan" + index + " Noise floor" + this.noiseVal[index] + "dBm|g|");	
	
	}
}

NodeData.prototype.updateStatusTab = function() {

	var	i;
	
	if(this.statusTabRow == null)
		return;
	
	this.statusTabRow.setValueAt(0, this.nodeId);

	for(i = 0; i < this.noiseVal.length; i++) {
		this.statusTabRow.setValueAt(i + 1, this.noiseVal[i]);	
	}
	
	nmsui.updateStatusTabRow(this.statusTabRow);
	
	nmsui.requestPropertySectionUpdate("RadioNoise", this.nodeId);
		
}

/**
 * --------------------------------------------------------------
 * MenuCommandHandler class - implements NMSUI.MenuCommandHandler
 * --------------------------------------------------------------
 */
 

function MenuCommandHandler(extension) {

	this.extension	= extension;
	
}

MenuCommandHandler.prototype.onCommand = function(menuSection, networkName, nodeId) {

	var	nodeData;
	var commandId;
	
	nodeData	= this.extension.nodes[nodeId];
	
	if(nodeData == null) {
	
		stdOut.println("Node data not found for  " + nodeId);
		
		return;	
	}
	
	commandId = menuSection.getId();

	if(commandId == "Enable") {	
		nodeData.enabled	= true;
	} else if(commandId == "Disable") {
		nodeData.enabled	= false;
	} else if(commandId == "Enable Selected") {	
	
		groupSel = nmsui.getGroupSelection();
		
		while(groupSel.hasMoreElements()) {		
			nodeData = this.extension.nodes[groupSel.nextElement()];
			nodeData.enabled = true;					
		}	
		
	} else if(commandId == "Disable Selected") {
		
		groupSel = nmsui.getGroupSelection();
		
		while(groupSel.hasMoreElements()) {		
			nodeData = this.extension.nodes[groupSel.nextElement()];
			nodeData.enabled = false;					
		}		
	}
}


/**
 * ----------------------------------------------
 * Extension class - implements NMSUI.Extension
 * ----------------------------------------------
 */
 

function Extension() {
	
	this.nodes			= new Array();
	this.thread			= null;
	this.statusTab		= null;	
	this.menuSection	= null;
	this.menuHandler	= new NMSUI.MenuCommandHandler(new MenuCommandHandler(this));
	
}	

Extension.prototype.createMenuIfNotCreated = function() {

	if(this.menuSection != null)
		return;

	this.menuSection = nmsui.createContextMenuSection(nmsExt, null, "Noise Floor Diagnostics", null);
	
	nmsui.createMenuCommand(this.menuSection,
	                        "Enable",
	                        "Enable",
	                        null,
	                        this.menuHandler
	                        );
	                        
	nmsui.createMenuCommand(this.menuSection,
	                        "Disable",
	                        "Disable",
	                        null,
	                        this.menuHandler
	                        );	   
	                        
	nmsui.createMenuCommand(this.menuSection,
	                        "Enable Selected",
	                        "Enable Selected",
	                        null,
	                        this.menuHandler
	                        );			                                  
	                        
	nmsui.createMenuCommand(this.menuSection,
	                        "Disable Selected",
	                        "Disable Selected",
	                        null,
	                        this.menuHandler
	                        );		                                   
}

Extension.prototype.createStatusTabIfNotCreated = function() {

	var headerObj;
	
	if(this.statusTab != null)
		return;

	headerObj = new NMSUI.StatusTabHeader();
	headerObj.addColumn("Node", NMSUI.StatusTabColumn.COLUMN_ALIGN_LEFT, 200);
	headerObj.addColumn("wlan0", NMSUI.StatusTabColumn.COLUMN_ALIGN_LEFT, 100);
	headerObj.addColumn("wlan1", NMSUI.StatusTabColumn.COLUMN_ALIGN_LEFT, 100);
	headerObj.addColumn("wlan2", NMSUI.StatusTabColumn.COLUMN_ALIGN_LEFT, 100);
	headerObj.addColumn("wlan3", NMSUI.StatusTabColumn.COLUMN_ALIGN_LEFT, 100);

	this.statusTab = nmsui.createStatusTab(nmsExt, "Noise Floor", headerObj);		
}

Extension.prototype.viewerStarted = function() {

	var	nmsThread;
	var	runnable;
	
	this.thread			= new ProcessingThread(this);	
		
	runnable			= new NMS.Thread.Runnable(this.thread);
	nmsThread			= new NMS.Thread(runnable);	

	nmsThread.start();		
		
	this.createStatusTabIfNotCreated();
	this.createMenuIfNotCreated();
	
}

Extension.prototype.viewerStopped = function() {

	this.thread.quit	= true;	
			
}

Extension.prototype.networkOpened = function(networkName) {

}

Extension.prototype.networkClosed = function(networkName) {

}

Extension.prototype.networkSelected = function(networkName) {

}

Extension.prototype.nodeSelected = function(networkName, nodeId) {

}

Extension.prototype.nodeAdded = function(networkName, nodeId) {

	var	node;
	
	this.createStatusTabIfNotCreated();
	this.createMenuIfNotCreated();

	node = new NodeData(networkName, nodeId);
	
	node.statusTabRow	= nmsui.addStatusTabRow(this.statusTab);
	
	if(node.statusTabRow == null) {
		stdOut.println("Status tab row could not be created");		
	}
	
	node.updateStatusTab();

	this.nodes[nodeId]	= node;
		
}

Extension.prototype.nodeDeleted = function(networkName, nodeId) {

	this.nodes[nodeId]	= null;

}

Extension.prototype.getPropertyValue = function(sectionId, nodeId, propertyId) {

	var	nodeData;
	var	index;
			
	nodeData	= this.nodes[nodeId];
	index		= parseInt(propertyId.substr(4,1));
								
	return nodeData.noiseVal[index];
}


