
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RadioImpedence.js
 * Comments : Network Viewer 10.1 extension for showing radio impedence problems
 * Created  : 9/9/2008
 * Copyright (c) 2008 MeshDynamics, Inc. All rights reserved.
 ********************************************************************************/
 
 /**
 * The RadioImpedence.js extension script queries selected nodes
 * for the impedence information for each radio.
 *
 * It publishes the information into a custom status-tab 
 * of the Network Viewer
 *
 * The information is also made available to the generic Health Checker script
 */
 
/**
 * ----------------------------------------------
 * Insert our extension into the Network Viewer
 * ----------------------------------------------
 */
 
var nmsExt
var extension;

extension	= new Extension();
nmsExt 		= new NMSUI.Extension(extension);

nmsui.addExtension(nmsExt);

propInit();

function propInit() {

	nmsui.createPropertySection(nmsExt, "RadioImpedence", "Impedence");
	nmsui.addProperty("RadioImpedence", "wlan0", "");
	nmsui.addProperty("RadioImpedence", "wlan1", "");
	nmsui.addProperty("RadioImpedence", "wlan2", "");
	nmsui.addProperty("RadioImpedence", "wlan3", "");
	
}


/**
 * ------------------------------------------------------------
 * Processing thread class - implements NMS.Thread.Runnable
 * ------------------------------------------------------------
 */

function ProcessingThread(extension) {

	this.extension	= extension;
	this.quit		= false;

}

ProcessingThread.prototype.run = function() {

	stdOut.println("RadioImpedence : Processing thread started.");

	while(true) {

		NMS.Thread.sleep(2000);

		if(this.quit)
			break;

		this.threadExecute();

	}

	stdOut.println("RadioImpedence : Processing thread stopped");
}

ProcessingThread.prototype.threadExecute = function() {

	var	nodeData;
	var	network;
	var	node;
	var	output;
	var	j;
	var	commands;
	var	nodeId;
	
	commands	= [
					"cat /proc/net/atheros/wlan0/calib",
					"cat /proc/net/atheros/wlan1/calib",
					"cat /proc/net/atheros/wlan2/calib",
					"cat /proc/net/atheros/wlan3/calib"
			      ];
			     
	for(nodeId in this.extension.nodes) {     			     	   	

		nodeData = this.extension.nodes[nodeId];

		if(!nodeData.enabled) {
			nodeData.disable();
			continue;
		}
		
		network	= nms.getNetworkByName(nodeData.networkId);
		
		if(network == null) {
			continue;
		}			
		
			
		node	= network.getNodeByMacAddress(nodeData.nodeId);
		
		if(node == null) {
			continue;	
		}			
											
		for(j = 0; j < commands.length; j++) {	
		
			output = node.executeCommand(commands[j]);			
		
			nodeData.setCalibValue(j, output);						
		}
		
		nodeData.updateStatusTab();
				
	}	

}

/**
 * ----------------------------------------------
 * Node data class
 * ----------------------------------------------
 */

function NodeData(networkId, nodeId) {

	var	i;
	
	this.networkId		= networkId;
	this.nodeId			= nodeId;
	this.calibVal		= new Array(4);
	this.enabled		= false;
	this.statusTabRow	= null;
	
	for(i = 0; i < this.calibVal.length; i++) {
		this.calibVal[i] = "Disabled";
	}

}

NodeData.prototype.disable = function() {

	var	i;
	var hcProperties;
	
	hcProperties	= nmsui.getProperty(this.networkId, this.nodeId, "HealthChecker.Properties");	
	
	for(i = 0; i < this.calibVal.length; i++) {
		this.calibVal[i]	= "Disabled";
		if(hcProperties != null)		
			hcProperties.remove("RadioImpedence-wlan-" + i);			
	}
	
	this.updateStatusTab();

}

NodeData.prototype.setCalibValue = function(index,value) {

	var	i;
	var	lines;
	var	columns;
	var hcProperties;
	
	hcProperties	= nmsui.getProperty(this.networkId, this.nodeId, "HealthChecker.Properties");
		
	if(value.indexOf("cat") != -1) {
		this.calibVal[index] = "n/a";
		hcProperties.put("RadioImpedence-wlan-" + index,
		                 "wlan" + index + " Impedence " + this.calibVal[index] + "|g|");				
		return;	
	}
	
	lines	= value.split("\n");
	
	if(lines.length < 4)
		return;
						
	columns	= ("" + lines[3]).split("|");
	
	if(columns[2] != "000030") {	
		this.calibVal[index] = "Mismatch";
		hcProperties.put("RadioImpedence-wlan-" + index,
		                 "wlan" + index + " Impedence mismatch|y|");		
	} else {
		this.calibVal[index] = "OK";
		hcProperties.put("RadioImpedence-wlan-" + index,
		                 "wlan" + index + " Impedence OK|g|");			
	}
				
}

NodeData.prototype.updateStatusTab = function() {

	var	i;
	
	if(this.statusTabRow == null)
		return;
	
	this.statusTabRow.setValueAt(0, this.nodeId);

	for(i = 0; i < this.calibVal.length; i++) {
		this.statusTabRow.setValueAt(i + 1, this.calibVal[i]);	
	}
	
	nmsui.updateStatusTabRow(this.statusTabRow);
	
	nmsui.requestPropertySectionUpdate("RadioImpedence", this.nodeId);
		
}

/**
 * --------------------------------------------------------------
 * MenuCommandHandler class - implements NMSUI.MenuCommandHandler
 * --------------------------------------------------------------
 */
 

function MenuCommandHandler(extension) {

	this.extension	= extension;
	
}

MenuCommandHandler.prototype.onCommand = function(menuSection, networkName, nodeId) {

	var	nodeData;
	var commandId;
	
	nodeData	= this.extension.nodes[nodeId];
	
	if(nodeData == null) {
	
		stdOut.println("Node data not found for  " + nodeId);
		
		return;	
	}
	
	commandId = menuSection.getId();

	if(commandId == "Enable") {
		nodeData.enabled	= true;	
	} else if(commandId == "Disable") {
		nodeData.enabled	= false;
	} else if(commandId == "Enable Selected") {	
	
		groupSel = nmsui.getGroupSelection();
		
		while(groupSel.hasMoreElements()) {		
			nodeData = this.extension.nodes[groupSel.nextElement()];
			nodeData.enabled = true;					
		}	
		
	} else if(commandId == "Disable Selected") {
		
		groupSel = nmsui.getGroupSelection();
		
		while(groupSel.hasMoreElements()) {		
			nodeData = this.extension.nodes[groupSel.nextElement()];
			nodeData.enabled = false;					
		}		
	}
}


/**
 * ----------------------------------------------
 * Extension class - implements NMSUI.Extension
 * ----------------------------------------------
 */
 

function Extension() {
	
	this.nodes			= new Array();
	this.thread			= null;
	this.statusTab		= null;	
	this.menuSection	= null;
	this.menuHandler	= new NMSUI.MenuCommandHandler(new MenuCommandHandler(this));
	
}	

Extension.prototype.createMenuIfNotCreated = function() {

	if(this.menuSection != null)
		return;

	this.menuSection = nmsui.createContextMenuSection(nmsExt, null, "Impedence Diagnostics", null);
	
	nmsui.createMenuCommand(this.menuSection,
	                        "Enable",
	                        "Enable",
	                        null,
	                        this.menuHandler
	                        );
	                        
	nmsui.createMenuCommand(this.menuSection,
	                        "Disable",
	                        "Disable",
	                        null,
	                        this.menuHandler
	                        );	  

	nmsui.createMenuCommand(this.menuSection,
	                        "Enable Selected",
	                        "Enable Selected",
	                        null,
	                        this.menuHandler
	                        );			                                  
	                        
	nmsui.createMenuCommand(this.menuSection,
	                        "Disable Selected",
	                        "Disable Selected",
	                        null,
	                        this.menuHandler
	                        );	                     
	
}

Extension.prototype.createStatusTabIfNotCreated = function() {

	var headerObj;
	
	if(this.statusTab != null)
		return;

	headerObj = new NMSUI.StatusTabHeader();
	headerObj.addColumn("Node", NMSUI.StatusTabColumn.COLUMN_ALIGN_LEFT, 200);
	headerObj.addColumn("wlan0", NMSUI.StatusTabColumn.COLUMN_ALIGN_LEFT, 100);
	headerObj.addColumn("wlan1", NMSUI.StatusTabColumn.COLUMN_ALIGN_LEFT, 100);
	headerObj.addColumn("wlan2", NMSUI.StatusTabColumn.COLUMN_ALIGN_LEFT, 100);
	headerObj.addColumn("wlan3", NMSUI.StatusTabColumn.COLUMN_ALIGN_LEFT, 100);

	this.statusTab = nmsui.createStatusTab(nmsExt, "Impedence", headerObj);		
}

Extension.prototype.viewerStarted = function() {

	var	nmsThread;
	var	runnable;
	
	this.thread			= new ProcessingThread(this);	
		
	runnable			= new NMS.Thread.Runnable(this.thread);
	nmsThread			= new NMS.Thread(runnable);	

	nmsThread.start();		
		
	this.createStatusTabIfNotCreated();
	this.createMenuIfNotCreated();
	
}

Extension.prototype.viewerStopped = function() {

	this.thread.quit	= true;	
			
}

Extension.prototype.networkOpened = function(networkName) {

}

Extension.prototype.networkClosed = function(networkName) {

}

Extension.prototype.networkSelected = function(networkName) {

}

Extension.prototype.nodeSelected = function(networkName, nodeId) {

}

Extension.prototype.nodeAdded = function(networkName, nodeId) {

	var	node;
	
	this.createStatusTabIfNotCreated();
	this.createMenuIfNotCreated();

	node = new NodeData(networkName, nodeId);
	
	node.statusTabRow	= nmsui.addStatusTabRow(this.statusTab);
	
	if(node.statusTabRow == null) {
		stdOut.println("Status tab row could not be created");		
	}
	
	node.updateStatusTab();

	this.nodes[nodeId]	= node;
		
}

Extension.prototype.nodeDeleted = function(networkName, nodeId) {

	this.nodes[nodeId]	= null;

}

Extension.prototype.getPropertyValue = function(sectionId, nodeId, propertyId) {

	var	nodeData;
	var	index;
			
	nodeData	= this.nodes[nodeId];
	index		= parseInt(propertyId.substr(4,1));
								
	return nodeData.calibVal[index];
}

