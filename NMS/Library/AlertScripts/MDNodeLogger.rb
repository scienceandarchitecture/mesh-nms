
###******************************************************************************
 # MeshDynamics 
 # -------------- 
 # File     : MDNodeLogger.rb
 # Comments : Node Logger Script for Network Viewer 10.1
 # Created  : 2/13/2008
 # Copyright (c) 2008 MeshDynamics, Inc. All rights reserved.
###***************************

***************************************************

def getNodeFileName(node)

	address  = node.getUnitMacAddress
	fileName = "Log_" + address.gsub(/[:]/, '_') + ".csv"	
	
	return fileName

end

def getParentNeighborNode(node)

	neighbors	= node.getNeighborNodes()
	entry		= nil
		
	while neighbors.hasMoreElements()
	
		neighbor = neighbors.nextElement()
		
		children = neighbor.getNode().getConnectedDevices()	
				
		while children.hasMoreElements()			
		
			child = children.nextElement()
						
			if node.getUnitMacAddress() == child.getMacAddress()		
				entry = neighbor
				break
			end
		
		end	
		
		break if entry != nil	
	
	end
	
	return entry

end

# Network Viewer calls this function on every heartbeat

def checkAlert(network, node)   

	fileName = getNodeFileName node
  
    begin
        
		if File.exist?(fileName)
			fd = File.new(fileName,"a")
		else
			fd = File.new(fileName,"w")
			fd.puts "TIMESTAMP,PARENT,RX-SIGNAL,RX-RATE,TX-SIGNAL,TX-RATE,LATITUDE,LONGITUDE,ALTITUDE"
		end
				
		entry = getParentNeighborNode(node)
				
		lineString  = "" + Time.now.to_s()			
		
		if entry != nil
			
			lineString << "," << node.getParentBssid()
			lineString << "," << entry.getUplinkSignal().to_s()
			lineString << "," << node.getParentDownlinkTxBitRate().to_s()
			lineString << "," << node.getParentDownlinkSignal().to_s()
			lineString << "," << entry.getUplinkTxBitRate().to_s()
				
		else
				
			lineString << ",--,--,--,--,--"
		
		end
		
		lineString << "," << node.getGpsCurrentLatitude() 
		lineString << "," << node.getGpsCurrentLongitude() 
		lineString << "," << node.getGpsAltitude().to_s()
				
		fd.puts lineString
		
		fd.close 
				    
    rescue
    
    end

end
