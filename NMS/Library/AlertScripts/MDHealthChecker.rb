
###******************************************************************************
 # MeshDynamics 
 # -------------- 
 # File     : MDHealthChecker.rb
 # Comments : Generic Health Checker Script for Network Viewer 10.1
 # Created  : 9/9/2008
 # Copyright (c) 2008 MeshDynamics, Inc. All rights reserved.
###******************************************************************************

#
# The MDHealthChecker.rb script maintains for each node
# a property known as HealthChecker.Data
# This property is a Ruby-Hashtable consisting of objects of
# HCProperty class indexed by the @name member
# Each HCProperty object consists if name, value, condition members
#
# The MDHealthChecker.rb script also maintains for each node
# a property known as HealthChecker.Properties
# This property is a NMS::Hashtable containing string elements
# Each string is of the form  VALUE|CONDITION|SPECIAL
# Extension scripts can publish their data into each node's
# HealthChecker.Properties Hashtable.
#
# The condition field can be 'r' 'g' or 'y'
#

# Definiton of the HCProperty class

class HCProperty 
  
        def initialize(name)         
          @name           = name
          @value          = ""
          @condition      = ""
          @special        = ""   
          @alertCondition = ""
          @custom         = false
          @flag           = false
        end

        attr_accessor :custom
        attr_accessor :value
        attr_accessor :condition
        attr_accessor :special
        attr_accessor :alertCondition
        attr_accessor :flag
        attr_reader   :name    
                
end

# Helper function to create an instance of HCProperty
# and add it to the properties hash-table

def createProperty(properties, name)

        prop = properties[name]

        if  prop == nil then
                prop = HCProperty.new(name)
                properties[name] = prop
        end

        return prop
end


# Defines the standard properties for the first time

def defineStandardProperties(networkId, nodeId)

        properties = Hash.new
        
        createProperty properties, "Temperature"
        createProperty properties, "Voltage"
        createProperty properties, "Signal"

        prop = createProperty properties, "HB-Sqnr"
        prop.flag = true

        $nmsui.setProperty(networkId, nodeId, "HealthChecker.Data", properties)

        return properties
end

# Searches through the value of HealthChecker.Properties
# and adds any new property to the Hash table

def updateCustomProperties(networkId, nodeId, properties)

    customProps = $nmsui.getProperty(networkId, nodeId, "HealthChecker.Properties")
    
    keys = customProps.keys
    
    while keys.hasMoreElements()
    
      name = keys.nextElement()
    
      propString = customProps.get(name)
      
      # Each propString is in the format VALUE|CONDITION|SPECIAL
      
      values = propString.split('|')         
      
      prop           = createProperty properties, name
      prop.value     = values[0]
      prop.condition = values[1]
      prop.special   = values[2]
      prop.custom    = true
    
  end
  
        
end

def updateHBSqnr(node, properties)

  tempProperty = properties["HB-Sqnr"]
  sqnr         = node.getHeartbeatSqnr().to_s()

  if tempProperty.special == "" then

    tempProperty.condition = "g"    
    tempProperty.special =  sqnr   
    tempProperty.value = ""

    return

  end

  if sqnr.to_i() < tempProperty.special.to_i() then
    tempProperty.condition = "r"
    tempProperty.value = "Node rebooted or reset"
  else
    tempProperty.condition = "g"
    tempProperty.value = ""

  end

  tempProperty.special =  sqnr

end

def updateTemperature(node, properties)
  
  tempProperty = properties["Temperature"]
  
  tempValue = node.getTemperature()
    
  tempProperty.value = "Temperature : " + tempValue.to_s() + " C"
  
  if tempValue < -20 then    
    tempProperty.condition = "r"
    tempProperty.value = tempProperty.value + " (Very Low)"
  elsif tempValue < 0 then  
    tempProperty.condition = "y"    
  elsif tempValue >= 0 and tempValue < 50 then   
    tempProperty.condition = "g"  
  elsif tempValue >= 50 and tempValue < 65 then  
    tempProperty.condition = "y"    
    tempProperty.value = tempProperty.value + " (Higher than normal)"
  else    
    tempProperty.condition = "r"    
    tempProperty.value = tempProperty.value + " (Very High)"
  end

end

def updateVoltage(node, properties)

  voltProperty = properties["Voltage"]

  voltValue = node.getInputVoltage()
  
  if(voltValue < 9)
    return
  end
  
  voltProperty.value = "Voltage : " + voltValue.to_s() + " V"
  
  if voltValue < 12  then    
    voltProperty.condition = "r"
    voltProperty.value = voltProperty.value + " (Very Low)"
  elsif voltValue < 18
    voltProperty.condition = "y"
    voltProperty.value = voltProperty.value + " (Voltage >= 18V is ideal)"
  else
    voltProperty.condition = "g"    
  end

end

def updateSignalStrength(node, properties)

  signalProperty = properties["Signal"]
  
  if node.getHopCount() > 0 then
    
    signalValue = node.getParentDownlinkSignal()

    # Ignore 0 and -96 signal values

    if signalValue == 0 or signalValue == -96 then
      return
    end
    
    signalProperty.value = "Signal Strength : " + signalValue.to_s() + " dBm"
    
    if signalValue < -85  then    
      signalProperty.condition = "r"
      signalProperty.value = signalProperty.value + " (Very Low)"
    elsif signalValue <= -70
      signalProperty.condition = "y"
      signalProperty.value = signalProperty.value + " (On the lower side)"
    elsif signalValue <= -40
      signalProperty.condition = "g"
    elsif signalValue > -40
      signalProperty.condition = "r"
      signalProperty.value = signalProperty.value + " (Very high)"
    end
    
  else
    signalProperty.condition = "g"
    signalProperty.value = "Signal : N/A"    
  end
  
end

def evaluateProperties(networkId, nodeId, properties)  
  
  keys = properties.keys
  reds = NMS::ObjectArray.new
  yellows = NMS::ObjectArray.new
  greens = NMS::ObjectArray.new
  ledIndex = 0
  ledColors = ["",""]
  ledLabels = ["",""]
  
  customProps = $nmsui.getProperty(networkId, nodeId, "HealthChecker.Properties")  
    
  0.upto(keys.length - 1) do |i|
    
    property = properties[keys[i]]
    
    # For custom properties, if the property no longer exists
    # in the global HealthChecker.Properties bag, delete it from
    # our hashtable also.
    
    if property.custom == true then
      if customProps.get(property.name) == nil then
        properties.delete(keys[i])
        next if true
      end
    end
      
    if property.condition == "r" then      
        reds.add property
        if property.alertCondition != property.condition then
            $nmsui.showAlert NMSUI::Alert.new(NMSUI::Alert::ALERT_LEVEL_HIGH, nodeId + " " + property.value, nodeId + " " + property.value)
            property.alertCondition = property.condition
        end
    elsif property.condition == "y" then
        yellows.add property
        if property.alertCondition != property.condition then
            $nmsui.showAlert NMSUI::Alert.new(NMSUI::Alert::ALERT_LEVEL_LOW, nodeId + " " + property.value, nodeId + " " + property.value)
            property.alertCondition = property.condition
        end        
    elsif property.condition == "g" then    
        greens.add property
    end            
    
  end
      
  # Go through all the Reds first
  
  0.upto(reds.length() - 1) do |i|
    
    next if reds.get(i).flag == true
    break if ledIndex >= 2    
    
    ledColors[ledIndex] = NMSUI::NODE_ELEMENT_VALUE_COLOR_RED 
    ledLabels[ledIndex] = reds.get(i).value
    
    ledIndex = ledIndex + 1
    
  end  
  
  # Go through all the Yellows next
  
  0.upto(yellows.length() - 1) do |i|
    
    next if yellows.get(i).flag == true
    break if ledIndex >= 2
    
    ledColors[ledIndex] = NMSUI::NODE_ELEMENT_VALUE_COLOR_YELLOW 
    ledLabels[ledIndex] = yellows.get(i).value
    
    ledIndex = ledIndex + 1
    
  end   
  
  # Go through all the Greens last
  
  0.upto(greens.length() - 1) do |i|
    
    next if greens.get(i).flag == true
    break if ledIndex >= 2
    
    ledColors[ledIndex] = NMSUI::NODE_ELEMENT_VALUE_COLOR_GREEN
    ledLabels[ledIndex] = greens.get(i).value
    
    ledIndex = ledIndex + 1
    
  end      
       
  $nmsui.setNodeElementProperty networkId, nodeId, NMSUI::NODE_ELEMENT_PROPERTY_LED1_COLOR, ledColors[0]
  $nmsui.setNodeElementProperty networkId, nodeId, NMSUI::NODE_ELEMENT_PROPERTY_LED2_COLOR, ledColors[1]
  
  $nmsui.setNodeElementProperty networkId, nodeId, NMSUI::NODE_ELEMENT_PROPERTY_LED1_LABEL , ledLabels[0]
  $nmsui.setNodeElementProperty networkId, nodeId, NMSUI::NODE_ELEMENT_PROPERTY_LED2_LABEL , ledLabels[1]
  
end


# Network Viewer calls this function on every heartbeat

def checkAlert(network, node)   
  
    begin
         
        # Create the HealthChecker.Data for the node if not already created.
  
        properties = $nmsui.getProperty(network.getName(), node.getUnitMacAddress(), "HealthChecker.Data")
        
        if properties == nil then
             properties = defineStandardProperties(network.getName(), node.getUnitMacAddress())
        end
       
        # Create the HealthChecker.Properties for the node if not already created.
        
        customProps = $nmsui.getProperty(network.getName(), node.getUnitMacAddress(), "HealthChecker.Properties")
        
        if customProps == nil then
            $nmsui.setProperty(network.getName(), node.getUnitMacAddress(), "HealthChecker.Properties", NMS::Hashtable.new)
        end
        
        # Update the values and conditions of the standard properties
       
        updateHBSqnr node, properties
        updateTemperature node, properties
        updateVoltage node, properties
        updateSignalStrength node, properties   
        
        # Update the values and conditions of custom properties
        
        updateCustomProperties network.getName(), node.getUnitMacAddress(), properties
        
        # Evaluate all the properties and decide which ones shall be displayed on the LEDs

        evaluateProperties network.getName(), node.getUnitMacAddress(), properties  
        
    rescue
      
      # Silently ignore any exceptions
      
    end
       
end

