#Help Resource Table
#CONTEXTTAG,PAGENUMBER (in pdf),COMMENTS

mainwindow,				8,	"Press F1 for Help"
propertieswindow,		8,	"Press F1 for Help"
alertswindow, 			9,	"Press F1 for Help"
networkswindow, 		9,	"Press F1 for Help"
heartbeatwindow,		9,	"Press F1 for Help"
macroactionswindow,		9,	"Press F1 for Help"
clientactivitywindow,	10, "Press F1 for Help"
pbvwindow,				10, "Press F1 for Help"

configtabgeneral,		15,	"Press F1 for Help"
configtabinterface,		16,	"Press F1 for Help"
configtabsecurity,		17,	"Press F1 for Help"
configtabvlan,			21,	"Press F1 for Help"
configtabacl,			22,	"Press F1 for Help"

advconfigtabigmp,		23,	"Press F1 for Help"
advconfigtabrfeditor,	24, "Press F1 for Help"
advconfigtabeffistream,	25,	"Press F1 for Help"
advconfigtabethernet,	26,	"Press F1 for Help"
advconfigtabdot11e,		27,	"Press F1 for Help"
advconfigtabpbv,		28,	"Press F1 for Help"
advconfigtabp3m,		29,	"Press F1 for Help"

dlgeffistreamaddrule,	25,	"Press F1 for Help"
dlgeffistreamaddaction,	25,	"Press F1 for Help"
dlgnewnetwork,			31,	"Press F1 for Help"
dlgrunmacropage1,		32,	"Press F1 for Help"
dlgrunmacropage2,		33,	"Press F1 for Help"
dlgviewnodesettings,	39,	"Press F1 for Help"
dlgmgproperties,		42,	"Press F1 for Help"
dlgiperf,				44,	"Press F1 for Help"
dlgrfspaceinformation,	46,	"Press F1 for Help"
dlgfipswizard,			47,	"Press F1 for Help"
dlggpswizard,			48,	"Press F1 for Help"
 
 
 





