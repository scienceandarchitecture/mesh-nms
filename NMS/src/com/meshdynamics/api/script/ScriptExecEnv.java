/*
 * Created Date: Jul 21, 2008
 * Author : Sri
 */
package com.meshdynamics.api.script;

import java.io.Reader;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.script.Bindings;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import com.meshdynamics.api.NMS;
import com.meshdynamics.api.NMS.Network;

import sun.misc.Service;

/**
 * ScriptExecEnv 
 */

public class ScriptExecEnv implements ScriptEnvironment {
    
    @SuppressWarnings("unchecked")
    public ScriptExecEnv(String engineName) throws Exception {
    	
        ScriptEngineManager     manager;
        
        this.engineName = engineName;
        
        manager = new ScriptEngineManager();
        engine  = manager.getEngineByName(engineName);               
        
        if(engine == null)
            throw new Exception("Engine " + engineName + " was not found");  
 
        nameSpace = new Namespace();
        
        engine.setBindings(nameSpace, ScriptContext.GLOBAL_SCOPE);
        
        importClass("com.meshdynamics.api.NMS");
        
        /**
         * Call the script environment initializers 
         */        
        
        try {
            
            Iterator            itServices;
            ScriptExecEnvInit   initializer;
            
            itServices = Service.providers(ScriptExecEnvInit.class);
                               
            while(itServices.hasNext()) {    
                try {
                    initializer = (ScriptExecEnvInit)itServices.next();
                    initializer.initialize(this);
                } catch(Exception e) {             
                }
            }
        } catch(Exception e) {
            /** Silently Ignore */
        }
    } 
    
        
    public void execute(String script) throws Exception {
        engine.eval(script);
    }
    
    public void execute(Reader reader) throws Exception {
        engine.eval(reader);        
        
    }   
    
    public void defineVariable(String name, Object value) throws Exception {
        nameSpace.put(name, value);       
    }
    
    public Object getVariable(String name)  throws Exception {
        return nameSpace.get(name);
    }
    
    public Object invokeMethod(Object thisObject, String name, Object...args) throws Exception {
        
        Invocable invocableEngine;
        
        invocableEngine = (Invocable)engine;
        
        return invocableEngine.invokeMethod(thisObject, name, args);
    }
    
    public Object invokeFunction(String name, Object...args) throws Exception {
        
        Invocable invocableEngine;
        
        invocableEngine = (Invocable)engine;
        
        return invocableEngine.invokeFunction(name, args);
        
    }
    
    private class Namespace implements Bindings {
        
        Namespace() {
            
            map = new Hashtable<String, Object>();
            nms = NMS.getInstance();
            
        }

        
        public boolean containsKey(Object obj) {
            
            boolean ret;
            
            ret = map.containsKey(obj);
            
            if(ret == false) {
                
                if(((String)obj).startsWith("nmsNet")) {
                    
                    Network network;
                    
                    network = nms.getNetworkByName(((String)obj).substring(4));
                    
                    if(network != null)
                        map.put((String)obj, network);
                    
                    return true;
                } else if(((String)obj).equals("nms")) {
                	map.put((String)obj, nms);
                	return true;
                } else if(((String)obj).equals("stdOut")) {
                	map.put((String)obj, System.out);
                	return true;
                } else if(((String)obj).equals("stdErr")) {
                	map.put((String)obj, System.err);
                	return true;
                } 
            }            
            
            return ret;
        }

        public Object get(Object obj) {
            
            Object  ret;
            
            ret = map.get(obj);
            
            if(ret == null) {
                
                if(((String)obj).startsWith("nmsNet")) {
                    
                    Network network;
                    
                    network = nms.getNetworkByName(((String)obj).substring(6));
                    
                    if(network != null)
                        map.put((String)obj, network);
                    
                    return network;
                }  else if(((String)obj).equals("nms")) {
                	map.put((String)obj, nms);
                	return nms;
                } else if(((String)obj).equals("stdOut")) {
                	map.put((String)obj, System.out);
                	return System.out;
                } else if(((String)obj).equals("stdErr")) {
                	map.put((String)obj, System.err);
                	return System.err;
                }                 
                
            }
            
            return ret;
        }

        public Object put(String s, Object obj) {
            return map.put(s, obj);
        }

        public void putAll(Map<? extends String, ? extends Object> toMerge) {                        
            map.putAll(toMerge);            
        }

        public Object remove(Object obj) {
            return map.remove(obj);
        }

        public void clear() {
            map.clear();            
        }

        public boolean containsValue(Object obj) {
            
            boolean ret;
            
            ret = map.containsValue(obj);
            
            if((ret == false) && (obj instanceof Network)) {                
                
                map.put("nmsNet" + ((Network)obj).getName(), obj);
                
                return true;
            }
                        
            return ret;
        }

        public Set<java.util.Map.Entry<String, Object>> entrySet() {            
            return map.entrySet();
        }

        public boolean isEmpty() {
            return map.isEmpty();
        }

        public Set<String> keySet() {
            return map.keySet();
        }

        public int size() {
            return map.size();
        }

        public Collection<Object> values() {
            return map.values();
        }

        private Hashtable<String, Object>   map;
        private NMS                         nms;
     
    }
    
    public void importClass(String qualifiedClassName) throws Exception {

        if(engineName.equals(_ENGINE_NAME_JS)) {
            engine.eval("importClass(" + qualifiedClassName + ");");
        } else if(engineName.equals(_ENGINE_NAME_JRUBY)) {
            engine.eval("import " + qualifiedClassName + "\n");            
        }          
    	
    }

    private final String _ENGINE_NAME_JS		= "js";
    private final String _ENGINE_NAME_JRUBY		= "jruby";
    
    private ScriptEngine    engine;
    private String 			engineName;
    private Namespace		nameSpace;
    
    public static void main(String args[]){
    	try {
			ScriptExecEnv exe=new ScriptExecEnv("jruby");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
}
