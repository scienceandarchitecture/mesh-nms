/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Branding.java
 * Comments : 
 * Created  : Mar 10, 2005
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  3  |Mar 09, 2007 | new functions exposed                           | Abhijit|
 * ---------------------------------------------------------------------------------
 * |  2  |Jan 30, 2006 | Restructuring                                   | Mithil |
 * ---------------------------------------------------------------------------------
 * |  1  |Apr 26, 2005 | Brand Generator DLL renamed to BLoader          | Bindu  |
 * --------------------------------------------------------------------------------
 * |  0  |Mar 10, 2005 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui;

import java.io.ByteArrayInputStream;

import org.eclipse.swt.graphics.Image;

public class Branding {
	
	public static final byte IMAGE_TYPE_ICON			= 1;
	public static final byte IMAGE_TYPE_MAIN			= 2;
	public static final byte IMAGE_TYPE_DIALOG			= 3;
	public static final byte IMAGE_TYPE_ABOUT			= 4;
	
	public static final byte TITLE_TYPE_MAIN			= 1;
	
	private static boolean isMeshDyanmics;
	private static String 	brandTitle;
	private static Image	iconImage;
	private static Image	mainImage;
	private static Image	dialogImage;
	private static Image	aboutImage;
	
	
	static {
		
		isMeshDyanmics = false;
		System.loadLibrary("BLoader");
		brandTitle = getBrandTitle(TITLE_TYPE_MAIN);
		if(brandTitle.toLowerCase().startsWith("meshdynamics") == true)
			isMeshDyanmics = true;
		
		byte[] 					imageByteArray 	= getBrandImage(IMAGE_TYPE_ICON);
		ByteArrayInputStream 	bais 			= new ByteArrayInputStream(imageByteArray);
		iconImage 								= new Image(null,  bais);

		imageByteArray 	= getBrandImage(IMAGE_TYPE_DIALOG);
		bais 			= new ByteArrayInputStream(imageByteArray);
		dialogImage		= new Image(null,  bais);

		imageByteArray 	= getBrandImage(IMAGE_TYPE_MAIN);
		bais 			= new ByteArrayInputStream(imageByteArray);
		mainImage		= new Image(null,  bais);

		imageByteArray 	= getBrandImage(IMAGE_TYPE_ABOUT);
		bais 			= new ByteArrayInputStream(imageByteArray);
		aboutImage		= new Image(null,  bais);
		
	}
	
	private static native String getBrandTitle(byte type);
	private static native byte[] getBrandImage(byte type);

	public static boolean isMeshDyanmics() {
		return isMeshDyanmics;
	}

	public static Image getAboutImage() {
		return aboutImage;
	}

	public static String getBrandTitle() {
		return brandTitle;
	}

	public static Image getDialogImage() {
		return dialogImage;
	}

	public static Image getIconImage() {
		return iconImage;
	}

	public static Image getMainImage() {
		return mainImage;
	}
	

}
