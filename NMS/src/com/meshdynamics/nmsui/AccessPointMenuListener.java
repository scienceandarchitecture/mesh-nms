/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : AccessPointMenuListener 
 * Comments : Menu Handlers for accesspoint Menu.
 * Created  : Dec 05, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date	       |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  0  |Dec 05, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 ********************************************************************************/
package com.meshdynamics.nmsui;

import java.io.FileOutputStream;
import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.INetworkConfiguration;
import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.configuration.runtimeconfiguration.IRuntimeConfigStatus;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.dialogs.InputLatLngDlg;
import com.meshdynamics.nmsui.dialogs.MeshCommandDialog;
import com.meshdynamics.nmsui.dialogs.OfflineViewNodeImageDlg;
import com.meshdynamics.nmsui.dialogs.advancedsettings.AdvancedSettingsConfigDlg;
import com.meshdynamics.nmsui.dialogs.iperf.PerfMonitorDialog;
import com.meshdynamics.nmsui.dialogs.settings.ConfigurationDialog;
import com.meshdynamics.nmsui.maps.GeoPosition;
import com.meshdynamics.nmsui.mesh.ExtendedMenuInfo;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.nmsui.mesh.views.AccessPointUI;
import com.meshdynamics.nmsui.mesh.views.offlinemapview.OfflineMapView;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

/**
 * @author Abhijit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AccessPointMenuListener extends SelectionAdapter {
	
	private MeshViewerUI	meshViewerUI;
	private MacAddress		dsMacAddress;

	// Declarations for items in Accesspoint Popup menu..
	
	/**
	 * 
	 */
	public AccessPointMenuListener(MeshViewerUI meshViewerUI) {
		super();
		this.meshViewerUI 	= meshViewerUI;
		dsMacAddress		= null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	public void widgetSelected(SelectionEvent arg0) {
		
		try {
			Item src = (Item)arg0.getSource();
			int s =  Integer.parseInt(src.getData().toString());
		    switch(s){
			case AccessPointMenu.APPOPUP_ITEM_DELETE :
				deleteAccessPoint();
				break;
				
			case AccessPointMenu.APPOPUP_ITEM_SETTINGS_GENERAL:
				configureAccessPoint();
				break;
				
			case AccessPointMenu.APPOPUP_ITEM_SETTINGS_ADVANCED :
				configureAdvancedSettings();
				break;
				
			case AccessPointMenu.APPOPUP_ITEM_SETTINGS_RESTORE_DEFAULTS :
				restoreApDefaultSettings();
				break;

			case AccessPointMenu.APPOPUP_ITEM_SETTINGS_EXPORT_CONFIG_SCRIPT :
			    exportConfigScript();
			    break;
			    
			case AccessPointMenu.APPOPUP_ITEM_SETTINGS_EXPORT_RF_CONFIG_SCRIPT:
			    exportRFConfigScript();
			    break;
			    
			case AccessPointMenu.APPOPUP_ITEM_SETTINGS_IMPORT_CONFIG_SCRIPT :
			    importConfigScript();
			    break;
			    
			case AccessPointMenu.APPOPUP_ITEM_REBOOT :
				rebootAccessPoint();
				break;

			case AccessPointMenu.APPOPUP_ITEM_TOOLS_PERFORMANCE_TEST :
			    showPerformanceTestDialog(); 
				break;

			case AccessPointMenu.APPOPUP_ITEM_TOOLS_MESHCOMMAND :
				showMeshCommandDialog(); 
				break;
				
			case AccessPointMenu.APPOPUP_ITEM_TOOLS_RF_SPACEINFO :
			    showRFSPaceInfoDlg();
				break;
				
			case AccessPointMenu.APPOPUP_ITEM_TOOLS_NODE_STATUS_WEB_PAGE :
			    showNodeStatusWebPage();
				break;
			case AccessPointMenu.APPOPUP_ITEM_TOOLS_UPDATE_GEOPOSITION :
			    setLatLngFromMap();
			    break;
			case AccessPointMenu.APPOPUP_ITEM_SET_OFFLINE_IMAGES :
			    setOfflineMapImages();
			    break;
			
			default:
			    if(s >= AccessPointMenu.EXTENDED_MENU) {
			        extendedMenuHandler(src);
			    }else {
			        System.err.println("Invalid accesspoint menu id called");
			    }
		    
		    }
		} catch (Exception e) {
		    e.printStackTrace();
		}
	}
	
	private void setOfflineMapImages() {
		OfflineViewNodeImageDlg dlg = new OfflineViewNodeImageDlg();
		dlg.show();
		if(dlg.getStatus() != SWT.OK)
			return;
		
		AccessPointUI apUI = meshViewerUI.getSelectedMeshNetworkUI().getAccesspointUI(dsMacAddress.toString());
		apUI.setApStateImage(AccessPointUI.STATE_DEAD, dlg.getOffImgPath());
		apUI.setApStateImage(AccessPointUI.STATE_HB_MISSED, dlg.getHbMissImgPath());
		apUI.setApStateImage(AccessPointUI.STATE_RUNNING, dlg.getOnImgPath());
		apUI.setApStateImage(AccessPointUI.STATE_SCANNING, dlg.getScanImgPath());
	}
	
    private void exportRFConfigScript() {
        
        MeshNetworkUI   meshNetworkUI   = meshViewerUI.getSelectedMeshNetworkUI();
        String          configScript    = meshViewerUI.exportRFConfigScript(meshNetworkUI.getNwName(), dsMacAddress.toString());
        
        if(configScript == null || configScript == "") {
            MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
            msg.setMessage("RF Config script could not be exported.");
            msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
            msg.open(); 
            return;
        }
        
        String fileName = MFile.getTemplatePath() + "/" + dsMacAddress.toString().replaceAll(":", "_") + "-RF.mns";
        boolean success = true;
        try {
            FileOutputStream fileOut = new FileOutputStream(fileName);
            fileOut.write(configScript.getBytes());
            fileOut.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            success = false;
        }

        if(success == true) {
            MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_INFORMATION);
            msg.setMessage("RF Config script exported to " + fileName);
            msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
            msg.open(); 
            return;
        }
        
        MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
        msg.setMessage("RF Config script could not be exported.");
        msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
        msg.open(); 
    }	

	private void exportConfigScript() {
		
		MeshNetworkUI 	meshNetworkUI 	= meshViewerUI.getSelectedMeshNetworkUI();
		String 			configScript 	= meshViewerUI.exportConfigScript(meshNetworkUI.getNwName(), dsMacAddress.toString());
		if(configScript == null || configScript == "") {
        	MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
			msg.setMessage("Config script could not be exported.");
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.open(); 
			return;
		}
		
		String fileName = MFile.getTemplatePath() + "/" + dsMacAddress.toString().replaceAll(":", "_") + ".mns";
		boolean success = true;
		try {
			FileOutputStream fileOut = new FileOutputStream(fileName);
			fileOut.write(configScript.getBytes());
			fileOut.close();
 		} catch (Exception e) {
			System.out.println(e.getMessage());
			success = false;
		}
		
		if(success == true) {
        	MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_INFORMATION);
			msg.setMessage("Config script exported to " + fileName);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.open(); 
			return;
		}
		
    	MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
		msg.setMessage("Config script could not be exported.");
		msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		msg.open(); 
    }

	private void importConfigScript() {
		
		FileDialog fileDialog = new FileDialog(new Shell());
		fileDialog.setFilterPath(MFile.getTemplatePath());
		fileDialog.setFilterExtensions(new String[]{"*.mns"});
		fileDialog.setText("Select configuration script");
		String filePath = fileDialog.open(); 
		if((filePath == null) || (filePath.equalsIgnoreCase("") == true)) {
			return;
		}
		
		String[] dsAddresses = {dsMacAddress.toString()};
		
		MeshNetworkUI 	meshNetworkUI 	= meshViewerUI.getSelectedMeshNetworkUI();
		boolean[] 		ret 			= meshViewerUI.importConfigScript(filePath, meshNetworkUI.getNwName(), dsAddresses);
		
		if(ret[0] == true) {
			MeshNetwork 	meshNetwork 		= meshNetworkUI.getMeshNetwork();
		    AccessPoint 	ap 					= meshNetwork.getAccessPoint(dsMacAddress);
		    ap.isRebootRequired();
		    meshViewerUI.showMessage(ap,Mesh. MACRO_IMPORT_UPDATE,Mesh.STATUS_REBOOTREQD,"" );
        	MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_INFORMATION);
			msg.setMessage("Config script imported.");
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.open(); 
			return;
		}
		
    	MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
		msg.setMessage("Config script could not be imported.");
		msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		msg.open(); 
    }
	
    private void deleteAccessPoint() {
		MeshNetworkUI 	meshNetworkUI 	= meshViewerUI.getSelectedMeshNetworkUI();
		meshNetworkUI.deleteAccesspoint(dsMacAddress.toString());
    }
	
    private void configureAccessPoint() {
    	
        MeshNetworkUI 	meshNetworkUI 		= meshViewerUI.getSelectedMeshNetworkUI();
        MeshNetwork 	meshNetwork 		= meshNetworkUI.getMeshNetwork();
        AccessPoint 	ap 					= meshNetwork.getAccessPoint(dsMacAddress);

        if(ap == null) {
        	return;
        }
        
       	if(ap.canConfigure() == false) {
        	MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_INFORMATION);
			msg.setMessage("Insufficient Configuration.");
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.open(); 
			return;
		}

        IConfiguration configuration = ap.getConfiguration();
        
        if(configuration == null)
        	return;

		IRuntimeConfiguration 	runtimeConfiguration	= ap.getRuntimeApConfiguration();
        IRuntimeConfigStatus  	runtimeStatus			= ap.getRuntimeConfigStatus();
        IVersionInfo 			versionInfo				= ap.getVersionInfo();
        
        ConfigurationDialog cfgDlg	= new ConfigurationDialog( ConfigurationDialog.CONFIG_MODE_NODE,
        		configuration, meshNetwork, versionInfo);
        cfgDlg.setTitle("Node Configuration - "+configuration.getDSMacAddress());
        
        if(runtimeConfiguration.isRebootRequired()  == true &&
        	runtimeStatus.isRfUpdateSent()     	    == true) {
        	cfgDlg.setRfConfigChanged(true);
        }
        if(runtimeConfiguration.isRebootRequired()  == true &&
            runtimeStatus.isCountryCodeChanged()    == true) {
           	cfgDlg.setCountryConfigChanged(true);
        }
             
		cfgDlg.show();
		
		int result = cfgDlg.getStatus();
		if(result != SWT.OK) {
			return;
		}
		
		IConfigStatusHandler statusHandler	= ap.getUpdateStatusHandler();
		statusHandler.clearStatus();
		IConfiguration	configDlgConfig		= cfgDlg.getConfiguration();
		if(configDlgConfig == null) {
			return;
		}
		
		configuration.compare(configDlgConfig,statusHandler);
		MessageBox msg = new MessageBox(new Shell(SWT.CLOSE), SWT.YES|SWT.NO|SWT.ICON_QUESTION);
		
		if(statusHandler.isAnyPacketChanged() == true){
			configDlgConfig.copyTo(configuration);
			ap.updateChangedConfiguration();
		} else {
			msg.setMessage("AP Configuration has not been changed."+ SWT.LF+
							"Do you want to update node settings? ");
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			result	=	msg.open();
			
			if(result == SWT.YES) { 
				configDlgConfig.copyTo(configuration);
				ap.updateAllConfiguration();
			}
		}
    }
	
    private void configureAdvancedSettings() {
    	
        MeshNetworkUI 	meshNetworkUI 		= meshViewerUI.getSelectedMeshNetworkUI();
        MeshNetwork 	meshNetwork 		= meshNetworkUI.getMeshNetwork();
        AccessPoint 	ap 					= meshNetwork.getAccessPoint(dsMacAddress);
        
        if(ap == null){
        	return;
        }
        
        IVersionInfo 	versionInfo			= ap.getVersionInfo();
        
        if(ap.canConfigure() == false){
        
        	MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_INFORMATION);
			msg.setMessage("Insufficient Configuration.");
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.open(); 
			return;
		}

        IConfiguration configuration = ap.getConfiguration();
        if(configuration == null) {
        	return;
        }
      
        AdvancedSettingsConfigDlg cfgDlg	= new AdvancedSettingsConfigDlg(ConfigurationDialog.CONFIG_MODE_NODE, configuration, versionInfo);
        cfgDlg.setTitle("Advanced Configuration - "+configuration.getDSMacAddress());
        cfgDlg.show();
        
        int result = cfgDlg.getStatus();
        if(result != SWT.OK) {
        	return;
        }
        
        IConfiguration	configDlgConfig		= cfgDlg.getConfiguration();
		if(configDlgConfig == null) {
			return;
		}
		
		IConfigStatusHandler statusHandler	= ap.getUpdateStatusHandler();
		statusHandler.clearStatus();
	
		configuration.compare(configDlgConfig,statusHandler);

		
		MessageBox msg = new MessageBox(new Shell(SWT.CLOSE), SWT.YES|SWT.NO|SWT.ICON_QUESTION);
		
		if(statusHandler.isAnyPacketChanged() == true){
			configDlgConfig.copyTo(configuration);
			ap.updateChangedConfiguration();
		} else {
			msg.setMessage("AP Advanced Configuration has not been changed."+ SWT.LF+
							"Do you want to update advanced settings? ");
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			result	=	msg.open();
			
			if(result == SWT.YES) { 
				configDlgConfig.copyTo(configuration);
				ap.updateAllConfiguration();
			}
		}
	 }	
   
    private void restoreApDefaultSettings() {
		
		MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.YES|SWT.NO|SWT.ICON_QUESTION);
		msg.setMessage("Do you want to Restore Default Settings ?");
		msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		int ans = msg.open();
		if(ans  != SWT.YES){
			return;
		}
		
		MeshNetworkUI 	meshNetworkUI	= meshViewerUI.getSelectedMeshNetworkUI();
	    MeshNetwork 	meshNetwork 	= meshNetworkUI.getMeshNetwork();	        
	    AccessPoint 	ap 				= meshNetwork.getAccessPoint(dsMacAddress);
	    
	    if(ap == null) {
			return;	    
	    }
	    
	    meshViewerUI.showMessage(ap, Mesh.MACRO_RESTORE_FACTORY, "", "");
	       	meshNetwork.restoreDefaultSettings(dsMacAddress); 
	       	
		}

    private void rebootAccessPoint() {
	
		MeshNetworkUI 	meshNetworkUI 	= meshViewerUI.getSelectedMeshNetworkUI();
	    MeshNetwork 	meshNetwork 	= meshNetworkUI.getMeshNetwork();
	    AccessPoint		ap 				= meshNetwork.getAccessPoint(dsMacAddress);
	    int 			canReboot 		= meshNetwork.canRebootAp(dsMacAddress);
	    boolean			reboot			= false;
	    
	    if(canReboot == 0) {
			MessageBox msgBox =  new MessageBox(new Shell(SWT.CLOSE),SWT.YES|SWT.NO|SWT.ICON_WARNING);
			String msg = "Do you want to reboot Node ''" + dsMacAddress.toString() + "'' ? ";
			msgBox.setMessage(msg);
			msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			if(msgBox.open() == SWT.YES) 
				reboot = true;
	    	
	    } else if(canReboot == 1) {
			MessageBox msgBox =  new MessageBox(new Shell(SWT.CLOSE),SWT.YES|SWT.NO|SWT.ICON_WARNING);
			String msg = "Node has active children. Do you still want  to reboot Node ''" + dsMacAddress.toString() + "'' ? ";
			msgBox.setMessage(msg);
			msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			if(msgBox.open() == SWT.YES) 
				reboot = true;
	    }
	    
	    if(reboot == true){
	    	meshViewerUI.showMessage(ap,Mesh.MACRO_REBOOT_NODES,"","");
	    	meshNetwork.rebootAccesspoint(dsMacAddress);
	    }
	    
	}
	
	private void showPerformanceTestDialog(){
	    
	    MeshNetworkUI 	meshNetworkUI	= meshViewerUI.getSelectedMeshNetworkUI();
	    MeshNetwork 	meshNetwork 	= meshNetworkUI.getMeshNetwork();
	    
	    AccessPoint 	ap	= meshNetwork.getAccessPoint(dsMacAddress);
	    if(ap == null){
	    	return;
	    }
	    
	    IVersionInfo versionInfo = ap.getVersionInfo();
	    
	    if(versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,IVersionInfo.MINOR_VERSION_4,IVersionInfo.VARIANT_VERSION_98) == false) {
			MessageBox msg = new MessageBox(new Shell(), SWT.OK|SWT.ICON_INFORMATION);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Performance Monitoring not supported!");
			msg.open();
	    	return;
	
	    }
	    
	    IpAddress addr = ap.getIpAddress();
	    if(addr == null) {
	    	System.out.println("ipconfig not found for performance monitor dialog.");
	    	return;
	    }
	    
	    PerfMonitorDialog perfDlg = new PerfMonitorDialog(meshViewerUI, ap);
	    perfDlg.show();  
	
	}
	
	private void showMeshCommandDialog(){
	    
	    MeshNetworkUI 	meshNetworkUI	= meshViewerUI.getSelectedMeshNetworkUI();
	    MeshNetwork 	meshNetwork 	= meshNetworkUI.getMeshNetwork();	        
	
	    AccessPoint	ap = meshNetwork.getAccessPoint(dsMacAddress);
	    if(ap == null){
	    	return;
	    }
	    
	    MeshCommandDialog meshCmd = new MeshCommandDialog(ap);
	    meshCmd.setTitle("Mesh Command for " + dsMacAddress.toString());
	    meshCmd.show();  
	   
	}
	
	private void showRFSPaceInfoDlg() {
		
	    MeshNetwork meshNetwork 	= meshViewerUI.getSelectedMeshNetworkUI().getMeshNetwork();
	    if(meshNetwork == null)
	    	return;
	
	    AccessPoint ap 				= meshNetwork.getAccessPoint(dsMacAddress);
	    if(ap == null)
	    	return;
	
	    IVersionInfo versionInfo = ap.getVersionInfo();
	    
	    if(versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,IVersionInfo.MINOR_VERSION_4,IVersionInfo.VARIANT_VERSION_94) == false) {
			MessageBox msg = new MessageBox(new Shell(), SWT.OK|SWT.ICON_INFORMATION);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("RF Space Information not supported!");
			msg.open();
	    	return;
	    }
	
	    IConfiguration iConfig 		= ap.getConfiguration();
	    if(iConfig == null)
	    	return;
	    
	    meshViewerUI.showDLSaturationInfo(meshNetwork,iConfig);
	    
	}
	    
	private void showNodeStatusWebPage(){
		
	    MeshNetwork 			meshnetwork 	= meshViewerUI.getSelectedMeshNetworkUI().getMeshNetwork();
	    AccessPoint 			ap 				= meshnetwork.getAccessPoint(dsMacAddress);
	    IConfiguration 			iConfig 		= ap.getConfiguration();
	    INetworkConfiguration	nwConfig		= iConfig.getNetworkConfiguration();
	    String 					ipAddress 		= nwConfig.getIpAddress().toString();
	    try{
			  Runtime.getRuntime().exec("cmd /c start iexplore.exe http://"+ipAddress+":8080");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
    private void extendedMenuHandler(Item srcItem) {
        
        ExtendedMenuInfo extMenuInfo   = (ExtendedMenuInfo)srcItem.getData(srcItem.getText());
        
        /*get command id and handler*/
        if(extMenuInfo.handler != null){
            extMenuInfo.handler.onCommand(extMenuInfo, meshViewerUI.getSelectedMeshNetworkUI().getNwName(), dsMacAddress.toString());
        }
    }
	
	/**
	 * @param dsMacAddress The dsMacAddress to set.
	 */
	public void setDsMacAddress(MacAddress dsMacAddress) {
		this.dsMacAddress = dsMacAddress;
	}
	
	private void setLatLngFromMap () {
	    
	    MeshNetworkUI  meshnetworkUI 	= meshViewerUI.getSelectedMeshNetworkUI();
	    MeshNetwork    meshnetwork		= meshnetworkUI.getMeshNetwork();
	    
	    if(meshnetworkUI.getCurrentViewType() != MeshNetworkUI.VIEW_OFFLINE_MAP) {
	        return;
	    }

        AccessPoint 			ap = meshnetwork.getAccessPoint(dsMacAddress);
        
        if(ap.canConfigure() == false) {
        	MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_INFORMATION);
			msg.setMessage("Insufficient Configuration. Cannot Update.");
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.open(); 
			return;
		} 
	    
    	OfflineMapView 	currentView = (OfflineMapView)meshnetworkUI.getCurrentView();
        GeoPosition 	pos 		=  currentView.getNodeGeoPosition(dsMacAddress);
        InputLatLngDlg 	inputBox 	= new InputLatLngDlg(String.valueOf(pos.getLatitude()),
                								String.valueOf(pos.getLongitude()));
        if(inputBox.open() == false)
        	return;
        
	    IConfiguration 			iConfig = ap.getConfiguration();
	    
	    DecimalFormat df 	= new DecimalFormat("##000.000000");
	    double lat 			= Double.parseDouble(inputBox.getLatitude());
	    double lng 			= Double.parseDouble(inputBox.getLongitude());
	    
	    iConfig.setLatitude(df.format(lat));
	    iConfig.setLongitude(df.format(lng));
	    meshnetwork.updateConfiguration(dsMacAddress.toString(), PacketFactory.AP_CONFIGURATION_INFO);
	
	}
	
}
