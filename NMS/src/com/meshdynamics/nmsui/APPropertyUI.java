/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : APProperty.java
 * Comments : Properties view Handler class.
 * Created  : Sep 28, 2004
 * Author   : Amit Chavan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 * --------------------------------------------------------------------------------
 * | 55  |Aug 31,2007  | Cpu usage, free memory added    	      	 	 |Abhishek| 
 * --------------------------------------------------------------------------------
 * | 54  |Aug 24,2007  | Parent node information added          	 	 |Abhishek| 
 * --------------------------------------------------------------------------------
 * | 53  |May 03, 2007 | References to imcp packets removed				 | Abhijit |
 * --------------------------------------------------------------------------------
 * | 52  |Apr 13, 2007 | Perspective settings changes					 |Prachiti |
 * --------------------------------------------------------------------------------
 * | 51  |Mar 08, 2007 | Voltage unavailable displayed for value 0		 | Abhijit |
 * --------------------------------------------------------------------------------
 * | 50  |Feb 20, 2007 | FIPS enabled flag changes(Hb detection removed) | Abhishek|
 * --------------------------------------------------------------------------------
 * | 49  |Feb 06, 2007 | FIPS enabled flag changes                  	 | Abhishek|
 * --------------------------------------------------------------------------------
 * | 48  |Jan 30, 2007 | FIPS enabled flag added in flags           	 | Abhishek|
 * --------------------------------------------------------------------------------
 * | 47  |Jan 24, 2007 | Prints removed                		        	 | Bindu   |
 * --------------------------------------------------------------------------------
 * | 46  |Jan 24, 2007 | STA Fix                     		        	 | Bindu   |
 * --------------------------------------------------------------------------------
 * | 45  |Jan 15, 2007 | Flags are added                     		     | Abhishek|
 * --------------------------------------------------------------------------------
 * | 44  |Dec 28, 2006 | column width saved to file,updateAPProp restruct| Abhijit |
 * --------------------------------------------------------------------------------
 * | 43  |Nov 07, 2006 | Miscellenous Changes & redesign     		     | Abhishek|
 * --------------------------------------------------------------------------------
 * | 42  |Nov 07, 2006 | Displaying Information of deadnodes		     | Abhishek|
 * --------------------------------------------------------------------------------
 * | 41  |May 12, 2006 | parent rate Property display's 100 for root     | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 40  |Apr 10, 2006 | Properties displayed for nodes in sel network   | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 39  |Feb 17, 2006 | Hidden ESSID rmvd from display				 	 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 38  |Feb 15, 2006 | All Flags Removed Rboot Reqd put in general     | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 37  |Feb 15, 2006 | Known AP's Signal not Displayed properly		 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 36  |Feb 14, 2006 | Parent Signal not Displayed properly			 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 35  |Feb 14, 2006 | Hidden ESSID errors in displaying				 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 35  |Feb 13, 2006 | Hidden ESSID Implemented						 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 34  |Feb 13, 2006 | Reject Clients removed						 	 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 33  |Feb 08, 2006 | Reg Domain removed							 	 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 32  |Feb 03, 2006 | DIsplay in Property UI		 					 | Mithil |
 * --------------------------------------------------------------------------------
 * | 31  |Feb 02,2006  | GDI Objects not cleared				       	 | Mithil |
 * -------------------------------------------------------------------------------
 * | 30  | Jan 04,2006 | '--' for Root for ulink rate & prt sig strength | Mithil |
 * --------------------------------------------------------------------------------
 * | 29  | Jan 03,2006 | Clear All Items from table after stopping viewer| Mithil |
 * --------------------------------------------------------------------------------
 * | 28  | Nov 30,2005 | Ack timeout value * 10 added        			 | Amit   |
 * --------------------------------------------------------------------------------
 * | 27  | Nov 25,2005 | Ack timeout items added 1027        			 | Mithil |
 * -------------------------------------------------------------------------------- 
 * | 26  | Nov 15,2005 | Reg Domain and Country code items added  1015   | Amit   |
 * --------------------------------------------------------------------------------
 * | 25  | Oct 24,2005 | Cleanup removing unused variables               | Mithil |
 * --------------------------------------------------------------------------------
 * | 24  | Oct 13,2005 | Default vlan values added                       |Amit    |
 * --------------------------------------------------------------------------------
 * | 23  | Oct 7,2005  | Unwanted Vlan properties removed from table     | Amit   |
 * --------------------------------------------------------------------------------
 * | 22  | Oct 7,2005  | Label for Interface info changed to downlinks   |Amit    |
 * --------------------------------------------------------------------------------
 * |  21 |Sep 27, 2005 | default vlan values disabled 					 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  20 |Sep 26, 2005 | temp,lasi,hbmc,mah disabled 					 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  19 |Sep 21, 2005 | Hardware info labels modified 					 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  18 |Sep 20, 2005 | Properties tabs merged added group selection    |Abhijit |
 * |	 |			   | and status tabs								 |		  |							 
 * -------------------------------------------------------------------------------- 
 * |  17 |Sep 8, 2005  | Sta address shows assoc time and macaddress now |Abhijit |
 * --------------------------------------------------------------------------------
 * |  16 |Sep 8, 2005  | Label showing WM changed to downlink 			 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  15 |Sep 8, 2005  | UPdate for bug no 57          					 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  14 |Aug 25, 2005 | STA property for "not available" fixed          |Prachiti|
 * --------------------------------------------------------------------------------
 * |  14 |May 12, 2005 | Config change alert commented  		         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  13 |May 10, 2005 | Latitude & Longitude bug fixes  		         | Anand  |
 * --------------------------------------------------------------------------------
 * |  12 |May 06, 2005 | Added Latitude & Longitude fields 		         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  11 |May 02, 2005 | New look UI changes    		   		         | Anand  |
 * --------------------------------------------------------------------------------
 * |  10 |May 02, 2005 | bug - Checking for Vlans fixed    		         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  9  |Apr 27, 2005 | New look tabs & ui changes      		         | Anand  |
 * --------------------------------------------------------------------------------
 * |  8  |Apr 19, 2005 | Board Temperature Property Added		         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  7  |Mar 07, 2005 | DEF_DOT11E_ENABLED Added		                 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  6  |Mar 07, 2005 | Changed dot11i to dot11e		                 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  5  |Mar 04, 2005 | VLANInfo & VLANSubInfo & Dyn Prop changes 	     | Bindu  |
 * --------------------------------------------------------------------------------
 * |  4  |Mar 02, 2005 | UI Display Changes				                 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  3  |Mar 01, 2005 | Changed dot11e to dot11i		                 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  2  |Mar 01, 2005 | Vlan & Interface Sec Info added                 | Anand  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 11, 2005 | Change Detection-PropertyUI changes             | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Amit   |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui;

import java.util.Enumeration;
import java.util.Hashtable;

import com.meshdynamics.util.MacAddress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;
import com.meshdynamics.meshviewer.configuration.INetworkConfiguration;
import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.IStaConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.meshviewer.configuration.KnownAp;
import com.meshdynamics.meshviewer.configuration.KnownAp.KnownApInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.InterfaceConfigInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.SecurityInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.HardwareInfo;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;
import com.meshdynamics.meshviewer.mesh.ap.HardwareInfo.InterfaceData;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabColumnInfo;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabInfo;
import com.meshdynamics.nmsui.PerspectiveSettings.ITableInfo;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.nmsui.util.MeshViewerGDIObjects;
import com.meshdynamics.util.IpAddress;

public class APPropertyUI  implements IAPPropListener {
    
	private static final int 	DEFAULT_TAB_COUNT		= 1;
	
	private int 	columnWidth[];
	
    private class PropData {
        public String	label;
        public int 		mainProperty;
        public int 		subProperty;
        
        public PropData(String lbs, int mainProp, int subProp) {
            label 			= lbs;
            mainProperty	= mainProp;
            subProperty		= subProp;
        }
    }
    
    private class PropArray extends PropData {
       
    	public PropData[] props;
        
        public PropArray(String lbs, int mainProp, PropData[] props) {
            super(lbs, mainProp, IAPPropListener.MASK_ALL);
            this.props = props;
        }
    }
    
    private class PropUI {
        public String 		label		= "";
    	public Tree			tableTree	= null;
    	public Canvas      	canvas		= null;
    	public CTabItem     tabItem		= null;
    	public PropData[]	properties	= null;
    }
    
    private static final int 	UNTAGGED 			= -1;
	private static final String DEFAULT_VALUE		= "Not Available";
	
    private Display						display;
	private CTabFolder  				propertyTab;
	private TreeColumn 					propertyColumn,valueColumn;
	private PropUI[]					propUIs;
	private TreeItem[][]				propItems;
	private Color						blackColor;
	private MeshViewerUI				meshViewerUI;
	private AccessPoint					currentSelectedAp;
	private Hashtable<String, PropUI>	extPropSections;
	
	public APPropertyUI(Canvas canvas,MeshViewerUI mvUI) {
		currentSelectedAp = null;
		initPropertyTree();
		meshViewerUI = mvUI;
		createControls(canvas);		
		
		blackColor = Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);
		extPropSections = new Hashtable<String, PropUI>();
	}

	private void initPropertyTree() {
	    int i = 0;
	    
	    /**
	     * 1) Main Section or Header Should contain Array of Prop Data
	     * 2) Saperate Array for Each Tabs
	     * 3) Main Array for all Tabs
	     * 4) Direct Prop should Have PropData only
	     */
	    
	    /**
	     * Gen Tab
	     */
	    PropData[] properties = new PropData[Mesh.PROP_TOTAL_COUNT];
	    
	    properties[i++] = new PropData("    Name", 					Mesh.PROP_GENERAL, Mesh.GENERAL_PROP_NAME);
	    properties[i++] = new PropData("    Description",			Mesh.PROP_GENERAL, Mesh.GENERAL_PROP_DESC);
	    properties[i++] = new PropData("    Default Latitude", 		Mesh.PROP_GENERAL, Mesh.GENERAL_PROP_LATITUDE);
	    properties[i++] = new PropData("    Default Longitude", 	Mesh.PROP_GENERAL, Mesh.GENERAL_PROP_LONGITUDE);
	    properties[i++] = new PropData("    Country Code", 			Mesh.PROP_GENERAL, Mesh.GENERAL_PROP_COUNTRY_CODE);
	    
	    properties[i++] = new PropArray("Runtime Flags", 	Mesh.PROP_RUNTIME_FLAG,
	    	    new PropData[] {
    			new PropData("Reboot Required", 				Mesh.PROP_RUNTIME_FLAG, Mesh.FLAG_PROP_REBOOT_REQUIRED),
    			new PropData("Radar Detected"	  ,				Mesh.PROP_RUNTIME_FLAG, Mesh.FLAG_PROP_RADAR_DETECT),
    			new PropData("Location"	  ,						Mesh.PROP_RUNTIME_FLAG, Mesh.FLAG_PROP_LOCATION),
	    });
	    
	    properties[i++] = new PropArray("Options", 	Mesh.PROP_OPTIONS,
	    	    new PropData[] {
    			new PropData("FIPS 140-2 Compliant",			Mesh.PROP_OPTIONS, Mesh.OPTIONS_PROP_FIPS_ENABLED),
    			new PropData("IGMP",							Mesh.PROP_OPTIONS, Mesh.OPTIONS_PROP_IGMP_SNOOPING),
    			new PropData("P3M(tm)",							Mesh.PROP_OPTIONS, Mesh.OPTIONS_PROP_P3M),
    			new PropData("DHCP",							Mesh.PROP_OPTIONS, Mesh.OPTIONS_PROP_DHCP_ENABLED),
    			new PropData("PBV(tm)",							Mesh.PROP_OPTIONS, Mesh.OPTIONS_PROP_PBV)
	    });
	    
	    properties[i++] = new PropArray("Hardware Info", 	Mesh.PROP_HWINFO,
            new PropData[] {
    			new PropData("Firmware Version", 			Mesh.PROP_HWINFO, Mesh.HWINFO_PROP_FW_VERSION),
    			new PropData("Model Info", 					Mesh.PROP_HWINFO, Mesh.HWINFO_PROP_MODELINFO),
    			new PropData("Unit Address", 				Mesh.PROP_HWINFO, Mesh.HWINFO_UNIT_ADDRESS),
            	new PropData("Mac Addresses", 				Mesh.PROP_HWINFO, Mesh.HWINFO_PROP_LINKS)
	    });
	    
	    properties[i++] = new PropArray("IP-Settings", 		Mesh.PROP_IPCONF, 
	        new PropData[] {	            
	    		new PropData("HostName", 					Mesh.PROP_IPCONF, Mesh.IPCONF_PROP_HOSTNAME),
				new PropData("IP Address", 					Mesh.PROP_IPCONF, Mesh.IPCONF_PROP_IPADDR),
				new PropData("SubNet Mask", 				Mesh.PROP_IPCONF, Mesh.IPCONF_PROP_NETMASK),
				new PropData("Gateway", 					Mesh.PROP_IPCONF, Mesh.IPCONF_PROP_GATEWAY)
	    });
	    
	    properties[i++] = new PropArray("Information", 		Mesh.PROP_HEARTBEAT, 
	    	new PropData[] {
	    		new PropData("Heartbeat Count", 			Mesh.PROP_HEARTBEAT, Mesh.HEARTBEAT_PROP_SQNR),
				new PropData("Parent Signal Strength", 		Mesh.PROP_HEARTBEAT, Mesh.HEARTBEAT_PROP_SIGNALSTRNTH),
				new PropData("Parent Node", 				Mesh.PROP_HEARTBEAT, Mesh.HEARTBEAT_PROP_PARENT_NODE),
				new PropData("Neighbour's", 				Mesh.PROP_HEARTBEAT, Mesh.HEARTBEAT_PROP_KNOWNAPS),
				new PropData("Mesh Child List", 			Mesh.PROP_HEARTBEAT, Mesh.HEARTBEAT_PROP_IMCP_CHILD),
                new PropData("Client Activity", 	        Mesh.PROP_HEARTBEAT, Mesh.HEARTBEAT_PROP_CLIENT),
				new PropData("Uplink TX Rate", 				Mesh.PROP_HEARTBEAT, Mesh.HEARTBEAT_PROP_TXRATE ),
				new PropData("Hop Count", 					Mesh.PROP_HEARTBEAT, Mesh.HEARTBEAT_PROP_HPC),

	    });

	    properties[i++] = new PropArray("Board Information", Mesh.PROP_BOARD_INFO,
		    	new PropData[] {
	    		new PropData(" Temperature",				Mesh.PROP_BOARD_INFO, Mesh.BOARD_PROP_TEMPERATURE),
				new PropData(" Voltage", 					Mesh.PROP_BOARD_INFO, Mesh.BOARD_PROP_VOLTAGE),
				new PropData(" Avg CPU Load", 		    	Mesh.PROP_BOARD_INFO, Mesh.BOARD_PROP_CPU_USAGE),
				new PropData(" Free Memory",		    	Mesh.PROP_BOARD_INFO, Mesh.BOARD_PROP_FREE_MEMORY)
	    });

	    properties[i++] = new PropArray("Mesh Settings", 	Mesh.PROP_MESHCONF,
	    	new PropData[] {
	    		new PropData("HeartBeat Interval", 			Mesh.PROP_MESHCONF, Mesh.MESHCONF_PROP_HBI),
				new PropData("Preferred Parent", 			Mesh.PROP_MESHCONF, Mesh.MESHCONF_PREFFERED_PARENT)
	    });
	    
	    properties[i++] = new PropArray("Downlink Security",Mesh.PROP_APCONF, 
	        new PropData[] {
	    		new PropData("Downlink Interfaces", 		Mesh.PROP_APCONF, Mesh.APCONF_PROP_IF_SEC),
	    });
	    
	    properties[i++] = new PropArray("VLAN Settings", 	Mesh.PROP_VLANCONF, 
	        new PropData[] {
	    		new PropData("VLANs",  						Mesh.PROP_VLANCONF,Mesh.VLANCONF_PROP_VLANS),
	    });
	    
	    properties[i++] = new PropArray("DownLinks", 		Mesh.PROP_APCONF, 
	        new PropData[] { 
	    		new PropData("Downlink", 					Mesh.PROP_APCONF, Mesh.APCONF_PROP_WM_INFO),
	    });

	    propUIs 				= new PropUI[DEFAULT_TAB_COUNT];
	    propUIs[0]				= new PropUI();
	    propUIs[0].label		= "  Properties  ";
	    propUIs[0].properties	= properties;
	    
	    /**
	     * Property Items Array MODIFY this when Add/Remove Properties in IAPPropListeners class
	     */
	    propItems 	 = new TreeItem[Mesh.PROP_TREES_COUNT][];
	    
	    propItems[0] = new TreeItem[Mesh.GENERAL_PROP_COUNT];
	    propItems[1] = new TreeItem[Mesh.RUNTIMEFLAG_PROP_COUNT];
	    propItems[2] = new TreeItem[Mesh.OPTIONS_PROP_COUNT];
	    propItems[3] = new TreeItem[Mesh.HEARTBEAT_PROP_COUNT];
	    propItems[4] = new TreeItem[Mesh.BOARD_PROP_COUNT];	
	    propItems[5] = new TreeItem[Mesh.APCONF_PROP_COUNT];
	    propItems[6] = new TreeItem[Mesh.MESHCONF_PROP_COUNT];
	    propItems[7] = new TreeItem[Mesh.IPCONF_PROP_COUNT];
	    propItems[8] = new TreeItem[Mesh.HWINFO_PROP_COUNT];
	    propItems[9] = new TreeItem[Mesh.STATS_PROP_COUNT];
	    propItems[10] = new TreeItem[Mesh.VLANCONF_PROP_COUNT];

	}
	
	private void createControls(Canvas parent) {
			
		parent.addControlListener(new ControlAdapter(){

			public void controlResized(ControlEvent arg0) {				
				resizeControls();
			}	
		});
		
		display 	= Display.getCurrent();
		propertyTab = new CTabFolder(parent,SWT.BORDER);
		propertyTab.setTabHeight(24);
		propertyTab.setSimple(false);
		propertyTab.setMinimumCharacters(8);
		propertyTab.setSelectionForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		
		propertyTab.setSelectionBackground(new Color[] {
				display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND), 
                display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND),
                display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT), 
                display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT)},
				new int[] {40, 70, 100}, true);

		for(int i=0; i < propUIs.length; i++) {
		    createTab(propUIs[i]);
			addTabProperties(propUIs[i]);
			propUIs[i].tabItem = new CTabItem(propertyTab,SWT.LEFT);
			propUIs[i].tabItem.setText(propUIs[i].label);
			propUIs[i].tabItem.setControl(propUIs[i].canvas);
		}
		resizeControls();
		propertyTab.setSelection(0);
	}

	/**
	 * 
	 */
	private void createTab(final PropUI propUI) {
	    propUI.canvas  = new Canvas(propertyTab,SWT.NONE);
	    propUI.canvas.addControlListener(new ControlAdapter(){
            public void controlResized(ControlEvent arg0) {
                propUI.tableTree.setBounds(0,0, propUI.canvas.getBounds().width, propUI.canvas.getBounds().height);
        }});
		
	    propUI.tableTree = new Tree(propUI.canvas,SWT.SIMPLE|SWT.FULL_SELECTION|SWT.READ_ONLY|SWT.V_SCROLL);
	    //propUI.tableTree.setRedraw(true);
	    propUI.tableTree.setHeaderVisible(true);
	    propUI.tableTree.setLinesVisible(true);
	}
	
	private void addProperties(PropUI propUI, PropArray propArr) {
		
		TreeItem itm = new TreeItem(propUI.tableTree,SWT.SIMPLE);	    
		itm.setFont(MeshViewerGDIObjects.boldVerdanaFont8);
		itm.setText(propArr.label);
		
		for(int i = 0; i < propArr.props.length; i++){
		    if(propArr.props[i] instanceof PropArray) {
		        PropArray propArrCh = (PropArray)propArr.props[i];
				addProperties(propUI, propArrCh);
		    } else {
				TreeItem itm1 = new TreeItem(itm,SWT.SIMPLE); 
				propItems[propArr.props[i].mainProperty][propArr.props[i].subProperty] = itm1;
				itm1.setText(0,propArr.props[i].label);
		    }
		}
		itm.setExpanded(true);
	}
	
	private void addTabProperties(PropUI propUI) {	
		String ptableHeaders[] 	= { "Property", "Value" };
		columnWidth				= new int[2];
		
		propertyColumn  = new TreeColumn(propUI.tableTree, SWT.NONE, 0);
		propertyColumn.setText(ptableHeaders[0]);

		columnWidth[0] = (columnWidth[0] <= 0) ? 
						propertyTab.getParent().getBounds().width/2 : columnWidth[0];

		propertyColumn.addControlListener(new ControlAdapter(){
			public void controlResized(ControlEvent arg0) {
				TreeColumn column = (TreeColumn) arg0.getSource();
				columnWidth[0] = column.getWidth();
			}
		}); 
		propertyColumn.setWidth(columnWidth[0]);
		
		valueColumn  = new TreeColumn(propUI.tableTree, SWT.NONE, 1);
		valueColumn.setText(ptableHeaders[1]);
		
		columnWidth[1] =  (columnWidth[1] <= 0) ?
			propertyTab.getParent().getBounds().width/2 : columnWidth[1];
		
		valueColumn.setWidth(columnWidth[1]);
		valueColumn.addControlListener(new ControlAdapter(){
			public void controlResized(ControlEvent arg0) {
				TreeColumn column 	= (TreeColumn) arg0.getSource();
				columnWidth[1] 		= column.getWidth();
			}
		}); 
		
		PropData[] properties = propUI.properties;
		if(properties == null) {
			return;
		}
		
		for(int i=0; i < properties.length; i++) {
		    if(properties[i] instanceof PropArray) {
		        PropArray propArr = (PropArray)properties[i];
				addProperties(propUI, propArr);
		    } else {		        
				TreeItem itm = new TreeItem(propUI.tableTree,SWT.SIMPLE);
				propItems[properties[i].mainProperty][properties[i].subProperty] = itm;
				itm.setText(0, properties[i].label);
			}
		}
	}

	private void resizeControls() {
	    Rectangle parentBounds = propertyTab.getParent().getBounds();
	    propertyTab.setBounds(1,1,parentBounds.width-2,parentBounds.height-3);		
	}
   
    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.mesh.IAccessPointPropertyListener#apPropertyChanged(int, int, com.meshdynamics.meshviewer.mesh.AccessPoint)
     */
    public void apPropertyChanged(final int filter, final int subFilter, final AccessPoint src) {        

    	if(propertyTab.isDisposed() == true) {
    		return;
    	}
    	
    	if(currentSelectedAp != src) {
    		return;
    	}

    	if(MeshViewerUI.display.isDisposed() == true) {
    		return;
    	}

    	MeshViewerUI.display.asyncExec(new Runnable() {
			public void run() {
		    	if(filter == IAPPropListener.MASK_AP_STATE && subFilter == IAPPropListener.AP_STATE_DISPOSED) {
					for(int i=0;i<propItems.length;i++) {
						clearData(i);
						setDefaultValues(propItems[i]);
					}
					return;
		    	}
		    	
				IConfiguration		config	= src.getConfiguration();
				
				if(config == null) {
					for(int i=0;i<propItems.length;i++) {
						clearData(i);
						setDefaultValues(propItems[i]);
					}
					updateHwProp	 (src, config);
					return;
				}
				updateIPProp			(src, config);
				updateGeneralProp		(src, config);
				updateRuntimeFlagProp	(src, config);
				updateOptionsProp 		(src, config);
				updateHwProp	 		(src, config);
				updateHBProp	 		(src, config);
			    updateBoardInfo	 		(src, config);
				updateMeshProp			(src, config);
				updateAPProp			(src, config);
				updateVlanProp	 		(src, config);
				updateExtProp	 		(src);
			}
		});
    	

    }

    private void disposeItems(TreeItem parentItem) {
		for(TreeItem item : parentItem.getItems()) {
			item.dispose();
		}
    }
    
    private void clearData(int propItemNo) {
    	
    	TreeItem 	item;
    	
    	switch(propItemNo) {
    	
    		case Mesh.PROP_HWINFO:
    			item = propItems[Mesh.PROP_HWINFO][Mesh.HWINFO_PROP_LINKS];
    			disposeItems(item);
    			break;
    		case Mesh.PROP_HEARTBEAT:
    			item = propItems[Mesh.PROP_HEARTBEAT][Mesh.HEARTBEAT_PROP_KNOWNAPS];
    			disposeItems(item);
    			item = propItems[Mesh.PROP_HEARTBEAT][Mesh.HEARTBEAT_PROP_IMCP_CHILD];
    			disposeItems(item);
    			break;
    		case Mesh.PROP_APCONF:
    			item = propItems[Mesh.PROP_APCONF][Mesh.APCONF_PROP_WM_INFO];
    			disposeItems(item);
    			item = propItems[Mesh.PROP_APCONF][Mesh.APCONF_PROP_IF_SEC];
    			disposeItems(item);
    			break;
    		case Mesh.PROP_VLANCONF:
    			item = propItems[Mesh.PROP_VLANCONF][Mesh.VLANCONF_PROP_VLANS];
    			disposeItems(item);
    			break;
    	}
	}

    private void setDefaultValues(TreeItem[] items) {
    	for(TreeItem item : items) {

    		if(item == null || item.isDisposed() == true) {
    			continue;
    		}
    		
    		TreeItem[] children = item.getItems();
    		if(children == null || children.length <= 0) {
    			item.setText(1,DEFAULT_VALUE);
    			item.setForeground(blackColor);
    		} else {
    			setDefaultValues(children);
    		}
    	}
    }
	
	/**
     * @param subFilter
     * @param src
     */
    private void updateVlanProp(AccessPoint src, IConfiguration nodeConfig) {

    	TreeItem[] 			items 		= propItems[Mesh.PROP_VLANCONF];
    	IVlanConfiguration	vlanConfig 	= nodeConfig.getVlanConfiguration();
    	
 		for(int i = 0; i < Mesh.VLANCONF_PROP_COUNT; i++) {
    	
	    	if(items[i] == null || items[i].isDisposed() == true) {
	    		continue;
	    	}
 			switch(i) {
				case Mesh.VLANCONF_PROP_VLANS:
					updateVlanConfig(items[i],vlanConfig);
	   	        	break;
	        }
 		}
    }
	

	private void updateVlanConfig(TreeItem item, IVlanConfiguration vlanConfig) {

		int vlanCount	= vlanConfig.getVlanCount(); 
		item.setText(1, "" + vlanCount);
		
		updateChildrensRows(item, vlanCount);
       	TreeItem[] itemsCh 	= item.getItems();
       	
       	for(int j=0;j<vlanCount;j++){
           	
       		IVlanInfo 	vlanInfo 		= (IVlanInfo)vlanConfig.getVlanInfoByIndex(j);
       		String 		vlanName		= vlanInfo.getName();			
       		TreeItem 	updateItem 		= getItemByName(vlanName, itemsCh);
       		
       		if(updateItem != null) {
       			updateVlanInfo(updateItem,vlanInfo);
       		} else {
       			updateItem = new TreeItem(item, SWT.SIMPLE);
       			updateItem.setText(0,vlanName);
       			updateVlanInfo(updateItem,vlanInfo);
       		}
       		
       	}
       	
       	for(int j=0;j<itemsCh.length;j++){
       		IVlanInfo 	vlanInfo 		= (IVlanInfo)vlanConfig.getVlanInfoByName(itemsCh[j].getText());
       		if(vlanInfo == null) {
       			itemsCh[j].dispose();
       		}
       	}
	}

	private void updateVlanInfo(TreeItem parent, IVlanInfo info) {
		
    	int 	tag 			= info.getTag();
    	short	dot11eEnabled 	= info.get80211eEnabled();
    	short	dot11eCategory 	= info.get80211eCategoryIndex();
    	short 	dot1pPriority	= info.get8021pPriority();
    	String	essid			= info.getEssid();

		updateChildrensRows(parent, Mesh.VLANINFO_PROP_COUNT); // 6 is no of Sub Props

		TreeItem[] items 	= parent.getItems();     	
	    for(int i=0; i < Mesh.VLANINFO_PROP_COUNT; i++) {
	    	
    	    String textValue  	= DEFAULT_VALUE;
    		String headerValue 	= "";
	        
	        switch(i) {
		        case Mesh.VLANINFO_PROP_TAG:  
		        	headerValue = "Tag";
		        	textValue 	= (tag == UNTAGGED) ? "Untagged" :  "" + tag;    	            
		        	break;
				case Mesh.VLANINFO_PROP_VLAN_ESSID:
					headerValue		= "ESSID";
					textValue		= essid;
					break;
		        case Mesh.VLANINFO_PROP_DOT11E_ENABLED:
		        	headerValue	= "802.11e";
		        	textValue 	= (dot11eEnabled == SecurityInfo.ENABLED) ?
		        		"Enabled" : "Disabled";
		        	break;
		        case Mesh.VLANINFO_PROP_DOT11E:
					headerValue		= "802.11e Category";
	        		if(dot11eEnabled == SecurityInfo.NOT_ENABLED) {
	        			textValue 	= "Not Applicable";
	        		} else if(dot11eEnabled == SecurityInfo.ENABLED){
				    	if(dot11eCategory == InterfaceConfigInfo.AC_BK) {
				    		textValue 	= "Background";
				    	} else if(dot11eCategory == InterfaceConfigInfo.AC_BE) {
				    		textValue 	= "Best Effort";
				    	} else if(dot11eCategory == InterfaceConfigInfo.AC_VI) {
				    		textValue 	= "Video";
				    	} else if(dot11eCategory == InterfaceConfigInfo.AC_VO) {
				    		textValue 	= "Voice";
				    	}
			    	} else {
						textValue		= DEFAULT_VALUE;
			    	}
					break;
				case Mesh.VLANINFO_PROP_DOT1P:
					headerValue		= "802.1p Priority";
					textValue		= "" + dot1pPriority;
					break;
			}
	        items[i].setText(0, headerValue);
	        items[i].setText(1, textValue);
		}
	    
	}
	
	/**
     * @param subFilter
     * @param src
     */
    private void updateAPProp(AccessPoint src, IConfiguration nodeConfig) {

        TreeItem[] 				items 		= propItems[Mesh.PROP_APCONF];
        IInterfaceConfiguration	ifConfig	= nodeConfig.getInterfaceConfiguration();
        
		for(int i=0; i < Mesh.APCONF_PROP_COUNT; i++) {
    	    
	    	if(items[i] == null || items[i].isDisposed() == true) {
	    		continue;
	    	}

			String textValue = DEFAULT_VALUE;

    	    switch(i) {
	        case Mesh.APCONF_PROP_IF_SEC:
	        	textValue = updateSecurityInfo(items[i],ifConfig);
				break;
	        case Mesh.APCONF_PROP_WM_INFO:
	        	textValue = updateWmConfig(items[i], ifConfig);
	        	break;
	        }
	        	
   	        items[i].setText(1, textValue);
		}
    }
    
    private TreeItem getItemByName(String itemName, TreeItem[] items) {
    	
    	for(int i=0;i<items.length;i++) {
    		String itemText = items[i].getText();
    		
    		if(itemName.equalsIgnoreCase(itemText) == true) {
    			return items[i];
    		}
    	}
    	
    	return null;
    }

	private void updateWmInfo(TreeItem parent,IInterfaceInfo wmInfo) {
    	
		updateChildrensRows(parent, Mesh.APCONF_PROP_WM_COUNT); 
		TreeItem[] items 	= parent.getItems();
		
	    for(int i=0; i < Mesh.APCONF_PROP_WM_COUNT; i++) {
    	    String textValue  	= DEFAULT_VALUE;
    		String headerValue 	= "";
	        
	        switch(i) {
		        case Mesh.APCONF_PROP_WM_ESSID:    
		        	headerValue 	= "ESSID";
		        	textValue		= wmInfo.getEssid();
		            break;
		        case Mesh.APCONF_PROP_WM_FRAG_THR:
		        	headerValue 	= "Frag Threshold";
	        		textValue		= "" + wmInfo.getFragThreshold();
	        		break;
	        	case Mesh.APCONF_PROP_WM_BEACON_INT:
		        	headerValue 	= "Beacon Interval";
	        		textValue		= "" + wmInfo.getBeaconInterval();
	        		break;
	        	case Mesh.APCONF_PROP_WM_RTS_THR:
		        	headerValue 	= "RTS Threshold";
	    			textValue		= "" + wmInfo.getRtsThreshold();
	    			break;
	        	case Mesh.APCONF_PROP_WM_ACK_TIMEOUT:
		        	headerValue 	= "Ack Timeout";
	    			textValue		= "" + (wmInfo.getAckTimeout()*10); 
	    			break;
	        	case Mesh.APCONF_PROP_WM_HIDE_ESSID:
		        	headerValue 	= "ESSID Hidden";
	    			textValue		= (wmInfo.getEssidHidden() != 0) ? "Yes" : "No"; 
	    			break;	
	    			
    		}

	        items[i].setText(0, headerValue);
	        items[i].setText(1, textValue);
		}
    	
    }
    
    private String updateWmConfig(TreeItem item, IInterfaceConfiguration ifConfig) {
    	
    	int 			ifCount	= getWmDownlinkIfCount(ifConfig);
    	
       	updateChildrensRows(item, ifCount);
       	
       	TreeItem[] itemsCh 	= item.getItems();
       	
       	for(int j=0;j<ifConfig.getInterfaceCount();j++){
       	
       		IInterfaceInfo 	ifInfo 		= (IInterfaceInfo)ifConfig.getInterfaceByIndex(j);
       		
       		if((ifInfo.getMediumType() != Mesh.PHY_TYPE_802_11 || ifInfo.getMediumType() != Mesh.PHY_TYPE_802_11_VIRTUAL) && ifInfo.getUsageType() != Mesh.PHY_USAGE_TYPE_WM) {
       			continue;
       		}
       		
       		String 		ifName		= ifInfo.getName();			
       		TreeItem 	updateItem 	= getItemByName(ifName, itemsCh);
       		
       		if(updateItem != null) {
       			updateWmInfo(updateItem,ifInfo);
       		} else {
       			updateItem = new TreeItem(item, SWT.SIMPLE);
       			updateItem.setText(0,ifName);
       			updateWmInfo(updateItem,ifInfo);
       		}
       		
       	}
       	
       	for(int j=0;j<itemsCh.length;j++){
       		IInterfaceInfo 	ifInfo 		= (IInterfaceInfo)ifConfig.getInterfaceByName(itemsCh[j].getText());
       		if(ifInfo == null) {
      			itemsCh[j].dispose();
       		}
       	}
       	
       	return "" + itemsCh.length;
	}

	private String updateSecurityInfo(TreeItem item, IInterfaceConfiguration ifConfig) {

    	int 			ifCount		= getWmDownlinkIfCount(ifConfig);
    	
    	updateChildrensRows(item, ifCount);

    	int				itemNo		= 0;
    	TreeItem[] itemsCh 	= item.getItems();
    	
    	for(int j=0;j<ifConfig.getInterfaceCount();j++ ) {
    		
    		IInterfaceInfo ifInfo = (IInterfaceInfo)ifConfig.getInterfaceByIndex(j);
    		
    		/*if((ifInfo.getMediumType() != Mesh.PHY_TYPE_802_11 || ifInfo.getMediumType() != Mesh.PHY_TYPE_802_11_VIRTUAL) &&
    			ifInfo.getUsageType() != Mesh.PHY_USAGE_TYPE_WM) {
    			continue;
    		}*/
    		
    		if(!((ifInfo.getMediumType() == Mesh.PHY_TYPE_802_11 || ifInfo.getMediumType() == Mesh.PHY_TYPE_802_11_VIRTUAL) &&
        			ifInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_WM	)) {
    			continue;
        	}
    		
	        itemsCh[itemNo].setText(0, ifInfo.getName());
	        
	        String 					securityString 	= "";
	        ISecurityConfiguration 	secConfig 		= (ISecurityConfiguration)ifInfo.getSecurityConfiguration();
	        
	        int enabledSecurity = secConfig.getEnabledSecurity();
	        
	        if((enabledSecurity & ISecurityConfiguration.SECURITY_TYPE_WEP) != 0) {
	        	securityString += "WEP ";
	        }
	        if((enabledSecurity & ISecurityConfiguration.SECURITY_TYPE_WPA_PERSONAL) != 0) {
	        	securityString += "WPA Personal ";
	        }
	        if((enabledSecurity & ISecurityConfiguration.SECURITY_TYPE_WPA_ENTERPRISE) != 0) {
	        	securityString += "WPA Enterprise ";
	        }
	        if((enabledSecurity & ISecurityConfiguration.SECURITY_TYPE_NONE) != 0 && 
	        		securityString.equals("")) {
	        	securityString += "No Encryption";
	        }
	        if(securityString.trim() == "") {
	        	securityString = "No Encryption";
	        }
	        
	        itemsCh[itemNo].setText(1, securityString);
	        
	        itemNo++;
    	}
    	
    	return "" + itemsCh.length;
	}

    private int getWmDownlinkIfCount(IInterfaceConfiguration ifConfig) {
    	
    	int ifCount = 0;
    	for(int i=0;i<ifConfig.getInterfaceCount();i++) {
    		
    		IInterfaceInfo ifInfo = (IInterfaceInfo)ifConfig.getInterfaceByIndex(i);
    		if((ifInfo.getMediumType() == Mesh.PHY_TYPE_802_11 || ifInfo.getMediumType() == Mesh.PHY_TYPE_802_11_VIRTUAL) &&
    			ifInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_WM	) {
    			ifCount++;
    		}
    	}
    	
    	return ifCount;
    }
	
	/**
     * @param subFilter
     * @param src
     */
    private void updateHBProp(AccessPoint src, IConfiguration nodeConfig) {
        
    	IRuntimeConfiguration 	runtimeConfig	= src.getRuntimeApConfiguration();
		TreeItem[] 	 			items 			= propItems[Mesh.PROP_HEARTBEAT];
        
    	if(runtimeConfig == null) {
			setDefaultValues(items);
        	return;
		} 
		
		for(int i=0; i < Mesh.HEARTBEAT_PROP_COUNT; i++) {

	    	String textValue = DEFAULT_VALUE ;
	    	if(items[i] == null || items[i].isDisposed() == true) {
	    		continue;
	    	}
	    	
	        switch(i) {
		        case Mesh.HEARTBEAT_PROP_SQNR:
		        	textValue = "" + runtimeConfig.getHeartbeat1SequenceNumber();
		        	break;
		        case Mesh.HEARTBEAT_PROP_SIGNALSTRNTH:
		        	if(src.isRoot() == true) { 
		        		textValue = "--";
		        	}else { 
	        			textValue = "" + runtimeConfig.getParentSignal() +" dBm";
		        	}
	        		break;
	        	case Mesh.HEARTBEAT_PROP_PARENT_NODE:
	        		AccessPoint	ap	= src.getParentAp();
    				if(ap != null) {
	        			textValue 		= getParentNodeInformation(src)+ " of ";
	        			textValue 		+= ap.toString();
	        		}else {
	        			textValue = "--";
	        		}
	        		break;
				case Mesh.HEARTBEAT_PROP_KNOWNAPS:
	   	        	textValue = updateKnownApInfo(items[i],runtimeConfig);
					break;
				case Mesh.HEARTBEAT_PROP_IMCP_CHILD:
					textValue = updateIMCPChildren(items[i],runtimeConfig);
					break;
				case  Mesh.HEARTBEAT_PROP_TXRATE:
					if(src.isRoot() == true) { 
	    				textValue = "--";
					} else { 
	    				textValue = "" + runtimeConfig.getBitRate() + " Mbps";
					}
					break;
				case  Mesh.HEARTBEAT_PROP_HPC:
				    textValue = "" + runtimeConfig.getHopCount();
				    break;
                case  Mesh.HEARTBEAT_PROP_CLIENT:
                    // dsunil: Get only the count of the CLIENTS
                    //         getStaCount() gives the count of all connected devices including access points
                    textValue = updateClientActivity(items[i], runtimeConfig);
                    break;

            }
	        
           	items[i].setText(1, textValue);
    	}
    }

    
   private void updateBoardInfo(AccessPoint src, IConfiguration nodeConfig){

	   	IRuntimeConfiguration runtimeConfig	= src.getRuntimeApConfiguration();
		TreeItem[] 	  			items 			= propItems[Mesh.PROP_BOARD_INFO];
		IVersionInfo 		  	versionInfo		= src.getVersionInfo();
		
		if(runtimeConfig == null) {
			setDefaultValues(items);
	    	return;
		} 
		
		for(int i=0; i < Mesh.BOARD_PROP_COUNT; i++) {
		
			String textValue = DEFAULT_VALUE ;
    		if(items[i] == null || items[i].isDisposed() == true) {
    			continue;
    		}
		    	
		    switch(i) {
				
		    	case Mesh.BOARD_PROP_TEMPERATURE:
		    		textValue = "" + runtimeConfig.getBoardTemperature() + " C";
					break;
		    	case Mesh.BOARD_PROP_VOLTAGE:
		    		int voltage = runtimeConfig.getVoltage();
					if(voltage <= 0 ) {
						textValue	=	DEFAULT_VALUE;
					} else {
						textValue	=	""	+	voltage	+ " V";
					}
					break;
		    	case Mesh.BOARD_PROP_CPU_USAGE:
		    		if(versionInfo == null)
		    			continue;
		   			if(versionInfo.versionGreaterThanEqualTo(IVersionInfo.MAJOR_VERSION_2,
		   					IVersionInfo.MINOR_VERSION_5, IVersionInfo.VARIANT_VERSION_65)== false)
		   				continue;
					int cpuUsage = runtimeConfig.getBoardCPUUsage();
   					if(cpuUsage <= 0 ) {
						textValue	=	DEFAULT_VALUE;
					} else {
						textValue	=	""	+	cpuUsage	+ "%";
					}
		   			break;
		    	case Mesh.BOARD_PROP_FREE_MEMORY:
		   			if(versionInfo != null) {
		   				if(versionInfo.versionGreaterThanEqualTo(IVersionInfo.MAJOR_VERSION_2,
		   						IVersionInfo.MINOR_VERSION_5, IVersionInfo.VARIANT_VERSION_65)== true){
		   						
	   						int memory = runtimeConfig.getBoardFreeMemory();
		   					if(memory <= 0 ) {
								textValue	=	DEFAULT_VALUE;
							} else {
								textValue	=	""	+	memory	+ " MB";
							}
		   				}
		   			}
		    	}
	       		items[i].setText(1, textValue);
	   	}
   }
    /**
	 * @param src
	 */
	private String getParentNodeInformation(AccessPoint src) {
		
		try {
			MeshNetworkUI			meshnetworkUI	= meshViewerUI.getSelectedMeshNetworkUI();
			IRuntimeConfiguration	runtimeConfig	= src.getRuntimeApConfiguration();
			return meshnetworkUI.getInterfaceFromBssid(runtimeConfig.getParentBssid());
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private String updateKnownApInfo(TreeItem item, IRuntimeConfiguration runtimeConfig) {

    	int kapCount = runtimeConfig.getKnownApCount();
       	
    	updateChildrensRows(item, kapCount);
       	
       	TreeItem[] itemsCh 	= item.getItems();
       	
    	Enumeration<KnownAp> kapEnum = runtimeConfig.getKnownAPs();
    	int 		j		= 0;
    	
    	while(kapEnum.hasMoreElements()) {
    		KnownAp knownAp = kapEnum.nextElement();
    		String kapText = "";
    		
    		if(knownAp.nodeName != "") {
    			kapText = knownAp.nodeName + "/";
    		}
    		
    		kapText += knownAp.macAddress.toString();
    		
    		itemsCh[j].setText(0,kapText);
	        
	        String kapInfo = "";
	        for(int i=0; i<knownAp.knownApInfo.size(); i++) {
	        	KnownApInfo info = (KnownApInfo) knownAp.knownApInfo.get(i);
	        	kapInfo 		+= "[" + info.signal + "dBm/"+ info.bitRate + "mbps] ";
	        }
	        
	        itemsCh[j].setText(1,kapInfo);
	        
	        j++;
    	}
    	
    	return "" + itemsCh.length;
	}

	private int updateChildrensforSta(TreeItem parent, Enumeration<IStaConfiguration> staList) {
    	
    	int length = 0;
    	
    	while(staList.hasMoreElements()){
    		IStaConfiguration staInfo	= staList.nextElement();
    		if(staInfo.getIsIMCP() == 1)
    			length++;
    	}
    	
    	int itemCount = parent.getItemCount();
     	while(itemCount < length) {
     	    new TreeItem(parent, SWT.SIMPLE);
     	    itemCount++;
     	} 
 	   
     	TreeItem[] items = parent.getItems();
     	
     	int no = 0;
     	while(itemCount > length) {
     	    items[no].dispose();
     	    no++;
     	    itemCount--;
     	}
     	return length;
    }

    private int updateClientActivityChildren(TreeItem parent, Enumeration<IStaConfiguration> staList) {

        int length = 0;

        while(staList.hasMoreElements()){
            IStaConfiguration staInfo	= staList.nextElement();
            if(!(staInfo.getIsIMCP() == Mesh.ENABLED ||
                    MacAddress.isMeshDynamicsMacaddress(staInfo.getStaMacAddress().toString()) == true))
                length++;
        }

        int itemCount = parent.getItemCount();
        while(itemCount < length) {
            new TreeItem(parent, SWT.SIMPLE);
            itemCount++;
        }

        TreeItem[] items = parent.getItems();

        int no = 0;
        while(itemCount > length) {
            items[no].dispose();
            no++;
            itemCount--;
        }
        return length;
    }
	
	private String updateIMCPChildren(TreeItem item,IRuntimeConfiguration runtimeConfig) {
    	
    	int 		staCount 	= updateChildrensforSta(item, runtimeConfig.getStaList());
    	TreeItem[] 	itemsCh 	= item.getItems();
       	String 		textValue	= "" + itemsCh.length;
       	
       	item.setText(1,"" + staCount);
       	
       	if(staCount <= 0) {
       		return textValue;
       	}
       	
       	Enumeration<IStaConfiguration> staEnum	= runtimeConfig.getStaList();
       	
        int itemNo = 0;
        while(staEnum.hasMoreElements()) {
        	IStaConfiguration staInfo	= staEnum.nextElement();
        	
        	if(itemNo >= staCount) {
        		continue;
        	}
        	
        	if(staInfo != null && staInfo.getIsIMCP() == 1) {
        		String staText = "";
        		
        		if(staInfo.getName() != null) {
        			staText = staInfo.getName() + "/"; 
        		}
        		
        		staText += staInfo.getStaMacAddress().toString();
        		
        		itemsCh[itemNo].setText(0,staText);
	    		itemsCh[itemNo].setText(1, "[" + staInfo.getSignal() + "dBm/" + staInfo.getBitRate() + "mbps]");
	    		itemNo++;
	    	}
    	}
        
    	return textValue;
    }

    private String updateClientActivity(TreeItem item,IRuntimeConfiguration runtimeConfig) {

        int 		staCount 	= updateClientActivityChildren(item, runtimeConfig.getStaList());
        TreeItem[] 	itemsCh 	= item.getItems();
        String 		textValue	= "" + itemsCh.length;

        item.setText(1,"" + staCount);

        if(staCount <= 0) {
            return textValue;
        }

        Enumeration<IStaConfiguration> staEnum	= runtimeConfig.getStaList();

        int itemNo = 0;
        while(staEnum.hasMoreElements()) {
            IStaConfiguration staInfo	= staEnum.nextElement();

            if(itemNo >= staCount) {
                continue;
            }

            if(staInfo != null && staInfo.getIsIMCP() != 1 && MacAddress.isMeshDynamicsMacaddress(staInfo.getStaMacAddress().toString()) == false) {
                String staText = "";

                if(staInfo.getName() != null && !staInfo.getName().isEmpty() ) {
                    staText = staInfo.getName() + "/";
                }

                staText += staInfo.getStaMacAddress().toString();

                itemsCh[itemNo].setText(0,staText);
                itemsCh[itemNo].setText(1, "[" + staInfo.getSignal() + "dBm/" + staInfo.getBitRate() + "mbps]");
                itemNo++;
            }
        }

        return textValue;
    } 
    
    private void updateMeshProp(AccessPoint src, IConfiguration nodeConfig) {
    
        TreeItem[] 	items 		= propItems[Mesh.PROP_MESHCONF];
        IMeshConfiguration	meshConfig 	= nodeConfig.getMeshConfiguration();
        
	    for(int i=0; i < Mesh.MESHCONF_PROP_COUNT; i++) {
    	
	    	if(items[i] == null || items[i].isDisposed() == true) {
	    		continue;
	    	}
	    	
	    	String textValue = DEFAULT_VALUE;

	        switch(i) {

		        case Mesh.MESHCONF_PROP_HBI:
	        		textValue = "" + meshConfig.getHeartbeatInterval();
		            break;
				case Mesh.MESHCONF_PREFFERED_PARENT:
					textValue = nodeConfig.getPreferedParent().toString();
					break;
	        }
        	items[i].setText(1, textValue);
    	}
    }
    
    private void updateChildrensRows(TreeItem parent, int length) {

    	int itemCount = parent.getItemCount();
     	while(itemCount < length) {
     	    new TreeItem(parent, SWT.SIMPLE);
     	    itemCount++;
     	} 

     	TreeItem[] items = parent.getItems();
     	int no = 0;
     	while(itemCount > length) {
     	    items[no].dispose();
     	    no++;
     	    itemCount--;
     	}        
    }
    
    /**
     * @param subFilter
     * @param src
     */
    private void updateGeneralProp(AccessPoint src, IConfiguration nodeConfig) {

    	TreeItem[] 	items 		= propItems[Mesh.PROP_GENERAL];

	    for(int i=0; i < Mesh.GENERAL_PROP_COUNT; i++) {
	    	
	    	if(items[i] == null || items[i].isDisposed() == true) {
	    		continue;
	    	}
	    	
	    	String textValue = DEFAULT_VALUE;
	        switch(i) {
		        case Mesh.GENERAL_PROP_NAME:
	        		textValue =  nodeConfig.getNodeName();
		            break;
		        case Mesh.GENERAL_PROP_DESC:
        		 	textValue = nodeConfig.getDescription();
	        		break;
		        case Mesh.GENERAL_PROP_LATITUDE:
	        		textValue = nodeConfig.getLatitude();
		        	break;
		        case Mesh.GENERAL_PROP_LONGITUDE:
        			textValue = nodeConfig.getLongitude();
		        	break;
		        case Mesh.GENERAL_PROP_COUNTRY_CODE:
	        		textValue = ""+nodeConfig.getCountryCode();
		        	break;
	        }
	        
       		items[i].setText(1, textValue);

    	}

    }

    private void updateRuntimeFlagProp(AccessPoint src, IConfiguration nodeConfig) {

    	TreeItem[] 				items 			= propItems[Mesh.PROP_RUNTIME_FLAG];
    	IRuntimeConfiguration runtimeConfig	= src.getRuntimeApConfiguration();
    	
 		if(runtimeConfig == null) {
 			setDefaultValues(items);
    		return;
    	}
		
	    for(int i=0; i < Mesh.RUNTIMEFLAG_PROP_COUNT ; i++) {
	    	
	    	if(items[i] == null || items[i].isDisposed() == true) {
	    		continue;
	    	}
	    	
	    	String 	textValue	= DEFAULT_VALUE ;
	    	
	        switch(i) {
    	        case Mesh.FLAG_PROP_REBOOT_REQUIRED:
    	        	textValue = (runtimeConfig.isRebootRequired()== true) ?
	    	        		"Yes" : "No";
	    	        break;
    	        case Mesh.FLAG_PROP_RADAR_DETECT:
    	        	textValue = (runtimeConfig.isRadarDetected() == true) ?
	    	        		"Yes" : "No";
	    	        break;
    	        case Mesh.FLAG_PROP_LOCATION:
    	        	textValue = (src.isLocal() == true) ?
	    	        		"Local" : "Remote";
	    	        break;
	        }
	        
        	items[i].setText(1, textValue);
    	}
    }
    
    private void updateOptionsProp(AccessPoint src, IConfiguration nodeConfig) {

    	TreeItem[] items = propItems[Mesh.PROP_OPTIONS];

    	for(int i=0; i < Mesh.OPTIONS_PROP_COUNT ; i++) {
	    	
	    	if(items[i] == null || items[i].isDisposed() == true) {
	    		continue;
	    	}
	    	
	    	String 	textValue	= DEFAULT_VALUE ;
	    	
	        switch(i) {
    	  		case Mesh.OPTIONS_PROP_FIPS_ENABLED:
    	        	textValue = (src.isFipsDetected() == true) ? "Yes" : "No";
	    	        break;
	    	    case Mesh.OPTIONS_PROP_IGMP_SNOOPING:
	    	    	textValue = (src.isIGMPEnabled() == true) ? "Yes" : "No";
	    	    	break;
	    	    case Mesh.OPTIONS_PROP_P3M:
	    	    	textValue = (src.isP3MEnabled() == true) ? "Yes" : "No";
    	            break;
	    	    case Mesh.OPTIONS_PROP_DHCP_ENABLED:
	    	    	textValue = (src.isDHCPEnabled() == true) ? "Yes" : "No";
	    	    	break;
	    	    case Mesh.OPTIONS_PROP_PBV:
	    	    	textValue = (src.isPBVEnabled() == true) ? "Yes" : "No";
	    	        break;
	        }
        	items[i].setText(1, textValue);
    	}
    }
        
    private void updateHwProp(AccessPoint src, IConfiguration nodeConfig) {
    	
		HardwareInfo 	hwInfo 			= src.getHardwareInfo();
		TreeItem[] 		items 			= propItems[Mesh.PROP_HWINFO];
		
		if(hwInfo == null) {
			setDefaultValues(items);
			return;
		}

	    for(int i=0; i < Mesh.HWINFO_PROP_COUNT; i++) {
	    	
	    	if(items[i] == null || items[i].isDisposed() == true) {
	    		continue;
	    	}
	    	
    	    String textValue = DEFAULT_VALUE;

	        switch(i) {
	        case Mesh.HWINFO_PROP_FW_VERSION:
	        	textValue	= src.getFirmwareVersion();
	        	break;
	        case Mesh.HWINFO_PROP_MODELINFO:
	        	 textValue 	= hwInfo.getHardwareModel();
	        	 break;
	        case Mesh.HWINFO_UNIT_ADDRESS:
	        	textValue 	= src.getDsMacAddress().toString();
	        	break;
	        case Mesh.HWINFO_PROP_LINKS:
	        	InterfaceData[] ifData 	= hwInfo.getWmInterfaceInfo();
	        	textValue 				= "" + ifData.length;
	        	updateHwWmInfo(ifData,items[i]);
	        	break;
	        }
	        
        	items[i].setText(1, textValue);
		}
    }

    private void updateHwWmInfo(InterfaceData[] ifData, TreeItem item) {
    	
    	if(item.isDisposed() || ifData == null) {
    		return;
    	}    	
    	
		updateChildrensRows(item, ifData.length);

		TreeItem[] itemsCh = item.getItems();
		for(int j=0; j< ifData.length; j++) {
			itemsCh[j].setText(0, ifData[j].getName());
			itemsCh[j].setText(1, ifData[j].getMacAddr().toString());
		}
		
	}

	private void updateIPProp(AccessPoint src, IConfiguration nodeConfig) {
    	
    	INetworkConfiguration	ipConfig 	= nodeConfig.getNetworkConfiguration();
        TreeItem[] 		items 		= propItems[Mesh.PROP_IPCONF];
        
        if(ipConfig == null) {
        	setDefaultValues(items);
        	return;
        }
        
		for(int i=0; i < Mesh.IPCONF_PROP_COUNT; i++) {
			
	    	if(items[i] == null || items[i].isDisposed() == true) {
	    		continue;
	    	}
			
			String 	textValue = DEFAULT_VALUE;
			
	        switch(i) {
	        case Mesh.IPCONF_PROP_HOSTNAME:
        		textValue = ipConfig.getHostName();
	            break;
	        case Mesh.IPCONF_PROP_IPADDR:
        		textValue = ipConfig.getIpAddress().toString();
	        	break;
			case Mesh.IPCONF_PROP_NETMASK:
				textValue = ipConfig.getSubnetMask().toString();
				break;
			case Mesh.IPCONF_PROP_GATEWAY:
				textValue = ipConfig.getGateway().toString();
				break;
			default:
				textValue = DEFAULT_VALUE;
				break;
	        }
	        
	        if(textValue == null || textValue.equals(IpAddress.resetAddress.toString())) {
	        	textValue = DEFAULT_VALUE;
	        }
	        
        	items[i].setText(1, textValue);
		}
    }
	    
    public void saveUIProperties(ITableInfo  tableInfo) {
    	
		tableInfo.setTabCount(DEFAULT_TAB_COUNT);
		
		ITabInfo tabInfo = tableInfo.getTabInfo(0);
		tabInfo.setColumnCount(columnWidth.length);
		
		for(int columnNo=0;columnNo<columnWidth.length;columnNo++) {
			ITabColumnInfo columnInfo = tabInfo.getColumnInfo(columnNo);
			columnInfo.setColumnWidth(columnWidth[columnNo]);
		}
		
    }

    private void updateExtProp(AccessPoint src) {

		String nodeId	= src.getDSMacAddress();
		
		Enumeration<String> eSectionIds = extPropSections.keys();
		while(eSectionIds.hasMoreElements()) {
			
			String sectionId = eSectionIds.nextElement();
			PropUI 			propUI 	= extPropSections.get(sectionId);
			if(propUI == null)
				continue;
			
			if(propUI.tableTree.isDisposed() == true)
				continue;
			
			String 			value	= "";
			
			for(TreeItem item : propUI.tableTree.getItems()) {
				if(item.isDisposed() == false) {
					item.clearAll(true);
					value = meshViewerUI.getExtPropertyValue(sectionId, nodeId, item.getText());
					item.setText(1,value);
				}
			}
		}
    }
    
	public void apSelectionChanged(AccessPoint newSelection) {
		currentSelectedAp = newSelection;
		if(currentSelectedAp == null) {
			for(int i=0;i<propItems.length;i++) {
				clearData(i);
				setDefaultValues(propItems[i]);
			}
			
			Enumeration<PropUI> e = extPropSections.elements();
			while(e.hasMoreElements()) {
				PropUI propUI = (PropUI)e.nextElement();
				setDefaultValues(propUI.tableTree.getItems());
			}
			
		} else {
			apPropertyChanged(IAPPropListener.MASK_ALL, IAPPropListener.MASK_ALL, currentSelectedAp);
		}
	}

	public int createExtPropertySection(String sectionId, String sectionText) {
		PropUI propUI 	= new PropUI();
		createTab(propUI);
		addTabProperties(propUI);
		propUI.label	= sectionText;
		propUI.tabItem 	= new CTabItem(propertyTab,SWT.LEFT);
		propUI.tabItem.setText(propUI.label);
		propUI.tabItem.setControl(propUI.canvas);
		extPropSections.put(sectionId, propUI);
		return 0;
	}

	/**
	 * @param sectionId
	 */
	public void destroyExtPropertySection(String sectionId) {
		
		PropUI propUI = (PropUI)extPropSections.remove(sectionId);
		if(propUI == null) return;
		
		propUI.tableTree.dispose();
		propUI.canvas.dispose();
		propUI	= null;
	}

	/**
	 * @param sectionId
	 */
	public void updateExtPropertySection(String sectionId, String nodeId) {
		
		if(currentSelectedAp == null) return;
		
		String macAddress = currentSelectedAp.getDSMacAddress();
		if(nodeId.equalsIgnoreCase(macAddress) == false) {
			return;
		}
		
		MeshViewerUI.display.asyncExec(new Runnable(){
			public void run() {
				updateExtProp(currentSelectedAp);
			}
		});
	}
	
	/**
	 * @param sectionId
	 * @param propertyId
	 * @param propertyText
	 */
	public boolean addExtProperty(String sectionId, String propertyId, String propertyText) {

		PropUI propUI = (PropUI)extPropSections.get(sectionId);
		if(propUI == null) return false;
		
		TreeItem item = new TreeItem(propUI.tableTree, SWT.SIMPLE);
		item.setText(0, propertyId);
		item.setText(1, propertyText);
		
		return true;
	}

	/**
	 * @param sectionId
	 * @param propertyId
	 */
	public void removeExtProperty(String sectionId, String propertyId) {

		PropUI propUI = (PropUI)extPropSections.get(sectionId);
		if(propUI == null) return;
		
		TreeItem items[] = propUI.tableTree.getItems();
		for(int i=0;i<items.length;i++) {
			
			if(propertyId.equalsIgnoreCase(items[i].getText()) == true) {
				items[i].dispose();
				break;
			}
		}
	}

	public void loadUIProperties(ITableInfo propTableInfo) {
		ITabInfo defaultTabInfo 		= propTableInfo.getTabInfo(0);

		for(int columnNo=0;columnNo<columnWidth.length;columnNo++) {
			ITabColumnInfo columnInfo = defaultTabInfo.getColumnInfo(columnNo);
			int width = columnInfo.getColumnWidth();
			if(width > 0)
				columnWidth[columnNo] = width;
		}
		
		if(columnWidth[0] > 0)
			propertyColumn.setWidth(columnWidth[0]);
		
		if(columnWidth[1] > 0)
			valueColumn.setWidth(columnWidth[1]);
	}
}
