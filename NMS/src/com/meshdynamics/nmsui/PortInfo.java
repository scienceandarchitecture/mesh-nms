/*
 * Created on Oct 8, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui;

/**
 * @author Prachiti Gaikwad
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PortInfo{
	
	public PortInfo() {
		typeOfRadio  	= PortModelInfo.TYPENOTUSED;
		freqOfRadio  	= "";
		colorOfRadio 	= PortModelInfo._WHITE;
		ifName			= "";
	}
	
	public int	 		ifIndex;
	public String 		ifName;
	public int			ifService;
	public int 			typeOfRadio;
	public String		freqOfRadio;
	public int			colorOfRadio;
	public int 			phySubType;
}