package com.meshdynamics.nmsui;

import java.io.File;
import java.io.FileReader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.meshdynamics.api.NMSUI;
import com.meshdynamics.api.script.ScriptExecEnv;
import com.meshdynamics.nmsui.util.MFile;

public class NMSUIImplementation extends NMSUI {
	
	private MeshViewerUI 					meshviewerUI;
	private Vector<ScriptExecEnv> 			scriptExecEnvs;
	private Vector<ExtensionData> 			extensions;
	private Hashtable<String, Extension>	mapPropSections;
	private Hashtable<String, Extension>	mapStatusTabs;
	private Hashtable<String, NodeTextData>	mapNodeId;
	private Hashtable<String, Object>		mapProperties;
	private Hashtable<String, NwProperties>	mapNwProperties;
	
	private class NodeProperties {
		public String 						nodeId;
		public Hashtable<String, Object> 	properties;

		NodeProperties(String nodeId) {
			this.nodeId		= nodeId;
			this.properties = new Hashtable<String, Object>();
		}

	}
	
	private class NwProperties {
		
		public Hashtable<String, NodeProperties>	nodeProperties;
		public Hashtable<String, Object> 			properties; 
		public String 								nwName;
		
		NwProperties(String nwName) {
			this.nwName				= nwName;
			this.properties 		= new Hashtable<String, Object>();
			this.nodeProperties		= new Hashtable<String, NodeProperties>();
		}

	}
	
	private class NodeTextData {
		public Hashtable<String, Extension>	mapNodeText;
		public Vector<NodeText> 			nodeTexts;
		
		NodeTextData() {
			mapNodeText = new Hashtable<String, Extension>();
			nodeTexts	= new Vector<NodeText>();
		}
	}
	
	private class ExtensionData {
		public Extension						extension;
		public Hashtable<String, String>		mapPropSection;
		public Hashtable<String, StatusTab>		mapStatusTab;
		
		public ExtensionData(Extension ext) {
			this.extension 	= ext;	
			mapPropSection 	= new Hashtable<String, String>();
			mapStatusTab	= new Hashtable<String, StatusTab>();
		}
	}
	
	NMSUIImplementation(MeshViewerUI meshviewerUI) {
		NMSUI.singleton 	= this;
		this.meshviewerUI 	= meshviewerUI;
		scriptExecEnvs		= new Vector<ScriptExecEnv>();
		extensions			= new Vector<ExtensionData>();
		mapPropSections		= new Hashtable<String, Extension>();
		mapStatusTabs		= new Hashtable<String, Extension>();
		mapNodeId			= new Hashtable<String, NodeTextData>();
		mapProperties		= new Hashtable<String, Object>();
		mapNwProperties		= new Hashtable<String, NwProperties>();

	}
	
	void unloadExtensions() {
		scriptExecEnvs.clear();
		extensions.clear();
		mapPropSections.clear();
		mapStatusTabs.clear();
		mapNodeId.clear();
		mapProperties.clear();
		mapNwProperties.clear();
	}
	
	void loadExtensions() {
		
		try {
			scriptExecEnvs.clear();
			
			String 		extScriptDirPath	= MFile.getExtensionsPath();
			File 		file 				= new File(extScriptDirPath);
			String[] 	files 				= file.list();
			
			if(files == null)
				return;
			
			if(files.length <= 0) {
				return;
			}
			
			for(String strFile : files) {
				
				FileReader 		scriptReader 	= new FileReader(extScriptDirPath + File.separator + strFile);
				ScriptExecEnv	scriptExecEnv	= null;
				
				if(strFile.endsWith(".rb") == true) {
					scriptExecEnv	= new ScriptExecEnv("jruby");
				} else if(strFile.endsWith(".js") == true) {
					scriptExecEnv	= new ScriptExecEnv("js");
				} 
				
				if(scriptExecEnv == null) {
					scriptReader.close();
					continue;
				}

				scriptExecEnv.importClass("com.meshdynamics.api.NMSUI");
				scriptExecEnv.defineVariable("nmsui", this);
				scriptExecEnv.execute(scriptReader);

				scriptExecEnvs.add(scriptExecEnv);
			}

		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private ExtensionData getExtensionData(Extension ext) {
		for(ExtensionData extData : extensions) {
			if(extData.extension == ext)
				return extData;
		}
		return null;
	}
	
	@Override
	public void showAlert(Alert objAlert) {
		if(objAlert == null)
			return;
		
		meshviewerUI.showAlert(objAlert);
	}

	@Override
	public void addExtension(Extension ext) {
		
		if(getExtensionData(ext) != null)
			return;
		extensions.add(new ExtensionData(ext));
	}

	@Override
	public int addProperty(String sectionId, String propertyId, String propertyText) {
		
		if(mapPropSections.containsKey(sectionId) == false)
			return -1;
		
		if(meshviewerUI.addExtProperty(sectionId, propertyId, propertyText) == true)
			return 0;
		
		return -2;
	}

	@Override
	public StatusTabRow addStatusTabRow(StatusTab statusTab) {
		return meshviewerUI.addStatusTabRow(statusTab);
	}

	@Override
	public ContextMenuSection createContextMenuSection(Extension ext, ContextMenuSection parent, String titleText, Object icon) {
		ExtensionData extData = getExtensionData(ext);
		if(extData == null)
			return null;
		
		return meshviewerUI.createExtendedMenu(parent, null, titleText, icon, null);
	}

	@Override
	public ContextMenuSection createMenuCommand(ContextMenuSection parent, String commandId, String text, Object icon, MenuCommandHandler handler) {
		return meshviewerUI.createExtendedMenu(parent, commandId, text, icon, handler);
	}

	@Override
	public NodeText createNodeText(Extension ext, String nodeId, String nodeTextId) {
		ExtensionData extData = getExtensionData(ext); 
		if(extData == null)
			return null;
		
		NodeTextData nodeTextData = mapNodeId.get(nodeId);
		if(nodeTextData == null) {
			nodeTextData = new NodeTextData();
			mapNodeId.put(nodeId, nodeTextData);
		}
		
		Extension e = nodeTextData.mapNodeText.get(nodeTextId);
		if(e != null) {
			if(e != ext)
				return null;
			
			for(NodeText nodeText : nodeTextData.nodeTexts) {
				if(nodeText.getNodeId().equalsIgnoreCase(nodeId) == true)
					return nodeText;
			}
			
			return null;
		} 
		
		NodeText nodeText = meshviewerUI.createNodeText(nodeId, nodeTextId);
		if(nodeText == null)
			return null;
		
		nodeTextData.mapNodeText.put(nodeId, ext);
		nodeTextData.nodeTexts.add(nodeText);
		
		return nodeText;
	}

	@Override
	public int createPropertySection(Extension ext, String sectionId, String sectionText) {
		
		ExtensionData extData = getExtensionData(ext); 
		if(extData == null)
			return -1;
		
		if(mapPropSections.containsKey(sectionId) == true)
			return -2;
		
		if(extData.mapPropSection.containsKey(sectionId) == true)
			return -3;
		
		if(meshviewerUI.createExtPropertySection(sectionId, sectionText) != 0)
			return -4;
	
		mapPropSections.put(sectionId, ext);
		extData.mapPropSection.put(sectionId, sectionText);
		return 0;
	}

	@Override
	public StatusTab createStatusTab(Extension ext, String tabName, StatusTabHeader columns) {
		
		Extension srcExt = mapStatusTabs.get(tabName);
		if(srcExt != null && srcExt != ext)
			return null;

		ExtensionData extData = getExtensionData(ext); 
		if(extData == null)
			return null;
		
		StatusTab statusTab = extData.mapStatusTab.get(tabName);
		if(statusTab != null)
			return statusTab;
		
		statusTab = meshviewerUI.createStatusTab(tabName, columns);
		if(statusTab == null)
			return null;
		
		mapStatusTabs.put(tabName, ext);
		extData.mapStatusTab.put(tabName, statusTab);
		
		return statusTab;
	}

	@Override
	public void destroyContextMenuSection(ContextMenuSection menuSection) {
		meshviewerUI.destroyExtendedMenu(menuSection);
	}

	@Override
	public void destroyNodeText(NodeText nodeText) {
		NodeTextData nodeData = mapNodeId.remove(nodeText.getNodeId());
		if(nodeData == null)
			return;
		
		meshviewerUI.destroyNodeText(nodeText);
		nodeData.mapNodeText.remove(nodeText.getTextId());
		nodeData.nodeTexts.remove(nodeText);
	}

	@Override
	public void destroyPropertySection(String sectionId) {

		Extension ext = mapPropSections.get(sectionId);
		if(ext == null)
			return;
		
		meshviewerUI.destroyExtPropertySection(sectionId);
		
		ExtensionData extData = getExtensionData(ext); 
		if(extData == null)
			return;
		
		extData.mapPropSection.remove(sectionId);
		
	}

	@Override
	public void destroyStatusTab(StatusTab statusTab) {
		
		String tabName = statusTab.getName();
		Extension ext = mapStatusTabs.remove(tabName);
		if(ext != null ) {
			ExtensionData extData = getExtensionData(ext); 
			if(extData != null) {
				extData.mapStatusTab.remove(tabName);
			}
		}

		meshviewerUI.destroyStatusTab(statusTab);
	}

	@Override
	public void removeExtension(Extension ext) {
		for(ExtensionData extData : extensions) {
			if(extData.extension.equals(ext) == true) {
				extensions.remove(extData);
				return;
			}
		}
	}

	@Override
	public void removeProperty(String sectionId, String propertyId) {
		meshviewerUI.removeExtProperty(sectionId, propertyId);
	}

	@Override
	public void removeStatusTabRow(StatusTabRow statusTabRow) {
		meshviewerUI.removeStatusTabRow(statusTabRow);
	}

	@Override
	public void requestPropertySectionUpdate(String sectionId, String nodeId) {
		if(mapPropSections.containsKey(sectionId) == false)
			return;
		
		meshviewerUI.updateExtPropertySection(sectionId, nodeId);
	}

	@Override
	public void updateStatusTabRow(StatusTabRow statusTabRow) {
		meshviewerUI.updateStatusTabRow(statusTabRow);
	}

	/**
	 * @param sectionId
	 * @param nodeId
	 * @param propertyText
	 * @return
	 */
	public String getPropertyValue(String sectionId, String nodeId, String propertyId) {
		
		Extension extension = mapPropSections.get(sectionId);
		if(extension == null) return "Not Available";
		
		return extension.getPropertyValue(sectionId, nodeId, propertyId);
	}

	public void notifyViewerStarted() {
		for(ExtensionData extData : extensions) {
			try {
				extData.extension.viewerStarted();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	public void notifyViewerStopped() {
		for(ExtensionData extData : extensions) {
			try {
				extData.extension.viewerStopped();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	public void notifyNetworkOpened(String networkName) {
		for(ExtensionData extData : extensions) {
			try {
				extData.extension.networkOpened(networkName);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	public void notifyNetworkClosed(String networkName) {
		for(ExtensionData extData : extensions) {
			try {
				extData.extension.networkClosed(networkName);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	public void notifyNetworkSelected(String networkName) {
		for(ExtensionData extData : extensions) {
			try {
				extData.extension.networkSelected(networkName);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	public void notifyNodeSelected(String nwName, String macAddress) {
		for(ExtensionData extData : extensions) {
			try {
				extData.extension.nodeSelected(nwName, macAddress);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	public void notifyNodeAdded(String nwName, String macAddress) {
		for(ExtensionData extData : extensions) {
			try {
				extData.extension.nodeAdded(nwName, macAddress);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public void notifyNodeDeleted(String nwName, String macAddress) {
		for(ExtensionData extData : extensions) {
			try {
				extData.extension.nodeDeleted(nwName, macAddress);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	@Override
	public Object getProperty(String name) {
		return mapProperties.get(name);
	}

	@Override
	public Object getProperty(String networkName, String name) {
		NwProperties nwProperty = mapNwProperties.get(networkName);
		if(nwProperty == null)
			return null;
		
		return nwProperty.properties.get(name);
	}

	@Override
	public Object getProperty(String networkName, String nodeId, String name) {
		NwProperties nwProperty = mapNwProperties.get(networkName);
		if(nwProperty == null)
			return null;
		
		NodeProperties nodeProperty = nwProperty.nodeProperties.get(nodeId);
		if(nodeProperty == null)
			return null;
		
		return nodeProperty.properties.get(name);
	}

	@Override
	public boolean setProperty(String name, Object value) {
		if(value == null)
			mapProperties.remove(name);
		else
			mapProperties.put(name, value);
		
		return true;
	}

	@Override
	public boolean setProperty(String networkName, String name, Object value) {
		
		NwProperties nwProperty = mapNwProperties.get(networkName);
		if(nwProperty == null) {
			if(name == null)
				return true;
			
			nwProperty = new NwProperties(networkName);
		}
		
		if(name == null) {
			nwProperty.properties.clear();
			return true;
		}
		
		if(value == null) 
			nwProperty.properties.remove(name);
		else
			nwProperty.properties.put(name, value);
		
		return true;
	}

	@Override
	public boolean setProperty(String networkName, String nodeId, String name, Object value) {

		NwProperties 	nwProperty 		= mapNwProperties.get(networkName);
		
		if(nwProperty == null) {
			if(nodeId == null)
				return true;
			
			nwProperty 		= new NwProperties(networkName);
			mapNwProperties.put(networkName, nwProperty);
			nwProperty.nodeProperties.put(nodeId, new NodeProperties(nodeId));
		}

		if(nodeId == null) {
			nwProperty.nodeProperties.clear();
			return true;
		}
		
		NodeProperties	nodeProperty	= nwProperty.nodeProperties.get(nodeId);
		
		if(nodeProperty == null) {
			if(name == null)
				return true;
			
			nodeProperty = new NodeProperties(nodeId);
			nwProperty.nodeProperties.put(nodeId, nodeProperty);
		}
		
		if(name == null) {
			nodeProperty.properties.clear();
			return true;
		}
		
		if(value == null) 
			nodeProperty.properties.remove(name);
		else
			nodeProperty.properties.put(name, value);
		
		return true;
	}

	@Override
	public String getNodeElementProperty(String networkName, String nodeId, String property) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setNodeElementProperty(String networkName, String nodeId, String property, String value) {
		meshviewerUI.setNodeElementProperty(networkName, nodeId, property, value);
	}

	@Override
	public Enumeration<String> getGroupSelection() {
		return meshviewerUI.getGroupSelection();
	}

	
}
