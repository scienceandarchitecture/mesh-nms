
/************************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : PerspectiveSettings.java
 * Comments : previous DATA.dat file settings file.
 * Created  : Apr 13, 2007
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ---------------------------------------------------------------------------------
 * | No  |Date      	 |  Comment                                        | Author |
 * ----------------------------------------------------------------------------------
 * |  0  |Apr 13, 2007   | Created. 	                                   |Prachiti|
 * ----------------------------------------------------------------------------------
 *************************************************************************************/
 
 
package com.meshdynamics.nmsui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.Bytes;


/**
 * This is the previous DATA.dat file settings file.
 * It contains all the NMS perspective settings.
 * e.g. showProperty,showStatusDisplay etc.
 * */
public class PerspectiveSettings {
	
	private static String 	fileName;
	/*
	 * These counts should not exceed 256
	 */
	private TableInfo		propertyTableInfo;
	private TableInfo		logTableInfo;

	
	public 	boolean 		showProperty;
	public 	boolean 		showStatusDisplay;
	public 	int 			propWidth;
	public 	int				logHeight;
	
	
	public PerspectiveSettings(MeshViewerUI meshViewerUI) {

		showProperty			= true;
		showStatusDisplay		= true;
		logHeight 				= 250;
		propWidth				= 295;
		propertyTableInfo 		= new TableInfo();
		logTableInfo			= new TableInfo();
		fileName 				= MFile.getConfigPath() + "/PerspectiveSettings.ini";
	}
	
	public boolean readSettings() {
		
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fileName);
			if(fis != null){
			 	read(fis);
			}
			fis.close();
			fis = null;
			
		}catch(Exception e){
	    	 System.out.println(fileName + " not found.");
	    	 return false;
	    }
		
		return true;
	 }
	
	private void read(FileInputStream in) {
		
		byte[] oneByte 		= new byte[1];
		byte[] fourBytes	= new byte[4];
		short value;
		
		try {
			in.read(oneByte);
			value = Bytes.byteToShort(oneByte[0]);
			if(value == 1)
				showProperty  = true;
			else
				showProperty = false;
			
			in.read(oneByte);
			value = Bytes.byteToShort(oneByte[0]);
			if(value == 1)
				showStatusDisplay  = true;
			else
				showStatusDisplay = false;
			
			in.read(fourBytes);
			propWidth = Bytes.bytesToInt(fourBytes);
			
			in.read(fourBytes);
			logHeight = Bytes.bytesToInt(fourBytes);
	
			in.read(oneByte);
			short tabCount = Bytes.byteToShort(oneByte[0]);
		
			if(tabCount > 0) {
				propertyTableInfo.setTabCount(tabCount);
				readTableInfo(in,propertyTableInfo);
			}
			
			in.read(oneByte);
			tabCount = Bytes.byteToShort(oneByte[0]);
		
			if(tabCount > 0) {
				logTableInfo.setTabCount(tabCount);
				readTableInfo(in,logTableInfo);
			}
			
		} catch (Exception e) {
			Mesh.logException(e);
		}
	}

	private void readTableInfo(FileInputStream in, ITableInfo tableInfo) {
		
		byte twoBytes[] = new byte[2];
		int tabCount 	= tableInfo.getTabCount();
		
		for(int tabNo=0;tabNo<tabCount;tabNo++) {
			try {
				
				in.read(twoBytes);
				short columnCount = Bytes.bytesToShort(twoBytes);
				ITabInfo tabInfo = tableInfo.getTabInfo(tabNo);
				tabInfo.setColumnCount(columnCount);
				
				for(int columnNo=0;columnNo<columnCount;columnNo++) {
					ITabColumnInfo columnInfo = tabInfo.getColumnInfo(columnNo);
					in.read(twoBytes);
					short columnWidth = Bytes.bytesToShort(twoBytes);
					columnInfo.setColumnWidth(columnWidth);
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void writeSettings() {
		
		FileOutputStream fos = null;
		try {
			File f = new File(fileName);
			if(f.exists() == false)
				f.createNewFile();
			
			fos = new FileOutputStream(f);
			if(fos != null){
			 	write(fos);
			}
			fos.close();
			fos = null;
			
		}catch(Exception e){
	    	 System.out.println(fileName + " not found.");
	    }
	}
	private void write(OutputStream out){
		
		byte[] fourBytes 	= new byte[4];
		byte[] oneByte		= new byte[1];
		byte[] checked		= new byte[1];
		byte[] unchecked	= new byte[1];
		
		checked[0] 		= 1;
		unchecked[0]	= 0;
		
		try{
			if(showProperty)
				out.write(checked);
			else
				out.write(unchecked);
			
			if(showStatusDisplay)
				out.write(checked);
			else
				out.write(unchecked);
			
			Bytes.intToBytes(propWidth,fourBytes);
			out.write(fourBytes);

			Bytes.intToBytes(logHeight,fourBytes);
			out.write(fourBytes);

			oneByte[0] = (byte) propertyTableInfo.tabCount;
			out.write(oneByte);
			if(propertyTableInfo.tabCount > 0)
				writeTableInfo(out,propertyTableInfo);

			oneByte[0] = (byte) logTableInfo.tabCount;
			out.write(oneByte);
			if(logTableInfo.tabCount > 0)
				writeTableInfo(out,logTableInfo);
			
		}catch (Exception e) {
			Mesh.logException(e);
		}
					
	}
	
	public ITableInfo getPropertyTableInfo() {
		return propertyTableInfo;
	}
	
	public ITableInfo getLogTableInfo() {
		return logTableInfo;
	}

	
	private void writeTableInfo(OutputStream out, ITableInfo tableInfo) {

		byte twoBytes[] = new byte[2];
		int tabCount 	= tableInfo.getTabCount();
		
		for(int tabNo=0;tabNo<tabCount;tabNo++) {
			try {
				
				ITabInfo tabInfo = tableInfo.getTabInfo(tabNo);
				short columnCount = (short) tabInfo.getColumnCount();
				Bytes.shortToBytes(columnCount, twoBytes);
				out.write(twoBytes);
				
				for(int columnNo=0;columnNo<columnCount;columnNo++) {
					ITabColumnInfo columnInfo = tabInfo.getColumnInfo(columnNo);
					short columnWidth = (short) columnInfo.getColumnWidth();
					Bytes.shortToBytes(columnWidth, twoBytes);
					out.write(twoBytes);
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private class TabColumnInfo implements ITabColumnInfo{
		int 		columnWidth;
		int  		columnIndentation;
		String 		columnName;
		
		public TabColumnInfo() {
			columnWidth		= 0;
		}

		public int getColumnWidth() {
			return columnWidth;
		}

		public void setColumnWidth(int width) {
			columnWidth = width;
		}
	}

	public interface ITabColumnInfo {
		public void setColumnWidth(int width);
		public int 	getColumnWidth();
	}
	
	private class TabInfo implements ITabInfo{
		int 					columnCount;
		Vector<ITabColumnInfo>	columnInfo;
		
		public TabInfo() {
			columnInfo		= new Vector<ITabColumnInfo>();
			columnCount 	= 0;
		}

		public void setColumnCount(int count) {
			columnCount = count;
			columnInfo.clear();
			columnInfo.setSize(count);
		}
		
		public int getColumnCount() {
			return columnCount;
		}
		
		public ITabColumnInfo getColumnInfo(int index) {
			
			ITabColumnInfo cInfo = null;
			try {
				cInfo = (ITabColumnInfo) columnInfo.get(index);
			}catch(Exception e) {
				cInfo = new TabColumnInfo();
				columnInfo.add(cInfo);
			}
			
			if(cInfo == null) {
				cInfo = new TabColumnInfo();
				columnInfo.setElementAt(cInfo, index);
			}
			
			return cInfo;
		}
		
	}
	
	public interface ITabInfo {
		public void 			setColumnCount(int count);
		public int 				getColumnCount();
		public ITabColumnInfo 	getColumnInfo(int index);
	}
	
	private class TableInfo implements ITableInfo{
		private int 				tabCount;
		private Vector<ITabInfo>	tabInfo;
		
		public TableInfo() {
			tabCount 	= 0;
			tabInfo		= new Vector<ITabInfo>();
		}
		
		public int getTabCount() {
			return tabCount;
		}

		public void setTabCount(int count) {
			tabCount = count;
			tabInfo.clear();
			tabInfo.setSize(count);
		}
		
		public ITabInfo getTabInfo(int index) {
			ITabInfo tInfo = null;
			try {
				tInfo = (ITabInfo) tabInfo.get(index);
			}catch(Exception e) {
				tInfo = new TabInfo();
				tabInfo.add(tInfo);
			}
			
			if(tInfo != null)
				return tInfo;
			
			tInfo = new TabInfo();
			tabInfo.setElementAt(tInfo, index);
			
			return tInfo;
		}
		
	}
	
	public interface ITableInfo {
		public void 	setTabCount(int count);
		public int 		getTabCount();
		public ITabInfo getTabInfo(int index);
	}
	


}
