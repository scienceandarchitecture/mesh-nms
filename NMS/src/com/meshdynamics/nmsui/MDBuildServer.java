package com.meshdynamics.nmsui;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class MDBuildServer {
	public static String 	server;
	public static int 		port; 
   static{
	  server 		    = "server.meshdynamics.com";
  	  port				= 8080;
      readSettings();
  } 
   
   public static void writeSettings() {
		
		try {
			BufferWriter writer = new BufferWriter(1024);
			writer.writeString(server);
			writer.writeInt((long)port);			
			FileOutputStream fileOut = new FileOutputStream(MFile.getConfigPath() + "\\MDBuild.properties");
			fileOut.write(writer.getBytes());
			fileOut.close();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void readSettings() {
		
		try {
			FileInputStream fileIn = new FileInputStream(MFile.getConfigPath() + "\\MDBuild.properties");
			int availableBytes = fileIn.available();
			byte[] buffer = new byte[availableBytes];
			fileIn.read(buffer);
			fileIn.close();			
			BufferReader reader = new BufferReader(buffer, availableBytes);			
			server = reader.readString();
//			port = (int) reader.readInt();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
