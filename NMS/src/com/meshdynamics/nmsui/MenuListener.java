/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MenuListner.java 
 * Comments : Menu Handlers for main Menu.
 * Created  : Sep 24, 2004
 * Author   : Amit Chavan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date	       |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * | 92  |Aug 28,2007  | checking for duplicate network added      	 	 |Imran   |
 *  --------------------------------------------------------------------------------
 * | 91  |Aug 13,2007  | Run macro changes                      	 	 |Abhishek |
 *  --------------------------------------------------------------------------------
 * | 90  |Aug 06,2007  | Windows explorer  added                	 	 |Abhishek |
 *  --------------------------------------------------------------------------------
 * | 89  |Jul 27,2007  | FIPS 140-2 Wizard  added               	 	 |Abhishek |
 *  --------------------------------------------------------------------------------
 * | 88  |Jul 27,2007  | Performance monitor added               	 	 |Abhishek |
 *  --------------------------------------------------------------------------------
 * | 87  |Jun 08,2007  | configureAdvancedSettings modified for updates  |Imran   |
 * --------------------------------------------------------------------------------
 * | 86  |May 02,2007  | Ap updateChangedConfig is added         	 	 |Abhishek |
 *  --------------------------------------------------------------------------------
 * | 85  |May 02,2007  | ConfigureAdvancedSettings added          	 	 |Abhishek |
 *  --------------------------------------------------------------------------------
 * | 85  |Apr 13, 2007 | Meshviewer Properties changes					 |Prachiti|
 * --------------------------------------------------------------------------------
 * | 84  |Mar 28,2007  | show topologocalView, map view   fixed 	 	 |Abhishek |
 *  --------------------------------------------------------------------------------
 * | 83  |Mar 29,2007  |show properties, show status enable/disable      |Abhishek|
 * --------------------------------------------------------------------------------
 * | 82  |Mar 28,2007  |RunMacro crashes when NMS is not running fix	 |Imran	  |
 * --------------------------------------------------------------------------------
 * | 81  |Mar 26,2007  |fipsEnabled NMS,fips compatible config chk added |Abhishek|
 * --------------------------------------------------------------------------------
 * | 80  |Mar 15,2007  | changes for configuration dialog				 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 79  |Mar 08,2007  | changes for rfspace dialog						 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 78  |Feb 27,2007  | unused code removed							 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 77  |Feb 13,2007  | healthMonitorStatus  method added				 |Abhishek|
 * --------------------------------------------------------------------------------
 * | 76  |Feb 13,2007  | healthMonitorReset method added				 |Abhishek|
 * --------------------------------------------------------------------------------
 * | 75  |Feb 7,2007   |  configureAccesspoint function changes			 | Abhijit|
 * --------------------------------------------------------------------------------
 * | 74  |Feb 2,2007   |  canConfigure function changes					 |Abhishek|
 * --------------------------------------------------------------------------------
 * | 73  |Dec 28,2006  |  Changes for MeshNetworkDialog					 | Abhijit|
 * --------------------------------------------------------------------------------
 * | 72  |Nov 13,2006  |  Refresh line tool item added                   |Abhishek|
 * --------------------------------------------------------------------------------
 * | 71  | Nov  2,2006 | editNetworkKey modified                         | Imran  |
 * --------------------------------------------------------------------------------
 * | 70  | Nov  1,2006 | newNetwork modified                             | Imran  |
 * --------------------------------------------------------------------------------
 * | 69  | Oct 31,2006 |  openMeshNetwork modified                       | Imran  |
 * --------------------------------------------------------------------------------
 * | 67  |Oct 18,2006  |  Node Status Web Page added                     | Imran  |
 *  -------------------------------------------------------------------------------
 * | 66  |Oct 17, 2006 |  Show macro action changes added                | Imran  |
 *  -------------------------------------------------------------------------------
 * | 65  |Jul 10, 2006 | f/w version Checking added					 	 |Bindu   |
 * --------------------------------------------------------------------------------
 * | 64  |Jul 07, 2006 |Advanced Setting Menu added			             |Prachiti|
 * -------------------------------------------------------------------------------- 
 * | 63  |Jul 05, 2006 |misc fix							             |Prachiti|
 * -------------------------------------------------------------------------------- 
 * | 62  |Jun 27, 2006 |Network dialog fix					             |Abhijit |
 * -------------------------------------------------------------------------------- 
 * | 61  |June 21, 2006|fix for bitrate color display	                 |Prachiti|
 * --------------------------------------------------------------------------------
 * | 60	 | May 11,2006 | removed unused methods		  			         | Mithil |
 * --------------------------------------------------------------------------------
 * | 59	 | Apr 13,2006 | e.printStackTrace() changes  			         | Mithil |
 * --------------------------------------------------------------------------------
 * | 58  |Mar 17, 2006 | Version in msg box generalised				     | Mithil |
 * --------------------------------------------------------------------------------
 * | 57  |Mar 16, 2006 | Line Color Change			 				     | Mithil |
 * --------------------------------------------------------------------------------
 * | 56  |Mar 16, 2006 |Changes for ACL							  		 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 55  |Mar 08, 2006 |VLAN QOS Template Dialog Added			  		 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 54  |Feb 28, 2006 | Send reset reboot called from run macro		 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 53  |Feb 28, 2006 | Group Select tool item problem spaning diff nets| Mithil |
 *  -------------------------------------------------------------------------------
 * | 52  |Feb 28, 2006 | msg box displayed for Factory default settings	 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 51  |Feb 28, 2006 | implicit rebooting after reboot removed	  	 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 50  | Feb 17,2006 | reboot packet key change				  		 | Mithil |
 * --------------------------------------------------------------------------------
 * | 49  | Feb 15,2006 | STA Log setings menu added				  		 | Mithil |
 * --------------------------------------------------------------------------------
 * | 48  |Feb 13, 2006 | Update and CTM Pop up Menus removed			 | Mithil |
 * --------------------------------------------------------------------------------
 * | 47  |Feb 13, 2006 | Reject Clients handling removed				 | Mithil |
 * --------------------------------------------------------------------------------
 * | 46  |Feb 13, 2006 | Log Dialog Removed				                 | Mithil |
 * --------------------------------------------------------------------------------
 * | 45  |Feb 03, 2006 | Generic Request handling changed thru flags 	 | Mithil |
 * --------------------------------------------------------------------------------
 * | 44  |Feb 03, 2006 | Generic Request		   	 					 | abhijit|
 * --------------------------------------------------------------------------------
 * | 43  |Feb 02, 2006 | STA Information Packet		 					 | Mithil |
 * --------------------------------------------------------------------------------
 * | 42  |Jan 30, 2006 | Restructuring									 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 41  |Jan 20, 2006 | Group select done								 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 40  |Jan 18, 2006 | edit network not working						 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 39  |Jan 17, 2006 |  Set Monitor Mode - Continuous Transmit mode	 | Mithil |
 * ----------------------------------------------------------------------------------
 * | 38  |Jan 16, 2006 |  Bug intoduced due to commenting of Prints		 | Mithil |
 * ----------------------------------------------------------------------------------
 * | 37  |Jan 13, 2006 |  Rmvd Edit View Settings Dialog from Edit Menu	 | Mithil |
 * ----------------------------------------------------------------------------------
 * | 36  |Jan 11, 2006 | Dialog Title changed to Node Config- Mac   	 | Mithil |
 * ----------------------------------------------------------------------------------
 * | 35  |Jan 11, 2006 |  Removed System Log Dialog					     | Mithil |
 * ----------------------------------------------------------------------------------
 * | 34  |Jan 05, 2006 | The Run Macro dialog should have things disabled| Mithil |
 * |			 		when network is not running								  |
 * ----------------------------------------------------------------------------------
 * | 33  |Jan 04, 2006 |  System.out.println()'s removed			     | Mithil |
 * ----------------------------------------------------------------------------------
 * | 32  |Jan 04, 2006 |  Toplogy View and Map View selection Toggling   | Mithil |
 * ----------------------------------------------------------------------------------
 * | 31  |Jan 04, 2006 |  Message Box Changes		                     | Mithil |
 * ----------------------------------------------------------------------------------
 * | 30  | Jan 04,2006 | Clear All Items from table after deleting AP	 | Mithil |
 * --------------------------------------------------------------------------------
 * | 29  |Jan 2, 2006  |  Message Box Changes		                     | Mithil |
 * ----------------------------------------------------------------------------------
 * | 28  |Jan 2, 2006  |  UI changes Ref 1066		                     | Mithil |
 * ----------------------------------------------------------------------------------
 * | 27  |Dec 08,2005  | Event for Log Setting menu handled			     | Amit   |
 * -------------------------------------------------------------------------------
 * | 26	 |Dec 02,2005  |After Reboot The Configuration received flag is  |Amit    | 
 * 		 |			   |	 reseted									 |        |
 * -------------------------------------------------------------------------------
 * | 25  |Nov 25,2005  |  Node file deleted on sendReboot Packet	     | Amit   |
 * -------------------------------------------------------------------------------
 * | 24  |Nov 25,2005  |  Changes for set Monitor mode		 		     | Mithil |
 *  --------------------------------------------------------------------------------
 * | 23  |Nov 7,2005   | Model number changed for INF changes 		     |Prachiti|
 *  --------------------------------------------------------------------------------
 * | 22  |Oct 24,2005  | Cleanup removed unused variables                | Mithil |
 *  --------------------------------------------------------------------------------
 * | 21  |Oct 20,2005  | Messagebox changes                              | Sneha  |
 *  --------------------------------------------------------------------------------
 * | 20  |Oct 5,2005   | MsgBox for Update/Rebbot/Restore & macro sts    | Bindu  |
 * --------------------------------------------------------------------------------
 * | 19  |Oct 4,2005   | Config load and save fixed                      | Amit   |
 * --------------------------------------------------------------------------------
 * | 18  |Oct 4,2005   | OpenNetwork dlg added							 | Amit   |
 * -------------------------------------------------------------------------------- 
 * | 17  |Sep 30,2005  | macroDlg-> runMacro/SelectTab slection added    | Bindu  |
 * --------------------------------------------------------------------------------
 * | 16  |Sep 30,2005  | Restore Factory & reboot ap fixed				 | Bindu  |
 * --------------------------------------------------------------------------------
 * | 15  |Sep 25,2005  | Properties dlg called template open/save added  | Abhijit|
 * --------------------------------------------------------------------------------
 * | 14  |Sep 22,2005  | LedSetting dlg called and values passed         | Amit   |
 * --------------------------------------------------------------------------------
 * | 13  |Sep 22, 2005 | deleted import of led dialog	 	             | Sneha  | 
 * --------------------------------------------------------------------------------
 * | 12  |Sep 19, 2005 | rearranged menulistener for version 3.0 	 	 | Abhijit|
 * --------------------------------------------------------------------------------
 * | 11  |Sep 08, 2005 | changes in configureAccessPoint function 	 	 | Abhijit|
 * --------------------------------------------------------------------------------
 * | 10  |May 15, 2005 | BatchMove implemented using Batch Result Dialog | Anand  |
 * --------------------------------------------------------------------------------
 * |  9  |May 05, 2005 | AP Config dlg init params changed  			 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  8  |May 03, 2005 | Bug fixed - Outgoing packet encrypted			 | Anand  |
 * --------------------------------------------------------------------------------
 * |  7  |Apr 25, 2005 | Batch Configure bug fixed						 | Anand  |
 * --------------------------------------------------------------------------------
 * |  6  |Apr 25, 2005 | Accelerators & images added to menu items       | Amit   |
 * --------------------------------------------------------------------------------
 * |  5  |Apr 25, 2005 | Batch Update Firmware implemented		         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  4  |Apr 19, 2005 | In showMeshPropertiesDialog call to hide 		 | Bindu  | 
 * |	 |			   | Neigbor Lines added           					 | 		  |
 * --------------------------------------------------------------------------------
 * |  3  |Mar 25, 2005 | Temperature graph added to MeshViewer           | Anand  |
 * --------------------------------------------------------------------------------
 * |  2  |Mar 02, 2005 | VLAN Packet Sent                                | Bindu  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 02, 2005 | Added VLAN Packet                               | Bindu  |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004 | Created                                         | Amit   |
 * --------------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.nmsui;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.MeshViewer;
import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationTextReader;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.nmsui.dialogs.AdminPasswordDlg;
import com.meshdynamics.nmsui.dialogs.AdminPasswordInputDlg;
import com.meshdynamics.nmsui.dialogs.ClientActivitySettingsDlg;
import com.meshdynamics.nmsui.dialogs.ExecuteMssDialog;
import com.meshdynamics.nmsui.dialogs.FirmwareUpgradeDlg;
import com.meshdynamics.nmsui.dialogs.FwUpdateDlg;
import com.meshdynamics.nmsui.dialogs.GPSUpdateDlg;
import com.meshdynamics.nmsui.dialogs.MGMT_GWDlg;
import com.meshdynamics.nmsui.dialogs.MGSetupDlg;
import com.meshdynamics.nmsui.dialogs.MeshCommandDialog;
import com.meshdynamics.nmsui.dialogs.ScriptConsoleDlg;
import com.meshdynamics.nmsui.dialogs.ScriptOutputDlg;
import com.meshdynamics.nmsui.dialogs.SearchDialog;
import com.meshdynamics.nmsui.dialogs.ServerSetupDlg;
import com.meshdynamics.nmsui.dialogs.iperf.IperfSrcDestDlg;
import com.meshdynamics.nmsui.dialogs.macros.MacrosDlg;
import com.meshdynamics.nmsui.dialogs.macros.RunMacroTab;
import com.meshdynamics.nmsui.dialogs.network.NewNetworkDialog;
import com.meshdynamics.nmsui.dialogs.network.OpenNetworkDialog;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.nmsui.mesh.MeshNetworkUIProperties;
import com.meshdynamics.nmsui.mesh.views.AccessPointHandler;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.nmsui.util.SipConfigHelper;
import com.meshdynamics.nmsui.wizards.fips.FipsWizardDialog;
import com.meshdynamics.nmsui.wizards.networkkeyedit.EditNetworkWizard;
import com.meshdynamics.util.MacAddress;


public class MenuListener extends SelectionAdapter{
	// Declarations for items in file menu..
	public static final int    	FILE_ITEM   								= 1;
	public static final int		FILE_ITEM_OPEN_TEMPLATE  					= 2;
	public static final int		FILE_ITEM_NEW_NETWORK	  					= 3;	
	public static final int		FILE_ITEM_OPEN_NETWORK	 					= 4;
	public static final int		FILE_ITEM_SAVE_NETWORK	 					= 5;
	public static final int    	FILE_ITEM_EXIT  							= 6;
	
	// Declarations for items in Edit menu..	
	public static final int    	EDIT_ITEM		            				= 11;
	public static final int    	EDIT_ITEM_EDIT_TEMPLATE		       			= 12;
	public static final int  	EDIT_ITEM_EDIT_NETWORK_KEY					= 13;
	public static final int  	EDIT_ITEM_EDIT_VIEW_SETTINGS				= 14;
	public static final int     EDIT_ITEM_EDIT_VLAN_TEMPLATE				= 15;
	public static final int 	EDIT_ITEM_EDIT_ACL_TEMPLATE 				= 16;
	public static final int 	EDIT_ITEM_EDIT_RF_CUSTOM_TEMPLATE 			= 17;
	
	// Declarations for items in Run menu..		
	public static final int		RUN_ITEM									= 21;
	public static final int		RUN_ITEM_START_VIEWER						= 22;
	public static final int		RUN_ITEM_STOP_VIEWER						= 23;
	public static final int		RUN_ITEM_GROUP_SELECT_ON					= 24;
	public static final int		RUN_ITEM_GROUP_SELECT_OFF					= 25;
	public static final int		RUN_ITEM_RUN_MACROS							= 26;
    public static final int     RUN_ITEM_GROUP_SELECTION					= 27;

	// Declarations for items in View menu..		
	public static final int		VIEW_ITEM									= 31;
	public static final int		VIEW_ITEM_SHOW_TOPOLOGY_VIEW				= 32;
	public static final int		VIEW_ITEM_SHOW_ONLINE_MAP_VIEW				= 33;
	public static final int		VIEW_ITEM_SHOW_OFFLINE_MAP_VIEW				= 34;	
	public static final int		VIEW_ITEM_SHOW_PROPERTY_VIEW				= 35;
	public static final int		VIEW_ITEM_SHOW_MESSAGES_VIEW				= 36;
	public static final int		VIEW_ITEM_SHOW_SYSTEM_LOG					= 37;
	public static final int		VIEW_ITEM_TOGGLE_VIEWS						= 38;
	
	// Declarations for items in Tools menu..
	public static final int    	TOOLS_ITEM   								= 41;
	public static final int    	TOOLS_ITEM_ANALYSER							= 42;
	public static final int		TOOLS_ITEM_LOG_SETTINGS						= 43;
	public static final int		TOOLS_ITEM_STA_ACTIVITY						= 44;
	public static final int    	TOOLS_ITEM_MESHCOMMAND						= 45;
	public static final	int		TOOLS_ITEM_IPERF_SRCDEST_TOOL				= 46;
	public static final	int		TOOLS_ITEM_WIZARDS							= 47;
	public static final	int		TOOLS_ITEM_WIZARDS_FIPS						= 48;
	public static final	int		TOOLS_ITEM_WIZARDS_GPS						= 49;
	public static final	int		TOOLS_ITEM_SCRIPT_CONSOLE					= 50;
	
	// Declarations for items in View Popup menu..
	public static final int    	VIEWPOPUP_ITEM   							= 61;
	public static final int    	VIEWPOPUP_ITEM_DELETEALL					= 62;
	public static final int    	VIEWPOPUP_ITEM_MINIMIZEALL					= 63;
	public static final int    	VIEWPOPUP_ITEM_MAXIMIZEALL					= 64;
	public static final int     VIEWPOPUP_ITEM_HEALTH_MONITOR_STATUS		= 65;
	public static final int     VIEWPOPUP_ITEM_HEALTH_MONITOR_RESET			= 66;
	public static final int     VIEWPOPUP_ITEM_SET_ADMIN_PASSWORD			= 67;
	public static final int     VIEWPOPUP_ITEM_LOCK_THE_NETWORK				= 68;
	public static final int     VIEWPOPUP_ITEM_UNLOCK_THE_NETWORK			= 69;
	
	
	public static final int    	MG_ITEM 	  								= 81;
	public static final int		MG_ITEM_START								= 82;
	public static final int		MG_ITEM_STOP								= 83;
	public static final int		MG_ITEM_SETTINGS							= 84;
	
	public static final int     SERVER_SETTINGS                             = 85;
		
	// Declarations for items in Help menu..
	public static final int    	HELP_ITEM   								= 91;
	public static final int    	HELP_ITEM_CONTENT  							= 92;
	public static final int    	HELP_ITEM_ABOUT  							= 93;
	
	// Declarations for items in Search menu..
	public static final int    	SEARCH_ITEM   								= 111;
	public static final int    	SEARCH_ITEM_BY_ANY							= 112;	
	public static final int		WINDOWS_EXPLORER							= 113;
	
	private MeshViewer 			meshViewer;
	private MeshViewerUI		meshViewerUI;
	int 						timeout;
	/**
	 * @param meshViewer
	 */
	public MenuListener(MeshViewer meshViewer, MeshViewerUI meshViewerUI) {
	    this.meshViewer 	= meshViewer;
	    this.meshViewerUI	= meshViewerUI;
	    timeout				= 0; //used for open network 
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	public void widgetSelected(SelectionEvent arg0) {
		
		try {
			Item src = (Item)arg0.getSource();
			int s =  Integer.parseInt(src.getData().toString());
		    switch(s){

		    	case FILE_ITEM_NEW_NETWORK :
		    		newMeshNetwork();
		    		break;
		    		
		    	case FILE_ITEM_OPEN_NETWORK :
		    		openMeshNetwork();
		    		break;

		    	case FILE_ITEM_SAVE_NETWORK :
		    		saveMeshNetwork();
		    		break;

		    	case FILE_ITEM_OPEN_TEMPLATE :
		    		break;
		    		
		    	case FILE_ITEM_EXIT :
		    		meshViewerUI.close();
		    		break;
		    		
		    	case EDIT_ITEM_EDIT_TEMPLATE :
		    		break;
		    	
		    	case EDIT_ITEM_EDIT_VLAN_TEMPLATE :
		    		break;
		    		
		    	case EDIT_ITEM_EDIT_ACL_TEMPLATE :
		    		break;	
		    		
		    	case EDIT_ITEM_EDIT_RF_CUSTOM_TEMPLATE:
		    		openRFCustomInfo();
		    		break;
		    		
		    	case EDIT_ITEM_EDIT_NETWORK_KEY :
		    		editNetworkKey();
		    		break;

		    	case EDIT_ITEM_EDIT_VIEW_SETTINGS :
		    		editViewSettings();
		    		break;
		    		
		    	case RUN_ITEM_START_VIEWER :
		    		meshViewer.startViewer();
		    		break;

		    	case RUN_ITEM_STOP_VIEWER :
		    		meshViewer.stopViewer();
		    		break;
		    		
		    	case RUN_ITEM_GROUP_SELECTION :
		    		handleGroupSelection();
		    		break;
		    	
		    	case RUN_ITEM_RUN_MACROS :
		    		showRunMacroDialog();
		    		break;

		    	case VIEW_ITEM_SHOW_TOPOLOGY_VIEW :
		    		showLogicalView();
		    		meshViewerUI.setViewSelection(MeshNetworkUI.VIEW_DETAIL);
		    		MeshNetworkUIProperties netWorkProp =  meshViewerUI.getSelectedMeshNetworkUI().getProperties();
		    		netWorkProp.setShowTopologyView(true);
					break;
		    		
		    	case VIEW_ITEM_SHOW_ONLINE_MAP_VIEW :
		    		showOnlineMapView();
		    		meshViewerUI.setViewSelection(MeshNetworkUI.VIEW_ONLINE_MAP);
		    		netWorkProp =  meshViewerUI.getSelectedMeshNetworkUI().getProperties();
		    		netWorkProp.setShowTopologyView(false);
		    		break;

		    	case VIEW_ITEM_SHOW_OFFLINE_MAP_VIEW :
		    		showOfflineMapView();
		    		meshViewerUI.setViewSelection(MeshNetworkUI.VIEW_OFFLINE_MAP);
		    		netWorkProp =  meshViewerUI.getSelectedMeshNetworkUI().getProperties();
		    		netWorkProp.setShowTopologyView(false);
		    		break;
		    		
		    	case VIEW_ITEM_SHOW_PROPERTY_VIEW :
		    		meshViewerUI.showHidePropertyUI(!meshViewerUI.isPropVisible(MeshViewerUI.PROPERTY_WINDOW));
		    		break;
		    	
		    	case VIEW_ITEM_SHOW_MESSAGES_VIEW :
		    		meshViewerUI.showHideLogUI(!meshViewerUI.isPropVisible(MeshViewerUI.LOG_WINDOW));
		    		break;
		    	
		    	case HELP_ITEM_CONTENT:
		    	    showContentDialog();
		    	    break;
		    	    
		    	case HELP_ITEM_ABOUT :
				    meshViewerUI.showAboutDialog();	
					break;
		    		
		    	case TOOLS_ITEM_STA_ACTIVITY:
					showStaActivityDlg();
				    break;

				case TOOLS_ITEM_MESHCOMMAND:
					showMeshCommandDialog();
				    break;
				
				case TOOLS_ITEM_WIZARDS_FIPS:
					showFipsWizard();
					break;
				
				case TOOLS_ITEM_WIZARDS_GPS:
					showGPSWizard();
					break;
				
				case TOOLS_ITEM_SCRIPT_CONSOLE:
					showScriptConsole();
					break;
					
				case TOOLS_ITEM_IPERF_SRCDEST_TOOL:
					showPerformanceTool();
					break;
				case MG_ITEM_START:
				    meshViewerUI.startMGClient();
				    break;
				    
				case MG_ITEM_STOP:
				    meshViewerUI.stopMGClient();
				    break;
				    
				case MG_ITEM_SETTINGS:
				    showMGPropertiesDlg();
				    break;
				case SERVER_SETTINGS:
				    showSevrerSetupDlg();
				    break;

				case VIEWPOPUP_ITEM_HEALTH_MONITOR_STATUS	:
					 showHealthMonitorStatus(); 
					 break;
					 
				case VIEWPOPUP_ITEM_HEALTH_MONITOR_RESET	:
					 resetHealthMonitorStatus();
					break;
				
				case SEARCH_ITEM_BY_ANY:
					showSearchDialog();
					break;

				case WINDOWS_EXPLORER:
					 showWindowsExplorer();
					 break;
					
				case VIEWPOPUP_ITEM_MAXIMIZEALL :
					maximizeAllNodes();
					break;

				case VIEWPOPUP_ITEM_MINIMIZEALL :
					minimizeAllNodes();
					break;
				
				case VIEW_ITEM_TOGGLE_VIEWS :
					toggleViews();
					break;

				case VIEWPOPUP_ITEM_SET_ADMIN_PASSWORD:
					MeshNetworkUI netUI = meshViewerUI.getSelectedMeshNetworkUI();
					AdminPasswordDlg adminDlg = new AdminPasswordDlg(netUI);
					adminDlg.show();
					if(adminDlg.getStatus() == SWT.OK) {
						meshViewerUI.saveMeshNetwork(netUI.getNwName());
					}
					break;

				case VIEWPOPUP_ITEM_UNLOCK_THE_NETWORK:
					unlockTheNetwork();
					break;
					
				case VIEWPOPUP_ITEM_LOCK_THE_NETWORK:
					lockTheNetwork();
					break;
					
				default:
			        System.err.println("Invalid main menu id called " + s);
		    }
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	

	private void showScriptConsole() {
		ScriptConsoleDlg dlg = new ScriptConsoleDlg();
		dlg.show();
	}

	private void unlockTheNetwork() {
		
		MeshNetworkUI netUI 			= this.meshViewerUI.getSelectedMeshNetworkUI();
		AdminPasswordInputDlg passDlg 	= new AdminPasswordInputDlg(netUI.getSuPassword());
		passDlg.show();
		
		if(passDlg.getStatus() == SWT.CANCEL) {
			return;
		}
	
		netUI.setIsNetworkLocked(false);
		meshViewerUI.setLockThisNetwork(netUI);
	}
	
	private void lockTheNetwork() {
		
		MeshNetworkUI netUI = this.meshViewerUI.getSelectedMeshNetworkUI();
		netUI.setIsNetworkLocked(true);
		meshViewerUI.setLockThisNetwork(netUI);
	}
	
	private void toggleViews() {
		MeshNetworkUI networkUI = meshViewerUI.getSelectedMeshNetworkUI();
		int nextView	= (networkUI.getCurrentViewType() + 1) % MeshNetworkUI.VIEW_TYPE_COUNT;
		networkUI.showView(nextView);
		meshViewerUI.setViewSelection(nextView);
	}

	private void showWindowsExplorer() {
		
		try {
			String folderPath   = MFile.getUpdatesPath();
			String newPath		= folderPath.replace('/','\\');
			String explorerPath = "EXPLORER.EXE /n,/e," + newPath;
       		if(explorerPath != null){
       			Runtime.getRuntime().exec(explorerPath);
       		}
		}catch (IOException e) {
	       	   e.printStackTrace();
    	}
	}

	/**
	 * 
	 */
	private void showPerformanceTool() {
		
		Enumeration<AccessPointHandler> e = meshViewerUI.getSelectedMeshNetworkUI().getAccesspoints();
		AccessPointHandler peer1 = null;
		AccessPointHandler peer2 = null;	
		while(e.hasMoreElements()) {
			AccessPointHandler apHandler = e.nextElement();
			if(apHandler.isGroupSelected() == false)
				continue;
			
			if(peer1 == null) {
				peer1 = apHandler;
				continue;
			}

			if(peer2 == null) {
				peer2 = apHandler;
				continue;
			}
			
		}
		
		if(peer1 == null || peer2 == null)
			return;
		
		IperfSrcDestDlg	iperfDlg	= new IperfSrcDestDlg(peer1.getAccesspoint(), peer2.getAccesspoint());
		iperfDlg.setTitle("Internode Performance Test");
		iperfDlg.show();
	}

	/**
	 * 
	 */
	private void showFipsWizard() {
		String				currentNetwork	= meshViewerUI.getSelectedMeshNetworkUI().getNwName(); 
		FipsWizardDialog	fipsWizard		= new FipsWizardDialog(meshViewerUI, meshViewer.getMeshNetworks(), currentNetwork);
		fipsWizard.show();
	}

	private void showGPSWizard() {
		MeshNetworkUI 	currentNetworkUI	= meshViewerUI.getSelectedMeshNetworkUI();
		MeshNetwork		currentNetwork		= currentNetworkUI.getMeshNetwork();
		
		Enumeration<String> apList = currentNetworkUI.getSelectedApList();
		
		if(apList == null)
			return;
		
		GPSUpdateDlg 	dlg = new GPSUpdateDlg(currentNetwork, apList);
		dlg.show();
	}
	
	private void minimizeAllNodes() {
		meshViewerUI.getSelectedMeshNetworkUI().minimizeAllNodes();
	}

	private void maximizeAllNodes() {
		meshViewerUI.getSelectedMeshNetworkUI().maximizeAllNodes();
	}

	private void showSearchDialog() {
		MeshNetworkUI 	searchHelper	= meshViewerUI.getSelectedMeshNetworkUI();
		SearchDialog 	searchDialog 	= new SearchDialog(searchHelper);
		
		searchDialog.setTitle("Search Node in " + searchHelper.getNwName());
		searchDialog.show();
	}
	
	private void handleGroupSelection() {

		boolean newGroupSelection = (meshViewerUI.isGroupSelectionEnabled() == true) ?
										false : true;
		
		meshViewerUI.setGroupSelection(newGroupSelection);
		
		if(newGroupSelection == false) {
			return;
		}
		
		MessageBox msgbox = new MessageBox(new Shell(SWT.OK | SWT.ICON_INFORMATION));
		msgbox.setText("Group Selection");
		msgbox.setMessage("To Select node SHIFT + Click node to be selected");
		msgbox.open(); 

	}

	/**
	 * 
	 */
	private void resetHealthMonitorStatus() {
		MeshNetwork meshNetwork = meshViewerUI.getSelectedMeshNetworkUI().getMeshNetwork();
		meshNetwork.resetHealthMonitorStatus();
	}

	/**
	 * 
	 */
	private void showHealthMonitorStatus() {
		  meshViewerUI.showHealthMonitorStaus();  
	}

	/**
	 * 
	 */
	private void openRFCustomInfo() {
		 	
/*		
		FileDialog  fileDialog = new FileDialog(new Shell(SWT.CLOSE),SWT.SIMPLE|SWT.APPLICATION_MODAL);
	    String [] filters = {"*.mtf"};  
	    fileDialog.setFilterPath(MFile.getTemplatePath() + "/RFCustomTemplates");
	    fileDialog.setFilterExtensions(filters);
	    String fileName = fileDialog.open();
	    if(fileName == null){
	    	return;
	    }
		Template configTemplate = new Template();
		if(configTemplate.loadFile(fileName) != true){
			MessageBox msg = new MessageBox(new Shell(SWT.OK | SWT.ICON_ERROR));
			msg.setText("Error occured while loading template file !");
			msg.setMessage("Could not load file " + fileName);
			msg.open(); 
			return;
		}
		configTemplate.setCountryCode(Mesh.CUSTOM_COUNTRY_CODE);
		RfCustomDialogTemplate cfgDlg	= new RfCustomDialogTemplate();
		cfgDlg.setModelName(configTemplate.getHardwareModel());
		cfgDlg.setTitle();
		cfgDlg.setDialogTitle("Edit "+fileName);
		cfgDlg.setConfiguration(configTemplate);
		cfgDlg.setFileName(fileName);
		cfgDlg.show();

*/			
	}

	/**
     * 
     */
    private void showContentDialog() {
       try {
       		String helpPath;
       		
       		helpPath = MeshViewerProperties.contentPath;
       		if(helpPath != null){
       			Runtime.getRuntime().exec(helpPath); 
       		}else{
       			System.out.println("help path not found");
       		}
       } catch (IOException e) {
           e.printStackTrace();
       }
    }

    /**
	 * 
	 */
	private void showMGPropertiesDlg() {
		if(meshViewerUI.isMGRunning() == false) {
			MGSetupDlg dlg = new MGSetupDlg();
			dlg.show();
		} else {
			MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.ICON_INFORMATION|SWT.OK);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Management Gateway(MG) Properties cannot be modified in running state!");
			msg.open();
		}
	}
	private void showSevrerSetupDlg() {
		ServerSetupDlg dlg=new ServerSetupDlg();
		dlg.show();		
	}
	/**
     * 
     */
/*	
    private void showLoggerDlg() {
        Enumeration enm 		= 	meshViewerUI.getSelectedMeshNetworkUI().getMeshNetwork().getAccessPoints();
        
        Hashtable apList		=   new Hashtable();
        
        while(enm.hasMoreElements()){
            AccessPoint ap  = (AccessPoint)enm.nextElement();
            apList.put(ap.getDsMacAddress().toString(),ap);
        }
        
        LoggerSettingDlg logDlg = 	new LoggerSettingDlg(apList);
        
        logDlg.showDlg();
        
    }
*/
	
    private void showStaActivityDlg() {
    	MeshNetwork meshNetwork 		=  meshViewerUI.getSelectedMeshNetworkUI().getMeshNetwork();
        Enumeration<AccessPoint> enm 	=  meshNetwork.getAccessPoints();
        
        Hashtable<String, AccessPoint> apList = new Hashtable<String, AccessPoint>();
        
        while(enm.hasMoreElements()){
            AccessPoint ap  = (AccessPoint)enm.nextElement();
            apList.put(ap.getDsMacAddress().toString(),ap);
        }
        
        ClientActivitySettingsDlg staDlg = 	new ClientActivitySettingsDlg(apList);
        staDlg.showDlg();
        meshViewerUI.clientActivityChanged(meshNetwork.getNwName(), staDlg.isClientActivityForNetwork());
    }

    private void newMeshNetwork() {
		
		NewNetworkDialog 	nwDlg  = new NewNetworkDialog();
		nwDlg.setTitle("New Network ");
		nwDlg.show();
		
		if(nwDlg.getStatus() != SWT.OK) {
			return;
		}
		if(isNetworkAlreadyPresent(nwDlg.getNetworkName()) == true) {
			MessageBox msg =  new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.ICON_WARNING);
			msg.setMessage("Network already present.");
			msg.setText("Error");
			msg.open();
			return;
		}
		
		if(meshViewerUI.createMeshNetwork(nwDlg.getNetworkName(),nwDlg.getNetworkKey(), nwDlg.getNewtworkType()) == false) {
			MessageBox msg =  new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.ICON_WARNING);
			msg.setMessage("Network could not be created.");
			msg.setText("Error");
			msg.open();
		}
	}

	/**
	 * @return
	 */
	private boolean isNetworkAlreadyPresent(String strNetwork) {
		File f= new File(MFile.getNetworkPath());
		String[] networks	= f.list();
		for(int i=0;i<networks.length; i++) {
			if(strNetwork.equalsIgnoreCase(networks[i].substring(0,networks[i].length()-4))) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 */
	private void showOnlineMapView() {
		MeshNetworkUI meshNetworkUI = meshViewerUI.getSelectedMeshNetworkUI(); 
	 	meshNetworkUI.showView(MeshNetworkUI.VIEW_ONLINE_MAP);
	}

	private void showOfflineMapView() {
		MeshNetworkUI meshNetworkUI = meshViewerUI.getSelectedMeshNetworkUI(); 
	 	meshNetworkUI.showView(MeshNetworkUI.VIEW_OFFLINE_MAP);
	}
	
	/**
	 * 
	 */
	private void showLogicalView() {
	 	MeshNetworkUI meshNetworkUI = meshViewerUI.getSelectedMeshNetworkUI(); 
	 	meshNetworkUI.showView(MeshNetworkUI.VIEW_DETAIL);
	}

	private void editViewSettings() {
		showMeshPropertiesDialog();
	}
	
	private void editNetworkKey() {
		
		String			  currentNetwork		= meshViewerUI.getSelectedMeshNetworkUI().getNwName(); 
		EditNetworkWizard editNetworkKeyWizard	= new EditNetworkWizard(meshViewer.getMeshNetworks(), currentNetwork);
		editNetworkKeyWizard.show();
	}	
 

	/**
	 * 
	 */
	private void showRunMacroDialog() {
		
		MacrosDlg 	rmd 	= new MacrosDlg(meshViewerUI);
		rmd.show();
		int selectedMacro	= rmd.getSelectedMacroAction();
		
		String 				strMacAddr;
		
		MeshNetworkUI		meshNetworkUI	= meshViewerUI.getSelectedMeshNetworkUI();
		MeshNetwork			meshNetwork		= meshNetworkUI.getMeshNetwork();
		MacAddress			macAddr			= new MacAddress();
		Enumeration<String>	macEnum			= rmd.getAddressList();
		SipConfigHelper		sipConfigHelper	= null;
		
		if(macEnum == null) {
			return;
		}
     if(selectedMacro == RunMacroTab.MAC_TAB_MGMT_GW){
       	 MGMT_GWDlg mgmt_gw=new MGMT_GWDlg(meshNetwork,macEnum);
    	 mgmt_gw.show();
    	 return;
    	/* String gateway=mgmt_gw.getGatewayAddress();
    	 boolean enabled=mgmt_gw.isEnabled();
    	 while(macEnum.hasMoreElements()) { 			
 			strMacAddr	= (String)macEnum.nextElement();
 			macAddr.resetAddress();    	 
 			macAddr.setBytes(strMacAddr);
 			AccessPoint accessPoint=meshNetwork.getAccessPoint(macAddr);
 			accessPoint.moveNode_localNMSTOremoteNMS(gateway, enabled);
    	 }*/
     }else if(selectedMacro == RunMacroTab.MAC_TAB_UPD_NODE_FIRMWARE) {
			String updateFolder		= rmd.getFirmwareDirName();
			//FwUpdateDlg dlg = new FwUpdateDlg(updateFolder, meshNetwork, macEnum);
			FirmwareUpgradeDlg dlg=new FirmwareUpgradeDlg(macEnum, meshNetwork);
			dlg.show();
		}else if(selectedMacro == RunMacroTab.MAC_TAB_REBOOT_NODES) {
			meshNetwork.reboot(macEnum);
		} else if(selectedMacro == RunMacroTab.MAC_TAB_IMPORT_SIP_REGISTRY) {
			sipConfigHelper = new SipConfigHelper();
			try {
				FileInputStream 		inStream 		= new FileInputStream(rmd.getSipRegistryFileName());
				ConfigurationTextReader configReader 	= new ConfigurationTextReader(inStream);
				sipConfigHelper.load(configReader);
				inStream.close();
			} catch(Exception e) {
				MessageBox msgBox = new MessageBox(new Shell(), SWT.OK);
				msgBox.setText("Error occured while loading sip registry");
				msgBox.setMessage(e.getMessage());
				msgBox.open();
				return;
			}
		} else if(selectedMacro == RunMacroTab.MAC_TAB_EXECUTE_MSS) {
			String mssPath = rmd.getMssFileName();
			ExecuteMssDialog dlg = new ExecuteMssDialog(meshNetworkUI, mssPath, macEnum);
			dlg.show();
			return;
		} else if(selectedMacro == RunMacroTab.MAC_TAB_IMPORT_CONFIG_SCRIPT) {
			Enumeration<String> dsMacEnum 	= rmd.getAddressList();
			String[] 			dsAddresses = new String[rmd.getAddressCount()];
			int 				i 			= 0;
			
			while(dsMacEnum.hasMoreElements()) {
				dsAddresses[i++] = new String(dsMacEnum.nextElement());
			}

			final ScriptOutputDlg dlg = new ScriptOutputDlg(meshViewerUI, 
															rmd.getImportScriptFileName(),
															dsAddresses,
															meshNetwork);
			dlg.show();
			return;
		}

		while(macEnum.hasMoreElements()) {
			
			strMacAddr	= (String)macEnum.nextElement();
			macAddr.resetAddress();
			macAddr.setBytes(strMacAddr);
	
			switch(selectedMacro) { 
			case RunMacroTab.MAC_TAB_MOVEDEF_NODES_TO:
				 String dstNetworkName	= rmd.getSelectedNetworkName();
				 String dstMeshEncKey	= meshViewer.getMeshEncKey(dstNetworkName);
				 short dstNetworkType	= meshViewer.getMeshNetworkType(dstNetworkName);
				 moveNode(macAddr, meshNetwork, dstNetworkName, dstMeshEncKey, dstNetworkType);
				 break;
			case RunMacroTab.MAC_TAB_RESTORE_FACT_DEFS:
				 meshNetwork.restoreDefaultSettings(macAddr);
				 break;
			case RunMacroTab.MAC_TAB_IMPORT_SIP_REGISTRY:
				meshNetwork.importSipConfiguration(macAddr, sipConfigHelper.getSipConfiguration());
				break;
			case RunMacroTab.MAC_TAB_CANCEL_PRESSED	:
				 return;
			default:	
				MessageBox msgBox =  new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.OK|SWT.ICON_WARNING);
				String msg = "Selection incomplete.";
				msgBox.setMessage(msg);
				msgBox.setText("Error");
				msgBox.open();
				return;
			}
		}
	}

	private void moveNode(MacAddress macAddr, MeshNetwork srcNetwork, String dstNetworkName, String dstMeshEncKey, short dstNetworkType) {
		
		if(srcNetwork.getNetworkType() == MeshNetwork.NETWORK_FIPS_ENABLED &&
				dstNetworkType == MeshNetwork.NETWORK_REGULAR) {
			
			MessageBox msgBox =  new MessageBox(new Shell(),SWT.OK| SWT.ICON_INFORMATION);
			String msg = "Accesspoint ''" + macAddr.toString() + "'' is FIPS 140-2 compliant." +SWT.CR+
					"On moving it to '"+dstNetworkName+"' network, FIPS 140-2 settings will be disabled.";
			msgBox.setMessage(msg);
			msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			if(msgBox.open() == SWT.OK) {
				srcNetwork.moveNode(macAddr, dstNetworkName, dstMeshEncKey, dstNetworkType);
			}
		} else
			srcNetwork.moveNode(macAddr, dstNetworkName, dstMeshEncKey, dstNetworkType);
	}
	
	private void showMeshPropertiesDialog() {
		
		MeshNetworkUI selNetworkUI 			= meshViewerUI.getSelectedMeshNetworkUI();
		selNetworkUI.editProperties();
	}
   
    private void openMeshNetwork() {
        
        Runnable 			timerThread;
        OpenNetworkDialog 	nwDlg;
        
        timerThread = new Runnable() {
		      public void run() {
		          timeout = 0;
		          return;
		     }
		};
		
        if(timeout != 0) {
            handleTimeout("Open Dialog Session Expired..Please try in sometime.");
            return;
        }
        
        nwDlg  = new OpenNetworkDialog(meshViewer);
        
        nwDlg.setTitle("Open Network ");
		nwDlg.show();
		if(nwDlg.getStatus() != SWT.OK) {
		    if(nwDlg.getAttempts() > 0) {
		        timeout++;
				Display.getDefault().timerExec(OpenNetworkDialog.MAX_TIMEOUT_SECS * 1000,timerThread);
			}
			return;
		}
		
		boolean retVal = false;
		if(nwDlg.getNetworkType() == MeshNetwork.NETWORK_FIPS_ENABLED)
			retVal = meshViewerUI.openFipsNetwork(nwDlg.getSelectedNetworkName(), nwDlg.getNetworkKey());
		else
			retVal = meshViewerUI.openMeshNetwork(nwDlg.getSelectedNetworkName());
			
		if(retVal == false) {
			MessageBox msg =  new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.ICON_WARNING);
			msg.setMessage("Error occured while opening network from file.");
			msg.setText("Error");
			msg.open();
		}
    }
    
	public void  handleTimeout(String msg) {
	 
	    MessageBox msgBx = new MessageBox(new Shell(SWT.CLOSE), SWT.OK | SWT.ICON_INFORMATION);
		msgBx.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		msgBx.setMessage(msg);
		msgBx.open();
	}

	private void saveMeshNetwork() {
        MeshNetworkUI meshNetworkUI = meshViewerUI.getSelectedMeshNetworkUI();
        if(meshNetworkUI == null) {
        	return;
        }
        
        if(meshViewerUI.saveMeshNetwork(meshNetworkUI.getNwName()) == true) {
	        MessageBox msgBox = new MessageBox(new Shell(SWT.OK));            
	        msgBox.setMessage(meshNetworkUI.getNwName() +  " network saved.");
	        msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msgBox.open();
        } else {
	        MessageBox msgBox = new MessageBox(new Shell(SWT.OK));            
	        msgBox.setMessage("Error occured while saving " + meshNetworkUI.getNwName() +  ".");
	        msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msgBox.open();
        }
    }
    
    private void showMeshCommandDialog() {
        MeshCommandDialog meshCmd = new MeshCommandDialog((AccessPoint)null);
		meshCmd.setTitle("Mesh Command");
        meshCmd.show();  
    }
}
