/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ActivityMapControl
 * Comments : Saturation Control for RF Space Info
 * Created  : May 11, 2006
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  9  | May 24, 2007 | DLsaturationInfo packet references removed      |Imran   |
 * --------------------------------------------------------------------------------
 * |  8  | Mar 13, 2007 | Paint and Dispose listener Implemented          |Abhijit |
 * --------------------------------------------------------------------------------
 * |  7  | Feb 27, 2007 |  function access made private				      |Abhijit |
 * --------------------------------------------------------------------------------
 * |  6  | Jun 12, 2006 | UI changes for uncategorised                    |Bindu   |
 * --------------------------------------------------------------------------------
 * |  5  | May 31, 2006 | Typo					                          |Bindu   |
 * --------------------------------------------------------------------------------
 * |  4  | May 31, 2006 | Coloring Logic changed                          |Bindu   |
 * --------------------------------------------------------------------------------
 * |  3  | May 23, 2006 | Color count increment                           |Bindu   |
 * --------------------------------------------------------------------------------
 * |  2  | May 22, 2006 | Misc UI Fixes                                   |Bindu   |
 * --------------------------------------------------------------------------------
 * |  1  | May 19, 2006 | Color changes                                   |Bindu   |
 * --------------------------------------------------------------------------------
 * |  0  | May 11, 2006 | Created                                         |Bindu   |
 * --------------------------------------------------------------------------------
**********************************************************************************/

package com.meshdynamics.nmsui.controls;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

import com.meshdynamics.meshviewer.configuration.ISaturationInfo;



public class ActivityMapControl extends Canvas implements PaintListener, DisposeListener {
	
	static {
			 lightGrayColor				= new Color(null, 206, 206, 206);	//Light Gray
			 darkGrayColor				= new Color(null, 162, 162, 162);
	}
	public static final Color []  colorList = { 
												new Color(null	,0		,0		,160), // Blue
												new Color(null	,255	,255	,0	), // Yellow
												new Color(null	,255	,0		,255), // Mauve
												new Color(null	,0		,0		,113), // Dark Blue
												new Color(null	,255	,128	,0	), // Peech
												new Color(null	,187	,187	,0	), //Yellow Ocre
												new Color(null	,234	,189	,149), //Peech-Pink
												new Color(null	,68		,81		,89	), //Grey-Blue
												new Color(null	,233	,240	,94	), //Light-Yello
												new Color(null	,40		,193	,186), //Light-Blue
												new Color(null	,248	,67		,35	), //Orange
												new Color(null	,157	,0		,0	), //Maroon
												new Color(null	,128	,0		,128), //Purple
												new Color(null	,255	,128	,255), //Pink
												new Color(null	,64		,128	,128), //Blue-Grey
												new Color(null	,54		,139	,52	), //Blue-Grey
												new Color(null	,150	,115	,41	), //Blue-Grey
												new Color(null	,67		,64		,128), //Blue-Grey
												new Color(null	,158	,33		,155), //Blue-Grey
												new Color(null	,136	,67		,55	), //Blue-Grey
												new Color(null	,39		,22		,84	), //Blue-Grey
												new Color(null	,166	,204	,32	), //Blue-Grey
												new Color(null	,155	,41		,70	), //Blue-Grey
												new Color(null	,68		,146	,187), //Blue-Grey
												new Color(null	,243	,143	,241), //Blue-Grey
												new Color(null	,45		,75		,37	), //Blue-Grey
												new Color(null	,100	,32		,13	), //Blue-Grey
												new Color(null	,162	,115	,158), //Blue-Grey
												new Color(null	,85		,15		,34	), //Blue-Grey
												new Color(null	,240	,98		,30	), //Blue-Grey
												
										}; 
	
    private double 					maxTxPercentage;
    private int 					apCount;
    private	ISaturationInfo		 	saturationInfo;
    private	int						channelIndex;
    private long 					channelTotalDuration;
    private long 					channelTxDuration;
    private static Color 			lightGrayColor;
	private static Color 			darkGrayColor;
    
	public ActivityMapControl(Composite parent,ISaturationInfo saturationInfo,int channelIndex,double maxTxPercentage) {
		super(parent,SWT.NONE);

		this.saturationInfo			= saturationInfo;
		if(this.saturationInfo == null)
			return;
		
		this.maxTxPercentage 		= maxTxPercentage;
		this.apCount 				= this.saturationInfo.getChannelAPCount(channelIndex);
		this.channelIndex 			= channelIndex;
		this.channelTotalDuration 	= this.saturationInfo.getChannelTotalDuration(channelIndex);
		this.channelTxDuration		= this.saturationInfo.getChannelTxDuration(channelIndex);

		createControl(this);
				
	}
	
	private void createControl(Composite parent){	
		
		setLayout(new GridLayout(5,false));
		this.setBackground(getDisplay().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
			
		
		this.addPaintListener(this);				
		
		this.addDisposeListener(this);
	}
	
	private void drawImage(GC gc) {
		
		double 	totalAPTX 		= 0;
 		int 	startPos		= 0;
		int 	controlWidth 	= getBounds().width;
		gc.setBackground(lightGrayColor);
		gc.fillRectangle(0, 2, controlWidth, 10);

		for(int i = 0; i < apCount; i++) {		
			
			totalAPTX 			+= saturationInfo.getChannelAPTxDuration(channelIndex, i);
			
			double 	scaledAPTX 		= (double)((getSaturation(i) * 100)/(double)maxTxPercentage);
			double 	apColorWidth 	= (double)(((scaledAPTX * controlWidth) /(double)100));
			int 	colorIndex		= i % colorList.length;
			
			gc.setBackground(colorList[colorIndex]);				
			gc.fillRectangle(startPos, 2, (int)apColorWidth, 10);
			startPos += apColorWidth;
			
		}
		
		double 	unCategorisedTx				= (double)(((this.channelTxDuration - totalAPTX) * 100) / (double)this.channelTotalDuration);
		double	unCategorisedTxPercentage	= (unCategorisedTx  * 100) / maxTxPercentage;
		double	txChannelColorWidth			= ((unCategorisedTxPercentage * controlWidth) / (double)100);

		gc.setBackground(darkGrayColor);
		gc.fillRectangle(startPos, 2, (int)txChannelColorWidth, 10);

	}	
	
	private double getSaturation(int apIndex) {
		double 	saturation 		= 0;
		long 	apTxDuration 	= 0;		
		
		apTxDuration 	= saturationInfo.getChannelAPTxDuration(channelIndex, apIndex);
		saturation 		= (double)((apTxDuration * 100 )/(double)channelTotalDuration);

		return saturation;
	}
	
	public void rePaint() {		
		redraw();
	}

	public void paintControl(PaintEvent arg0) {
		drawImage(arg0.gc);
	}

	public void widgetDisposed(DisposeEvent arg0) {
		this.removePaintListener(this);
	}
	
}
