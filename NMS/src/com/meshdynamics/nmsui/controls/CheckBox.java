/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : CheckBox.java
 * Comments : 
 * Created  : Mar 15, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Mar 15, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.controls;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class CheckBox extends Button {
	
	public static final int 	TRISTATE_OFF		= 0;
	public static final int 	TRISTATE_ON			= 1;
	public static final int 	TRISTATE_GRAYED		= 2;
	
	private Color 	triStateColor;
	private int 	triState;
	
	public CheckBox(Composite parent) {
		super(parent,SWT.CHECK);
		triState = 0;
		triStateColor =  Display.getCurrent().getSystemColor(SWT.COLOR_DARK_GRAY);
		
		this.addPaintListener(new PaintListener(){

			public void paintControl(PaintEvent arg0) {
				if(triState == TRISTATE_GRAYED){
					arg0.gc.setBackground(triStateColor);
					int relY = (arg0.height - 13) / 2;
					arg0.gc.fillRoundRectangle(arg0.x + 3, relY + 3, 7, 7, 2, 2);
				}
			}
			
		});
		
		this.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				setTriState(false);
			}});

	}

	public int getTriState() {
		return triState;
	}
	
	public void setTriState(boolean value) {
		if(value == true)
			triState = TRISTATE_GRAYED;
		else
			triState = TRISTATE_OFF;
		redraw();
	}
	
	protected void checkSubclass() {
		//we donot call super() otherwise this control wont work
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Display d = Display.getDefault();
		Shell s = new Shell(d,SWT.CLOSE);

		s.setBounds(0,0,100,100);
		CheckBox cbox = new CheckBox(s);
		cbox.setBounds(10, 10, 13, 13);

		CheckBox cbox1 = new CheckBox(s);
		cbox1.setBounds(30, 30, 13, 13);
		
		s.open();
		
		while(!s.isDisposed()){
			if(!d.readAndDispatch()){
				d.sleep();
			}
		}

	}

}
