/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Slider.java
 * Comments : Common Slider control
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  5  |Nov 27, 2006 | slider color change		  			         |  Bindu |
 * --------------------------------------------------------------------------------
 * |  4  |Jun 27, 2006 |Slider bar keylistener fixed		             |Abhijit |
 * ------------------------------------------------------------------------------- 
 * |  3  |Feb 27, 2006 |Slider bar cut bug fixed			             |Prachiti|
 * ------------------------------------------------------------------------------- 
 * |  2  |Dec 05, 2005 |Slider bar cut bug fixed			             |Prachiti|
 * ------------------------------------------------------------------------------- 
 * |  1  |Oct 24, 2005 | Cleanup removing unused variables               | Mithil |
 * ------------------------------------------------------------------------------- 
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.controls;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

public class Slider extends Canvas {

	private int 			maxValue,minValue,value;
	private int 			changeValue;
	private Point 			controlValCo;
	private boolean 		movingFlag;
	private Listener		valueListener = null;
	
	static Image _sliderArrowImg = new Image(null, Slider.class.getResourceAsStream("icons/SliderArrow.gif"));
  	

	/**
	 * @param arg0
	 * @param arg1
	 */
	public Slider(Composite arg0, int arg1) {
		super(arg0, arg1);
		controlValCo 	= new Point(0, 0);
		value		 	= 0;
		minValue		= 0;
		maxValue		= 100;
		changeValue		= 10;
		movingFlag		= false;
		
		updateControlValue();
		initControls();
		updateControlValue();
	}
	
	private void updateControlValue() {
	    
		Rectangle rect = this.getBounds();
		if(rect.height > 0)
			rect.height    = rect.height - 1;
		if(rect.width > 0)
			rect.width    = rect.width - 1;
		
		if(rect.height != 0 &&  rect.width != 0) {
		 	controlValCo.x = (int)(((double)(value - minValue)/(double)(maxValue - minValue)) * (double)rect.width);
			if(controlValCo.x <= 0){
			   controlValCo.x = 0;
			}else if(controlValCo.x >= rect.width){
			    controlValCo.x = rect.width;
			}
			controlValCo.y = (int)(((double)rect.height / (double)rect.width) * (double)controlValCo.x);
		} else {
			controlValCo.x = 0;
			controlValCo.y = 0;
		}
		
	}
	
	private void initControls() {
		
		
		this.addPaintListener(new PaintListener(){

			public void paintControl(PaintEvent arg0) {
				GC gc = arg0.gc;
				Rectangle rect = getBounds();

				if(rect.height > 0)
					rect.height    = rect.height - 1;
				if(rect.width > 0)
					rect.width    = rect.width - 5;
				
				if(controlValCo.x <= 0){
				   controlValCo.x = 0;
				}else if(controlValCo.x >= rect.width){
				    controlValCo.x = rect.width - 4;
				}	
				
				int x1 = 7;
				int y1 = 2;
				int x2 = 3 + rect.width-9;
				int y2 = 3;
		
				Display dis = Display.getCurrent();
					
				if(getEnabled() == true) {
					gc.setBackground(dis.getSystemColor(SWT.COLOR_WHITE));
					gc.fillRectangle(x1, y1, x2, y2);
				}
				
				
				gc.setForeground(dis.getSystemColor(SWT.COLOR_WIDGET_DARK_SHADOW));
				gc.drawLine(3,0, rect.width+1, 0);
				gc.drawLine(3,2, 3, 5);
				
				gc.setForeground(dis.getSystemColor(SWT.COLOR_WIDGET_DARK_SHADOW));
				gc.drawLine(3,1, rect.width, 1);
				gc.drawLine(3,5, rect.width, 5);
				gc.drawLine(3,2, 3, 5);
				gc.drawLine(rect.width,1, rect.width, 5);
				
				gc.setForeground(dis.getSystemColor(SWT.COLOR_WHITE));
				gc.drawLine(2,6, rect.width, 6);
				gc.drawLine(rect.width+1,0, rect.width + 1, 6);
					
				gc.setBackground(dis.getSystemColor(SWT.COLOR_BLUE));
				gc.fillRectangle(3,2, controlValCo.x - 1, 3);
				gc.drawImage(_sliderArrowImg,controlValCo.x, 2);
				
				
			}
		}
		);
		
		this.addMouseListener(new MouseAdapter(){
			
			public void mouseDown(MouseEvent arg0) {
				movingFlag = true;
			}

			public void mouseUp(MouseEvent arg0) {
				movingFlag = false;
			}
		}	
		);
		
		this.addMouseMoveListener(new MouseMoveListener() {

			public void mouseMove(MouseEvent movearg) {
				Rectangle rect = getBounds();
				if(movingFlag && ( movearg.x < rect.width && movearg.x >= 0)) {
					if(rect.height > 0)
						rect.height    = rect.height - 1;
					if(rect.width > 0)
						rect.width    = rect.width - 1;
					
					//movearg.y
					value = minValue + (int)((((double)movearg.x/(double)rect.width) * (double)(maxValue - minValue)));
					updateControlValue();
					redraw();
					sendEvent();
				}
			}

		});
		this.addKeyListener(new KeyAdapter(){

			public void keyPressed(KeyEvent arg0) {
			
				if(arg0.keyCode == SWT.ARROW_RIGHT) {
					value = ((value + 1) > maxValue) ? maxValue : value + 1;
				} else if(arg0.keyCode == SWT.ARROW_LEFT) {
						value = ((value - 1) < minValue) ? minValue : value - 1;
				} else if(arg0.keyCode == SWT.ARROW_UP) {
						value = ((value + 10) > maxValue) ? maxValue : value + changeValue;
				} else if(arg0.keyCode == SWT.ARROW_DOWN) {
						value = ((value - 10) < minValue) ? minValue : value - changeValue;
				} else if(arg0.keyCode == SWT.TAB) {
					
				}

				updateControlValue();
				redraw();
				sendEvent();
				
			}
		});
		
		this.addControlListener(new ControlListener() {

			public void controlMoved(ControlEvent arg0) {
				redraw();
			}

			public void controlResized(ControlEvent arg0) {
				updateControlValue();				
			}});
		
		
	}

	
	public int getMaxValue(){
		return maxValue;
	}
	
	public int getMinValue(){
		return minValue;
	}
	
	public void setMaxValue(int newMaxValue){
		maxValue = newMaxValue;
		updateControlValue();
		redraw();
		sendEvent();
	}
	
	public void setMinValue(int newMinValue){
		minValue = newMinValue;
		updateControlValue();
		redraw();
		sendEvent();
	}
	
	
	public int getValue(){
		return value;
	}
	
	public void setValue(int new_value){
		value = new_value;
		updateControlValue();
		redraw();
		sendEvent();
	}
	
	public int getChangeValue(){
		return changeValue;
	}
	
	public void setChangeValue(int newChangeValue){
		changeValue = newChangeValue;
	}
	
	
	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Widget#addListener(int, org.eclipse.swt.widgets.Listener)
	 */
	public void addValueListener(Listener arg1) {
		//super.addListener(arg0, arg1);
		valueListener = arg1;
	}
	
	private void sendEvent() {
		if(valueListener != null) {
			Event evt = new Event();
			evt.data = new Integer(value);
			valueListener.handleEvent(evt);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#getEnabled()
	 */
	public boolean getEnabled() {
		return super.getEnabled();
	}
	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#setEnabled(boolean)
	 */
	public void setEnabled(boolean flag) {
		super.setEnabled(flag);
		
	}

}
