/*
 * Created on May 10, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.controls;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.util.ChannelListInfo;
import com.meshdynamics.meshviewer.util.CountryCodeComboHelper;
import com.meshdynamics.meshviewer.util.CountryCodeXMLReader;
import com.meshdynamics.meshviewer.util.CountryData;
import com.meshdynamics.nmsui.dialogs.MeshviewerStringConstants;
import com.meshdynamics.nmsui.dialogs.base.IMessage;
import com.meshdynamics.nmsui.util.MFile;

/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CountryCodeCombo extends Combo implements IMessage{

	private Hashtable<String, CountryData>	countryCodes; 
	private Vector<ChannelListInfo>			channelList;
	private int 							lastSelectedCountryIndex;
	
	private CountryCodeComboHelper	cCodeHelper;
	private CountryCodeXMLReader 	ccxml;
	
	public CountryCodeCombo(Composite arg0, int arg1,CountryCodeComboHelper comboHelper) {
		
		super(arg0, arg1);
		
		cCodeHelper	= comboHelper;
		init();
		setCountryCodeData(cCodeHelper.getCountryCode());
		
		this.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				thisHandler();
		}});
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Combo#checkSubclass()
	 */
	protected void checkSubclass() {
		// TODO Auto-generated method stub
	}
	
	
	private int addSortedCountries(int countryCode, int itemIndex){
		
		int		count 		= 0;
		int		code		= 0;
		int selectionIndex 	= -1;
		String 	country;
		
		String[]	arrcountryCodes	= new String[countryCodes.size()-itemIndex];
		Enumeration<String> keys	= countryCodes.keys();
		
		while(keys.hasMoreElements()) {
			
			String hashKey		= (String)keys.nextElement();
			CountryData cData 	= (CountryData) countryCodes.get(hashKey);
			
			/*Skip Default and Custom(if applicable) */
			if(cData.countryCode == Mesh.DEFAULT_COUNTRY_CODE 	
					||
			   cData.countryCode == Mesh.CUSTOM_COUNTRY_CODE 	||
			   cData.countryName == MeshviewerStringConstants.UNKNOWN_COUNTRY) {
				continue;
			}
			
			arrcountryCodes[count++]	= (String)cData.countryName + "~" + cData.countryCode;
			//~ is a separator here. e.g. Australia~36
		}
		
		Arrays.sort(arrcountryCodes);
		
		for(int i = 0; i < arrcountryCodes.length; i++){
				
			String CountryandCode[]	= arrcountryCodes[i].split("~");
			if(CountryandCode.length != 2 ){
				continue;
			}
			country 			= CountryandCode[0] + " (" + CountryandCode[1] +")";
			code				= Integer.parseInt(CountryandCode[1]);
			CountryData conData = (CountryData) countryCodes.get(""+code);
						
			this.add(country);
			if(conData.countryCode == countryCode ) {
				selectionIndex = itemIndex+i;
			}
		}		
		return selectionIndex;
	
	}
	
	/**
	 * Adds unknown country to cmbCountryCode when country code is not present in CountryCode.XML
	 * @param countryCode
	 */
	private void addUnknownCountryCode(int countryCode) {
	
		CountryData unknownData	= new CountryData(MeshviewerStringConstants.UNKNOWN_COUNTRY,countryCode,cCodeHelper.getRegDomain());
		countryCodes.put(""+countryCode,unknownData);
		this.add(MeshviewerStringConstants.UNKNOWN_COUNTRY + " (" + countryCode +")");
	}


	public void init() {
		
		ccxml	= new CountryCodeXMLReader();
		ccxml.setXmlFileName(MFile.getConfigPath() + "/CountryCode.xml");
		
		if(ccxml.createXmlDocument() == true) {
			countryCodes = ccxml.readCountryCodesForModel(cCodeHelper.getHardwareModel());
		}
		
		if(countryCodes == null){
			System.out.print("Country not found in XML File.");
			countryCodes = new Hashtable<String, CountryData>();
		}
	}
	
	/**
	 * @param countryCode
	 */
	public void setCountryCodeData(int countryCode) {
		
		int itemIndex 			= 0;
		int unknownCountryCode	= -1;
		
		cCodeHelper.setCountryCode(countryCode);
		
		this.removeAll();
		
		if(cCodeHelper.getCountryCode() != Mesh.CUSTOM_COUNTRY_CODE && 
		   cCodeHelper.isCountryCodeChanged() == true) {
			countryCodes.remove(""+Mesh.CUSTOM_COUNTRY_CODE);
		}
		
		//if(iConfiguration.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,IVersionInfo.MINOR_VERSION_5,IVersionInfo.VARIANT_VERSION_30) == true){
			
		if(countryCode == Mesh.CUSTOM_COUNTRY_CODE){	
				
			this.add(MeshviewerStringConstants.CUSTOM_COUNTRY);
			CountryData customData = new CountryData(MeshviewerStringConstants.CUSTOM_COUNTRY, Mesh.CUSTOM_COUNTRY_CODE,Mesh.REG_DOMN_CODE_CUSTOM);
			countryCodes.put(""+Mesh.CUSTOM_COUNTRY_CODE,customData);
			itemIndex++;
			
		}
	//	}
		
		CountryData cDataDefault	= (CountryData)countryCodes.get("" + 0 );
		
		if(cDataDefault != null){
			String defaultText	= cDataDefault.countryName + " (" + cDataDefault.countryCode +")" ;
			this.add(defaultText);
			itemIndex++;
		}
		
		if (countryCode != Mesh.CUSTOM_COUNTRY_CODE){
			
			int cCode 			= cCodeHelper.getCountryCode();
			CountryData cData 	= (CountryData)countryCodes.get(""+cCode);
			
			if(cData == null){
			
				unknownCountryCode = cCode;
				addUnknownCountryCode(cCode);
				itemIndex++;
				
			}else{
			
				String countryName 	= cData.countryName;
				ccxml.readCountryCodeInformation(cCodeHelper.getHardwareModel(),countryName,cCode);
				channelList 		= ccxml.getChannelList();
			}
		}
		
		int selectionIndex	= addSortedCountries(countryCode, itemIndex);
		
		if(selectionIndex == -1) {
					
			if(countryCode == Mesh.CUSTOM_COUNTRY_CODE ){
		
				this.select(itemIndex-2);
				lastSelectedCountryIndex	= itemIndex-2;
				
				if(itemIndex -2 < 0){
				
					this.select(0);	
					lastSelectedCountryIndex	= 0;
				}
			}
			
			if(countryCode == Mesh.DEFAULT_COUNTRY_CODE){
				
				this.select(itemIndex-1);
				lastSelectedCountryIndex = itemIndex-1;
				if(itemIndex -1 < 0){
				
					this.select(0);	
					lastSelectedCountryIndex	= 0;
					
				}
			}
			if(countryCode == unknownCountryCode){
			
				this.select(itemIndex-1);
				lastSelectedCountryIndex	= itemIndex-1;
				
			}
		}else{
			this.select(selectionIndex);
			lastSelectedCountryIndex	= selectionIndex;
		}
		
	}
	
	/**
	 * 
	 */
	protected void thisHandler() {
		
		String 		msgStr;
		
		int			oldCountryCode;
		int 		cCode 			= 0;
		int 		selIndex 		= this.getSelectionIndex();
		
		CountryData cData   		= null;

		if(selIndex == -1) 
			return;
	
		String selCCode	= this.getItem(selIndex);
		int charIndex 	= selCCode.lastIndexOf("(");
		selCCode 		= selCCode.substring(charIndex+1 , selCCode.length()-1);
		cData   		= (CountryData)countryCodes.get(selCCode);

		if(cData == null) {
			System.out.println("Country code data unavailable for " + selCCode);
			return;
		}

		cCode	 		= cData.countryCode;
		oldCountryCode 	= cCodeHelper.getCountryCode();
    	
    	if(cCode == oldCountryCode){
			return;
		}
		
    	/*Get the info of a country from XML*/
			msgStr 	= "The DCA List will be changed according to the country specified." + 
	        			SWT.CR +" Do you want to change the country ?" ;
		
		int ans = showMessage(msgStr,SWT.YES|SWT.NO|SWT.ICON_INFORMATION);
		
		if(ans == SWT.YES) {
			lastSelectedCountryIndex = this.getSelectionIndex();
		}
		if(ans == SWT.NO){
			this.select(lastSelectedCountryIndex);
			return;
		}
		if(cCodeHelper.getCountryCode() != cCode) {
			cCodeHelper.setCountryCodeChanged(true);
		}else {
			cCodeHelper.setCountryCodeChanged(false);
		}
	}

	
	public Vector<ChannelListInfo> getChannelList(){
		return channelList;
	}
	
	
	public int getCountryCode(){
		
		int 		cCode		= 0;
		int 		index		= this.getSelectionIndex();
		CountryData cData   	= null;
		String 		selCCode	= this.getItem(index);
		if(selCCode.equals(MeshviewerStringConstants.CUSTOM_COUNTRY)){
			return Mesh.CUSTOM_COUNTRY_CODE;
		}
		int 		charIndex 	= selCCode.lastIndexOf("(");
		selCCode 				= selCCode.substring(charIndex+1 , selCCode.length()-1);
		cData   				= (CountryData)countryCodes.get(selCCode);
		cCode	 				= cData.countryCode;
		return cCode;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMessage#showMessage(java.lang.String, int)
	 */
	public int showMessage(String message, int style) {
		
		Shell shell = new Shell(Display.getDefault());
		MessageBox messageBox = new MessageBox(shell,SWT.MULTI|style);
		messageBox.setText(" Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		messageBox.setMessage(message);
		return messageBox.open();	
	}

	/**
	 * @param code
	 * @return
	 */
	public CountryData getCountryCodeData(String code) {
		return (CountryData)countryCodes.get(code);
	}
	
}
