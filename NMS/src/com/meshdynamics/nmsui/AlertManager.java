package com.meshdynamics.nmsui;

import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Vector;

import com.meshdynamics.api.NMS;
import com.meshdynamics.api.NMSUI;
import com.meshdynamics.api.NMS.Network;
import com.meshdynamics.api.NMS.Node;
import com.meshdynamics.api.script.ScriptExecEnv;
import com.meshdynamics.meshviewer.mesh.INetworkListener;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.SyncEvent;

public class AlertManager implements INetworkListener, IAPPropListener {

	private NMS 					nmsInstance;
	private NMSUI					nmsUIInstance;
	private	Vector<ScriptExecEnv>	scriptExecEnvs;
	private SyncEvent				execEvent;
	private LinkedList<String> 		eventList;
	private Thread					eventThread;
	private boolean					quit;
	
	public AlertManager(MeshViewerUI meshViewerUI) {
		nmsInstance 		= NMS.getInstance();
		nmsUIInstance		= meshViewerUI.getNMSUI();
		scriptExecEnvs		= new Vector<ScriptExecEnv>();
		try {
			execEvent 		= SyncEvent.create(SyncEvent.STATE_NOT_SIGNALLED, true, null);
			eventList		= new LinkedList<String>();
			quit 			= false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		loadScripts();
	}

	private void loadScripts() {
		
		try {
			String 		alertScriptDirPath	= MFile.getAlertScriptPath();
			File 		file 				= new File(alertScriptDirPath);
			String[] 	files 				= file.list();
			
			if(files == null)
				return;
			
			if(files.length <= 0) {
				return;
			}
			
			for(int i=0;i<files.length;i++) {
				
				FileReader 		scriptReader 	= new FileReader(alertScriptDirPath + File.separator + files[i]);
				ScriptExecEnv	scriptExecEnv	= null;
				
				if(files[i].endsWith(".rb") == true) {
					scriptExecEnv	= new ScriptExecEnv("jruby");
				} else if(files[i].endsWith(".js") == true) {
					scriptExecEnv	= new ScriptExecEnv("js");
				} 
				
				if(scriptExecEnv == null) {
					scriptReader.close();
					continue;
				}

				scriptExecEnv.importClass("com.meshdynamics.api.NMSUI");
				scriptExecEnv.defineVariable("nmsui", nmsUIInstance);
				scriptExecEnv.execute(scriptReader);

				scriptExecEnvs.add(scriptExecEnv);
			}

		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Override
	public void accessPointAdded(AccessPoint accessPoint) {
		accessPoint.addPropertyListener(this);
	}

	@Override
	public void accessPointDeleted(AccessPoint accessPoint) {
		accessPoint.removePropertyListener(this);
	}

	@Override
	public void meshViewerStarted() {
		// TODO Auto-generated method stub
	}

	@Override
	public void meshViewerStopped() {
		// TODO Auto-generated method stub
	}

	@Override
	public void notifyHealthAlert(int healthStatus, String meshId) {
		// TODO Auto-generated method stub
	}

	@Override
	public void saturationInfoReceived(AccessPoint ap, String ifName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void apPropertyChanged(int filter, int subFilter, final AccessPoint src) {
		
		if(filter != IAPPropListener.MASK_HEARTBEAT2) 
			return;

		if(eventThread == null) {
			
			eventThread = new Thread(new Runnable(){
				@Override
				public void run() {
					
					while(quit == false) {
						
						while(true) {
							
							String nodeInfo = null;
							synchronized (eventList) {
								try {
									nodeInfo		= eventList.removeFirst();
									String[] tokens	= nodeInfo.split("#");
									if(tokens.length != 2)
										continue;
									
									Network nw 		= nmsInstance.getNetworkByName(tokens[0]);
									if(nw == null)
										continue;
									
									Node 	node	= nw.getNodeByMacAddress(tokens[1]);
									if(node == null)
										continue;
									
									for(ScriptExecEnv env : scriptExecEnvs) {
										env.invokeFunction("checkAlert", nw, node);
									}
								} catch(NoSuchElementException e) {
									break;
								} catch(Exception e) {
									System.out.println(e.getMessage());
								}
								
							}
						}
						
			            execEvent.resetEvent();
			            execEvent.waitOn();                

					}
					
					eventThread = null;
				}
			});
			eventThread.start();
		}
		
		String nodeInfo = src.getNetworkId() + "#" + src.getDSMacAddress();
		synchronized (eventList) {
			eventList.addLast(nodeInfo);
		}
		execEvent.signalEvent();
	}

	void stopServices() {
		quit = true;
		execEvent.signalEvent();
	}

}
