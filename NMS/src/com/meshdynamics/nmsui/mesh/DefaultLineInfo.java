/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : LineInfo.java
 * Comments : 
 * Created  : Sep 30, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  3  |June 21, 2006|fix for bitrate color display	                 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  2  |May 5, 2006  |Arrow drawing and bitrate display                |Prachiti|
 * --------------------------------------------------------------------------------
 * |  1  |Jan 17, 2006 | Null check added for Line drawing               | Mithil |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 30, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.mesh;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import com.meshdynamics.nmsui.mesh.views.AccessPointHandler;
import com.meshdynamics.nmsui.mesh.views.AccessPointUI;
import com.meshdynamics.util.Geometry;
import com.meshdynamics.util.MacAddress;

public class DefaultLineInfo extends LineInfo {
        
    private int			lineWidth;
    private int 		lineStyle;
    private MacAddress	bssid;
    private Color		lineColor;
    private	String		info;
    private Point		srcIntersectionPoint;
    private Point		dstIntersectionPoint;
    private Rectangle	bitrateCoordinates;
    
    
    public DefaultLineInfo(ILinePoint srcPoint, ILinePoint dstPoint) {
        super(srcPoint, dstPoint);
        
        lineWidth				= 1;
        lineStyle				= SWT.LINE_SOLID;
        lineColor				= Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);


        srcIntersectionPoint	= new Point(0,0);
        dstIntersectionPoint	= new Point(0,0);
        bitrateCoordinates		= null; 	
        
    }
    
    public void drawLine(GC gc) {

    	if(this.dstPoint == null) {
    		return;
    	}
    	
    	Color defaultBk 	= gc.getBackground();
	 	int oldLineStyle 	= gc.getLineStyle();
		int oldLineWidth 	= gc.getLineWidth();
		
    	gc.setLineWidth(lineWidth);
    	gc.setLineStyle(lineStyle);
    	gc.setForeground(lineColor);

    	if(srcPoint.getPoint() == null || dstPoint.getPoint() == null)
       		return;
       
    	gc.drawLine(srcPoint.getPoint().x, srcPoint.getPoint().y, dstPoint.getPoint().x, dstPoint.getPoint().y);
       
    	if(lineType == LINE_KAP || lineType == LINE_DS_WIRELESS){
           
			AccessPointHandler 	apH 	= (AccessPointHandler)srcPoint;
			AccessPointUI 		apUI 	= apH.getUI();
			Rectangle  			bounds 	= apUI.getBounds();
			
			if(EthLinePoint.getIntersectPoint(bounds,srcPoint,dstPoint,srcIntersectionPoint) == false) {
				return;
			}
			
			apH 	= (AccessPointHandler)dstPoint;
		 	apUI 	= apH.getUI();
		 	bounds 	= apUI.getBounds();
		 	
		 	if(EthLinePoint.getIntersectPoint(bounds,srcPoint,dstPoint,dstIntersectionPoint) == false) {
		 		return;
		 	}
		 	
		 	dstIntersectionPoint.x  = (int)((srcIntersectionPoint.x + dstIntersectionPoint.x) * 0.5);
		 	dstIntersectionPoint.x  = (int)((srcIntersectionPoint.x + dstIntersectionPoint.x) * 0.5);
		 	dstIntersectionPoint.y  = (int)((srcIntersectionPoint.y + dstIntersectionPoint.y) * 0.5);
		 	dstIntersectionPoint.y  = (int)((srcIntersectionPoint.y + dstIntersectionPoint.y) * 0.5);
			     
		 	int poly[] 		= Geometry.createArrow(srcIntersectionPoint.x,srcIntersectionPoint.y,
		 									dstIntersectionPoint.x,dstIntersectionPoint.y,5,Math.PI /4,1);
		 	
			
	 	    gc.setForeground(lineColor);
		    gc.setBackground(lineColor);
		    gc.drawPolygon(poly);
			gc.fillPolygon(poly);
		    gc.setBackground(defaultBk);
		    drawBitRate(gc,srcIntersectionPoint,dstIntersectionPoint,info,lineColor,defaultBk);
    	}
    	
    	gc.setForeground(defaultBk);
		gc.setLineStyle(oldLineStyle);
		gc.setLineWidth(oldLineWidth);
    }
    
    public void drawBitRate(GC gc,Point srcPoint,Point dstPoint,String bitRate,Color foregrnd,Color bkgrnd ){
        
    	double x = 0;
    	double y = 0;
    	
        gc.setForeground(foregrnd);
	    gc.setBackground(bkgrnd);
	    
		x	= (srcPoint.x + dstPoint.x) * 0.5;
		y 	= (srcPoint.y + dstPoint.y) * 0.5;
				
		double centrex	= (double)(x + 0.5);
		double centrey	= (double)(y + 0.5);
		
		if(bitRate != null) {
			if(bitrateCoordinates == null) {
				bitrateCoordinates = new Rectangle(0,0,0,0);
			}
			bitrateCoordinates.x		= (int) centrex;
			bitrateCoordinates.y		= (int) centrey;
			bitrateCoordinates.height	= gc.getFontMetrics().getHeight();
			bitrateCoordinates.width	= gc.stringExtent(" "+ bitRate + " ").x;
		    gc.drawText(bitRate,(int)centrex,(int)centrey,SWT.DRAW_TRANSPARENT);
		}
        
    }
    public Point drawArrow(GC gc,Point srcPoint,Point dstPoint){
        
        Point		srcpt	 			= new Point(0,0);
    	Point		dstpt 				= new Point(0,0);
    	Point	 	rotation_point 		= new Point(0,0);
    	Point		rotated_dst			= new Point(0,0);
    	Point		arrow_up			= new Point(0,0);
    	Point		arrow_down			= new Point(0,0);
    	double		slope = 0;
		double 		angle = 0;
		int			arrow_diff;
		double		arrow_angle;
		
		arrow_diff 	= 8;
		arrow_angle = Math.PI /4;
        if(srcPoint.x != dstPoint.x) {
       		slope	= (double)( dstPoint.y - srcPoint.y)/(double)(dstPoint.x - srcPoint.x);
       		angle 	= Math.atan(slope);
	    } else {
	    	angle	= Math.PI/2;	
	    }
      
        dstpt.x  = (int)((srcPoint.x + dstPoint.x) * 0.5);dstpt.x = (int)((dstpt.x + srcPoint.x) * 0.5);
        dstpt.y  = (int)((srcPoint.y + dstPoint.y) * 0.5);dstpt.y = (int)((dstpt.y + srcPoint.y) * 0.5);
	    
	    srcpt.x 			= srcPoint.x;
	    srcpt.y				= srcPoint.y;
	
	    rotation_point 		= srcpt;
	    
	    if(srcPoint.x != dstPoint.x) {
	    	
	    	_rotate(dstpt,rotation_point,angle,rotated_dst);
	    	
	       	//gc.drawLine(srcpoint.x, srcpoint.y, rotated_dst.x, rotated_dst.y);
	    	
	       	arrow_up.x = rotated_dst.x;
	       	arrow_up.y = rotated_dst.y - arrow_diff;
	
	       	arrow_down.x = rotated_dst.x;
	       	arrow_down.y = rotated_dst.y + arrow_diff;
	
	       	_rotate(arrow_up,rotated_dst,arrow_angle *3,arrow_up);
	       	_rotate(arrow_down,rotated_dst,arrow_angle,arrow_down);	         
     	
	       	if(srcPoint.x > dstPoint.x) {	       	
	       		_rotate(arrow_up,rotation_point,Math.PI - angle,arrow_up);
	       		_rotate(arrow_down,rotation_point,Math.PI -angle,arrow_down);
	       	} else {
	       		_rotate(arrow_up,rotation_point,-angle,arrow_up);
	       		_rotate(arrow_down,rotation_point,-angle,arrow_down);	       		
	       	}
	    } else {
	    	
    		arrow_up.x  = dstpt.x - arrow_diff;
    		arrow_up.y  = dstpt.y;
    		
    		arrow_down.x = dstpt.x + arrow_diff;
    		arrow_down.y = dstpt.y;  
    		
    		_rotate(arrow_up,dstpt,-arrow_angle,arrow_up);
    		_rotate(arrow_down,dstpt,-arrow_angle*3,arrow_down);     
	    }
	
	   gc.drawLine(arrow_up.x, arrow_up.y, dstpt.x, dstpt.y);
	   gc.drawLine(arrow_down.x, arrow_down.y, dstpt.x, dstpt.y);
	   
	   int poly[] = new int[12];
	   poly[0] = arrow_up.x;
	   poly[1] = arrow_up.y;
	   poly[2] = dstpt.x;
	   poly[3] = dstpt.y;
	   
	   poly[4] = arrow_up.x;
	   poly[5] = arrow_up.y;
	   poly[6] = arrow_down.x;
	   poly[7] = arrow_down.y;
	   
	   poly[8] = arrow_down.x;
	   poly[9] = arrow_down.y;
	   poly[10] = dstpt.x;
	   poly[11] = dstpt.y;
	   
	   int oldLineStyle = gc.getLineStyle();
	   int oldLineWidth = gc.getLineWidth();
	   gc.setLineStyle(lineStyle);
       gc.setForeground(lineColor );
       Color defaultBk = gc.getBackground(); 
       gc.setBackground(lineColor );
       
	   gc.drawPolygon(poly);
	   gc.fillPolygon(poly);
	   
	   gc.setForeground(defaultBk);
       gc.setBackground(defaultBk);
       gc.setLineStyle(oldLineStyle);
       gc.setLineStyle(oldLineWidth);
	   return dstpt;
    }
	public void drawCircle(GC gc) {
	    Color 	oldLineColor 	= gc.getForeground();
	    int		oldLineStyle	= gc.getLineStyle();
	    int		oldLineWidth	= gc.getLineWidth();
	    
		AccessPointHandler 	apH = (AccessPointHandler)srcPoint;
		AccessPointUI 		apUI = apH.getUI();
		Rectangle  bounds = apUI.getBounds();
		gc.setForeground(lineColor);
		gc.setLineStyle(lineStyle);
		gc.setLineWidth(lineWidth);
		gc.drawRoundRectangle( bounds.x-5,bounds.y-5,bounds.width + 10,bounds.height + 10,20,20);
		
		gc.setForeground(oldLineColor);
		gc.setLineStyle(oldLineStyle);
		gc.setLineWidth(oldLineWidth);
	}
    
    public void setLineType(int lnType,Color newLineColor) {
        lineType = lnType;
        lineColor = newLineColor;
        switch(lineType) {
        case LINE_DS_ETH:
            lineWidth		= 3;
            lineStyle		= SWT.LINE_SOLID;
            //lineColor		= Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);
            break;
        
        case LINE_DS_WIRELESS:
            lineWidth		= 1;
            lineStyle		= SWT.LINE_SOLID;
            //lineColor		= Display.getCurrent().getSystemColor(SWT.COLOR_BLUE);
            break;
            
        case LINE_KAP:
            lineWidth		= 1;
            lineStyle		= SWT.LINE_SOLID;
            //lineColor		= new Color(null,127,127,127);
            break;
        }
    }
    
    public int getLineType() {
        return lineType;
    }
    
    /**
     * @return Returns the bssid.
     */
    public MacAddress getBssid() {
        return bssid;
    }
    
    /**
     * @param bssid The bssid to set.
     */
    public void setBssid(MacAddress bssid) {
        this.bssid = bssid;
    }
    
	/**
	 * @return Returns the lineColor.
	 */
	public Color getLineColor() {
		return lineColor;
	}
	
	/**
	 * @param lineColor The lineColor to set.
	 */
	public void setLineColor(Color lineColor) {
		this.lineColor = lineColor;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.LineInfo#drawCircle(org.eclipse.swt.graphics.GC)
	 */
	
	
	static void _cartesian_to_polar(Point point, _polar_coordinate polar_point)
	{
		double	val = (double)point.x * (double)point.x + (double)point.y * (double)point.y;
		
		polar_point.r	= Math.sqrt(val);
		
		if(point.x != 0)
			polar_point.theta		= Math.atan((double)point.y / (double)point.x);
		else
			polar_point.theta		= 22.0/14.0;
	}

	static void _polar_to_cartesian(_polar_coordinate polar_point, Point point)
	{
		double	x,y;
		x	= polar_point.r * Math.cos(polar_point.theta);
		y	= polar_point.r * Math.sin(polar_point.theta);
		if(x < 0)
			point.x = (int)(x - 0.5);
		else
			point.x = (int)(x + 0.5);
		if(y < 0)
			point.y = (int)(y - 0.5);
		else
			point.y = (int)(y + 0.5);
	}

	public static void _rotate(Point point,Point rotation_point,double angle,Point rotated_point)
	{
		int						py,rpy;
		Point					shifted_point_to_rotate = new Point(0,0);
		_polar_coordinate		angle_point_polar		= new _polar_coordinate() ;
		_polar_coordinate		point_to_rotate_polar	= new _polar_coordinate();
		_polar_coordinate		rotated_point_polar		= new _polar_coordinate();

		/**
		 * Origin shift the point w.r.t rotation_point
		 */

		py = point.y * -1;
		rpy = rotation_point.y * -1;
		
		shifted_point_to_rotate.x	= point.x - rotation_point.x;
		shifted_point_to_rotate.y	= py - rpy;

		angle_point_polar.r			= 1.0;
		angle_point_polar.theta		= angle;

		_cartesian_to_polar(shifted_point_to_rotate,point_to_rotate_polar);

		rotated_point_polar.r		= point_to_rotate_polar.r;
		rotated_point_polar.theta	= point_to_rotate_polar.theta + angle_point_polar.theta;

		_polar_to_cartesian(rotated_point_polar,rotated_point);

		/**
		 * Origin un-shift the point w.r.t rotation_point
		 */

		rotated_point.x += rotation_point.x;
		rotated_point.y += rpy;
		
		rotated_point.y *= -1;
	}
	
    /**
     * @return Returns the arrowDest.
     */
    public String getInfo() {
        if(info != "" && info != null)
            return info;
        return "";
    }
    
    public void setInfo(String newInfo) {
        if(newInfo != null && newInfo != "")
            this.info = "" + newInfo;
        else
            this.info = "";
    }
    
	public Rectangle getBitrateCoordinates() {
		return bitrateCoordinates;
	}
}

class _polar_coordinate {
	double	r;
	double	theta;
}

