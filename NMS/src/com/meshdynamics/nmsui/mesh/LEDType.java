/*
 * MeshDynamics 
 * -------------- 
 * File     : LEDManager.java
 * Comments : 
 * Created  : May 5, 2005
 * Author   : Sneha
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * |  0  |May 5, 2005    | Created	                                 | Sneha         |
 * ----------------------------------------------------------------------------------
 */

package com.meshdynamics.nmsui.mesh;


public class LEDType {

	public int 		id;
	public String   description;	
	
	public LEDType() {
	}
	
}
