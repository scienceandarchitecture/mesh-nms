/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ILinePoint.java
 * Comments : 
 * Created  : Oct 1, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 1, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.mesh;

import org.eclipse.swt.graphics.Point;

public interface ILinePoint {
    public Point 		getPoint();
}
