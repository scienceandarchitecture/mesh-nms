/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : IMeshNetworkListener.java
 * Comments : 
 * Created  : May 15, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |May 15, 2007  | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.mesh;

import com.meshdynamics.meshviewer.mesh.IMeshNetworkStatus;

public interface INetworkStatusListener {

	public void networkCreated			(String networkName);
	public void networkDeleted			(String networkName);
	public void networkSelected			(String networkName);
	
	public void networkStatusChanged	(IMeshNetworkStatus status);
}
