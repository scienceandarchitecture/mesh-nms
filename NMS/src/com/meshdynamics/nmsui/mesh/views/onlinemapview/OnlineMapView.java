/*
 * Created on Nov 26, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.mesh.views.onlinemapview;

import java.util.Enumeration;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.nmsui.mesh.MeshNetworkUIProperties;
import com.meshdynamics.nmsui.mesh.views.AccessPointHandler;
import com.meshdynamics.nmsui.mesh.views.AccessPointUI;
import com.meshdynamics.nmsui.mesh.views.MeshNetworkView;

/**
 * @author Abhijit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class OnlineMapView extends MeshNetworkView {
	
	private Browser		browser;
	private String 		currentSelectedAp;
	
	/**
	 * @param arg0
	 * @param arg1
	 * @param networkUI
	 */
	public OnlineMapView(Composite arg0, int arg1, MeshNetworkUI networkUI) {
		super(arg0, arg1, networkUI);
		browser	= new Browser(this, SWT.BORDER);
		this.setLayout(new FillLayout());
	
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#onClose()
	 */
	public void onClose() {
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#accessPointAdded(com.meshdynamics.meshviewer.mesh.AccessPoint)
	 */
	public void accessPointAdded(AccessPoint accessPoint) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#accessPointDeleted(com.meshdynamics.meshviewer.mesh.AccessPoint)
	 */
	public void accessPointDeleted(AccessPoint accessPoint) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#selectAccessPoint(java.lang.String)
	 */
	public void selectAccessPoint(String apDsMacAddress) {
		currentSelectedAp = apDsMacAddress;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#deleteAccessPoint(java.lang.String)
	 */
	public void deleteAccessPoint(String dsMacAddress) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#load(java.io.InputStream)
	 */
	public void load() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#save(java.io.OutputStream)
	 */
	public void save() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#notifyHealthAlert(int, java.lang.String)
	 */
	public void notifyHealthAlert(int healthStatus, String meshId) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#maximizeAllNodes()
	 */
	public void maximizeAllNodes() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#minimizeAllNodes()
	 */
	public void minimizeAllNodes() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#setGroupSelectionEnabled(boolean)
	 */
	public void setGroupSelectionEnabled(boolean groupSelectionEnabled) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#isGroupSelected(java.lang.String)
	 */
	public boolean isGroupSelected(String dsAddress) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#hasAccessPoint(java.lang.String)
	 */
	public boolean hasAccessPoint(String dsAddress) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#editProperties()
	 */
	public boolean editProperties() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#getSelectedApAddress()
	 */
	public String getSelectedApAddress() {
		return currentSelectedAp;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#getProperties()
	 */
	public MeshNetworkUIProperties getProperties() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#getAccesspointHandlers()
	 */
	public Enumeration<AccessPointHandler> getAccesspointHandlers() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#apPropertyChanged(int, int, com.meshdynamics.meshviewer.mesh.AccessPoint)
	 */
	public void apPropertyChanged(int filter, int subFilter, AccessPoint src) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#setVisible(boolean)
	 */
	public void setVisible(boolean arg0) {
		super.setVisible(arg0);
		if(arg0 == false) {
			//browser.setUrl("http://localhost:12345/stop");
		} else {
			String url = "http://localhost:12345/mesh.html::" + networkUI.getNwName();
			String existingUrl = browser.getUrl();
			
			if(existingUrl.equalsIgnoreCase(url) == false) {
				browser.setUrl(url);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#groupSelectionChanged(com.meshdynamics.meshviewer.mesh.ui.views.DefaultView.AccessPointUI)
	 */
	public void groupSelectionChanged(AccessPointUI pointUI) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#selectionChanged(boolean)
	 */
	public void selectionChanged(boolean selected) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected AccessPointHandler getApHandler(String dsMacAddress) {
		// TODO Auto-generated method stub
		return null;
	}

}
