/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshNetworkView.java
 * Comments : 
 * Created  : Sep 04, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 04, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/
package com.meshdynamics.nmsui.mesh.views;

import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;
import com.meshdynamics.nmsui.AccessPointMenu;
import com.meshdynamics.nmsui.mesh.DefaultLineInfo;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.nmsui.mesh.MeshNetworkUIProperties;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI.NodeDisplayText;
import com.meshdynamics.util.MacAddress;
import org.eclipse.swt.widgets.Display;

public abstract class MeshNetworkView extends Canvas {
	
	protected MeshNetworkUI 						networkUI;
	protected MeshNetworkUIProperties 				properties;
	protected Hashtable<String, AccessPointHandler> accessPointHandlers;
	protected boolean 								groupSelectionEnabled;
	protected AccessPointHandler					selectedAp;
	protected Hashtable<String, AccessPointUI> 		accessPointUIs;
	protected Hashtable<String, String>				groupSelection;
	
	public MeshNetworkView(Composite arg0, int arg1, MeshNetworkUI networkUI) {
		super(arg0, arg1);
		this.networkUI			= networkUI;
		properties				= networkUI.getProperties();
		accessPointHandlers		= new Hashtable<String, AccessPointHandler>();
		accessPointUIs			= new Hashtable<String, AccessPointUI>();
		groupSelection			= new Hashtable<String, String>();
		selectedAp				= null;
	}
	
	public abstract void 							onClose						();
	public abstract void 							load						();
	public abstract void 							save						();
	public abstract void 							notifyHealthAlert			(int healthStatus, String meshId);
	public abstract boolean 						editProperties				();
	public abstract void 							apPropertyChanged			(int filter, int subFilter, AccessPoint src);
	public abstract void 							selectionChanged			(boolean selected);
	
	protected abstract AccessPointHandler getApHandler(String dsMacAddress);
	
	public void parentChanged(AccessPointHandler handler) {
	    
	}
	public void parentInfoChanged(AccessPointHandler handler) {
	    
	}
	public void knownApListChanged(AccessPointHandler handler) {
	    
	}
	public  void knownApInfoChanged(AccessPointHandler handler,DefaultLineInfo line) {
	}
	
	public void lineStateChanged(AccessPointHandler handler) {
	    
	}
	public void  lineInfoStateChanged(AccessPointHandler handler) {
	    
	}

	public AccessPointHandler getAccesspointHandler(MacAddress dsMacAddress) {
        return accessPointHandlers.get(dsMacAddress.toString());
	}
	
	public void meshViewerStarted() {
		
	}
	
	public void meshViewerStopped() {
		
	}

	public boolean isNodeMoveAllowed() {
		return true;
	}

	public AccessPointMenu getAccessPointMenu() {
		return networkUI.getAccessPointMenu();
	}

	public boolean isGroupSelectionEnabled() {
		return networkUI.isGroupSelectionEnabled();
	}

	public void addNodeDisplayText(NodeDisplayText t) {
		AccessPointHandler apHandler = accessPointHandlers.get(t.nodeId);
		if(apHandler == null)
			return;
		
		AccessPointUI apUI = apHandler.getUI();
		apUI.addNodeDisplayText(t);
	}

	public void removeNodeDisplayText(NodeDisplayText t) {
		AccessPointHandler apHandler = accessPointHandlers.get(t.nodeId);
		if(apHandler == null)
			return;
		
		AccessPointUI apUI = apHandler.getUI();
		apUI.removeNodeDisplayText(t);
	}

	public void updateNodeDisplayText(NodeDisplayText t) {
		AccessPointHandler apHandler = accessPointHandlers.get(t.nodeId);
		if(apHandler == null)
			return;
		
		AccessPointUI apUI = apHandler.getUI();
		apUI.updateNodeDisplayText(t);
	}

	public void apSelectionChanged(String apDsMacAddress) {
		selectAccessPoint(apDsMacAddress);
		networkUI.apSelectionChanged(this, apDsMacAddress);
	}

    protected void notifyStatusBar() {
    	
    	String statusText	 	= "";	    	
    	groupSelection.clear();
    	networkUI.notifyStatusBar("");
    	
		if(groupSelectionEnabled == true) {
			Enumeration<AccessPointHandler> e = accessPointHandlers.elements();
			while(e.hasMoreElements()) {
				AccessPointHandler apHandler = e.nextElement();
				if(apHandler.isGroupSelected() == true) {
					String apAddress = apHandler.getDSMacAddr().toString();
					groupSelection.put(apAddress, apAddress);
				}
			}
	    } else {
	    	String selectedApAddress = getSelectedApAddress();
	    	if(selectedApAddress != null) {
	    		groupSelection.put(selectedApAddress, selectedApAddress);
	    	}
	    	
	    }
		
		statusText = "NODES = " + accessPointHandlers.size() + ", RUNNING = " + networkUI.getAliveApCount() + ", SELECTED = " + groupSelection.size(); 
		
		networkUI.notifyStatusBar(statusText);
    }

    public void selectAccessPoint(String apDsMacAddress) {
    	selectedAp = accessPointHandlers.get(apDsMacAddress);
    	
    	Enumeration<AccessPointHandler> enumeration = accessPointHandlers.elements();
        while(enumeration.hasMoreElements()) {
        	AccessPointHandler obj = enumeration.nextElement();
            if(obj.equals(selectedAp) == false) {
                obj.setSelected(false);
            }
        }
        
        selectedAp.setSelected(true);
        notifyStatusBar();
    }
    
    protected void groupSelectionChanged() {
    	notifyStatusBar();
    }
    
	public AccessPointHandler getAccesspointHandler(String dsMacAddress) {
		return accessPointHandlers.get(dsMacAddress);
	}

	public AccessPointUI getAccesspointUI(String dsMacAddress) {
		return accessPointUIs.get(dsMacAddress);
	}
    
	public boolean isGroupSelected(MacAddress macaddress) {
		AccessPointHandler apHandler = accessPointHandlers.get(macaddress.toString());
		return apHandler.isGroupSelected();
	}
	
	public void setGroupSelectionEnabled(boolean groupSelectionEnabled) {

		this.groupSelectionEnabled = groupSelectionEnabled;
		
		Enumeration<AccessPointUI> 	e	= accessPointUIs.elements();
    	while (e.hasMoreElements()) {
			AccessPointUI  	apUI	= e.nextElement();
			apUI.uiEventSetGroupSelect(false);
    	}
		
	}
	
	public void maximizeAllNodes() {
		Enumeration<AccessPointUI> enumeration = accessPointUIs.elements();
		while(enumeration.hasMoreElements()) {
			AccessPointUI apUI = enumeration.nextElement();
			apUI.setControlDetails(AccessPointUI.CONTROL_MAXIMIZED);
		}
		redraw();
	}

	public void minimizeAllNodes() {
		Enumeration<AccessPointUI> enumeration = accessPointUIs.elements();
		while(enumeration.hasMoreElements()) {
			AccessPointUI apUI = enumeration.nextElement();
			apUI.setControlDetails(AccessPointUI.CONTROL_MINIMIZED);
		}
		redraw();
	}

	public int getGroupSelectedNodes() {
		return groupSelection.size();
	}
	
	public boolean isGroupSelected(String dsAddress) {
		
		AccessPointUI apUI = (AccessPointUI) accessPointUIs.get(dsAddress);
		if(apUI == null) {
			return false;
		}
		
		return apUI.isGroupSelected();
	}

    public void accessPointAdded(final AccessPoint accessPoint) {
    	AccessPointHandler apHandler = getApHandler(accessPoint.getDsMacAddress().toString());
    	apHandler.setAccessPoint(accessPoint);
        apHandler.apPropertyChanged(IAPPropListener.MASK_HWINFO, IAPPropListener.MASK_ALL, accessPoint);
        notifyStatusBar();
        return;
    } 
	
	public void accesspointStarted(AccessPointHandler handler) {
		Enumeration<AccessPointHandler> apEnum = accessPointHandlers.elements();
		
		while(apEnum.hasMoreElements()) {
			AccessPointHandler apHandler = (AccessPointHandler) apEnum.nextElement();
			
			if(handler.equals(apHandler) == true) {
				continue;
			}
			
			apHandler.apStarted(handler);
		}

		notifyStatusBar();
		
		if(isDisposed() == false) {
			redraw();
		}

	}

	public void accesspointStopped(AccessPointHandler handler) {
		Enumeration<AccessPointHandler> apEnum = accessPointHandlers.elements();
		
		while(apEnum.hasMoreElements()) {
			AccessPointHandler apHandler = (AccessPointHandler) apEnum.nextElement();
			
			if(handler.equals(apHandler) == true) {
				continue;
			}
			
			apHandler.apStopped(handler);
		}

		notifyStatusBar();
		if(isDisposed() == false) {
		    redraw();//Rect(getAPSEnclosingRegion());
		}
	}
	
    public void deleteAccessPoint(String dsMacAddress) {
    	
        try{
        	AccessPointUI 		apUI 		= (AccessPointUI)accessPointUIs.remove(dsMacAddress);
        	accessPointHandlers.remove(dsMacAddress);
        	if(apUI.isDisposed() == false)
        		apUI.dispose();

        	redraw();
            notifyStatusBar();
        }catch(Exception e){
        	e.printStackTrace();
        	Mesh.logException(e);
        }
	}
	
	public Enumeration<AccessPointHandler> getAccesspointHandlers() {
		return accessPointHandlers.elements();
	}
    
	public MeshNetworkUIProperties getProperties() {
		return networkUI.getProperties();
	}
	
	public String getSelectedApAddress() {
		return (selectedAp == null) ? null : selectedAp.getDSMacAddr().toString();
	}
	
	public boolean hasAccessPoint(String dsAddress) {
		return accessPointHandlers.containsKey(dsAddress);
	}

	public void groupSelectAp(String apDsMacAddress, boolean selected) {
		AccessPointUI apUI = accessPointUIs.get(apDsMacAddress);
		if(apUI == null)
			return;
		
		apUI.uiEventSetGroupSelect(selected);
		return;
	}

	public void setNodeElementProperty(String nodeId, String property, String value) {
		AccessPointUI apUI = accessPointUIs.get(nodeId);
		if(apUI == null)
			return;
		
		if(apUI.getApState() == AccessPointUI.STATE_DEAD)
			return;
		
		apUI.setElementProperty(property, value);
	}

	public Enumeration<String> getGroupSelection() {
		return groupSelection.elements();
	}

	public Enumeration<String> getApAddresses() {
		return accessPointHandlers.keys();
	}

    public void ShowRoot(GC gc, AccessPointUI apUI) {
       if(null == gc || null == apUI){
            return;
        }

        //  Check the settings to display this circle to indicate the access point or not
        if(properties.getAssociationLines() == MeshNetworkUIProperties.ASSOCIATIONLINE_STATE_SHOW &&
                properties.getRootLine()==MeshNetworkUIProperties.ROOTLINE_STATE_HIDE) {
            Color oldLineColor 	= gc.getForeground();
            int		oldLineStyle	= gc.getLineStyle();
            int		oldLineWidth	= gc.getLineWidth();
            Rectangle  rootBounds = apUI.getBounds();

            gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
            gc.setLineStyle(SWT.LINE_SOLID);
            gc.setLineWidth(2);
            gc.drawRoundRectangle( rootBounds.x-5,rootBounds.y-5,rootBounds.width + 10,rootBounds.height + 10,20,20);

            gc.setForeground(oldLineColor);
            gc.setLineStyle(oldLineStyle);
            gc.setLineWidth(oldLineWidth);
        }
    }


}
