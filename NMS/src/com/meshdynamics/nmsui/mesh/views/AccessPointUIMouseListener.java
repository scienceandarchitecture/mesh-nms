/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : AccessPointUIMouseListener.java
 * Comments : 
 * Created  : Oct 11, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * | 22  |May 24, 2007 |group selection implementation					 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 21  |Mar 26, 2007 |chk for Root added in mouseDown for neighbourLines|Imran  |
 * --------------------------------------------------------------------------------
 * | 20  |Dec 28, 2006 |kap-lines,bitrate properties implemented 		 |Abhijit |
 * ----------------------------------------------------------------------------------
 * | 19  |Nov 19, 2006 |Msg box added on selection of dead node 		 |Abhishek|
 * ----------------------------------------------------------------------------------
 * | 18  |Oct 15, 2006 | Mouseclick buttons confusion fixed		         |Abhijit |
 * --------------------------------------------------------------------------------
 * | 17  |Oct 06, 2006 | signal added to line				             |Bindu   |
 * --------------------------------------------------------------------------------
 * | 16  |July 07,2006 | RF Space dialog handler removed                 |Prachiti|
 * --------------------------------------------------------------------------------
 * | 15  |Jun 15, 2006 | misc fix 										 | Bindu  |
 * --------------------------------------------------------------------------------
 * | 14  |Jun 15, 2006 | misc fix for RF dlg for dca = 0				 | Bindu  |
 * --------------------------------------------------------------------------------
 * | 13  |Jun 14, 2006 | misc fix for RF dlg							 | Bindu  |
 * --------------------------------------------------------------------------------
 * | 12  |May 25, 2006 | Toggle added to show/hide KnownAPLines          |Prachiti|
 *  -------------------------------------------------------------------------------
 * | 11  | May 31, 2006| Misc UI Fixes      			                 |Bindu   |
 * --------------------------------------------------------------------------------
 * | 10  | May 25, 2006| Misc UI Fixes      			                 |Bindu   |
 * --------------------------------------------------------------------------------
 * |  9  | May 23, 2006| Misc UI Fixes      			                 |Bindu   |
 * --------------------------------------------------------------------------------
 * |  8  | May 23, 2006| ifname add to DLS dlg			                 |Bindu   |
 * --------------------------------------------------------------------------------
 * |  7  | May 22, 2006| UI Fixes						                 |Bindu   |
 * --------------------------------------------------------------------------------
 * |  6  | May 19, 2006| UI Updation on DL Saturation Packet receive     |Bindu   |
 * --------------------------------------------------------------------------------
 * |  5  |May 19, 2006 | Toggle added to show/hide bitrate               |Prachiti|
 *  --------------------------------------------------------------------------------
 * |  4  |May 12, 2006 | Changes for DL Saturation					     | Bindu  |
 *  -------------------------------------------------------------------------------
 * |  3  |Feb 16, 2006 | Config Sequence Number change                   |Prachiti|
 *  -------------------------------------------------------------------------------
 * |  2  | Feb 03,2006 | Generic Request handling done thru flags  		 | Mithil |
 * --------------------------------------------------------------------------------
 * |  1  |Jan 20, 2006 | Group select done								 | Mithil |
 *  -------------------------------------------------------------------------------
 * |  0  |Oct 11, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.mesh.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Control;

import com.meshdynamics.nmsui.AccessPointMenu;
import com.meshdynamics.nmsui.MeshViewerUI;
import com.meshdynamics.nmsui.mesh.MeshNetworkUIProperties;

public class AccessPointUIMouseListener extends MouseAdapter implements MouseMoveListener {

	private AccessPointUI 			apCtrl;
	private Rectangle 				rect;
	private MeshNetworkView 		meshNetworkView;
	private Point 					pt;
	private Cursor					handCursor;
	
	public AccessPointUIMouseListener(MeshNetworkView meshNetworkView, final AccessPointUI apCtrl){
		super();
		this.apCtrl 			= apCtrl;
		this.meshNetworkView	= meshNetworkView;
		rect 					= new Rectangle(0,0,0,0);
		pt 						= new Point(0,0);
		handCursor				= new Cursor(null,SWT.CURSOR_HAND);
	}
		
	@Override
	public void mouseDoubleClick(MouseEvent mouseevent) {
		apCtrl.handleSelectionEvent();
	}

	public void mouseDown(MouseEvent e){
	
		if(e.button == 1) {
		
		  	rect.width 				= e.x;
		    rect.height				= e.y;
		    apCtrl.setCapture(true);
		    apCtrl.setCursor(handCursor);
		    pt.x					= e.x;
		    pt.y					= e.y;
		    
		    if(e.stateMask == SWT.SHIFT) {

		    	if(apCtrl.getApState() != AccessPointUI.STATE_RUNNING) {
		    		return;
		    	}

		    	if(meshNetworkView.isGroupSelectionEnabled() == false) {
			    	return;
		    	}
		    	
		    	boolean newSelectionState = (apCtrl.isGroupSelected() == true) ?
		    									false : true;
		    	apCtrl.uiEventSetGroupSelect(newSelectionState);
			    
		    }else if(e.stateMask == SWT.CTRL) {
		        
		    	/* select nodes show/hide bitrate on the lines */
		    	int info = apCtrl.getShowNeighbourLineInfo();
		        info = (info + 1) % MeshNetworkUIProperties.NEIGHBOURLINE_INFO_STATE_COUNT;
		        apCtrl.setShowNeighbourLineInfo(info);
		       
		    } else if(e.stateMask == SWT.CTRL + SWT.SHIFT) {
			    /* select nodes show/hide known AP lines */
		    	apCtrl.setShowKnownAPLines(!apCtrl.isShowKnownAPLines());
			}

		} else if(e.button == 3){
			apCtrl.setCapture(true);
			AccessPointMenu menu = meshNetworkView.getAccessPointMenu();
			menu.refresh(apCtrl.getDsMacAddress(), apCtrl.getApState());
			
	        apCtrl.setMenu(menu.getMenu());
	        Point location 		= MeshViewerUI.display.map((Control) e.getSource(), null, e.x, e.y);	        
	        menu.getMenu().setLocation(location.x, location.y);

			apCtrl.getMenu();
		}
	}

	public void mouseUp(MouseEvent e){

		if(e.button != 1) {
			return;
		}

	    rect.x = e.x - rect.width;
	    rect.y = e.y - rect.height;

	    if(rect.x == 0 && rect.y == 0) {
	    	apCtrl.setCursor(new Cursor(null, SWT.CURSOR_ARROW));
	        return;
	    }

	    if(apCtrl.isNodeMoveAllowed() == false)
	    	return;

	    Rectangle 	rect  		= meshNetworkView.getBounds();
		Point 		loc 		= apCtrl.getLocation();
		Rectangle 	ctrlBounds 	= apCtrl.getBounds();

		loc.x += (e.x-pt.x);
		loc.y += (e.y-pt.y);

		if(loc.x < 0)
			loc.x = 0;
		if(loc.x > (rect.x+rect.width))
			loc.x = (rect.x+rect.width)- ctrlBounds.width;
		if(loc.y < 0)
			loc.y = 0;
		if(loc.y > (rect.y+rect.height))
			loc.y = (rect.y+rect.height)- ctrlBounds.height;


		apCtrl.setLocation(loc.x, loc.y);
	}

	public void showPopupMenu(Canvas childCan, int x, int y) {
		childCan.setCapture(true);
		AccessPointMenu menu = meshNetworkView.getAccessPointMenu();
		menu.refresh(apCtrl.getDsMacAddress(), apCtrl.getApState());
        childCan.setMenu(menu.getMenu());
        Point location 		= MeshViewerUI.display.map((Control) apCtrl, null, x, y);
        menu.getMenu().setLocation(location.x, location.y);
        childCan.getMenu();

	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.events.MouseMoveListener#mouseMove(org.eclipse.swt.events.MouseEvent)
	 */
	public void mouseMove(MouseEvent arg0) {
		//apCtrl.getParent().redraw();
	}


}
