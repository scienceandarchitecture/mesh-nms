/**********************************************************************************
 * MeshDynamics
 * --------------
 * File     : AccessPointUI.java
 * Comments :
 * Created  : May 4, 2005
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History
 * -------------------------------------------------------------------------------
 * | No  |Date        |  Comment                                        | Author |
 *   --------------------------------------------------------------------------------
 * | 44  |Aug 01, 2007 | mouseDoubleClickedHandler added			    | Imran  |
 * -------------------------------------------------------------------------------
 * | 43  |May 24,2007  | group selection implementation					|Abhijit |
 * -------------------------------------------------------------------------------
 * | 42  |May 09,2007  | removed unused code							|Abhijit |
 * -------------------------------------------------------------------------------
 * | 41  |Mar 21, 2007| led,display text logic changed					|Abhijit |
 * --------------------------------------------------------------------------------
 * | 40  |Mar 14, 2007| RR tooltip on DEAD node is removed      	 	|Abhishek|
 * --------------------------------------------------------------------------------
 * | 39  |Feb 27,2007 |  removed unused code							|Abhijit |
 *---------------------------------------------------------------------------------
 * | 38  |Jan 15, 2007| Degrees symbol added					  	 	|Abhishek|
 * --------------------------------------------------------------------------------
 * | 38  |Dec 28, 2006| kap_lines, bitrate properties added		        |Abhijit |
 * --------------------------------------------------------------------------------
 * | 37  |Nov 27, 2006| clean-up					  			        |  Bindu |
 * --------------------------------------------------------------------------------
 * | 36  |Oct 15,2006 | Removed unused mouselisteners	                |Abhijit |
 * -------------------------------------------------------------------------------
 * | 35  |Oct 06,2006 | Misc Fixes						                |Bindu	 |
 * -------------------------------------------------------------------------------
 * | 34  |July 07,2006| RF Space dialog handler removed                 |Prachiti|
 * -------------------------------------------------------------------------------
 * | 33  |June 20,2006| Reboot Required Displayed                       |Prachiti|
 * -------------------------------------------------------------------------------
 * | 32  |May 23, 2006| Misc Fixes     	                                |Bindu   |
 * -------------------------------------------------------------------------------
 * | 31  |May 22, 2006| Misc Fixes     	                                |Bindu   |
 * -------------------------------------------------------------------------------
 * | 30  |May 19, 2006| UI Updation on Packet receive                   |Bindu   |
 * -------------------------------------------------------------------------------
 * | 29  |May 12, 2006|Changes for DL Saturation					    | Bindu  |
 *  ------------------------------------------------------------------------------
 * | 28  |Mar 27, 2006| Reboot Checker							  	 	| Mithil |
 * -------------------------------------------------------------------------------
 * | 27  |Mar 17, 2006| Reboot Flag update on Nodecolor change	     	| Mithil |
 *  ------------------------------------------------------------------------------
 * | 26  |Mar 17, 2006| Reboot Flag update on Node				     	| Mithil |
 *  ------------------------------------------------------------------------------
 * | 25  |Mar 10, 2006|Node display value changes after heartbeat     	| Mithil |
 *  ------------------------------------------------------------------------------
 * | 24  |Feb 28, 2006|tooltip channel for ixp0 and ixp1 removed       	| Mithil |
 *  ------------------------------------------------------------------------------
 * | 23  |Feb 22, 2006 | Led Color Changes			 				    | Mithil |
 * -------------------------------------------------------------------------------
 * | 22  |Feb 17, 2006|4452 and 4458 tooltip change                    	| Mithil |
 *  ------------------------------------------------------------------------------
 * | 21  |Feb 16, 2006|Config Sequence Number change                    |Prachiti|
 *  ------------------------------------------------------------------------------
 * | 20  |Feb 15, 2006| Reboot Flag updation		                    | Mithil |
 *  ------------------------------------------------------------------------------
 * |  19 | Feb 03,2006 | Generic Request handling done thru flags  		| Mithil |
 * -------------------------------------------------------------------------------
 * |  18 |Feb 03, 2006  | Generic Request		   	 					| abhijit|
 * -------------------------------------------------------------------------------
 * |  17 |Feb 02,2006 | GDI Objects not cleared				       		| Mithil |
 * -------------------------------------------------------------------------------
 * |  16 |Jan 27, 2006 | select done only when picSelect is clicked		| Mithil |
 *  ------------------------------------------------------------------------------
 * |  15 |Jan 20, 2006 | Group select bug								| Mithil |
 *  ------------------------------------------------------------------------------
 * |  14 |Jan 20, 2006 | Group select done								| Mithil |
 *  ------------------------------------------------------------------------------
 * |  13 |Jan 18, 2006 |SCAN mode persists after stopping viewer         |Mithil |
 * -------------------------------------------------------------------------------
 * |  12 |Jan 18,2006 | Node name >= 7 ! 2 be displayed display name + ..|Mithil |
 * -------------------------------------------------------------------------------
 * |  11 |Jan 17,2006 | Node name > 4 ! 2 be displayed display name + ..| Mithil |
 * -------------------------------------------------------------------------------
 * |  10 |Jan 17,2006 | Node name not displayed after starting viewer	| Mithil |
 * -------------------------------------------------------------------------------
 * |  9  |Jan 17,2006 | GDI Objects not cleared				       		| Mithil |
 * -------------------------------------------------------------------------------
 * |  8  |Jan 12,2006 | Root node display faulty when bitrate        	| Mithil |
 * |                    or Sig Strength is selected						|		 |
 * -------------------------------------------------------------------------------
 * |  7  |Jan 12,2006 | Root node should display temperature       		| Mithil |
 * -------------------------------------------------------------------------------
 * |  6  |Jan 04,2006 | Dbm changed to dBm			              		| Mithil |
 * -------------------------------------------------------------------------------
 * |  5  |Nov 29, 2005|Display text problem for root fixed		        | Amit   |
 * ------------------------------------------------------------------------------
 * |  4  |Nov 25, 2005|1037 : EditViewSettings dialog -- when set to	| Mithil |
 * |	 |			  |	display mbps dbm values displayed       		|        |
 * -------------------------------------------------------------------------------
 * |  3  |Nov 09, 2005|1005 :  Tool tip formatting change required      | Mithil |
 * -------------------------------------------------------------------------------
 * |  2  |Oct 19, 2005| Added method for wlan3 appended tooltiptext     | Mithil |
 * -------------------------------------------------------------------------------
 * |  1  |Sep 25, 2005| View properties added to constructor            | Abhijit|
 * -------------------------------------------------------------------------------
 * |  0  |May 4, 2005 | Created                                         | Anand  |
 * -------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.mesh.views.defaultview;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

import com.meshdynamics.api.NMSUI;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.HardwareInfo.InterfaceData;
import com.meshdynamics.nmsui.MeshViewerUI;
import com.meshdynamics.nmsui.dialogs.UIConstants;
import com.meshdynamics.nmsui.mesh.MeshNetworkUIProperties;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI.NodeDisplayText;
import com.meshdynamics.nmsui.mesh.views.AccessPointHandler;
import com.meshdynamics.nmsui.mesh.views.AccessPointUI;
import com.meshdynamics.nmsui.mesh.views.AccessPointUIMouseListener;
import com.meshdynamics.nmsui.mesh.views.MeshNetworkView;
import com.meshdynamics.util.MacAddress;

public class DefaultAccessPointUI extends Canvas implements AccessPointUI {
    
	private static  final int BULB_OFF 					= 0;
	private static  final int BULB_RED 					= 1;
	private static  final int BULB_YELLOW 				= 2;
	private static  final int BULB_GREEN 				= 3;
	private static  final int BULB_ORANGE 				= 4;
	private static  final int BULB_BLUE 				= 5;
	
	private static  final int STATE_DEAD 				= 0;
	private static  final int STATE_SCANNING 			= 1;
	private static  final int STATE_RUNNING				= 2;
	
	private static  final int DISPLAY_TEXT_MODEL		= 0;
	private static  final int DISPLAY_TEXT_NAME 		= 1;
	private static  final int DISPLAY_TEXT_BITRATE		= 2;
	private static  final int DISPLAY_TEXT_SIGNAL 		= 3;
	private static  final int DISPLAY_TEXT_TEMP 		= 4;
	private static  final int DISPLAY_TEXT_VOLTAGE		= 5;
	
	private static  final int DEFAULT_DISPLAY_TEXT_COUNT	= 6;
	
    private static final int BULB_STATUS_OFF			= 0;
    private static final int BULB_STATUS_ON				= 2;

    private static  final int DISPLAY_STATE_NORMAL		= 0;
    private static  final int DISPLAY_STATE_REBOOT		= 1;
    private static  final int DISPLAY_STATE_RADAR		= 2;
	
    private static  final int DISPLAY_STATE_MAX_INDEX	= 3;
	
	private class InterfaceControl {
		public String 				name;
		public int					type;
		public Canvas				picInf;
		public InterfaceControl		next = null;
	};
	
	private class GenericBulb {
		public int				value;
		public Canvas			picBulb;
		public String 			label;	
	}

	private static Image 	_backGroundFull;
    private static Image 	_backGroundFullRoot;
	private static Image 	_backGroundLow;
    private static Image 	_backGroundLowRoot;
	private static Image 	_minimizeButton;
	private static Image 	_maximizeButton;
	private static Image	_bulbImages[];
	private static Image	_adapterTypeImages[];
	
	private static Image 	_selectButton;
	private static Image 	_unselectButton;
	private static Image 	_groupselectButton;
	private static FontData	fVdata;
	private static Font		fontVerdana;

    private boolean _showDetails = false;

	static {
		_backGroundFull			= new Image(null,
									AccessPointUI.class.getResourceAsStream(
									"icons/BackSmall.gif"));
        _backGroundFullRoot			= new Image(null,
                AccessPointUI.class.getResourceAsStream(
                        "icons/BackSmallRoot.png"));
		_backGroundLow			= new Image(null,
									AccessPointUI.class.getResourceAsStream(
									"icons/back_sqr.gif"));
        _backGroundLowRoot			= new Image(null,
                AccessPointUI.class.getResourceAsStream(
                        "icons/back_sqrRoot.png"));
		
		_minimizeButton			= new Image(null,
									AccessPointUI.class.getResourceAsStream(
									"icons/MinButton.gif"));

		_maximizeButton			= new Image(null,
									AccessPointUI.class.getResourceAsStream(
									"icons/MaxButton.gif"));

		_selectButton			= new Image(null,
								AccessPointUI.class.getResourceAsStream(
								"icons/selected.gif"));

		_unselectButton			= new Image(null,
								 AccessPointUI.class.getResourceAsStream(
								 "icons/unselected.gif"));

		_groupselectButton			= new Image(null,
								 AccessPointUI.class.getResourceAsStream(
								 "icons/groupselected.gif"));

		_bulbImages 			= new Image[6];
		_bulbImages[0]			= new Image(null,
										AccessPointUI.class.getResourceAsStream(
										"icons/YellowOff.gif"));
		_bulbImages[1]			= new Image(null,
										AccessPointUI.class.getResourceAsStream(
										"icons/RedOn.gif"));
		_bulbImages[2]			= new Image(null,
										AccessPointUI.class.getResourceAsStream(
										"icons/YellowOn.gif"));
		_bulbImages[3]			= new Image(null,
										AccessPointUI.class.getResourceAsStream(
										"icons/GreenOn.gif"));
		_bulbImages[4]			= new Image(null,
										AccessPointUI.class.getResourceAsStream(
										"icons/OrengeOn.gif"));
		_bulbImages[5]			= new Image(null,
										AccessPointUI.class.getResourceAsStream(
										"icons/BlueOn.gif"));

		_adapterTypeImages 		= new Image[2];
		_adapterTypeImages[0]	= new Image(null,
										AccessPointUI.class.getResourceAsStream(
										"icons/AdapterEthernet.gif"));
		_adapterTypeImages[1]	= new Image(null,
										AccessPointUI.class.getResourceAsStream(
										"icons/AdapterWireless.gif"));

		fVdata				 		= new FontData();
		fVdata.setName("VERDANA");
		fVdata.setHeight(7);
		fontVerdana 				= new Font(null,fVdata);

		foregroundColor			= new Color(null, 127,127,127);
        backgroundColor			= new Color(null, 228,228,241);

	}


	/*
	 * control property variables
	 */
	private int 				bulbOnOff;
	private InterfaceControl	dsInf;
	private int 				wmCount;
	private InterfaceControl	wmListHead = null;
	
	
	/*
	 * control variables
	 */
	private byte				controlDetails;
	private boolean 			controlSelected;
	private boolean 			groupSelect;
	private String				model;
	private String				name;
	private long 				signal;
	private long 				txRate;
	private int 				apState;
	private long 				boardTemperature;
	private long				flags;
	private	int					voltage;
	private long				lastHeartbeatTime;
	
	private Canvas 				picDisplay;
	private Canvas 				picOnOff;
	private Canvas 				picSelectButton;
	private Canvas 				picMinButton;
	private Canvas 				picMaxButton;
	private Canvas				picAlert1;
	private Canvas				picAlert2;

	/*
	 * Other Variables
	 */
	private Image						backGroundImage;
	private int							displayText;
	private AccessPointUIMouseListener 	apUIML;
	private MouseListener 				childUIML;
	private MeshNetworkUIProperties		nwProperties;
	private static Color				foregroundColor;
	private static Color 				backgroundColor;
	private int 						displayStateIndex;
	private AccessPointHandler			apHandler;
	private MeshNetworkView				networkView;
	private boolean						displayTimerStarted;
	private Runnable					multiDisplayThread;
	Vector<NodeDisplayText>				extDisplayText;
	private Point 						centerOfControl; 
	private GenericBulb					alert1Info;
	private GenericBulb					alert2Info;
	/**
	 * @param arg0
	 * @param arg1
	 */
	public DefaultAccessPointUI(Composite parent, MeshNetworkView meshNetworkView) {

		super(parent, SWT.FLAT);
		
		this.apHandler			= null;
		this.networkView		= meshNetworkView;
		this.nwProperties		= networkView.getProperties();
		dsInf					= new InterfaceControl();
		dsInf.type				= Mesh.PHY_TYPE_802_3;
		wmCount					= 0;
		wmListHead 				= null;
		controlDetails			= AccessPointUI.CONTROL_MAXIMIZED;
		controlSelected			= false;

		name 					= "";
		model					= "";
		init();
		loadControls(parent);
		centerOfControl 		= new Point(0,0);
	}

	private void init() {
		
		signal					= 0;
		txRate					= 0;
		boardTemperature		= 0;
		bulbOnOff				= BULB_STATUS_OFF;
		apState					= STATE_DEAD;
		displayText				= nwProperties.getNodeDisplayValue();
		flags 					= 0;
		displayStateIndex		= DISPLAY_STATE_NORMAL;		
		displayTimerStarted		= false;
		multiDisplayThread		= new Runnable() {
			public void run() {
				if(MeshViewerUI.display.isDisposed() == true) {
					return;
				}
				if(picDisplay.isDisposed() == true) {
					return;
				}
				
				picDisplay.redraw();
				displayStateIndex++;
				displayStateIndex %= DISPLAY_STATE_MAX_INDEX;

				if(flags > 0) {
					MeshViewerUI.display.timerExec(5000, this);
				} else {
					displayTimerStarted = false;
					displayStateIndex 	= DISPLAY_STATE_NORMAL;
					picDisplay.redraw();
				}
			}
		};
		
		extDisplayText	= new Vector<NodeDisplayText>();
		alert1Info		= new GenericBulb();
		alert2Info		= new GenericBulb();
	}
	
	/**
     * 
     */

    private void createMouseListeners() {

    	apUIML 		= new AccessPointUIMouseListener(networkView, this);
    	childUIML 	= new MouseAdapter() {

            public void mouseDoubleClick(MouseEvent e) {
            	handleSelectionEvent();
            }

            public void mouseDown(MouseEvent e) {
            	Canvas cv = (Canvas)e.widget;
				Point pt = cv.getLocation();
				e.x += pt.x;
				e.y += pt.y;
				if(e.button == 3){
					if(e.getSource().equals(picDisplay))
						changeDisplayText();
					else 
						apUIML.showPopupMenu(cv, e.x, e.y);
				} else {
					apUIML.mouseDown(e);
				}
				picSelectButton.redraw();
			}

        };

	}
	
	private void loadControls(Composite parent) {

		createMouseListeners();
		
		this.addMouseListener(apUIML);

		/* canvas for Name/Temp/BitRate/Sqnr/HopCount */
    	picDisplay	= new Canvas(this, SWT.SIMPLE);
    	picDisplay.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				paintDisplay(e.gc);
			}
		});
    	picDisplay.addMouseListener(childUIML);
    	picDisplay.setToolTipText("");
    	picDisplay.setFont(fontVerdana);
    	picDisplay.setBackground(backgroundColor);

		/* DS Adapter type Picture control */
    	dsInf.picInf = new Canvas(this,SWT.SIMPLE);
    	dsInf.picInf.setData(dsInf);
    	dsInf.picInf.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				paintInterface(e.gc, (Canvas)e.widget);
			}
		});
    	dsInf.picInf.addMouseListener(childUIML);

		/* Bulb control */
		picOnOff = new Canvas(this,SWT.SIMPLE);
		picOnOff.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				paintBulbOnOff(e.gc);
			}
		});
		picOnOff.addMouseListener(childUIML);
		picOnOff.addMouseListener(new MouseAdapter() {
			public void mouseDoubleClick(MouseEvent arg0) {
				if(controlDetails == AccessPointUI.CONTROL_MINIMIZED)
					setControlDetailHigh();
			}
		});

		picAlert1 = new Canvas(this,SWT.SIMPLE);
		picAlert1.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				if(apState != STATE_RUNNING)
					e.gc.drawImage(_bulbImages[BULB_OFF],0,0);
				else
					e.gc.drawImage(_bulbImages[alert1Info.value],0,0);
			}
		});
		picAlert1.addMouseListener(childUIML);

		picAlert2 = new Canvas(this,SWT.SIMPLE);
		picAlert2.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				if(apState != STATE_RUNNING)
					e.gc.drawImage(_bulbImages[BULB_OFF],0,0);
				else
					e.gc.drawImage(_bulbImages[alert2Info.value],0,0);
			}
		});
		picAlert2.addMouseListener(childUIML);
		
		picSelectButton = new Canvas(this,SWT.SIMPLE);
		picSelectButton.setData("select");
		picSelectButton.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				paintSelectButton(e.gc);
			}
		});
		picSelectButton.addMouseListener(childUIML);

		picDisplay.setBounds(15, 24, this.getBounds().width - 25, 12);
		Rectangle  rect = picDisplay.getBounds();
		picSelectButton.setBounds(rect.width + 45,rect.width + 25,13,11);

		picMinButton = new Canvas(this,SWT.SIMPLE);
		picMinButton.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				paintMinButton(e.gc);
				picMinButton.setToolTipText("Minimize");
			}
		});
		
		picMinButton.addMouseListener(new MouseAdapter() {
			private boolean state = false;
			public void mouseDown(MouseEvent arg0) {
				if(arg0.button==1)
					state = true;
			}
			public void mouseUp(MouseEvent arg0) {
				if(arg0.button==1){
					if(state == true) {
						if(controlDetails == AccessPointUI.CONTROL_MAXIMIZED)
							setControlDetailLow();
						else if(controlDetails == AccessPointUI.CONTROL_MINIMIZED)
							setControlDetailLow();
					}
					state = false;
				}

			}
		});

		picMaxButton = new Canvas(this,SWT.SIMPLE);
		picMaxButton.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				paintMaxButton(e.gc);
			}
		});
		
		picMaxButton.addMouseListener(new MouseAdapter() {
			private boolean state = false;
			public void mouseDown(MouseEvent arg0) {
				state = true;
			}
			public void mouseUp(MouseEvent arg0) {
				if(state == true) {
					if(controlDetails == AccessPointUI.CONTROL_MINIMIZED)
						setControlDetailHigh();
				}
				state = false;
			}
		});
		
		this.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
                if(null != apHandler && apHandler.isRoot()) {
                    if(_showDetails) {
                        backGroundImage	= _backGroundFullRoot;
                    } else {
                        backGroundImage	= _backGroundLowRoot;
                    }
                }
				paintBackGround(e.gc);
			}
		});
		dsInf.picInf.setBounds(5, 45, 11, 10);
		picOnOff.setBounds(6, 3, 11, 11);
		picAlert1.setBounds(18, 3, 11, 11);
		picAlert2.setBounds(30, 3, 11, 11);
		
		setControlDetailHigh();
	}

	private void paintInterface(GC gc, Canvas pic) {
		InterfaceControl inf = (InterfaceControl)pic.getData();
		if(inf.type == Mesh.PHY_TYPE_802_3)
			gc.drawImage(_adapterTypeImages[0], 0, 0);
		else
			gc.drawImage(_adapterTypeImages[1], 0, 0);
	}

	private void paintSelectButton(GC gc) {

		if(controlSelected == true){
			if(groupSelect == true)
				gc.drawImage(_groupselectButton, 0, 0);
			else
				gc.drawImage(_selectButton, 0, 0);
		} else if(groupSelect == true){
			gc.drawImage(_groupselectButton, 0, 0);
		} else if(groupSelect == false || controlSelected == false){
			_unselectButton.setBackground(picDisplay.getBackground());
			gc.drawImage(_unselectButton, 0, 0);
		}

	}

	private void paintMinButton(GC gc) {
		gc.drawImage(_minimizeButton, 0, 0);
	}

	private void paintMaxButton(GC gc) {
		gc.drawImage(_maximizeButton, 0, 0);
	}

	private void displayNormalText(GC gc) {
	
		switch(displayText) {

		case DISPLAY_TEXT_MODEL:
			gc.drawString(model + "               ", 0,0, false);
			break;
			
		case DISPLAY_TEXT_NAME:
			if(name != null){
				if(name.length() >= 7 )
					gc.drawString(name.substring(0,4) + "..." + "               ", 0,0, false);
				else
					gc.drawString(name + "               ", 0,0, false);
			}
			break;

		case DISPLAY_TEXT_TEMP:
			gc.drawString("  " + boardTemperature+"C         ", 0,0, false);
			break;

		case DISPLAY_TEXT_BITRATE:
			if(dsInf.type != Mesh.PHY_TYPE_802_3){
				gc.drawString(txRate + "Mbps   ", 0,0, false);
			} else {
				gc.drawString(model  + "               ", 0,0, false);
			}
			break;

		case DISPLAY_TEXT_SIGNAL:
			if(dsInf.type != Mesh.PHY_TYPE_802_3) {
				gc.drawString(signal + "dBm         ", 0,0, false);
			} else {
				gc.drawString(model  + "               ", 0,0, false);
			}
			break;

		case DISPLAY_TEXT_VOLTAGE:
			if(voltage > 0) {
				gc.drawString("  " + voltage  + "V              ", 0,0, false);
			} else {
				gc.drawString(model  + "               ", 0,0, false);
			}
			break;
		
		default:
		    if((displayText-DEFAULT_DISPLAY_TEXT_COUNT) <= extDisplayText.size()) {
			    NodeDisplayText t = extDisplayText.get((displayText-DEFAULT_DISPLAY_TEXT_COUNT));
				if(t != null)
				    gc.drawString(t.nodeText + "               ", 0,0, false);
				else
				    gc.drawString(model  + "               ", 0,0, false);
		    }
		}
	}
	
	private void displayRebootText(GC gc) {
	
		if(getRebootFlag() == true) {
			gc.setForeground(UIConstants.RED_COLOR);
			gc.drawString("     RR     " , 0,0, false);
		    return;
		}
		displayNormalText(gc);
	}
	
	private void displayRadarDetectText(GC gc) {
		
		if(getRadarDetectFlag() == true) {
			gc.setForeground(UIConstants.BLUE_COLOR);
			gc.drawString("     RD     " , 0,0, false);
		    return;
		}
		displayNormalText(gc);
	}

	private void paintDisplay(GC gc) {
		if(apState == STATE_DEAD) {
			gc.setForeground(foregroundColor);
			displayNormalText(gc);
			picDisplay.setToolTipText("");
			return;
		}else if(apState == STATE_SCANNING) {
			gc.setForeground(UIConstants.BLUE_COLOR);			
			gc.drawString("  SCAN" +"          " , 0,0, false);
			picDisplay.setToolTipText("");
			return;
		}
		
		String tooltipText = "";
		if(getRebootFlag() == true)
			tooltipText += "RR = Reboot Required" + SWT.LF ;
		if(getRadarDetectFlag() == true)
			tooltipText += "RD = Radar Detected" + SWT.LF ;
		
		gc.setForeground(UIConstants.BLACK_COLOR);
		picDisplay.setToolTipText(tooltipText);
		
		switch(displayStateIndex) {
			case DISPLAY_STATE_NORMAL:
				displayNormalText(gc);
				break;
			case DISPLAY_STATE_REBOOT:
				displayRebootText(gc);
				break;
			case DISPLAY_STATE_RADAR:
				displayRadarDetectText(gc);
				break;
		}
		
	}
	
    protected void paintGenericBulb(GC gc, Canvas pic) {
		GenericBulb gb = (GenericBulb)pic.getData();
		if(apState != STATE_RUNNING)
			gc.drawImage(_bulbImages[BULB_OFF],0,0);
		else
			gc.drawImage(_bulbImages[gb.value],0,0);
	}

	protected void paintBulbOnOff(GC gc) {
		gc.drawImage(_bulbImages[bulbOnOff],0,0);
	}

	private void paintBackGround(GC gc) {
		gc.drawImage(backGroundImage, 0 ,0);
	}

	private void setControlDetailLow() {
        _showDetails = false;
		controlDetails = AccessPointUI.CONTROL_MINIMIZED;
        if(null != apHandler && apHandler.isRoot()) {
            backGroundImage	= _backGroundLowRoot;
        } else {
		    backGroundImage	= _backGroundLow;
        }
		picMinButton.setVisible(false);
		picMaxButton.setLocation(0,0);
		picOnOff.setLocation(0,0);
		Rectangle rect = backGroundImage.getBounds();
		this.setSize(rect.width, rect.height);
		redrawAll();
	}

	private void setControlDetailHigh() {
        _showDetails = true;
		controlDetails = AccessPointUI.CONTROL_MAXIMIZED;
        if(null != apHandler && apHandler.isRoot()) {
            backGroundImage	= _backGroundFullRoot;
        } else {
		    backGroundImage	= _backGroundFull;
        }
		setSignal(signal);
		picOnOff.setLocation(6,3);
		picMinButton.setBounds(50, 5, 11, 10);
		picMaxButton.setVisible(false);
		picMinButton.setVisible(true);
		picDisplay.setVisible(true);
		picAlert1.setVisible(true);
		picAlert2.setVisible(true);
		Rectangle rect = backGroundImage.getBounds();
		this.setSize(rect.width, rect.height);
		picDisplay.setBounds(8, 24, this.getBounds().width - 25, 12);

		redrawAll();
	}

	private void redrawAll() {
		this.redraw();
		picOnOff.redraw();
		picDisplay.redraw();
		picSelectButton.redraw();
	}

	/**
	 * @param bulb_status_off2
	 */
	private void setOnOff(int bulb_status) {

		if(isDisposed() == true)
			return;

		bulbOnOff 	= bulb_status;
        String tooltipText = "";
		switch(apState) {
			case STATE_SCANNING:
                tooltipText = "SCANNING";
				break;
			case STATE_DEAD:
                tooltipText = "OFF";
				break;
			case STATE_RUNNING:
                tooltipText = "RUNNING";
				break;
		}

        /*
            When a AP is minimised, the status bulb is the only thing displayed
            Hence we need to ensure that the tooptip displayed also containf the AP name
        */
        if(this.controlDetails == AccessPointUI.CONTROL_MINIMIZED && this.getToolTipText() != null) {
            tooltipText = this.getToolTipText() + ": " + tooltipText;
        }
        picOnOff.setToolTipText(tooltipText);
	}
	
	private void setApStateRunning() {
		apState		= STATE_RUNNING;
		setOnOff(BULB_STATUS_ON);
		picOnOff.redraw();
		picAlert1.redraw();
		picAlert2.redraw();
		picDisplay.setBounds(8, 24, this.getBounds().width - 25, 12);
	}
	
	
	private void setApStateScanning() {
		apState		= STATE_SCANNING;
		setOnOff(BULB_STATUS_ON);
		picOnOff.redraw();
		picDisplay.setBounds(8, 24, this.getBounds().width - 25, 12);
		picDisplay.redraw();
	}

	private void setApStateDead() {

		if(isDisposed() == true)
			return;

		apState		= STATE_DEAD;
		init();
		setOnOff(BULB_STATUS_OFF);
		picOnOff.redraw();
		setLedColor(1, NMSUI.NODE_ELEMENT_VALUE_COLOR_GRAY);
		setLedColor(2, NMSUI.NODE_ELEMENT_VALUE_COLOR_GRAY);
		setLedLabel(1, "");
		setLedLabel(2, "");
		picDisplay.setBounds(8, 24, this.getBounds().width - 25, 12);
		redrawAll();
		
		if(isGroupSelected() == true) {
			uiEventSetGroupSelect(false);
		}
		
	}

	public void setVoltage(int voltage) {
		this.voltage = voltage;
	}

	private boolean getRadarDetectFlag(){
		if ((flags & MESH_FLAG_RADAR_DETECT) != 0)
			return true;

		return false;
	}

	private boolean getRebootFlag(){
		if ((flags & MESH_FLAG_REBOOT_REQUIRED) != 0)
			return true;

		return false;
	}

	public void handleSelectionEvent() {
		controlSelected = true;
		if(controlDetails != AccessPointUI.CONTROL_MINIMIZED) {		 
			picSelectButton.redraw();
			picDisplay.redraw();
		}
		if(apHandler != null)
			apHandler.notifySelection();
	}

	/**
	 * @return Returns the apState.
	 */
	public int getApState() {
		return apState;
	}

	/**
	 * @param boardTemperature The boardTemperature to set.
	 */
	public void setBoardTemperature(long boardTemperature) {
		this.boardTemperature = boardTemperature;
		picDisplay.redraw();
	}

	/**
	 * @param controlSelected The controlSelected to set.
	 */
	public void setSelected(boolean controlSelected) {
		this.controlSelected = controlSelected;
		redrawAll();
	}

	public void uiEventSetGroupSelect(boolean val){
		groupSelect = val;
		apHandler.uiEventGroupSelectionChanged(val);
		redrawAll();
	}
	
	public boolean isGroupSelected() {
		return groupSelect;
	}

	/**
	 * @return Returns the dsMacAddress.
	 */
	public MacAddress getDsMacAddress() {
		return apHandler.getDSMacAddr();
	}

	/**
	 * @param name The name to set.
	 */
	public void setAPName(String name) {
		this.name = name;
        //Set the tooltip to the name of the AP
        this.setToolTipText(name);

		setOnOff(bulbOnOff);

		picDisplay.redraw();
	}

	public void setAPModel(String ModelName) {
		this.model = ModelName.substring(2, 6);
		setOnOff(bulbOnOff);
		picDisplay.redraw();
	}

	/**
	 * @param signal The signal to set.
	 */
	public void setSignal(long signal) {
		this.signal 	= signal;
		picDisplay.redraw();
	}

	/**
	 * @param txRate The txRate to set.
	 */
	public void setTxRate(long txRate) {
		this.txRate = txRate;
		picDisplay.redraw();
	}

	/**
	 * @param dsIf
	 * @param wmIfs
	 */
	public void setHardwareInfo(InterfaceData dsIf, InterfaceData[] wmIfs,AccessPoint src) {

		/**
		 * Update DS Interface data
		 */
		dsInf.type = dsIf.getIfType();
		dsInf.picInf.setToolTipText(getToolTipText(dsIf));

		dsInf.picInf.redraw();

		/**
		 * Update WM interface data
		 */
		InterfaceControl node, node_end;
		node = wmListHead;
		while(node != null) {
			node.picInf.dispose();
			node = node.next;
		}
		wmListHead = null;
		wmCount = 0;
		for(int i=0; i< wmIfs.length; i++ ) {
			if(wmIfs[i].getName().equals("wlan3"))
				continue;
			node = new InterfaceControl();
			node.type = wmIfs[i].getIfType();
			node.name = wmIfs[i].getName();
			node.picInf = new Canvas(this,SWT.SIMPLE);
			node.picInf.setData(node);
			node.picInf.setBounds(50-12*wmCount, 45, 11, 10);
			node.picInf.setToolTipText(getToolTipText(wmIfs[i],wmIfs,src));
			node.picInf.addPaintListener(new PaintListener() {
				public void paintControl(PaintEvent e) {
					paintInterface(e.gc, (Canvas)e.widget);
				}
			});
			node.picInf.addMouseListener(childUIML);

			node.next	 = null;

			if(i == 0)
				wmListHead = node;
			else {
				node_end = wmListHead;
				while(node_end.next != null)
					node_end = node_end.next;
				node_end.next = node;
			}
			wmCount++;
		}
	}

	/**
	 * @param inf
	 * @param inf[]
	 * @return
	 */
	private String getToolTipText(InterfaceData inf,InterfaceData[] wmIfs,AccessPoint src) {

		String 	ret 		= inf.getName();
		int 	type		= inf.getIfType();

		if( type != Mesh.PHY_TYPE_802_3) {
	    	ret += "\nChannel = " + inf.getChannel();
	    }
		if(src.getHardwareInfo().getHardwareModel().indexOf("4452") > 0){
			if(inf.getName().equals("wlan2")) {
				for (int i = 0; i < wmIfs.length; i++) {
					if (wmIfs[i].getName().equals("wlan3")) {
						ret +=  "\n" + wmIfs[i].getName() + "\n";
						ret += "Channel = " + wmIfs[i].getChannel();
					}
				}
			}
		}
		else if(src.getHardwareInfo().getHardwareModel().indexOf("4458") > 0){
			if(inf.getName().equals("wlan0")) {
				for (int i = 0; i < wmIfs.length; i++) {
					if (wmIfs[i].getName().equals("wlan3")) {
						ret +=  "\n" + wmIfs[i].getName() + "\n";
						ret += "Channel = " + wmIfs[i].getChannel();
					}
				}
			}
		}
		return ret;
    }

	private String getToolTipText(InterfaceData inf) {
		String ret = inf.getName();
		int type= inf.getIfType();
	    if( type != Mesh.PHY_TYPE_802_3) {
	    	ret += "\nChannel = " + inf.getChannel();
	    }
		return ret;
	}

	/**
	 * @return Returns the controlDetails.
	 */
	public byte getControlDetails() {
		return controlDetails;
	}

	/**
	 * @param controlDetails The controlDetails to set.
	 */
	public void setControlDetails(byte controlDetails) {
		this.controlDetails = controlDetails;
		switch(controlDetails) {
			case AccessPointUI.CONTROL_MAXIMIZED:
				setControlDetailHigh();
				break;
			case AccessPointUI.CONTROL_MINIMIZED:
				setControlDetailLow();
				break;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#addMouseListener(org.eclipse.swt.events.MouseListener)
	 */
	public void addMouseListener(MouseListener arg0) {
		super.addMouseListener(arg0);
		if(arg0 instanceof AccessPointUIMouseListener)
			apUIML = (AccessPointUIMouseListener) arg0;
	}

	protected void changeDisplayText() {
		    
	    displayText++;
	    if(dsInf.type == Mesh.PHY_TYPE_802_3) {
	        
	        if(displayText == DISPLAY_TEXT_BITRATE) {
	            displayText++;
	        }
	        if(displayText == DISPLAY_TEXT_SIGNAL) {
	            displayText++;
	        }
	    }
	    
	   setDisplayText(displayText);
	   displayText %= (DEFAULT_DISPLAY_TEXT_COUNT + extDisplayText.size());
	      
		picDisplay.redraw();
	}
	
	public void setDisplayText(int nodeDisplayValue) {
		displayText 			= 	nodeDisplayValue;
	}

	public int getShowNeighbourLineInfo() {
		return apHandler.getkapLineInfoState();
	}

	public void setShowNeighbourLineInfo(int info) {
		apHandler.setkapLineInfoState(info);
	}

	public boolean isShowKnownAPLines() {
		return (apHandler.getKapLineState() == MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE) ?
					false : true;
	}

	public void setShowKnownAPLines(boolean showKnownAPLines) {
		int newLineState;
		
		newLineState = (showKnownAPLines == true) ? MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_ALL :
			MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE;
				
		apHandler.setKapLineState(newLineState);
	}

	public void heartbeatReceived() {
		
		apState = STATE_RUNNING;
		long currentTime = System.currentTimeMillis();
		if((currentTime - lastHeartbeatTime) < 3000)
			return;
		
		lastHeartbeatTime = currentTime;
		
		MeshViewerUI.display.timerExec(500, new Runnable () {
	        public void run () {
	        	try{
	        		long 			currentTime 	= System.currentTimeMillis();
	        		long 			timeDiff 		= currentTime - lastHeartbeatTime;
	        		int 			newBulbColor 	= bulbOnOff;
	        		
        			if(timeDiff < 3000){
        				
   				 		newBulbColor = (bulbOnOff != BULB_YELLOW) ? BULB_YELLOW : BULB_GREEN;
   				 		
    				 	if(apState != STATE_DEAD) {
    				 		bulbOnOff = newBulbColor;
    				 		if(picOnOff.isDisposed() == false) {
    				 			picOnOff.redraw();
    				 			MeshViewerUI.display.timerExec(500,this);
    				 		}
    				 	}
    				 	return;
        			}
        			
					if(apState != STATE_DEAD) {
						bulbOnOff = BULB_GREEN;
						if(picOnOff.isDisposed() == false)
							picOnOff.redraw();
					}

	        	}catch(Exception e){

	        	}
			}
		});
	}

	public void setApHandler(AccessPointHandler apHandler) {
		this.apHandler = apHandler;
	}

	public void addNodeDisplayText(NodeDisplayText  t) {
	    extDisplayText.add(t);
    }

    public void removeNodeDisplayText(NodeDisplayText  t) {
        
        for(NodeDisplayText nodeText : extDisplayText) {
	        if(nodeText.nodeTextId.equals(t.nodeTextId)) {
	            extDisplayText.remove(nodeText);
	            break;
	        } 	
	    }
    }
 
	@Override
	public void updateNodeDisplayText(NodeDisplayText t) {
		
		if(picDisplay == null)
			return;
		if(picDisplay.isDisposed() == true)
			return;
		
        MeshViewerUI.display.asyncExec(new Runnable(){
			public void run() {
				picDisplay.redraw();
			}
		});
    }
    
    public boolean isNodeMoveAllowed() {
    	return networkView.isNodeMoveAllowed();
    }

	@Override
	public Point getPoint() {
    	if(this.isDisposed())
    		return null;
        Rectangle rect = this.getBounds();
        
        centerOfControl.x = rect.x + rect.width /2;
        centerOfControl.y = rect.y + rect.height/2;
        return centerOfControl;
	}

	@Override
	public void setApState(int state) {
		switch(state) {
			case STATE_DEAD:
				setApStateDead();
				break;
			case STATE_SCANNING:
				setApStateScanning();
				break;
			case STATE_RUNNING:
				setApStateRunning();
				break;
			case STATE_HB_MISSED:
				setApStateHbMissed();
				break;
		}
	}

	private void setApStateHbMissed() {
		MeshViewerUI.display.asyncExec (new Runnable () {
			public void run () {
				if(picOnOff.isDisposed() == true)
					return;
				bulbOnOff = BULB_ORANGE;
				picOnOff.redraw();
			}
		});
	}

	@Override
	public void load(InputStream in) {
	}

	@Override
	public void save(OutputStream out) {
	}

	@Override
	public void setApStateImage(int state, String imagePath) {
	}

	@Override
	public void setInterfaceInfo(IInterfaceConfiguration ifConfig) {
	}

	@Override
	public void setFlags(long flags) {
		this.flags = flags;
		if(flags == 0)
			return;
		
		if(displayTimerStarted == false) {
			displayTimerStarted = true;
			MeshViewerUI.display.timerExec(5000, multiDisplayThread);
		}
	}

	@Override
	public void setElementProperty(String property, String value) {
		
		if(apState == STATE_DEAD)
			return;
		
		if(property.equalsIgnoreCase(NMSUI.NODE_ELEMENT_PROPERTY_LED1_COLOR) == true)
			setLedColor(1, value);
		else if(property.equalsIgnoreCase(NMSUI.NODE_ELEMENT_PROPERTY_LED2_COLOR) == true)
			setLedColor(2, value);
		else if(property.equalsIgnoreCase(NMSUI.NODE_ELEMENT_PROPERTY_LED1_LABEL) == true) 
			setLedLabel(1, value);
		else if(property.equalsIgnoreCase(NMSUI.NODE_ELEMENT_PROPERTY_LED2_LABEL) == true)
			setLedLabel(2, value);

	}

	private void setLedLabel(int ledIndex, String value) {
		switch(ledIndex) {
		case 1:
			alert1Info.label = value;
			MeshViewerUI.display.asyncExec(new Runnable(){
				@Override
				public void run() {
					if(picAlert1.isDisposed() == false) {
						picAlert1.setToolTipText(alert1Info.label);
						picAlert1.redraw();
					}
				}
			});
			break;
		case 2:
			alert2Info.label = value;
			MeshViewerUI.display.asyncExec(new Runnable(){
				@Override
				public void run() {
					if(picAlert2.isDisposed() == false) {
						picAlert2.setToolTipText(alert2Info.label);
						picAlert2.redraw();
					}
				}
			});
			break;
		} 
		
	}
	
	private void setLedColor(int ledIndex, String value) {
		int colorIndex = BULB_OFF;
		if(value.equalsIgnoreCase(NMSUI.NODE_ELEMENT_VALUE_COLOR_BLUE))
			colorIndex = BULB_BLUE;
		else if(value.equalsIgnoreCase(NMSUI.NODE_ELEMENT_VALUE_COLOR_GRAY))
			colorIndex = BULB_OFF;
		else if(value.equalsIgnoreCase(NMSUI.NODE_ELEMENT_VALUE_COLOR_GREEN))
			colorIndex = BULB_GREEN;
		else if(value.equalsIgnoreCase(NMSUI.NODE_ELEMENT_VALUE_COLOR_ORANGE))
			colorIndex = BULB_ORANGE;
		else if(value.equalsIgnoreCase(NMSUI.NODE_ELEMENT_VALUE_COLOR_RED))
			colorIndex = BULB_RED;
		else if(value.equalsIgnoreCase(NMSUI.NODE_ELEMENT_VALUE_COLOR_YELLOW))
			colorIndex = BULB_YELLOW;
		
		switch(ledIndex) {
			case 1:
				alert1Info.value = colorIndex;
				MeshViewerUI.display.asyncExec(new Runnable(){
					@Override
					public void run() {
						if(picAlert1.isDisposed() == false)
							picAlert1.redraw();
					}
				});
				
				break;
			case 2:
				alert2Info.value = colorIndex;
				MeshViewerUI.display.asyncExec(new Runnable(){
					@Override
					public void run() {
						if(picAlert2.isDisposed() == false)
							picAlert2.redraw();
					}
				});
				break;
		} 
		
	}
}
