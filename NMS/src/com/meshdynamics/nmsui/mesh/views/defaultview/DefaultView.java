/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : DefaultView.java
 * Comments : 
 * Created  : Sep 04, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 04, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.mesh.views.defaultview;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.nmsui.Branding;
import com.meshdynamics.nmsui.dialogs.MeshNetworkPropertiesDialog;
import com.meshdynamics.nmsui.mesh.DefaultLineInfo;
import com.meshdynamics.nmsui.mesh.EthLinePoint;
import com.meshdynamics.nmsui.mesh.ILinePoint;
import com.meshdynamics.nmsui.mesh.LineInfo;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.nmsui.mesh.MeshNetworkUIProperties;
import com.meshdynamics.nmsui.mesh.views.AccessPointHandler;
import com.meshdynamics.nmsui.mesh.views.AccessPointUI;
import com.meshdynamics.nmsui.mesh.views.MeshNetworkView;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.MacAddress;

public class DefaultView extends MeshNetworkView {
    
	private Image 										logoImage;
	private Image 										backBuffer;
	private GC 											gc;
	private Image										backImage;
	private Point										farthestPoint;
	protected int										prevHSelection;
	protected int										prevVSelection;
	
	public DefaultView(Composite arg0, int arg1,MeshNetworkUI networkUI) {
		super(arg0,SWT.NO_BACKGROUND | SWT.H_SCROLL | SWT.V_SCROLL,networkUI);
		
        this.addPaintListener(new MeshNetworkUIPaintAdaptor(this));
        logoImage				= Branding.getMainImage();
        this.setBackground((Display.getCurrent().getSystemColor(SWT.COLOR_LIST_BACKGROUND)));
	    
        backBuffer				= new Image (Display.getDefault(),Display.getDefault().getBounds()); 			
	    gc						= new GC(backBuffer);
	    backImage				= null;
	    farthestPoint			= new Point(0,0);

	    final ScrollBar hBar 	= getHorizontalBar();
	    hBar.addListener(SWT.Selection, new Listener() {
    	
	    	public void handleEvent(Event e) {
	    		
	    		int 	selection 	= hBar.getSelection ();
	    		int 	diff 		= Math.abs(prevHSelection - selection);
	    		boolean sign 		= (selection > prevHSelection) ? true : false;
	    		
	    		Enumeration<AccessPointUI> enumeration = accessPointUIs.elements();
	    		while(enumeration.hasMoreElements()) {
	    			AccessPointUI ui = enumeration.nextElement();
	    			Point location = ui.getLocation ();
	    			if(sign == false)
	    				location.x += diff;
	    			else
	    				location.x -= diff;
	    			
	    			ui.setLocation (location);
	    		}
	    			
	    		prevHSelection = selection;
	    		
	    	}
	    });	      

	    final ScrollBar vBar = getVerticalBar();
	    vBar.addListener(SWT.Selection, new Listener() {
	    	
	    	public void handleEvent(Event e) {
	    		int 	selection 	= vBar.getSelection ();
	    		int 	diff 		= Math.abs(prevVSelection - selection);
	    		boolean sign 		= (selection > prevVSelection) ? true : false;
	    		
	    		Enumeration<AccessPointUI> enumeration = accessPointUIs.elements();
	    		while(enumeration.hasMoreElements()) {
	    			AccessPointUI ui = enumeration.nextElement();
	    			Point location = ui.getLocation ();
	    			if(sign == false)
	    				location.y += diff;
	    			else
	    				location.y -= diff;
	    			
	    			ui.setLocation (location);
	    		}
	    			
	    		prevVSelection = selection;
	    	}
	    });
	    
		this.addListener (SWT.Resize,  new Listener () {
			public void handleEvent (Event e) {
				recalcScrollbars();
			}
		});	
		
	}

	private void recalcScrollbars() {
		Rectangle r 	= getClientArea();
		ScrollBar hBar	= getHorizontalBar();
		ScrollBar vBar	= getVerticalBar();
		
		Enumeration<AccessPointUI>enumeration;
		
		if(r.x+r.width < farthestPoint.x) {
			hBar.setVisible(true);
			hBar.setMaximum (farthestPoint.x);
		} else {
			hBar.setVisible(false);
    		enumeration 	= accessPointUIs.elements();
    		int selection 	= hBar.getSelection ();
    		
    		while(enumeration.hasMoreElements()) {
    			AccessPointUI ui = enumeration.nextElement();
    			Point location = ui.getLocation ();
    			location.x += selection;
    			ui.setLocation(location);
    		}
    		
    		hBar.setSelection(0);
    		prevHSelection = 0;
		}

		if(r.y+r.height < farthestPoint.y) {
			vBar.setVisible(true);
			vBar.setMaximum (farthestPoint.y);
		} else {
			vBar.setVisible(false);
    		enumeration 	= accessPointUIs.elements();
    		int selection 	= vBar.getSelection ();
		
    		while(enumeration.hasMoreElements()) {
    			AccessPointUI ui = enumeration.nextElement();
    			Point location = ui.getLocation ();
				location.y += selection;
    			ui.setLocation(location);
    		}
    		
    		vBar.setSelection(0);
			prevVSelection = 0;
		}

	}
	
    class MeshNetworkUIPaintAdaptor implements PaintListener {

    	DefaultView view;
    	
		public MeshNetworkUIPaintAdaptor(DefaultView view) {
			super();
			this.view = view;
		}
		
		public void paintControl(PaintEvent e) {
			
			Rectangle bounds 	= view.getClientArea();
	        
			if(backBuffer == null || backBuffer.isDisposed() == true)
			    return;

			gc.fillRectangle(backBuffer.getBounds());
			
			if(backImage == null)
				gc.setBackground(e.gc.getBackground());
			else {
				Rectangle imageBounds = backImage.getBounds();
				gc.drawImage(backImage,imageBounds.x, imageBounds.y, imageBounds.width, imageBounds.height, 
						bounds.x, bounds.y, bounds.width, bounds.height);
			}
			
			if(properties.isShowGrid() == true) {
				DrawGrid(bounds,gc);
			}
			
			DrawBorder(bounds,gc);
			
			Rectangle logoBounds = logoImage.getBounds();
			gc.drawImage(logoImage,bounds.width -logoBounds.width-15,bounds.height - logoBounds.height-15);
					
			if(!networkUI.isMeshViewerStarted()){
			    e.gc.drawImage(backBuffer,0,0);
				return;
			}
			
			Enumeration<AccessPointHandler> apEnum = accessPointHandlers.elements();
			while(apEnum.hasMoreElements()) {
				
				AccessPointHandler apHandler = (AccessPointHandler) apEnum.nextElement();
                if(apHandler.isRoot()) {
                    ShowRoot(gc, apHandler.getUI());
                }

				if(apHandler.isRunning() == false) {
					continue;
				}
				
				if(apHandler.getKapLineState() != MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE) {
					Enumeration<LineInfo> kapEnum = apHandler.getKnownApLines();
					while(kapEnum.hasMoreElements()) {
						LineInfo kapLine = (LineInfo) kapEnum.nextElement();
						kapLine.drawLine(gc);
					}
				}
			}

			/*
			 * we use different loops for knownap lines and ds lines
			 * that way ds lines are drawn after all knownap lines and
			 * are hence visible on top of known ap lines 
			 */
			
			apEnum = accessPointHandlers.elements();
			while(apEnum.hasMoreElements()) {
				
				AccessPointHandler apHandler = (AccessPointHandler) apEnum.nextElement();
				if(apHandler.isRunning() == false) {
					continue;
				}
				if(apHandler.getAssocLineState() == MeshNetworkUIProperties.ASSOCIATIONLINE_STATE_SHOW) {	
					DefaultLineInfo dsLine = apHandler.getDsLine();
					dsLine.drawLine(gc);
				}
			}
			e.gc.drawImage(backBuffer,0,0);
			
		}

		private void DrawBorder(Rectangle bounds,GC gc){
			bounds.x = 2;
			bounds.y = 2;
/*
			if(properties.getHealthMonitorStatus() == MeshNetworkUIProperties.ENABLE) {
				switch(networkUI.getCurrentHealthStatus()) {
				case IHealthMonitor.HEALTH_STATUS_GREEN:
					gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
					break;
				case IHealthMonitor.HEALTH_STATUS_YELLOW:
					gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_YELLOW));
					break;
				case IHealthMonitor.HEALTH_STATUS_RED:
					gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
					break;
				default:
					gc.setForeground(MeshNetworkUIProperties.DEFAULT_ETH_LINK_COLOR);
					break;
				}
				gc.setLineWidth(20);
			} else  {
				gc.setForeground(MeshNetworkUIProperties.DEFAULT_ETH_LINK_COLOR);
				gc.setLineWidth(10);
			}
*/
			gc.setForeground(MeshNetworkUIProperties.DEFAULT_ETH_LINK_COLOR);
			gc.setLineWidth(10);
			
			bounds.height	-= 5;
			bounds.width  	-= 5;
			gc.drawRectangle(bounds);
			gc.setLineWidth(0);
		}
		
		private void DrawGrid(Rectangle bounds,GC gc){

			bounds.x = 2;
			bounds.y = 2;

			gc.setLineStyle(SWT.LINE_DOT);
/*			
			if(properties.getHealthMonitorStatus() == MeshNetworkUIProperties.ENABLE) {
				switch(networkUI.getCurrentHealthStatus()) {
				case IHealthMonitor.HEALTH_STATUS_GREEN:
					gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_GREEN));
					break;
				case IHealthMonitor.HEALTH_STATUS_YELLOW:
					gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_YELLOW));
					break;
				case IHealthMonitor.HEALTH_STATUS_RED:
					gc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
					break;
				default:
					gc.setForeground(MeshNetworkUIProperties.DEFAULT_ETH_LINK_COLOR);
					break;
				}
			} else  {
				gc.setForeground(properties.getGridLineColor());
			}
*/
			gc.setForeground(properties.getGridLineColor());
			
	    	int gridSize 	= properties.getGridSize();
			int xDist 		= bounds.width / gridSize;
			int yDist 		= bounds.height / gridSize;
			int xPos 		= xDist, yPos = yDist;
		
			for (int i = 0; i < gridSize; i++) {
				gc.drawLine(xPos, 0, xPos, bounds.height);
				gc.drawLine(0, yPos, bounds.width, yPos);
				xPos += xDist;
				yPos += yDist;
			}		
			
			gc.setLineStyle(SWT.LINE_SOLID);
		}    
	}
	
	private void redrawRect(Rectangle rect) {
        if(rect != null) {
        	redraw(rect.x,rect.y,rect.width,rect.height,true);
        	update();
        } else {
        	redraw();
        }
	}

    private void updateApLocation(AccessPointUI apUIObj) {
        
        AccessPointHandler 	apH = accessPointHandlers.get(apUIObj.getDsMacAddress().toString());
        
        apH.setViewLocation(apUIObj.getLocation());

		farthestPoint.x = 0;
		farthestPoint.y = 0;
        Enumeration<AccessPointHandler> enumeration = accessPointHandlers.elements();
        while(enumeration.hasMoreElements()) {
        	
        	apH 		= enumeration.nextElement();
        	Point apLoc = apH.getLocation();
    		
    		if(apLoc.x > farthestPoint.x)
    			farthestPoint.x = apLoc.x;

    		if(apLoc.y > farthestPoint.y)
    			farthestPoint.y = apLoc.y;

        }
        LineInfo line = apH.getDsLine();
        if(line == null) {
        	return;
        }
        ILinePoint ptDest = line.getDstPoint();
        
        if(ptDest instanceof EthLinePoint) {
            Point pt 			= EthLinePoint.getConnectionPoint(apH.getPoint(), this.getClientArea());
            EthLinePoint ethPt 	= (EthLinePoint) ptDest;
            
            ethPt.updatePoint(pt);
        }
        redraw();
    }
    
	/**
	 * @param string
	 * @return
	 */
	protected AccessPointHandler getApHandler(String dsMacAddress) {
		
		AccessPointHandler apHandler 	= (AccessPointHandler)accessPointHandlers.get(dsMacAddress);
		
		if(apHandler != null) {
			return apHandler;
		}
		
		DefaultAccessPointUI apUI = new DefaultAccessPointUI(this,this);

        accessPointUIs.put(dsMacAddress, apUI);

        Rectangle 	rect = apUI.getBounds();
        int 		xpos = accessPointUIs.size() / 10;
        
        rect.x = accessPointUIs.size() * 20 + (xpos * 20);
        rect.y = accessPointUIs.size() * 20;
        
        apUI.setBounds(rect);
        apUI.addControlListener(new ControlAdapter(){

            public void controlResized(ControlEvent arg0) {
                controlMoved(arg0);
            }
            public void controlMoved(ControlEvent arg0) {
            	updateApLocation((DefaultAccessPointUI)arg0.getSource());
            }
        });
        
        apHandler = new AccessPointHandler(this,null, apUI,networkUI.getProperties());
        apHandler.setDSMacAddress(dsMacAddress);
        accessPointHandlers.put(dsMacAddress, apHandler);
        apUI.setApHandler(apHandler);
        notifyStatusBar();
		return apHandler;
	}

    
	/**
	 * @param properties The properties to set.
	 */
	public void recalculateNeighbourLineState() {

		Enumeration<AccessPointHandler> enumeration 			= accessPointHandlers.elements();
		int 		neighbourStateNoneCount = 0;
		int 		apHandlerCount			= accessPointHandlers.size();
		
		while(enumeration.hasMoreElements()) {
			AccessPointHandler apHandler = (AccessPointHandler) enumeration.nextElement();
			if(apHandler.isRoot() == true){
				apHandlerCount--;
				continue;
			}
			
			if(apHandler.getKapLineState() == MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE) {
				neighbourStateNoneCount++;
			}
		}
		
		if(neighbourStateNoneCount == 0)
			properties.setNeighbourLines(MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_ALL);
		else if(apHandlerCount == neighbourStateNoneCount)
			properties.setNeighbourLines(MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE);
		else
			properties.setNeighbourLines(MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_PARTIAL);
		
	}
	
	public void parentChanged(AccessPointHandler handler) {
		if(isDisposed() == false) {
		    redraw();//Rect(getAPSEnclosingRegion());
		}
	}
	
	public void parentInfoChanged(AccessPointHandler handler) {
		DefaultLineInfo line = handler.getDsLine();
		Rectangle rect = line.getBitrateCoordinates();
   		redrawRect(rect);
	}

	public void knownApListChanged(AccessPointHandler handler) {
		if(isDisposed() == false) {
			redraw();//Rect(getAPSEnclosingRegion());
		}
	}
	
	public void knownApInfoChanged(AccessPointHandler handler,DefaultLineInfo line) {
		Rectangle rect = line.getBitrateCoordinates();
		redrawRect(rect);
	}

	public void lineStateChanged(AccessPointHandler handler) {
		recalculateNeighbourLineState();
		redraw();
	}
	
	public void lineInfoStateChanged(AccessPointHandler handler) {
	    redraw();//Rect(getAPSEnclosingRegion());
	}

	public void refresh() {

		Enumeration<AccessPointHandler> apEnum = accessPointHandlers.elements();
		
		while(apEnum.hasMoreElements()) {
			AccessPointHandler apHandler = (AccessPointHandler) apEnum.nextElement();
			apHandler.refresh();
		}
	}
	
	public void load() {
    	try {
    		FileInputStream in = new FileInputStream(MFile.getNetworkPath() + "/" + networkUI.getNwName() + "-def.dat");    		
    		properties.read(in);
    		loadAccessPoints(in);
    		in.close();
    		this.recalcScrollbars();
    	}catch(Exception e){
    	    e.printStackTrace();
    	}
		String backImagePath = properties.getMapImagePath();
		if(backImagePath.length() > 0)
			backImage = new Image(null, backImagePath);
		else
			backImage = null;
    	
    }

    public void save() {
    	
    	try{
    		FileOutputStream os = new FileOutputStream(MFile.getNetworkPath() + "/" + networkUI.getNwName() + "-def.dat");
    		properties.write(os);
	    	saveAccessPoints(os);
	    	os.close();
    	}catch(Exception e){
    		System.out.println(e.getMessage());
    	} 	
    }
  
    private void loadAccessPoints(InputStream in) {
		byte[] 		oneByte  	= new byte[1];
		int 		apCount;
		
		try {
			in.read(oneByte);
			apCount = oneByte[0];
			
			while(apCount-- > 0) {
			    MacAddress	dsMacAddress = new MacAddress();
				dsMacAddress.read(in);   
				
				AccessPointHandler 	apHandler = getApHandler(dsMacAddress.toString());
				apHandler.load(in);
				
				AccessPoint ap = networkUI.addAccessPoint(dsMacAddress);
				ap.load(in);
				
				Point apLoc = apHandler.getLocation();
				if(apLoc.x > farthestPoint.x)
					farthestPoint.x = apLoc.x;

				if(apLoc.y > farthestPoint.y)
					farthestPoint.y = apLoc.y;

			}

		} catch (IOException ee) {
			Mesh.logException(ee);
		}
	}
    
	private void adjustViewLocation(boolean savingToFile) {
		ScrollBar hBar		= getHorizontalBar();
		ScrollBar vBar		= getVerticalBar();
		
		Enumeration<AccessPointHandler>enumeration = accessPointHandlers.elements();;
		while(enumeration.hasMoreElements()) {
			
			AccessPointHandler apH = enumeration.nextElement();
			
			if(savingToFile == true) {
				Point location 			= apH.getLocation();
				
				if(vBar.getVisible() == true)
					location.y += vBar.getSelection();
			
				if(hBar.getVisible() == true) 
					location.x += hBar.getSelection();
					
				apH.setViewLocation(location);
			} else {
				AccessPointUI apUI 	= apH.getUI();
				Point location 		= apUI.getLocation();
				
				apH.setViewLocation(location);
			}
		}
		
	}
    
	public void saveAccessPoints(OutputStream out) {
		
		Enumeration<AccessPointHandler> e 	= accessPointHandlers.elements();
		byte[] oneByte						= new byte[1];
		
		try {

			oneByte[0] = (byte) accessPointHandlers.size();
			out.write(oneByte);
			
			while(e.hasMoreElements()) {
				
				AccessPointHandler 	accessPointHandler 	= (AccessPointHandler)e.nextElement();
				MacAddress			dsAddress			= accessPointHandler.getDSMacAddr();
 
				dsAddress.write(out);
				adjustViewLocation(true);
				accessPointHandler.save(out);
				adjustViewLocation(false);
				AccessPoint ap =  accessPointHandler.getAccesspoint();
				ap.save(out);
			}
		} catch (IOException ee) {
			Mesh.logException(ee);
		}        			
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#editProperties()
	 */
	public boolean editProperties() {
		MeshNetworkPropertiesDialog dlg = new MeshNetworkPropertiesDialog(networkUI.getNwName(), networkUI.getProperties());
		dlg.show();
		if(dlg.getStatus() != SWT.OK) {
			return false;
		}
		
		Enumeration<AccessPointHandler> enumeration 			= accessPointHandlers.elements();
		while(enumeration.hasMoreElements()) {
			AccessPointHandler apHandler = (AccessPointHandler) enumeration.nextElement();
			apHandler.refreshProperties();
		}
		this.properties.setResetNeighbourLineInfo(false);

		String backImagePath = properties.getMapImagePath();
		if(backImagePath.length() > 0)
			backImage = new Image(null, backImagePath);
		else
			backImage = null;
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#notifyHealthAlert(int, java.lang.String)
	 */
	public void notifyHealthAlert(int healthStatus, String meshId) {
		if(isDisposed() == true) {
			return;
		}
		redraw();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#onClose()
	 */
	public void onClose() {
	   gc.dispose();
	   gc = null;
	   backBuffer.dispose();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#apPropertyChanged(int, int, com.meshdynamics.meshviewer.mesh.AccessPoint)
	 */
	public void apPropertyChanged(int filter, int subFilter, AccessPoint src) {
		AccessPointHandler apHandler = (AccessPointHandler) accessPointHandlers.get(src.getDSMacAddress());
		if(apHandler != null) {
			apHandler.apPropertyChanged(filter,subFilter,src);
		} else 
			accessPointAdded(src);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#selectionChanged(boolean)
	 */
	public void selectionChanged(boolean selected) {
		if(selected == true) {
			Enumeration<AccessPointUI> e = accessPointUIs.elements();
			while(e.hasMoreElements()) {
				updateApLocation(e.nextElement());
			}
			redraw();
		}
			
	}
	
}
