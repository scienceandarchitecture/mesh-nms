/**********************************************************************************
 * MeshDynamics
 * --------------
 * File     : AccessPointUI.java
 * Comments :
 * Created  : May 4, 2005
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History
 * -------------------------------------------------------------------------------
 * | No  |Date        |  Comment                                        | Author |
 *   --------------------------------------------------------------------------------
 * | 44  |Aug 01, 2007 | mouseDoubleClickedHandler added			    | Imran  |
 * -------------------------------------------------------------------------------
 * | 43  |May 24,2007  | group selection implementation					|Abhijit |
 * -------------------------------------------------------------------------------
 * | 42  |May 09,2007  | removed unused code							|Abhijit |
 * -------------------------------------------------------------------------------
 * | 41  |Mar 21, 2007| led,display text logic changed					|Abhijit |
 * --------------------------------------------------------------------------------
 * | 40  |Mar 14, 2007| RR tooltip on DEAD node is removed      	 	|Abhishek|
 * --------------------------------------------------------------------------------
 * | 39  |Feb 27,2007 |  removed unused code							|Abhijit |
 *---------------------------------------------------------------------------------
 * | 38  |Jan 15, 2007| Degrees symbol added					  	 	|Abhishek|
 * --------------------------------------------------------------------------------
 * | 38  |Dec 28, 2006| kap_lines, bitrate properties added		        |Abhijit |
 * --------------------------------------------------------------------------------
 * | 37  |Nov 27, 2006| clean-up					  			        |  Bindu |
 * --------------------------------------------------------------------------------
 * | 36  |Oct 15,2006 | Removed unused mouselisteners	                |Abhijit |
 * -------------------------------------------------------------------------------
 * | 35  |Oct 06,2006 | Misc Fixes						                |Bindu	 |
 * -------------------------------------------------------------------------------
 * | 34  |July 07,2006| RF Space dialog handler removed                 |Prachiti|
 * -------------------------------------------------------------------------------
 * | 33  |June 20,2006| Reboot Required Displayed                       |Prachiti|
 * -------------------------------------------------------------------------------
 * | 32  |May 23, 2006| Misc Fixes     	                                |Bindu   |
 * -------------------------------------------------------------------------------
 * | 31  |May 22, 2006| Misc Fixes     	                                |Bindu   |
 * -------------------------------------------------------------------------------
 * | 30  |May 19, 2006| UI Updation on Packet receive                   |Bindu   |
 * -------------------------------------------------------------------------------
 * | 29  |May 12, 2006|Changes for DL Saturation					    | Bindu  |
 *  ------------------------------------------------------------------------------
 * | 28  |Mar 27, 2006| Reboot Checker							  	 	| Mithil |
 * -------------------------------------------------------------------------------
 * | 27  |Mar 17, 2006| Reboot Flag update on Nodecolor change	     	| Mithil |
 *  ------------------------------------------------------------------------------
 * | 26  |Mar 17, 2006| Reboot Flag update on Node				     	| Mithil |
 *  ------------------------------------------------------------------------------
 * | 25  |Mar 10, 2006|Node display value changes after heartbeat     	| Mithil |
 *  ------------------------------------------------------------------------------
 * | 24  |Feb 28, 2006|tooltip channel for ixp0 and ixp1 removed       	| Mithil |
 *  ------------------------------------------------------------------------------
 * | 23  |Feb 22, 2006 | Led Color Changes			 				    | Mithil |
 * -------------------------------------------------------------------------------
 * | 22  |Feb 17, 2006|4452 and 4458 tooltip change                    	| Mithil |
 *  ------------------------------------------------------------------------------
 * | 21  |Feb 16, 2006|Config Sequence Number change                    |Prachiti|
 *  ------------------------------------------------------------------------------
 * | 20  |Feb 15, 2006| Reboot Flag updation		                    | Mithil |
 *  ------------------------------------------------------------------------------
 * |  19 | Feb 03,2006 | Generic Request handling done thru flags  		| Mithil |
 * -------------------------------------------------------------------------------
 * |  18 |Feb 03, 2006  | Generic Request		   	 					| abhijit|
 * -------------------------------------------------------------------------------
 * |  17 |Feb 02,2006 | GDI Objects not cleared				       		| Mithil |
 * -------------------------------------------------------------------------------
 * |  16 |Jan 27, 2006 | select done only when picSelect is clicked		| Mithil |
 *  ------------------------------------------------------------------------------
 * |  15 |Jan 20, 2006 | Group select bug								| Mithil |
 *  ------------------------------------------------------------------------------
 * |  14 |Jan 20, 2006 | Group select done								| Mithil |
 *  ------------------------------------------------------------------------------
 * |  13 |Jan 18, 2006 |SCAN mode persists after stopping viewer         |Mithil |
 * -------------------------------------------------------------------------------
 * |  12 |Jan 18,2006 | Node name >= 7 ! 2 be displayed display name + ..|Mithil |
 * -------------------------------------------------------------------------------
 * |  11 |Jan 17,2006 | Node name > 4 ! 2 be displayed display name + ..| Mithil |
 * -------------------------------------------------------------------------------
 * |  10 |Jan 17,2006 | Node name not displayed after starting viewer	| Mithil |
 * -------------------------------------------------------------------------------
 * |  9  |Jan 17,2006 | GDI Objects not cleared				       		| Mithil |
 * -------------------------------------------------------------------------------
 * |  8  |Jan 12,2006 | Root node display faulty when bitrate        	| Mithil |
 * |                    or Sig Strength is selected						|		 |
 * -------------------------------------------------------------------------------
 * |  7  |Jan 12,2006 | Root node should display temperature       		| Mithil |
 * -------------------------------------------------------------------------------
 * |  6  |Jan 04,2006 | Dbm changed to dBm			              		| Mithil |
 * -------------------------------------------------------------------------------
 * |  5  |Nov 29, 2005|Display text problem for root fixed		        | Amit   |
 * ------------------------------------------------------------------------------
 * |  4  |Nov 25, 2005|1037 : EditViewSettings dialog -- when set to	| Mithil |
 * |	 |			  |	display mbps dbm values displayed       		|        |
 * -------------------------------------------------------------------------------
 * |  3  |Nov 09, 2005|1005 :  Tool tip formatting change required      | Mithil |
 * -------------------------------------------------------------------------------
 * |  2  |Oct 19, 2005| Added method for wlan3 appended tooltiptext     | Mithil |
 * -------------------------------------------------------------------------------
 * |  1  |Sep 25, 2005| View properties added to constructor            | Abhijit|
 * -------------------------------------------------------------------------------
 * |  0  |May 4, 2005 | Created                                         | Anand  |
 * -------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.mesh.views;

import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Menu;

import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.HardwareInfo.InterfaceData;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI.NodeDisplayText;
import com.meshdynamics.util.MacAddress;


public interface AccessPointUI {

	public static  final int STATE_DEAD 				= 0;
	public static  final int STATE_SCANNING 			= 1;
	public static  final int STATE_RUNNING 				= 2;
	public static  final int STATE_HB_MISSED 			= 3;
	
	public static final byte CONTROL_MAXIMIZED			= 1;
    public static final byte CONTROL_MINIMIZED			= 2;

    public static final int MESH_FLAG_CT_MODE 			= 1;
    public static final int MESH_FLAG_REBOOT_REQUIRED 	= 2;
    public static final int MESH_FLAG_DENY_CLIENTS 		= 4;
    public static final int MESH_FLAG_RADAR_DETECT 		= 8;
    public static final int MESH_FLAG_FIPS				= 16;
    
	public Point getLocation();

	public void setHardwareInfo(InterfaceData dsIf, InterfaceData[] wmIfs,
			AccessPoint ap);

	public void setAPModel(String hardwareModel);

	public void setAPName(String name);

	public void heartbeatReceived();

	public void setVoltage(int voltage);

	public void setBoardTemperature(long boardTemperature);

	public void setTxRate(long bitRate);

	public void setSignal(long parentSignal);

	public void setApState(int state);
	
	public boolean isDisposed();

	public Rectangle getBounds();

	public void setLocation(Point detailViewLocation);

	public byte getControlDetails();

	public void setControlDetails(byte b);

	public void setShowNeighbourLineInfo(int kapLineInfoState);

	public void setShowKnownAPLines(boolean b);

	public void dispose();

	public MacAddress getDsMacAddress();

	public void setDisplayText(int nodeDisplayValue);

	public boolean isGroupSelected();

	public void uiEventSetGroupSelect(boolean b);

	public void addNodeDisplayText(NodeDisplayText nodeText);

	public void removeNodeDisplayText(NodeDisplayText nodeText);

	public void updateNodeDisplayText(NodeDisplayText t);
	
	public Point getPoint();

	public void setCapture(boolean b);

	public void setCursor(Cursor handCursor);

	public int getApState();

	public int getShowNeighbourLineInfo();

	public boolean isShowKnownAPLines();

	public void setMenu(Menu menu);

	public Menu getMenu();

	public boolean isNodeMoveAllowed();

	public void setLocation(int x, int y);

	public void setSelected(boolean controlSelected);

	public void handleSelectionEvent();

	public void load(InputStream in);

	public void save(OutputStream out);
	
	public void setApStateImage(int state, String imagePath);
	
	public void setInterfaceInfo(IInterfaceConfiguration ifInfo);

	public void setFlags(long flags);

	/*
	 * See
	 * NMSUI.java for explanation of node properties and values
	 */
	public void setElementProperty(String property, String value);
	
}
