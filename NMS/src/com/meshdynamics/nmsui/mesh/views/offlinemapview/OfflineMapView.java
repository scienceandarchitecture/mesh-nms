/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : OfflineMapView.java
 * Comments : Implementation of Offline Map using openstreetmaps 
 * Created  : Jan 10, 2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Jan 10, 2008 | Created                                         |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.mesh.views.offlinemapview;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Scale;

import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;
import com.meshdynamics.nmsui.Branding;
import com.meshdynamics.nmsui.dialogs.MeshNetworkPropertiesDialog;
import com.meshdynamics.nmsui.maps.GeoPosition;
import com.meshdynamics.nmsui.maps.MDMap;
import com.meshdynamics.nmsui.mesh.DefaultLineInfo;
import com.meshdynamics.nmsui.mesh.EthLinePoint;
import com.meshdynamics.nmsui.mesh.ILinePoint;
import com.meshdynamics.nmsui.mesh.LineInfo;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.nmsui.mesh.MeshNetworkUIProperties;
import com.meshdynamics.nmsui.mesh.views.AccessPointHandler;
import com.meshdynamics.nmsui.mesh.views.AccessPointUI;
import com.meshdynamics.nmsui.mesh.views.MeshNetworkView;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.Bytes;
import com.meshdynamics.util.MacAddress;

/**
 * @author prachiti
 *
 */

public class OfflineMapView extends MeshNetworkView {
    
    private MDMap									mdMap;
	private Image 									logoImage;
	private Menu		 							popup;
	private MenuItem								itmRefresh;
	private MenuItem								itmAdjustMode;
	private MenuItem 								itmTrackMode;
	private boolean									trackMode;
	private MapAdjustTimer							panTimer;
	private Thread									panTimerThread;
	private Image 									backBuffer;
	private GC										backGC;
	private Rectangle 								apRegion;
	private boolean									adjustMode;
    private Scale									zoomScale;
    private Button									plusZoom;
    private Button									minusZoom;
	
    public OfflineMapView(Composite arg0, int arg1, MeshNetworkUI networkUI) {
        super(arg0, arg1, networkUI);

        backBuffer				= new Image (Display.getDefault(),Display.getDefault().getBounds());
        backGC					= new GC(backBuffer);
	    apRegion				= null;
	    checkMapFile();
	    createControls();
        logoImage				= Branding.getMainImage();
        this.setBackground((Display.getCurrent().getSystemColor(SWT.COLOR_WHITE)));
        
        this.addControlListener(new ControlAdapter(){

			public void controlResized(ControlEvent arg0) {
				mdMap.resize();
			}
		});
        
        this.addListener(SWT.MouseMove, new Listener(){

			public void handleEvent(Event e) {
        		if (e.type == SWT.MouseMove) {
    			    Point pt = new Point(e.x,e.y);
    			    GeoPosition gp 	= mdMap.convertPointToGeoPosition(pt);
    			    notifyStatusBarCoordinates(gp.toString());
    			}
			}

		});

        this.addPaintListener(new PaintListener(){

			public void paintControl(PaintEvent arg0) {
				
				if(backBuffer == null || backBuffer.isDisposed() == true)
				    return;
				
				paintControlBackground(arg0.gc);
			}
		});
        
        popup = new Menu(this.getShell(), SWT.POP_UP);
        this.setMenu(popup);
        createMenuItems();
        trackMode 	= true;
        adjustMode 	= false;
        mdMap.setMoveAllowed(adjustMode);
        itmTrackMode.setSelection(true);
        itmAdjustMode.setSelection(false);
        zoomScale.setEnabled(false);
        plusZoom.setEnabled(false);
        minusZoom.setEnabled(false);
    }
    
    private void createControls() {

		mdMap	= new MDMap(this, SWT.BORDER);
		
		try {
            String mapPath 	= MFile.getMapPath()+"\\Maps.zip";
		    mapPath 		= mapPath.replace("\\","/");
		    mapPath 		= mapPath.replace(" ","%20");
		    
		    mdMap.setTileFactory(new URI(mapPath).toString(),true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        zoomScale = new Scale(this, SWT.VERTICAL); 
        zoomScale.setBounds(10,10,40,150);
        zoomScale.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
        zoomScale.setMaximum(MDMap.MAXZOOM);
        zoomScale.setIncrement(1);
        setMapZoom(zoomScale.getMaximum());
        
        plusZoom = new Button(this, SWT.NONE);
        plusZoom.setBounds(10,170,20,20);
        plusZoom.setText("+");
        plusZoom.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
			    zoomIn();
			}
        });
        
        minusZoom = new Button(this, SWT.NONE);
        minusZoom.setBounds(30,170,20,20);
        minusZoom.setText("-");
        minusZoom.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
			    zoomOut();
			}
        });
        
        zoomScale.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
			    int zoom 	= zoomScale.getSelection();
			    zoom 		= MDMap.MAXZOOM - zoom;
			    zoomScale.setToolTipText("" + zoom);
			    mdMap.setZoom(((int)zoom));
			}
        });
        
        Listener l = new Listener() {
        	Point prev = new Point(0,0);
            public void handleEvent(Event e)  {
            	
            	if(trackMode == true)
            		return;
            	
            	if(mdMap.isMoveAllowed() == false)
            		return;
            	
				if (e.type == SWT.MouseDown && e.button == 1) {
					prev.x = e.x;
					prev.y = e.y;
				}
				
				if (e.type == SWT.MouseMove && (e.stateMask & SWT.BUTTON1) != 0) {
					Point pt = new Point(e.x,e.y);
					mdMap.setCenterOnMove(pt,prev);
					prev.x = e.x;
					prev.y = e.y;
				}
				if (e.type == SWT.MouseDoubleClick) {
				    Point selPt = new Point(e.x,e.y);
				    GeoPosition center = mdMap.convertPointToGeoPosition(selPt);
				    zoomIn();
				    mdMap.setCenterPosition(center);
				}
            }
        };
        
        this.addListener(SWT.MouseDown, l);
        this.addListener(SWT.MouseMove, l);	
        this.addListener(SWT.MouseDoubleClick, l);
        
    }
    
    private void zoomOut() {
        int curZoom = zoomScale.getSelection();
        zoomScale.setSelection(curZoom + 1);
	    zoomScale.notifyListeners(SWT.Selection,new Event());
    }
    
    private void zoomIn() {
        int curZoom = zoomScale.getSelection();
        zoomScale.setSelection(curZoom - 1);
	    zoomScale.notifyListeners(SWT.Selection,new Event());
    }
    
    private void setMapZoom(int zoom) {
        zoomScale.setSelection(zoom);
        zoomScale.notifyListeners(SWT.Selection,new Event());
    }

    private int getMapZoom() {
        return zoomScale.getSelection();
    }
    
    private void checkMapFile() {
    
    	String fileName = MFile.getMapPath()+"\\Maps.zip";
    	
    	try {
    		File zipFile = new File(fileName);
    		if(zipFile.exists() == true)
    			return;
    			
			fileName 	= MFile.getMapBackupPath()+"\\Maps.zip";
			zipFile 	= new File(fileName);
			if(zipFile.exists() == true) {
				
				fileName 							= MFile.getMapPath()+"\\Maps.zip";
				BufferedInputStream 	in 			= new BufferedInputStream(new FileInputStream(zipFile));
				BufferedOutputStream	out			= new BufferedOutputStream(new FileOutputStream(fileName));
				byte[] 					buffer		= new byte[4096];
				int 					bytesRead	= in.read(buffer);
				
				while(bytesRead > 0) {
					out.write(buffer, 0, bytesRead);
					bytesRead = in.read(buffer);
				}
				
				in.close();
				out.close();
				return;
				
			}
			
			fileName 				= MFile.getMapPath()+"\\Maps.zip";
			ZipOutputStream out 	= new ZipOutputStream(new FileOutputStream(fileName));
			ZipEntry 		entry 	= new ZipEntry("MD-Map-Entry");
			
			out.putNextEntry(entry);
			out.close();

    	} catch(Exception e) {
    		System.out.println("Following error occured while checking for Map.zip.");
    		System.out.println(e.getMessage());
    	}
    	
    }

    private void createMenuItems() {
        
        itmRefresh 	= new MenuItem(popup,SWT.PUSH);
        itmRefresh.setText("Refresh"); 		
        itmRefresh.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
                redraw();
            }
        });
        itmAdjustMode = new MenuItem(popup,SWT.CHECK);
        itmAdjustMode.setText("Adjust Map/Nodes");
        itmAdjustMode.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
            	handleAdjustMode(itmAdjustMode.getSelection());
            }
        });
        
        itmTrackMode 	= new MenuItem(popup,SWT.CHECK);
        itmTrackMode.setText("Track All Nodes");
        itmTrackMode.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
           		handleTrackMode(itmTrackMode.getSelection());
            }
        });
    }
    
    private void handleAdjustMode(boolean enable) {
    	adjustMode = enable;
    	itmAdjustMode.setSelection(enable);
    	mdMap.setMoveAllowed(adjustMode);
		zoomScale.setEnabled(adjustMode);
		plusZoom.setEnabled(adjustMode);
		minusZoom.setEnabled(adjustMode);
		mdMap.resize();
		Rectangle viewBounds 	= getClientArea();
		Point center 			= new Point(viewBounds.x+(viewBounds.width/2),
											viewBounds.y+(viewBounds.height/2));
		mdMap.setCenterPosition(mdMap.convertPointToGeoPosition(center));
		updateNodePositions();
    	
    	if(adjustMode == true) {
    		trackMode = false;
    		itmTrackMode.setSelection(false);
    	}
		
    	redraw();
    }
    
    protected void handleTrackMode(boolean enable) {
        
    	if(enable == true && networkUI.isMeshViewerStarted() == false)
    		return;
    	
    	trackMode = enable;
    	itmTrackMode.setSelection(trackMode);
    	if(trackMode == false)
    		stopMapAdjustTimer();
    	else {
    		handleAdjustMode(false);
    		startMapAdjustTimer();
    	}
    }

	private void redrawRect(Rectangle rect) {
        if(rect != null) {
        	mdMap.redraw(rect.x,rect.y,rect.width,rect.height,true);
        	update();
        }
	}
	
	private void paintControlBackground(GC paintGC) {
	    
		if(backGC == null)
			return;
		
		backGC.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		backGC.fillRectangle(backBuffer.getBounds());
		
		mdMap.drawMapTiles(backGC);
		
		Rectangle bounds 		= this.getBounds();
		Rectangle logoBounds 	= logoImage.getBounds();
		backGC.drawImage(logoImage,bounds.width -logoBounds.width-15,bounds.height - logoBounds.height-15);
		
		if(!networkUI.isMeshViewerStarted()) {
			paintGC.drawImage(backBuffer,0,0);
			return;
		}
		
		Enumeration<AccessPointHandler> apEnum = accessPointHandlers.elements();
		while(apEnum.hasMoreElements()) {
			
			AccessPointHandler apHandler = (AccessPointHandler) apEnum.nextElement();

            //dsunil(Ver  10.1): If the root line is hidden, show circle around the root node
            if(apHandler.isRoot()) {
                ShowRoot(paintGC, apHandler.getUI());
            }

			if(apHandler.isRunning() == false) {
				continue;
			}
			
			if(apHandler.getKapLineState() != MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE) {
				Enumeration<LineInfo> kapEnum = apHandler.getKnownApLines();
				while(kapEnum.hasMoreElements()) {
					LineInfo kapLine = (LineInfo) kapEnum.nextElement();
					kapLine.drawLine(backGC);
				}
			}
		}
		
		/*
		 * we use different loops for knownap lines and ds lines
		 * that way ds lines are drawn after all knownap lines and
		 * are hence visible on top of known ap lines 
		 */
		
		apEnum = accessPointHandlers.elements();
		while(apEnum.hasMoreElements()) {
			
			AccessPointHandler apHandler = (AccessPointHandler) apEnum.nextElement();
			if(apHandler.isRunning() == false) {
				continue;
			}
			
			DefaultLineInfo dsLine = apHandler.getDsLine();
            if(dsLine.getDstPoint() instanceof EthLinePoint ) {
                dsLine.drawCircle(backGC);
            }else{
                if(apHandler.getAssocLineState() == MeshNetworkUIProperties.ASSOCIATIONLINE_STATE_SHOW)  {
                    //dsunil(Ver  10.1): If the root line is hidden, show circle around the root node
                    if(apHandler.isRoot() && properties.getRootLine() == MeshNetworkUIProperties.ROOTLINE_STATE_HIDE) {
                        dsLine.drawCircle(backGC);
                    } else {
                        dsLine.drawLine(backGC);
                    }
                }
            }
		}
		
		paintGC.drawImage(backBuffer,0,0);
		
	}
	
    private void updateApLocation(AccessPointUI apUIObj) {
        
        AccessPointHandler 	apH 	= (AccessPointHandler)accessPointHandlers.get(apUIObj.getDsMacAddress().toString());
       	apH.setViewLocation(apUIObj.getLocation());
        
        LineInfo line = apH.getDsLine();
        if(line == null) {
        	return;
        }
        ILinePoint ptDest = line.getDstPoint();
        if(ptDest instanceof EthLinePoint) {
            Point pt = EthLinePoint.getConnectionPoint(apH.getPoint(), getBounds());
            EthLinePoint ethPt = (EthLinePoint) ptDest;
            ethPt.updatePoint(pt);
        }
        
    }
    
	/**
	 * @param string
	 * @return
	 */
	protected AccessPointHandler getApHandler(String dsMacAddress) {
		
		AccessPointHandler apHandler = (AccessPointHandler)accessPointHandlers.get(dsMacAddress);
		
		if(apHandler != null) {
			return apHandler;
		}
		
    	OfflineAccessPointUI apUI = new OfflineAccessPointUI(this, this);
        accessPointUIs.put(dsMacAddress, apUI);

        Rectangle 	rect = apUI.getBounds();
        int 		xpos = accessPointUIs.size() / 10;
        int			ypos 	= 10;
        xpos   				= xpos + 10;
        
        rect.x = accessPointUIs.size() * 20 + (xpos * 10);
        rect.y = ypos + accessPointUIs.size() * 20;
        
        apUI.setBounds(rect);
        apUI.addControlListener(new ControlAdapter(){

            public void controlResized(ControlEvent arg0) {
                controlMoved(arg0);
            }
            public void controlMoved(ControlEvent arg0) {
            	updateApLocation((AccessPointUI)arg0.getSource());
            	redraw();
            }
        });

        apHandler = new AccessPointHandler(this,null, apUI,networkUI.getProperties());
        apHandler.setDSMacAddress(dsMacAddress);
        accessPointHandlers.put(dsMacAddress, apHandler);
        apUI.setApHandler(apHandler);
         notifyStatusBar();
		return apHandler;
	}

	/**
	 * @param properties The properties to set.
	 */
	public void recalculateNeighbourLineState() {

		Enumeration<AccessPointHandler> enumeration	= accessPointHandlers.elements();
		int 		neighbourStateNoneCount 		= 0;
		int 		apHandlerCount					= accessPointHandlers.size();
		
		while(enumeration.hasMoreElements()) {
			AccessPointHandler apHandler = (AccessPointHandler) enumeration.nextElement();
			if(apHandler.isRoot() == true){
				apHandlerCount--;
				continue;
			}
			
			if(apHandler.getKapLineState() == MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE) {
				neighbourStateNoneCount++;
			}
		}
		
		if(neighbourStateNoneCount == 0)
			properties.setNeighbourLines(MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_ALL);
		else if(apHandlerCount == neighbourStateNoneCount)
			properties.setNeighbourLines(MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE);
		else
			properties.setNeighbourLines(MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_PARTIAL);
	}
	
	private void notifyStatusBarCoordinates(String statusText) {
	    networkUI.notifyStatusBar(statusText);
	}
	
	public void parentChanged(AccessPointHandler handler) {
		if(isDisposed() == false) {
			redraw();
		}
	}
	
	public void parentInfoChanged(AccessPointHandler handler) {
		
		DefaultLineInfo line = handler.getDsLine();
		Rectangle rect = line.getBitrateCoordinates();
   		redrawRect(rect);
   		
	}

	public void knownApListChanged(AccessPointHandler handler) {
	
		if(isDisposed() == false) {
			redraw();
		}
	
	}
	
	public void knownApInfoChanged(AccessPointHandler handler,DefaultLineInfo line) {
		
		Rectangle rect = line.getBitrateCoordinates();
		redrawRect(rect);
		
	}

	public void lineStateChanged(AccessPointHandler handler) {
		recalculateNeighbourLineState();
		redraw();
	}
	
	public void lineInfoStateChanged(AccessPointHandler handler) {
		redraw();
	}

	public void refresh() {

		Enumeration<AccessPointHandler> apEnum = accessPointHandlers.elements();
		
		while(apEnum.hasMoreElements()) {
			AccessPointHandler apHandler = (AccessPointHandler) apEnum.nextElement();
			apHandler.refresh();
		}
	}
	
    /**
     * @param is
     */
    public void load() {
   	
    	try{
    		FileInputStream in = new FileInputStream(MFile.getNetworkPath() + "/" + networkUI.getNwName() + "-map.dat");    		
    		properties.read(in);
    		loadAccessPoints(in);
    		in.close();
    	}catch(Exception e){
    		System.out.println(e.getMessage());
    	}
    }

    /**
     * @param os
     */
    public void save() {
    	
    	try{
    		FileOutputStream os = new FileOutputStream(MFile.getNetworkPath() + "/" + networkUI.getNwName() + "-map.dat");
    		properties.write(os);
	    	saveAccessPoints(os);
	    	os.close();
    	}catch(Exception e){
    		System.out.println(e.getMessage());
    	} 	
    }
    
    private void loadAccessPoints(InputStream in) {
		
		byte[] 		oneByte  	= new byte[1];
		int 		apCount;
		MacAddress	dsMacAddress = new MacAddress();
		
		try {
		    
			in.read(oneByte);
			int zoom = oneByte[0];
			
			setMapZoom(zoom);
					    
			in.read(oneByte);
			int mode = oneByte[0];
			
			if(mode == 1) {
			    handleAdjustMode(true);
			}else{
				handleAdjustMode(false);
			} 
			
			in.read(oneByte);
			mode = oneByte[0];
			
			if(mode == 1) {
			    handleTrackMode(true);
			}else{
				handleTrackMode(false);
			} 
			
			loadCenterofMap(in);
			
			in.read(oneByte);
			apCount = oneByte[0];
			
			while(apCount-- > 0) {
				dsMacAddress.read(in);
				AccessPointHandler 	apHandler = getApHandler(dsMacAddress.toString());
				apHandler.load(in);
				AccessPointUI		apUI = accessPointUIs.get(dsMacAddress.toString());
				apUI.load(in);
			}
		} catch (IOException ee) {
			Mesh.logException(ee);
		}
	}
   
    public void saveAccessPoints(OutputStream out) {
		
		Enumeration<AccessPointHandler> e	= accessPointHandlers.elements();
		byte[] oneByte						= new byte[1];
		
		try {
		    
		    oneByte[0] = (byte) getMapZoom();
			out.write(oneByte);
			
			if(adjustMode == true)
			    oneByte[0] = (byte) 1;
			else
			    oneByte[0] = (byte) 0;
			out.write(oneByte);
			
			if(trackMode == true)
			    oneByte[0] = (byte) 1;
			else
			    oneByte[0] = (byte) 0;
			out.write(oneByte);
			
			saveCenterofMap(out);
			
			oneByte[0] = (byte) accessPointHandlers.size();
			out.write(oneByte);
			
			while(e.hasMoreElements()) {
				
				AccessPointHandler 	accessPointHandler 	= e.nextElement();
				AccessPointUI		apUI				= accessPointHandler.getUI();
				MacAddress			dsAddress			= accessPointHandler.getDSMacAddr();
				
				dsAddress.write(out);
				accessPointHandler.save(out);
				apUI.save(out);
				
			}
	    	
		} catch (IOException ee) {
			Mesh.logException(ee);
		}        			
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#editProperties()
	 */
	public boolean editProperties() {
		MeshNetworkPropertiesDialog dlg = new MeshNetworkPropertiesDialog(networkUI.getNwName(), networkUI.getProperties());
		dlg.show();
		if(dlg.getStatus() != SWT.OK) {
			return false;
		}
		
		Enumeration<AccessPointHandler> enumeration = accessPointHandlers.elements();
		while(enumeration.hasMoreElements()) {
			AccessPointHandler apHandler = (AccessPointHandler) enumeration.nextElement();
			apHandler.refreshProperties();
		}
		this.properties.setResetNeighbourLineInfo(false);
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#notifyHealthAlert(int, java.lang.String)
	 */
	public void notifyHealthAlert(int healthStatus, String meshId) {
		if(isDisposed() == true) {
			return;
		}
		redraw();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#onClose()
	 */
	public void onClose() {
	    stopMapAdjustTimer();
	}

	public void apPropertyChanged(int filter, int subFilter, AccessPoint src) {
		AccessPointHandler apHandler = (AccessPointHandler) accessPointHandlers.get(src.getDSMacAddress());
		if(apHandler != null) {
			apHandler.apPropertyChanged(filter,subFilter,src);
		} else
			accessPointAdded(src);
		
		if(filter != IAPPropListener.MASK_HEARTBEAT)
			return;
		
		if(adjustMode == false && mdMap.isMoveAllowed() == false &&
			trackMode == false) { 
			if(updateNodePosition(src) == true)
				redraw();
		}
	
	}
	
	public Rectangle getAPSEnclosingRegion() {
	 
	    Rectangle APRegion  = null;
	    
	    Enumeration<AccessPointUI> apEnum 	= accessPointUIs.elements();
		
		while(apEnum.hasMoreElements()) {
			AccessPointUI apUI 	= apEnum.nextElement();
			Rectangle bounds 	= apUI.getBounds();
			if(APRegion == null) {
		        APRegion = new Rectangle(bounds.x,bounds.y,bounds.height,bounds.width);
		    }else{
		        APRegion = APRegion.union(bounds);
		    }
		}
	    return APRegion;
	}
	
	public void adjustMap() {
	    
	    Point 		center 		= new Point(0,0);
    
	    if(trackMode == false)
	        return;
	    
	    if(this.isDisposed() == true)
	        return;

	    Rectangle clientArea = this.getClientArea();
        
        if(clientArea.height <= 0 && clientArea.width <= 0)
            return;/*minimized state*/
        
        updateNodePositions();
        
        apRegion = getAPSEnclosingRegion();
        
		if(apRegion == null)
		    return;
		
		int x1 = apRegion.x;
		int y1 = apRegion.y;
		int x2 = apRegion.x + apRegion.width;
		int y2 = apRegion.y + apRegion.height;
		
		center.x = (x1 + x2) / 2; 
		center.y = (y1 + y2) / 2;
		
    	if(isRectinRect(clientArea, apRegion) == false) {
		    if((clientArea.height <= apRegion.height) ||
		       (clientArea.width <= apRegion.width)) {
	            zoomOut();
	        }
		    
        	GeoPosition geoPos = mdMap.convertPointToGeoPosition(center);
            mdMap.setCenterPosition(geoPos);
		}

    	if(mdMap.getZoom() == MDMap.MAXZOOM)
    		return;
    	
		if((clientArea.height >= 2*(apRegion.height)) &&
	 	  (clientArea.width >= 2*(apRegion.width))) {
	       zoomIn();
	    }
	}
	
	private boolean isRectinRect(Rectangle clientArea, Rectangle rBounds) {
        
        int x1 = rBounds.x;
		int y1 = rBounds.y;
		int x2 = rBounds.x + rBounds.width;
		int y2 = rBounds.y + rBounds.height;
		
        if(clientArea.contains(x1, y1) &&
           clientArea.contains(x2, y1) &&
           clientArea.contains(x1, y2) &&
           clientArea.contains(x2, y2))
        {
            return true;
        }
        return false;
    }

    public OutputStream saveCenterofMap(OutputStream out) {
        
        Rectangle rect 	= getClientArea();
        Point center	= new Point(0,0);
        
        byte[] 	fourBytes 	= new byte[4];
		
        int x1 = rect.x;
		int y1 = rect.y;
		int x2 = rect.x + rect.width;
		int y2 = rect.y + rect.height;
		
		center.x = (x1 + x2) / 2; 
		center.y = (y1 + y2) / 2;
		
		GeoPosition cPos =  mdMap.convertPointToGeoPosition(center);
		String strCenter = cPos.toString();
        
        try {
            int len  = strCenter.length();		
    		Bytes.intToBytes(len,fourBytes);		
    		out.write(fourBytes);
    		out.write(strCenter.getBytes());
            
    		
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return out;
    }
    
    private void loadCenterofMap(InputStream in) {


        byte[] 	fourBytes 	= new byte[4];
        byte[] 		buffer;
        String 		geoPosCenter;
        double		latitude;
        double		longitude;

        try {
            in.read(fourBytes);
            int len = Bytes.bytesToInt(fourBytes);
            if(len > 0) {
    			buffer = new byte[len];
    			in.read(buffer);
    			geoPosCenter = new String(buffer);
    			/*parse "[lat,long]"*/
    			geoPosCenter 		= geoPosCenter.replace("["," ");
    			geoPosCenter		= geoPosCenter.replace("]"," ");
    			
    			String[] geoPos 	= geoPosCenter.split(",");
    			latitude 			= Double.parseDouble(geoPos[0]);
    			longitude 			= Double.parseDouble(geoPos[1]);
    			
    			GeoPosition	center	= new GeoPosition(latitude,longitude);
    			
    	        mdMap.setCenterPosition(center);
    		}
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public void startMapAdjustTimer() {
        
        if(panTimer != null)
            return;
        
        panTimer 		= new MapAdjustTimer(this);
        panTimerThread	= new Thread(panTimer);
        panTimerThread.start();
      
    }
    
    public void stopMapAdjustTimer() {

    	if(panTimer == null)
            return;
        panTimer.stop();
        panTimer 		= null;
        panTimerThread	= null;
        
    }
    
	public GeoPosition getNodeGeoPosition(MacAddress dsMacaddress) {
	    
	    if(dsMacaddress == null) return null;
	    
	    AccessPointHandler apHandler = (AccessPointHandler)accessPointHandlers.get(dsMacaddress.toString());
	    if(apHandler == null)
	    	return null;
	    
	    Point location = apHandler.getLocation();
	    return mdMap.convertPointToGeoPosition(location);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.ui.views.MeshNetworkView#selectionChanged(boolean)
	 */
	public void selectionChanged(boolean selected) {
		if(selected == true && networkUI.isMeshViewerStarted() == true && trackMode == true) {
			startMapAdjustTimer();
		} else if(selected == false && networkUI.isMeshViewerStarted() == true) {
			if(trackMode == true)
				stopMapAdjustTimer();
			mdMap.stopServices();
		}
	}

	private boolean updateNodePosition(AccessPoint ap) {

		AccessPointUI apUI = accessPointUIs.get(ap.getDSMacAddress());
		
		if(apUI == null)
			return false;
		
		IRuntimeConfiguration runtimeConfig = ap.getRuntimeApConfiguration();
		
		String latitude 	= runtimeConfig.getLatitude();
		if(latitude == null || latitude.length() <= 0)
			return false;
		
		String longitude	= runtimeConfig.getLongitude();
		if(longitude == null || longitude.length() <= 0)
			return false;
		
		GeoPosition gPos 	= new GeoPosition(Double.parseDouble(latitude),
												Double.parseDouble(longitude));	
		Point pt 			= mdMap.convertGeoPositionToPoint(gPos);
		Point oldLocation 	= apUI.getLocation();
		
		if(oldLocation.x != pt.x || oldLocation.y != pt.y) {
			apUI.setLocation(pt);
			return true;
		}
	
		return false;
	}
	
	private void updateNodePositions() {
		
		Enumeration<AccessPointHandler> e 	= accessPointHandlers.elements();
		while(e.hasMoreElements()) {
			AccessPointHandler 	apHandler 	= (AccessPointHandler)e.nextElement();
			AccessPoint			ap			= apHandler.getAccesspoint();
			
			if(ap == null)
				continue;
			
			updateNodePosition(ap);
		}
		
	}
	
	/**
	 * 
	 */
	public void redraw() {

        if(adjustMode == false && mdMap.isMoveAllowed() == false)
        	updateNodePositions();

        GC gc = new GC(this);
        paintControlBackground(gc);
        gc.dispose();

	}
	
	public void meshViewerStarted() {
		if(trackMode == true)
			startMapAdjustTimer();
	}
	
	public void meshViewerStopped() {
		if(trackMode == true)
			stopMapAdjustTimer();
	}
	
	public boolean isNodeMoveAllowed() {
		return adjustMode;
	}
	
}

class MapAdjustTimer implements Runnable {
	
    boolean 		stop;
	OfflineMapView 	thisObj;
	
	public MapAdjustTimer(OfflineMapView thisObj) {
		super();
		stop = true;
		this.thisObj = thisObj;
	}

	public void run() {

		int	sleepInterval 	= 1000;
		stop = false;
		while (!stop) {
			try {
				Thread.sleep(sleepInterval);
			} catch (InterruptedException e) {
				if(stop == true) {
					System.out.println("MapAdjustTimer stopped.");
					break;
				}
			}
			try {
				Display.getDefault().asyncExec(new Runnable() {
	                public void run() {
	                   thisObj.adjustMap();
	                }
	            });
			} catch(Exception e) {
				e.printStackTrace();
				if(stop == true)
					return;
			}
			Runtime.getRuntime().gc();
		}
	}
	
	public void stop() {
        stop = true;
    }
	
	
}
