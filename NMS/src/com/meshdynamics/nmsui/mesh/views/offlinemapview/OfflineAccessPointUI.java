/**********************************************************************************
 * MeshDynamics
 * --------------
 * File     : AccessPointUI.java
 * Comments :
 * Created  : May 4, 2005
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History
 * -------------------------------------------------------------------------------
 * | No  |Date        |  Comment                                        | Author |
 * -------------------------------------------------------------------------------
 * |  0  |May 4, 2005 | Created                                         | Anand  |
 * -------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.mesh.views.offlinemapview;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

import com.meshdynamics.api.NMSUI;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.HardwareInfo.InterfaceData;
import com.meshdynamics.nmsui.MeshViewerUI;
import com.meshdynamics.nmsui.dialogs.UIConstants;
import com.meshdynamics.nmsui.mesh.MeshNetworkUIProperties;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI.NodeDisplayText;
import com.meshdynamics.nmsui.mesh.views.AccessPointHandler;
import com.meshdynamics.nmsui.mesh.views.AccessPointUI;
import com.meshdynamics.nmsui.mesh.views.AccessPointUIMouseListener;
import com.meshdynamics.util.Bytes;
import com.meshdynamics.util.MacAddress;

public class OfflineAccessPointUI extends Canvas implements AccessPointUI {
    
	private class IfData {
		public String 	name;
		public int 		channel;
		public int 		type;
		public int 		subType;
	}
	
	private class ElementProperty {
		public String label;
		public String color;
	}
	
	private static Image _imgOn;
	private static Image _imgOff;
	private static Image _imgScan;
	private static Image _imgHbMiss;
	
	static {
		try {
			_imgOn = new Image(null, AccessPointUI.class.getResourceAsStream("icons/MeshOn.gif"));
		} catch(Exception e) {
			_imgOn = null;
		}
		try {
			_imgOff = new Image(null, AccessPointUI.class.getResourceAsStream("icons/MeshOff.gif"));
		} catch(Exception e) {
			_imgOff = null;
		}
		try {
			_imgScan = new Image(null, AccessPointUI.class.getResourceAsStream("icons/MeshScan.gif"));
		} catch(Exception e) {
			_imgScan = null;
		}
		try {
			_imgHbMiss = new Image(null, AccessPointUI.class.getResourceAsStream("icons/MeshHbMiss.gif"));
		} catch(Exception e) {
			_imgHbMiss = null;
		}
		
	}
	
	/*
	 * control variables
	 */
	private boolean 					groupSelect;
	private String						model;
	private String						name;
	private long 						signal;
	private long 						txRate;
	private int 						apState;
	private long 						boardTemperature;
	private long						flags;
	private	int							voltage;
	private long						lastHeartbeatTime;
		
	private Image						imgOn;
	private Image						imgOff;
	private Image						imgScanning;
	private Image						imgHbMissed;
	
	private AccessPointUIMouseListener 	apUIML;
	private AccessPointHandler			apHandler;
	private OfflineMapView				networkView;
	Vector<NodeDisplayText>				nodeDisplayText;
	private Point 						centerOfControl;
	private boolean 					onOff;	
	Vector<IfData>						interfaces;
	private boolean 					selected;

	private String 						onImagePath;
	private String 						offImagePath;
	private String 						scanImagePath;
	private String 						hbMissImagePath;
	
	private	boolean						ifInitDone;
	private ElementProperty				elementProperty1;
	private ElementProperty				elementProperty2;

	/**
	 * @param arg0
	 * @param arg1
	 */
	public OfflineAccessPointUI(Composite parent, OfflineMapView meshNetworkView) {
		super(parent, SWT.SIMPLE);
		
		this.apHandler			= null;
		this.networkView		= meshNetworkView;
		name 					= "";
		model					= "";
		init();
		loadControls(parent);

		centerOfControl 		= new Point(0,0);
		onOff 					= true;
		interfaces				= new Vector<IfData>();

		imgOn 					= _imgOn;
		imgOff 					= _imgOff;
		imgScanning 			= _imgScan;
		imgHbMissed 			= _imgHbMiss;
		onImagePath				= "";
		offImagePath			= "";
		scanImagePath			= "";
		hbMissImagePath			= "";
		ifInitDone				= false;
		elementProperty1		= new ElementProperty();
		elementProperty2		= new ElementProperty();
	}

	private void init() {
		signal					= 0;
		txRate					= 0;
		boardTemperature		= 0;
		apState					= STATE_DEAD;
		flags 					= 0;
		nodeDisplayText			= new Vector<NodeDisplayText>();
		selected				= false;
	}
	
	private void loadControls(Composite parent) {
		
		this.setBounds(0, 0, 16, 16);
		this.setBackground(UIConstants.RED_COLOR);
    	this.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				switch(apState) {
					case STATE_DEAD:
						if(imgOff != null)
							e.gc.drawImage(imgOff, 0, 0);
						else {
							e.gc.setBackground(UIConstants.RED_COLOR);
							e.gc.fillRectangle(getBounds());
						}
						break;
					case STATE_SCANNING:
						if(imgScanning != null)
							e.gc.drawImage(imgScanning, 0, 0);
						else {
							e.gc.setBackground(UIConstants.BLUE_COLOR);
							e.gc.fillRectangle(getBounds());
						}
						break;
					case STATE_RUNNING:
						if(onOff == false) {
							if(imgOff != null)
								e.gc.drawImage(imgOff, 0, 0);
							else {
								e.gc.setBackground(UIConstants.YELLOW_COLOR);
								e.gc.fillRectangle(getBounds());
							}
						} else {
							if(imgOn != null)
								e.gc.drawImage(imgOn, 0, 0);
							else {
								e.gc.setBackground(UIConstants.GREEN_COLOR);
								e.gc.fillRectangle(getBounds());
							}
						}
						break;
					case STATE_HB_MISSED:
						if(imgHbMissed != null)
							e.gc.drawImage(imgHbMissed, 0, 0);
						else {
							e.gc.setBackground(UIConstants.ORANGE_COLOR);
							e.gc.fillRectangle(getBounds());
						}
						break;
				}
			}
		});
    	
    	apUIML = new AccessPointUIMouseListener(networkView, this);
    	this.addMouseListener(apUIML);
    	this.setToolTipText("");
	}

	private void setApStateRunning() {
		apState		= STATE_RUNNING;
	}
	
	private void setApStateScanning() {
		apState		= STATE_SCANNING;
	}

	private void setApStateDead() {
		apState		= STATE_DEAD;
		init();
		if(isGroupSelected() == true) {
			uiEventSetGroupSelect(false);
		}
		onOff = true;
		redraw();
	}

	public void setVoltage(int voltage) {
		this.voltage = voltage;
	}

	protected void notifySelectionEvent() {
		if(apHandler != null)
			networkView.selectAccessPoint(apHandler.getDSMacAddr().toString());
	}

	/**
	 * @return Returns the apState.
	 */
	public int getApState() {
		return apState;
	}

	/**
	 * @param boardTemperature The boardTemperature to set.
	 */
	public void setBoardTemperature(long boardTemperature) {
		this.boardTemperature = boardTemperature;
	}

	/**
	 * @param controlSelected The controlSelected to set.
	 */
	public void setSelected(boolean controlSelected) {
		this.selected = controlSelected;
		updateDisplay();
	}

	public void uiEventSetGroupSelect(boolean val){
		groupSelect = val;
		apHandler.uiEventGroupSelectionChanged(val);
	}
	
	public boolean isGroupSelected() {
		return groupSelect;
	}

	/**
	 * @return Returns the dsMacAddress.
	 */
	public MacAddress getDsMacAddress() {
		return apHandler.getDSMacAddr();
	}

	/**
	 * @param name The name to set.
	 */
	public void setAPName(String name) {
		this.name = name;
		updateDisplay();
	}

	public void setAPModel(String ModelName) {
		this.model = ModelName;
		updateDisplay();
	}

	/**
	 * @param signal The signal to set.
	 */
	public void setSignal(long signal) {
		this.signal 	= signal;
		updateDisplay();
	}

	/**
	 * @param txRate The txRate to set.
	 */
	public void setTxRate(long txRate) {
		this.txRate = txRate;
		updateDisplay();
	}

	/**
	 * @param dsIf
	 * @param wmIfs
	 */
	public void setHardwareInfo(InterfaceData dsIf, InterfaceData[] wmIfs,AccessPoint src) {
		
		interfaces.clear();
		IfData data = new IfData();
		data.name = dsIf.getName();
		data.channel = dsIf.getChannel();
		data.type = dsIf.getIfType();
		data.subType = dsIf.getIfSubType();
		interfaces.add(data);
		
		for(InterfaceData wmIf : wmIfs) {
			data 			= new IfData();
			data.name 		= wmIf.getName();
			data.channel 	= wmIf.getChannel();
			data.type 		= wmIf.getIfType();
			data.subType 	= wmIf.getIfSubType();
			interfaces.add(data);
		}
		
	}

	/**
	 * @return Returns the controlDetails.
	 */
	public byte getControlDetails() {
		return AccessPointUI.CONTROL_MAXIMIZED;
	}

	/**
	 * @param controlDetails The controlDetails to set.
	 */
	public void setControlDetails(byte controlDetails) {
	}

	public int getShowNeighbourLineInfo() {
		return apHandler.getkapLineInfoState();
	}

	public void setShowNeighbourLineInfo(int info) {
		apHandler.setkapLineInfoState(info);
	}

	public boolean isShowKnownAPLines() {
		return (apHandler.getKapLineState() == MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE) ?
					false : true;
	}

	public void setShowKnownAPLines(boolean showKnownAPLines) {
		int newLineState;
		
		newLineState = (showKnownAPLines == true) ? MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_ALL :
			MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE;
				
		apHandler.setKapLineState(newLineState);
	}

	public void heartbeatReceived() {
		
		apState = STATE_RUNNING;
		long currentTime = System.currentTimeMillis();
		if((currentTime - lastHeartbeatTime) < 3000)
			return;
		
		lastHeartbeatTime = currentTime;
		
		MeshViewerUI.display.timerExec(500, new Runnable () {
	        public void run () {
	        	try{
	        		long 		currentTime 	= System.currentTimeMillis();
	        		long 		timeDiff 		= currentTime - lastHeartbeatTime;
	        		
	        		if(currentTime == lastHeartbeatTime)
	        			updateDisplay();
	        		else if(timeDiff < 3000){
    				 	if(apState != STATE_DEAD) {
    				 		onOff = !onOff;
    				 		MeshViewerUI.display.timerExec(500,this);
    				 	} else {
    				 		onOff = false;
    				 	}
    				 	if(!isDisposed())
    				 		redraw();
    				 	
    				 	return;
        			} else {
        				onOff = true;
        				if(!isDisposed())
        					redraw();
        			}
	        	}catch(Exception e){
	        		System.out.println(e.getMessage());
	        	}
			}
		});
	}

	public void setApHandler(AccessPointHandler apHandler) {
		this.apHandler = apHandler;
	}

	public void addNodeDisplayText(NodeDisplayText nodeText) { 
	    nodeDisplayText.add(nodeText);
	    updateDisplay();
    }

	public void removeNodeDisplayText(NodeDisplayText nodeText) {
		nodeDisplayText.remove(nodeText);
    }
    
    protected void updateDisplay() {
    	
    	String  toolTip = "";

    	if(selected == true) 
    		toolTip += "Selected : Yes" + SWT.CR + SWT.LF + SWT.CR + SWT.LF;
    	
    	toolTip += "Unit Address : " + ((apHandler != null) ? apHandler.getDSMacAddr().toString() : "") + SWT.CR + SWT.LF;
    	toolTip += "Name : " + this.name + SWT.CR + SWT.LF;
    	toolTip += "Model : " + this.model + SWT.CR + SWT.LF;
    	toolTip += "Signal : " + this.signal + " dBm" + SWT.CR + SWT.LF;
    	toolTip += "BitRate : " + this.txRate + " Mbps" + SWT.CR + SWT.LF;
    	toolTip += "Temperature : " + this.boardTemperature + " C" + SWT.CR + SWT.LF;
    	toolTip += "Voltage : " + this.voltage + " V" + SWT.CR + SWT.LF;
    	toolTip += "" + SWT.CR + SWT.LF;
    	toolTip += "Interfaces : " + SWT.CR + SWT.LF;
    	for(IfData data : interfaces) {
    		toolTip += data.name + " : ";
    		switch(data.type) {
	    		case Mesh.PHY_TYPE_802_3:
	    			toolTip += "Ethernet" + SWT.CR + SWT.LF;
	    			break;
	    		case Mesh.PHY_TYPE_802_11:
	    			toolTip += "Wireless : ";
	    			break;
    		}
    		
    		if(data.type == Mesh.PHY_TYPE_802_11) {
        		switch(data.subType) {
		    		case Mesh.PHY_SUB_TYPE_802_11_A:
		    			toolTip += "802.11a : ";
		    			break;
		    		case Mesh.PHY_SUB_TYPE_802_11_B:
		    			toolTip += "802.11b : ";
		    			break;
		    		case Mesh.PHY_SUB_TYPE_802_11_G:
		    			toolTip += "802.11g : ";
		    			break;
		    		case Mesh.PHY_SUB_TYPE_802_11_B_G:
		    			toolTip += "802.11bg : ";
		    			break;
		    		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ:
		    			toolTip += "802.11 Public Safety 10MHz : ";
		    			break;
		    		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ:
		    			toolTip += "802.11 Public Safety 20MHz : ";
		    			break;
		    		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ:
		    			toolTip += "802.11 Public Safety 5MHz : ";
		    			break;
		    		default:
		    			toolTip += "unknown : ";
        		}
    			toolTip += "Channel : " + data.channel + SWT.CR + SWT.LF;
    		}
    	}

		if(nodeDisplayText.size() > 0) {
        	toolTip += "" + SWT.CR + SWT.LF;
        	toolTip += "Extension Info : " + SWT.CR + SWT.LF;
			
			for(NodeDisplayText t : nodeDisplayText) {
				toolTip += t.nodeTextId + " : " + t.nodeText + SWT.CR + SWT.LF; 
			}
		}
    	
    	if(flags != 0) {
	    	toolTip += "" + SWT.CR + SWT.LF;
	    	toolTip += "Flags : " + SWT.CR + SWT.LF;
	    	toolTip += (isGroupSelected() == true) ? "Group Selected " + SWT.CR + SWT.LF : "";
	    	toolTip += ((flags & MESH_FLAG_REBOOT_REQUIRED) != 0) ? "Reboot Required" : "";
	    	toolTip += ((flags & MESH_FLAG_RADAR_DETECT) != 0) ? "Radar Detected" : "";
    	}
    	
    	if(elementProperty1.label != null || elementProperty2.label != null) {
	    	toolTip += "" + SWT.CR + SWT.LF;
	    	toolTip += "Element Properties  : " + SWT.CR + SWT.LF;
	    	if(elementProperty1.label != null)
	    		toolTip += elementProperty1.label + " : " +elementProperty1.color + SWT.CR + SWT.LF;
	    	if(elementProperty2.label != null)
	    		toolTip += elementProperty2.label + " : " +elementProperty2.color + SWT.CR + SWT.LF;
    	}
    	
    	this.setToolTipText(toolTip);
    	this.getToolTipText();
    	
    	redraw();
	}

	public boolean isNodeMoveAllowed() {
    	return networkView.isNodeMoveAllowed();
    }

	@Override
	public Point getPoint() {
    	if(this.isDisposed())
    		return null;
        Rectangle rect = this.getBounds();
        
        centerOfControl.x = rect.x + rect.width /2;
        centerOfControl.y = rect.y + rect.height/2;
        return centerOfControl;
	}

	@Override
	public void setDisplayText(int nodeDisplayValue) {
	}

	@Override
	public void updateNodeDisplayText(NodeDisplayText t) {
		MeshViewerUI.display.asyncExec (new Runnable () {
			public void run () {
				updateDisplay();
			}
		});
	}

	@Override
	public void setApState(int state) {
		switch(state) {
			case STATE_DEAD:
				setApStateDead();
				break;
			case STATE_SCANNING:
				setApStateScanning();
				break;
			case STATE_RUNNING:
				setApStateRunning();
				break;
			case STATE_HB_MISSED:
				setApStateHbMissed();
				break;
		}
	}

	private void setApStateHbMissed() {
		apState = STATE_HB_MISSED;
		updateDisplay();
	}

	@Override
	public void handleSelectionEvent() {
		this.selected = true;
		if(apHandler != null)
			apHandler.notifySelection();
		
		updateDisplay();
	}

	@Override
	public void load(InputStream in) {
		byte[] 	fourBytes = new byte[4];
		int 	len;
		String  imgPath;
		byte[]	buffer;
		int states[] = {STATE_DEAD, STATE_HB_MISSED, STATE_RUNNING, STATE_HB_MISSED};
		
		try {
			for(int state : states) {
				in.read(fourBytes);
				len = Bytes.bytesToInt(fourBytes);
				if(len <= 0)
					continue;
				
				buffer = new byte[len];
				in.read(buffer);
				imgPath = new String(buffer);
				setApStateImage(state, imgPath);
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void save(OutputStream out) {
		byte[] 		fourBytes = new byte[4];
		int 		len;
		String[] 	imgPaths = {offImagePath, hbMissImagePath, onImagePath, scanImagePath}; 
		try {
			for(String path : imgPaths) {
				
				len = path.length();
				Bytes.intToBytes(len, fourBytes);
				out.write(fourBytes);
				if(len <= 0)
					continue;
				
				out.write(path.getBytes());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void setApStateImage(int state, String imagePath) {
		
		if(imagePath == null)
			return;
		if(imagePath.trim().equals("") == true)
			return;

		Image img = null;
		try {
			img = new Image(null, imagePath);
		} catch(Exception e) {
			img = null;
		}
		
		if(img == null)
			return;
		
		switch(state) {
			case STATE_DEAD:
				offImagePath 	= imagePath;
				imgOff 			= img;
				break;
			case STATE_SCANNING:
				scanImagePath 	= imagePath;
				imgScanning		= img;
				break;
			case STATE_RUNNING:
				onImagePath 	= imagePath;
				imgOn 			= img;
				break;
			case STATE_HB_MISSED:
				hbMissImagePath = imagePath;
				imgHbMissed		= img;
				break;
		}
		
		redraw();
	}

	@Override
	public void setInterfaceInfo(IInterfaceConfiguration ifConfig) {
		if(ifInitDone == true)
			return;
	
		for(int i=0;i<ifConfig.getInterfaceCount();i++) {
			
			IInterfaceInfo ifInfo = ifConfig.getInterfaceByIndex(i);

			for(IfData data : interfaces) {
				
				if(data.name.equalsIgnoreCase(ifInfo.getName()) == false)
					continue;

				data.type	 	= ifInfo.getMediumType();
				data.subType 	= ifInfo.getMediumSubType();
			}
		
		}
		
		ifInitDone = true;
		
	}

	@Override
	public void setFlags(long flags) {
		this.flags = flags;
	}

	@Override
	public void setElementProperty(String property, String value) {
		if(property.equalsIgnoreCase(NMSUI.NODE_ELEMENT_PROPERTY_LED1_COLOR) == true)
			elementProperty1.color = value;
		else if(property.equalsIgnoreCase(NMSUI.NODE_ELEMENT_PROPERTY_LED2_COLOR) == true)
			elementProperty2.color = value;
		else if(property.equalsIgnoreCase(NMSUI.NODE_ELEMENT_PROPERTY_LED1_LABEL) == true) 
			elementProperty1.label = value;
		else if(property.equalsIgnoreCase(NMSUI.NODE_ELEMENT_PROPERTY_LED2_LABEL) == true)
			elementProperty2.label = value;
		
		MeshViewerUI.display.asyncExec(new Runnable(){
			@Override
			public void run() {
				updateDisplay();
			}
		});
		
	}

}
