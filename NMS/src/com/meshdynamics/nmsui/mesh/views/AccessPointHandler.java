/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : AccessPointUIHandler.java
 * Comments : 
 * Created  : Oct 8, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * | 22  |June 5,2007  | line management added							 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 21  |May 28,2007  |heartbeat references removed             		 |Abhishek|
 * --------------------------------------------------------------------------------
 * | 20  |May 24,2007  | group selection implementation					 |Abhijit |
 * -------------------------------------------------------------------------------
 * | 19  |May 09,2007  | changes due to modification of IAPPropListener	 |Abhijit |
 * -------------------------------------------------------------------------------
 * | 18  |Mar 20,2007  |  changes for voltage							 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 17  |Mar 16,2007  |  signal corrected in refreshLEDs and            | Imran  |
 * |	 |			   |  updateHeartBeatProperty					 	 |  	  |
 * --------------------------------------------------------------------------------
 * | 16  |Mar 08,2007  |  changes for saturation info					 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 15  |Feb 27,2007  |  removed unused code							 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 14  |Nov 10, 2006| LED bug changes					  	 			 |Abhishek|
 * --------------------------------------------------------------------------------
 * | 13  |Mar 16, 2006|Led changes							           	 |Mithil  |
 *  --------------------------------------------------------------------------------
 * | 12  |Feb 17, 2006|propertyChanged function pollinfo fix           	 |Prachiti|
 *  --------------------------------------------------------------------------------
 * | 11  |Feb 17, 2006|4452 and 4458 tooltip change                    	|Mithil  |
 *  --------------------------------------------------------------------------------
 * | 10  |Feb 16, 2006|Config Sequence Number change                    |Prachiti |
 *  --------------------------------------------------------------------------------
 * |  9  |Feb 15, 2006 | Health index removed							 | Mithil |
 * --------------------------------------------------------------------------------
 * |  8  |Jan 17, 2006 | If APUI disposed then getPoint shld return null | Mithil |
 * --------------------------------------------------------------------------------
 * |  7  |Jan 04,2006  | Dbm changed to dBm			              		 | Mithil |
 * -------------------------------------------------------------------------------
 * |  6  |Nov 25,2005  | Changes for set Monitor Mode              		 | Mithil |
 * ------------------------------------------------------------------------------- 
 * |  5  |Sep 25, 2005 | Changes for view specific led settings  		 | Abhijit|
 * --------------------------------------------------------------------------------
 * |  4  |Sep 2, 2005  | Independent view coordinates,currentview added  | Abhijit|
 * --------------------------------------------------------------------------------
 * |  3  |Apr 20, 2005 | HEARTBEAT_MASK_TEMPERATURE added                | Bindu  |
 * --------------------------------------------------------------------------------
 * |  2  |Mar 07, 2005 | DynPropertyMaskparam added to propertyChanged() | Bindu  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 11, 2005 | Change Detection-PropertyUI changes             | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 8, 2004  | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.mesh.views;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.swt.graphics.Point;

import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.configuration.KnownAp;
import com.meshdynamics.meshviewer.configuration.KnownAp.KnownApInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.HardwareInfo;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;
import com.meshdynamics.meshviewer.mesh.ap.HardwareInfo.InterfaceData;
import com.meshdynamics.nmsui.http.IJsonMeshInfoProvider.IJsonAp;
import com.meshdynamics.nmsui.mesh.DefaultLineInfo;
import com.meshdynamics.nmsui.mesh.EthLinePoint;
import com.meshdynamics.nmsui.mesh.ILinePoint;
import com.meshdynamics.nmsui.mesh.LineInfo;
import com.meshdynamics.nmsui.mesh.MeshNetworkUIProperties;
import com.meshdynamics.util.Bytes;
import com.meshdynamics.util.MacAddress;

public class AccessPointHandler implements IAPPropListener, ILinePoint, IJsonAp {
	
    private AccessPointUI 						apUI;
    private MeshNetworkUIProperties 			networkProperties;
    private AccessPoint 						ap;
    private DefaultLineInfo						dsLine;
    private MeshNetworkView						networkView;
    private Hashtable<String, LineInfo>			knownApLines;
    private int 								kapLineInfoState;
    private int									kapLineState;
    private int									assocLineState;
    private MacAddress							dsMacAddress;
    private Point								viewLocation;
    /**
     * 
     */
    public AccessPointHandler (MeshNetworkView networkView, AccessPoint ap, AccessPointUI apUI,MeshNetworkUIProperties networkProperties) {
    
    	this.networkView		= networkView;
        this.apUI 				= apUI;
        this.networkProperties 	= networkProperties;
        this.ap 				= ap;
        this.dsMacAddress		= new MacAddress();
        
        if(ap != null) {
        	setDSMacAddress(ap.getDSMacAddress());
        }
        
        viewLocation 		= apUI.getLocation();
        dsLine				= new DefaultLineInfo(this,null);
        knownApLines		= new Hashtable<String, LineInfo>();
        kapLineInfoState	= MeshNetworkUIProperties.SHOW_LINE_INFO_ALL;
        kapLineState		= networkProperties.getNeighbourLines();
        
        assocLineState		= networkProperties.getAssociationLines();
        
    }

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IAccessPointPropertyListener#apPropertyChanged(int, com.meshdynamics.meshviewer.mesh.AccessPoint)
	 */
	public void apPropertyChanged(int filter, int subFilter, AccessPoint src) {
	    try {
		    switch(filter) {
		    case MASK_AP_STATE:
		        updateApStateProperty(subFilter);
		        break;
		    case MASK_HEARTBEAT:
		    case MASK_HEARTBEAT2:
		        updateHeartBeatProperty(subFilter);
		        break;
		    case MASK_HWINFO:
		        updateHardwareInfo(subFilter);
		        break;
		    }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
	}
	
	private void updateHardwareInfo(int subFilter) {

	    if(ap == null)
	        return;
	    
		HardwareInfo 	info	= ap.getHardwareInfo();
		if(info == null)
			return;
		
        InterfaceData   dsIf 	= info.getDsInterfaceInfo();
        InterfaceData[] wmIfs 	= info.getWmInterfaceInfo();
        
        apUI.setHardwareInfo(dsIf, wmIfs,ap);
        apUI.setAPModel(info.getHardwareModel());
        
    }
    
    /**
     * @param subFilter
     * @param src
     */
    private void updateHeartBeatProperty(int subFilter) {
        
    	if(ap  == null || ap.isRunning() == false) {
    		return;
    	}
    	
    	IRuntimeConfiguration runtimeConfig = ap.getRuntimeApConfiguration();
    	
    	long flags = 0;
    	if(runtimeConfig.isRadarDetected())
    		flags |= AccessPointUI.MESH_FLAG_RADAR_DETECT;
    	
    	if(runtimeConfig.isRebootRequired())
    		flags |= AccessPointUI.MESH_FLAG_REBOOT_REQUIRED;
    	
   		apUI.setFlags(flags);
    	apUI.setAPName(ap.getName());
    	apUI.heartbeatReceived();
        apUI.setVoltage(runtimeConfig.getVoltage());
        if(ap.isRoot() == false) {
        	apUI.setSignal(runtimeConfig.getParentSignal());
        	apUI.setTxRate(runtimeConfig.getBitRate());
        }
        
        apUI.setBoardTemperature((long)runtimeConfig.getBoardTemperature());
        
    	updateDSLine();
   		updateKnownApLines();
   		
   		if(ap.canConfigure() == true) {
   			apUI.setInterfaceInfo(ap.getConfiguration().getInterfaceConfiguration());
   		}
    }

    private void clearLines() {
    	dsLine.dstPoint = null;
    	networkView.parentChanged(this);
    	knownApLines.clear();
    	networkView.knownApListChanged(this);
	}

	private void updateKnownApLines() {
		
		if(ap == null) {
			return;
		}
		
		if(ap.isRoot() == true) {
			return;
		}
	
		IRuntimeConfiguration runtimeConfig 		= ap.getRuntimeApConfiguration();
		int 					kapCount 			= runtimeConfig.getKnownApCount();
		boolean 				knownApListChanged 	= false;
		
		if(kapCount <= 0 && knownApLines.size() > 0) {
			knownApLines.clear();
			networkView.knownApListChanged(this);
			return;
		}
		
		Enumeration<KnownAp> kapEnum 	= runtimeConfig.getKnownAPs();
	
		while(kapEnum.hasMoreElements()) {

			KnownAp 		knownAp = (KnownAp) kapEnum.nextElement();
			DefaultLineInfo line 	= (DefaultLineInfo) knownApLines.get(knownAp.macAddress.toString());
			
			if(line == null) {
				
				AccessPointHandler apHandler = networkView.getAccesspointHandler(knownAp.macAddress);
				if(apHandler == null) {
					continue;
				}
				
				if(apHandler.isRunning() == false) {
					continue;
				}
				
				line = new DefaultLineInfo(this,apHandler);
				line.setLineType(LineInfo.LINE_KAP, MeshNetworkUIProperties.DEFAULT_KAP_LINK_COLOR);
				knownApLines.put(knownAp.macAddress.toString(), line);
				knownApListChanged = true;
				
			} 

			if(updateLineInfo(knownAp.getInfo(),line,runtimeConfig.getUplinkTransmitRate()) == true) {
				networkView.knownApInfoChanged(this,line);
			}
		}

		Enumeration<LineInfo> lineEnum = knownApLines.elements();
		while(lineEnum.hasMoreElements()) {
			
			DefaultLineInfo line = (DefaultLineInfo) lineEnum.nextElement();
			if(line == null) {
				continue;
			}

			AccessPointHandler apHandler = (AccessPointHandler) line.dstPoint;
			if(apHandler == null) {
				continue;
			}
			
			MacAddress 	dsMacAddress 	= apHandler.getDSMacAddr();
			if(runtimeConfig.getKnownAp(dsMacAddress) == null) {
				knownApLines.remove(dsMacAddress.toString());
				knownApListChanged = true;
			}			
		}
		
		if(knownApListChanged == true) {
			networkView.knownApListChanged(this);
		}
	}

	private boolean updateLineInfo(Enumeration<KnownApInfo> knownApInfo, DefaultLineInfo line,long uplinkTransmitRate) {
	
    	String 					oldInfo			= line.getInfo();
    	String 					newInfo			= "";
    	
    	if(kapLineInfoState != MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE) {
    		
	    	while(knownApInfo.hasMoreElements()) {
	
	    		KnownApInfo info = (KnownApInfo) knownApInfo.nextElement();
	
	    		newInfo			+= "[";
	    		
	        	if(kapLineInfoState == MeshNetworkUIProperties.SHOW_LINE_INFO_ALL) {
	        		newInfo	+= info.signal + "/" + uplinkTransmitRate;
	        	} else if(kapLineInfoState == MeshNetworkUIProperties.SHOW_LINE_INFO_BIT_RATE) {
	        		newInfo	+= info.bitRate + " Mbps";
	        	} else if(kapLineInfoState == MeshNetworkUIProperties.SHOW_LINE_INFO_SIGNAL) {
	        		newInfo	+= info.signal + " dBm";
	        	}
	        	
	        	newInfo			+= "]";
	        	 if(line.equals(dsLine) == true) {
	        		break;
	        	}
	       }
	    	
    	}
    	
		if(newInfo.equalsIgnoreCase(oldInfo) == false) {
			line.setInfo(newInfo);
			return true;
		}
    	
    	return false;
		
	}

	private void updateDSLine() {
		
		if(ap == null) {
			return;
		}
		
		IRuntimeConfiguration 	runtimeConfig 	= ap.getRuntimeApConfiguration();
		
    	if(ap.isRoot() == true) {

            // The root line needs to be hidden if ROOTLINE_STATE_HIDE
            if(networkProperties.getRootLine() == MeshNetworkUIProperties.ROOTLINE_STATE_HIDE) {
                dsLine.dstPoint = null;
            }

    		if(dsLine.dstPoint == null || (dsLine.dstPoint instanceof EthLinePoint) == false) {
                if(networkProperties.getRootLine() == MeshNetworkUIProperties.ROOTLINE_STATE_SHOW) {
                    Point pt = EthLinePoint.getConnectionPoint(this.getPoint(), networkView.getClientArea());
                    dsLine.setDstPoint(new EthLinePoint(pt));
                    dsLine.setLineType(LineInfo.LINE_DS_ETH,MeshNetworkUIProperties.DEFAULT_ETH_LINK_COLOR);
                    }


                networkView.parentChanged(this);
    		}
    	} else {
    		 if(networkProperties.getRootLine() == MeshNetworkUIProperties.ROOTLINE_STATE_HIDE) {
                 dsLine.dstPoint = null;
             }
	    	AccessPoint parentAp = ap.getParentAp();
	    	if(parentAp == null) {
	    		dsLine.dstPoint = null;
	    		networkView.parentChanged(this);
	    		return;
	    	}
	    	
	    	if(parentAp.isRunning() == false) {
	    		dsLine.dstPoint = null;
	    		networkView.parentChanged(this);
	    		return;
	    	}
	    	
	    	AccessPointHandler	parentApHandler = networkView.getAccesspointHandler(parentAp.getDsMacAddress());
	    	ILinePoint 			dsParent		= dsLine.getDstPoint();
	   	
	    	if(dsParent == null || parentApHandler.equals(dsParent) == false) {
	    		dsLine.setLineType(LineInfo.LINE_DS_WIRELESS, MeshNetworkUIProperties.DEFAULT_WIRELESS_LINK_COLOR);
	    		dsLine.setDstPoint(parentApHandler);
	    		networkView.parentChanged(this);
	    	}

    	}
    	
		if(updateLineInfo(runtimeConfig.getParentInfo(),dsLine,runtimeConfig.getUplinkTransmitRate()) == true) {    			
			networkView.parentInfoChanged(this);
		}
    	
	}

	/**
     * @param subFilter
     * @param src
     */
    private void updateApStateProperty(int subFilter) {

        switch(subFilter) {
	        case IAPPropListener.AP_STATE_DEAD:
	        	clearLines();
	        	networkView.accesspointStopped(this);
	            apUI.setApState(AccessPointUI.STATE_DEAD);
	            break;
	        case IAPPropListener.AP_STATE_SCANNING:
	        	clearLines();
	        	apUI.setApState(AccessPointUI.STATE_SCANNING);
	            break;
	        case IAPPropListener.AP_STATE_STARTUP:
	        	networkView.accesspointStarted(this);
	        	apUI.setApState(AccessPointUI.STATE_RUNNING);
	        	break;
	        case IAPPropListener.AP_STATE_ALIVE:
	        	apUI.setApState(AccessPointUI.STATE_RUNNING);
	            break;
	        case IAPPropListener.AP_STATE_HEARTBEAT_MISSED:
	        	apUI.setApState(AccessPointUI.STATE_HB_MISSED);
	            break;
        }
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.mesh.ui.ILinePoint#getPoint()
     */
    public Point getPoint() {
    	return apUI.getPoint();
    }

    /**
     * 
     */
    public MacAddress getDSMacAddr() {
        return dsMacAddress;
    }

	public AccessPointUI getUI(){
		return apUI;
	}
	
	public void setViewLocation(Point viewLocation) {
		this.viewLocation = viewLocation;
	}
	
	public void uiEventGroupSelectionChanged(boolean currentSelectionState) {
		
		String selectionMessage = (currentSelectionState == true) ? 
									Mesh.SELECTED : Mesh.UNSELECTED;
		
		if(ap == null) {
			return;
		}
		
		ap.showMessage(ap, Mesh.MACRO_GROUP_SELECTION,selectionMessage , "");
		networkView.groupSelectionChanged();
	}

	public DefaultLineInfo getDsLine() {
		 return dsLine;
	}

	public Enumeration<LineInfo> getKnownApLines() {
		return knownApLines.elements();
	}
	
	public boolean isRunning() {
		return (ap != null) ? ap.isRunning() : false;
	}

	public void refreshProperties() {
		
		if(ap == null) {
			return;
		}
		
		networkProperties 					= networkView.getProperties();
		Enumeration<LineInfo> knownApEnum 	= knownApLines.elements();

		if(ap.isRoot() == false)
			assocLineState = networkProperties.getAssociationLines();
		else
			assocLineState = MeshNetworkUIProperties.ASSOCIATIONLINE_STATE_SHOW;
		
		if(ap.isRoot() == false) {
			dsLine.setLineColor(networkProperties.getWirelessLinkColor());
		}
		
		while(knownApEnum.hasMoreElements()) {
			DefaultLineInfo line = (DefaultLineInfo) knownApEnum.nextElement();
			line.setLineColor(networkProperties.getKapLinkColor());
		}
		
		byte state	 = networkProperties.getNeighbourLines();
		if(state != MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_PARTIAL) { 
			kapLineState 		= state;
		}
		kapLineInfoState 	= networkProperties.getNeighbourLineInfo();

		apUI.setDisplayText(networkProperties.getNodeDisplayValue());
		
		updateDSLine();
		updateKnownApLines();
		
	}

	public int getkapLineInfoState() {
		return kapLineInfoState;
	}

	public void setkapLineInfoState(int lineInfoState) {
		this.kapLineInfoState = lineInfoState;
		if(isRunning() == false) {
			return;
		}
		updateDSLine();
		updateKnownApLines();
		networkView.lineInfoStateChanged(this);
	}

	public void apStarted(AccessPointHandler handler) {
		
		if(ap == null) {
			return;
		}
		
		if(ap.isRoot() == true) {
			return;
		}
		IRuntimeConfiguration 	runtimeConfig 	= ap.getRuntimeApConfiguration();
		MacAddress 				parentBssid 	= runtimeConfig.getParentBssid();
		
		if(parentBssid == null) {
			return;
		}
		
		if(handler.containsAddress(parentBssid) == true) {
			if(assocLineState != MeshNetworkUIProperties.ASSOCIATIONLINE_STATE_HIDE) {
				updateDSLine();
			}
		}
		
		MacAddress startedApAddress = handler.getDSMacAddr();
		
		if(runtimeConfig.getKnownAp(startedApAddress) != null) {
	    	if(kapLineState != MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE) {
	    		updateKnownApLines();
	    	}
		}
	}

	private boolean containsAddress(MacAddress parentBssid) {
		return ap.containsAddress(parentBssid);
	}

	public int getKapLineState() {
		return kapLineState;
	}

	public void setKapLineState(int lineState) {
		this.kapLineState = lineState;
		networkView.lineStateChanged(this);
	}

	public boolean isRoot() {
		return (ap != null) ? ap.isRoot() : false;
	}

	public void apStopped(AccessPointHandler handler) {
		if(ap == null) {
			return;
		}
		
		if(ap.isRoot() == true) {
			return;
		}

		IRuntimeConfiguration 	runtimeConfig 	= ap.getRuntimeApConfiguration();
		MacAddress 				parentAddress 	= runtimeConfig.getParentDSMacAddress();
		
		if(parentAddress == null) {
			return;
		}
		
		MacAddress stoppedApAddress = handler.getDSMacAddr();
		
		if(parentAddress.equals(stoppedApAddress) == true) {
			dsLine.dstPoint = null;
			networkView.parentChanged(this);
		}

		if(runtimeConfig.getKnownAp(stoppedApAddress) == null) {
			return;
		}

		Enumeration<LineInfo> knownApEnum = knownApLines.elements();
		
		while(knownApEnum.hasMoreElements()) {
			DefaultLineInfo line = (DefaultLineInfo) knownApEnum.nextElement();
			if(handler.equals(line.dstPoint) == true) {
				knownApLines.remove(stoppedApAddress.toString());
				if(kapLineState != MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE) {
					networkView.knownApListChanged(this);
		    	}
			}
		}
	}

	public AccessPoint getAccesspoint() {
		return ap;
	}

	public void refresh() {
		dsLine.dstPoint = null;
		updateDSLine();
		knownApLines.clear();
		updateKnownApLines();
	}

	/**
	 * @param out
	 */
	public void save(OutputStream out) {
		
		byte[]	oneByte		= new byte[1];
		byte[] 	fourBytes 	= new byte[4];
		
    	try {
    		
    		oneByte[0] = (byte) apUI.getControlDetails();
    		out.write(oneByte);
    		
    		Bytes.intToBytes(viewLocation.x,fourBytes);
			out.write(fourBytes);
	    	Bytes.intToBytes(viewLocation.y,fourBytes);
	    	out.write(fourBytes);

			Bytes.intToBytes(kapLineInfoState,fourBytes);
	    	out.write(fourBytes);
	    	Bytes.intToBytes(kapLineState,fourBytes);
	    	out.write(fourBytes);
	    	
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
	}

	/**
	 * @param in
	 */
	public void load(InputStream in) {
		
		byte[]	oneByte		= new byte[1];
		byte[] 	fourBytes 	= new byte[4];

		try {
			in.read(oneByte);
			apUI.setControlDetails(oneByte[0]);
	    	
	    	in.read(fourBytes);
	    	viewLocation.x = Bytes.bytesToInt(fourBytes);
			in.read(fourBytes);
			viewLocation.y = Bytes.bytesToInt(fourBytes);
			
			in.read(fourBytes);
			kapLineInfoState = Bytes.bytesToInt(fourBytes);
			apUI.setShowNeighbourLineInfo(kapLineInfoState);
			
			in.read(fourBytes);
			kapLineState = Bytes.bytesToInt(fourBytes);
			if(kapLineState != MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE) {
				apUI.setShowKnownAPLines(true);
			} else {
				apUI.setShowKnownAPLines(false);
			}
			
			apUI.setLocation(viewLocation);
			refreshProperties();
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

	/**
	 * @param accessPoint
	 */
	public void setAccessPoint(AccessPoint accessPoint) {
		this.ap = accessPoint;
	}

	/**
	 * @param dsMacAddress
	 */
	public void setDSMacAddress(String dsMacAddress) {
		this.dsMacAddress.setBytes(dsMacAddress);
	}

	/**
	 * @return the assocLineState
	 */
	public int getAssocLineState() {
		return assocLineState;
	}

	@Override
	public String getJsonLatitude() {
		String latitude = "";
		if(ap != null)
			latitude = ap.getRuntimeApConfiguration().getLatitude();
		
		if(latitude == "")
			latitude = ap.getConfiguration().getLatitude();
		
		return latitude;
	}

	@Override
	public String getJsonLongitude() {
		String longitude = "";
		if(ap != null)
			longitude = ap.getRuntimeApConfiguration().getLongitude();
		
		if(longitude == "")
			longitude = ap.getConfiguration().getLongitude();
		
		return longitude;
	}

	@Override
	public String getJsonName() {
		if(ap != null)
			return ap.getApName();
		
		return "";
	}

	@Override
	public String getJsonNetworkId() {
		if(ap != null)
			return ap.getNwName();
		
		return "";
	}

	@Override
	public String getJsonNodeId() {
		if(ap == null)
			return "";

		return ap.getNwName() + "/" + ap.getDSMacAddress();
	}

	@Override
	public String getJsonParentNodeId() {
		if(ap == null)
			return "";

		AccessPoint parentAp = ap.getParentAp();
		if(parentAp == null) {
			return "";
		}
		
		return ap.getNwName() + "/" + parentAp.getDSMacAddress();
	}

	@Override
	public int jsonIsMobile() {
		if(ap != null)
			return ap.isMobile();

		return 0;
	}

	public Point getLocation() {
		return viewLocation;
	}
	
	public boolean isGroupSelected() {
		return apUI.isGroupSelected();
	}

	public void setSelected(boolean b) {
		apUI.setSelected(b);
		if(b)
			apUI.setControlDetails(AccessPointUI.CONTROL_MAXIMIZED);
	}
	
	public void notifySelection() {
		networkView.apSelectionChanged(dsMacAddress.toString());
	}
}
