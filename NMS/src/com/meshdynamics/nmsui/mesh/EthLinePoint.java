/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EthLinePoint.java
 * Comments : 
 * Created  : Oct 14, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |May 5, 2006  |Arrow drawing and bitrate display                |Prachiti|
 * --------------------------------------------------------------------------------
 * |  0  |Oct 14, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.mesh;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

import com.meshdynamics.util.Geometry;

public class EthLinePoint implements ILinePoint {

    private Point			point;
    
    public EthLinePoint(Point pt) {
        point = pt;
    }
    
    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.mesh.ui.ILinePoint#getPoint()
     */
    public Point getPoint() {
        return point;
    }

	public static Point getConnectionPoint(Point position, Rectangle bounds) {

	    /**
	     *       1
	     *     ------
	     *    2|    |3
	     *     ------
	     * 		  4 
	     */
	    Point	ret = new Point(0,0);
	      
	    if(position.x < position.y && position.x < (bounds.width-position.x) && position.x < (bounds.height-position.y)) {
	        ret.x = 0;
	        ret.y = position.y;
	    } else if(position.y < (bounds.width-position.x) && position.y < (bounds.height-position.y)) {
	        ret.x = position.x;
	        ret.y = 0;
	    } else if((bounds.width-position.x) < (bounds.height-position.y)) {
	        ret.x = bounds.width;
	        ret.y = position.y;
	    } else {
	        ret.x = position.x;
	        ret.y = bounds.height;
	    }
		return ret;
	}
	
	public static boolean getIntersectPoint(Rectangle rect,ILinePoint nsrc,ILinePoint ndst,Point pt){
	
	    /**
	     *    			TOP HORI
	     *     			 --------
	     *    LEFT VERT	|        |RIGHT VERT
	     *     			 --------
	     * 		  		BTTM HORI 
	     */
	
	    
	    //	TOP HORI
	    Point lsrc	= new Point(rect.x,rect.y); 
	    Point ldst	= new Point(rect.x+rect.width,rect.y);
	    int ret 	= 0;
	    
	    double intersection[] = new double[2];
	    
	    ret = Geometry.findLineSegmentIntersection(
	            lsrc.x,lsrc.y,
	            ldst.x,ldst.y,
	            nsrc.getPoint().x,nsrc.getPoint().y,
				ndst.getPoint().x,ndst.getPoint().y,
				intersection);
	    
	    if(ret == 1){
	        pt.x = (int)intersection[0];pt.y = (int)intersection[1];
	        return true;
	    }
	    
		lsrc.x  = rect.x;
		lsrc.y  = rect.y;
		
		ldst.x	= rect.x;
		ldst.y	= rect.y+rect.height;
		ret = Geometry.findLineSegmentIntersection(lsrc.x,
				lsrc.y,
				ldst.x,
				ldst.y,
				nsrc.getPoint().x,
				nsrc.getPoint().y,
				ndst.getPoint().x,
				ndst.getPoint().y,
				intersection );
		
		if(ret == 1){
		    pt.x 	= (int)intersection[0];pt.y = (int)intersection[1];
		    return true;
		}
		//RIGHT VERT
	    lsrc.x  = rect.x+rect.width;
	    lsrc.y  = rect.y;
	    
	    ldst.x	= rect.x+rect.width;
	    ldst.y	= rect.y+rect.height;
	    
	    ret = Geometry.findLineSegmentIntersection(lsrc.x,
				lsrc.y,
				ldst.x,
				ldst.y,
				nsrc.getPoint().x,
				nsrc.getPoint().y,
				ndst.getPoint().x,
				ndst.getPoint().y,
				intersection );
	    
	    if(ret == 1){
	        pt.x = (int)intersection[0];pt.y = (int)intersection[1];
	        return true;
	    }
		//BOTTOM HORI
	    lsrc.x  = rect.x;
	    lsrc.y  = rect.y+rect.height;
	    
	    ldst.x	= rect.x+rect.width;
	    ldst.y	= rect.y+rect.height;

	    ret = Geometry.findLineSegmentIntersection(lsrc.x,
				lsrc.y,
				ldst.x,
				ldst.y,
				nsrc.getPoint().x,
				nsrc.getPoint().y,
				ndst.getPoint().x,
				ndst.getPoint().y,
				intersection );
	    if(ret == 1){
	        pt.x 	= (int)intersection[0];pt.y = (int)intersection[1];
	        return true;
	    }
	    
	    return false;
	}
	
	public void updatePoint(Point pt) {
        point = pt;
    }
	
}
