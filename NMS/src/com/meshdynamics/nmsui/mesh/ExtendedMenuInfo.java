/*
 * Created on Nov 12, 2007
 *
 */
package com.meshdynamics.nmsui.mesh;



import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import com.meshdynamics.api.NMSUI.ContextMenuSection;
import com.meshdynamics.api.NMSUI.MenuCommandHandler;

/**
 * @author prachiti
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ExtendedMenuInfo implements ContextMenuSection {
    public  Menu				submenu;
    public  MenuItem			menuItem;
    public  int					menuID;
    public 	ContextMenuSection	parentSection;
    public 	String 				text;
    public 	String 				commandId;
    public	Object 				icon;
    public MenuCommandHandler 	handler;
    
	@Override
	public String getId() {
		return commandId;
	}
	@Override
	public ContextMenuSection getParent() {
		return parentSection;
	}
	@Override
	public String getText() {
		return text;
	}
	@Override
	public Object getIcon() {
		return icon;
	}
	@Override
	public boolean isCommand() {
		return (handler == null) ? false : true;
	}
};
