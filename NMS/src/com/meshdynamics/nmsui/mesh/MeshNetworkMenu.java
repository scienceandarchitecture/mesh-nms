/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshNetworkMenu.java
 * Comments : 
 * Created  : Feb 12, 2007
 * Author   : Abhishek Patankar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  2  |Jun 10, 2008 | Menus for Lock Network and SetPassword added    |Prachiti|
 * --------------------------------------------------------------------------------
 * |  1  |May 03, 2007 | New menu items added							 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 12, 2007 | Created                                         |Abhishek|
 * --------------------------------------------------------------------------------
 **********************************************************************************/
package com.meshdynamics.nmsui.mesh;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import com.meshdynamics.meshviewer.mesh.healthmonitor.IHealthMonitor;
import com.meshdynamics.nmsui.MenuListener;


public class MeshNetworkMenu {

	private Menu			menu;
	private MenuItem 		itmShowHealthStatus;
	private MenuItem 		itmResetHealthStatus;
	private MenuItem 		itmLockNetwork;

	private MeshNetworkUI	networkUI;
	private static Image	imgHealthStatus;
	private static Image 	imgRestoreHealth; 
	private static Image	imgMaximizeAll;
	private static Image 	imgMinimizeAll;
	private static Image 	imgSetPassword;
	private static Image 	imgLockNetwork,imgUnLockNetwork;
	
	static {
			imgHealthStatus 	=	new Image(null, MeshNetworkMenu.class.getResourceAsStream("icons/healthstatus.gif"));
			imgRestoreHealth 	=	new Image(null, MeshNetworkMenu.class.getResourceAsStream("icons/restore.jpg"));
			imgMaximizeAll 		=	new Image(null, MeshNetworkMenu.class.getResourceAsStream("icons/MaxButton.gif"));
			imgMinimizeAll 		=	new Image(null, MeshNetworkMenu.class.getResourceAsStream("icons/MinButton.gif"));
			imgSetPassword 		=	new Image(null, MeshNetworkMenu.class.getResourceAsStream("icons/key_add.png"));
			imgLockNetwork 		=	new Image(null, MeshNetworkMenu.class.getResourceAsStream("icons/lock_network.png"));
			imgUnLockNetwork 	=	new Image(null, MeshNetworkMenu.class.getResourceAsStream("icons/unlock_network.png"));
	}
	
	public MeshNetworkMenu(Composite comp, MenuListener menuListener) {
		
		menu = new Menu(comp); 
		
		itmShowHealthStatus = new MenuItem(menu,SWT.PUSH);
		itmShowHealthStatus.setText("Show Health Status"); 		
		itmShowHealthStatus.addSelectionListener(menuListener);
		itmShowHealthStatus.setData(new Integer(MenuListener.VIEWPOPUP_ITEM_HEALTH_MONITOR_STATUS));
		itmShowHealthStatus.setImage(imgHealthStatus);

		itmResetHealthStatus = new MenuItem(menu,SWT.PUSH);
		itmResetHealthStatus.setText("Reset Health Status");
		itmResetHealthStatus.addSelectionListener(menuListener);
		itmResetHealthStatus.setData(new Integer(MenuListener.VIEWPOPUP_ITEM_HEALTH_MONITOR_RESET));
		itmResetHealthStatus.setImage(imgRestoreHealth);

		new MenuItem(menu,SWT.SEPARATOR);
		
		MenuItem itm	= new MenuItem(menu,SWT.PUSH);
		itm.setText("Minimize All Nodes");
		itm.addSelectionListener(menuListener);
		itm.setData(new Integer(MenuListener.VIEWPOPUP_ITEM_MINIMIZEALL));
		itm.setImage(imgMinimizeAll);

		itm	= new MenuItem(menu,SWT.PUSH);
		itm.setText("Maximize All Nodes");
		itm.addSelectionListener(menuListener);
		itm.setData(new Integer(MenuListener.VIEWPOPUP_ITEM_MAXIMIZEALL));
		itm.setImage(imgMaximizeAll);
		
		new MenuItem(menu,SWT.SEPARATOR);
		
		itm	= new MenuItem(menu,SWT.PUSH);
		itm.setText("Set Password for this network");
		itm.addSelectionListener(menuListener);
		itm.setData(new Integer(MenuListener.VIEWPOPUP_ITEM_SET_ADMIN_PASSWORD));
		itm.setImage(imgSetPassword);
		
		itmLockNetwork	= new MenuItem(menu,SWT.PUSH);
		itmLockNetwork.setText("Lock This Network");
		itmLockNetwork.addSelectionListener(menuListener);
		itmLockNetwork.setData(new Integer(MenuListener.VIEWPOPUP_ITEM_LOCK_THE_NETWORK));
		itmLockNetwork.setImage(imgLockNetwork);
	}
	
	public Menu getMenu() {
		return menu;
	}
	
	class MeshNetwokMenuListener extends SelectionAdapter{
		
	public void widgetSelected(SelectionEvent e){
		super.widgetSelected(e);
	  }	
	}

	public void setNetworkUI(MeshNetworkUI networkUI) {
	
		this.networkUI = networkUI;
		
		if(networkUI.getCurrentHealthStatus() == IHealthMonitor.HEALTH_STATUS_DISABLED) {
			itmResetHealthStatus.setEnabled(false);
			itmShowHealthStatus.setEnabled(false);
		} else {
			itmResetHealthStatus.setEnabled(true);
			itmShowHealthStatus.setEnabled(true);
		}
		
		if(networkUI.getSuPassword().length() == 0) {
			itmLockNetwork.setEnabled(false);
		}else {
			itmLockNetwork.setEnabled(true);
		}
		
		setLockThisNetworkMode(networkUI);
	}
	
	public void setLockThisNetworkMode(MeshNetworkUI netUI) {
		
		if(!networkUI.equals(netUI)) 
			return;
		
		if(networkUI.getIsNetworkLocked() == true) {
			itmLockNetwork.setText("UnLock This Network");
			itmLockNetwork.setData(new Integer(MenuListener.VIEWPOPUP_ITEM_UNLOCK_THE_NETWORK));
			itmLockNetwork.setImage(imgUnLockNetwork);
		}else {
			itmLockNetwork.setText("Lock This Network");
			itmLockNetwork.setData(new Integer(MenuListener.VIEWPOPUP_ITEM_LOCK_THE_NETWORK));
			itmLockNetwork.setImage(imgLockNetwork);
		}
	}
}
