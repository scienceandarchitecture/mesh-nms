/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : AccessPointUI.java
 * Comments : 
 * Created  : May 4, 2005
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * -------------------------------------------------------------------------------
 * | No  |Date        |  Comment                                        | Author |
 *  -------------------------------------------------------------------------------
 * |  9  |Feb 13,2007 |colors initialised according to View Settings dlg| Imran  |
 * -------------------------------------------------------------------------------
 * |  9  |Feb 13,2007 |setHealtMonitor status method added           	| Imran  |
 * -------------------------------------------------------------------------------
 * |  8  |Feb 8, 2007 |read method modified for Health Monitor          | Imran  |
 * -------------------------------------------------------------------------------
 * |  7  |Feb 8, 2007 |write method modified                            | Imran  |
 * -------------------------------------------------------------------------------
 * |  6  |Dec 28,2006 |HealthMonitor properties added 					|Abhijit |
 * -------------------------------------------------------------------------------
 * |  6  |Dec 28,2006 |Restructuring 									|Imran |
 * --------------------------------------------------------------------------------
 * |  5  |Nov 09,2006 |misc fix 						                 |Abhishek|
 * --------------------------------------------------------------------------------
 * |  4  |June 21,2006 |misc fix 						                 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  3  |Mar 16, 2006 | Line Color Change			 				     | Mithil |
 * ----------------------------------------------------------------------------------
 * |  2  |Feb 16, 2006 | Grid Color Change			 				     | Mithil |
 * ----------------------------------------------------------------------------------
 * |  1  |Sep 25, 2005| Changes for version 3.0            				| Abhijit|
 * -------------------------------------------------------------------------------
 * |  0  |Jun 21, 2005| Created                                         | Anand  |
 * -------------------------------------------------------------------------------
 */
package com.meshdynamics.nmsui.mesh;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import com.meshdynamics.util.Bytes;

public class MeshNetworkUIProperties {

	public static final byte SHOW_NEIGHBOURLINE_STATE_NONE 		= 0;
	public static final byte SHOW_NEIGHBOURLINE_STATE_PARTIAL 	= 1;
	public static final byte SHOW_NEIGHBOURLINE_STATE_ALL 		= 2;
	
	public static final byte ASSOCIATIONLINE_STATE_SHOW			= 0;
	public static final byte ASSOCIATIONLINE_STATE_HIDE			= 1;

    public static final byte ROOTLINE_STATE_SHOW			= 0;
    public static final byte ROOTLINE_STATE_HIDE			= 1;
	
	public static final int SHOW_LINE_INFO_NOTHING		= 0;
	public static final int SHOW_LINE_INFO_BIT_RATE		= 1;
	public static final int SHOW_LINE_INFO_SIGNAL		= 2;
	public static final int SHOW_LINE_INFO_ALL			= 3;
	
	public static final int NEIGHBOURLINE_INFO_STATE_COUNT		= 4;
	
	public static final Color DEFAULT_ETH_LINK_COLOR			= Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);
	public static final Color DEFAULT_WIRELESS_LINK_COLOR		= Display.getCurrent().getSystemColor(SWT.COLOR_BLUE);
	public static final Color DEFAULT_KAP_LINK_COLOR			= Display.getCurrent().getSystemColor(SWT.COLOR_GRAY);
	public static final Color DEFAULT_GRID_LINE_COLOR			= Display.getCurrent().getSystemColor(SWT.COLOR_GRAY);
	
	public static final int	DISABLE					 			= 0;
	public static final int	ENABLE					 			= 1;
	
	private int							gridSize;
	private String						mapImagePath;
	private Vector<LEDSetup> 			ledContainer;
	private int							nodeDisplayValue;
	private boolean						showGrid;
	private boolean     				showTopo;
	private byte	    				neighbourLines;
	private byte	    				associationLines;
    private byte	    				rootLine;
	private byte						neighbourLineInfo;
	
	private Color 						gridLineColor;
	private Color						ethLinkColor;
	private Color 						wirelessLinkColor;
	private Color 						kapLinkColor;
	private boolean						resetNeighbourLineInfo;
	
	/*
	 * this variable is used to reset line info if UI has explicitely asked to reset
	 * line info for all accesspoints
	 */
	
	public MeshNetworkUIProperties() {
		
		gridSize			= 12;	
		mapImagePath 		= "";
		showGrid			= true;
		showTopo			= true;
		associationLines  	= ASSOCIATIONLINE_STATE_SHOW;
        rootLine  	= ROOTLINE_STATE_SHOW;
		neighbourLines  	= SHOW_NEIGHBOURLINE_STATE_ALL;
		neighbourLineInfo	= SHOW_LINE_INFO_ALL;
		ledContainer 		= new Vector<LEDSetup>();
		
		LEDSetup led1 = new LEDSetup();
		led1.setVisible(true);
		led1.setTypeDescription("LED1");
		led1.setLEDType(LEDSetup.LED_TYPE_TEMP);
		led1.setRedValue(LEDSetup.LED_VALUE_MAX);
		led1.setYellowValue(LEDSetup.LED_VALUE_MID);
		led1.setGreenValue(LEDSetup.LED_VALUE_LOW);
		ledContainer.add(led1);
		
		LEDSetup led2 = new LEDSetup();
		led2.setVisible(true);
		led2.setTypeDescription("LED2");
		led2.setLEDType(LEDSetup.LED_TYPE_BITRATE);
		led2.setRedValue(LEDSetup.LED_VALUE_LOW);
		led2.setYellowValue(LEDSetup.LED_VALUE_MID);
		led2.setGreenValue(LEDSetup.LED_VALUE_MAX);
		ledContainer.add(led2);
		
		gridLineColor 		= DEFAULT_GRID_LINE_COLOR;
		ethLinkColor 		= DEFAULT_ETH_LINK_COLOR;
		wirelessLinkColor 	= DEFAULT_WIRELESS_LINK_COLOR;
		kapLinkColor 		= DEFAULT_KAP_LINK_COLOR;

		resetNeighbourLineInfo = false;
		
	}
		
	public void read(InputStream in){

		byte[] fourBytes = new byte[4];
		byte[] oneByte	 = new byte[1];
		int colorR,colorG,colorB;
		
		try {
			
			in.read(fourBytes);
			nodeDisplayValue = Bytes.bytesToInt(fourBytes);
			
			in.read(fourBytes);
			gridSize = Bytes.bytesToInt(fourBytes);

			in.read(oneByte);
			if(oneByte[0] == 0)
			    showGrid = false;
			else
			    showGrid = true;
			
			in.read(oneByte);
			if(oneByte[0] == 0)
			    showTopo = false;
			else
			    showTopo = true;
			
			in.read(oneByte);
		    neighbourLines = oneByte[0];
			
		    in.read(oneByte);
		    neighbourLineInfo = oneByte[0];
		    
			in.read(fourBytes);
			colorR = Bytes.bytesToInt(fourBytes);
			in.read(fourBytes);
			colorG = Bytes.bytesToInt(fourBytes);
			in.read(fourBytes);
			colorB = Bytes.bytesToInt(fourBytes);
			gridLineColor = new Color(null,colorR,colorG,colorB);
			
			in.read(fourBytes);
			colorR = Bytes.bytesToInt(fourBytes);
			in.read(fourBytes);
			colorG = Bytes.bytesToInt(fourBytes);
			in.read(fourBytes);
			colorB = Bytes.bytesToInt(fourBytes);
			ethLinkColor = new Color(null,colorR,colorG,colorB);
			
			in.read(fourBytes);
			colorR = Bytes.bytesToInt(fourBytes);
			in.read(fourBytes);
			colorG = Bytes.bytesToInt(fourBytes);			
			in.read(fourBytes);
			colorB = Bytes.bytesToInt(fourBytes);
			kapLinkColor = new Color(null,colorR,colorG,colorB);
			
			in.read(fourBytes);
			colorR = Bytes.bytesToInt(fourBytes);
			in.read(fourBytes);
			colorG = Bytes.bytesToInt(fourBytes);			
			in.read(fourBytes);
			colorB = Bytes.bytesToInt(fourBytes);
			wirelessLinkColor = new Color(null,colorR,colorG,colorB);
			
			in.read(fourBytes);
			int len = Bytes.bytesToInt(fourBytes);
			
			if(len > 0){
				byte[] buffer = new byte[len];
				in.read(buffer);
				mapImagePath = new String(buffer);
			}

			in.read(fourBytes);
			int ledCount = Bytes.bytesToInt(fourBytes);

			//if(ledContainer.size() < ledCount || ledContainer.size() > ledCount) {
				ledContainer.removeAllElements();
				ledContainer = new Vector<LEDSetup>(ledCount);
			//}
			
			for(int i = 0; i < ledCount; i++) {
				LEDSetup led = new LEDSetup();				
				led.read(in);
				led.setTypeDescription("LED"+(i+1));
				
				ledContainer.add(led);
			}
			
			in.read(oneByte);
			associationLines = oneByte[0];

            in.read(oneByte);
            rootLine = oneByte[0];

		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void write(OutputStream os){

		byte[] fourBytes    = new byte[4];
		byte[] oneByte  	= new byte[1];
    	
    	try {

    		Bytes.intToBytes(nodeDisplayValue,fourBytes);
	    	os.write(fourBytes);
    		
	    	Bytes.intToBytes(gridSize,fourBytes);
	    	os.write(fourBytes);
	    	
	    	if(showGrid == true)
	    	    oneByte[0] = 1;
	    	else
	    		oneByte[0] = 0;
	    	
	    	os.write(oneByte);

	    	if(showTopo == true)
	    	    oneByte[0] = 1;
	    	else
	    		oneByte[0] = 0;
	    	
	    	os.write(oneByte);

	    	oneByte[0] = neighbourLines;
	    	os.write(oneByte);
	    	
	    	oneByte[0] = neighbourLineInfo;
	    	os.write(oneByte);
	    	
	    	Bytes.intToBytes(gridLineColor.getRed(),fourBytes);
	    	os.write(fourBytes);
	    	Bytes.intToBytes(gridLineColor.getGreen(),fourBytes);
	    	os.write(fourBytes);
	    	Bytes.intToBytes(gridLineColor.getBlue(),fourBytes);
	    	os.write(fourBytes);
	    	
	    	Bytes.intToBytes(ethLinkColor.getRed(),fourBytes);
	    	os.write(fourBytes);
	    	Bytes.intToBytes(ethLinkColor.getGreen(),fourBytes);
	    	os.write(fourBytes);
	    	Bytes.intToBytes(ethLinkColor.getBlue(),fourBytes);
	    	os.write(fourBytes);
	    	
	    	Bytes.intToBytes(kapLinkColor.getRed(),fourBytes);
	    	os.write(fourBytes);
	    	Bytes.intToBytes(kapLinkColor.getGreen(),fourBytes);
	    	os.write(fourBytes);
	    	Bytes.intToBytes(kapLinkColor.getBlue(),fourBytes);
	    	os.write(fourBytes);

	    	Bytes.intToBytes(wirelessLinkColor.getRed(),fourBytes);
	    	os.write(fourBytes);
	    	Bytes.intToBytes(wirelessLinkColor.getGreen(),fourBytes);
	    	os.write(fourBytes);
	    	Bytes.intToBytes(wirelessLinkColor.getBlue(),fourBytes);
	    	os.write(fourBytes);
	    	
	    	Bytes.intToBytes(mapImagePath.length(),fourBytes);
	    	os.write(fourBytes);
			
			os.write(mapImagePath.getBytes());
			
	    	Bytes.intToBytes(ledContainer.size(),fourBytes);
	    	os.write(fourBytes);
	    	for(int i = 0; i < ledContainer.size(); i++) {
	    		LEDSetup led = (LEDSetup)ledContainer.get(i);
	    		led.write(os);
	    	}
	    	
	    	oneByte[0] = associationLines;
	    	os.write(oneByte);

            oneByte[0] = rootLine;
            os.write(oneByte);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return Returns the ledContainer.
	 */
	public Vector<LEDSetup> getLedContainer() {
		return ledContainer;
	}

	public Color getEthLinkColor() {
		return ethLinkColor;
	}
	
	public void setEthLinkColor(Color ethLinkColor) {
		this.ethLinkColor = ethLinkColor;
		
	}
	
	public Color getGridLineColor() {
		return gridLineColor;
	}
	
	public void setGridLineColor(Color gridLineColor) {
		this.gridLineColor = gridLineColor;
	}
	
	public int getGridSize() {
		return gridSize;
	}
	
	public void setGridSize(int gridSize) {
		this.gridSize = gridSize;
	}
	
	public Color getKapLinkColor() {
		return kapLinkColor;
	}
	
	public void setKapLinkColor(Color kapLinkColor) {
		this.kapLinkColor = kapLinkColor;
	}
	
	public String getMapImagePath() {
		return mapImagePath;
	}
	
	public void setMapImagePath(String mapImagePath) {
		this.mapImagePath = mapImagePath;
	}
	
	public byte getNeighbourLines() {
		return neighbourLines;
	}
	
	public void setNeighbourLines(byte neighbourLines) {
		this.neighbourLines = neighbourLines;
	}
	
	public byte getAssociationLines() {
		return associationLines;
	}
	
	public void setAssociationLines(byte associationLines) {
		this.associationLines = associationLines;
	}

    public byte getRootLine() {
        return rootLine;
    }

    public void setRootLine(byte rootLine) {
        this.rootLine = rootLine;
    }

	public int getNodeDisplayValue() {
		return nodeDisplayValue;
	}
	
	public void setNodeDisplayValue(int nodeDisplayValue) {
		this.nodeDisplayValue = nodeDisplayValue;
	}
	
	public boolean isShowGrid() {
		return showGrid;
	}
	
	public void setShowGrid(boolean showGrid) {
		this.showGrid = showGrid;
	}
	
	public boolean isShowTopologyView() {
		return showTopo;
	}
	
	public void setShowTopologyView(boolean enable) {
		this.showTopo = enable ;
	}
	
	public Color getWirelessLinkColor() {
		return wirelessLinkColor;
	}
	
	public void setWirelessLinkColor(Color wirelessLinkColor) {
		this.wirelessLinkColor = wirelessLinkColor;
	}

	public byte getNeighbourLineInfo() {
		return neighbourLineInfo;
	}

	public void setNeighbourLineInfo(byte neighbourLineInfo) {
		this.neighbourLineInfo = neighbourLineInfo;
	}

	public void setResetNeighbourLineInfo(boolean resetNeighbourLineInfo) {
		this.resetNeighbourLineInfo = resetNeighbourLineInfo;
	}

	public boolean isResetNeighbourLineInfo() {
		return resetNeighbourLineInfo;
	}

}
