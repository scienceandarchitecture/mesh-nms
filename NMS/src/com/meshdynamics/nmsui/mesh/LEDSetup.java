/**
 * MeshDynamics 
 * -------------- 
 * File     : LEDSetup.java
 * Comments : 
 * Created  : May 5, 2005
 * Author   : Sneha
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * |  4  |Nov 09, 2006   | LED types Fixes and misc changes           | Abhishek     |
 * ----------------------------------------------------------------------------------
 * |  3  |Feb 14, 2006   | Health Index Removed				           | Mithil      |
 * ----------------------------------------------------------------------------------
 * |  2  |Jan 04, 2006   | BitRate Changed to Transmit Rate           | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  1  |Sep 25, 2005   | Class made non static                      | Abhijit      |
 * ----------------------------------------------------------------------------------
 * |  0  |May 5, 2005    | Created	                                  | Sneha        |
 * ----------------------------------------------------------------------------------
 */

package com.meshdynamics.nmsui.mesh;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.meshdynamics.util.Bytes;

public class LEDSetup {
	

	public static final int LED_TYPE_COUNT					= 3;
	public static final int LED_TYPE_TEMP					= 0;
	public static final int LED_TYPE_BITRATE				= 1;
	public static final int LED_TYPE_SIGNAL					= 2;

	public static int LED_VALUE_LOW							= 0;
	public static int LED_VALUE_MID							= 25;
	public static int LED_VALUE_MAX							= 75;
	
	public static int LED_INDEX_TEMP						= 0;
	public static int LED_INDEX_BITRATE						= 1;
	public static int LED_INDEX_SIGNAL						= 2;
	
	public static final String LED_TYPE_DESC_TEMP			= "Temperature";
	public static final String LED_TYPE_DESC_BITRATE		= "Transmit Rate";
	public static final String LED_TYPE_DESC_SIGNAL			= "Signal";
	
	/**
	 * LED -> 1 RowWidget ->1colorValues 2Darray
	 * 1st Row : Temperature 	set to (INDEX_TEMP, 	MAX,MID, LOW)
	 * 2nd Row : Bitrate	 	set to (INDEX_BITRATE, 	LOW,MID, MAX)
	 * 3rd Row : Signal      	set to (INDEX_SIGNAL, 	LOW,MID, MAX)
	
	 */
	
	
	public int colorValues[][] = {
			{LED_INDEX_TEMP, 	LED_VALUE_MAX, LED_VALUE_MID, LED_VALUE_LOW},
			{LED_INDEX_BITRATE,	LED_VALUE_LOW, LED_VALUE_MID, LED_VALUE_MAX},
			{LED_INDEX_SIGNAL,	LED_VALUE_LOW, LED_VALUE_MID, LED_VALUE_MAX}
	};
	
	private		int 		ledType;
    private 	boolean		visible;
	private		String 		typeDesc;
    
    public LEDSetup() {
    }
    
	/**
	 * @return Returns the greenValue.
	 */
	public int getGreenValue() {
		if(this.ledType 		== LED_TYPE_TEMP)
		     	return this.colorValues[0][3];//02
		else if(this.ledType	== LED_TYPE_BITRATE )
				return this.colorValues[1][3];//12
		else if(this.ledType	== LED_TYPE_SIGNAL)
				return this.colorValues[2][3];//23
	  	
		return 0;
	}
	/**
	 * @param greenValue The greenValue to set.
	 */
	public void setGreenValue(int greenVal) {
		
		if(this.ledType			== LED_TYPE_TEMP)
		      this.colorValues[0][3] = greenVal;//02
		else if(this.ledType	== LED_TYPE_BITRATE )
			 this.colorValues[1][3] = greenVal;
		else if(this.ledType 	== LED_TYPE_SIGNAL)
			 this.colorValues[2][3] = greenVal;
	}
	/**
	 * @return Returns the redValue.
	 */
	public int getRedValue() {
				
		if(this.ledType==LED_TYPE_TEMP)
			return this.colorValues[0][1];
		else if(this.ledType==LED_TYPE_BITRATE )
			return this.colorValues[1][1];
		else if(this.ledType==LED_TYPE_SIGNAL)
			return this.colorValues[2][1];
			
		return 0;
	}
	/**
	 * @param redValue The redValue to set.
	 */
	public void setRedValue(int redVal) {

		if(this.ledType 		== LED_TYPE_TEMP)
		    this.colorValues[0][1] = redVal;
		else if(this.ledType	== LED_TYPE_BITRATE )
			this.colorValues[1][1] = redVal;
		else if(this.ledType	== LED_TYPE_SIGNAL)
			this.colorValues[2][1] = redVal;
		
	}
	/**
	 * @return Returns the yellowValue.
	 */
	public int getYellowValue() {
		if(this.ledType==LED_TYPE_TEMP)
			return this.colorValues[0][2];
		else if(this.ledType==LED_TYPE_BITRATE )
			return this.colorValues[1][2];
		else if(this.ledType==LED_TYPE_SIGNAL)
			return this.colorValues[2][2];
		
		return 0;
	}
	/**
	 * @param yellowValue The yellowValue to set.
	 */
	public void setYellowValue(int yellowVal) {	
		
		if(this.ledType 		== LED_TYPE_TEMP)
		      this.colorValues[0][2] = yellowVal;
		else if(this.ledType 	== LED_TYPE_BITRATE )
			this.colorValues[1][2] = yellowVal;
		else if(this.ledType	== LED_TYPE_SIGNAL)
			this.colorValues[2][2] = yellowVal;
		
	}
    
	/**
	 * @return Returns the ledType.
	 */
	public int getLEDType() {
		return ledType;
	}
	/**
	 * @param ledType The ledType to set.
	 */
	public void setLEDType(int ledType) {
		this.ledType = ledType;
	}
	/**
	 * @return Returns the typeDesc.
	 */
	public String getTypeDescription() {
		return typeDesc;
	}
	/**
	 * @param typeDesc The typeDesc to set.
	 */
	public void setTypeDescription(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	
	/**
	 * @return Returns the visible.
	 */
	public boolean isVisible() {
		return visible;
	}
	/**
	 * @param visible The visible to set.
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	/**
	 * @param fos
	 */
	public void write(OutputStream fos) {

		byte[] fourBytes = new byte[4];

		byte oneByte;
		
		try {
			if(visible)
				oneByte = 1;
			else
				oneByte = 0;
			fos.write(oneByte);
			
			Bytes.intToBytes(ledType, fourBytes);
			fos.write(fourBytes);			
			
		    for(int j=0;j<LEDSetup.LED_TYPE_COUNT;j++ ) {
		    	/**
		    	 * Temp, TX Rate & Signal Values
		    	 */
				
				Bytes.intToBytes(colorValues[j][0],fourBytes);
				fos.write(fourBytes);
				
				Bytes.intToBytes(colorValues[j][3],fourBytes);
				fos.write(fourBytes);
	
				Bytes.intToBytes(colorValues[j][2],fourBytes);
				fos.write(fourBytes);
	
				Bytes.intToBytes(colorValues[j][1],fourBytes);
				fos.write(fourBytes);
		      }
			
/*			oneByte = (byte)ledContainer.size();
			fos.write(oneByte);

			for(int i=0; i < ledContainer.size(); i++) {
				LEDSetup led = (LEDSetup)ledContainer.get(i);
				
				if(led.visible)
					oneByte = 1;
				else
					oneByte = 0;
				fos.write(oneByte);
				
				Bytes.intToBytes(led.getLEDType(),fourBytes);
				fos.write(fourBytes);			
				
			    for(int j=0;j<LEDSetup.LED_TYPE_COUNT;j++ )
			    {
			    	/**
			    	 * Temp, TX Rate & Signal Values
			    	 //
			    	//LEDSetup.LED_TYPE
			    	//Bytes.intToBytes(j,fourBytes);
					//fos.write(fourBytes);
				
					//LEDSetup.LED_INDEX_
					
					Bytes.intToBytes(led.colorValues[j][0],fourBytes);
					fos.write(fourBytes);
					
					Bytes.intToBytes(led.colorValues[j][3],fourBytes);
					fos.write(fourBytes);
		
					Bytes.intToBytes(led.colorValues[j][2],fourBytes);
					fos.write(fourBytes);
		
					Bytes.intToBytes(led.colorValues[j][1],fourBytes);
					fos.write(fourBytes);
			      }
				}*/
			} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param fis
	 */
	public void read(InputStream fis) {
		byte[] 	fourBytes = new byte[4];
		byte[] 	oneByte   = new byte[1];
		try {
				
				fis.read(oneByte);
				if(oneByte[0] == 1)
					visible = true;
				else
					visible = false;
				
				fis.read(fourBytes);
				ledType = Bytes.bytesToInt(fourBytes);
				
				for(int k=0;k<LEDSetup.LED_TYPE_COUNT;k++) {
					
					fis.read(fourBytes);
					colorValues[k][0]= Bytes.bytesToInt(fourBytes);
						
					fis.read(fourBytes);
					colorValues[k][3] = Bytes.bytesToInt(fourBytes);
		
					fis.read(fourBytes);
					colorValues[k][2] = Bytes.bytesToInt(fourBytes);
		
					fis.read(fourBytes);
					colorValues[k][1] = Bytes.bytesToInt(fourBytes);
				}
			
		} catch (IOException e) {
			e.printStackTrace();
		}

/*		byte oneByte[] = new byte[1];
		int count;
		ledContainer.clear();
		try {
			fis.read(oneByte);
			count = oneByte[0];
			System.out.println("count =="+count);
			
			for(int i=0; i < count; i++) {
				LEDSetup led = new LEDSetup();
				
				fis.read(oneByte);
				if(oneByte[0] == 1)
					led.visible = true;
				else
					led.visible = false;
				
				fis.read(fourBytes);
				led.ledType = Bytes.bytesToInt(fourBytes);
				
				for(int k=0;k<LEDSetup.LED_TYPE_COUNT;k++) {
					
					fis.read(fourBytes);
					led.colorValues[k][0]= Bytes.bytesToInt(fourBytes);
						
					fis.read(fourBytes);
					led.colorValues[k][3] = Bytes.bytesToInt(fourBytes);
		
					fis.read(fourBytes);
					led.colorValues[k][2] = Bytes.bytesToInt(fourBytes);
		
					fis.read(fourBytes);
					led.colorValues[k][1] = Bytes.bytesToInt(fourBytes);
				}
				ledContainer.add(led);
			}
		} */
		
	}
	/**
	 * @return Returns the lED_INDEX_TEMP.
	 */
	public int getLEDIndex() {
		if(this.ledType	== LED_TYPE_TEMP)
			 return LED_INDEX_TEMP;
		if(this.ledType == LED_TYPE_BITRATE)
			return LED_INDEX_BITRATE;
		if(this.ledType == LED_TYPE_SIGNAL)
			return LED_INDEX_SIGNAL;
		
		return 0;
			
		
	}
	/**
	 * @param led_index_temp The lED_INDEX_TEMP to set.
	 */
	public void setLEDIndex(int led_index) {
		
		if(this.ledType 		== LED_TYPE_TEMP)
			led_index = LED_INDEX_TEMP;
		else if(this.ledType	== LED_TYPE_BITRATE)
			led_index = LED_INDEX_BITRATE;
		else if(this.ledType	== LED_TYPE_SIGNAL)
			led_index = LED_INDEX_SIGNAL;
			
	}
}
