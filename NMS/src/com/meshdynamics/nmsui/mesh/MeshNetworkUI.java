/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshNetworkUI.java
 * Comments : 
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *   --------------------------------------------------------------------------------
 * | 49  |Sep 06, 2007 | Restructuring									 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 48  |Aug 01, 2007 | apDoubleClickedFromStatusDisplay added          | Imran  |
 *  --------------------------------------------------------------------------------
 * | 47  |May 28,2007  |status tooltip on shell tray item          		 |Prachiti|
 * --------------------------------------------------------------------------------
 * | 46  |May 28,2007  |heartbeat references removed             		 |Abhishek|
 * --------------------------------------------------------------------------------
 * | 45  |May 24,2007  | group selection implementation					 |Abhijit |
 *  --------------------------------------------------------------------------------
 * | 44  |May 24,2007  | updateDlSaturationInfo modified				 |Imran   |
 *  --------------------------------------------------------------------------------
 * | 43  |May 09,2007  | changes due to modification of IAPPropListener	 |Abhijit |
 * -------------------------------------------------------------------------------
 * | 42  |Mar 27,2007  |  recalculateNeighbourLineState modified         |Imran   |
 *  -------------------------------------------------------------------------------
 * | 41  |Mar 26,2007  |  constructor modified       					 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 40  |Mar 21,2007  |  blinker logic modified						 |Abhijit |
 * |	 |			   |  viewer starting logic changed					 |		  |
 * --------------------------------------------------------------------------------
 * | 39  |Mar 20,2007  |  findInAssociationVector,blinker modified       |Abhijit |
 * --------------------------------------------------------------------------------
 * | 38  |Mar 15,2007  |  bitRate color set in setWirelessLinkColor      |Imran   |
 * --------------------------------------------------------------------------------
 * | 37  |Mar 15,2007  |  showNeighbourLinesModified for Map/Detail view |Imran   |
 * --------------------------------------------------------------------------------
 * | 36  |Mar 08,2007  |  fixes for display propreties					 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 35  |Feb 27,2007  |  removed unused code							 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 34  |Feb 14, 2007 | drawBorder & drawGrid modified for HealthMonitor|Abhijit |
 * --------------------------------------------------------------------------------
 * | 33  |Dec 27, 2006 | Changes for kap lines,bitrate display	         |Abhijit |
 * --------------------------------------------------------------------------------
 * | 32  |Nov 27, 2006 | clean-up					  			         |  Bindu |
 * --------------------------------------------------------------------------------
 * | 31  |Oct 26, 2006 | bug fix in UpdateKapBitRate					 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 30  |Oct 15, 2006 | UpdateKnownAPLines & UpdateKapBitRate modified  |Abhijit |
 * --------------------------------------------------------------------------------
 * | 29  |Oct 06, 2006 | signal added to line				             |Bindu   |
 * --------------------------------------------------------------------------------
 * | 28  |July 07, 2006|getAccessPointUI added			                 |Prachiti|
 * --------------------------------------------------------------------------------
 * | 27  |June 21, 2006|fix for bitrate color display	                 |Prachiti|
 * --------------------------------------------------------------------------------
 * | 26  |May 25, 2006 | Toggle added to show/hide KnownAPLines          |Prachiti|
 *  -------------------------------------------------------------------------------
 * | 25  |May 25, 2006 | versionGreaterThan94 added                      | Bindu  |
 * --------------------------------------------------------------------------------
 * | 24  |May 23, 2006 | Root-Relay conversion			                 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 23  |May 19, 2006 | Root-Relay conversion			                 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 22  |May 19, 2006 | Toggle added to show/hide bitrate               |Prachiti|
 *  -------------------------------------------------------------------------------
 * | 21  |May 16, 2006 | addn changes for line drawing					 | Mithil |
 * --------------------------------------------------------------------------------
 * |  20 |May 5, 2006  | updateKnownAPBitRate changed for multiple wms	 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  19 |May 5, 2006  | updateKnownAPBitRate(AccessPoint)			 	 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  18 |Apr 15, 2006 | Redraw Problems							 	 | Mithil |
 * --------------------------------------------------------------------------------
 * |  17 |Apr 03, 2006 | assoc Line not drawn			 				 | Mithil |
 * --------------------------------------------------------------------------------
 * |  16 |Mar 16, 2006 | Line Color Change			 				     | Mithil |
 * --------------------------------------------------------------------------------
 * |  15 |Mar 10, 2006 | Kap and assoc lines drawn to dead node		     | Mithil |
 * --------------------------------------------------------------------------------
 * |  14 |Feb 23, 2006 | Led does not flicker on hb arrival			     | Mithil |
 * --------------------------------------------------------------------------------
 * |  13 |Feb 22, 2006 | Fix for neighbour line removal				     |Prachiti|
 * --------------------------------------------------------------------------------
 * |  12 |Feb 22, 2006 | Led Color Changes			 				     | Mithil |
 * --------------------------------------------------------------------------------
 * |  11 |Feb 16, 2006 | Grid Color Change			 				     | Mithil |
 * --------------------------------------------------------------------------------
 * | 10  |Jan 20, 2006 | Group select done								 | Mithil |
 *  -------------------------------------------------------------------------------
 * |  9  |Jan 04, 2006 | System.out.println()'s removed			     	 | Mithil |
 * --------------------------------------------------------------------------------
 * |  8  |Sep 25, 2005 | Changes for version 3.0.  			 			 | Abhijit|
 * --------------------------------------------------------------------------------
 * |  7  |Sep 2, 2005  | View specific control coordinates implemented.  | Abhijit|
 * --------------------------------------------------------------------------------
 * |  6  |Apr 26, 2005 | Popup menu added to MeshNetwork.                | Anand  |
 * --------------------------------------------------------------------------------
 * |  5  |Apr 20, 2005 | Line drawing logic fixed by Anand				 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  4  |Mar 25, 2005 | Images moved to branding native dll.            | Anand  |
 * --------------------------------------------------------------------------------
 * |  3  |Mar 07, 2005 | DynPropertyMaskparam added to propertyChanged() | Bindu  |
 * --------------------------------------------------------------------------------
 * |  2  |Feb 11, 2005 | Change Detection-PropertyUI changes             | Anand  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 02, 2005 | Changes for Customized MeshViewer               | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 ******************************************0****************************************/

package com.meshdynamics.nmsui.mesh;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

import com.meshdynamics.api.NMSUI.NodeText;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.mesh.IAPSearchHelper;
import com.meshdynamics.meshviewer.mesh.INetworkListener;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.MeshNetworkStatus;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;
import com.meshdynamics.meshviewer.mesh.healthmonitor.IHealthMonitor;
import com.meshdynamics.nmsui.AccessPointMenu;
import com.meshdynamics.nmsui.MeshViewerUI;
import com.meshdynamics.nmsui.PortInfoInfHelper;
import com.meshdynamics.nmsui.http.IJsonMeshInfoProvider.IJsonAp;
import com.meshdynamics.nmsui.mesh.views.AccessPointHandler;
import com.meshdynamics.nmsui.mesh.views.AccessPointUI;
import com.meshdynamics.nmsui.mesh.views.MeshNetworkView;
import com.meshdynamics.nmsui.mesh.views.defaultview.DefaultView;
import com.meshdynamics.nmsui.mesh.views.offlinemapview.OfflineMapView;
import com.meshdynamics.nmsui.mesh.views.onlinemapview.OnlineMapView;
import com.meshdynamics.util.Bytes;
import com.meshdynamics.util.MacAddress;

public class MeshNetworkUI extends Canvas implements IAPPropListener, INetworkListener, IAPSearchHelper {
    
    public static final int 	VIEW_DETAIL			= 0;
    public static final int 	VIEW_ONLINE_MAP		= 1;
    public static final int 	VIEW_OFFLINE_MAP	= 2;
    
    public static final int 	VIEW_TYPE_COUNT		= 3;
    
	private MeshNetwork				meshNetwork;
    private MeshViewerUI 			meshViewerUI;
    
    private MeshNetworkUIProperties 	properties;
    private int 					currentHealthStatus;
    private boolean					groupSelectionEnabled;
    
    private MeshNetworkView			currentView;
	private String 					statusText;
	private OnlineMapView			onlineView;
	private DefaultView				topologyView;
	private OfflineMapView			offlineView;
	
	/*used for admin password*/
	private boolean					isNetworkLocked;
	private String 					suPassword;

	private Hashtable<String, MeshNetworkView> 	views;	
	private Vector<INetworkStatusListener>		networkListeners;
    private MeshNetworkStatus					networkStatus;
    private Vector<NodeDisplayText>				nodeDisplayText;
    private Hashtable<String, AccessPoint>		accessPoints;

	public class NodeDisplayText implements NodeText {
	    
	    public NodeDisplayText(String nodeId, String textId) {
	    	this.nodeId			= nodeId;
	        this.nodeTextId 	= textId;
	        this.nodeText		= "";
	    }
	    
	    public String			nodeTextId;
	    public String			nodeText;
	    public String 			nodeId;
	    
		@Override
		public String getNodeId() {
			return nodeId;
		}
		@Override
		public String getTextId() {
			return nodeTextId;
		}
		@Override
		public void setText(String text) {
			nodeText = text;
			Enumeration<MeshNetworkView> viewEnum = views.elements();
			while(viewEnum.hasMoreElements()) {
				viewEnum.nextElement().updateNodeDisplayText(this);
			}
		}
	};
    
	/**
     * @param arg0
     * @param arg1
     */
    public MeshNetworkUI(MeshNetwork meshNetwork, Composite arg0, int arg1,MeshViewerUI meshViewerUI) {
        super(arg0, arg1);

        this.meshNetwork			= meshNetwork;
        meshNetwork.addNetworkListener(this);
        this.meshViewerUI			= meshViewerUI;
        accessPoints				= new Hashtable<String, AccessPoint>();        
        properties 					= new MeshNetworkUIProperties();
        views						= new Hashtable<String, MeshNetworkView>();
        createViews();
        statusText					= "";
        suPassword					= "";
        
        this.addDisposeListener(new DisposeListener() {
            public void widgetDisposed(DisposeEvent arg0) {
            	onClose();
            }
        });
        this.addControlListener(new ControlAdapter(){

			public void controlResized(ControlEvent arg0) {
				currentView.setBounds(1,1, getBounds().width-2, getBounds().height-2);
			}
		});
        
        currentHealthStatus			= IHealthMonitor.HEALTH_STATUS_DISABLED;
        groupSelectionEnabled		= false;
        
        Enumeration<AccessPoint> e = meshNetwork.getAccessPoints();
        while(e.hasMoreElements()) {
        	AccessPoint ap = (AccessPoint)e.nextElement();
        	ap.addPropertyListener(this);
        }
        
        networkListeners	= new Vector<INetworkStatusListener>();
        networkStatus		= new MeshNetworkStatus(meshNetwork);
        nodeDisplayText		= new Vector<NodeDisplayText>();
    }

    private void createViews() {
    	topologyView = new DefaultView(this,SWT.NONE,this);
    	views.put("Topology", topologyView);

		offlineView = new OfflineMapView(this, SWT.SIMPLE,this);
        views.put("OfflineMap", offlineView);
    	
		onlineView = new OnlineMapView(this, SWT.SIMPLE, this);
        views.put("OnlineMap", onlineView);
        
        topologyView.selectionChanged(true);
        currentView = topologyView;
        currentView.setVisible(true);
        
    }
    
    private void onClose() {
    	
    	Enumeration<MeshNetworkView> viewEnum	= views.elements();
    	while(viewEnum.hasMoreElements()) {
    		MeshNetworkView view = (MeshNetworkView)viewEnum.nextElement();
    		view.onClose();
    	}
        if(meshNetwork != null) {
        	meshNetwork.onClose();
        }
    }
    
    public void accessPointAdded(final AccessPoint accessPoint) {
    	
    	if(accessPoints.get(accessPoint.getDsMacAddress()) == null) {
    		accessPoints.put(accessPoint.getDSMacAddress(), accessPoint);
    		accessPoint.addPropertyListener(this);
    	}
    	
    	if(MeshViewerUI.display.isDisposed() == true)
    		return;
    	
    	MeshViewerUI.display.syncExec(new Runnable(){
			public void run() {
				Enumeration<MeshNetworkView> viewEnum = views.elements();
				while(viewEnum.hasMoreElements()) {
					MeshNetworkView view = (MeshNetworkView)viewEnum.nextElement();
					if(view.isDisposed() == false)
						view.accessPointAdded(accessPoint);
				}
			}
		});

    	meshViewerUI.accesspointAdded(accessPoint);
    	return;
    } 

    public void accessPointDeleted(final AccessPoint accessPoint) {
    	accessPoint.removePropertyListener(this);
    	
		MeshViewerUI.display.asyncExec(new Runnable() {
			public void run() {
				deleteAccesspoint(accessPoint.getDSMacAddress());
			}
		});
    }      
    
    public MeshNetwork getMeshNetwork() {        
        return meshNetwork;
    }
    
    public void meshViewerStopped() {
    	Enumeration<MeshNetworkView> viewEnum	= views.elements();
    	while(viewEnum.hasMoreElements()) {
    		MeshNetworkView view = (MeshNetworkView)viewEnum.nextElement();
    		view.meshViewerStopped();
    	}
    }

	public void meshViewerStarted() {
    	Enumeration<MeshNetworkView> viewEnum	= views.elements();
    	while(viewEnum.hasMoreElements()) {
    		MeshNetworkView view = (MeshNetworkView)viewEnum.nextElement();
    		view.meshViewerStarted();
    	}
	}
	
    public void load(InputStream in) {
    	byte[] fourBytes = new byte[4];
    	byte[] twoBytes = new byte[2];
    	
    	Enumeration<MeshNetworkView> viewEnum	= views.elements();
    	while(viewEnum.hasMoreElements()) {
    		MeshNetworkView view = (MeshNetworkView)viewEnum.nextElement();
    		view.load();
    	}
    	
    	try{
    		in.read(fourBytes);
    		Bytes.bytesToInt(fourBytes);	//width
    		
    		in.read(fourBytes);
    		Bytes.bytesToInt(fourBytes);	//height

    		in.read(fourBytes);
    		int viewType = Bytes.bytesToInt(fourBytes);
    		showView(viewType);

    		in.read(fourBytes);
			int len = Bytes.bytesToInt(fourBytes);
			
			if(len > 0) {
				byte[] buffer = new byte[len];				// Read su Password
				in.read(buffer);
				suPassword = new String(buffer);
			}			
    		
			in.read(twoBytes);
    		short lock = Bytes.bytesToShort(twoBytes);
    		if(lock == 0) 
    			isNetworkLocked = false;
    		else 
    			isNetworkLocked = true;
    	}catch(Exception e){
    		System.out.println(e.getMessage());
    	}
    }

    public void save(OutputStream os) {
    	
    	byte[] fourBytes = new byte[4];
    	byte[] twoBytes 	= new byte[2];
    	
    	Enumeration<MeshNetworkView> viewEnum	= views.elements();
    	while(viewEnum.hasMoreElements()) {
    		MeshNetworkView view = (MeshNetworkView)viewEnum.nextElement();
    		view.save();
    	}
    	
    	try{
    		
    		Rectangle rect  =  this.getBounds();
    		Bytes.intToBytes(rect.width,fourBytes);
        	os.write(fourBytes);
        	
        	Bytes.intToBytes(rect.height,fourBytes);
        	os.write(fourBytes);

        	Bytes.intToBytes(getCurrentViewType(),fourBytes);
        	os.write(fourBytes);
        	
	    	//currentView.save();
	    	
	    	Bytes.intToBytes(suPassword.length(),fourBytes);	/* Write superuser Password */
			os.write(fourBytes);
			os.write(suPassword.getBytes());		
	    	
			if(isNetworkLocked == true)
				Bytes.shortToBytes((short)1,twoBytes);
			else 
				Bytes.shortToBytes((short)0,twoBytes);
        	os.write(twoBytes);
    	}catch(Exception e){
    		System.out.println(e.getMessage());
    	} 	
    }
    
	/**
	 * @param viewType
	 */
	public void showView(int viewType) {

		MeshNetworkView oldView = currentView;
		
		if(viewType == VIEW_DETAIL) {
			currentView	= topologyView;
		} else if(viewType == VIEW_ONLINE_MAP) {
			currentView = onlineView;
		} else if(viewType == VIEW_OFFLINE_MAP) {
			currentView = offlineView;
		}

		if(oldView != currentView) {
			oldView.selectionChanged(false);
			oldView.setVisible(false);
			currentView.setVisible(true);
			currentView.setBounds(this.getClientArea());
			currentView.selectionChanged(true);
		}
	}
    
	public MeshNetworkUIProperties getProperties() {
		return properties;
	}
	
	public void notifyHealthAlert(final int healthStatus, final String meshId) {
		currentHealthStatus	= healthStatus;
		if(isDisposed() == true) {
			return;
		}
		
		MeshViewerUI.display.asyncExec(new Runnable() {
			public void run() {
				Enumeration<MeshNetworkView> viewEnum = views.elements();
				while(viewEnum.hasMoreElements()) {
					MeshNetworkView view = (MeshNetworkView)viewEnum.nextElement();
					view.notifyHealthAlert(healthStatus, meshId);
				}
				meshViewerUI.notifyHealthAlert(healthStatus,meshId);
			}
		});
	}
	
	public void saturationInfoReceived(final AccessPoint ap, String ifName) {
		meshViewerUI.updateDLSaturationInfo(ap, ifName);
	}
	
	public String getNwName() {
		return meshNetwork.getNwName();
	}

	public int getCurrentHealthStatus() {
		return currentHealthStatus;
	}
	
	public void maximizeAllNodes() {
		currentView.maximizeAllNodes();
	}

	public void minimizeAllNodes() {
		currentView.minimizeAllNodes();
	}
	
    public void notifyStatusBar(String statusText) {
    	this.statusText	= statusText;
    	meshViewerUI.updateStatusMessage(statusText);
    }
    
	public boolean isGroupSelectionEnabled() {
		return groupSelectionEnabled;
	}

	public boolean isGroupSelected(String macaddress) {
		return currentView.isGroupSelected(macaddress);
	}
	
	public void setGroupSelectionEnabled(boolean groupSelectionEnabled) {
		this.groupSelectionEnabled = groupSelectionEnabled;
		Enumeration<MeshNetworkView> viewEnum = views.elements();
		while(viewEnum.hasMoreElements()) {
			MeshNetworkView view = (MeshNetworkView)viewEnum.nextElement();
			view.setGroupSelectionEnabled(groupSelectionEnabled);
		}
	}
	
	public Enumeration<AccessPointHandler> getAccesspoints() {
		return currentView.getAccesspointHandlers();
	}

	public AccessPoint getSelectedAccessPoint() {
		
		String dsAddress = currentView.getSelectedApAddress();
		if(dsAddress == null) {
			return null;
		}
		 
		MacAddress.tempAddress.setBytes(dsAddress);
		return meshNetwork.getAccessPoint(MacAddress.tempAddress);
	}

	public void deleteAccesspoint(final String dsMacAddress) {

		AccessPoint ap = accessPoints.remove(dsMacAddress);
		if(ap == null)
			return;

		meshViewerUI.accesspointRemoved(ap);
		Enumeration<MeshNetworkView> viewEnum = views.elements();
		while(viewEnum.hasMoreElements()) {
			MeshNetworkView view = (MeshNetworkView)viewEnum.nextElement();
			view.deleteAccessPoint(dsMacAddress);
		}
		
	}

	/**
	 * @return Returns the statusText.
	 */
	public String getStatusText() {
		return statusText;
	}

	public String getInterfaceFromBssid(MacAddress parentBssid) {
		return meshNetwork.getInterfaceFromBssid(parentBssid);
	}

	/**
	 * @return
	 */
	public String getNetworkKey() {
		return meshNetwork.getKey();
	}

	/**
	 * @return
	 */
	public byte getNetworkType() {
		return meshNetwork.getNetworkType();
	}

	/**
	 * @return
	 */
	public boolean isMeshViewerStarted() {
		return meshViewerUI.isMeshViewerStarted();
	}

	/**
	 * @return
	 */
	public MeshViewerUI getMeshviewerUI() {
		return meshViewerUI;
	}

	/**
	 * 
	 */
	public void editProperties() {
		if(currentView.editProperties() == true) {
			meshNetwork.refreshProperties();
		}
	}

	/**
	 * @param string
	 * @return
	 */
	public boolean hasAccessPoint(String dsAddress) {
		return currentView.hasAccessPoint(dsAddress);
	}

	/**
	 * @param string
	 * @return
	 */
	public boolean isApRunning(String dsAddress) {
		return meshNetwork.isApRunning(dsAddress);
	}

	/**
	 * @return
	 */
	public int getAliveApCount() {
		return meshNetwork.getAliveApCount();
	}

	public void apSelectionChanged(MeshNetworkView view, String apDsMacAddress) {
		MacAddress.tempAddress.setBytes(apDsMacAddress);
		AccessPoint ap = meshNetwork.getAccessPoint(MacAddress.tempAddress);
		if(ap == null) {
			return;
		}
		
		Enumeration<MeshNetworkView> viewEnum = views.elements();
		while(viewEnum.hasMoreElements()) {
			MeshNetworkView dstView = viewEnum.nextElement();
			if(dstView == view)
				continue;
			
			dstView.selectAccessPoint(apDsMacAddress);
		}
		
		meshViewerUI.apSelectionChanged(ap);
	}

	public void apGroupSelectionChanged(MeshNetworkView view, String apDsMacAddress, boolean selected) {

		Enumeration<MeshNetworkView> viewEnum = views.elements();
		while(viewEnum.hasMoreElements()) {
			MeshNetworkView dstView = viewEnum.nextElement();
			if(dstView == view)
				continue;
			
			dstView.groupSelectAp(apDsMacAddress, selected);
		}
		
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IAPPropListener#apPropertyChanged(int, int, com.meshdynamics.meshviewer.mesh.AccessPoint)
	 */
	public void apPropertyChanged(final int filter, final int subFilter, final AccessPoint src) {
		
		if(MeshViewerUI.display.isDisposed() == true)
			return;
		
		MeshViewerUI.display.asyncExec (new Runnable(){
			public void run() {
				if(accessPoints.get(src.getDSMacAddress()) == null) {
					accessPointAdded(src);
				}
				
				Enumeration<MeshNetworkView> viewEnum = views.elements();
				while(viewEnum.hasMoreElements()) {
					MeshNetworkView view = (MeshNetworkView)viewEnum.nextElement();
					if(view.isDisposed()  == false) {
						view.apPropertyChanged(filter,subFilter,src);
					}
				}
			}
		});
	
		switch(filter) {
			case IAPPropListener.MASK_HEARTBEAT:
			case IAPPropListener.MASK_HEARTBEAT2:
			case IAPPropListener.MASK_AP_STATE:
				networkStatus.update();
				MeshViewerUI.display.asyncExec (new Runnable(){
					public void run() {
						for(INetworkStatusListener l : networkListeners) {
							l.networkStatusChanged(networkStatus);
						}
					}
				});
				break;
			case IAPPropListener.MASK_APCONF:
				checkInfFile(src);
				break;
		}
		
	}
	
	private void checkInfFile(AccessPoint src) {
		
		String 					hwModel		= src.getModel();
		IInterfaceConfiguration ifConfig 	= src.getConfiguration().getInterfaceConfiguration();
		String 					dsMac		= src.getDSMacAddress();
		
        PortInfoInfHelper.updateInfInformation(hwModel, dsMac, ifConfig);
	}

	public int getCurrentViewType() {
		if(currentView instanceof DefaultView) {
			return VIEW_DETAIL;
		} else if(currentView instanceof OnlineMapView) {
			return VIEW_ONLINE_MAP;
        }else if(currentView instanceof OfflineMapView) {
			return VIEW_OFFLINE_MAP;
		}
        return VIEW_DETAIL;
    }
    
    public MeshNetworkView getCurrentView() {
        return currentView;
	}
    
    public AccessPoint addAccessPoint(MacAddress dsMacAddress) {
        return meshNetwork.addAccessPoint(dsMacAddress);
    }

	/**
	 * @return the isNetworkLocked
	 */
	public boolean getIsNetworkLocked() {
		return isNetworkLocked;
	}

	/**
	 * @param isNetworkLocked the isNetworkLocked to set
	 */
	
	public void setIsNetworkLocked(boolean isNetworkLocked) {
		this.isNetworkLocked = isNetworkLocked;
	}

	/**
	 * @return the suPassword
	 */
	public String getSuPassword() {
		return suPassword;
	}

	/**
	 * @param suPassword the suPassword to set
	 */
	public void setSuPassword(String suPassword) {
		this.suPassword = suPassword;
	}
	
	public void addNetworkStatusListener(INetworkStatusListener listener) {
		
		if(networkListeners.contains(listener) == true)
			return;
		
		networkListeners.add(listener);
	}
	
	public void removeNetworkStatusListener(INetworkStatusListener listener) {
		networkListeners.remove(listener);
	}

	@Override
	public AccessPoint getAccessPointFromBssid(MacAddress bssid) {
		return meshNetwork.getAccessPointFromBssid(bssid);
	}

	@Override
	public AccessPoint[] getAccesspointsByName(String matchName, boolean partialMatch) {
		return meshNetwork.getAccesspointsByName(matchName, partialMatch);
	}

	@Override
	public AccessPoint getStaParent(MacAddress staAddress) {
		return meshNetwork.getStaParent(staAddress);
	}

	@Override
	public void selectAccesspoint(AccessPoint ap) {
		apSelectionChanged(null, ap.getDSMacAddress());
	}

	public IJsonAp getJsonAp(String macAddress) {
		return topologyView.getAccesspointHandler(macAddress);
	}

	public NodeText createNodeText(String nodeId, String nodeTextId) {
		
		NodeDisplayText t = null;
		for(NodeDisplayText it : nodeDisplayText) {
			if(it.nodeId.equalsIgnoreCase(nodeId) == true &&
					it.nodeTextId.equalsIgnoreCase(nodeTextId) == true) {
				t = it;
			}
		}
		
		if(t == null) {
			t = new NodeDisplayText(nodeId, nodeTextId);
			nodeDisplayText.add(t);
		}
		
		Enumeration<MeshNetworkView> viewEnum = views.elements();
		while(viewEnum.hasMoreElements()) {
			viewEnum.nextElement().addNodeDisplayText(t);
		}
		return t;
	}

	public void destroyNodeText(NodeText nodeText) {
		nodeDisplayText.remove(nodeText);
		
		Enumeration<MeshNetworkView> viewEnum = views.elements();
		while(viewEnum.hasMoreElements()) {
			viewEnum.nextElement().removeNodeDisplayText((NodeDisplayText) nodeText);
		}
	}

	public AccessPointMenu getAccessPointMenu() {
		return meshViewerUI.getAccessPointMenu();
	}

	public int getGroupSelectedApCount() {
		return currentView.getGroupSelectedNodes();
	}

	public AccessPointUI getAccesspointUI(String dsMacAddress) {
		return currentView.getAccesspointUI(dsMacAddress) ;
	}

	public void setNodeElementProperty(String nodeId, String property, String value) {
		Enumeration<MeshNetworkView> viewEnum = views.elements();
		while(viewEnum.hasMoreElements()) {
			viewEnum.nextElement().setNodeElementProperty(nodeId, property, value);
		}
	}

	public Enumeration<String> getGroupSelection() {
		return currentView.getGroupSelection();
	}

	public Enumeration<String> getSelectedApList() {
		return (groupSelectionEnabled == true) ? 
					currentView.getGroupSelection() : currentView.getApAddresses();
	}

}