/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ILineInfo.java
 * Comments : 
 * Created  : Oct 10, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 10, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.mesh;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;

public abstract class LineInfo {

    public static final int LINE_DS_ETH			= 1;
    public static final int LINE_DS_WIRELESS	= 2;
    public static final int LINE_KAP			= 3;    
    
    public	 	ILinePoint 	srcPoint;
    public 		ILinePoint 	dstPoint;
    public	 	int			lineType;
    

    
    /**
     * @return Returns the dstPoint.
     */
    public ILinePoint getDstPoint() {
        return dstPoint;
    }
    /**
     * @return Returns the srcPoint.
     */
    public ILinePoint getSrcPoint() {
        return srcPoint;
    }
    
    public LineInfo(ILinePoint 	srcPoint, ILinePoint 	dstPoint){    
    	this.srcPoint 	= srcPoint;
    	this.dstPoint	= dstPoint;
    }
    
    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.mesh.ui.ILineInfo#setSrc(com.meshdynamics.meshviewer.mesh.ui.ILinePoint)
     */
    public final void setSrcPoint(ILinePoint src) {
        this.srcPoint = src;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.mesh.ui.ILineInfo#setDest(com.meshdynamics.meshviewer.mesh.ui.ILinePoint)
     */
    public final void setDstPoint(ILinePoint dest) {
        this.dstPoint = dest;
    }
    
    
    public abstract void drawLine(GC gc);
    public abstract void setLineType(int lnType,Color lineColor);
    public abstract void drawCircle(GC gc);
    
    public int getLineType() {
        return lineType;
    }
    
}
