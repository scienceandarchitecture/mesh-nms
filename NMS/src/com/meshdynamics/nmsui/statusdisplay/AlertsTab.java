package com.meshdynamics.nmsui.statusdisplay;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import com.meshdynamics.api.NMSUI;
import com.meshdynamics.api.NMSUI.Alert;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabColumnInfo;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabInfo;
import com.meshdynamics.nmsui.dialogs.AlertInfoDlg;
import com.meshdynamics.nmsui.dialogs.UIConstants;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.DateTime;

public class AlertsTab extends StatusUITab {
	
	private Table 				tblAlerts;
	private Vector<AlertInfo>	alertInfos;	
	private ToolBar				toolBar;
	private ToolItem			itmShowHighAlerts;
	private ToolItem			itmShowLowAlerts;
	private ToolItem			itmShowMediumAlerts;
	private ToolItem			itmShowAllAlerts;
	private ToolItem			itmClearAlerts;
	private int 				showAlerts;
	
	private static final int SHOW_ALERTS_NONE 		= 0;
	private static final int SHOW_ALERTS_HIGH 		= 1;
	private static final int SHOW_ALERTS_MEDIUM 	= 2;
	private static final int SHOW_ALERTS_LOW 		= 4;
	private static final int SHOW_ALERTS_ALL 		= 7;
	
	private class AlertInfo {
		
		public static final int ALERT_LEVEL_LOW			= 4;
		public static final int ALERT_LEVEL_MEDIUM		= 2;
		public static final int ALERT_LEVEL_HIGH		= 1;
		
		public int 		level;
		public String 	timeStamp;
		public String 	message;
		public String 	description;
		
		AlertInfo() {
			this.level 			= ALERT_LEVEL_LOW;
			this.message		= "";
			this.description	= "";
			this.timeStamp		= DateTime.getDateTime();
		}
		
		AlertInfo(NMSUI.Alert alert) {
			switch(alert.level) {
				case Alert.ALERT_LEVEL_HIGH:
					this.level = AlertInfo.ALERT_LEVEL_HIGH;
					break;
				case Alert.ALERT_LEVEL_MEDIUM:
					this.level = AlertInfo.ALERT_LEVEL_MEDIUM;
					break;
				case Alert.ALERT_LEVEL_LOW:
					this.level = AlertInfo.ALERT_LEVEL_LOW;
					break;
				default:
					this.level = AlertInfo.ALERT_LEVEL_LOW;
			}
			
			this.message		= alert.message;
			this.description	= alert.description;
			this.timeStamp		= DateTime.getDateTime();
		}
		
	}
	
	private enum COLS {
		
		LEVEL			("", SWT.NONE, 20),
		TIME			("Timestamp", SWT.LEFT, 150),
		MESSAGE			("Message", SWT.LEFT, 250),
		DESCRIPTION		("Description", SWT.LEFT, 250);
		
		private final String 	header;
		private final int 		align;
		private int 		width;
		
		COLS(String header,int align,int width) {
			this.width 	= width;
			this.align 	= align;
			this.header	= header;
		}
		
	}
	
	AlertsTab(StatusUIContainer parent) {
		super(parent);
		alertInfos = new Vector<AlertInfo>();
		showAlerts = SHOW_ALERTS_ALL;
		loadAlerts();
	}

	private void loadAlerts() {
		
		try {
			FileInputStream in 	= new FileInputStream(MFile.getConfigPath() + "\\Alerts.txt");
			BufferedReader  br	= new BufferedReader(new InputStreamReader(in));
			
			String 		line 		= null;
			AlertInfo 	alertInfo 	= null;
			
			while((line = br.readLine()) != null) {
				
				String[] tokens = line.split("#");
				if(tokens == null) {
					if(alertInfo == null)
						continue;
					
					alertInfo.description += (line + "\n");
					continue;
				}

				int alertLevel ;
				
				if(tokens[0].equalsIgnoreCase("LOW")) {
					alertLevel = AlertInfo.ALERT_LEVEL_LOW;
				} else if(tokens[0].equalsIgnoreCase("MEDIUM")) {
					alertLevel = AlertInfo.ALERT_LEVEL_MEDIUM;
				} else if(tokens[0].equalsIgnoreCase("HIGH")) {
					alertLevel = AlertInfo.ALERT_LEVEL_HIGH;
				} else
					continue;
				
				alertInfo = new AlertInfo();
				
				alertInfo.level			= alertLevel;
				alertInfo.timeStamp		= tokens[1];

				if(tokens.length > 2)
					alertInfo.message 		= tokens[2];
				
				if(tokens.length > 3)
					alertInfo.description	= tokens[3];
				
				alertInfos.add(alertInfo);
			}
			
			br.close();
			in.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private void saveAlerts() {
		
		try {
			FileOutputStream out 	= new FileOutputStream(MFile.getConfigPath() + "\\Alerts.txt");
			PrintWriter		 p		= new PrintWriter(out);
			
			for(AlertInfo alertInfo : alertInfos) {
				
				String line = "";
				switch (alertInfo.level) {
				case AlertInfo.ALERT_LEVEL_LOW:
					line += "LOW#";
					break;
				case AlertInfo.ALERT_LEVEL_MEDIUM:
					line += "MEDIUM#";
					break;
				case AlertInfo.ALERT_LEVEL_HIGH:
					line += "HIGH#";
					break;
				}
				line += (alertInfo.timeStamp + "#");
				line += (alertInfo.message + "#");
				line += alertInfo.description;
				
				p.println(line);
			}
			
			p.close();
			out.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Override
	void createControls(CTabFolder parent) {
		super.createControls(parent);
		
		createToolbar();
		
		tblAlerts = new Table (canvas,SWT.FULL_SELECTION|SWT.MULTI);
		tblAlerts.setHeaderVisible(true);
		tblAlerts.setLinesVisible(true);
		tblAlerts.setBounds(canvas.getBounds());
		tblAlerts.addKeyListener(new KeyAdapter(){

			@Override
			public void keyPressed(KeyEvent arg0) {
				if(arg0.keyCode != SWT.DEL)
					return;
				
				clearUI();
			}
		});
		tblAlerts.addMouseListener(new MouseAdapter(){

			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
				TableItem item = tblAlerts.getItem(tblAlerts.getSelectionIndex());
				if(item == null)
					return;
				
				AlertInfo info = (AlertInfo) item.getData();
				String title = "";
				switch(info.level) {
				case AlertInfo.ALERT_LEVEL_HIGH:
					title = "High level alert.";
					break;
				case AlertInfo.ALERT_LEVEL_MEDIUM:
					title = "Medium level alert.";
					break;
				case AlertInfo.ALERT_LEVEL_LOW:
					title = "Low level alert.";
					break;
				}
				AlertInfoDlg dlg = new AlertInfoDlg(title, info.message, info.description);
				dlg.show();
			}

		});
		
		TableColumn  col;
		int			 index;
		
		for(index = 0; index < COLS.values().length; index++) {
			col  = new TableColumn(tblAlerts, COLS.values()[index].align);
			col.setText(COLS.values()[index].header);
			col.setWidth(COLS.values()[index].width);
			col.setData(COLS.values()[index]);
			
			col.addControlListener(new ControlAdapter(){

				@Override
				public void controlResized(ControlEvent arg0) {
					TableColumn col = (TableColumn)arg0.getSource();
					COLS c = (COLS)col.getData();
					c.width = col.getWidth();
				}
			});
		}
		
		ContextHelp.addContextHelpHandler(canvas, contextHelpEnum.ALERTSWINDOW);
		
		for(AlertInfo info : alertInfos) {
			displayAlert(info);
		}
	}

	private void createToolbar() {
		toolBar				= new ToolBar(canvas, SWT.FLAT);
		itmShowAllAlerts	= new ToolItem(toolBar, SWT.CHECK);
		itmShowAllAlerts.setText("Show All");
		itmShowAllAlerts.setSelection(true);
		itmShowAllAlerts.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e) {
				boolean selected = itmShowAllAlerts.getSelection(); 
				itmShowHighAlerts.setSelection(selected);
				itmShowMediumAlerts.setSelection(selected);
				itmShowLowAlerts.setSelection(selected);
				
				if(selected == true) {
					showAlerts = SHOW_ALERTS_ALL;
				} else { 
					showAlerts = SHOW_ALERTS_NONE;
				}
				
				reloadAlerts();
			}
		});
		
		itmShowHighAlerts	= new ToolItem(toolBar, SWT.CHECK);
		itmShowHighAlerts.setText("Red Alerts");
		itmShowHighAlerts.setSelection(true);
		itmShowHighAlerts.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e) {
				if(itmShowHighAlerts.getSelection() == true) {
					showAlerts |= SHOW_ALERTS_HIGH;
				} else {
					itmShowAllAlerts.setSelection(false);
					showAlerts &= ~SHOW_ALERTS_HIGH;
				}
				reloadAlerts();
			}
		});
		
		itmShowMediumAlerts	= new ToolItem(toolBar, SWT.CHECK);
		itmShowMediumAlerts.setText("Orange Alerts");
		itmShowMediumAlerts.setSelection(true);
		itmShowMediumAlerts.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e) {
				if(itmShowMediumAlerts.getSelection() == true) {
					showAlerts |= SHOW_ALERTS_MEDIUM;
				} else {
					itmShowAllAlerts.setSelection(false);
					showAlerts &= ~SHOW_ALERTS_MEDIUM;
				}
				reloadAlerts();
			}
		});

		itmShowLowAlerts	= new ToolItem(toolBar, SWT.CHECK);
		itmShowLowAlerts.setText("Yellow Alerts");
		itmShowLowAlerts.setSelection(true);
		itmShowLowAlerts.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e) {
				if(itmShowLowAlerts.getSelection() == true) {
					showAlerts |= SHOW_ALERTS_LOW;
				} else {
					itmShowAllAlerts.setSelection(false);
					showAlerts &= ~SHOW_ALERTS_LOW;
				}
				reloadAlerts();
			}
		});

		itmClearAlerts	= new ToolItem(toolBar, SWT.PUSH);
		itmClearAlerts.setText("Clear");
		itmClearAlerts.setSelection(true);
		itmClearAlerts.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e) {
				clearUI();
			}
		});
		
	}

	protected void reloadAlerts() {
		tblAlerts.clearAll();
		for(AlertInfo alert : alertInfos) {
			displayAlert(alert);
		}
	}

	private void displayAlert(AlertInfo info) {
		if(info == null)
			return;
		
		if(tblAlerts.isDisposed() == true) 
			return;
		
		if((info.level & showAlerts) == 0)
			return;
		
		TableItem item = new TableItem(tblAlerts, SWT.SIMPLE, 0);
		
		switch(info.level) {
			case AlertInfo.ALERT_LEVEL_HIGH:
				item.setBackground(0, UIConstants.RED_COLOR);
				break;
			case AlertInfo.ALERT_LEVEL_LOW:
				item.setBackground(0, UIConstants.YELLOW_COLOR);
				break;
			case AlertInfo.ALERT_LEVEL_MEDIUM:
				item.setBackground(0, UIConstants.ORANGE_COLOR);
				break;
		}
		
		item.setText(1, info.timeStamp);
		item.setText(2, info.message);
		item.setText(3, info.description);
		item.setData(info);
	}

	@Override
	void reSize(Rectangle parentBounds) {
		super.reSize(parentBounds);
		Point size 				= toolBar.computeSize (SWT.DEFAULT, SWT.DEFAULT);
		int x					= 0;
		int y					= size.y;
		int height				= parentBounds.height - size.y;
		
		toolBar.setBounds(x, 0, size.x, size.y);
		tblAlerts.setBounds(x, y, parentBounds.width, height);
	}

	void showAlert(Alert alertObj) {
		AlertInfo info = new AlertInfo(alertObj);
		alertInfos.add(info);
		displayAlert(info);
	}

	@Override
	void clearUI() {
		TableItem[] selItems = tblAlerts.getSelection();
		if(selItems == null || selItems.length <= 0) {
			MessageBox msgBox = new MessageBox(new Shell(), SWT.YES|SWT.NO|SWT.CANCEL);
			msgBox.setText("No Items selected.");
			msgBox.setMessage("Do you want to clear all alerts?");
			if(msgBox.open() != SWT.YES)
				return;
			
			selItems = tblAlerts.getItems();
		}
		
		for(TableItem item : selItems) {
			
			AlertInfo info = (AlertInfo) item.getData();
			if(info != null)
				alertInfos.remove(info);
			
			item.dispose();
		}
	}

	@Override
	void saveData() {
		saveAlerts();
	}

	@Override
	protected void onDispose() {
		saveAlerts();
	}

	@Override
	void saveUIProperties(ITabInfo tabInfo) {
		tabInfo.setColumnCount(COLS.values().length);
		
		int i = 0;
		for(COLS col : COLS.values()) {
			ITabColumnInfo columnInfo = tabInfo.getColumnInfo(i++);
			columnInfo.setColumnWidth(col.width);
		}
	}

	@Override
	void loadUIProperties(ITabInfo tabInfo) {
		for(int i=0;i<tabInfo.getColumnCount();i++) {
			ITabColumnInfo columnInfo = tabInfo.getColumnInfo(i);
			COLS col = COLS.values()[i];
			col.width = columnInfo.getColumnWidth();
			tblAlerts.getColumn(i).setWidth(col.width);
		}
	}
	
}
