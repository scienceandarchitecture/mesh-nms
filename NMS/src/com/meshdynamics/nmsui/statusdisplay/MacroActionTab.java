/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MacroActionTab.java
 * Comments : 
 * Created  : May 8, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  1  |May 10, 2007 | createControls added                            | imran  |
 * --------------------------------------------------------------------------------
 * |  0  |May 8, 2007  | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.statusdisplay;

import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.IMessageHandler.IMessageSource;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabColumnInfo;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabInfo;
import com.meshdynamics.nmsui.dialogs.UIConstants;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.util.DateTime;

public class MacroActionTab extends StatusUITab {

	private Table			tblMacroAction;
	private	Hashtable<String, Hashtable<String, ApMacroActionData>>	networkHashTable;
	private String 			currentSelectedNetwork;
	
	private static final int    MACRO_ACTIONS_COLUMN_MACADDRESS 	= 0;
	private static final int    MACRO_ACTIONS_COLUMN_TIMESTAMP		= 1;
	private static final int 	MACRO_ACTIONS_COLUMN_NODE_NAME		= 2;
	private static final int    MACRO_ACTIONS_COLUMN_MACRO_NAME		= 3;
	private static final int    MACRO_ACTIONS_COLUMN_STATUS			= 4;
	private static final int 	MACRO_ACTIONS_COLUMN_PROGRESS		= 5;
	
	private class ApMacroActionData{

		public  String 	    macAddr;
		public  String 		timeStamp;
		public  String 		nodeName;
		public  String 		macroName;
		public  String 		status;
		public  String 		progress;
		
		ApMacroActionData() {
			macAddr 	= "";
			timeStamp 	= "";
			nodeName	= "";
			macroName	= "";
			status		= "";
			progress	= "";
		}

	}
	
	public MacroActionTab(StatusUIContainer	parent) {
		super(parent);	
		networkHashTable 		= new Hashtable<String, Hashtable<String, ApMacroActionData>>();
		currentSelectedNetwork	= Mesh.DEFAULT_MESH_ID;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.ui.statusdisplay.ui.StatusPage#createControls(org.eclipse.swt.custom.CTabFolder)
	 */
	public void createControls(CTabFolder parent) {
		
		super.createControls(parent);
		
		tblMacroAction = new Table (canvas,SWT.FULL_SELECTION|SWT.MULTI);
		tblMacroAction.setHeaderVisible(true);
		tblMacroAction.setLinesVisible(true);
		
		tblMacroAction.addKeyListener(new KeyAdapter(){

			@Override
			public void keyPressed(KeyEvent keyevent) {
				if(MacroActionTab.this.parent.isGroupSelectionEnabled() == false)
					return;
				
				if(keyevent.keyCode == 'a') {
					if((keyevent.stateMask & SWT.CTRL) == 0)
						return;
					tblMacroAction.selectAll();
					for(int i=0;i<tblMacroAction.getItemCount();i++) {
						TableItem item = tblMacroAction.getItem(i);
						MacroActionTab.this.parent.notifyApGroupSelected(item.getText(), tblMacroAction.isSelected(i));
					}
					
				}

			}

		});
		
		tblMacroAction.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				if(MacroActionTab.this.parent.isGroupSelectionEnabled() == false)
					return;
				
				for(int i=0;i<tblMacroAction.getItemCount();i++) {
					TableItem item = tblMacroAction.getItem(i);
					MacroActionTab.this.parent.notifyApGroupSelected(item.getText(), tblMacroAction.isSelected(i));
				}
				
			}
				
		});
				
		String Headers[] = {"Mac Address",
							"Macro Action Time Stamp",
							"Node Name",
							"Macro Name",
							"Status",
							"Progress" };
		
		TableColumn col;
		int 		index;
		for(index = 0; index < Headers.length; index++ ) {
			col  = new TableColumn(tblMacroAction, SWT.NONE);
			col.setText(Headers[index]);
			col.setWidth(170);	
		}
		
		col =  tblMacroAction.getColumn(MACRO_ACTIONS_COLUMN_MACADDRESS);
		col.setWidth(165);
		col =  tblMacroAction.getColumn(MACRO_ACTIONS_COLUMN_TIMESTAMP);
		col.setWidth(165);
		ContextHelp.addContextHelpHandlerEx(this.canvas,contextHelpEnum.MACROSWINDOW);
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.ui.statusdisplay.ui.StatusPage#reSize(org.eclipse.swt.graphics.Rectangle)
	 */
	public void reSize(Rectangle parentBounds) {
		super.reSize(parentBounds);
		tblMacroAction.setBounds(0,0,canvas.getBounds().width,canvas.getBounds().height);
	}
	
	/**
	 * @param item
	 * @param macroData
	 */
	private void fillItemData(TableItem item, ApMacroActionData macroData) {
		
		item.setText(MACRO_ACTIONS_COLUMN_MACADDRESS,	macroData.macAddr);
		item.setText(MACRO_ACTIONS_COLUMN_TIMESTAMP,	macroData.timeStamp);
		item.setText(MACRO_ACTIONS_COLUMN_NODE_NAME,	macroData.nodeName);
		item.setText(MACRO_ACTIONS_COLUMN_MACRO_NAME,	macroData.macroName);
		item.setText(MACRO_ACTIONS_COLUMN_STATUS,		macroData.status);
		item.setText(MACRO_ACTIONS_COLUMN_PROGRESS,		macroData.progress);

		if(macroData.status.startsWith("Failed") ==  true) {
			item.setForeground(MACRO_ACTIONS_COLUMN_STATUS, UIConstants.RED_COLOR);
		}else {
			item.setForeground(MACRO_ACTIONS_COLUMN_STATUS, UIConstants.BLACK_COLOR);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.ui.statusdisplay.ui.StatusPage#clearData()
	 */
	public void clearUI() {
		for(TableItem item : tblMacroAction.getItems()) {
			item.dispose();
		}
		
		tblMacroAction.removeAll();
	}
	
	public void showMessage(IMessageSource msgSource, String messageHeader, String message, String progress) {
		
		String 									networkId 	= msgSource.getNetworkId();
		Hashtable<String, ApMacroActionData> 	maData 		= networkHashTable.get(networkId);
		
		if(maData == null) {
			maData = new Hashtable<String, ApMacroActionData>();
			networkHashTable.put(networkId, maData);
		}
		
		String 				srcMacAddress 	= msgSource.getDSMacAddress();
		ApMacroActionData 	apData 			= maData.get(srcMacAddress);
		
		if(apData == null) {
			apData 			= new ApMacroActionData();
			apData.macAddr 	= srcMacAddress;
			maData.put(srcMacAddress, apData);
		}
		
		apData.macroName 	= messageHeader;
		apData.nodeName		= msgSource.getSrcName();
		apData.progress		= progress;
		apData.status		= message;
		apData.timeStamp	= DateTime.getDateTime();
	
		if(currentSelectedNetwork.equalsIgnoreCase(networkId) == true)
			updateUI(apData);
		
	}

	private void updateUI(ApMacroActionData apData) {
		
		TableItem item 		= null;
		int itemIndex		= -1;
		TableItem items[] 	= tblMacroAction.getItems();
		
		for(int i=0;i<items.length;i++) {
			String macAddr	= items[i].getText(MACRO_ACTIONS_COLUMN_MACADDRESS).trim();
			if(apData.macAddr.equalsIgnoreCase(macAddr) == false)
				continue;
			
			item 		= items[i];
			itemIndex	= i;
			break;
		}
		
		if(item == null) {
			item 		= new TableItem(tblMacroAction, SWT.NONE);
			itemIndex 	= items.length;
		}
		
		fillItemData(item, apData);
		if(apData.macroName.equals(Mesh.MACRO_GROUP_SELECTION) == true) {
			if(apData.status.equalsIgnoreCase("Selected") == true)
				tblMacroAction.select(itemIndex);
			else
				tblMacroAction.deselect(itemIndex);
		}
			
	}

    public void networkSelected(String networkName) {
    	if(currentSelectedNetwork.equalsIgnoreCase(networkName) == true)
    		return;
    
    	currentSelectedNetwork = networkName;
		tblMacroAction.removeAll();
		
		Hashtable<String, ApMacroActionData> maData = networkHashTable.get(networkName);
		if(maData == null) {
			maData = new Hashtable<String, ApMacroActionData>();
			networkHashTable.put(networkName, maData);
		}
		
		Enumeration <ApMacroActionData> apDataEnum = maData.elements();
		while(apDataEnum.hasMoreElements()) {
		
			ApMacroActionData	apData	= apDataEnum.nextElement();
			TableItem	item	= new TableItem(tblMacroAction, SWT.NONE);
			fillItemData(item, apData);
			
		}
    }
	
    public void networkDeleted(String networkName) {
		Hashtable<String, ApMacroActionData> maData = networkHashTable.remove(networkName);
		if(maData == null)
			return;

		if(currentSelectedNetwork.equalsIgnoreCase(networkName) == false)
			return;
		
		clearUI();
    }

	@Override
	protected void onDispose() {
		// TODO Auto-generated method stub
		
	}
    
	@Override
	void saveUIProperties(ITabInfo tabInfo) {
		tabInfo.setColumnCount(tblMacroAction.getColumnCount());
		
		int i = 0;
		for(TableColumn col : tblMacroAction.getColumns()) {
			ITabColumnInfo columnInfo = tabInfo.getColumnInfo(i++);
			columnInfo.setColumnWidth(col.getWidth());
		}
	}

	@Override
	void loadUIProperties(ITabInfo tabInfo) {
		int i=0;
		for(TableColumn col : tblMacroAction.getColumns()) {
			ITabColumnInfo columnInfo = tabInfo.getColumnInfo(i++);
			int width = columnInfo.getColumnWidth();
			if(width > 0)
				col.setWidth(width);
		}
	}
	
}
