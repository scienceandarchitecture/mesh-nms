/*
 * Created on Mar 13, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.statusdisplay.helpers;

import com.meshdynamics.util.DateTime;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SipCallDialog {

	public static final int CALL_STATE_START 			= 1;
	public static final int CALL_STATE_INVITE 			= 2;
	public static final int CALL_STATE_TRYING 			= 3;	
	public static final int CALL_STATE_RINGING 			= 4;
	public static final int CALL_STATE_STATUS_OK 		= 5;
	public static final int CALL_STATE_ESTABLISHED 		= 6;
	public static final int CALL_STATE_BYE 				= 7;
	public static final int CALL_STATE_STATUS_ERROR 	= 8;
	public static final int CALL_STATE_END 				= 9;
	public static final int CALL_STATE_CANCEL 			= 10;
	
	private long		dlgId;
	private SipPhone 	caller;
	private SipPhone 	callee;
	private long		callTimeInSeconds;
	private boolean		callEstablished;
	private	String		startTimeString;
	private short		callerState;
	private short		calleeState;
		
	/**
	 * 
	 */
	public SipCallDialog(long dlgId, SipPhone caller, SipPhone callee) {
		super();
		this.dlgId				= dlgId;
		this.caller 			= caller;
		this.callee				= callee;
		this.callTimeInSeconds	= 0;
		this.callEstablished	= false;
		this.startTimeString	= DateTime.getDateTime();
		this.callerState		= CALL_STATE_START;
		this.calleeState		= CALL_STATE_START;
	}

	/**
	 * @return Returns the callee.
	 */
	public SipPhone getCallee() {
		return callee;
	}
	/**
	 * @return Returns the caller.
	 */
	public SipPhone getCaller() {
		return caller;
	}
	/**
	 * @return Returns the dlgId.
	 */
	public long getDlgId() {
		return dlgId;
	}

	/**
	 * 
	 */
	public void updateCallStatus() {
		
		if((callerState == CALL_STATE_ESTABLISHED || callerState == CALL_STATE_STATUS_OK) &&
			(calleeState == CALL_STATE_ESTABLISHED || calleeState == CALL_STATE_STATUS_OK)) {
			
			callEstablished = true;
			callTimeInSeconds++;
			return;
		}
		
		if(callerState == CALL_STATE_END && calleeState == CALL_STATE_END) {
			callEstablished = false;
		}
		
	}
	
	/**
	 * @return Returns the callTimeInSeconds.
	 */
	public long getCallTimeInSeconds() {
		return callTimeInSeconds;
	}
	/**
	 * @return Returns the callEstablished.
	 */
	public boolean isCallEstablished() {
		return callEstablished;
	}
	/**
	 * @return Returns the startTimeString.
	 */
	public String getStartTimeString() {
		return startTimeString;
	}
	/**
	 * @return Returns the calleeState.
	 */
	public short getCalleeState() {
		return calleeState;
	}
	/**
	 * @param calleeState The calleeState to set.
	 */
	public void setCalleeState(short calleeState) {
		this.calleeState = calleeState;
	}
	/**
	 * @return Returns the callerState.
	 */
	public short getCallerState() {
		return callerState;
	}
	/**
	 * @param callerState The callerState to set.
	 */
	public void setCallerState(short callerState) {
		this.callerState = callerState;
	}
	
	public boolean isDialogEnded() {
		return (callerState == CALL_STATE_END && calleeState == CALL_STATE_END);
	}
}
