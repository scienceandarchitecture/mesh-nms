/*
 * Created on Mar 13, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.statusdisplay.helpers;

import com.meshdynamics.util.DateTime;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SipPhone {
	private String 		extension;
	private MacAddress 	macAddress;
	private IpAddress	ipAddress;
	private int			rport;
	private boolean		active;
	private String		lastActivityTime;
	
	/**
	 * @param arg0
	 * @param arg1
	 */
	public SipPhone() {
		super();
		extension 			= "";
		macAddress			= new MacAddress();
		ipAddress			= new IpAddress();
		rport				= 0;
		active				= false;
		lastActivityTime	= "";
	}
	
	/**
	 * @return Returns the extension.
	 */
	public String getExtension() {
		return extension;
	}
	/**
	 * @param extension The extension to set.
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}
	/**
	 * @return Returns the macAddress.
	 */
	public MacAddress getMacAddress() {
		return macAddress;
	}
	/**
	 * @param macAddress The macAddress to set.
	 */
	public void setMacAddress(byte[] macAddressBytes) {
		this.macAddress.setBytes(macAddressBytes);
	}

	/**
	 * @return Returns the ipAddress.
	 */
	public IpAddress getIpAddress() {
		return ipAddress;
	}
	/**
	 * @param ipAddress The ipAddress to set.
	 */
	public void setIpAddress(byte[] ipAddressBytes) {
		this.ipAddress.setBytes(ipAddressBytes);
		active = true;
	}
	/**
	 * @return Returns the rport.
	 */
	public int getRport() {
		return rport;
	}
	/**
	 * @param rport The rport to set.
	 */
	public void setRport(int rport) {
		this.rport = rport;
	}
	/**
	 * @return Returns the active.
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active The active to set.
	 */
	public void setActive(boolean active) {
		this.active 			= active;
		this.lastActivityTime	= DateTime.getDateTime();
	}
	/**
	 * @return Returns the lastActivityTime.
	 */
	public String getLastActivityTime() {
		return lastActivityTime;
	}
}
