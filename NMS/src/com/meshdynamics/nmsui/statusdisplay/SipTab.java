package com.meshdynamics.nmsui.statusdisplay;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipStaInfo;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationTextReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationTextWriter;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.sip.ISipEventListener;
import com.meshdynamics.meshviewer.sip.ISipEventListener.ICallDlgStatusInfo;
import com.meshdynamics.meshviewer.sip.ISipEventListener.INewCallDlgInfo;
import com.meshdynamics.meshviewer.sip.ISipEventListener.ISipEventInfo;
import com.meshdynamics.meshviewer.sip.ISipEventListener.IStaRegistrationInfo;
import com.meshdynamics.nmsui.dialogs.SipConfigDlg;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.nmsui.statusdisplay.helpers.SipCallDialog;
import com.meshdynamics.nmsui.statusdisplay.helpers.SipPhone;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.nmsui.util.SipConfigHelper;
import com.meshdynamics.util.DateTime;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

public class SipTab extends StatusUITab {

	private static final String STR_CALL_STATE_START 				= "Idle";
	private static final String STR_CALL_STATE_TRYING				= "Trying";
	private static final String STR_CALL_STATE_RINGING				= "Ringing";
	private static final String STR_CALL_STATE_STATUS_OK			= "Ok";
	private static final String STR_CALL_STATE_ESTABLISHED 			= "In Call";
	private static final String STR_CALL_STATE_BYE					= "Hung Up";
	private static final String STR_CALL_STATE_STATUS_ERROR 		= "Error";
	private static final String STR_CALL_STATE_END 					= "Ended";
	private static final String STR_CALL_STATE_CANCEL 				= "Cancel";
	
	private static final int CALL_TABLE_COLUMN_CALLER_EXTN			= 0;
	private static final int CALL_TABLE_COLUMN_CALLER_STATE			= 1;
	private static final int CALL_TABLE_COLUMN_CALL_STATUS			= 2;
	private static final int CALL_TABLE_COLUMN_CALLEE_EXTN			= 3;
	private static final int CALL_TABLE_COLUMN_CALLEE_STATE			= 4;
	
	private static final int REGISTRY_TABLE_COLUMN_EXTN				= 0;
	private static final int REGISTRY_TABLE_COLUMN_MAC_ADDR			= 1;
	private static final int REGISTRY_TABLE_COLUMN_IP_ADDR			= 2;
	private static final int REGISTRY_TABLE_COLUMN_PORT				= 3;
	private static final int REGISTRY_TABLE_COLUMN_ACTIVE			= 4;
	
	private Table								callDlgTable;
	private Table								staTable;
	private ToolBar								toolBar;
	private ToolItem							itmSettings;

	private Hashtable<String, SipData> 			mapSipData;
	private SipData								selectedSipData;
	private Display 							display;
	
	class SipData {
		
		public Hashtable<String, SipCallDialog>		callDlgHash;
		public Hashtable<String, SipPhone>			phoneHash;
		public Thread								callTimerThread;
		public boolean								callTimerStarted;
		public String 								nwName;
		public SipConfigHelper						configHelper;
		
		SipData(String nwName) {
			this.nwName			= nwName;
			configHelper		= new SipConfigHelper();
			phoneHash			= new Hashtable<String, SipPhone>();			
			load();
			callDlgHash 		= new Hashtable<String, SipCallDialog>();
			callTimerThread		= null;
			callTimerStarted	= false;
		}
		
		void startCallTimer() {
			if(callTimerStarted == true)
				return;
			
			if(callDlgHash.size() <= 0)
				return;
			
			callTimerStarted 	= true;
			callTimerThread 	= new Thread(new Runnable(){

				@Override
				public void run() {
					while(true) {
						
						try {
							Thread.sleep(1000);
						} catch(Exception e) {
							System.out.println(nwName + " Sip call timer thread exiting after interruption.");
							callTimerStarted = false;
							return;
						}
						
						try {
							Enumeration<SipCallDialog> e = callDlgHash.elements();
							while(e.hasMoreElements()) {
								SipCallDialog callDlg = e.nextElement(); 
								callDlg.updateCallStatus();
								if(selectedSipData.nwName.equalsIgnoreCase(nwName) == true) {
									updateCallTime(callDlg);
								}
							}
						} catch(Exception e) {
							System.out.println("Error occured in call timer for " + nwName + ".Call timer stopped.");
							break;
						}
					}
					callTimerStarted	= false;
					callTimerThread		= null;
				}
				
			});
			callTimerThread.start();
			
		}
		
		void stopCallTimer() {
			if(callTimerStarted == false)
				return;
			
			if(callTimerThread == null) {
				callTimerStarted = false;
				return;
			}
				
			callTimerThread.interrupt();
		}
		
		void load() {
			try {
				FileInputStream 		inStream 		= new FileInputStream(MFile.getConfigPath() + "\\" + this.nwName + ".sip");
				ConfigurationTextReader configReader 	= new ConfigurationTextReader(inStream);
				configHelper.load(configReader);
				inStream.close();
			} catch(Exception e) {
				System.out.println(e);
			}
		
			phoneHash.clear();
			ISipConfiguration sipConfig = configHelper.getSipConfiguration();
			for(int i=0;i<sipConfig.getStaInfoCount();i++) {
				
				ISipStaInfo staInfo = sipConfig.getStaInfoByIndex(i);
				SipPhone 	phone 	= new SipPhone();
				phone.setExtension(""+staInfo.getExtension());
				
				MacAddress macAddress = staInfo.getMacAddress(); 
				phone.setMacAddress(macAddress.getBytes());
				phoneHash.put(macAddress.toString(), phone);
				
			}
			
		}
		
		void save() {
			String caption	= "Configuration saved successfully";
			String result 	= "Configuration saved to " + MFile.getConfigPath() + "\\" + this.nwName + ".sip";
			try {
				FileOutputStream 		outStream 	= new FileOutputStream(MFile.getConfigPath() + "\\" + this.nwName + ".sip");
				ConfigurationTextWriter configWriter = new ConfigurationTextWriter(outStream);
				configHelper.save(configWriter);
				configWriter.close();
				outStream.close();
			} catch(Exception e) {
				caption = "Error occured while saving configuration";
				result	= e.getMessage();
			}
			
			MessageBox msgBox = new MessageBox(new Shell(), SWT.OK);
			msgBox.setText(caption);
			msgBox.setMessage(result);
			msgBox.open();
			
		}
	}
	
	public SipTab(StatusUIContainer parent) {
		super(parent);
		display 			= Display.getDefault();
		mapSipData 			= new Hashtable<String, SipData>();
		selectedSipData		= new SipData(Mesh.DEFAULT_MESH_ID);
		mapSipData.put(Mesh.DEFAULT_MESH_ID, selectedSipData);
	}

	@Override
	public void clearUI() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void onDispose() {
		
		Enumeration<SipData> e = mapSipData.elements();
		while(e.hasMoreElements()) {
			SipData sipData = e.nextElement();
			if(sipData.callTimerStarted == true) {
				sipData.callTimerThread.interrupt();
			}
			
		}
	}

	@Override
	public void createControls(CTabFolder parent) {
		super.createControls(parent);
		
		toolBar			= new ToolBar(canvas, SWT.FLAT);
		itmSettings		= new ToolItem(toolBar, SWT.PUSH);
		itmSettings.setText("Settings");
		itmSettings.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e) {
				SipConfigDlg dlg = new SipConfigDlg(selectedSipData.configHelper);
				dlg.show();
				if(dlg.getStatus() == SWT.OK) {
					IConfiguration dlgConfig = dlg.getConfiguration();
					ISipConfiguration dlgSipConfig = dlgConfig.getSipConfiguration();
					dlgSipConfig.copyTo(selectedSipData.configHelper.getSipConfiguration());
					selectedSipData.save();
				}
			}
		});
		
		staTable 		= new Table(canvas, SWT.FULL_SELECTION|SWT.BORDER);
		staTable.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
		staTable.setHeaderVisible(true);
	
		String[] headers = {"Extn",
							"Mac Address",
							"Ip Address",
							"Rport",
							"Active"
							};
		
		TableColumn col;
		int 		index;
		for(index = 0; index < headers.length; index++) {
			col  = new TableColumn(staTable ,SWT.NONE );
			col.setText(headers[index]);
			col.setWidth(110);	
		}

		callDlgTable = new Table(canvas, SWT.FULL_SELECTION|SWT.BORDER);
		callDlgTable.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
		
		callDlgTable.setHeaderVisible(true);
		col  = new TableColumn(callDlgTable ,SWT.NONE );
		col.setText("Caller");
		col.setAlignment(SWT.CENTER);
		col.setWidth(75);
		col  = new TableColumn(callDlgTable ,SWT.NONE );
		col.setText("State");
		col.setAlignment(SWT.CENTER);
		col.setWidth(75);
		col  = new TableColumn(callDlgTable ,SWT.NONE );
		col.setText("Call Status");
		col.setAlignment(SWT.CENTER);
		col.setWidth(330);
		col  = new TableColumn(callDlgTable ,SWT.NONE );
		col.setText("Callee");
		col.setAlignment(SWT.CENTER);
		col.setWidth(75);
		col  = new TableColumn(callDlgTable ,SWT.NONE );
		col.setText("State");
		col.setAlignment(SWT.CENTER);
		col.setWidth(75);
		
		ContextHelp.addContextHelpHandler(canvas, contextHelpEnum.PBVWINDOW);
		
		reloadUI();
	}

	@Override
	public void reSize(Rectangle parentBounds) {
		super.reSize(parentBounds);
		Point size 				= toolBar.computeSize (SWT.DEFAULT, SWT.DEFAULT);
		int x					= 0;
		int y					= size.y;
		int width				= parentBounds.width/2;
		int height				= parentBounds.height - size.y;
		
		toolBar.setBounds(x, 0, size.x, size.y);
		staTable.setBounds(x, y, width, height);
		x += width;
		
		callDlgTable.setBounds(x, y, width, height);
	}

	private void updatePhoneRegistryUI(SipData sipData, SipPhone phone) {
		
		if(sipData != selectedSipData)
			return;
		
		int i;
		TableItem itm = null;
		
		for(i=0;i<staTable.getItemCount();i++) {
			
			itm 					= staTable.getItem(i);
			String strMacAddress 	= (String)itm.getData(); 
			if(phone.getMacAddress().toString().equals(strMacAddress)) {
				break;
			}
			
			itm = null;
		}
		
		if(itm == null) {
			itm = new TableItem(staTable, SWT.NONE);
			itm.setData(phone.getMacAddress().toString());
		}
		
		itm.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
		
		if(phone.isActive() == true) {
			itm.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_GREEN));
			itm.setText(REGISTRY_TABLE_COLUMN_ACTIVE, phone.getLastActivityTime());
		} else {
			itm.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_RED));
			itm.setText(REGISTRY_TABLE_COLUMN_ACTIVE, "");
		}
		
		itm.setText(REGISTRY_TABLE_COLUMN_EXTN, phone.getExtension());
		itm.setText(REGISTRY_TABLE_COLUMN_MAC_ADDR, phone.getMacAddress().toString());
		itm.setText(REGISTRY_TABLE_COLUMN_IP_ADDR, phone.getIpAddress().toString());
		itm.setText(REGISTRY_TABLE_COLUMN_PORT, ""+phone.getRport());
		
	}
	
	/**
	 * @param itm
	 * @param callerCallState
	 */
	private void setCallState(TableItem itm, boolean forCaller, short callState) {
		
		Display display 	= Display.getDefault();
		Color	backColor 	= null;
		Color	foreColor	= null;
		int		extnIndex	= CALL_TABLE_COLUMN_CALLER_EXTN;
		int 	stateIndex	= CALL_TABLE_COLUMN_CALLER_STATE;
		String	stateString = "";
		
		foreColor = display.getSystemColor(SWT.COLOR_WHITE);
		
		if(forCaller == false) {
			extnIndex 	= CALL_TABLE_COLUMN_CALLEE_EXTN;
			stateIndex 	= CALL_TABLE_COLUMN_CALLEE_STATE;
		}
		
		switch(callState) {
			case SipCallDialog.CALL_STATE_START:
				backColor	= display.getSystemColor(SWT.COLOR_GRAY);
				foreColor	= display.getSystemColor(SWT.COLOR_BLACK);
				stateString = STR_CALL_STATE_START;
				break;
			case SipCallDialog.CALL_STATE_INVITE:
				backColor	= display.getSystemColor(SWT.COLOR_YELLOW);
				foreColor 	= display.getSystemColor(SWT.COLOR_DARK_GRAY);
				if(forCaller == true)
					stateString = "Calling";
				else
					stateString = "Incoming Call";
				break;
			case SipCallDialog.CALL_STATE_TRYING:
				backColor = display.getSystemColor(SWT.COLOR_CYAN);
				foreColor = display.getSystemColor(SWT.COLOR_DARK_GRAY);
				stateString = STR_CALL_STATE_TRYING;
				break;
			case SipCallDialog.CALL_STATE_RINGING:
				backColor = display.getSystemColor(SWT.COLOR_YELLOW);
				foreColor = display.getSystemColor(SWT.COLOR_DARK_GRAY);
				stateString = STR_CALL_STATE_RINGING;
				break;
			case SipCallDialog.CALL_STATE_STATUS_OK:
				backColor 	= display.getSystemColor(SWT.COLOR_GREEN);
				if(forCaller == true)
					stateString = STR_CALL_STATE_STATUS_OK;
				else
					stateString = STR_CALL_STATE_ESTABLISHED;
				break;
			case SipCallDialog.CALL_STATE_ESTABLISHED:
				backColor 	= display.getSystemColor(SWT.COLOR_GREEN);
				stateString = STR_CALL_STATE_ESTABLISHED;
				break;
			case SipCallDialog.CALL_STATE_BYE:
				backColor 	= display.getSystemColor(SWT.COLOR_CYAN);
				foreColor	= display.getSystemColor(SWT.COLOR_BLACK);
				stateString = STR_CALL_STATE_BYE;
				break;
			case SipCallDialog.CALL_STATE_STATUS_ERROR:
				backColor 	= display.getSystemColor(SWT.COLOR_RED);
				stateString = STR_CALL_STATE_STATUS_ERROR;
				break;
			case SipCallDialog.CALL_STATE_END:
				backColor 	= display.getSystemColor(SWT.COLOR_GRAY);
				foreColor	= display.getSystemColor(SWT.COLOR_BLACK);
				stateString = STR_CALL_STATE_END;
				break;
			case SipCallDialog.CALL_STATE_CANCEL:
				backColor 	= display.getSystemColor(SWT.COLOR_BLUE);
				stateString = STR_CALL_STATE_CANCEL;
				break;
		}
	
		itm.setBackground(extnIndex, backColor);
		itm.setForeground(extnIndex, foreColor);
		itm.setBackground(stateIndex, backColor);
		itm.setForeground(stateIndex, foreColor);
		itm.setText(stateIndex, stateString);
		itm.setBackground(CALL_TABLE_COLUMN_CALL_STATUS, backColor);
		itm.setForeground(CALL_TABLE_COLUMN_CALL_STATUS, foreColor);
		
	}
	
	private void updateCallDialog(SipData sipData, SipCallDialog dialog) {
		
		if(sipData != selectedSipData)
			return;
		
		TableItem itm = null;
		for(TableItem tblItm : callDlgTable.getItems()) {
			
			String dlgId 	= (String)tblItm.getData();
			
			if(dlgId.equals(""+dialog.getDlgId()) == true) {
				itm = tblItm;
				break;
			}
		}
		
		if(itm == null) {
			itm = new TableItem(callDlgTable, SWT.NONE);
			itm.setData(""+dialog.getDlgId());
			itm.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));
		}
		
		setCallState(itm, true, dialog.getCallerState());
		itm.setText(CALL_TABLE_COLUMN_CALLER_EXTN, dialog.getCaller().getExtension());
		setCallState(itm, false, dialog.getCalleeState());
		itm.setText(CALL_TABLE_COLUMN_CALLEE_EXTN, dialog.getCallee().getExtension());
	
		if(dialog.isDialogEnded() == true)
			itm.dispose();

	}

	public void handleSipEvent(int event, ISipEventInfo eventInfo) {
		
		switch(event) {
			case ISipEventListener.SIP_EVENT_STA_REGISTRATION:
				handleSipRegistrationEvent((ISipEventListener.IStaRegistrationInfo)eventInfo);
				break;
			case ISipEventListener.SIP_EVENT_NEW_CALL_DLG:
				handleNewCallDlgEvent((ISipEventListener.INewCallDlgInfo)eventInfo);
				break;
			case ISipEventListener.SIP_EVENT_CALL_DLG_STATUS:
				handleCallStatusEvent((ISipEventListener.ICallDlgStatusInfo)eventInfo);
				break;
		}
	}

	private void handleCallStatusEvent(ICallDlgStatusInfo eventInfo) {

		SipCallDialog 	callDlg;
		SipPhone		callerPhone;
		SipPhone		calleePhone;
		long			dlgId;
		IpAddress		ipAddress;
		SipData			sipData;
		Hashtable<String, SipPhone> phoneHash;
		
		sipData 	= getSipData(eventInfo.getMeshId());
		phoneHash	= sipData.phoneHash;
		dlgId 		= eventInfo.getDlgId();
		callDlg 	= sipData.callDlgHash.get(""+dlgId);
		
		if(callDlg == null) {
			
			MacAddress phoneMacAddress = eventInfo.getCallerMacAddress();
			if(phoneMacAddress == null) {
				System.out.println("Ignoring call status " + dlgId + " with invalid caller address");
				return;
			}
			callerPhone = phoneHash.get(phoneMacAddress.toString());
			if(callerPhone == null) {
				System.out.println("Ignoring status for caller " + phoneMacAddress.toString());
				return;
			}
			phoneMacAddress = eventInfo.getCalleeMacAddress();
			if(phoneMacAddress == null) {
				System.out.println("Ignoring call status " + dlgId + " with invalid callee address");
				return;
			}
			calleePhone = phoneHash.get(phoneMacAddress.toString());
			if(calleePhone == null) {
				System.out.println("Ignoring status for callee " + phoneMacAddress.toString());
				return;
			}
			
			callDlg = new SipCallDialog(dlgId,callerPhone, calleePhone);
			sipData.callDlgHash.put(""+dlgId, callDlg);
		}
		
		callerPhone = callDlg.getCaller();
		ipAddress 	= eventInfo.getCallerIpAddress();
		if(ipAddress != null) {
			if(ipAddress.equals(IpAddress.resetAddress) == false) {
				callerPhone.setIpAddress(ipAddress.getBytes());
				callerPhone.setActive(true);
				callerPhone.setRport(eventInfo.getCallerRport());
			}
		}
		if(eventInfo.getPendingStatus() == ICallDlgStatusInfo.PENDING_STATUS_FOR_CALLEE)
			updatePhoneRegistryUI(sipData, callerPhone);
		
		calleePhone = callDlg.getCallee();
		ipAddress 	= eventInfo.getCalleeIpAddress();
		if(ipAddress != null) {
			if(ipAddress.equals(IpAddress.resetAddress) == false) {
				calleePhone.setIpAddress(ipAddress.getBytes());
				calleePhone.setActive(true);
				calleePhone.setRport(eventInfo.getCalleeRport());
			}
		}
		
		if(eventInfo.getPendingStatus() == ICallDlgStatusInfo.PENDING_STATUS_FOR_CALLER)
			updatePhoneRegistryUI(sipData, calleePhone);
		
		if(eventInfo.getPendingStatus() != ICallDlgStatusInfo.PENDING_STATUS_FOR_CLEANUP) {
			callDlg.setCallerState(eventInfo.getCallerCallState());
			callDlg.setCalleeState(eventInfo.getCalleeCallState());
			callDlg.updateCallStatus();
			updateCallDialog(sipData, callDlg);
		}
		
		if(sipData.callTimerStarted == false) {
			sipData.startCallTimer();
		}
		
	}

	private SipData getSipData(String meshId) {
		SipData sipData = mapSipData.get(meshId);
		if(sipData == null) {
			sipData = new SipData(meshId);
			mapSipData.put(meshId, sipData);
		}
			
		return sipData;
	}

	private void handleNewCallDlgEvent(INewCallDlgInfo eventInfo) {

		SipData			sipData;
		Hashtable<String, SipPhone> phoneHash;
		
		sipData 	= getSipData(eventInfo.getMeshId());
		phoneHash	= sipData.phoneHash;
		
		long			dlgId	= eventInfo.getDlgId();
		SipCallDialog 	callDlg = sipData.callDlgHash.get(""+dlgId);
		
		if(callDlg == null) {
			MacAddress phoneMacAddress = eventInfo.getCallerMacAddress();
			if(phoneMacAddress == null) {
				System.out.println("Ignoring new call dialog " + dlgId + " with invalid caller address");
				return;
			}
			SipPhone callerPhone = phoneHash.get(phoneMacAddress.toString());
			if(callerPhone == null) {
				System.out.println("Ignoring new call dialog for caller " + phoneMacAddress.toString());
				return;
			}

			phoneMacAddress = eventInfo.getCalleeMacAddress();
			if(phoneMacAddress == null) {
				System.out.println("Ignoring new call dialog " + dlgId + " with invalid callee address");
				return;
			}
			
			SipPhone calleePhone = phoneHash.get(phoneMacAddress.toString());
			if(calleePhone == null) {
				System.out.println("Ignoring new call dialog for callee " + phoneMacAddress.toString());
				return;
			}
			
			callDlg = new SipCallDialog(dlgId, callerPhone, calleePhone);
			sipData.callDlgHash.put(""+dlgId, callDlg);
			
		}
		
		updateCallDialog(sipData, callDlg);
	}

	private void handleSipRegistrationEvent(IStaRegistrationInfo eventInfo) {
		
		SipData			sipData;
		Hashtable<String, SipPhone> phoneHash;
		
		sipData 	= getSipData(eventInfo.getMeshId());
		phoneHash	= sipData.phoneHash;
		
		SipPhone phone = phoneHash.get(eventInfo.getMacAddress().toString());
		if(eventInfo.getRegistered() != 0) {
			if(phone == null) {
				phone = new SipPhone();
				phone.setExtension("Unknown");
				phone.setMacAddress(eventInfo.getMacAddress().getBytes());
				phoneHash.put(phone.getMacAddress().toString(), phone);
			}
			IpAddress ipAddress = eventInfo.getIpAddress();
			if(ipAddress != null)
				phone.setIpAddress(ipAddress.getBytes());
			
			phone.setRport(eventInfo.getRport());
			phone.setActive(true);
		} else {
			phone.setActive(false);
		}
		
		updatePhoneRegistryUI(sipData, phone);
	}
	
	private void updateCallTime(final SipCallDialog callDlg) {

		if(display == null)
			return;
		if(display.isDisposed() == true)
			return;
		
		display.syncExec(new Runnable(){
			public void run() {
				if(callDlgTable.isDisposed() == true)
					return;
				
				String srcCallId	= ""+callDlg.getDlgId();
				
				for(TableItem 	itm : callDlgTable.getItems()) {
					
					String callId = (String)itm.getData();
					 
					if(srcCallId.equalsIgnoreCase(callId) == false)
						continue;
					
					String statusString = "Start Time : " + callDlg.getStartTimeString();
					statusString += ", Call Duration : " + 
					DateTime.getTimeSpan(callDlg.getCallTimeInSeconds() * 1000);
			
					itm.setText(CALL_TABLE_COLUMN_CALL_STATUS, statusString);
					
				}
			}
		});
		
		
	}
	
	@Override
	void networkDeleted(String networkName) {
		super.networkDeleted(networkName);
		
		SipData sipData = mapSipData.remove(networkName);
		
		if(sipData.callTimerStarted == true)
			sipData.stopCallTimer();
		
		if(selectedSipData != sipData)
			return;

		selectedSipData = null;
		reloadUI();
	}

	@Override
	void networkSelected(String networkName) {
		super.networkSelected(networkName);
		if(selectedSipData != null) {
			if(selectedSipData.nwName.equalsIgnoreCase(networkName) == true)
				return;
		}
		
		selectedSipData = getSipData(networkName);
		reloadUI();
	}

	private void reloadUI() {

		staTable.clearAll();
		
		if(selectedSipData == null)
			return;
		
		Enumeration<SipPhone> phoneEnum = selectedSipData.phoneHash.elements();
		while(phoneEnum.hasMoreElements()) {
			updatePhoneRegistryUI(selectedSipData, phoneEnum.nextElement());
		}
		
		callDlgTable.clearAll();
		Enumeration<SipCallDialog> callDlgEnum = selectedSipData.callDlgHash.elements();
		while(callDlgEnum.hasMoreElements()) {
			updateCallDialog(selectedSipData, callDlgEnum.nextElement());
		}
		
	}
	
}
