/*
 * Created on Nov 7, 2007
 *
 */
package com.meshdynamics.nmsui.statusdisplay;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.api.NMSUI.StatusTab;
import com.meshdynamics.api.NMSUI.StatusTabColumn;
import com.meshdynamics.api.NMSUI.StatusTabHeader;
import com.meshdynamics.api.NMSUI.StatusTabRow;
import com.meshdynamics.nmsui.MeshViewerUI;

/**
 * @author prachiti
 *
 */

public class ExtStatusTab extends StatusUITab implements StatusTab {
    
    private Table				tblExtension;
    private StatusTabHeader		header;
    private String 				tabName;
    
    private class ExtTabRow implements StatusTabRow {
    	
    	ExtStatusTab	parent;
    	TableItem 		item;
    	Vector<String>	values;
    	
    	ExtTabRow(ExtStatusTab parent, int columnCount) {
    		this.parent = parent;
    		values = new Vector<String>();
    		for(int i=0;i<columnCount;i++)
    			values.add("");
    	}
    	
		@Override
		public void setValueAt(int index, String value) {
			values.set(index, value);
		}

		@Override
		public StatusTab getStatusTab() {
			return parent;
		}
    	
    }
    
    public ExtStatusTab(StatusUIContainer parent, String tabName, StatusTabHeader header) {
        super(parent);
        this.header 	= header;
        this.tabName 	= tabName;
    }
    
    public void createControls(CTabFolder parent) {
		
		super.createControls(parent);
		
		tblExtension = new Table (canvas,SWT.FULL_SELECTION|SWT.MULTI);
		tblExtension.setHeaderVisible(true);
		tblExtension.setLinesVisible(true);
		tblExtension.setBounds(canvas.getBounds());
	    for(int i=0;i<header.getColumnCount();i++) {
	        /*todo alignment*/
	        TableColumn col  = new TableColumn(tblExtension,SWT.NONE);
	        StatusTabColumn colInfo = header.getColumnAt(i);
	    	col.setText(colInfo.name);
	    	col.setWidth(colInfo.width);
	    	switch(colInfo.align) {
		    	case StatusTabColumn.COLUMN_ALIGN_LEFT:
		    		col.setAlignment(SWT.LEFT);
		    		break;
		    	case StatusTabColumn.COLUMN_ALIGN_RIGHT:
		    		col.setAlignment(SWT.RIGHT);
		    		break;
		    	default:
		    		col.setAlignment(SWT.LEFT);
		    		break;
	    	}
	    }

    }
        
    public StatusTabRow addStatusTabRow() {
        
        final ExtTabRow 	row = new ExtTabRow(this, header.getColumnCount());
        
		MeshViewerUI.display.syncExec(new Runnable(){
       		public void run() {
       	        row.item 		= new TableItem(tblExtension, SWT.NONE);
       		}
		});
        return row;
    }

    public void removeStatusTabRow(StatusTabRow statusTabRow) {
        
    	if(!(statusTabRow instanceof ExtTabRow))
    		return;
    	
    	ExtTabRow row = (ExtTabRow) statusTabRow;
    	
        for(TableItem item : tblExtension.getItems()) {
            if(item == row.item) {
            	item.dispose();
            	row.item = null;
            }
        }
    }

    public void updateStatusTabRow(StatusTabRow statusTabRow) {
        
    	if(!(statusTabRow instanceof ExtTabRow))
    		return;
    	
    	final ExtTabRow row = (ExtTabRow) statusTabRow;

		MeshViewerUI.display.asyncExec(new Runnable(){
       		public void run() {
       	        if(row.item == null)
       	        	return;
       	        if(row.item.isDisposed())
       	        	return;
       	
       	        String itemText;
       	        
       	        for(int i=0;i<row.values.size();i++) {
       	            if((itemText = row.values.get(i)) != null) {
       	                row.item.setText(i,itemText);
       	            }
       	        }
       		}
		});
    	
    }
    
    public void reSize(Rectangle parentBounds) {
		super.reSize(parentBounds);
		tblExtension.setBounds(0,0,canvas.getBounds().width,canvas.getBounds().height);
	}
    
	@Override
	public void clearUI() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void onDispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public StatusTabHeader getHeader() {
		return header;
	}

	@Override
	public String getName() {
		return tabName;
	}
}
