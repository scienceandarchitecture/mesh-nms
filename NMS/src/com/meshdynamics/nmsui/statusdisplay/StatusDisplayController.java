/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : StatusDisplayController.java
 * Comments : 
 * Created  : May 8, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  1  |Aug 1, 2007  | INotifier implemented                           | Imran  |
 * --------------------------------------------------------------------------------
 * |  0  |May 8, 2007  | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.statusdisplay;

import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Table;

import com.meshdynamics.api.NMSUI;
import com.meshdynamics.api.NMSUI.StatusTab;
import com.meshdynamics.api.NMSUI.StatusTabHeader;
import com.meshdynamics.api.NMSUI.StatusTabRow;
import com.meshdynamics.meshviewer.mesh.IMeshNetworkStatus;
import com.meshdynamics.meshviewer.mesh.IMessageHandler;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;
import com.meshdynamics.meshviewer.sip.ISipEventListener;
import com.meshdynamics.nmsui.MeshViewerUI;
import com.meshdynamics.nmsui.PerspectiveSettings.ITableInfo;
import com.meshdynamics.nmsui.mesh.INetworkStatusListener;
import com.meshdynamics.util.MacAddress;

public class StatusDisplayController implements IAPPropListener, IMessageHandler, INetworkStatusListener, ISipEventListener  {

	private MeshViewerUI		meshviewerUI;
	private StatusUIContainer   uiContainer;
	
    public static class RetObject {
    	public Object obj;
    }
	
	public StatusDisplayController(Canvas logCanvas, MeshViewerUI viewerUI) {
		this.meshviewerUI	= viewerUI;
		uiContainer			= new StatusUIContainer(this, logCanvas, viewerUI);
	}

	public void apPropertyChanged(final int filter, final int subFilter, final AccessPoint src) {
		
		if(MeshViewerUI.display.isDisposed() == true)
			return;
		
		MeshViewerUI.display.asyncExec(new Runnable(){
			public void run() {
				uiContainer.apPropertyChanged(filter, subFilter, src);
			}
		});
	}

	public void showMessage(final IMessageSource msgSource, final String messageHeader, final String message, final String progress) {
		if(MeshViewerUI.display.isDisposed() == true)
			return;
		
		MeshViewerUI.display.asyncExec(new Runnable(){
			public void run() {
				uiContainer.showMessage(msgSource, messageHeader,message, progress);
			}
		});
	}

	public void networkCreated(String networkName) {
		networkSelected(networkName);
	}

	public void networkDeleted(String networkName) {
		uiContainer.networkDeleted(networkName);
	}

	public void networkSelected(String networkName) {
		uiContainer.networkSelected(networkName);
	}

	public void groupSelectionChanged(boolean groupSelectionEnabled) {
		uiContainer.groupSelectionChanged(groupSelectionEnabled);
	}

	void notifyApDoubleClicked(String strMac) {
		meshviewerUI.apDoubleClickedFromStatusDisplay(strMac);
	}

	void notifyApGroupSelected(String strMacAddress, boolean selected) {
		meshviewerUI.apGroupSelectedFromStatusDisplay(strMacAddress, selected);
	}
	
	void notifyApRightClicked(Table tblHeartBeat, MacAddress mac) {
		meshviewerUI.apRightClicked(tblHeartBeat, mac);
	}
	
	public StatusTab createStatusTab(String tabName,StatusTabHeader columns) {
		return uiContainer.createStatusTab(tabName, columns);
	}

	/**
	 * @param statusTab
	 */
	public void destroySTatusTab(StatusTab statusTab) {
		uiContainer.destroySTatusTab(statusTab);
	}
	
	public StatusTabRow addStatusTabRow(StatusTab statusTab) {
		return uiContainer.addStatusTabRow(statusTab);
	}
	
	public void removeStatusTabRow(StatusTabRow statusTabRow) {
		uiContainer.removeStatusTabRow(statusTabRow);
	}
	
	public void  updateStatusTabRow(StatusTabRow statusTabRow) {
		uiContainer.updateStatusTabRow(statusTabRow);
	}

	public void selectAccessPoint(final String strMacAddress) {
		MeshViewerUI.display.syncExec(new Runnable(){
       		public void run() {
       			uiContainer.selectAccessPoint(strMacAddress);
       		}
		});
		
	}
	
	public void networkStatusChanged(final IMeshNetworkStatus status) {
		if(MeshViewerUI.display.isDisposed() == true)
			return;
		
		MeshViewerUI.display.asyncExec(new Runnable(){
			public void run() {
				uiContainer.networkStatusChanged(status);
			}
		});
	}

	public void showAlert(final NMSUI.Alert alertObj) {
	 
		if(MeshViewerUI.display.isDisposed() == true)
			return;
		
		MeshViewerUI.display.asyncExec(new Runnable(){
			public void run() {
				uiContainer.showAlert(alertObj);
			}
		});
	}

	public void clientActivityChanged(final String name, final boolean enable) {
		if(MeshViewerUI.display.isDisposed() == true)
			return;
		
		MeshViewerUI.display.asyncExec(new Runnable(){
			public void run() {
				uiContainer.clientActivityChanged(name, enable);
			}
		});
	}

	@Override
	public void sipEvent(final int event, final ISipEventInfo eventInfo) {
		if(MeshViewerUI.display.isDisposed() == true)
			return;
		
		MeshViewerUI.display.asyncExec(new Runnable(){
			public void run() {
				uiContainer.handleSipEvent(event, eventInfo);
			}
		});
	}

	public void saveUIProperties(ITableInfo  tableInfo) {
		
		uiContainer.saveUIProperties(tableInfo);
		uiContainer.saveData();
	}

	public void loadUIProperties(ITableInfo tableInfo) {
		uiContainer.loadUIProperties(tableInfo);
	}

    public void toggleStatusControlParent(boolean bNewWindow) {
        if(null != uiContainer) {
            this.uiContainer.toggleStatusControlParent(bNewWindow);
        }
    }
}
