package com.meshdynamics.nmsui.statusdisplay;

import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.mesh.IMeshNetworkStatus;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabColumnInfo;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabInfo;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.util.DateTime;

public class NetworkStatusTab extends StatusUITab {
	
	private	Table								tblNetworks;
	private Hashtable<String, NetworkStatus>	hashNetworkData;
	
	public class NetworkStatus {
		
		public boolean dataChanged;
		
		public String		networkName;
		public int 			apCount;
		public int 			aliveApCount;
		public long			minSignal;
		public long			maxSignal;
		public long			minBitRate;
		public long 		maxBitRate;
		public long			upTime;
		public TableItem	tableItem;
		
		public NetworkStatus(String networkName) {
			this.networkName = networkName;
		}

		public void clearData() {
			apCount 		= 0;
			aliveApCount 	= 0;
			minSignal 		= 0;
			maxSignal 		= 0;
			minBitRate	 	= 0;
			maxBitRate 		= 0;
			upTime 			= 0;
			networkName		= "";
		}
	}
	
	enum COLS {
		
		NW_NAME			("Network Name",SWT.LEFT,150),
		UPTIME			("Up Time",SWT.LEFT,150),
		AP_COUNT		("Active Nodes",SWT.RIGHT,80),
		SIGNAL			("Signal(Min dBm/Max dBm)",SWT.RIGHT,150),
		BITRATE			("Bit Rate(Min Mbps/Max Mbps)",SWT.RIGHT,160);
		
		private final String 	header;
		private final int 		align;
		private int 			width;
		
		COLS(String header,int align,int width) {
			this.width 	= width;
			this.align 	= align;
			this.header	= header;
		}
		
	}
	
	public NetworkStatusTab(StatusUIContainer parent) {
		super(parent);
		hashNetworkData = new Hashtable<String, NetworkStatus>();
	}

	private void updateStatus(NetworkStatus status) {
		
		if(status.tableItem == null)
			status.tableItem = new TableItem(tblNetworks, SWT.SIMPLE);

		TableItem item = status.tableItem;
		
		if(item.isDisposed() == true)
			return;
		
		item.setText(status.networkName);

		if(status.upTime <= 0 || status.aliveApCount <= 0) {
			item.setText(COLS.UPTIME.ordinal(), "");
			item.setText(COLS.AP_COUNT.ordinal(), "");
			item.setText(COLS.SIGNAL.ordinal(), "");
			item.setText(COLS.BITRATE.ordinal(), "");
			return;
		}

		item.setText(COLS.UPTIME.ordinal(), DateTime.getTimeSpan(status.upTime*1000));
		item.setText(COLS.AP_COUNT.ordinal(), ""+status.aliveApCount);
		item.setText(COLS.SIGNAL.ordinal(), status.minSignal+" dBm/"+status.maxSignal + " dBm");
		item.setText(COLS.BITRATE.ordinal(), status.minBitRate+" Mbps/" +status.maxBitRate + " Mbps");
		
	}
	
	@Override
	public void createControls(CTabFolder parent) {
		super.createControls(parent);
		tblNetworks = new Table (canvas,SWT.FULL_SELECTION|SWT.MULTI);
		tblNetworks.setHeaderVisible(true);
		tblNetworks.setLinesVisible(true);
		tblNetworks.setBounds(canvas.getBounds());
		
		TableColumn  col;
		int			 index;
		
		for(index = 0; index < COLS.values().length; index++) {
			col  = new TableColumn(tblNetworks, COLS.values()[index].align);
			col.setText(COLS.values()[index].header);
			col.setWidth(COLS.values()[index].width);
		}
		
		ContextHelp.addContextHelpHandler(canvas, contextHelpEnum.ALERTSWINDOW);
	}

	@Override
	public void reSize(Rectangle parentBounds) {
		super.reSize(parentBounds);
		tblNetworks.setBounds(0,0,canvas.getBounds().width,canvas.getBounds().height);
	}

	@Override
	public void clearUI() {
		// TODO Auto-generated method stub
		
	}

	public void updateNetworkStatus(IMeshNetworkStatus status) {

		String 			networkName = status.getNetworkName();
		NetworkStatus 	statusData 	= hashNetworkData.get(networkName);
		if(statusData == null) {
			statusData = new NetworkStatus(networkName);
			hashNetworkData.put(networkName, statusData);
		}
		
		statusData.aliveApCount	= status.getActiveApCount();
		statusData.apCount		= status.getTotalApCount();
		statusData.maxBitRate	= status.getMaxBitRate();
		statusData.maxSignal	= status.getMaxSignal();
		statusData.minBitRate	= status.getMinBitRate();
		statusData.minSignal	= status.getMinSignal();
		statusData.upTime 		= status.getUpTime();
		
		updateStatus(statusData);
	}

	public void networkDeleted(String networkName) {
		NetworkStatus 	statusData 	= hashNetworkData.remove(networkName);
		if(statusData == null)
			return;
		
		if(statusData.tableItem != null && statusData.tableItem.isDisposed() == false)
			statusData.tableItem.dispose();
	}
	
    public void networkSelected(String networkName) {

		NetworkStatus 	statusData 	= hashNetworkData.get(networkName);
		if(statusData == null) {
			statusData = new NetworkStatus(networkName);
			hashNetworkData.put(networkName, statusData);
		}
		
		updateStatus(statusData);
    }

	@Override
	protected void onDispose() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	void saveUIProperties(ITabInfo tabInfo) {
		tabInfo.setColumnCount(COLS.values().length);
		
		int i = 0;
		for(COLS col : COLS.values()) {
			ITabColumnInfo columnInfo = tabInfo.getColumnInfo(i++);
			columnInfo.setColumnWidth(col.width);
		}
	}

	@Override
	void loadUIProperties(ITabInfo tabInfo) {
		for(int i=0;i<tabInfo.getColumnCount();i++) {
			ITabColumnInfo columnInfo = tabInfo.getColumnInfo(i);
			COLS col = COLS.values()[i];
			col.width = columnInfo.getColumnWidth();
			tblNetworks.getColumn(i).setWidth(col.width);
		}
	}

}
