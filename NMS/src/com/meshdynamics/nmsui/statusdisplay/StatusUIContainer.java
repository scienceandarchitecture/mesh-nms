/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : StatusUIContainer.java
 * Comments : 
 * Created  : May 8, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  2  |Jun 04, 2007  | resize function modified for toolbar bounds    | Imran  |
 * --------------------------------------------------------------------------------
 * |  1  |May 10, 2007  | createControls, initPages, resize method added | Imran  |
 * --------------------------------------------------------------------------------
 * |  0  |May 8, 2007  | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.statusdisplay;

import java.util.Vector;

import com.meshdynamics.nmsui.MeshViewerUI;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.*;

import com.meshdynamics.api.NMSUI.Alert;
import com.meshdynamics.api.NMSUI.StatusTab;
import com.meshdynamics.api.NMSUI.StatusTabHeader;
import com.meshdynamics.api.NMSUI.StatusTabRow;
import com.meshdynamics.meshviewer.mesh.IMeshNetworkStatus;
import com.meshdynamics.meshviewer.mesh.IMessageHandler.IMessageSource;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;
import com.meshdynamics.meshviewer.sip.ISipEventListener.ISipEventInfo;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabInfo;
import com.meshdynamics.nmsui.PerspectiveSettings.ITableInfo;
import com.meshdynamics.util.MacAddress;


public class StatusUIContainer {
	
	private 	CTabFolder				tabFolder;
	private 	Vector<StatusUITab>		pages;
	private 	StatusDisplayController controller;
	private 	AlertsTab 				alertsTab;
	private 	NetworkStatusTab 		nwTab;
	private 	HeartbeatTab 			hbTab;
	private 	MacroActionTab 			macroTab;
	private 	ClientActivityTab 		clientTab;
	private 	SipTab					sipTab;
	private 	boolean 				groupSelectionEnabled;
    private     MeshViewerUI            meshviewerUI;
    private static Image outImage = new Image(null, MeshViewerUI.class.getResourceAsStream("icons/OpenInNewWindow.png"));
    private static Image inImage = new Image(null, MeshViewerUI.class.getResourceAsStream("icons/OpenInExistingWindow.png"));
	
	private static final int DEFAULT_TAB_COUNT = 6;
    public static final int	 TOOLBAR_ITEM_TOGGLE_PARENT = 1;
    public ToolItem toggleParentToolItem;
	
	public StatusUIContainer(StatusDisplayController controller, Canvas logCanvas, MeshViewerUI meshViewerUI) {
        this.controller 			= controller;
        this.meshviewerUI           = meshViewerUI;
		this.groupSelectionEnabled	= false;
		initPages();
		
		createControls(logCanvas);
	}
	
	private void initPages() {
		
		pages						= new Vector<StatusUITab>();
		
		alertsTab					= new AlertsTab(this);
		alertsTab.setCaption("Alerts");
		pages.add(alertsTab);

		nwTab						= new NetworkStatusTab(this);
		nwTab.setCaption("Networks");
		pages.add(nwTab);
		
		hbTab						= new HeartbeatTab(this);
		hbTab.setCaption("HeartBeat");
		pages.add(hbTab);
		
		macroTab					= new MacroActionTab(this);
		pages.add(macroTab);
		macroTab.setCaption("Macro Actions");
		
		clientTab					= new ClientActivityTab(this);
		pages.add(clientTab);
		clientTab.setCaption("Client Activity");
		
		sipTab						= new SipTab(this);
		pages.add(sipTab);
		sipTab.setCaption("PBV(tm)");
	
	}
	
	protected void createControls(Canvas mainCnv) {
	
		mainCnv.addControlListener(new ControlAdapter(){

			public void controlResized(ControlEvent arg0) {				
				resize();
			}	
		});
		
		tabFolder = new CTabFolder(mainCnv,SWT.BORDER);

        //Add tool bar
        ToolBar toolBar = new ToolBar( tabFolder, SWT.FLAT );
        toggleParentToolItem = new ToolItem( toolBar, SWT.PUSH );
        toggleParentToolItem.setImage( outImage );
        toggleParentToolItem.setData(new Integer(TOOLBAR_ITEM_TOGGLE_PARENT));

        Listener toolbarListener = new Listener() {
            public void handleEvent(Event event) {
                ToolItem toolItem = (ToolItem) event.widget;
                int s =  Integer.parseInt(toolItem.getData().toString());
                switch(s){

                    case TOOLBAR_ITEM_TOGGLE_PARENT :
                        meshviewerUI.toggleStatusLogParent();
                        break;
                }
            }
        };
        toggleParentToolItem.addListener(SWT.Selection, toolbarListener);

        tabFolder.setTopRight(toolBar, SWT.RIGHT);
		tabFolder.setTabHeight(24);
		tabFolder.setSimple(false);
		tabFolder.setMinimumCharacters(20);
		tabFolder.setSelectionForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		Display display = Display.getCurrent();
		tabFolder.setSelectionBackground(new Color[]{display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND), 
                					  display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND),
									  display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT), 
									  display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT)},
									  new int[] {40, 70, 100}, true);
		
		for(StatusUITab tab : pages) {
	        tab.createControls(tabFolder);
		}
		tabFolder.setSelection(alertsTab.tabItem);
		resize();
	}

	private void resize() {
		 
		Rectangle parentBounds 	= tabFolder.getParent().getBounds();
		tabFolder.setBounds(3,1,parentBounds.width-6,parentBounds.height-1);
			
		Rectangle	tabFolderBounds	= tabFolder.getClientArea();
		for(StatusUITab tab : pages) {
	        tab.reSize(tabFolderBounds);
		}

	}

	public void apPropertyChanged(int filter, int subFilter, AccessPoint src) {
		
		switch(filter) {
			case IAPPropListener.MASK_HEARTBEAT:
			case IAPPropListener.MASK_HEARTBEAT2:
			case IAPPropListener.MASK_AP_STA_CONF:
				clientTab.apPropertyChanged(filter, subFilter, src);
			case IAPPropListener.MASK_AP_STATE:
			case IAPPropListener.MASK_IPCONF:
			case IAPPropListener.MASK_APCONF:				
				hbTab.apPropertyChanged(filter, subFilter, src);
                clientTab.apPropertyChanged(filter, subFilter, src);
				break;
		}	
			
	}

	public void showMessage(IMessageSource msgSource, String messageHeader, String message, String progress) {
		tabFolder.setSelection(macroTab.tabItem);
		macroTab.showMessage(msgSource, messageHeader, message, progress);
	}

	public void networkDeleted(String networkName) {
		nwTab.networkDeleted(networkName);
		hbTab.networkDeleted(networkName);
		macroTab.networkDeleted(networkName);
		clientTab.networkDeleted(networkName);
		sipTab.networkDeleted(networkName);
	}

	public void networkSelected(String networkName) {
		nwTab.networkSelected(networkName);
		hbTab.networkSelected(networkName);
		macroTab.networkSelected(networkName);
		clientTab.networkSelected(networkName);
		sipTab.networkSelected(networkName);
	}

	public void groupSelectionChanged(boolean groupSelectionEnabled) {
		this.groupSelectionEnabled = groupSelectionEnabled;
		if(groupSelectionEnabled == false)
			macroTab.clearUI();
	}

	public StatusTab createStatusTab(String tabName, StatusTabHeader columns) {
		
	    ExtStatusTab 	extTab 		= new ExtStatusTab(this, tabName, columns);
	    pages.add(extTab);
	    /*verify caption against all others*/
	    extTab.setCaption(tabName);
	    extTab.createControls(tabFolder);

	    return extTab;
	}

	public void destroySTatusTab(StatusTab statusTab) {
		ExtStatusTab 	extTab = (ExtStatusTab)statusTab;
		pages.remove(extTab);
		extTab.dispose();
	}

	public StatusTabRow addStatusTabRow(StatusTab statusTab) {
		final ExtStatusTab 	extTab = (ExtStatusTab)statusTab;
	    
		return extTab.addStatusTabRow();
	}

	public void removeStatusTabRow(StatusTabRow statusTabRow) {
		ExtStatusTab 	extTab = (ExtStatusTab)statusTabRow.getStatusTab();
	    /*todo update data*/
	    extTab.removeStatusTabRow(statusTabRow);
	}

	public void updateStatusTabRow(StatusTabRow statusTabRow) {
		ExtStatusTab 	extTab = (ExtStatusTab)statusTabRow.getStatusTab();
		extTab.updateStatusTabRow(statusTabRow);
	    /*todo update data*/
	}

	public void networkStatusChanged(IMeshNetworkStatus status) {
		nwTab.updateNetworkStatus(status);
	}

	public void showAlert(Alert alertObj) {
		alertsTab.showAlert(alertObj);
	}

	public void selectAccessPoint(String strMacAddress) {
		hbTab.selectEntry(strMacAddress);
	}

	void clientActivityChanged(String name, boolean enable) {
		clientTab.clientActivityChanged(name, enable);
	}
	
	void notifyApDoubleClicked(String strMacAddress) {
		controller.notifyApDoubleClicked(strMacAddress);
	}

	void notifyApGroupSelected(String strMacAddress, boolean selected) {
		controller.notifyApGroupSelected(strMacAddress, selected);
	}
	
	void notifyApRightClicked(Table tblHeartBeat, MacAddress mac) {
		controller.notifyApRightClicked(tblHeartBeat, mac);
	}

	void handleSipEvent(int event, ISipEventInfo eventInfo) {
		sipTab.handleSipEvent(event, eventInfo);
	}

	void saveData() {
		for(StatusUITab tab : pages) {
			tab.saveData();
		}
	}

	void saveUIProperties(ITableInfo  tableInfo) {

		tableInfo.setTabCount(DEFAULT_TAB_COUNT);
		
		int i=0;
		for(StatusUITab tab : pages) {
			if(tab instanceof ExtStatusTab)
				continue;

			ITabInfo tabInfo = tableInfo.getTabInfo(i++);
			tab.saveUIProperties(tabInfo);
		}
	}

	void loadUIProperties(ITableInfo tableInfo) {
		
		for(int i=0;i<tableInfo.getTabCount();i++) {
			ITabInfo tabInfo = tableInfo.getTabInfo(i);
			StatusUITab tab = pages.get(i);
			if(tab != null)
				tab.loadUIProperties(tabInfo);
		}
	}

	boolean isGroupSelectionEnabled() {
		return groupSelectionEnabled;
	}

    protected void toggleStatusControlParent(boolean bNewWindow) {
        if(bNewWindow) {
            toggleParentToolItem.setImage( inImage );
        } else{
            toggleParentToolItem.setImage( outImage );
        }
    }

}
