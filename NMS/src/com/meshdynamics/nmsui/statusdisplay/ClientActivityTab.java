/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ClientActivityTab.java
 * Comments : 
 * Created  : May 8, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  2  |Feb 23, 2009 | Fix for single radio configuration				 | Abhijit|
 * --------------------------------------------------------------------------------
 * |  1  |May 10, 2007 | createControls added                            | imran  |
 * --------------------------------------------------------------------------------
 * |  0  |May 8, 2007  | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.statusdisplay;

import java.util.Enumeration;
import java.util.Hashtable;

import com.meshdynamics.util.IpAddress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.configuration.IStaConfiguration;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabColumnInfo;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabInfo;
import com.meshdynamics.nmsui.dialogs.MeshviewerStringConstants;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.util.DateTime;
import com.meshdynamics.util.MacAddress;

public class ClientActivityTab extends StatusUITab {

	private Tree							staTableTree;
	private	Hashtable<String, NetworkInfo>	hashNetworks;
	private String 							currentSelectedNetwork;

	private class StaInfo {
		
		public boolean	exInfoAvailable;
		public String 	staAddress;
		public long		upStreamRate;
		public long		signal;
		public String	essid;
		public String 	bssid;
		public long 	activityTime;
		public long		bytesRecvd;
		public long 	bytesSent;
		public short	isImcp;
		public String   ipAddress;

		StaInfo(String staAddress) {
			this.exInfoAvailable 	= false;
			this.staAddress 		= staAddress;
			this.bytesRecvd			= -1;
			this.bytesSent			= -1;
		}
	}
	
	private class ApInfo {
        public IpAddress                    ipAddress;
		public String 						macAddress;
		public boolean						clientActivityEnabled;
		public Hashtable<String, StaInfo>	staTable;

		ApInfo(String macAddress) {
			this.macAddress 			= macAddress;
			this.staTable				= new Hashtable<String, StaInfo>();
			this.clientActivityEnabled	= false;
		}

		public StaInfo getStaInfo(String strStaAddress, boolean createNew) {
			StaInfo staInfo = staTable.get(strStaAddress);
			if(staInfo == null && createNew == true) {
				staInfo = new StaInfo(strStaAddress);
				staTable.put(strStaAddress, staInfo);
			}
			return staInfo;
		}
	}
	
	private class NetworkInfo {
		public String 						networkName;
		public boolean						showClientDetails;
		public Hashtable<String, ApInfo>	apTable;
		
		NetworkInfo(String networkName) {
			this.networkName 	= networkName;
			showClientDetails	= false;	
			apTable 			= new Hashtable<String, ApInfo>();
		}

		public ApInfo getApInfo(String apMacAddress, boolean createNew ) {
			ApInfo info = apTable.get(apMacAddress);
			if(info == null && createNew == true) {
				info = new ApInfo(apMacAddress);
				apTable.put(apMacAddress, info);
			}
			return info;
		}
	}
	
	enum COLS {
		
		MACADDRESS		("MacAddress",SWT.NONE,150,true),
        IPADDRESS		("IpAddress",SWT.NONE,150,true),
		UPSTREAM_RATE	("Upstream Rate",SWT.RIGHT,125,true),
		SIGNAL			("Signal",SWT.RIGHT,125,true),
		ESSID			("Essid",SWT.LEFT,125,false),
		BSSID			("Bssid",SWT.LEFT,125,false),
		ACTIVITY_TIME	("Activity Time",SWT.LEFT,110,false),
		BYTES_RECEIVED	("Bytes Received",SWT.RIGHT,125,false),
		BYTES_SENT		("Bytes Sent",SWT.RIGHT,125,false);
		
		
		private final String 	header;
		private final int 		align;
		private int 			width;
		private final boolean	visible;
		
		COLS(String header,int align,int width,boolean visible) {
			this.width 		= width;
			this.align 		= align;
			this.header		= header;
			this.visible 	= visible;
		}
		
		public static COLS indexOf(String arg) {
			for(int i=0;i<COLS.values().length;i++) {
				if(arg.equals(COLS.values()[i].header)) {
					return COLS.values()[i];
				}
			}
			return null;
		}
	}
	
	public ClientActivityTab(StatusUIContainer parent) {
		super(parent);
		hashNetworks 			= new Hashtable<String, NetworkInfo>();
		currentSelectedNetwork 	= Mesh.DEFAULT_MESH_ID;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.ui.statusdisplay.ui.StatusPage#createControls(org.eclipse.swt.custom.CTabFolder)
	 */
	public void createControls(CTabFolder parent) {
		
		super.createControls(parent);
		
		staTableTree	=	new Tree(canvas,SWT.FULL_SELECTION);
		staTableTree.setLinesVisible(true);
		staTableTree.setHeaderVisible(true);

		staTableTree.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
//				getSelectedApsList();
			}
		});
		
		for(COLS colInfo : COLS.values()) {
			TreeColumn col  = new TreeColumn(staTableTree, colInfo.align);
			col.setText(colInfo.header);
			showColumn(col, colInfo.visible);
		}
		ContextHelp.addContextHelpHandlerEx(this.canvas,contextHelpEnum.CLIENTACTIVITYWINDOW);
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.ui.statusdisplay.ui.StatusPage#reSize(org.eclipse.swt.graphics.Rectangle)
	 */
	public void reSize(Rectangle parentBounds) {
		super.reSize(parentBounds);
		staTableTree.setBounds(0,0,canvas.getBounds().width,canvas.getBounds().height);
	}
	
	/**
	 * @param staList
	 * @param item
	 */
	private void updateApChildren(ApInfo apInfo, TreeItem apItem) {
		
		for(TreeItem item : apItem.getItems()) {
			if(apInfo.getStaInfo(item.getText(0).trim(), false) == null)
				item.dispose();
		}

		Enumeration<StaInfo>	staEnum		= apInfo.staTable.elements();
		while(staEnum.hasMoreElements()) {
			
			StaInfo staInfo	= staEnum.nextElement();
			
			if(staInfo.isImcp == Mesh.ENABLED || 
				MacAddress.isMeshDynamicsMacaddress(staInfo.staAddress) == true) {
				for(TreeItem item : apItem.getItems()) {
					if(staInfo.staAddress.equals(item.getText(0).trim())) {
						item.dispose();
						break;
					}
				}
				continue;
			}
		
			TreeItem updateItem = null; 
			for(TreeItem item : apItem.getItems()) {
				if(staInfo.staAddress.equals(item.getText(0).trim())) {
					updateItem = item;
					break;
				}
			}
			if(updateItem == null)
				updateItem = new TreeItem(apItem, SWT.SIMPLE);
			
			updateStaItem(updateItem, staInfo);
		}
	}

	private void updateStaItem(TreeItem item, StaInfo staData) {

		if(item == null || staData == null) {
			return;
		}
		
		item.clearAll(true);
		
		item.setText(COLS.MACADDRESS.ordinal(),	staData.staAddress);
        // staData does not contain Ip Adddress
        item.setText(COLS.IPADDRESS.ordinal(), "");
		item.setText(COLS.UPSTREAM_RATE.ordinal(), ""+staData.upStreamRate+" Mbps");
		item.setText(COLS.SIGNAL.ordinal(), ""+staData.signal+" dBm");
		
		if(staData.exInfoAvailable == false)
			return;
		
		String temp = "";
		
		
		temp		= (staData.essid == null) ? 
						MeshviewerStringConstants.NOT_AVAILABLE : staData.essid;
		item.setText(COLS.ESSID.ordinal(), temp);
		
		temp		= (staData.bssid == null) ? 
						MeshviewerStringConstants.NOT_AVAILABLE : staData.bssid;
		item.setText(COLS.BSSID.ordinal(), temp);
		
		temp		= (staData.activityTime <= 0) ? 
						MeshviewerStringConstants.NOT_AVAILABLE : DateTime.getTimeSpan(staData.activityTime);
		item.setText(COLS.ACTIVITY_TIME.ordinal(), temp);
		
		temp		= (staData.bytesRecvd == -1) ? 
						MeshviewerStringConstants.NOT_AVAILABLE : ""+staData.bytesRecvd;
		item.setText(COLS.BYTES_RECEIVED.ordinal(),	temp);
		
		temp		= (staData.bytesSent == -1) ? 
						MeshviewerStringConstants.NOT_AVAILABLE : ""+staData.bytesSent;
		item.setText(COLS.BYTES_SENT.ordinal(),	temp);
		
	}

	/**
	 * 
	 */
/*	
	private String[] getAllApsList() {
		
		String[]	apList 		= null;
		
		int index;
		TableTreeItem[]	items	= staTableTree.getItems();
		apList					= new String[items.length];
		
		for(index = 0; index < items.length; index++) {
			apList[index]	 = items[index].getText(0);
	    }
		return apList;
	}
*/
	/**
	 * 
	 */
/*	
	private String[] getSelectedApsList() {
		
		int[] 		selectedIndices = null;
		int 		itemIndex;
		String[]	selectedApsList;
		
		selectedIndices	= lstSTAActivity.getSelectionIndices();
	
		
		selectedApsList = new String[selectedIndices.length];
		boolean childrenFound = false;
		
		ClientActivityData.NetworkInfo networkInfo	= (ClientActivityData.NetworkInfo)staData.getReceivedInfo();
		
		if(networkInfo == null)
			return null;
		Hashtable<String, Hashtable<String, IStaConfiguration>>	apList		= (Hashtable<String, Hashtable<String, IStaConfiguration>>)networkInfo.apTable;
		
		for(itemIndex = 0; itemIndex < selectedIndices.length; itemIndex++) {
			TableItem	item		= lstSTAActivity.getItem(selectedIndices[itemIndex]);
			String 		apName		=  item.getText(0); 
			selectedApsList[itemIndex]  = apName;
			Hashtable<String, IStaConfiguration> 	staList		= (Hashtable<String, IStaConfiguration>)apList.get(apName);
			if(staList == null) {
				childrenFound = true;
			}
		}
		return selectedApsList;
	}
*/	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.ui.statusdisplay.ui.StatusPage#saveData()
	 */
	public void saveData() {
/*		
		int 			apIndex;
		String[]        filter= {".csv"};
		String          fileName;
		String[]		apList = null;
		BufferedWriter  bw;
		File            fp;
		FileDialog      fd;
		FileWriter      fos;
		
		OptionMessageBox msgBox = new OptionMessageBox(
									  "Do you want to save selected or all Access Points?",
									  "Save all Access Points",
									  "Save selected Access Points");
		
		msgBox.show();
		if(msgBox.getBtnSelected() == -1){
			return;
		} else if(msgBox.getBtnSelected() == 1){
			apList	= getAllApsList();
		}else if(msgBox.getBtnSelected() == 2){
			apList	= getSelectedApsList();
		}
		
		if(apList == null) {
			return;
		}
		if(apList.length == 0) {
			MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_INFORMATION);
			msg.setMessage("No Access Point selected.");
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.open();
			return;
		}
		
		fd		 = new FileDialog(new Shell(SWT.CLOSE),SWT.SAVE);
		fd.setFilterPath(MFile.getFileAbsolutePath());
		fd.setFilterExtensions(filter);
		fileName = fd.open();
		if(fileName == null) {
			return;
		}
		String headers   =  "Parent Mac Address,"+
							"STA Mac Address,"+
							"ESSID,"+
							"BSSID,"+
							"Upstream Rate (Mbps),"+
							"Signal(dBm),"+
							"Active Time,"+
							"Bytes Received,"+
							"Bytes Sent";

		
		fp		 = new File(fileName);
		try {
			fos 				= new FileWriter(fp);
			bw 					= new BufferedWriter(fos);
			bw.write(headers);
			bw.newLine();
			bw.flush();
		
			for(apIndex = 0; apIndex < apList.length; apIndex++) {
				saveDataInFile(bw,apList[apIndex]);
			}	
			bw.flush(); 
			bw.close(); 
			fos.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
*/		
	}
/*
	private void saveDataInFile(BufferedWriter bw, String apName) {
		
		Object		obj			= staData.getReceivedInfo();
		Hashtable	apList		= (Hashtable)obj;
		Hashtable 	staList		= (Hashtable)apList.get(apName);
		
		if(staList == null) {
			MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_INFORMATION);
			msg.setMessage("Invalid Access Point selection.");
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.open();
			return;
		}
		
		Enumeration staEnum		= staList.elements();
		try {
				bw.write(apName);
				bw.newLine();
				bw.flush();
				
				int staIndex	= 1;
				
				while(staEnum.hasMoreElements()) {
			
					IStaConfiguration 	tempStaData	= (IStaConfiguration)staEnum.nextElement();
					if(tempStaData.getIsIMCP() == 1) {
						continue;
					}
					String 				staInfo		=  "," + tempStaData.getStaMacAddress()+
													   "," + tempStaData.getParentESSID()+	
													   "," + tempStaData.getParentwmMacAddress()+
													   "," + tempStaData.getBitRate()+
													   "," + tempStaData.getSignal()+
													   "," + DateTime.getTimeSpan(tempStaData.getActiveTime())+
													   "," + tempStaData.getBytesReceived()+
													   "," + tempStaData.getBytesSent();
					bw.write(staInfo);
					bw.newLine();
					bw.flush();
					staInfo = null;
				}
		}catch(IOException ioe) {
			ioe.printStackTrace();
			return;
		}
	}
*/	

	@Override
	public void clearUI() {
		// TODO Auto-generated method stub
		
	}
	
	private NetworkInfo getNetworkInfo(String networkName, boolean createNew) {

		NetworkInfo nwInfo = hashNetworks.get(networkName);
		if(nwInfo == null && createNew == true) {
			nwInfo = new NetworkInfo(networkName);
			hashNetworks.put(networkName, nwInfo);
		}
		
		return nwInfo;
		
	}
	
	public void apPropertyChanged(int filter, int subFilter, AccessPoint src) {
		
		if(filter != IAPPropListener.MASK_HEARTBEAT &&
			filter != IAPPropListener.MASK_HEARTBEAT &&
			filter != IAPPropListener.MASK_AP_STA_CONF)
			return;
			
		String networkName = src.getNetworkId();
		NetworkInfo nwInfo = getNetworkInfo(networkName, true);
		
		if(filter == IAPPropListener.MASK_AP_STA_CONF &&
			subFilter == IAPPropListener.AP_STA_CONF_STA_INFO) {
			if(nwInfo.showClientDetails == false)
				showClientDetailCloumns(true);
			
			nwInfo.showClientDetails = true;
		}

		String 	apMacAddress 	= src.getDSMacAddress();
		ApInfo	apInfo			= nwInfo.getApInfo(apMacAddress, true);
        apInfo.ipAddress        = src.getIpAddress();
		apInfo.clientActivityEnabled = src.isStaActivityEnabled();
		
		IRuntimeConfiguration	runtimeConfig	= src.getRuntimeApConfiguration();
		
		Enumeration<StaInfo> staEnum = apInfo.staTable.elements();
		MacAddress 	staAddress = new MacAddress();
		
		while(staEnum.hasMoreElements()) {
			StaInfo staInfo = staEnum.nextElement();
			staAddress.setBytes(staInfo.staAddress);
			if(runtimeConfig.getStaInformation(staAddress) == null) {
				apInfo.staTable.remove(staInfo.staAddress);
			}
		}
		
		Enumeration<IStaConfiguration>enumSta	= runtimeConfig.getStaList();
		while(enumSta.hasMoreElements()) {
			IStaConfiguration staConf = enumSta.nextElement();
			if(staConf == null) {
				continue;
			}
			
			String 	strStaAddress 	= staConf.getStaMacAddress().toString(); 
			StaInfo staInfo 		= apInfo.getStaInfo(strStaAddress, true); 
			staInfo.exInfoAvailable = apInfo.clientActivityEnabled;
			updateStaInfo(staConf, staInfo);
			
		}
		
		if(currentSelectedNetwork.equalsIgnoreCase(networkName) == true) {
			updateApUI(apInfo);
		}
	
	}

	private void updateStaInfo(IStaConfiguration staConf, StaInfo staInfo) {

		staInfo.signal			= staConf.getSignal();
		staInfo.upStreamRate	= staConf.getBitRate();
		staInfo.isImcp			= staConf.getIsIMCP();
		
		if(staInfo.exInfoAvailable == false)
			return;
	
		staInfo.activityTime 	= staConf.getActiveTime();
		MacAddress bssid 		= staConf.getParentwmMacAddress();
		if(bssid != null)
			staInfo.bssid			= bssid.toString();
		staInfo.bytesRecvd		= staConf.getBytesReceived();
		staInfo.bytesSent		= staConf.getBytesSent();
		staInfo.essid			= staConf.getParentESSID();
		
	}

	private void updateApUI(ApInfo apInfo) {

		TreeItem 	apItem 	= null;
		
		if(staTableTree.isDisposed() == true)
			return;
		
		for(TreeItem item : staTableTree.getItems()) {
			if(apInfo.macAddress.equalsIgnoreCase(item.getText(0).trim())) {
				apItem = item;
                apItem.setText(1, apInfo.ipAddress == null ? "Not Set" : apInfo.ipAddress.toString());
				break;
			}
		}
		if(apItem == null) { 
			apItem = new TreeItem(staTableTree, SWT.NONE);
			apItem.setText(0, apInfo.macAddress);
            apItem.setText(1, apInfo.ipAddress == null ? "Not Set" : apInfo.ipAddress.toString());
		}

		updateApChildren(apInfo, apItem);
		
	}
	
	private void updateNetworkUI(NetworkInfo nwInfo) {
		
		showClientDetailCloumns(nwInfo.showClientDetails);
		
		Enumeration<ApInfo>	apEnum	= nwInfo.apTable.elements();
		
		while(apEnum.hasMoreElements()) {
			updateApUI(apEnum.nextElement());	 		
		}
		
	}

	@Override
	public void networkDeleted(String networkName) {
		super.networkDeleted(networkName);
		NetworkInfo nwInfo = hashNetworks.remove(networkName);
		if(nwInfo == null)
			return;

		if(currentSelectedNetwork.equalsIgnoreCase(networkName) == true) {
			updateNetworkUI(nwInfo);
		}
	}

	@Override
	public void networkSelected(String networkName) {
		super.networkSelected(networkName);

    	if(currentSelectedNetwork.equalsIgnoreCase(networkName) == true)
    		return;
    
    	currentSelectedNetwork = networkName;
		
		NetworkInfo nwInfo = hashNetworks.get(networkName);
		if(nwInfo == null) {
			nwInfo = new NetworkInfo(networkName);
			hashNetworks.put(networkName, nwInfo);
		}

		staTableTree.removeAll();
		updateNetworkUI(nwInfo);
	}

	private void showClientDetailCloumns(boolean show) {
		
		for(int i=0;i<COLS.values().length;i++){
			TreeColumn col = staTableTree.getColumn(i);
			if(col == null)
				continue;
			
			if(show == true)
				showColumn(col, true);
			else 
				showColumn(col, COLS.values()[i].visible);
		}
		
	}

	private void showColumn(TreeColumn col,boolean show) {
		
		if(show == false) {
			col.setWidth(0);
			col.setResizable(false);
		}else {
			COLS c = COLS.indexOf(col.getText());
			if(c != null)
				col.setWidth(c.width);
			col.setResizable(true);
		}
		
	}

	public void clientActivityChanged(String networkName, boolean enable) {
		
		NetworkInfo nwInfo = hashNetworks.get(networkName);
		if(nwInfo == null) {
			nwInfo = new NetworkInfo(networkName);
			hashNetworks.put(networkName, nwInfo);
		}
		
		if(nwInfo.showClientDetails != enable)
			showClientDetailCloumns(enable);
		
		nwInfo.showClientDetails = enable;
	
		if(currentSelectedNetwork.equalsIgnoreCase(networkName) == true)
			updateNetworkUI(nwInfo);
	}

	@Override
	protected void onDispose() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	void saveUIProperties(ITabInfo tabInfo) {
		tabInfo.setColumnCount(COLS.values().length);
		
		int i = 0;
		for(COLS col : COLS.values()) {
			ITabColumnInfo columnInfo = tabInfo.getColumnInfo(i++);
			columnInfo.setColumnWidth(col.width);
		}
	}

	@Override
	void loadUIProperties(ITabInfo tabInfo) {
		
		for(int i=0;i<tabInfo.getColumnCount();i++) {
			ITabColumnInfo columnInfo = tabInfo.getColumnInfo(i);
			COLS col = COLS.values()[i];
			col.width = columnInfo.getColumnWidth();
			if(col.visible == true)
				staTableTree.getColumn(i).setWidth(col.width);
		}
		
	}

}
