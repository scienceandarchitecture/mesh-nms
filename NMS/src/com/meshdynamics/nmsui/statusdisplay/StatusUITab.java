/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : StatusPage.java
 * Comments : 
 * Created  : May 10, 2007
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |May 10, 2007  | Created                                        | Imran|
 * --------------------------------------------------------------------------------
 **********************************************************************************/
/*
 * Created on May 10, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.statusdisplay;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;

import com.meshdynamics.nmsui.PerspectiveSettings.ITabInfo;

/**
 * @author imran
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public abstract class StatusUITab {
	
	protected 	CTabItem			tabItem;
	private 	String				caption;
	protected 	Canvas				canvas;		
	protected   StatusUIContainer	parent;
	protected	boolean				selected;
		
	/**
	 * @param parent
	 */
	StatusUITab(StatusUIContainer parent) {
		
		caption			= "Status";
		this.parent		= parent;
	}

	void createControls(CTabFolder parent) {
		
		tabItem = new CTabItem(parent,SWT.NONE); 
		tabItem.setText(caption);
		
		canvas = new Canvas(parent,SWT.NONE);
		canvas.setBounds(parent.getBounds());
		
		tabItem.setControl(canvas);
		tabItem.addDisposeListener(new DisposeListener(){

			@Override
			public void widgetDisposed(DisposeEvent arg0) {
				onDispose();
			}
		});
		
		tabItem.setData(this);
	}

	
	void selectionChanged(boolean selected) {
		this.selected = selected;
	}

	void setCaption(String caption) {
		this.caption = caption;
		
		if(tabItem != null)
			tabItem.setText(caption);
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.ui.statusdisplay.ui.IStatusUI#reSize(org.eclipse.swt.graphics.Rectangle)
	 */
	void reSize(Rectangle parentBounds) {
		canvas.setBounds(parentBounds);
	}

	boolean isDisposed(){
		return tabItem.isDisposed();
	}

	abstract void clearUI();
	abstract void onDispose();
	
    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.ui.statusdisplay.ui.IStatusUI#getCaption()
     */
    String getCaption() {
        return caption;
    }
    
    void dispose() {
    	canvas.dispose();
    	tabItem.dispose();
    }
    
    void networkDeleted(String networkName) {
    }

    void networkSelected(String networkName) {
    }

	void saveData() {
	}

	void saveUIProperties(ITabInfo tabInfo) {
	}

	void loadUIProperties(ITabInfo tabInfo) {
	}
	
}
