/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : HeartbeatTab.java
 * Comments : 
 * Created  : May 8, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  2  |May 10, 2007 | column names modified                           | Imran  |
 *  --------------------------------------------------------------------------------
 * |  1  |May 10, 2007 | createControls added                            | Imran  |
 * --------------------------------------------------------------------------------
 * |  0  |May 8, 2007  | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.statusdisplay;

import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.configuration.IStaConfiguration;
import com.meshdynamics.meshviewer.configuration.KnownAp;
import com.meshdynamics.meshviewer.configuration.KnownAp.KnownApInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabColumnInfo;
import com.meshdynamics.nmsui.PerspectiveSettings.ITabInfo;
import com.meshdynamics.nmsui.dialogs.UIConstants;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.util.DateTime;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

public class HeartbeatTab extends StatusUITab {
	
	private Table		tblHeartbeat;
	private Hashtable<String, Hashtable<String, ApHeartbeatData>>	hashNetworkHeartBeatData;
	private String		currentSelectedNetwork;
	
	private static final int    HEARTBEAT_COLUMN_AP_STATE			= 0;
	private static final int    HEARTBEAT_COLUMN_MACADDRESS 		= 1;
	private static final int    HEARTBEAT_COLUMN_IPADDRESS 			= 2;
	private static final int 	HEARTBEAT_COLUMN_NODE_NAME			= 3;
	private static final int    HEARTBEAT_COLUMN_TIMESTAMP			= 4;
	private static final int 	HEARTBEAT_COLUMN_MODEL_NO			= 5;
	private static final int 	HEARTBEAT_COLUMN_DOWNLINK_SIGNAL 	= 6;
	private static final int	HEARTBEAT_COLUMN_DOWNLINK_RATE		= 7;
	private static final int	HEARTBEAT_COLUMN_UPLINK_SIGNAL		= 8;
	private static final int	HEARTBEAT_COLUMN_UPLINK_RATE		= 9;
	private static final int	HEARTBEAT_COLUMN_GPS_LATITUDE		= 10;
	private static final int	HEARTBEAT_COLUMN_GPS_LONGITUDE		= 11;
	private static final int	HEARTBEAT_COLUMN_GPS_SPEED			= 12;
	private static final int	HEARTBEAT_COLUMN_GPS_ALTITUDE		= 13;

	public static final int 	HBTAB_HB_1 = 1;
	public static final int 	HBTAB_HB_2 = 2;
	
	private class ApHeartbeatData {

		public int          apState;	
		public String 	    macAddr;
		public String 		timeStamp;
		public String 		nodeName;
		public String 		modelNumber;
		public String 		parentDlinkSignal;
		public String 		parentDlinkRate;
		public String 		uplinkSignal;
		public String 		uplinkRate;
		public String 		ipAddress;
		public String 		latitude;
		public String 		longitude;
		public String 		speed;
		public String 		altitude;
		
		public boolean		justReceived;
		public boolean 		canConfigure;
		
		public ApHeartbeatData() {
			
			apState				= AccessPoint.STATUS_DEAD;
			macAddr        	    = ""; 
			ipAddress			= "Not Available";
			nodeName			= "";
			timeStamp 			= DateTime.getDateTime();
			modelNumber			= "";			
			parentDlinkSignal 	= "--"; 
			parentDlinkRate		= "--";
			uplinkSignal		= "--";
			uplinkRate			= "--";
			justReceived		= false;
			canConfigure		= false;
			latitude			= "";
			longitude			= "";
			speed				= "";
			altitude			= "";
		}
	}
	
	public HeartbeatTab(StatusUIContainer parent) {
		super(parent);
		hashNetworkHeartBeatData 	= new Hashtable<String, Hashtable<String, ApHeartbeatData>>();
		currentSelectedNetwork		= Mesh.DEFAULT_MESH_ID;
	}

	public void createControls(CTabFolder parent) {
		
		super.createControls(parent);
		
		tblHeartbeat = new Table (canvas,SWT.FULL_SELECTION|SWT.MULTI);
		tblHeartbeat.setHeaderVisible(true);
		tblHeartbeat.setLinesVisible(true);
		tblHeartbeat.setBounds(canvas.getBounds());
		tblHeartbeat.addMouseListener(new MouseAdapter() {

			public void mouseDoubleClick(MouseEvent arg0) {
				tblDoubleClickedHandler();
			}

			public void mouseDown(MouseEvent arg0) {
				if(arg0.button == 3) {
					tblItemRightClicked();
				}
				
			}

		});
		String Headers[] = {"",
							"Mac Address",
							"IP Address",
							"Node Name",
							"Time Stamp",							
							"Model No",
							"Rx Signal (dBm)",
							"Rx Rate (Mbps)" ,
							"Tx Signal (dBm)",
							"Tx Rate (Mbps)",
							"Latitude",
							"Longitude",
							"Speed",
							"Altitude"
							};
		TableColumn col;
		int 		index;
		for(index = 0; index < Headers.length; index++) {
			if(index > 5 && index <= 9) {
				col  = new TableColumn(tblHeartbeat ,SWT.RIGHT );
   			}else {
				col  = new TableColumn(tblHeartbeat ,SWT.NONE );
			}
			col.setText(Headers[index]);
			col.setWidth(120);	
		}
		
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_AP_STATE);
		col.setWidth(15);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_MACADDRESS);
		col.setWidth(120);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_IPADDRESS);
		col.setWidth(120);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_NODE_NAME);
		col.setWidth(100);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_TIMESTAMP);
		col.setWidth(110);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_MODEL_NO);
		col.setWidth(90);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_DOWNLINK_SIGNAL);
		col.setWidth(90);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_DOWNLINK_RATE);
		col.setWidth(90);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_UPLINK_SIGNAL);
		col.setWidth(90);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_UPLINK_RATE);
		col.setWidth(90);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_GPS_LATITUDE);
		col.setWidth(70);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_GPS_LONGITUDE);
		col.setWidth(70);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_GPS_SPEED);
		col.setWidth(50);
		col =  tblHeartbeat.getColumn(HEARTBEAT_COLUMN_GPS_ALTITUDE);
		col.setWidth(50);
		
		ContextHelp.addContextHelpHandler(canvas,contextHelpEnum.HEARTBEATWINDOW);
		
		networkSelected(currentSelectedNetwork);
	}
	
	
	private void tblDoubleClickedHandler() {
		int 		index;
		TableItem	item;
		
		index 	= tblHeartbeat.getSelectionIndex();
		item	= tblHeartbeat.getItem(index);
		parent.notifyApDoubleClicked(item.getText( HEARTBEAT_COLUMN_MACADDRESS ));
	}
	
	public void tblItemRightClicked() {
		int 		index;
		TableItem	item;
		
		index 	= tblHeartbeat.getSelectionIndex();
		item	= tblHeartbeat.getItem(index);
		String strMac	= item.getText(1);
		MacAddress	mac	= new MacAddress();
		mac.setBytes(strMac); 
		parent.notifyApRightClicked(tblHeartbeat, mac);
	}
	

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.ui.statusdisplay.ui.StatusPage#reSize(org.eclipse.swt.graphics.Rectangle)
	 */
	public void reSize(Rectangle parentBounds) {
		super.reSize(parentBounds);
		tblHeartbeat.setBounds(0,0,canvas.getBounds().width,canvas.getBounds().height);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.ui.statusdisplay.ui.StatusPage#saveData()
	 */
	public void saveData() {
/*			
		int 			apIndex;
		String[]        filter= {"*.txt"};
		String          fileName;
		String[]		apList = null;
		BufferedWriter  bw;
		File            fp;
		FileDialog      fd;
		FileWriter      fos;
		
		OptionMessageBox msgBox = new OptionMessageBox(
									  "Do you want to save selected or all Access Points?",
									  "Save all Access Points",
									  "Save selected Access Points");
		
		msgBox.show();
		if(msgBox.getBtnSelected() == -1){
			return;
		} else if(msgBox.getBtnSelected() == 1){
			apList	= getAllApsList();
		}else if(msgBox.getBtnSelected() == 2){
			apList	= getSelectedApsList();
		}
		
		if(apList == null) {
			return;
		}
		fd		 = new FileDialog(new Shell(SWT.CLOSE),SWT.SAVE);
		fd.setFilterPath(MFile.getFileAbsolutePath());
		fd.setFilterExtensions(filter);
		fileName = fd.open();
		if((fileName == null)	||
				fileName.equalsIgnoreCase("")) {
			return;
		}
		fp		 = new File(fileName);
		try {
			fos 				= new FileWriter(fp);
			bw 					= new BufferedWriter(fos); 
			for(apIndex = 0; apIndex < apList.length; apIndex++) {
			
				bw.write(apList[apIndex]);
				bw.newLine();
			}
				
			bw.flush(); 
			bw.close(); 
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
*/		
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.ui.statusdisplay.ui.IStatusUI#selectEntry(java.lang.String)
	 */
	public void selectEntry(String macAddress) {
		tblHeartbeat.deselectAll();
		for(TableItem item : tblHeartbeat.getItems()) {
			if(item.getText(1).equalsIgnoreCase(macAddress)) {
				tblHeartbeat.setSelection(item);
				tblHeartbeat.showItem(item);
				tblHeartbeat.showSelection();
				break;
			}
		}
	}

	@Override
	public void clearUI() {
		for(TableItem item : tblHeartbeat.getItems()) {
			item.dispose();
		}
		tblHeartbeat.removeAll();
	}

	private void updateHeartBeatInfo(AccessPoint ap, ApHeartbeatData heartbeatData) {
		
		IRuntimeConfiguration runtimeConfig	=	ap.getRuntimeApConfiguration();
		if(runtimeConfig == null) {
			return;
		}

		heartbeatData.parentDlinkSignal = "--";
		heartbeatData.uplinkRate		= "--";
		
		heartbeatData.latitude 	= runtimeConfig.getLatitude();
		heartbeatData.longitude = runtimeConfig.getLongitude();
		heartbeatData.speed		= runtimeConfig.getSpeed();
		heartbeatData.altitude	= runtimeConfig.getAltitude();
		
		if(ap.isRoot() == true)
			return;

		MacAddress parentDSMacAddress = runtimeConfig.getParentDSMacAddress();
		if(parentDSMacAddress == null) {
			return;
		}
	
		Enumeration<KnownAp> knownApEnum = runtimeConfig.getKnownAPs();
		while(knownApEnum.hasMoreElements()) {

			KnownAp knownAp = knownApEnum.nextElement();
			if(knownAp.macAddress.equals(parentDSMacAddress) == true) {
				KnownApInfo knownApInfo = (KnownApInfo) knownAp.knownApInfo.get(0);
				heartbeatData.parentDlinkSignal = String.valueOf(knownApInfo.signal);
				//heartbeatData.uplinkRate		= String.valueOf(knownApInfo.bitRate);
				break;
			}
			
		}
	}
	
	private void updateHeartBeat2Info(AccessPoint	ap, ApHeartbeatData heartbeatData ) {
		
		IRuntimeConfiguration runtimeConfig	=	ap.getRuntimeApConfiguration();
 		if(runtimeConfig == null) {
 			return;
 		}
 		
		heartbeatData.uplinkSignal		= "--";
		heartbeatData.parentDlinkRate	= "--";
		
		if(ap.isRoot() == true)
			return;
		
		heartbeatData.uplinkRate		= String.valueOf(runtimeConfig.getUplinkTransmitRate());
		AccessPoint parentAp = ap.getParentAp();
		if(parentAp == null) {
			return;
		}
		
		IRuntimeConfiguration parentConfig = parentAp.getRuntimeApConfiguration();
		if(parentConfig == null) {
			return;
		}
		
		Enumeration<IStaConfiguration> 	staEnum			= parentConfig.getStaList();
		MacAddress						dsMacAddress	= ap.getDsMacAddress();
		
		while(staEnum.hasMoreElements()){
			IStaConfiguration staInfo	= staEnum.nextElement();
			
			if(staInfo.getStaMacAddress().equals(dsMacAddress)) {
				heartbeatData.uplinkSignal 		= String.valueOf(staInfo.getSignal());  
				heartbeatData.parentDlinkRate   = String.valueOf(staInfo.getBitRate());
				break;
			}
		}
	}

	private void updateApUI(ApHeartbeatData hbData) {
		
		if(isDisposed() == true) {
			return;
		}
		
		TableItem	updateItem	= null;
		for(TableItem item : tblHeartbeat.getItems()) {

			item.setForeground(UIConstants.BLACK_COLOR);
			String macAddr	= item.getText(1).trim();
			if(hbData.macAddr.equalsIgnoreCase(macAddr) == false)
				continue;

			updateItem 	= item;
			
		}

		if(hbData.apState == IAPPropListener.AP_STATE_DISPOSED) {
			if(updateItem != null)
				updateItem.dispose();
			return;
		}
		
		if(updateItem == null) 
			updateItem = new TableItem(tblHeartbeat, SWT.NONE);
		
		if(hbData.apState == IAPPropListener.AP_STATE_ALIVE ) {
			updateItem.setForeground(UIConstants.BLUE_COLOR);
		}
		fillItemData(updateItem, hbData);
	}
	
	private void updateIpAddress(ApHeartbeatData hbData, AccessPoint src) {
		IpAddress ipAddress = src.getIpAddress();
		hbData.ipAddress	= (ipAddress.equals(IpAddress.resetAddress) == true) ?
								"Not Available" : ipAddress.toString();
	}

	private void updateApConfig(ApHeartbeatData hbData, AccessPoint src) {
		hbData.nodeName			= src.getName();
		hbData.modelNumber		= src.getModel();
		hbData.canConfigure		= src.canConfigure();
	}
	
	public void apPropertyChanged(int filter, int subFilter, AccessPoint src) {
		
		String networkName = src.getNetworkId();
		Hashtable<String, ApHeartbeatData> hashApHbData = hashNetworkHeartBeatData.get(networkName);
		
		if(hashApHbData == null) {
			hashApHbData = new Hashtable<String, ApHeartbeatData>();
			hashNetworkHeartBeatData.put(networkName, hashApHbData);
		}
		
		String apMacAddress 	= src.getDSMacAddress();
		ApHeartbeatData hbData 	= hashApHbData.get(apMacAddress);
		if(hbData == null) {
			hbData 			= new ApHeartbeatData();
			hbData.macAddr 	= apMacAddress;
			hashApHbData.put(apMacAddress, hbData);
		}
		
		switch(filter) {
			case IAPPropListener.MASK_AP_STATE:
				if(subFilter == IAPPropListener.AP_STATE_DISPOSED)
					hashApHbData.remove(apMacAddress);
				hbData.apState = subFilter;
				if(currentSelectedNetwork.equalsIgnoreCase(networkName))
					updateApUI(hbData);
				return;
			case IAPPropListener.MASK_IPCONF:
				updateIpAddress(hbData, src);
				if(currentSelectedNetwork.equalsIgnoreCase(networkName))
					updateApUI(hbData);
				return;
			case IAPPropListener.MASK_APCONF:
				updateApConfig(hbData, src);
				if(currentSelectedNetwork.equalsIgnoreCase(networkName))
					updateApUI(hbData);
				return;
			case IAPPropListener.MASK_HEARTBEAT:
				hbData.timeStamp = DateTime.getDateTime();
				updateHeartBeatInfo(src, hbData);
				break;
			case IAPPropListener.MASK_HEARTBEAT2:
				updateHeartBeat2Info(src, hbData);
				break;
		}
		
		updateIpAddress(hbData, src);
		updateApConfig(hbData, src);
		
		if(currentSelectedNetwork.equalsIgnoreCase(networkName))
			updateApUI(hbData);
	}

	public void networkSelected(String networkName) {
		
    	if(currentSelectedNetwork.equalsIgnoreCase(networkName) == true)
    		return;

    	currentSelectedNetwork = networkName;
    	
		Hashtable<String, ApHeartbeatData> hashApHbData = hashNetworkHeartBeatData.get(networkName);
		
		if(hashApHbData == null) {
			hashApHbData = new Hashtable<String, ApHeartbeatData>();
			hashNetworkHeartBeatData.put(networkName, hashApHbData);
		}
		
		tblHeartbeat.removeAll();
		Enumeration<ApHeartbeatData>		hbDataEnum		= hashApHbData.elements();
		while(hbDataEnum.hasMoreElements()) {
			ApHeartbeatData	hbData	=(ApHeartbeatData)hbDataEnum.nextElement();
			TableItem	item		= new TableItem(tblHeartbeat, SWT.NONE);
			fillItemData(item, hbData);
		}
	}

	/**
	 * @param item
	 * @param hbData
	 */
	private void fillItemData(TableItem item, ApHeartbeatData hbData) {
				
		switch(hbData.apState) {
			case IAPPropListener.AP_STATE_DEAD:
				item.setBackground(0, UIConstants.RED_COLOR);
				break;
			case IAPPropListener.AP_STATE_STARTUP:				
			case IAPPropListener.AP_STATE_ALIVE:
				if(hbData.canConfigure == false)
					item.setBackground(0, UIConstants.YELLOW_COLOR);
				else
					item.setBackground(0, UIConstants.GREEN_COLOR);
				break;
			case IAPPropListener.AP_STATE_SCANNING:
				item.setBackground(0, UIConstants.BLUE_COLOR);
				break;
			case IAPPropListener.AP_STATE_HEARTBEAT_MISSED:
				item.setBackground(0, UIConstants.ORANGE_COLOR);
				break;
		}
			
		item.setText(HEARTBEAT_COLUMN_MACADDRESS,hbData.macAddr);
		item.setText(HEARTBEAT_COLUMN_IPADDRESS,hbData.ipAddress);
		item.setText(HEARTBEAT_COLUMN_NODE_NAME,hbData.nodeName);
		item.setText(HEARTBEAT_COLUMN_TIMESTAMP,hbData.timeStamp);
		item.setText(HEARTBEAT_COLUMN_MODEL_NO,hbData.modelNumber);
		item.setText(HEARTBEAT_COLUMN_DOWNLINK_SIGNAL,hbData.parentDlinkSignal);
		item.setText(HEARTBEAT_COLUMN_DOWNLINK_RATE,hbData.parentDlinkRate);
		item.setText(HEARTBEAT_COLUMN_UPLINK_SIGNAL,hbData.uplinkSignal);
		item.setText(HEARTBEAT_COLUMN_UPLINK_RATE,hbData.uplinkRate);
		item.setText(HEARTBEAT_COLUMN_GPS_LATITUDE,hbData.latitude);
		item.setText(HEARTBEAT_COLUMN_GPS_LONGITUDE,hbData.longitude);
		item.setText(HEARTBEAT_COLUMN_GPS_SPEED,hbData.speed);
		item.setText(HEARTBEAT_COLUMN_GPS_ALTITUDE,hbData.altitude);
	}
	
    public void networkDeleted(String networkName) {
    	
		Hashtable<String, ApHeartbeatData> hashApHbData = hashNetworkHeartBeatData.remove(networkName);
		
		if(hashApHbData == null)
			return;
		
		if(currentSelectedNetwork.equals(networkName) == false)
			return;

		clearUI();
    }

	@Override
	protected void onDispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	void saveUIProperties(ITabInfo tabInfo) {
		tabInfo.setColumnCount(tblHeartbeat.getColumnCount());
		
		int i = 0;
		for(TableColumn col : tblHeartbeat.getColumns()) {
			ITabColumnInfo columnInfo = tabInfo.getColumnInfo(i++);
			columnInfo.setColumnWidth(col.getWidth());
		}
	}

	@Override
	void loadUIProperties(ITabInfo tabInfo) {
		int i=0;
		for(TableColumn col : tblHeartbeat.getColumns()) {
			ITabColumnInfo columnInfo = tabInfo.getColumnInfo(i++);
			int width = columnInfo.getColumnWidth();
			if(width > 0)
				col.setWidth(width);
		}
	}
	
}
