/*
 * Created on Nov 20, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.http;

import java.util.Vector;

/**
 * @author Abhijit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IJsonMeshInfoProvider {

	public interface IJsonAp {
		public String getJsonNodeId();
		public String getJsonNetworkId();
		public String getJsonLatitude();
		public String getJsonLongitude();
		public String getJsonParentNodeId();
		public String getJsonName();
		public int 	  jsonIsMobile();
	};
	
	public Vector<IJsonAp> getJsonApList();			//Vector has objects of type IJsonAp
	
	//Events
	public static int JSON_MOUSE_SINGLE_CLICK		= 1;
	public static int JSON_MOUSE_DOUBLE_CLICK		= 2;

	public static int JSON_MOUSE_BUTTON_LEFT		= 1;
	public static int JSON_MOUSE_BUTTON_RIGHT		= 2;
	public static int JSON_MOUSE_BUTTON_CENTER		= 3;
	
	public void handleJsonMouseClick	(int type, int button, String[] eventInfo);  //has node id
	
}
