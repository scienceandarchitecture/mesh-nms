/*
 * Created on Nov 19, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.http;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Vector;

import com.meshdynamics.nmsui.http.IJsonMeshInfoProvider.IJsonAp;
import com.meshdynamics.nmsui.util.MFile;

/**
 * @author Abhijit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class MeshJsonServer extends Thread {
	
	private IJsonMeshInfoProvider	meshDataProvider;
	private ServerSocket 			socket;
	
	private static final String CMD_GET_WEB_PAGE 		= "GET /mesh.html";
	private static final String CMD_GET_ACCESSPOINTS 	= "/getAccessPoints";
	private static final String CMD_GET_PROPERTIES 		= "/getProperties";
	private static final String CMD_AP_DBL_CLICKED		= "/apDblClicked";
	private static final String CMD_AP_RIGHT_CLICKED	= "/apRightClicked";
	private static final String CMD_DEBUG_STR			= "GET /debug";
	
	/**
	 * 
	 */
	public MeshJsonServer(IJsonMeshInfoProvider	meshDataProvider) {
		super();
		this.meshDataProvider = meshDataProvider;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		
		try {
			socket = new ServerSocket(12345);
		} catch (UnknownHostException e) {
			System.out.println(e);
			return;
		} catch (IOException e) {
			System.out.println(e);
			return;
		}

		System.out.println("JSON Server running on " + socket.getInetAddress().toString() + ":" + socket.getLocalPort());
		
		while(true) {
			
			BufferedReader	clientReader;
			PrintStream		clientWriter;
			Socket			connection;		
			String 			clientText;
			
			try {

				connection		= null;
				connection 		= socket.accept();
				clientReader	= new BufferedReader(new InputStreamReader(connection.getInputStream()));
				clientWriter 	= new PrintStream(connection.getOutputStream());
				
				String text;
				clientText		= "";
				String outStr 	= null;
				
				while((text = clientReader.readLine()) != null) {
					if(text.startsWith(CMD_DEBUG_STR) == true) {
						String tokens[] = text.split(" ");
						System.out.println(tokens[1]);
					} else if(text.startsWith(CMD_GET_WEB_PAGE) == true) {
						outStr = getIndexPage(text);
					} else if(text.startsWith("GET") == true) {
						outStr = getJsonObj(text);
						if(outStr == null) {
							byte[] outBytes = getFile(text);
							if(outBytes != null) {
								clientWriter.write(outBytes);
							} else {
								handleJsonEvent(text);
							}
						}
					}
					
					clientText += text;
					if(text.length() == 0) {
						break;
					}
				}

				if(outStr != null) {
					clientWriter.write(outStr.getBytes());
				}
				
				clientReader.close();
				clientWriter.close();
				connection.close();
			} catch (IOException e1) {
				System.out.println(e1);
				if(socket.isClosed() == true) {
					break;
				}
				continue;
			}
			
		}

		try {
			socket.close();
		} catch (IOException e1) {
			System.out.println(e1);
		}
	}
	
	/**
	 * @param text
	 */
	private void handleJsonEvent(String text) {
		String tokens[] 	= text.split(" ");
		String eventInfo[]	= tokens[1].split("::");

		if(eventInfo == null || eventInfo.length < 2) {
			return;
		}
		
		if(tokens[1].startsWith(CMD_AP_DBL_CLICKED) == true) {
			meshDataProvider.handleJsonMouseClick(	IJsonMeshInfoProvider.JSON_MOUSE_DOUBLE_CLICK,
													IJsonMeshInfoProvider.JSON_MOUSE_BUTTON_LEFT,
													eventInfo
												);
		} else if(tokens[1].startsWith(CMD_AP_RIGHT_CLICKED) == true) {
			meshDataProvider.handleJsonMouseClick(	IJsonMeshInfoProvider.JSON_MOUSE_SINGLE_CLICK,
													IJsonMeshInfoProvider.JSON_MOUSE_BUTTON_RIGHT,
													eventInfo
												);
		} 
	}

	private byte[] getBinaryFile(String fileName) {

		try {
			FileInputStream fStream = new FileInputStream(fileName);
			byte buffer[] = new byte[fStream.available()];
			
			try {
				
				fStream.read(buffer);
				fStream.close();
				return buffer;
				
			} catch (IOException e1) {
				System.out.println(e1);
			}
			
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return null;
	}

	/**
	 * @param string
	 * @return
	 */
	private byte[] getTextFile(String fileName) {
		
		System.out.println("Serving " + fileName);
		String fileStr = "";
		try {
			FileReader 		fReader = new FileReader(fileName);
			BufferedReader	br 		= new BufferedReader(fReader);
			String 			line;
			try {
				while((line = br.readLine()) != null) {
					fileStr += (line + "\r\n");
				}
			
				br.close();
				fReader.close();
				
			} catch (IOException e1) {
				System.out.println(e1);
				return null;
			}
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
		
		return fileStr.getBytes();
	}
	
	/**
	 * @param text
	 * @return
	 */
	private byte[] getFile(String text) {
		
		String imageExt[] 	= {"gif", "jpg", "jpeg", "bmp", "png", "ico"};
		String scriptExt  	= "js";
		String tokens[] 	= text.split(" ");
		String fileExt 		= tokens[1].substring(tokens[1].indexOf(".")+1);
		String contentType	= "Content-Type: ";
		
		boolean	loadFile 	= false;
		byte[] 	buffer 		= null;
		
		for(int i=0;i<imageExt.length;i++) {
			if(imageExt[i].equalsIgnoreCase(fileExt) == true) {
				loadFile = true;
				contentType += " image/" + fileExt + ";";
				buffer = getBinaryFile(MFile.getWebPath() + tokens[1]);
				break;
			}
		}
		if(loadFile == false) {
			if(scriptExt.equalsIgnoreCase(fileExt) == true) {
				contentType += " text/javascript; charset=UTF-8";
				buffer = getTextFile(MFile.getWebPath() + tokens[1]);
			}
		}
		
		String outStr = "";
		
		if(buffer != null) {
			outStr = "HTTP/1.1 200 OK\r\n";
			outStr += contentType + "\r\n";
			outStr += "Cache-Control: no-cache\r\n";
			outStr += "Content-Length: " + buffer.length + "\r\n";
			outStr += "\r\n";

			byte outStrBytes[] 	= outStr.getBytes();
			byte outBuffer[] 	= new byte[outStrBytes.length + buffer.length];
			
			System.arraycopy(outStrBytes, 0, outBuffer, 0, outStrBytes.length);
			System.arraycopy(buffer, 0, outBuffer, outStrBytes.length, buffer.length);
			
			return outBuffer;
		}
	
		return null;
	}

	/**
	 * @param text
	 * @return
	 */
	private String getJsonObj(String text) {
		
		String tokens[] 	= text.split(" ");
		String jsonObjStr	= "";
		String outStr 		= "HTTP/1.1 200 OK\r\n";
		
		outStr += "Content-Type: text/plain; charset=UTF-8\r\n";
		outStr += "Cache-Control: no-cache\r\n";
		
		if(tokens[1].startsWith(CMD_GET_ACCESSPOINTS) == true) {
			String apTokens[] = tokens[1].split("::"); 
			jsonObjStr = getAccessPoints(apTokens[1]);
		} else if(tokens[1].equalsIgnoreCase(CMD_GET_PROPERTIES) == true) {
			jsonObjStr = getProperties();
		} else {
			return null;
		}

		outStr += "Content-Length: " + jsonObjStr.length() + "\r\n";
		outStr += "\r\n";
		outStr += jsonObjStr;
		
		return outStr;
	}

	/**
	 * @return
	 */
	private String getIndexPage(String httpCommand) {
		
		String tokens[] 	= httpCommand.split(" ");
		String pageTokens[] = tokens[1].split("::");
		String networkIdStr = "var networkId";
		String retStr		= "";
		String fileStr 		= "";
		
		if(pageTokens[0].equalsIgnoreCase("/mesh.html") == false) {
			return null;
		}
		
		retStr = "HTTP/1.1 200 OK\r\n";
		retStr += "Content-Type: text/html; charset=UTF-8\r\n";
		retStr += "Cache-Control: no-cache\r\n";
		
		try {
			FileReader 		fReader = new FileReader(MFile.getWebPath() + "/mesh.html");
			BufferedReader	br = new BufferedReader(fReader);
			String 			line;
			try {
				while((line = br.readLine()) != null) {
					if(line.trim().startsWith(networkIdStr) == true) {
						fileStr += (networkIdStr + " = \"" + pageTokens[1] + "\";\r\n");
						continue;
					}
					fileStr += (line + "\r\n");
				}
			
				br.close();
				fReader.close();
				
			} catch (IOException e1) {
				System.out.println(e1);
				return null;
			}
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}

		retStr += "Content-Length: " + fileStr.length() + "\r\n";
		retStr += "\r\n";
		retStr += fileStr;
		
		return retStr;
	}

	/**
	 * @return
	 */
	private String getProperties() {
		String properties 	= "{";
		properties 			+= "\"lat\": \"38.40659\",";
		properties 			+= "\"lng\": \"-121.98115\",";
		properties 			+= "\"refresh_timeout\": \"5000\"";
		properties			+= "}";
		return properties;
	}

	/**
	 * @return
	 */
	private String getAccessPoints(String networkId) {
		String 			jsonObjStr	= "apList: [";
		Vector<IJsonAp>	apList		= meshDataProvider.getJsonApList();
	
		for(IJsonAp ap : apList) {

			if(networkId.equalsIgnoreCase(ap.getJsonNetworkId()) == false) {
				continue;
			}

			jsonObjStr += "{";
			jsonObjStr += "\"id\": " + "\"" + ap.getJsonNodeId() + "\",";
			jsonObjStr += "\"lat\": " + "\"" + ap.getJsonLatitude() + "\",";
			jsonObjStr += "\"lng\": " + "\"" + ap.getJsonLongitude() + "\",";
			jsonObjStr += "\"parentId\": " + "\"" + ap.getJsonParentNodeId() + "\",";
			jsonObjStr += "\"name\": " + "\"" + ap.getJsonName() + "\",";
			if(ap.jsonIsMobile() == 1) {
				jsonObjStr += "\"mobile\": " + "\"Yes\"";
			} else {
				jsonObjStr += "\"mobile\": " + "\"No\"";
			}
			jsonObjStr += "},";
		}
		
		if(jsonObjStr.endsWith(",") == true) {
			jsonObjStr = jsonObjStr.substring(0, jsonObjStr.length()-1);
		}
		jsonObjStr += "]";
		
		return jsonObjStr;
	}

	public void stopServer() {
		
		if(socket != null) {
			try {
				socket.close();
				this.interrupt();
			} catch (IOException e) {
				System.out.println(e);
			}
		}
	}
	
}
