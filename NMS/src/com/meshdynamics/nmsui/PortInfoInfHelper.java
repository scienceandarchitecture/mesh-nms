package com.meshdynamics.nmsui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Vector;

import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.nmsui.util.MFile;

public class PortInfoInfHelper {

	private static final int 				MIN_ACTIVE_PORTS_MD6000			= 6;
	private static final int                MIN_ACTIVE_PORTS_MD4000         = 4;
	private static final String 			STR_SUBTYPE_A					= "802.11a";
	private static final String 			STR_SUBTYPE_B					= "802.11b";
	private static final String 			STR_SUBTYPE_G					= "802.11g";
	private static final String 			STR_SUBTYPE_BG					= "802.11bg";
	
	private static final String 			STR_SUBTYPE_5GHz_N				= "802.11n_5GHz";
	private static final String 			STR_SUBTYPE_24GHz_N				= "802.11n_24GHz";
	
	private static final String 			STR_SUBTYPE_AC					= "802.11ac";
	private static final String 			STR_SUBTYPE_AN					= "802.11an";
	private static final String 			STR_SUBTYPE_ANAC				= "802.11anac";
	private static final String 			STR_SUBTYPE_BGN				    = "802.11bgn";
	
	private static final String 			STR_SUBTYPE_PSQ					= "psq";
	private static final String 			STR_SUBTYPE_PSH					= "psh";
	private static final String 			STR_SUBTYPE_PSF					= "psf";
	
	private static ComparePortInfo 	comparePortInfo = null;
	private static CompareIFName	compareIfName	= null;
	
	private static int getIfType(String ifType) {
		if(ifType.equalsIgnoreCase("dn")){
			return PortModelInfo.TYPEDOWNLINK;
		}else if(ifType.equalsIgnoreCase("up")){
			return PortModelInfo.TYPEUPLINK;
		}else if(ifType.equalsIgnoreCase("sn")){
			return PortModelInfo.TYPESCANNER;
		}else if(ifType.equalsIgnoreCase("cap")){
			return PortModelInfo.TYPECLIENTAP;
		}else if(ifType.equalsIgnoreCase("sdn")){
			return PortModelInfo.TYPESECONDDOWNLINK;
		}else if(ifType.equalsIgnoreCase("na")){
			return PortModelInfo.TYPENOTUSED;
		}else if(ifType.equalsIgnoreCase("tm")){
			return PortModelInfo.TYPE900MHZ;
		}else if(ifType.equalsIgnoreCase("ap")){
			return PortModelInfo.TYPEREGULARAP;
		}
		
		return PortModelInfo.TYPENOTUSED;
	}
	
	private static int getIfSubType(String ifFreq) {
		if(ifFreq.endsWith("anac"))
			return Mesh.PHY_SUB_TYPE_802_11_ANAC;
		else if(ifFreq.endsWith("ac"))
			return Mesh.PHY_SUB_TYPE_802_11_AC;
		else if(ifFreq.endsWith("an"))
			return Mesh.PHY_SUB_TYPE_802_11_AN;
		else if(ifFreq.endsWith("bgn"))
			return Mesh.PHY_SUB_TYPE_802_11_BGN;
		else if(ifFreq.endsWith("n_5GHz"))
			return Mesh.PHY_SUB_TYPE_802_11_5GHz_N;
		else if(ifFreq.endsWith("n_24GHz"))
			return Mesh.PHY_SUB_TYPE_802_11_24GHz_N;
		else if(ifFreq.endsWith("bg"))
			return Mesh.PHY_SUB_TYPE_802_11_B_G;
		else if(ifFreq.endsWith("a"))
			return Mesh.PHY_SUB_TYPE_802_11_A;
		else if(ifFreq.endsWith("b"))
			return Mesh.PHY_SUB_TYPE_802_11_B;
		else if(ifFreq.endsWith("g"))
			return Mesh.PHY_SUB_TYPE_802_11_G;
		else if(ifFreq.endsWith("psq"))
			return Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ;
		else if(ifFreq.endsWith("psh"))
			return Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ;
		else if(ifFreq.endsWith("psf"))
			return Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ;
		
		return Mesh.PHY_SUB_TYPE_INGNORE;
	}
	
	public static boolean readFromInfFile(String fileName, Vector<PortInfo> ports) {
		
		try {
			int i;
		    BufferedReader br = new BufferedReader(new FileReader(fileName));
			String interfaceData;
			i = 0;
			
			while((interfaceData = br.readLine()) != null) {
				
				String[] ifInfo = interfaceData.split(":");
				if(ifInfo.length != 4)
					continue;
				
				PortInfo portInfo = new PortInfo();
	
				portInfo.typeOfRadio 	= getIfType(ifInfo[0]);
				portInfo.ifName 		= ifInfo[1];
				portInfo.ifService 		= Integer.parseInt(ifInfo[2]);
				portInfo.phySubType 	= getIfSubType(ifInfo[3]);
				portInfo.ifIndex  		= i;
				ports.add(portInfo);
				
				i++;
			}
				
			br.close();
		} catch(Exception e) {
			Mesh.logException(e);
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private static boolean verifyUsageType(short ifUsageType, int typeOfRadio) {
		
		boolean ret = true;
		
		switch(typeOfRadio) {
			case PortModelInfo.TYPECLIENTAP:
			case PortModelInfo.TYPEDOWNLINK:
			case PortModelInfo.TYPESECONDDOWNLINK:
			case PortModelInfo.TYPE900MHZ:
			case PortModelInfo.TYPEREGULARAP:
				if(ifUsageType != Mesh.PHY_USAGE_TYPE_WM)
					ret = false;
				break;
			case PortModelInfo.TYPENOTUSED:
				ret = false;
				break;
			case PortModelInfo.TYPESCANNER:
				if(ifUsageType != Mesh.PHY_USAGE_TYPE_PMON && ifUsageType != Mesh.PHY_USAGE_TYPE_AMON)
					ret = false;
				break;
			case PortModelInfo.TYPEUPLINK:
				if(ifUsageType != Mesh.PHY_USAGE_TYPE_DS)
					ret = false;
				break;
		}
		
		return ret;
	}
	
	private static boolean verifyInfInformation(String filePath, IInterfaceConfiguration ifConfig) {
		
		boolean 			ret 	= true;
		Vector<PortInfo> 	ports 	= new Vector<PortInfo>();
		
		if(readFromInfFile(filePath, ports) == true) {
		
			for(int i=0;i<ifConfig.getInterfaceCount();i++) {
				
				IInterfaceInfo 	ifInfo 		= ifConfig.getInterfaceByIndex(i);
				
				if((ifInfo.getMediumType() == Mesh.PHY_TYPE_802_3))
					continue;
				
				String 			ifName 		= ifInfo.getName();
				boolean 		mismatch 	= false;
				boolean 		portFound	= false;
				
				for(PortInfo j : ports) {
					
					if(j.ifName.equalsIgnoreCase(ifName) == false)
						continue;
	
					portFound 		= true;
					short usageType = ifInfo.getUsageType();
					
					if(verifyUsageType(usageType, j.typeOfRadio) == false)
						mismatch = true;
					else if(ifInfo.getServiceType() != j.ifService)
						mismatch = true;
					else if(ifInfo.getMediumSubType() != j.phySubType)
						mismatch = true;
					
					break;
				}
				
				if(portFound == false || mismatch == true) {
					ret = false;
					break;
				}
			}
		}
		
		while(ports.size() > 0) {
			ports.remove(0);
		}
		ports.removeAllElements();
		ports = null;
		return ret;
	}
	
    /**
	 * 
	 */
	public static boolean updateInfInformation(String hwModel, String dsMacAddress, IInterfaceConfiguration ifConfig) {

		String fileName = hwModel;
		
		for(Mesh.PsuedoModels m : Mesh.PsuedoModels.values()) {
			if(m.model.equalsIgnoreCase(hwModel) == true) {
				fileName = dsMacAddress.replace(':', '_');
				break;
			}
		}
		
		String filePath = MFile.getConfigPath() + "/" + fileName + ".inf";
	    File f = new File(filePath);
	    
		if(f.exists()  == false) {
			generateInfData(filePath, ifConfig,fileName);
		} else {
			if(verifyInfInformation(filePath, ifConfig) == false)
				generateInfData(filePath, ifConfig,fileName);
		}
		
		return true;
	}
	
	private static void initPortVector(Vector<PortInfo> vPorts,String hwModel)
	{
		PortInfo portInfo;
		vPorts.setSize(MIN_ACTIVE_PORTS_MD6000);
			for(int i=0;i<MIN_ACTIVE_PORTS_MD6000;i++){
				portInfo = new PortInfo();
				portInfo.typeOfRadio  = PortModelInfo.TYPENOTUSED;
				portInfo.freqOfRadio  = STR_SUBTYPE_BG;
				portInfo.ifName 	  = "";
				portInfo.ifIndex 	  = i;
				portInfo.ifService	  = -1;	
				vPorts.set (i,portInfo);
			}
		}
private static void setFrequency(PortInfo portInfo,int subType){
		
		/*Frequency Parameter is used to specify SUBTYPE in STRING 
		 * For example freqOfRadio = " 802.11bg" */
		
		portInfo.phySubType = subType;  
		switch(subType){
		     case Mesh.PHY_SUB_TYPE_802_11_24GHz_N:
			      portInfo.freqOfRadio  =  STR_SUBTYPE_24GHz_N;
			      break;
		     case Mesh.PHY_SUB_TYPE_802_11_5GHz_N:
			      portInfo.freqOfRadio  =  STR_SUBTYPE_5GHz_N;
			      break;
		     case Mesh.PHY_SUB_TYPE_802_11_AC:
			      portInfo.freqOfRadio  =  STR_SUBTYPE_AC;
			      break;
		     case Mesh.PHY_SUB_TYPE_802_11_ANAC:
			      portInfo.freqOfRadio  =  STR_SUBTYPE_ANAC;
			      break;
		     case Mesh.PHY_SUB_TYPE_802_11_AN:
			      portInfo.freqOfRadio  =  STR_SUBTYPE_AN;
			      break;
		     case Mesh.PHY_SUB_TYPE_802_11_BGN:
			      portInfo.freqOfRadio  =  STR_SUBTYPE_BGN;
			      break;
			case  Mesh.PHY_SUB_TYPE_802_11_A:
				  portInfo.freqOfRadio  =  STR_SUBTYPE_A;
				  break;
			case  Mesh.PHY_SUB_TYPE_802_11_B:
				  portInfo.freqOfRadio  =  STR_SUBTYPE_B;
				  break;
			case  Mesh.PHY_SUB_TYPE_802_11_B_G:
				  portInfo.freqOfRadio  =  STR_SUBTYPE_BG;
				  break;
			case  Mesh.PHY_SUB_TYPE_802_11_G:
				  portInfo.freqOfRadio  = STR_SUBTYPE_G;
			 	  break;
			case  Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ:
				  portInfo.freqOfRadio  = STR_SUBTYPE_PSQ;
				  break;
			case  Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ:
				  portInfo.freqOfRadio  = STR_SUBTYPE_PSH;
				  break;
			case  Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ:
				  portInfo.freqOfRadio  = STR_SUBTYPE_PSF;
				  break;
		}
	}
	
	private static void setUsageType(PortInfo portInfo,int useType)
	{
		switch (useType) {
		case Mesh.PHY_USAGE_TYPE_DS:
			portInfo.typeOfRadio = PortModelInfo.TYPEUPLINK; 
			break;
		case Mesh.PHY_USAGE_TYPE_WM:
			portInfo.typeOfRadio = PortModelInfo.TYPEDOWNLINK;
			break;
		case Mesh.PHY_USAGE_TYPE_PMON:
		case Mesh.PHY_USAGE_TYPE_AMON:
			portInfo.typeOfRadio = PortModelInfo.TYPESCANNER;
			break;
		}
	}
	
	private static void sortInfData(Vector<PortInfo> vPorts)
	{
		if(compareIfName == null)
			compareIfName = new CompareIFName();
		
		if(comparePortInfo == null)
			comparePortInfo = new ComparePortInfo();
		
		Collections.sort(vPorts, compareIfName);
		Collections.sort(vPorts, comparePortInfo); 
	}
	
	private static void writeToInfFile(String filePath, Vector<PortInfo> vPorts) throws IOException 
	{
		File 				file;
		PortInfo 			portInfo;
		String 				output;
		
		if(vPorts == null) return;
		
		file				= new File(filePath);
		BufferedWriter bw	= new BufferedWriter(new FileWriter(file));
		
		Iterator<PortInfo> it = vPorts.iterator();
		while(it.hasNext()){
			
			output 		= "";
			portInfo 	= (PortInfo)it.next();
			
			switch(portInfo.typeOfRadio) 
			{
				case PortModelInfo.TYPEUPLINK:
					output = "up:"; 
					break;
				case PortModelInfo.TYPEDOWNLINK:
					output = "dn:";
					break;
				case PortModelInfo.TYPESCANNER:
					output = "sn:";
					break;
				case PortModelInfo.TYPENOTUSED:
					output = "na:"; 
					break;	
			}
			
			output += portInfo.ifName + ":" +
					portInfo.ifService + ":" + portInfo.freqOfRadio;
			bw.write(output);
			bw.newLine();
		}
		bw.flush(); 
		bw.close(); 
	}
	
	private static void generateInfData(String filePath, IInterfaceConfiguration ifConfig,String hwModel)
	{
		Vector<PortInfo> 		vPorts;		
		int						ifCount;
		IInterfaceInfo			currentIfInfo;

		if(ifConfig == null || ifConfig.getInterfaceCount() <= 0)
			return;
		
		vPorts	= new Vector<PortInfo>();
		ifCount = ifConfig.getInterfaceCount();
		
		initPortVector(vPorts,hwModel); 
	
		boolean dsFound 		= false;
		int 	emptyPortIndex 	= 0;
		
		for(int i=0;i<ifCount;i++) {
			
			currentIfInfo	= ifConfig.getInterfaceByIndex(i);
			if(currentIfInfo == null)
				continue;
			
			if((currentIfInfo.getMediumType() == Mesh.PHY_TYPE_802_3))
				continue;

			if(currentIfInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_DS) {
				if(dsFound == true)
					continue;
				
				dsFound = true;
			}
			
			boolean 	ifFound		= false;
			PortInfo 	portInfo  	= null;
			
			for(int j=0;j<vPorts.size();j++) 
			{
				portInfo  = (PortInfo)vPorts.get(j);
				if(portInfo.ifName.equalsIgnoreCase(currentIfInfo.getName()) == true) {
					ifFound = true;
					break;
				}
			}
			
			if(ifFound == false)
				portInfo = (PortInfo)vPorts.get(emptyPortIndex++);
			
			portInfo.ifName		= currentIfInfo.getName();
			portInfo.ifService 	= currentIfInfo.getServiceType();  
			setFrequency(portInfo,currentIfInfo.getMediumSubType());
			setUsageType(portInfo,currentIfInfo.getUsageType());

		}
		
		sortInfData(vPorts);
		try {
			writeToInfFile(filePath, vPorts);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static class ComparePortInfo implements Comparator<Object>  {  
		
		public int compare(Object o1, Object o2) {
			
			PortInfo p1 = (PortInfo)o1;
			PortInfo p2 = (PortInfo)o2;
			
			return  p1.typeOfRadio - p2.typeOfRadio;
		}  
	}  
	
	static class CompareIFName implements Comparator<Object>  {  
		public int compare(Object o1, Object o2) {
			
			PortInfo p1 = (PortInfo)o1;
			PortInfo p2 = (PortInfo)o2;
			
			if(p2.ifName == "") return -1;
			
			return  p1.ifName.compareTo(p2.ifName);
		}  
	}  
	
}
