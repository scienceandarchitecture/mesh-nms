/**
 * MeshDynamics 
 * -------------- 
 * File     : PortSettingTab.java
 * Comments : 
 * Created  : Sept 14, 2005
 * Author   : Amit 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 19  |Mar 26, 2007   | inf file dependency introduced             | Abhijit 	 |
 * ----------------------------------------------------------------------------------
 * | 18  |Mar 15, 2007   | inf file dependency removed	              | Abhijit 	 |
 * ----------------------------------------------------------------------------------
 * | 17  |Mar 12, 2007   | getPortInfoByName added                    | Imran        |
 * ----------------------------------------------------------------------------------
 * | 16  |Mar 12, 2007   | getRadioTypeModified                       | Imran        |
 * ----------------------------------------------------------------------------------
 * | 15  |Mar 08,2007    | Changes for inf file dependency removal	  | Abhijit      |
 *---------------------------------------------------------------------------------
 * | 14  |Feb 27,2007    |  removed unused code						  | Abhijit      |
 *---------------------------------------------------------------------------------
 * | 13  |Jan 08, 2007 	 | getRadioType	added		      			  | Abhishek	 |
 * ----------------------------------------------------------------------------------
 * | 12  |Jan 08, 2007 	 | get hw model is added				      | Abhishek	 |
 * ----------------------------------------------------------------------------------
 * | 11  |Nov 27, 2006 	 | not used type added					      | Abhijit 	 |
 * ----------------------------------------------------------------------------------
 * | 10  |Oct 17, 2006 	 | phy_type added 						      | Abhijit 	 |
 * ----------------------------------------------------------------------------------
 * | 9   |Mar 17, 2006 	 | Version in msg box generalised		      | Mithil 		 |
 * ----------------------------------------------------------------------------------
 * | 8   |Mar 17,2006  	 | 5.8G changes to 5G					      |Mithil		 |
 * ----------------------------------------------------------------------------------
 * | 7   |Jan 30,2006  	 | Restructuring					          |Mithil		 |
 * ----------------------------------------------------------------------------------
 * | 6   |Jan 13,2006  	 | UP link changed to Uplink		          |Mithil		 |
 * ----------------------------------------------------------------------------------
 * | 5   |Dec 29,2005  	 | Uplink Downlink made dynamic		          |Mithil		 |
 * ----------------------------------------------------------------------------------
 * | 4   |Dec 1,2005  	 | 4458 bug fix						          |Prachiti		 |
 * ----------------------------------------------------------------------------------
 * |  3  |Nov 7,2005  	 | Model number changed for INF changes       |Prachiti		 |
 *  ---------------------------------------------------------------------------------
 * |  2  |Sept 21,2005   | Added fourth radio param                   | Sneha        | 
 * ----------------------------------------------------------------------------------
 * |  1  |Sept 21, 2005  | Added methods getNumOfActiveRadio and 
 *                         getClientAPRadioFrequency	              | Sneha        |                              
 * ----------------------------------------------------------------------------------
 * |  0  |Sept 14, 2005  |  Created	                                  | Amit         |
 * ----------------------------------------------------------------------------------
 */
package com.meshdynamics.nmsui;

import java.util.Vector;

import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.nmsui.util.MFile;



/**
 * @author Prachiti Gaikwad
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */


public class PortModelInfo {
	
	public static final int TYPEUPLINK		 				= 1;
	public static final int TYPEDOWNLINK		 			= 2;
    public static final int TYPECLIENTAP		 			= 3;
    public static final int TYPESCANNER						= 4;
    public static final int TYPENOTUSED						= 5;
    public static final int TYPESECONDDOWNLINK				= 6;
    //new entries
    public static final int TYPEREGULARAP                    =7;
    public static final int TYPE900MHZ                       =8;
    
    public static final String FREQ_FIVE					= "5G";
    public static final String FREQ_TWO						= "2.4G";
    public static final String FREQ_FOUR_DOT_NINE			= "4.9G";
    
    public static final int _YELLOW							= 0;
    public static final int _ORANGE							= 1;
    public static final int _WHITE							= 2;
    public static final int _GREY							= 3;
    
	public static final String uplink 						= "BHUp Link";
	public static final String bhdwnlink 					= "BHDown Link";
	public static final String dwnlink 					    = "Down Link";
	public static final String cliAp 						= "Client AP";
	public static final String scanner 						= "Scanner";
	public static final String scddwnlink 					= "Second Downlink";
	public static final String notUsed						=" ";
	//new entries 
	public static final String regularAP                    ="Regular AP";
	public static final String n_900MHZ                     ="900MHz";
	private static final int DEFAULT_RADIO_SLOT_COUNTfor_MD6000		= 6;
	private static final int DEFAULT_RADIO_SLOT_COUNTfor_MD4000     = 4;
    private Vector<PortInfo>	vPorts;
	private String  			hwModel;
	private String 				dsMacAddress;
	
	public PortModelInfo(String Model, String dsMacAddress, IInterfaceConfiguration ifConfig) {
		vPorts	 			= new Vector<PortInfo>();
	    hwModel             = Model;
		this.dsMacAddress	= dsMacAddress;
		String filePath = getInfFilePath();
		
		for(int i=0;i<2;i++) { 
			if(loadInfInfo(filePath) == false)
				PortInfoInfHelper.updateInfInformation(Model, dsMacAddress, ifConfig);
			else
				break;
		}
		
	}
	
 	public int getNumOfRadioSlots() { 	
 			 return DEFAULT_RADIO_SLOT_COUNTfor_MD6000; 			 
	}	

    private String getInfFilePath() {  
    	
		for(Mesh.PsuedoModels m : Mesh.PsuedoModels.values()) {
			if(m.model.equalsIgnoreCase(hwModel) == true) {
				String fileName = dsMacAddress.replace(':', '_');
				return MFile.getConfigPath() + "/" + fileName + ".inf";
			}
		}
    	
		return MFile.getConfigPath() + "/" + hwModel + ".inf";
    }
	private boolean loadInfInfo(String fileName) {
    
  		if(PortInfoInfHelper.readFromInfFile(fileName, vPorts) == false)
  			return false;

  		for(PortInfo portInfo : vPorts) {
  			
  			portInfo.freqOfRadio  = "";
  			
  			switch(portInfo.phySubType){
  			   case Mesh.PHY_SUB_TYPE_802_11_AN:  			  
			   case Mesh.PHY_SUB_TYPE_802_11_A:
			   case Mesh.PHY_SUB_TYPE_802_11_AC:
			   case Mesh.PHY_SUB_TYPE_802_11_ANAC :				
			   case Mesh.PHY_SUB_TYPE_802_11_5GHz_N:				
			 		 portInfo.freqOfRadio = FREQ_FIVE;	
					 break;
			    case Mesh.PHY_SUB_TYPE_802_11_24GHz_N: 
			    case Mesh.PHY_SUB_TYPE_802_11_B:
				case Mesh.PHY_SUB_TYPE_802_11_B_G:
				case Mesh.PHY_SUB_TYPE_802_11_G:
				case Mesh.PHY_SUB_TYPE_802_11_BGN:
					portInfo.freqOfRadio = FREQ_TWO;
					break;
				case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ:
				case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ:
				case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ:
					portInfo.freqOfRadio = FREQ_FOUR_DOT_NINE;
					break;
			}
  			
			switch(portInfo.typeOfRadio) {
				case PortModelInfo.TYPESCANNER:
					portInfo.colorOfRadio = PortModelInfo._GREY;
					break;
				case PortModelInfo.TYPECLIENTAP:
					portInfo.colorOfRadio = PortModelInfo._ORANGE;
					break;
				case PortModelInfo.TYPEREGULARAP:
					portInfo.colorOfRadio = PortModelInfo._ORANGE;
					break;
				case PortModelInfo.TYPE900MHZ:
					portInfo.colorOfRadio = PortModelInfo._GREY;
					break;
				case PortModelInfo.TYPENOTUSED:
					portInfo.colorOfRadio = PortModelInfo._WHITE;
					break;
			}
  			
			if(portInfo.freqOfRadio == PortModelInfo.FREQ_FIVE){
				if(portInfo.typeOfRadio == PortModelInfo.TYPEUPLINK ||
				   portInfo.typeOfRadio == PortModelInfo.TYPEDOWNLINK ||
				   portInfo.typeOfRadio == PortModelInfo.TYPESECONDDOWNLINK ) {
					portInfo.colorOfRadio = PortModelInfo._YELLOW;
				}
			} else if(portInfo.freqOfRadio == PortModelInfo.FREQ_TWO)	{
				if( portInfo.typeOfRadio == PortModelInfo.TYPEUPLINK ||
					portInfo.typeOfRadio == PortModelInfo.TYPEDOWNLINK ||
					portInfo.typeOfRadio == PortModelInfo.TYPESECONDDOWNLINK) {
					portInfo.colorOfRadio = PortModelInfo._ORANGE;
				}
			} else if(portInfo.freqOfRadio == PortModelInfo.FREQ_FOUR_DOT_NINE)	{
				portInfo.colorOfRadio 	= PortModelInfo._ORANGE;
			}
  		}
		
  		return true;
	}
	
	public PortInfo getPortInfo(int index) {
		if(index < 0 || index >= vPorts.size())
			return null;
		
		return (PortInfo) vPorts.get(index);
	}
	
	/**
	 * @return Returns the hwModel
	 */
	public String gethwModel() {
		return hwModel;
	}
	
	public String getRadioType(String ifName){
		
		for(int i=0; i<DEFAULT_RADIO_SLOT_COUNTfor_MD6000; i++){			
			PortInfo pmInfo	=	getPortInfo(i);
			
			if(pmInfo.ifName.equals(ifName) == false)
				continue;

			switch(pmInfo.typeOfRadio) {
				case PortModelInfo.TYPEUPLINK: 			return PortModelInfo.uplink;
				case PortModelInfo.TYPEDOWNLINK: 		return PortModelInfo.dwnlink;
				case PortModelInfo.TYPESCANNER: 		return PortModelInfo.scanner;
				case PortModelInfo.TYPECLIENTAP: 		return PortModelInfo.cliAp;
				case PortModelInfo.TYPESECONDDOWNLINK: 	return PortModelInfo.scddwnlink;
				case PortModelInfo.TYPEREGULARAP:       return PortModelInfo.regularAP;
				case PortModelInfo.TYPE900MHZ:          return PortModelInfo.n_900MHZ;
				case PortModelInfo.TYPENOTUSED: 		return " ";
				default : 								return " ";
			}
		}
	return "";
	}

	public PortInfo getPortInfoByName(String ifName) {
		for(int i = 0;i<vPorts.size();i++ ){
			PortInfo pi = (PortInfo)vPorts.get(i);
			if(ifName.equalsIgnoreCase( pi.ifName)){
				return pi;
			}
		}
		return null;
	}
}




