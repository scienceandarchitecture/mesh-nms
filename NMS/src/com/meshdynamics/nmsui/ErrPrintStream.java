package com.meshdynamics.nmsui;

import java.io.OutputStream;
import java.io.PrintStream;
import com.meshdynamics.api.NMSUI.Alert;

public class ErrPrintStream extends PrintStream {
	
	private enum ALERTS {
	
		LOW			("<LOW>", 5, Alert.ALERT_LEVEL_LOW),
		MEDIUM		("<MEDIUM>", 8, Alert.ALERT_LEVEL_MEDIUM),
		HIGH		("<HIGH>", 6, Alert.ALERT_LEVEL_HIGH);
		
		private final String 	strLevel;
		private final int 		length;
		private final int		level;
		
		ALERTS(String strLevel, int length, int level) {
			this.strLevel 	= strLevel;
			this.length 	= length;
			this.level		= level;
		}
		
	}
	
	private MeshViewerUI 	meshViewerUI;
	
	public ErrPrintStream(MeshViewerUI meshViewerUI, OutputStream outputstream) {
		super(outputstream);
		this.meshViewerUI 	= meshViewerUI;
	}

	@Override
	public void println(String s) {
/*
		for(ALERTS a : ALERTS.values()) {
			
			if(a == null)
				return;
			
			if(s.startsWith(a.strLevel) == true) {
				String s1 = s.substring(a.length, s.length());
				
				String message 		= "";
				String description 	= ""; 

				int index = s1.indexOf("\n");
				if(index > 0 && index < s1.length()) {
					message 		= s1.substring(0, index);
					description 	= s1.substring(index+1);
				} else {
					message = s1;
				}
				
				Alert alertObj 		= new Alert(a.level, message, description);
				meshViewerUI.showAlert(alertObj);
				alertObj = null;
				super.println(s1);
				return;
			}
			
		}
*/		
		super.println(s);		
	}

	@Override
	public void print(String s) {
		for(ALERTS a : ALERTS.values()) {
			
			if(s.startsWith(a.strLevel) == true) {
				String s1 = s.substring(a.length, s.length());
				
				String message 		= "";
				String description 	= ""; 

				int index = s1.indexOf("\n");
				if(index > 0 && index < s1.length()) {
					message 		= s1.substring(0, index);
					description 	= s1.substring(index+1);
				} else {
					message = s1;
				}
				
				Alert alertObj 		= new Alert(a.level, message, description);
				meshViewerUI.showAlert(alertObj);
				alertObj = null;
				super.print(s1);
				return;
			}
			
		}
		
		super.print(s);		
	}


}
