package com.meshdynamics.nmsui;
/*
 * Created on Nov 14, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author Abhijit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;

public class SplashScreen {

	Display 	display;
	Image		image;
    Shell		splash;
	Label		text;
	ProgressBar	pBar;
	
	public void setMaxProgressSteps(int count) {
		pBar.setMaximum(count);
		pBar.setSelection(pBar.getSelection());
	}
	
	public void setText(final String str) {
		if(display == null)
			return;
		
		text.setText(str);
		pBar.setSelection(pBar.getSelection()+1);
	}
	
	public void show() {
		
		final MeshViewerUI ui = new MeshViewerUI(false);
		display = Display.getDefault();
		image 	= new Image(display, SplashScreen.class.getResourceAsStream("splash.png"));
	    //dsunil : To temporarily disabling modal shell
	    //splash	= new Shell(SWT.ON_TOP);
        splash	= new Shell();

        pBar	= new ProgressBar(splash, SWT.NONE);
	    text	= new Label(splash, SWT.DRAW_TRANSPARENT);
	    text.setBackground(display.getSystemColor(SWT.COLOR_LIST_BACKGROUND));
	    
		Rectangle	imageBounds = image.getBounds();
		text.setBounds(imageBounds.x,
				imageBounds.y+imageBounds.height,
				imageBounds.width,
				15);
		
		pBar.setBounds(imageBounds.x, 
						imageBounds.y+imageBounds.height+15,
						imageBounds.width-2,
						20);
		
		imageBounds.height += 35;
	    splash.setBounds(imageBounds);
	    
	    splash.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent arg0) {
				Rectangle imageBounds = image.getBounds();
				Rectangle screenBounds = splash.getClientArea();
				arg0.gc.drawImage(image, 0, 0, imageBounds.width, imageBounds.height, 0, 0, screenBounds.width, screenBounds.height);
			}});
	    
	    Rectangle splashRect = splash.getBounds();
	    Rectangle displayRect = display.getPrimaryMonitor().getBounds();
	    int x = (displayRect.width - splashRect.width) / 2;
	    int y = (displayRect.height - splashRect.height) / 2;
	    splash.setLocation(x, y);
	    splash.open();
	    splash.redraw();
	    
	    display.asyncExec(new Runnable() {
	      public void run() {

			try {
				setText("opening ui");
				ui.open(SplashScreen.this);
			} catch (Exception e) {
				e.printStackTrace();
				setText(e.getMessage());
			}
	      }
	    });
	    
	    while (splash.isDisposed() == false) {
	      if (!display.isDisposed() && !display.readAndDispatch())
	        display.sleep();
	    }
	    splash = null;
	    display = null;
	}

	public void showMeshViewerUI() {
		
		final MeshViewerUI ui = new MeshViewerUI(false);
		try {
			ui.open(null);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	}
	
	public void close() {
		splash.close();
	}
	
	public static void main(String[] args) {
		SplashScreen splash = new SplashScreen();
		if(Branding.isMeshDyanmics() == false)
			splash.showMeshViewerUI();
		else
			splash.show();
	}


}

