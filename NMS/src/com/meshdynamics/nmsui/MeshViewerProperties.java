/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshViewer.java
 * Comments : 
 * Created  : 20 oct, 2004
 * Author   : Amit Chavan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                         | Author |
 * --------------------------------------------------------------------------------
 * |  8  |Apr 13, 2007 | MeshviewerProp separated from data.dat			  |Prachiti|
 * --------------------------------------------------------------------------------
 * |  7  |Apr 12,2007  |  removed unused code, auto network open fixed	  |Abhijit |
 *---------------------------------------------------------------------------------
 * |  6  |Mar 15, 2007 |  rfEditorEnabled flag added                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  5  |Feb 27,2007  |  removed unused code							  |Abhijit |
 *---------------------------------------------------------------------------------
 * |  4  | Jan 15,2006 |  Fips flag added                                 | Imran  |
 * --------------------------------------------------------------------------------
 * |  3  | Dec 27,2006 |  Table Info property implemented		   		  | Abhijit|
 * --------------------------------------------------------------------------------
 * |  2  | Nov 27,2006 |  Autostart for IMCP/SMGP , open n/w's if auto    | Bindu  |
 * --------------------------------------------------------------------------------
 * |  1  |Oct 4, 2005  |  Removed read write for 
 *                        automaticMeshNetworkAddition                    | Sneha  |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 20, 2004 | Created                                          | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

/**
 * @author amit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MeshViewerProperties {

	public static class MGProperties {
		
		public static final int MODE_FORWARDER 		= 1;
		public static final int MODE_REMOTE_MANAGER = 2;
		
		public static int 		mode;
		public static String 	userName;
		public static String 	password;
		public static String 	server;
		public static int 		port;
		public static boolean	useSSL;
		public static boolean	ignoreLocalPackets;
		
	    static {
	    	mode 				= MODE_FORWARDER;
	    	userName 			= "default";
	    	password 			= "default";
	    	server 				= "server.meshdynamics.com";
	    	port				= 80;
	    	useSSL				= false;
	    	ignoreLocalPackets	= false;
	    	readSettings();
	    }
	    
		public static void writeSettings() {
			
			try {
				BufferWriter writer = new BufferWriter(1024);
				writer.writeInt((long)mode);
				writer.writeString(userName);
				writer.writeString(password);
				writer.writeString(server);
				writer.writeInt((long)port);
				if(useSSL == true)
					writer.writeByte((short)1);
				else
					writer.writeByte((short)0);

				if(ignoreLocalPackets == true)
					writer.writeByte((short)1);
				else
					writer.writeByte((short)0);
				
				FileOutputStream fileOut = new FileOutputStream(MFile.getConfigPath() + "\\ManagementGateway.properties");
				fileOut.write(writer.getBytes());
				fileOut.close();
			} catch(Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		public static void readSettings() {
			
			try {
				FileInputStream fileIn = new FileInputStream(MFile.getConfigPath() + "\\ManagementGateway.properties");
				int availableBytes = fileIn.available();
				byte[] buffer = new byte[availableBytes];
				fileIn.read(buffer);
				fileIn.close();
				
				BufferReader reader = new BufferReader(buffer, availableBytes);
				mode = (int) reader.readInt();
				userName = reader.readString();
				password = reader.readString();
				server = reader.readString();
				port = (int) reader.readInt();
				if(reader.readByte() == 0)
					useSSL = false;
				else
					useSSL = true;

				if(reader.readByte() == 0)
					ignoreLocalPackets = false;
				else
					ignoreLocalPackets = true;
				
			} catch(Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
	}
	
	public 	static	int 	startViewer;
	public  static 	int 	startMGClient;
	public 	static	boolean	autoOpenNetwork;
	public  static  String 	contentPath;
	public  static	int 	mobilityAdvanced;
	public  static	int		meshCommandAdvanced;
	public  static	int		rfEditorEnabled;
    public 	static 	int		getMapTilesFromWeb;

    static {
    	startViewer				= 1;
    	autoOpenNetwork 		= true;
    	contentPath				= "help.bat";
    	mobilityAdvanced		= 0;
    	meshCommandAdvanced		= 0;
    	rfEditorEnabled			= 0;
    	getMapTilesFromWeb		= 1;
    	startMGClient			= 0;
    }
    
}

