/**
 * MeshDynamics 
 * -------------- 
 * File     : EditNetworkWizard.java
 * Comments : 
 * Created  : Aug 09, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 20, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/	
package com.meshdynamics.nmsui.wizards.networkkeyedit;

import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.mesh.IMessageHandler;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.nmsui.dialogs.base.IMessage;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;

public class EditNetworkWizard extends MeshDynamicsDialog implements IMessage, IMessageHandler{

	// wizard pages
	private EditNetworkIntroductionPage	introductionPage;
	private EditNetworkSelectionPage	networkSelectionPage;
	private EditNetworkUpdateStatusPage	editKeyUpdateStatusPage;
	private RebootStatusPage			rebootStatusPage;	
	
	private final short 		PAGE_COUNT						= 4;
	
	public static final	short	INTRODUCTION_PAGE				= 0;
	public static final	short	NETWORK_SELECTION_PAGE			= 1;
	public static final	short	NETWORK_KEY_UPDATE_STATUS_PAGE	= 2;
	public static final short   REBOOT_UPDATE_STATUS			= 3;
	
	private int								rel_x;
	private int								rel_y;
	
	private short							currentPage;	
	private String 							currentNetworkName;
	private Hashtable<String, MeshNetwork>	hashNetworkList;
	private boolean 						keyUpdateDone; //tells whether key update action is done
	
	/**
	 * @param enumeration
	 * 
	 */
	public EditNetworkWizard(Enumeration<MeshNetwork> enumNetworksList, String currentNetworkName) {
		super();
		this.currentNetworkName	= currentNetworkName;
		updateLocalInformation(enumNetworksList);
		setTitle("Edit Network Key Wizard");
		setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_WIZARD);
		enableCloseButton(false);
		currentPage		= INTRODUCTION_PAGE;
		keyUpdateDone	= false;
	}
	
	
	/**
	 * @param enumNetworksList
	 */
	private void updateLocalInformation(Enumeration<MeshNetwork> enumNetworksList) {
		
		hashNetworkList	= new Hashtable<String, MeshNetwork>();
		
		while(enumNetworksList.hasMoreElements()) {
			
			MeshNetwork	meshNetwork	= (MeshNetwork) enumNetworksList.nextElement();
			if(meshNetwork == null) {
				continue;
			}
			hashNetworkList.put(meshNetwork.getNwName(), meshNetwork);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 425;
		bounds.height	= 280;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	protected void createControls(Canvas mainCnv) {
		
		rel_x	= 10;
		rel_y	= 5;
		
		introductionPage			= new EditNetworkIntroductionPage(this, mainCnv, SWT.NO, INTRODUCTION_PAGE);
		introductionPage.setBounds(rel_x, rel_y,425,280);
		enablePreviousButton(false);
		enableApplyButton(false);
		
		networkSelectionPage		= new EditNetworkSelectionPage(this, mainCnv, SWT.NO, NETWORK_SELECTION_PAGE);
		networkSelectionPage.setBounds(rel_x, rel_y,425,280);
		
		editKeyUpdateStatusPage		= new EditNetworkUpdateStatusPage(this, mainCnv, SWT.NO, NETWORK_KEY_UPDATE_STATUS_PAGE);
		editKeyUpdateStatusPage.setBounds(rel_x, rel_y,425,280);
		
		rebootStatusPage			= new RebootStatusPage(this, mainCnv, SWT.NO, REBOOT_UPDATE_STATUS);
		rebootStatusPage.setBounds(rel_x, rel_y,425,280);
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onNext()
	 */
	protected void onNext() {
		super.onNext();
		
		if(validatePagedata() == false) {
			return;
		}
		currentPage++;
		if(currentPage > PAGE_COUNT){
			return;
		}
		selectPage(currentPage);
		performPageActionOnNext(currentPage);
	}
	
	/**
	 * 
	 */
	private void performPageActionOnNext(short pageIdentifier) {
		
		switch(currentPage) {
		
		case NETWORK_SELECTION_PAGE:
			 break;
			 
		case NETWORK_KEY_UPDATE_STATUS_PAGE:
			 addRemoveMsgListener(true);
			 updateIMCPKey();
			 break;
			 
		case REBOOT_UPDATE_STATUS:
			 rebootConfiguration();
			 MeshNetwork meshNetwork = getNetwork(getSelectedNetworkName());
			 meshNetwork.setMeshNetworkKey(getNewNetworkKey());
			 break;
		}
	}
	
	
	private void performPageActionOnBack(short currentPage) {
		
		switch(currentPage) {
		
		case NETWORK_SELECTION_PAGE:
			 addRemoveMsgListener(false);
			 break;
		}
	}
	
	
	
	
	/**
	 * 
	 */
	private void addRemoveMsgListener(boolean decision) {
		
		String networkName	= getSelectedNetworkName();
		if(networkName == null) {
			return;
		}
		MeshNetwork meshNetwork	= getNetwork(networkName);
		if(meshNetwork == null) {
			return;
		}
		Enumeration<AccessPoint> apEnum = meshNetwork.getAccessPoints();
		
		while(apEnum.hasMoreElements()) {
			
			AccessPoint ap = (AccessPoint) apEnum.nextElement();
			
			if(ap == null || ap.getStatus() != AccessPoint.STATUS_ALIVE) {
				continue;
			}
			if(decision ==true) {
				ap.addMessageListener(this);
			}else {
				ap.removeMessageListener(this);
			}
			
		}
	}


	private void updateLocalMeshNetworkKey() {
		
		MeshNetwork meshNetwork	= getNetwork(getSelectedNetworkName());
		if(meshNetwork == null) {
			return;
		}
		meshNetwork.setMeshNetworkKey(networkSelectionPage.getNewKey());
	}
	/**
	 * 
	 */
	private void updateIMCPKey() {
		
		MeshNetwork meshNetwork	= getNetwork(getSelectedNetworkName());
		if(meshNetwork == null) {
			return;
		}
		meshNetwork.setMeshNetworkKeyToAps(networkSelectionPage.getNewKey());
	}


	/**
	 * @param currentPage2
	 */
	private void selectPage(short pageIdentifier) {
		
		introductionPage.setVisible(false);
		networkSelectionPage.setVisible(false);
		editKeyUpdateStatusPage.setVisible(false);
		rebootStatusPage.setVisible(false);	
		
		switch(pageIdentifier) {

		case INTRODUCTION_PAGE:
			 introductionPage.initializePageData();
			 introductionPage.setVisible(true);
			 enablePreviousButton(false);
			 enableNextButton(true);
			 enableApplyButton(false);
			 break;
			 
		case NETWORK_SELECTION_PAGE:
			 networkSelectionPage.initializePageData();
			 networkSelectionPage.setVisible(true);
			 enablePreviousButton(true);
			 enableNextButton(true);
			 enableApplyButton(false);
			 if(keyUpdateDone == true) {
			 	enableCancelButton(false);
			 }else {
			 	enableCancelButton(true);
			 }
			 break;

	    case NETWORK_KEY_UPDATE_STATUS_PAGE:
	    	 editKeyUpdateStatusPage.initializePageData();
	    	 editKeyUpdateStatusPage.setVisible(true);
	    	 enablePreviousButton(true);
	    	 String networkName	= getSelectedNetworkName();
	 		 if(networkName == null) {
				return;
	 		 }
	 		 MeshNetwork meshNetwork	= getNetwork(networkName);
	 		 if(meshNetwork == null) {
				return;
	 		 }if(meshNetwork.getAliveApCount() == 0) {
	 		 	
	 		 	 enableNextButton(false);
		 		 enableApplyButton(true);
		 		 enableCancelButton(true);
		 		
	 		 }else {
	 		 	 enableNextButton(true);
		 		 enableApplyButton(false);
		 		 enableCancelButton(false);		 		
	 		 }
			 break;
		
		case REBOOT_UPDATE_STATUS:
			 rebootStatusPage.initializePageData();
			 rebootStatusPage.setVisible(true);
			 enablePreviousButton(false);
	    	 enableNextButton(false);
	    	 enableApplyButton(true);
	    	 enableCancelButton(false);
			 break;
			
		}
	}
	
	/**
	 * @return
	 */
	private boolean validatePagedata() {
		
		switch(currentPage) {

			case INTRODUCTION_PAGE:
				 return introductionPage.validatePageData();
				 
			case NETWORK_SELECTION_PAGE:
				 return networkSelectionPage.validatePageData();
		
			case NETWORK_KEY_UPDATE_STATUS_PAGE:
				return editKeyUpdateStatusPage.validatePageData();
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onPrevious()
	 */
	protected void onPrevious() {
		super.onPrevious();
		currentPage--;
		if(currentPage < INTRODUCTION_PAGE) {
			return;
		}
		selectPage(currentPage);
		performPageActionOnBack(currentPage);
	}



	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	protected boolean onOk() {
		// TODO Auto-generated method stub
		return false;
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		
		switch(currentPage) {
		
		case NETWORK_KEY_UPDATE_STATUS_PAGE:
			 updateLocalMeshNetworkKey();
			 break;
			
		case REBOOT_UPDATE_STATUS:
			 if(rebootStatusPage.validatePageData() == false) {
			 	return false;
			 }
			 break;
			 
		}
		return true;
	}
	
	
	public String getNewNetworkKey() {
		return networkSelectionPage.getNewKey();
	}
	/**
	 * 
	 */
	private void rebootConfiguration() {
		
		MeshNetwork meshNetwork	= getNetwork(getSelectedNetworkName());
		if(meshNetwork == null) {
			return;
		}
		Enumeration<AccessPoint> enumAps	= meshNetwork.getAccessPoints(); 
		while(enumAps.hasMoreElements()) {
			AccessPoint ap = (AccessPoint) enumAps.nextElement();
			if(ap.isRunning() == true) { 
				ap.reboot();
			}
		}
	}


	/**
	 * 
	 */
	private void clearPageData() {
		
		introductionPage.clearPageData();
		networkSelectionPage.clearPageData();
		editKeyUpdateStatusPage.clearPageData();
		rebootStatusPage.clearPageData();
		addRemoveMsgListener(false);
		
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	protected boolean onCancel() {
		clearPageData();
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Returns the networkNames
	 */
	public Enumeration<MeshNetwork> getNetworks() {
		return hashNetworkList.elements();
	}
	
	
	public String getSelectedNetworkName() {
		return networkSelectionPage.getSelectedNetwork();
	}
	/**
	 * Returns the current selected network name
	 */
	public String getCurrentNetworkName() {
		return currentNetworkName;
	}
	
	
	
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMessage#showMessage(java.lang.String, int)
	 */
	public int showMessage(String message, int style) {
	
		MessageBox msg = new MessageBox(new Shell(), style);
		msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		msg.setMessage(message);
		return msg.open();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IMessageHandler#showMessage(com.meshdynamics.meshviewer.mesh.IMessageHandler.IMessageSource, java.lang.String, java.lang.String, java.lang.String)
	 */
	public void showMessage(IMessageSource msgSource, String messageHeader, String message, String progress) {
		
		if(currentPage == NETWORK_KEY_UPDATE_STATUS_PAGE) {
			editKeyUpdateStatusPage.updateStatus(msgSource, messageHeader, message);
			keyUpdateDone	= true;
		}else if(currentPage == REBOOT_UPDATE_STATUS) {
			rebootStatusPage.updateStatus(msgSource, messageHeader, message);
		}
	}


	/**
	 * @param networkName
	 * @return
	 */
	public MeshNetwork getNetwork(String networkName) {
		return (MeshNetwork)hashNetworkList.get(networkName);
	}
   
	
	
}


