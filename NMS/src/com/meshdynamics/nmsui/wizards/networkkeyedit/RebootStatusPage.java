/**
 * MeshDynamics 
 * -------------- 
 * File     : RebootStatusPage.java
 * Comments : 
 * Created  : Aug 09, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 20, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.wizards.networkkeyedit;

import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.IMessageHandler.IMessageSource;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.nmsui.wizards.WizardPage;


public class RebootStatusPage extends WizardPage{

	private Group		grp;
	
	private Text 		txtStatus;
	
	private int 		rel_x;
	private int 		rel_y;
	private String 		strStatus;

	private EditNetworkWizard			parent;
	private Hashtable<String, String>	updateApHash;

	public RebootStatusPage(EditNetworkWizard parent, Composite arg0, int arg1, short pageIdentifier) {
		super(arg0, arg1);
		this.parent			= parent;	
		createControls();
		updateApHash		= new Hashtable<String, String>();
		strStatus			= "";	
	}

	/**
	 * 
	 */
	private void createControls() {
		
		String txtLabel	= "Reboot Update Status";
		
		rel_x	= 5;
		rel_y	= 5;
		
		grp	= new Group(this, SWT.NONE);
		grp.setBounds(rel_x,rel_y,420,275);
		grp.setText(txtLabel);
		
		rel_y += 25;
		rel_x += 10;
		
		txtStatus = new Text(grp, SWT.READ_ONLY|SWT.MULTI|SWT.BORDER|SWT.V_SCROLL);
		txtStatus.setBounds(rel_x+5, rel_y, 380, 180);
	}

	public void updateStatus(IMessageSource msgSource, String messageHeader, String message) {
		updateStatus(msgSource, messageHeader +" - "+message);
	}

	/**
	 * @param msgSource
	 * @param messageHeader
	 */
	private void updateStatus(IMessageSource msgSource, String messageHeader) {
		
		String macAddress	= msgSource.getDSMacAddress();
		String srcName		= msgSource.getSrcName();
		
		updateApHash.put(macAddress, macAddress);
		strStatus += " \n";
		strStatus += messageHeader + " for \n"+	macAddress+" [ "+srcName+"]\n";
		if(txtStatus.isDisposed() == false) {
			txtStatus.setText(strStatus);
		}
	}
	
	
	public boolean validatePageData() {
		
		MeshNetwork					meshNetwork	= parent.getNetwork(parent.getSelectedNetworkName());
		Enumeration<AccessPoint>	enumAps		= meshNetwork.getAccessPoints();
		
		String 		apNames		= "";
		boolean     found		= false;
		
		while(enumAps.hasMoreElements()) {
			AccessPoint	ap	= (AccessPoint) enumAps.nextElement();
			if(ap == null) {
				continue;
			}
			String mac	= ap.getDSMacAddress();
			if(updateApHash.contains(mac) == false) {
				apNames	+= mac+ " ";
				found = true;
			}
		}
		if(found == true) {
			parent.showMessage("Reboot is not successful for "+apNames, SWT.ICON_INFORMATION);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#initializePageData()
	 */
	public boolean initializePageData() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#clearPageData()
	 */
	public boolean clearPageData() {
		// TODO Auto-generated method stub
		return false;
	}

}
