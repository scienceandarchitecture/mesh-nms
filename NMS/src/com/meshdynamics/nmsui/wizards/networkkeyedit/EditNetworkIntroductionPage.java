/**
 * MeshDynamics 
 * -------------- 
 * File     : EditNetworkIntroduction.java
 * Comments : 
 * Created  : Aug 09, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 20, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.wizards.networkkeyedit;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import com.meshdynamics.nmsui.Branding;
import com.meshdynamics.nmsui.util.MeshViewerGDIObjects;
import com.meshdynamics.nmsui.wizards.WizardPage;


public class EditNetworkIntroductionPage extends WizardPage {

	private Group		grp;
	
	private Label		lblMsg;
	
	private int 		rel_x;
	private int 		rel_y;
	
	public EditNetworkIntroductionPage(EditNetworkWizard parent, Composite arg0, int arg1, short pageIdentifier) {
		super(arg0, arg1);
		createControls();
	}

	/**
	 * 
	 */
	private void createControls() {
		
		grp	= new Group(this, SWT.NONE);
		grp.setBounds(rel_x,rel_y,420,275);
		
		final Image image = Branding.getAboutImage();
		Canvas imageCanvas = new Canvas(grp,SWT.NONE);
		imageCanvas.setBounds(1, 7, 125,265);
		imageCanvas.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		imageCanvas.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent arg0) {
				arg0.gc.drawImage(image,0,50 );
		   }
		});
		
		rel_x += 128;
	
		rel_y += 15;
		String txtTitle = "Welcome to Edit Network Key Wizard!"+SWT.CR+SWT.CR+SWT.CR+SWT.CR;
		lblMsg	= new Label(grp, SWT.NONE | SWT.READ_ONLY);
		lblMsg.setText(txtTitle);
		lblMsg.setFont((MeshViewerGDIObjects.boldVerdanaFont8));
		lblMsg.setBounds(rel_x+15, rel_y, 260,20);
		
		String txtLabel	= SWT.CR+"This wizard allows user to change the "+
		"network key."+SWT.CR+SWT.CR+
		"Network key for 'default' network cannot be changed."+SWT.CR+
		"Please make sure that all Nodes are running."+SWT.CR+SWT.CR+
		"Press the 'Next' button to continue...";
						  
		rel_y += 25;
		lblMsg	= new Label(grp, SWT.NONE | SWT.READ_ONLY);
		lblMsg.setText(txtLabel);
		lblMsg.setBounds(rel_x+15, rel_y, 270,215);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#validatePageData()
	 */
	public boolean validatePageData() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#initializePageData()
	 */
	public boolean initializePageData() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#clearPageData()
	 */
	public boolean clearPageData() {
		// TODO Auto-generated method stub
		return false;
	}

}
