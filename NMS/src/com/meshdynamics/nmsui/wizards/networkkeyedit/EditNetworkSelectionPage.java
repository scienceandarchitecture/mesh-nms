/**
 * MeshDynamics 
 * -------------- 
 * File     : EditNetworkSelectionPage.java
 * Comments : 
 * Created  : Aug 09, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 20, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.wizards.networkkeyedit;

import java.util.Enumeration;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.nmsui.wizards.WizardPage;
import com.meshdynamics.nmsui.wizards.fips.FipsSecurityHelper;


public class EditNetworkSelectionPage extends WizardPage{

	private Group		grp;
	private Group		grpEdit;
	
	private Label		lblMsg;
	private Label		lblInfo;
	private Label		lblDstNetwork;
	private Label		lblOldKey;
	private Label		lblNewKey;
	private Label		lblConfirmKey;
	
	private Text		txtOldKey;
	private Text 		txtNewKey;
	private	Text 		txtConfirmKey;
	
	private Combo		cmbNetwork;
	
	private int 		rel_x;
	private int 		rel_y;
	
	private EditNetworkWizard	parent;
	private int 				lastSelectedIndex;
	/**
	 * @param arg0
	 * @param arg1
	 */
	public EditNetworkSelectionPage(EditNetworkWizard parent, Composite arg0, int arg1, short pageIdentifier) {
		super(arg0, arg1);
		this.parent			= parent;
		lastSelectedIndex	= 0;
		createControls();
	}

	/**
	 * 
	 */
	private void createControls() {
		
		String txtLabel	= "Network Selection";
		
		grp	= new Group(this, SWT.NONE);
		grp.setBounds(rel_x,rel_y,420,275);
		grp.setText(txtLabel);
		
		rel_y += 25;
		rel_x += 10;
		
		lblDstNetwork	= new Label(grp,SWT.READ_ONLY);
		lblDstNetwork.setBounds(rel_x+5, rel_y+5, 150, 30);
		lblDstNetwork.setText(txtLabel);
		
		rel_x += 165;
		
		cmbNetwork	= new Combo(grp, SWT.READ_ONLY|SWT.BORDER);
		cmbNetwork.setBounds(rel_x, rel_y, 125, 50);
		cmbNetwork.addSelectionListener(new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				clearUI();
			}
		});
		
		rel_y += 45;
		rel_x  = 15;
		
		txtLabel	= "Drop down contains the list of networks." +SWT.CR+
					  "Select the network to change its key.";
					  
		
		lblMsg	= new Label(grp, SWT.NONE | SWT.READ_ONLY);
		lblMsg.setText(txtLabel);
		lblMsg.setBounds(rel_x, rel_y, 320,40);
		
		rel_y += 40;
		
		grpEdit	= new Group(grp, SWT.NONE);
		grpEdit.setBounds(rel_x,rel_y,390,120);
		
		rel_y = 25;
		
		lblOldKey = new Label(grpEdit,SWT.NO);
		lblOldKey.setText("Old Network Key");
		lblOldKey.setBounds(rel_x, rel_y,90,20);
		
		rel_x += 145;
		
		txtOldKey	=	new Text(grpEdit,SWT.PASSWORD|SWT.BORDER);
		txtOldKey.setBounds(rel_x, rel_y,200,20);
			
		rel_x -= 145;
		rel_y += 30;
		
		lblNewKey = new Label(grpEdit,SWT.NO);
		lblNewKey.setText("New Network Key");
		lblNewKey.setBounds(rel_x,rel_y,90,20);
		
		rel_x += 145;
		
		txtNewKey =	new Text(grpEdit,SWT.PASSWORD|SWT.BORDER);
		txtNewKey.setBounds(rel_x,rel_y,200,20);
			
		rel_x -= 145;
		rel_y += 30;
		
		lblConfirmKey = new Label(grpEdit,SWT.NO);
		lblConfirmKey.setText("Confirm Key");
		lblConfirmKey.setBounds(rel_x, rel_y,90,20);
		
		rel_x += 145;
		
		txtConfirmKey	=	new Text(grpEdit,SWT.PASSWORD|SWT.BORDER);
		txtConfirmKey.setBounds(rel_x,rel_y,200,20);
		
		rel_y += 150;
		rel_x  = 15;
		
		lblInfo	= new Label(grp, SWT.NONE | SWT.READ_ONLY);
		lblInfo.setText(txtLabel);
		lblInfo.setBounds(rel_x, rel_y, 350,30);
		
		String strMsg	= "On Next, All the Nodes from selected network will be updated with " +SWT.CR+
						  "new key. Changes will take effect after reboot.";
		lblInfo.setText(strMsg);
			
	}

	/**
	 * 
	 */
	protected void clearUI() {
		txtOldKey.setText("");
		txtConfirmKey.setText("");
		txtNewKey.setText("");
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#validatePageData()
	 */
	public boolean validatePageData() {
		
		int    		index		= cmbNetwork.getSelectionIndex();
		if(index < 0) {
			parent.showMessage("No open networks.",SWT.OK|SWT.ICON_INFORMATION);
			return false;
		}
		
		if(chkForAliveAps() == false) {
			return false;
		}
		
		String 		oldKey 		= txtOldKey.getText().trim();
    	String 		newKey 		= txtNewKey.getText().trim();
    	String 		confirmKey 	= txtConfirmKey.getText().trim();
    	
    	String 		networkName	= cmbNetwork.getItem(index);
		MeshNetwork	meshNetwork	= parent.getNetwork(networkName);
		short 		networkType	= meshNetwork.getNetworkType();
		String 		encKey		= meshNetwork.getKey();
		
	
		
		if(oldKey.equals("") == true) {
			parent.showMessage("Old Network Key not entered.",SWT.OK|SWT.ICON_INFORMATION);
			txtOldKey.setFocus();
			return false;
		}	
		
		if(networkType == MeshNetwork.NETWORK_FIPS_ENABLED) {
    		oldKey		= new String(Mesh.convertTo16Bytes(oldKey));    		
    	}
						
		if(oldKey.equalsIgnoreCase(encKey) == false){
			parent.showMessage("Old Network Key incorrect.",SWT.OK|SWT.ICON_INFORMATION);
			txtOldKey.setFocus();
			return false;
		}
		
		
		if(newKey.equals("") == true) {
			parent.showMessage("New Network Key not entered.",SWT.OK|SWT.ICON_INFORMATION);
		    txtNewKey.setFocus();
			return false;			    
		}
		
		if(networkType == MeshNetwork.NETWORK_FIPS_ENABLED) {
			if(FipsSecurityHelper.isKeyFipsCompatible(newKey) == false) {
				return false;
			}
		}
		
		
		if(confirmKey.equals("") == true) {
			parent.showMessage("Confirm Network Key not entered.",SWT.OK|SWT.ICON_INFORMATION);
			txtConfirmKey.setFocus();
			return false;			    
		}
		
		if(newKey.equals(confirmKey) == false) {
			parent.showMessage("New Network key and Confirm key mismatch.",SWT.OK|SWT.ICON_INFORMATION);
			txtNewKey.setFocus();
            return false;
		}
		
		if(networkType == MeshNetwork.NETWORK_FIPS_ENABLED) {
			newKey = new String(Mesh.convertTo16Bytes(newKey));
		}
		
		if(oldKey.equalsIgnoreCase(newKey) == true) {
			parent.showMessage("Old Network key and new network key identical.",SWT.OK|SWT.ICON_INFORMATION);
			txtNewKey.setFocus();
            return false;
		}
		return true;
	}

	/**
	 * @return
	 */
	private boolean chkForAliveAps() {
		
		String 		networkName	= parent.getSelectedNetworkName();
		if(networkName == null) {
			return false;
		}
		MeshNetwork	meshNetwork	= parent.getNetwork(networkName);
		if(meshNetwork == null) {
			return false;
		}
		Enumeration<AccessPoint>	enumAps		= meshNetwork.getAccessPoints();
		
		while(enumAps.hasMoreElements()) {
			AccessPoint	ap	= (AccessPoint) enumAps.nextElement();
			if(ap == null) {
				continue;
			}
			if(ap.isRunning() == false){
				parent.showMessage("Accesspoint "+ap.getDSMacAddress()+" is not alive.", SWT.ICON_INFORMATION);
				return false;
		    }
		}
		return true;	
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#initializePageData()
	 */
	public boolean initializePageData() {
		
		cmbNetwork.removeAll();
		clearUI();
		
		Enumeration<MeshNetwork>	enumNetworks	= parent.getNetworks();
		if(enumNetworks == null)	{
			return false;
		}
		
		while(enumNetworks.hasMoreElements()) {
			
			MeshNetwork network		= (MeshNetwork)enumNetworks.nextElement();
			String 		networkName	= network.getNwName();
			
			if(networkName.equalsIgnoreCase(Mesh.DEFAULT_MESH_ID)== true) {
				continue; 
			}
			cmbNetwork.add(networkName);
		}
		cmbNetwork.select(lastSelectedIndex);
		int selectionIndex  = cmbNetwork.getSelectionIndex();
		if(selectionIndex < 0) {
			grpEdit.setEnabled(false);
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#clearPageData()
	 */
	public boolean clearPageData() {
		txtConfirmKey.setText("");
		txtNewKey.setText("");
		cmbNetwork.select(lastSelectedIndex);
		return false;
	}

	/**
	 * @return
	 */
	public String getSelectedNetwork() {
		int index = cmbNetwork.getSelectionIndex();
		if(index < 0) {
			return null;
		}
		lastSelectedIndex	= index;
		return cmbNetwork.getItem(index);
	}

	/**
	 * @return
	 */
	public String getNewKey() {
		
		String networkName = getSelectedNetwork();
		if(networkName == null) {
			return null;
		}
		MeshNetwork	network	=  parent.getNetwork(networkName);
		if(network == null) {
			return null;
		}
		String 	newKey		= txtNewKey.getText().trim();
		short 	networkType	= network.getNetworkType();
		
		if(networkType == MeshNetwork.NETWORK_FIPS_ENABLED) {
			return new String(Mesh.convertTo16Bytes(newKey));
		}
		return newKey;
	}

}
