/**
 * MeshDynamics 
 * -------------- 
 * File     : EditNetworkUpdateStatus.java
 * Comments : 
 * Created  : Aug 09, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 20, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.wizards.networkkeyedit;

import java.util.Enumeration;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.IMessageHandler.IMessageSource;
import com.meshdynamics.nmsui.wizards.WizardPage;


public class EditNetworkUpdateStatusPage extends WizardPage {

	private Group		grp;
	private Table		tblApList;
	
	private int 		rel_x;
	private int 		rel_y;
	
	private TableEditor editor;
	
	private Button		btnSelectAll;
	private Button		btnRetry;
	
	private Label		lblMsg;		
	
	private EditNetworkWizard	parent;
	
	public class RowWidget {
		
		Button	btn 			= null;
		String	apMacAddress	= "";
		String  apName			= "";
		String  status			= "";
	}
	
	
	/**
	 * @param arg0
	 * @param arg1
	 */
	public EditNetworkUpdateStatusPage(EditNetworkWizard parent, Composite arg0, int arg1, short pageIdentifier) {
		super(arg0, arg1);
		this.parent			= parent;
		createControls();
		
	}

	private void createControls() {
		
		String txtLabel	= "Network Key Update Status";
		
		grp	= new Group(this, SWT.NONE);
		grp.setBounds(rel_x,rel_y,420,275);
		grp.setText(txtLabel);
		
		rel_y += 25;
		rel_x += 10;
		
		tblApList	= new Table(grp, SWT.BORDER |SWT.V_SCROLL|SWT.H_SCROLL);
		tblApList.setBounds(rel_x+5, rel_y, 390, 170);
		tblApList.setHeaderVisible(true);
		tblApList.setLinesVisible(true);
		
		String [] headers	= {
								"",
								"Ds Mac Address",
								"Name",
								"Status"
							  };
		
		TableColumn col [] = new TableColumn[headers.length];
		int 		index;
		
		for(index = 0; index < headers.length; index++) {
			col[index]	= new TableColumn(tblApList,SWT.BORDER| SWT.LEFT);
			col[index].setText(headers[index]);
		}
		col[0].setWidth(15);
		col[1].setWidth(100);
		col[2].setWidth(100);
		col[3].setWidth(170);
		
		rel_y +=180;
		
		btnSelectAll	= new Button(grp, SWT.CHECK);
		btnSelectAll.setText("Select All");
		btnSelectAll.setBounds(rel_x+5, rel_y, 100, 25);
		btnSelectAll.addSelectionListener(new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				updateSelection();
			}	
		});
		
		rel_x += 315; 
		
		btnRetry	= new Button(grp, SWT.PUSH);
		btnRetry.setText("Retry");
		btnRetry.setBounds(rel_x+5, rel_y, 75, 25);
		btnRetry.setToolTipText("On retry, security update will be sent to selected Nodes.");
		
		btnRetry.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				retryKeyUpdate();
			}	
		});
		
		rel_y += 30;
		rel_x = 15;
		
		lblMsg	= new Label(grp, SWT.NO);
		lblMsg.setBounds(rel_x, rel_y,320, 25);
		
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#validatePageData()
	 */
	public boolean validatePageData() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#initializePageData()
	 */
	public boolean initializePageData() {
		
		String strMsg	= "On Next, network key for  network '"+parent.getSelectedNetworkName()+"' will be updated."; 
		MeshNetwork meshNetwork = parent.getNetwork(parent.getSelectedNetworkName());
		if(meshNetwork.getAliveApCount() == 0) {
			strMsg ="On Finish, local network key for network '"+parent.getSelectedNetworkName()+"' will be updated."+ 
				SWT.CR+" 0 Nodes are present in network '"+parent.getSelectedNetworkName()+"'";
			tblApList.setVisible(false);
			btnRetry.setVisible(false);
			btnSelectAll.setVisible(false);
			lblMsg.setBounds(15, 25, 320, 50);
		}
		lblMsg.setText(strMsg);
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#clearPageData()
	 */
	public boolean clearPageData() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * 
	 */
	protected void updateSelection() {
		
		int count	= tblApList.getItemCount();
		
		for(int i = 0; i < count; i++) {
			
			TableItem	item	= tblApList.getItem(i);
			RowWidget	rowData	= (RowWidget) item.getData();
			if(rowData == null) {
				continue;
			}
			if(btnSelectAll.getSelection() == true) {
				rowData.btn.setSelection(true);
			}else {
				rowData.btn.setSelection(false);
			}
		}
	
	}
	
	/**
	 * @return
	 */
	public Enumeration<String> getSelectedApList() {
		
		int 			count		= tblApList.getItemCount();
		Vector<String>	vectApMac	=	new Vector<String>();
		
		for(int i = 0; i < count; i++) {
			
			TableItem	item	= tblApList.getItem(i);
			RowWidget	rowData	= (RowWidget) item.getData();
			if(rowData == null) {
				continue;
			}
			if(rowData.btn.getSelection() == true) {
				 vectApMac.add(rowData.apMacAddress);
				}
			}
		return vectApMac.elements();
	}
	
	/**
	 * 
	 */
	private void retryKeyUpdate() {
		Enumeration<String>	enumRetryList	= getSelectedApList();
		while(enumRetryList.hasMoreElements()) {
			
			String 		apMac		= (String) enumRetryList.nextElement();
			MeshNetwork	meshnetwork	= parent.getNetwork(parent.getSelectedNetworkName());
			meshnetwork.setMeshNetworkKeyToSelectedAp(apMac,parent.getNewNetworkKey());
		}
	}

	public void updateItemData(IMessageSource msgSource, String messageHeader) {
		
		int 	count	= tblApList.getItemCount();
		boolean	found	= false;
		
		for(int i = 0; i < count; i++) {
			TableItem	item	=	tblApList.getItem(i);
			if(item.getText(1).trim().equals(msgSource.getDSMacAddress())== true) {
				found	= true;
				item.setText(3,messageHeader);
				break;
			}
		}
		if(found == true){
			return;
		}
		editor	= new TableEditor(tblApList);
	    editor.grabHorizontal 	= editor.grabVertical = true;
	   
		TableItem item 		= new TableItem(tblApList,SWT.NONE);
		RowWidget rowItem	= new RowWidget();
		Button	  btnChk	= new Button(tblApList, SWT.CHECK);
		
		editor.setEditor(btnChk,   item, 0);
			
		rowItem.btn					= btnChk;
		rowItem.apMacAddress		= msgSource.getDSMacAddress();
		rowItem.status				= messageHeader;
		rowItem.apName				= msgSource.getSrcName();
		
		item.setText(1, rowItem.apMacAddress);
		item.setText(2, rowItem.apName);
		item.setText(3, rowItem.status);
		
		item.setData(rowItem);
	}
	
	/**
	 * @param msgSource
	 * @param messageHeader
	 * @param message
	 */
	public void updateStatus(IMessageSource msgSource, String messageHeader, String message) {
		updateItemData(msgSource, message);
	}

}
