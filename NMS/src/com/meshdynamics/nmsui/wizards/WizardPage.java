/**
 * MeshDynamics 
 * -------------- 
 * File     : WizardPage.java
 * Comments : 
 * Created  : Jul 20, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 20, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/	

package com.meshdynamics.nmsui.wizards;

import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;


public abstract class WizardPage extends Canvas{

	public WizardPage(Composite arg0, int arg1) {
		super(arg0, arg1);
	}

	public abstract boolean validatePageData();
	public abstract boolean initializePageData();
	public abstract boolean clearPageData();
}
