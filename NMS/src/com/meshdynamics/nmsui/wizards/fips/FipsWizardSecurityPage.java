/*
 * Created on Jul 23, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.wizards.fips;


import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Rectangle;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.nmsui.dialogs.frames.security.ISecurityFrameListener;
import com.meshdynamics.nmsui.dialogs.frames.security.SecurityFrame;
import com.meshdynamics.nmsui.dialogs.frames.security.SecurityFrameSelectionEvent;



/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FipsWizardSecurityPage extends FipsConfigPage implements  ISecurityFrameListener, IInterfaceFrameListener{
	
	private SecurityFrame	securityFrame;
	private InterfaceFrame	interfaceFrame;
	
	public FipsWizardSecurityPage(IFipsBook parent){
		super(parent);
		minimumBounds.width 	= 425;
		minimumBounds.height	= 510;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#createControls(org.eclipse.swt.custom.CTabFolder)
	 */
	public void createControls(CTabFolder tabFolder) {
		
		short 		disableControls;
		Rectangle	temp;
		
		super.createControls(tabFolder);
		IConfiguration configuration	= parent.getConfiguration();
		interfaceFrame	= new InterfaceFrame(canvas, this, configuration.getInterfaceConfiguration());
		temp			= interfaceFrame.getMinimumBounds();
		interfaceFrame.setBounds(10,10,temp.width,temp.height); 
			
		
		disableControls	= 0;
		disableControls	|= SecurityFrame.DISABLE_WEP;
		disableControls	|= SecurityFrame.DISABLE_WPA_ENTERPRISE;
		disableControls	|= SecurityFrame.DISABLE_TKIP;
		
		securityFrame	= new SecurityFrame(canvas, this, disableControls, true, SecurityFrame.PARENT_FIPS_WIZARD);
		temp			= securityFrame.getMinimumBounds();
		securityFrame.setBounds(10,20,temp.width,temp.height); 
		securityFrame.setFrameTitle("Security Configuration");
		securityFrame.showFrame(true);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMessage#showMessage(java.lang.String, int)
	 */
	public int showMessage(String message, int style) {
		return parent.showMessage(message, style);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#getMinimumBounds()
	 */
	public Rectangle getMinimumBounds() {
		return minimumBounds;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#initalizeLocalData()
	 */
	public void initalizeLocalData() {
		int 	radioCount;
		int 	index;
		
		IConfiguration iConfig	=	parent.getConfiguration();
		if(iConfig == null) {
			return;
		}
		
		IInterfaceConfiguration	interfaceConfig	= 	iConfig.getInterfaceConfiguration();
		if(interfaceConfig == null) {
			return;
		}
		
		radioCount	= interfaceConfig.getInterfaceCount();
		for(index = 0; index < radioCount; index++) {
			
			IInterfaceInfo	interfaceInfo	= (IInterfaceInfo)interfaceConfig.getInterfaceByIndex(index);
			if(interfaceInfo == null) {
				continue;
			}
			
			ISecurityConfiguration	securityInfo  = (ISecurityConfiguration)interfaceInfo.getSecurityConfiguration();
			if(securityInfo == null) {
				continue;
			}
			
			interfaceFrame.setRadioDataByName(interfaceInfo.getName(),securityInfo);
		}
		
		setInterfaceSecurity(0);
		interfaceFrame.selectRadio(0);
	}



	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#validateLocalData()
	 */
	public boolean validateLocalData() {
		return securityFrame.validateLocalData();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#getDataFromUIToConfiguration()
	 */
	public boolean getDataFromUIToConfiguration() {
			
		ISecurityConfiguration securityInfo	= (ISecurityConfiguration)interfaceFrame.getRadioDataByIndex(interfaceFrame.getLastSelectedRadioIndex());

		if(securityInfo == null){
			return false;
		}
		
		securityFrame.getDataFromUIToConfiguration(securityInfo);	
	
		interfaceFrame.setRadioDataByIndex(interfaceFrame.getLastSelectedRadioIndex(),null);
		interfaceFrame.setRadioDataByIndex(interfaceFrame.getLastSelectedRadioIndex(),securityInfo);
	
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.IInterfaceFrameListener#interfaceSelected(int)
	 */
	public void interfaceSelected(int radioIndex) {
		int 	userSays;
		
		if(validateLocalData() == false) {
			doNotSwitchRadio();
			return;
		}
			
		if(compairConfigurationWithUI() == false){
			
			userSays	= parent.showMessage("You have changed security settings"+SWT.LF+
					"Do you want to save these changes?",SWT.YES|SWT.CANCEL|SWT.NO|SWT.ICON_QUESTION);
			
			if(userSays == SWT.YES){
				getDataFromUIToConfiguration();
				setInterfaceSecurity(radioIndex);	
			}
			if(userSays == SWT.NO){
				revertChanges();
				setInterfaceSecurity(radioIndex);	
			}
			if(userSays == SWT.CANCEL){
				doNotSwitchRadio();
			}
		}else{
			setInterfaceSecurity(radioIndex);	
		}
		
		
	}

	/**
	 * @param radioIndex
	 */
	private void setInterfaceSecurity(int radioIndex) {
		
		ISecurityConfiguration	securityInfo	= (ISecurityConfiguration)interfaceFrame.
												  getRadioDataByIndex(radioIndex); 
		if(securityInfo == null) {
			return;
		}
		securityFrame.setSecurityValues(securityInfo);
	}

	/**
	 * 
	 */
	private void revertChanges() {
		setInterfaceSecurity(interfaceFrame.getLastSelectedRadioIndex());
	}

	/**
	 * @return
	 */
	private boolean compairConfigurationWithUI() {
		ISecurityConfiguration	secConfig = (ISecurityConfiguration) interfaceFrame.getRadioDataByIndex(interfaceFrame.getLastSelectedRadioIndex());
		if(secConfig == null) {
			return false;
		}
		return securityFrame.compareConfigurationWithUI(secConfig);
	}

	/**
	 * 
	 */
	private void doNotSwitchRadio() {
		interfaceFrame.doNotSwitchRadio();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#isFipsFullFilled()
	 */
	public boolean isFipsFulfilled() {
		if(interfaceFrame.isFipsFullfilled() == false) {
			showMessage("Fips compatible security not set for all the interfaces.", SWT.ICON_INFORMATION);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.ISecurityFrameListener#securitySelected(com.meshdynamics.meshviewer.dialogs.SecurityFrameSelectionEvent)
	 */
	public void securitySelected(SecurityFrameSelectionEvent sfsEvent) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.ISecurityFrameListener#notifyWPAPersonalKeyRegenerated()
	 */
	public void notifyWPAPersonalKeyRegenerated() {
		// TODO Auto-generated method stub
		
	}

	
}
