/**
 * MeshDynamics 
 * -------------- 
 * File     : NetworkSelectionPage.java
 * Comments : 
 * Created  : Jul 20, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 20, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/	

package com.meshdynamics.nmsui.wizards.fips;


import java.util.Enumeration;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.nmsui.wizards.WizardPage;


public class NetworkSelectionPage extends WizardPage{

	private Group		grp;
	
	private Label		lblMsg;
	private Label		lblDstNetwork;
	
	private Combo		cmbNetwork;
	
	private int 		rel_x;
	private int 		rel_y;
	
	private FipsWizardDialog parent;	
	
	public NetworkSelectionPage(FipsWizardDialog parent, Composite arg0, int arg1, short pageIdentifier) {
	
		super(arg0, arg1);
		this.parent			= parent;
		createControls();
	}
	
	private void createControls(){
		
		String txtLabel	= "Network Selection";
		
		rel_x	= 5;
		rel_y	= 5;
		
		grp	= new Group(this, SWT.NONE);
		grp.setBounds(rel_x,rel_y,360,275);
		
		grp.setText(txtLabel);
		
		rel_y += 25;
		rel_x += 10;
		
		lblDstNetwork	= new Label(grp,SWT.READ_ONLY);
		lblDstNetwork.setBounds(rel_x+5, rel_y+5, 150, 50);
		lblDstNetwork.setText(txtLabel);
		
		rel_x += 175;
		
		cmbNetwork	= new Combo(grp, SWT.READ_ONLY|SWT.BORDER);
		cmbNetwork.setBounds(rel_x, rel_y, 125, 50);
		
		rel_y += 60;
		rel_x  = 15;
		
		txtLabel	= "Dropdown contains the list of FIPS 140-2 compliant networks." +SWT.CR+SWT.CR+
					  "Select the destination network to which Nodes from network " +SWT.CR+
					  "'"+parent.getCurrentNetworkName()+"' are to be moved.";
		
		
		lblMsg	= new Label(grp, SWT.NONE | SWT.READ_ONLY);
		lblMsg.setText(txtLabel);
		lblMsg.setBounds(rel_x, rel_y, 320,100);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#validatePageData()
	 */
	public boolean validatePageData() {
		
		if(cmbNetwork.getItemCount() == 0) {
			parent.showMessage("No open FIPS 140-2 compliant networks are present.", SWT.OK|SWT.ICON_INFORMATION);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#initializePageData()
	 */
	public boolean initializePageData() {
		
		cmbNetwork.removeAll();
		
		Enumeration<MeshNetwork> enumNetworks	= parent.getNetworks();
		if(enumNetworks == null)	{
			return false;
		}
		String currentNetworkName	= parent.getCurrentNetworkName();
		
		while(enumNetworks.hasMoreElements()) {
			
			MeshNetwork network		= (MeshNetwork)enumNetworks.nextElement();
			String 		networkName	= network.getNwName();
			
			if(networkName.equalsIgnoreCase(currentNetworkName) == true) {
				continue; 
			}
			if(network.getNetworkType() == MeshNetwork.NETWORK_FIPS_ENABLED) {
				cmbNetwork.add(networkName);
			}
		}
		cmbNetwork.select(0);
		return true;
	}
	
	public String	getSelectedNetworkName(){
		int selectionIndex	= cmbNetwork.getSelectionIndex();
		if(selectionIndex < 0) {
			return null;
		}
		return cmbNetwork.getItem(selectionIndex);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#clearPageData()
	 */
	public boolean clearPageData() {
		// TODO Auto-generated method stub
		return false;
	}
}
