package com.meshdynamics.nmsui.wizards.fips;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Label;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.nmsui.dialogs.frames.security.ISecurityFrameListener;
import com.meshdynamics.nmsui.dialogs.frames.security.SecurityFrame;
import com.meshdynamics.nmsui.dialogs.frames.security.SecurityFrameSelectionEvent;

/**
 * @author imran
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FipsWizardVLANSecurityPage extends FipsConfigPage implements ISecurityFrameListener{

	
	private SecurityFrame securityFrame;
	private Label		  lblInfo;

	public FipsWizardVLANSecurityPage(IFipsBook parent){
		super(parent);
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#createControls(org.eclipse.swt.custom.CTabFolder)
	 */
	public void createControls(CTabFolder tabFolder) {
		
		short disableControls; 
		super.createControls(tabFolder);
		
		lblInfo	= new Label(canvas, SWT.NONE);
		lblInfo.setBounds(10,20,400, 50);	
		String	msg	= "Set FIPS 140-2 compliant security for the VLAN. This security configuration will"+SWT.LF+
					  "be applied to all the the vlans found for the selected Accesspoints.";
		lblInfo.setText(msg);
		disableControls	= 0;
		disableControls	|= SecurityFrame.DISABLE_WEP;
		securityFrame	= new SecurityFrame(canvas, this, disableControls, true, SecurityFrame.PARENT_FIPS_WIZARD);
		Rectangle temp = securityFrame.getMinimumBounds();
		securityFrame.setBounds(10,20,temp.width,temp.height); 
		securityFrame.setFrameTitle("Vlan Security");
		securityFrame.showFrame(true);
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#initalizeLocalData()
	 */
	public void initalizeLocalData() {
		IConfiguration	iConfiguration	= parent.getConfiguration();
		if(iConfiguration == null){
			return;
		}
		
		IVlanConfiguration	vlanConfig	= iConfiguration.getVlanConfiguration();
		if(vlanConfig == null) {
			return;
		}
		
		IVlanInfo	vlanInfo					= (IVlanInfo)vlanConfig.getVlanInfoByName("vlan");
		if(vlanInfo == null){ 
			return;
		}
		ISecurityConfiguration	vlanSecConfig	= (ISecurityConfiguration)vlanInfo.getSecurityConfiguration();
		
		lblInfo.setData(vlanSecConfig);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#selectionChanged(boolean)
	 */
	public void selectionChanged(boolean selected) {
		// TODO Auto-generated method stub
		
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#validateLocalData()
	 */
	public boolean validateLocalData() {
		return securityFrame.validateLocalData();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#getDataFromUIToConfiguration()
	 */
	public boolean getDataFromUIToConfiguration() {
		ISecurityConfiguration	secConfig	= (ISecurityConfiguration)lblInfo.getData();
		securityFrame.getDataFromUIToConfiguration(secConfig);
		return true;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#isFipsFullFilled()
	 */
	public boolean isFipsFulfilled() {
		ISecurityConfiguration	secConfig	= (ISecurityConfiguration)lblInfo.getData();
		if(FipsSecurityHelper.isSecurityFipsCompliant(secConfig) == false) {
			showMessage("Fips compatible security not set for VLAN.", SWT.ICON_INFORMATION);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.ISecurityFrameListener#securitySelected(com.meshdynamics.meshviewer.dialogs.SecurityFrameSelectionEvent)
	 */
	public void securitySelected(SecurityFrameSelectionEvent sfsEvent) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.ISecurityFrameListener#notifyWPAPersonalKeyRegenerated()
	 */
	public void notifyWPAPersonalKeyRegenerated() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMessage#showMessage(java.lang.String, int)
	 */
	public int showMessage(String message, int style) {
		return parent.showMessage(message, style);
	}

}
