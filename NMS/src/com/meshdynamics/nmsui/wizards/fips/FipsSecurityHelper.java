/**
 * MeshDynamics 
 * -------------- 
 * File     : FipsSecurityHelper.java
 * Comments : 
 * Created  : Jul 25, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 25, 2007   | created                            		  | Abhishek /	 |
 * |																  | Imran		 |	
 * ----------------------------------------------------------------------------------*/	

package com.meshdynamics.nmsui.wizards.fips;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.IPSKConfiguration;
import com.meshdynamics.meshviewer.configuration.IRadiusConfiguration;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.util.HexHelper;

public class FipsSecurityHelper {
	
	
	public static boolean isFipsCompliantConfiguration(IConfiguration configuration) {

		if(configuration == null) {
			return false;
		}
		IInterfaceConfiguration	interfaceConfiguration	= configuration.getInterfaceConfiguration();
		
		if(interfaceConfiguration == null) {
			return false;
		}
		int ifCount = interfaceConfiguration.getInterfaceCount();
		
		for(int index = 0; index < ifCount; index++) {

			IInterfaceInfo	ifInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(index);
			if(ifInfo == null) {
				continue;
			}
			if(ifInfo.getMediumType() == Mesh.PHY_TYPE_802_11 &&
			   ifInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_WM) {
				
				ISecurityConfiguration	securityInfo  = (ISecurityConfiguration)ifInfo.getSecurityConfiguration();
				if(securityInfo == null) {
					return false;
				}
				if(isSecurityFipsCompliant(securityInfo) == false) {
					return false;
				}	
			}
		}
		IVlanConfiguration	vlanConfiguration	= configuration.getVlanConfiguration();
		if(vlanConfiguration == null) {
			return false;
		}
		int vlanCount	= vlanConfiguration.getVlanCount();
		if(vlanCount <= 0) {
			return true;
		}
		for(int i = 0; i < vlanCount; i++) {
			IVlanInfo	vlanInfo	= (IVlanInfo)vlanConfiguration.getVlanInfoByIndex(i);
			if(vlanInfo == null) {
				continue;
			}
			ISecurityConfiguration	vlanSecurityConf	= (ISecurityConfiguration)vlanInfo.getSecurityConfiguration();
			if(isSecurityFipsCompliant(vlanSecurityConf) == false) {
				return false;
			}
		}	
		return true;
	}
	/**
	 * @param securityInfo
	 * @return
	 */
	public static boolean isSecurityFipsCompliant(ISecurityConfiguration securityInfo) {
		
		int enabledSecurity	=	securityInfo.getEnabledSecurity();
		
		if(enabledSecurity == ISecurityConfiguration.SECURITY_TYPE_NONE) {
			return false;
		}else if(enabledSecurity == ISecurityConfiguration.SECURITY_TYPE_WEP) {
			return false;
		}else if(enabledSecurity == ISecurityConfiguration.SECURITY_TYPE_WPA_PERSONAL) {
			return isWpaPersonalSecurityFipsCompliant((IPSKConfiguration)securityInfo.getPSKConfiguration());
		}else if(enabledSecurity == ISecurityConfiguration.SECURITY_TYPE_WPA_ENTERPRISE) {
			return isWpaEnterpriseSecurityFipsCompliant((IRadiusConfiguration)securityInfo.getRadiusConfiguration());
		}
		return false;
	}
	/**
	 * @param radiusConfiguration
	 * @return
	 */
	private static boolean isWpaEnterpriseSecurityFipsCompliant(IRadiusConfiguration radiusConfiguration) {
		
		if(radiusConfiguration == null) {
			return false;
		}
		if(radiusConfiguration.getKey().length() < 16) {
			return false;
		}
		return true;
	}
	
	/**
	 * @param configuration
	 * @return
	 */
	private static boolean isWpaPersonalSecurityFipsCompliant(IPSKConfiguration wpaPersonalconfig) {
		
		if(wpaPersonalconfig == null) {
			return false;
		}
		String strPskKey	= HexHelper.toHex(wpaPersonalconfig.getPSKKey());
		if(MeshValidations.isHex(strPskKey) == false) {
			return false;
		}
		if(strPskKey.length() != 64 ){	
			return false;
		}
		if(wpaPersonalconfig.getPSKCipherCCMP() == 0)
			return false;
		
		return true;
	}
	
	
	public static boolean isKeyFipsCompatible(String key) {
		
		if((key.length() != Mesh.FIPS_VALID_NW_KEY_SIZE)) {
			MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
		    msg.setMessage("Selected network is FIPS 140-2 compatible."+SWT.LF+
		    		"Network Key should be 32 ASCII-HEX.");
		    msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		    msg.open();							
		    return false;
		}
		
		if((MeshValidations.isHex(key) == false)) {
			MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
		    msg.setMessage("Selected network is FIPS 140-2 compatible."+SWT.LF+
		    		Mesh.FIPS_NETWORK_KEY_TOOLTIP);
		    msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		    msg.open();							
         	return false;
		}
		
		return true;
	}
}
