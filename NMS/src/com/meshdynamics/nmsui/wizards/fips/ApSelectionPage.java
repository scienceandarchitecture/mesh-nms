/**
 * MeshDynamics 
 * -------------- 
 * File     : ApSelectionPage.java
 * Comments : 
 * Created  : Jul 20, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 20, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/	

package com.meshdynamics.nmsui.wizards.fips;


import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;

import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.nmsui.wizards.WizardPage;


public class ApSelectionPage extends WizardPage{

	private Group		grp;
	
	private Label		lblMsg;
	
	private List		lstAp;
	private List		lstSelectedAps;
	
	private Button		btnAdd;
	private Button		btnRemove;
	
	private int 		rel_x;
	private int 		rel_y;

	private Hashtable<String, AccessPoint>	hashApList;
	
	private FipsWizardDialog parent;
	
	public ApSelectionPage(FipsWizardDialog parent, Composite arg0, int arg1, short pageIdentifier) {
	
		super(arg0, arg1);
		this.parent			= parent;
		createControls();
		updateLocalInformation();
	}
	
	/**
	 * 
	 */
	private void updateLocalInformation() {

		MeshNetwork		meshNetwork		= parent.getCurrentNetwork();
		MeshNetworkUI	meshNetworkUI	= parent.getCurrentNetworkUI();
		
		if(meshNetwork == null || meshNetworkUI == null) {
			return;
		}
		
		hashApList							= new Hashtable<String, AccessPoint>(); 
		Enumeration<AccessPoint> enumApList	= meshNetwork.getAccessPoints();
		boolean	groupSelectionEnabled 		= meshNetworkUI.isGroupSelectionEnabled();
		
		while(enumApList.hasMoreElements()) {
			AccessPoint	ap	= (AccessPoint) enumApList.nextElement();
			if(ap == null) {
				continue;
			}
			String apMacAddress	= ap.getDSMacAddress();
			IVersionInfo	versionInfo	= ap.getVersionInfo();
			if(versionInfo.versionGreaterThanEqualTo(IVersionInfo.MAJOR_VERSION_2,
					IVersionInfo.MINOR_VERSION_5,
					IVersionInfo.VARIANT_VERSION_34)) {
				if(ap.isRunning() == false) {
					continue;
				}
				lstAp.add(apMacAddress);
				ap.addMessageListener(parent);
				hashApList.put(apMacAddress, ap);
				
				if(groupSelectionEnabled == true) {
					if(meshNetworkUI.isGroupSelected(apMacAddress) == true)
						lstAp.select(lstAp.getItemCount()-1);
				} else
					lstAp.select(lstAp.getItemCount()-1);
			}
		}
	}

	private void createControls(){
		
		String txtLabel	= "Nodes Selection";
		
		rel_x	= 5;
		rel_y	= 5;
		
		grp	= new Group(this, SWT.NONE);
		grp.setBounds(rel_x,rel_y,360,275);
		grp.setText(txtLabel);
		
		rel_y += 15;
		rel_x += 10;
		
		Label lbl = new Label(grp, SWT.NONE);
		lbl.setText("Node's");
		lbl.setBounds(rel_x+5, rel_y, 130, 15);
		
		rel_y += 15;
		lstAp	= new List(grp, SWT.READ_ONLY|SWT.MULTI|SWT.BORDER|SWT.V_SCROLL|SWT.H_SCROLL);
		lstAp.setBounds(rel_x+5, rel_y, 130, 205);
		
		rel_x += 150;
		
		btnAdd	= new Button(grp, SWT.NONE);
		btnAdd.setText("+");
		btnAdd.setBounds(rel_x, rel_y, 30, 20);
		btnAdd.addSelectionListener(new SelectionListener(){

			public void widgetSelected(SelectionEvent arg0) {
				addToList();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}});
		
		rel_y += 30;
		
		btnRemove	= new Button(grp, SWT.NONE);
		btnRemove.setText("-");
		btnRemove.setBounds(rel_x, rel_y, 30, 20);
		btnRemove.addSelectionListener(new SelectionListener(){

			public void widgetSelected(SelectionEvent arg0) {
				removeFromList();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}});
		rel_x += 45;
		rel_y -= 30;

		rel_y -= 15;
		lbl = new Label(grp, SWT.NONE);
		lbl.setText("Selected Node's");
		lbl.setBounds(rel_x+5, rel_y, 130, 15);
		
		rel_y += 15;
		lstSelectedAps	= new List(grp, SWT.READ_ONLY|SWT.MULTI|SWT.BORDER|SWT.V_SCROLL|SWT.H_SCROLL);
		lstSelectedAps.setBounds(rel_x, rel_y, 130, 205);

		rel_x = 20;
		rel_y += 210;
		
		lblMsg	= new Label(grp, SWT.NONE | SWT.READ_ONLY);
		lblMsg.setBounds(rel_x, rel_y, 320,20);
	}

	
	/**
     * 
     */
    private void addToList() {      
        
    	if(lstAp.getItemCount() == 0) {
	    	parent.showMessage("No Node present.", SWT.ICON_INFORMATION| SWT.OK);
	        return;
	    }
    	if(lstAp.getSelectionIndex() < 0) {
	    	parent.showMessage("No Node selected.", SWT.ICON_INFORMATION| SWT.OK);
	        return;
	    }
	    if(lstAp.getSelectionCount() > 1){
	    	
	    	int[] index = lstAp.getSelectionIndices();
	    	String[] apMac = new String[index.length];
	    	for(int i=0;i<index.length;i++){
	    		apMac[i] 	= lstAp.getItem(index[i]);
	        }
	    	
	    	for(int i=0;i<apMac.length;i++){
	    		lstAp.remove(apMac[i]);
	    		lstSelectedAps.add(apMac[i]);
	    	}
	    }
	    else{
	    	int index 		= lstAp.getSelectionIndex();
	    	String apMac 	= lstAp.getItem(index);
	    	lstAp.remove(index);
	    	lstSelectedAps.add(apMac);
	    }
    }
    
    /**
     * 
     */
    private void removeFromList() {
        
    	if(lstSelectedAps.getItemCount() == 0) {
	    	parent.showMessage("No Node present.", SWT.ICON_INFORMATION| SWT.OK);
	        return;
	    }
    	if(lstSelectedAps.getSelectionIndex() < 0){
    		parent.showMessage("No Node selected.", SWT.ICON_INFORMATION| SWT.OK);
            return ;
        }
        
        if(lstSelectedAps.getSelectionCount() > 1){
        	int[] index = lstSelectedAps.getSelectionIndices();
        	String[] apMac = new String[index.length];
        	for(int i = 0; i < index.length; i++){
        		apMac[i] = lstSelectedAps.getItem(index[i]);
            }
        	
        	for(int i=0;i<apMac.length;i++){
        		lstSelectedAps.remove(apMac[i]);
        		lstAp.add(apMac[i]);
        	}
        }else{
        	int index = lstSelectedAps.getSelectionIndex();
            String apMac = lstSelectedAps.getItem(index);
            lstSelectedAps.remove(index);
            lstAp.add(apMac);
        }
    }
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#validatePageData()
	 */
	public boolean validatePageData() {
		if(lstSelectedAps.getItemCount() == 0) {
			parent.showMessage("No Node selected.", SWT.OK|SWT.ICON_INFORMATION);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#initializePageData()
	 */
	public boolean initializePageData() {

		String txtLabel	= "Selected nodes will be moved to  network " + "'" +parent.getDestinationNetworkName()+"'.";   
		lblMsg.setText(txtLabel);
	
		return true;
	}
	
	public Enumeration<AccessPoint> getSelectedApList() {
		
		int 		count			= lstSelectedAps.getItemCount();
		Hashtable<String, AccessPoint>	hashSelectedAp	= new Hashtable<String, AccessPoint>();
		
		for(int i = 0; i < count; i++) {
			String item	= lstSelectedAps.getItem(i);
			AccessPoint ap = (AccessPoint)hashApList.get(item);
			hashSelectedAp.put(item, ap);
		}
		return hashSelectedAp.elements();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#clearPageData()
	 */
	public boolean clearPageData() {
		
		Enumeration<AccessPoint>	enumApList	= hashApList.elements();
		while(enumApList.hasMoreElements()) {
			AccessPoint ap = (AccessPoint)enumApList.nextElement();
			ap.removeMessageListener(parent);
		}
		return true;
	}
}
