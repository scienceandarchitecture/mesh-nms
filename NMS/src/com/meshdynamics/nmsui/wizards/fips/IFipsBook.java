/**
 * MeshDynamics 
 * -------------- 
 * File     : IFipsBook.java
 * Comments : 
 * Created  : Jul 24, 2007 
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 24, 2007   | created                            		  | Imran	   	 |
 * ----------------------------------------------------------------------------------*/	

package com.meshdynamics.nmsui.wizards.fips;


import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.nmsui.dialogs.base.IMessage;

/**
 * @author imran
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IFipsBook extends IMessage{
	

	public IConfiguration getConfiguration();

}
