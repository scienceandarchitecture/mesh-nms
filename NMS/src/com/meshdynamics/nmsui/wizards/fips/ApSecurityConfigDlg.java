/**
 * MeshDynamics 
 * -------------- 
 * File     : ApSecurityConfigDlg.java
 * Comments : 
 * Created  : Jul 23, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 23, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/	

package com.meshdynamics.nmsui.wizards.fips;

import java.util.Enumeration;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.meshviewer.configuration.impl.Configuration;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigPage;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.dialogs.frames.security.ISecurityFrameListener;
import com.meshdynamics.nmsui.dialogs.frames.security.SecurityFrameSelectionEvent;

public class ApSecurityConfigDlg extends MeshDynamicsDialog implements ISecurityFrameListener, IFipsBook{

	private	CTabFolder			tabFolder;
	private Rectangle			minimumBounds;
	
	private IMeshConfigPage[]	pages;
	
	private Vector<String>		vectIfNames;
	private boolean				isVlansPresent;
	
	private IConfiguration		configuration;
	
	
	
	public ApSecurityConfigDlg(IConfiguration configuration, Vector<String> vectIfNames, boolean isVlansPresent) {
		super.setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK_CANCEL_APPLY);
		this.vectIfNames	= vectIfNames;
		this.isVlansPresent = isVlansPresent;
		this.configuration	= configuration;
		if(this.configuration == null)
			createConfiguration();
		
		initPages();
	}
	
	/**
	 * 
	 */
	private void createConfiguration() {
		
		configuration		= new Configuration();
		IInterfaceConfiguration	ifConfig	= configuration.getInterfaceConfiguration();
		for(int i = 0; i < vectIfNames.size(); i++) {
			ifConfig.addInterfaceByName((String)vectIfNames.get(i));
		}
		
		int ifCount	= ifConfig.getInterfaceCount();
		for(int i = 0; i < ifCount; i++) {
			IInterfaceInfo	ifInfo	= (IInterfaceInfo)ifConfig.getInterfaceByIndex(i);
			if(ifInfo == null) {
				continue;
			}
			ISecurityConfiguration	secConfiguration	= (ISecurityConfiguration)ifInfo.getSecurityConfiguration();
			if(secConfiguration == null) {
				continue;
			}
			secConfiguration.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_NONE);
		}
		
		IVlanConfiguration	vlanConfig	= configuration.getVlanConfiguration();
		IVlanInfo			vlanInfo	= (IVlanInfo)vlanConfig.addVlanInfo("vlan");
		if(vlanInfo == null) {
			return;
		}
		ISecurityConfiguration	vlanSecurityConfig	= (ISecurityConfiguration)vlanInfo.getSecurityConfiguration();
		if(vlanSecurityConfig == null) {
			return;
		}
		vlanSecurityConfig.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_NONE);
	}

	/**
	 * 
	 */
	private void initPages() {
		int pageCount;
		if(isVlansPresent == true) {
			pageCount = 2;
		} else {
			pageCount = 1; 
		}
		pages	 = new IMeshConfigPage[pageCount];
		pages[0] = new FipsWizardSecurityPage(this);
		pages[0].setCaption("Interface security");
		if(isVlansPresent == true) {
			pages[1] = new FipsWizardVLANSecurityPage(this);
			pages[1].setCaption("VLAN security");
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	protected void createControls(Canvas mainCnv) {
		
		tabFolder = new CTabFolder(mainCnv,SWT.BORDER);
		final Display display = Display.getCurrent();
		
		tabFolder.setSelectionBackground(new Color[]{display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND), 
                display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND),
                display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT), 
                display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT)},
        new int[] {10, 70, 100}, true);
	    
		tabFolder.setBounds(5, 5, minimumBounds.width, minimumBounds.height);
	
		for(int i=0; i< pages.length; i++) {
			pages[i].createControls(tabFolder);
			pages[i].initalizeLocalData();
		}
		tabFolder.setSelection(0);
		
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	protected boolean onOk() {
		for(int i = 0;  i < pages.length; i++) {
			if(pages[i].onOk() == false) {
				return false;
			}
		}
		
		for(int i = 0;  i < pages.length; i++) {
			if(pages[i].isFipsFulfilled() == false) {
				return false;
			}
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	
	protected boolean onApply() {
		
		int pageNumber	=	tabFolder.getSelectionIndex();
		
		if(pages[pageNumber].validateLocalData() == false) {
			return false;
		}
		if(pages[pageNumber].getDataFromUIToConfiguration() == false) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	protected boolean onCancel() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.ISecurityFrameListener#securitySelected(com.meshdynamics.meshviewer.dialogs.SecurityFrameSelectionEvent)
	 */
	public void securitySelected(SecurityFrameSelectionEvent sfsEvent) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.ISecurityFrameListener#notifyWPAPersonalKeyRegenerated()
	 */
	public void notifyWPAPersonalKeyRegenerated() {
		// TODO Auto-generated method stub
		
	}

	protected void modifyMinimumBounds(Rectangle bounds) {
		
		minimumBounds 			= new Rectangle(0,0,0,0);
		Rectangle pageBounds 	= null;
		for(int i=0;i<pages.length;i++) {
			
			pageBounds = pages[i].getMinimumBounds();
			
			if(pageBounds == null)
				continue;
		
			minimumBounds.width = (pageBounds.width > minimumBounds.width) ?
									pageBounds.width : minimumBounds.width;
			
			minimumBounds.height = (pageBounds.height > minimumBounds.height) ?
									pageBounds.height : minimumBounds.height;
			
		}

		minimumBounds.width 	+= 15;
		minimumBounds.height 	+= 15;

		bounds.width = (bounds.width > minimumBounds.width) ?
						bounds.width : minimumBounds.width;

		bounds.height = (bounds.height > minimumBounds.height) ?
						bounds.height : minimumBounds.height;
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.IFipsBook#getInterfaceEnum()
	 */
	public Enumeration<String> getInterfaceEnum() {
		return	vectIfNames.elements();
	}
		
	public int showMessage(String message, int swtIcon) {
		
		Shell shell = new Shell(Display.getDefault());
		MessageBox messageBox = new MessageBox(shell,SWT.MULTI|swtIcon);
		messageBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		messageBox.setMessage(message);
		return messageBox.open();
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.IFipsBook#getInterfaceCount()
	 */
	public int getInterfaceCount() {
		return vectIfNames.size();
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.IFipsBook#getConfiguration()
	 */
	public IConfiguration getConfiguration() {
		return configuration;
	}
	
}
