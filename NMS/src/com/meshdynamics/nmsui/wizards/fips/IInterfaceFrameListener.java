/**
 * MeshDynamics 
 * -------------- 
 * File     : IInterfaceFrameListener.java
 * Comments : 
 * Created  : Jul 24, 2007 
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 24, 2007   | created                            		  | Imran	   	 |
 * ----------------------------------------------------------------------------------*/	

package com.meshdynamics.nmsui.wizards.fips;


public interface IInterfaceFrameListener {
	
	void interfaceSelected(int radioIndex);
}
