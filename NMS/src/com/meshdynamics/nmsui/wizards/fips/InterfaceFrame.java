/**
 * MeshDynamics 
 * -------------- 
 * File     : InterfaceFrame.java
 * Comments : 
 * Created  : Jul 24, 2007 
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 24, 2007   | created                            		  | Imran	   	 |
 * ----------------------------------------------------------------------------------*/	

package com.meshdynamics.nmsui.wizards.fips;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;

import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;

/**
 * @author imran
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class InterfaceFrame extends Canvas {

	private Button[]				btnRadios;
	private IInterfaceFrameListener	listener;
	private Rectangle 				minimumBounds;
	private String					lastSelectedRadioName;
	private int 					lastSelectedRadioIndex;
	private IInterfaceConfiguration	interfaceConfiguration;
	
	public InterfaceFrame(Composite arg0, IInterfaceFrameListener listener, IInterfaceConfiguration	interfaceConfiguration) {
		super(arg0, SWT.NONE);
		this.listener	= listener;
		this.interfaceConfiguration	= interfaceConfiguration;
		minimumBounds   = new Rectangle(2,50,415,110);
		createControls();
	}
	
	private void createControls() {
		
		int rel_x;
		int rel_y;
		int numRadios;
		int index;
		
		rel_x		= 0;
		rel_y		= 10;
		numRadios	= 4;
		
     /*   grp 			= new Group(this,SWT.NONE);
        grp.setBounds(rel_x, rel_y, 415,100);*/ 
       
        rel_x		+= 20;
        rel_y		+= 5;
        
    	SelectionAdapter selectionAdapter = new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				if(listener == null)
					return;

				Button btn = (Button) arg0.getSource();				
				if(btn.getSelection() == false){
					saveLastSelectedRadioIndex(btn);
					return;
				}
				
				Button lastSelectedButton = btnRadios[lastSelectedRadioIndex];
				if(lastSelectedButton.equals(btn) == true) {
					return;
				}
				
				for(int i=0;i<btnRadios.length;i++) {
					if(arg0.getSource().equals(btnRadios[i])) {
						listener.interfaceSelected(i);
						break;
					}
				}
				setLastSelectedRadioIndex(getCurrentSelectedRadioIndex());
				setLastSelectedRadioName(getCurrentSelectedRadioIndex());
			}
		};
		numRadios	= interfaceConfiguration.getInterfaceCount();
		btnRadios 	= new Button[numRadios];

		IInterfaceInfo	ifInfo;
		for(index = 0; index < numRadios; index++ ) {
			ifInfo		     = (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(index);
  	    	btnRadios[index] = new Button(this,SWT.RADIO);
			btnRadios[index].setBounds(rel_x,rel_y ,100,17);
			btnRadios[index].addSelectionListener(selectionAdapter);
			btnRadios[index].setText(ifInfo.getName());
			rel_y			 += 20;
  	    }
	}

	private void saveLastSelectedRadioIndex(Button btn){
		for(int i = 0;i<btnRadios.length;i++){
			if(btnRadios[i].equals(btn)){
				this.lastSelectedRadioIndex = i;
				this.lastSelectedRadioName   = btnRadios[i].getText();
	    		
			}
		}
	}

	public int getCurrentSelectedRadioIndex(){
		int index;
		for(index = 0; index < btnRadios.length; index++) {
			if(btnRadios[index].getSelection() == true){
				return index;
			}
		}
		return 0;
	}

	public void setLastSelectedRadioIndex(int lastSelectedRadioIndex) {
		this.lastSelectedRadioIndex = lastSelectedRadioIndex;
	}
	
	public void setLastSelectedRadioName (int lastSelectedRadioIndex) {
		this.lastSelectedRadioName = btnRadios[lastSelectedRadioIndex].getText();
	}
	/**
	 * @return Returns the minimumBounds.
	 */
	public Rectangle getMinimumBounds() {
		return minimumBounds;
	}
	/**
	 * @return Returns the lastSelectedRadioName.
	 */
	public String getLastSelectedRadioName() {
		return lastSelectedRadioName;
	}
	
	 public boolean setRadioDataByIndex(int index,Object data) {
    	
    	if(index < 0 )
    		return false;
    	
    	btnRadios[index].setData(data);
		return true;
    	
    }
	 
	 public boolean setRadioDataByName(String ifName,Object data) {
    	
    	for(int i=0;i<btnRadios.length;i++) {
    		if(btnRadios[i].getText().equalsIgnoreCase(ifName)) { 
    			btnRadios[i].setData(data);
    			return true;
    		}
    	}
		return false;
    }
	 
	  public Object getRadioDataByIndex(int index) {

    	if(index < 0)
    		return null;

    	return btnRadios[index].getData();
    }
	  
	   public Object getRadiodataByName(String ifName) {
    	
    	for(int i=0;i<btnRadios.length;i++) {
    		if(btnRadios[i].getText().equalsIgnoreCase(ifName)) { 
    			return btnRadios[i].getData();
    		}
    	}
		return null;
    }
	/**
	 * @return Returns the lastSelectedRadioIndex.
	 */
	public int getLastSelectedRadioIndex() {
		return lastSelectedRadioIndex;
	}
	
	public void setRadioSelection(int index,boolean enabled) {
    	btnRadios[index].setSelection(enabled);
    }

	/**
	 * @param i
	 */
	public void selectRadio(int radioIndex) {
		int	index;
		for(index = 0; index< btnRadios.length; index++) {
			if(index == radioIndex) {
				btnRadios[index].setSelection(true) ;
			} else {
				btnRadios[index].setSelection(false);
			}
		}
	}

	/**
	 * @return
	 */
	public boolean isFipsFullfilled() {
		int	index;
		for(index = 0; index< btnRadios.length; index++) {
			ISecurityConfiguration	secConfig = (ISecurityConfiguration)btnRadios[index].getData();
			if(secConfig == null) {
				return false;
			}
			if(FipsSecurityHelper.isSecurityFipsCompliant(secConfig) == false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 */
	public void doNotSwitchRadio() {
		int radioIndex;
		
		for(radioIndex = 0; radioIndex < btnRadios.length; radioIndex++){
			if(radioIndex == getLastSelectedRadioIndex()) {
				btnRadios[radioIndex].setSelection(true);
			}
			else {
				btnRadios[radioIndex].setSelection(false);
				
			}
		}
		
	}
}
