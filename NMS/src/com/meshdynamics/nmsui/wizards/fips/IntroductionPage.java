/**
 * MeshDynamics 
 * -------------- 
 * File     : IntroductionPage.java
 * Comments : 
 * Created  : Jul 20, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 20, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/	

package com.meshdynamics.nmsui.wizards.fips;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import com.meshdynamics.nmsui.Branding;
import com.meshdynamics.nmsui.util.MeshViewerGDIObjects;
import com.meshdynamics.nmsui.wizards.WizardPage;

public class IntroductionPage extends WizardPage{

	private Group		grp;
	private Canvas 		imageCanvas;
	private Label		lblMsg;
	
	private int 		rel_x;
	private int 		rel_y;
	
	public IntroductionPage(FipsWizardDialog parent, Composite arg0, int arg1, short pageIdentifier) {
		
		super(arg0, arg1);
		createControls();
	}
	
	private void createControls(){
		
		rel_x	= 5;
		rel_y	= 5;
		
		grp	= new Group(this, SWT.NONE);
		grp.setBounds(rel_x,rel_y,360,275);
		
		final Image image = Branding.getAboutImage();
		imageCanvas = new Canvas(grp,SWT.NONE);
		imageCanvas.setBounds(1, 7, 125,265);
		imageCanvas.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		imageCanvas.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent arg0) {
				Rectangle cnvBounds = imageCanvas.getBounds();
				Rectangle imgBounds = image.getBounds();
				arg0.gc.drawImage(image,imgBounds.x, imgBounds.y, imgBounds.width, imgBounds.height,
									cnvBounds.x, cnvBounds.y, cnvBounds.width, cnvBounds.height);
		   }
		});
		
		rel_x += 128;
	
		rel_y += 15;
		String txtTitle = "FIPS 140-2 Settings"+SWT.CR+SWT.CR+SWT.CR+SWT.CR;
		lblMsg	= new Label(grp, SWT.NONE | SWT.READ_ONLY);
		lblMsg.setText(txtTitle);
		lblMsg.setFont((MeshViewerGDIObjects.boldVerdanaFont8));
		lblMsg.setBounds(rel_x, rel_y, 220,20);
		
		String txtLabel	= SWT.CR+"This wizard makes FIPS 140-2 compatible "+SWT.CR+
						  "Nodes FIPS 140-2 compliant and allows "+SWT.CR+
						  "them to  move into FIPS 140-2 compliant"+SWT.CR+
						  "network."+SWT.CR+SWT.CR+
						  "Pre-requisites for making Nodes"+SWT.CR+
						  "FIPS 140-2 compliant:"+SWT.CR+SWT.CR+
						  "1. Nodes should be FIPS 140-2 "+SWT.CR+
						  "    compatible."+SWT.CR+
						  "2. FIPS 140-2 compliant network should be "+SWT.CR+
						  "    present."+SWT.CR+SWT.CR+
						  "Press the 'Next' button to continue...";
						  
		rel_y += 25;
		lblMsg	= new Label(grp, SWT.NONE | SWT.READ_ONLY);
		lblMsg.setText(txtLabel);
		lblMsg.setBounds(rel_x, rel_y, 220,215);
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#validatePageData()
	 */
	public boolean validatePageData() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#initializePageData()
	 */
	public boolean initializePageData() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#clearPageData()
	 */
	public boolean clearPageData() {
		// TODO Auto-generated method stub
		return false;
	}

}
