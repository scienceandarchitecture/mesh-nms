/**
 * MeshDynamics 
 * -------------- 
 * File     : FipsUpdateStatusPage.java
 * Comments : 
 * Created  : Jul 24, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 24, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/	

package com.meshdynamics.nmsui.wizards.fips;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.mesh.IMessageHandler.IMessageSource;
import com.meshdynamics.nmsui.wizards.WizardPage;

/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FipsUpdateStatusPage extends WizardPage{
	
	private Group				grp;
	private Text				txtStatus;
	
	private int 				rel_x;
	private int 				rel_y;
	private	String				strStatus;
	
	public FipsUpdateStatusPage(FipsWizardDialog dialog, Composite arg0, int arg1, short pageIdentifier) {
		super(arg0, arg1);
		strStatus			= "";
		createControls();
	}

	/**
	 * 
	 */
	private void createControls() {
		
		String txtLabel	= "FIPS 140-2 Update Status";
		
		rel_x	= 5;
		rel_y	= 5;
		
		grp	= new Group(this, SWT.NONE);
		grp.setBounds(rel_x,rel_y,360,275);
		grp.setText(txtLabel);
		
		rel_y += 25;
		rel_x += 10;
		
		txtStatus = new Text(grp, SWT.READ_ONLY|SWT.MULTI|SWT.BORDER|SWT.V_SCROLL);
		txtStatus.setBounds(rel_x+5, rel_y, 320, 180);
		
	
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#validatePageData()
	 */
	public boolean validatePageData() {
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#initializePageData()
	 */
	public boolean initializePageData() {
	//	strStatus	= "";
	//	txtStatus.setText("");
		return true;
	}
	
	public void updateStatus(IMessageSource msgSource, String messageHeader, String message) {
		updateStatus(msgSource, messageHeader +" - "+message);
	}

	/**
	 * @param msgSource
	 * @param messageHeader
	 */
	private void updateStatus(IMessageSource msgSource, String messageHeader) {
		strStatus += " \n";
		strStatus += messageHeader + " for \n"+
		msgSource.getDSMacAddress()+" [ "+msgSource.getSrcName()+"]\n";
		if(txtStatus.isDisposed() == false) {
			txtStatus.setText(strStatus);
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#clearPageData()
	 */
	public boolean clearPageData() {
		// TODO Auto-generated method stub
		return false;
	}

}
