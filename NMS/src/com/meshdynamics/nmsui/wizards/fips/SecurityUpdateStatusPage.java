
package com.meshdynamics.nmsui.wizards.fips;

import java.util.Enumeration;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.IMessageHandler.IMessageSource;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.nmsui.wizards.WizardPage;
import com.meshdynamics.util.MacAddress;

public class SecurityUpdateStatusPage extends WizardPage {

	private Group		grp;
	private Table		tblApList;
	
	private int 		rel_x;
	private int 		rel_y;
	
	private Button		btnRetry;
	
	private Label		lblFipsUpdate;
	
	private FipsWizardDialog parent;
	
	public class RowWidget {
		
		AccessPoint	ap	= null;
		String  status	= "";
	}
	
	
	public SecurityUpdateStatusPage(FipsWizardDialog dialog, Composite arg0, int arg1, short pageIdentifier) {
		super(arg0, arg1);
		this.parent			= dialog;	 
		createControls();
	}

	/**
	 * 
	 */
	private void createControls() {
		
		String txtLabel	= "Security Update Status";
		
		rel_x	= 5;
		rel_y	= 5;
		
		grp	= new Group(this, SWT.NONE);
		grp.setBounds(rel_x,rel_y,360,275);
		grp.setText(txtLabel);
		
		rel_y += 25;
		rel_x += 10;
		
		tblApList	= new Table(grp, SWT.MULTI|SWT.BORDER |SWT.V_SCROLL|SWT.H_SCROLL);
		tblApList.setBounds(rel_x+5, rel_y, 320, 170);
		tblApList.setHeaderVisible(true);
		tblApList.setLinesVisible(true);
		tblApList.addKeyListener(new KeyAdapter(){

			@Override
			public void keyPressed(KeyEvent keyevent) {
				if(keyevent.keyCode == 'a') {
					if((keyevent.stateMask & SWT.CTRL) == 0)
						return;
					tblApList.selectAll();
				}
			}

		});

		
		String [] headers	= {
								"Ds Mac Address",
								"Status"
							  };
		
		TableColumn col [] = new TableColumn[headers.length];
		int 		index;
		
		for(index = 0; index < headers.length; index++) {
			col[index]	= new TableColumn(tblApList,SWT.BORDER| SWT.LEFT);
			col[index].setText(headers[index]);
		}
		col[0].setWidth(135);
		col[1].setWidth(180);
		
		rel_y +=180;
		rel_x += 245; 
		
		btnRetry	= new Button(grp, SWT.PUSH);
		btnRetry.setText("Retry");
		btnRetry.setBounds(rel_x+5, rel_y, 75, 25);
		btnRetry.setToolTipText("On retry, security update will be sent to selected Nodes.");
		
		btnRetry.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				retrySeurityUpdate();
			}	
		});
		
		rel_x = 15;
		rel_y += 30;
		
		lblFipsUpdate	= new Label(grp, SWT.READ_ONLY);
		lblFipsUpdate.setText("On Update, FIPS 140-2 settings will be updated for selected "+SWT.CR
							  +"Nodes.");
		lblFipsUpdate.setBounds(rel_x+5, rel_y, 300, 30);
		
	}

	/**
	 * 
	 */
	private void retrySeurityUpdate() {
		
		if(validatePageData() == false) {
			return;
		}
		Enumeration<String>	enumRetryList	= getSelectedApList();
		while(enumRetryList.hasMoreElements()) {
			
			String 		apMac		= (String) enumRetryList.nextElement();
			MeshNetwork	meshnetwork	= parent.getCurrentNetwork();
			meshnetwork.updateConfiguration(apMac, PacketFactory.AP_CONFIGURATION_INFO);
			MacAddress apMacAddr	= new MacAddress();
			apMacAddr.setBytes(apMac);
			AccessPoint	ap			= meshnetwork.getAccessPoint(apMacAddr);
			IConfiguration	config	= ap.getConfiguration();
			if(config == null) {
				continue;
			}
			IVlanConfiguration	vlanConfig	= config.getVlanConfiguration();
			if(vlanConfig == null) {
				continue;
			}
			if(vlanConfig.getVlanCount() > 0) {
				meshnetwork.updateConfiguration(apMac, PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			}
		}
	}

	public void updateItemData(IMessageSource msgSource, String messageHeader) {
		
		int 	count	= tblApList.getItemCount();
		boolean	found	= false;
		
		for(int i = 0; i < count; i++) {
			TableItem	item	=	tblApList.getItem(i);
			if(item.getText(0).trim().equals(msgSource.getDSMacAddress())== true) {
				found	= true;
				item.setText(1,messageHeader);
				break;
			}
		}
		if(found == true){
			return;
		}
		
		String 						macAddress 	= msgSource.getDSMacAddress(); 
		Enumeration<AccessPoint> 	apEnum 		= parent.getSelectedApList();
		AccessPoint					ap			= null;
		while(apEnum.hasMoreElements()) {
			AccessPoint nxtAp = apEnum.nextElement();
			if(nxtAp == null)
				continue;
			
			if(nxtAp.getDSMacAddress().equalsIgnoreCase(macAddress) == true) {
				ap = nxtAp;
				break;
			}
		}
		
		if(ap == null)
			return;
		
		TableItem item 		= new TableItem(tblApList,SWT.NONE);
		RowWidget rowItem	= new RowWidget();
		rowItem.ap					= ap;
		rowItem.status				= messageHeader;
		
		item.setText(0, macAddress);
		item.setText(1, rowItem.status);
		tblApList.selectAll();
		item.setData(rowItem);
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#validatePageData()
	 */
	public boolean validatePageData() {
		Enumeration<String> enumSelectedApList	= getSelectedApList();
		if(enumSelectedApList.hasMoreElements() == false) {
			parent.showMessage("No Node selected.", SWT.ICON_INFORMATION | SWT.OK);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#initializePageData()
	 */
	public boolean initializePageData() {
		return false;
	}

	/**
	 * @param msgSource
	 * @param messageHeader
	 * @param message
	 */
	public void updateStatus(IMessageSource msgSource, String messageHeader, String message) {
		updateItemData(msgSource, message);
	}

	/**
	 * @return
	 */
	public Enumeration<String> getSelectedApList() {

		Vector<String>	vectApMac		= new Vector<String>();
		TableItem[] 	selectedItems	= null;
		
		if(tblApList.getSelectionCount() <= 0)
			selectedItems = tblApList.getItems();
		else
			selectedItems = tblApList.getSelection();
		
		for(TableItem	item : selectedItems) {
			
			RowWidget	rowData	= (RowWidget) item.getData();
			if(rowData == null) {
				continue;
			}
			vectApMac.add(rowData.ap.getDSMacAddress());
		}
		return vectApMac.elements();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#clearPageData()
	 */
	public boolean clearPageData() {
		// TODO Auto-generated method stub
		return false;
	}

	
}
