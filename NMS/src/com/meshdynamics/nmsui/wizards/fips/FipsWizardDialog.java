/**
 * MeshDynamics 
 * -------------- 
 * File     : FipsWizardDialog.java
 * Comments : 
 * Created  : Jul 20, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 20, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/	
package com.meshdynamics.nmsui.wizards.fips;



import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.mesh.IMessageHandler;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.nmsui.MeshViewerUI;
import com.meshdynamics.nmsui.dialogs.base.IMessage;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.util.MacAddress;

public class FipsWizardDialog extends MeshDynamicsDialog implements IMessage, IMessageHandler{

	// wizard pages
	private IntroductionPage					introductionPage;
	private NetworkSelectionPage				networkSelectionPage;
	private ApSelectionPage						apSelectionPage;
	private SecurityPage						securitySettingsPage;
	private SecurityUpdateStatusPage			securityUpdateStatusPage;
	private FipsUpdateStatusPage				fipsUpdateStatusPage;
	
	private final short 		PAGE_COUNT						= 6;
	
	public static final	short	INTRODUCTION_PAGE				= 0;
	public static final	short	NETWORK_SELECTION_PAGE			= 1;
	public static final	short	AP_SELECTION_PAGE				= 2;
	public static final	short	SECURITY_SETTINGS_PAGE			= 3;
	public static final	short	SECURITY_UPDATE_STATUS_PAGE		= 4;
	public static final short	FIPS_STATUS_PAGE				= 5;
	
	private int								rel_x;
	private int								rel_y;
	
	private short							currentPage;	
	
	private Hashtable<String, MeshNetwork>	hashNetworkList;
	private String							currentNetworkName;
	private MeshViewerUI					meshViewerUI;
	
	/**
	 * @param enumeration
	 * 
	 */
	public FipsWizardDialog(MeshViewerUI meshViewerUI, Enumeration<MeshNetwork> enumNetworksList, String currentNetworkName) {
		super();
		this.meshViewerUI		= meshViewerUI;
		this.currentNetworkName	= currentNetworkName;
		updateLocalInformation(enumNetworksList);
		setTitle("FIPS 140-2 Wizard");
		setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_WIZARD);
		currentPage	= INTRODUCTION_PAGE;
	}
	/**
	 * @param enumNetworksList
	 */
	private void updateLocalInformation(Enumeration<MeshNetwork> enumNetworksList) {
		
		hashNetworkList	= new Hashtable<String, MeshNetwork>();
		
		while(enumNetworksList.hasMoreElements()) {
			
			MeshNetwork	meshNetwork	= (MeshNetwork) enumNetworksList.nextElement();
			if(meshNetwork == null) {
				continue;
			}
			hashNetworkList.put(meshNetwork.getNwName(), meshNetwork);
		}
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 365;
		bounds.height	= 280;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	protected void createControls(Canvas mainCnv) {
		
		rel_x	= 5;
		rel_y	= 5;
		
		introductionPage			= new IntroductionPage(this, mainCnv, SWT.NO, INTRODUCTION_PAGE);
		introductionPage.setBounds(rel_x, rel_y,380,280);
		ContextHelp.addContextHelpHandlerEx(introductionPage,contextHelpEnum.DLGFIPSWIZARD);
		enablePreviousButton(false);
		enableApplyButton(false);
		
		networkSelectionPage		= new NetworkSelectionPage(this, mainCnv, SWT.NO, NETWORK_SELECTION_PAGE);
		networkSelectionPage.setBounds(rel_x, rel_y,380,280);
		ContextHelp.addContextHelpHandlerEx(networkSelectionPage,contextHelpEnum.DLGFIPSWIZARD);
		
		apSelectionPage				= new ApSelectionPage(this, mainCnv, SWT.NO, AP_SELECTION_PAGE);
		apSelectionPage.setBounds(rel_x, rel_y,380,280);
		ContextHelp.addContextHelpHandlerEx(apSelectionPage,contextHelpEnum.DLGFIPSWIZARD);
		
		securitySettingsPage		= new SecurityPage(this, mainCnv, SWT.NO, SECURITY_SETTINGS_PAGE);
		securitySettingsPage.setBounds(rel_x, rel_y,380,280);
		ContextHelp.addContextHelpHandlerEx(securitySettingsPage,contextHelpEnum.DLGFIPSWIZARD);
		
		securityUpdateStatusPage	= new SecurityUpdateStatusPage(this, mainCnv, SWT.NO, SECURITY_UPDATE_STATUS_PAGE);
		securityUpdateStatusPage.setBounds(rel_x, rel_y,380,280);
		ContextHelp.addContextHelpHandlerEx(securityUpdateStatusPage,contextHelpEnum.DLGFIPSWIZARD);
		
		fipsUpdateStatusPage		= new FipsUpdateStatusPage(this, mainCnv, SWT.NO, FIPS_STATUS_PAGE);
		fipsUpdateStatusPage.setBounds(rel_x, rel_y,380,280);
		ContextHelp.addContextHelpHandlerEx(fipsUpdateStatusPage,contextHelpEnum.DLGFIPSWIZARD);
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onNext()
	 */
	protected void onNext() {
		super.onNext();
		
		if(validatePagedata() == false) {
			return;
		}
		currentPage++;
		if(currentPage > PAGE_COUNT){
			return;
		}
		selectPage(currentPage);
		performPageAction(currentPage);
	}
	
	/**
	 * 
	 */
	private void performPageAction(short pageIdentifier) {
		
		switch(currentPage) {
		 
		case SECURITY_UPDATE_STATUS_PAGE:
			 updateSecurityConfiguration();
			 break;
			
		case FIPS_STATUS_PAGE:	
			break;
		}
	}
	
	
	/**
	 *  Sends fips security update to selected aps
	 */
	private void updateSecurityConfiguration() {
		
		Enumeration<AccessPoint> enumApList	= getSelectedApList();
		while(enumApList.hasMoreElements()) {
			AccessPoint	ap	= (AccessPoint) enumApList.nextElement();
			if(ap == null) {
				continue;
			}
			ap.updateChangedConfiguration();
		}
	}
	
	/**
	 *  Sends fips update and moves the board in to destination fips network
	 *  and reboots the board
	 */
	private void updateFipsConfiguration() {
		
		Enumeration<String> 	enumApUpdateList	= securityUpdateStatusPage.getSelectedApList();
		
		while(enumApUpdateList.hasMoreElements()) {
			
			String 						strMacAddress	= (String)enumApUpdateList.nextElement();
			Enumeration<AccessPoint> 	enumApList		= getSelectedApList();
			
			while(enumApList.hasMoreElements()) {
				
				AccessPoint	ap = (AccessPoint)enumApList.nextElement();
				if(ap.getDSMacAddress().equals(strMacAddress) == true) {
					moveApToDstFipsNetwork(ap.getDsMacAddress());
					ap.setFipsMode(true);
					break;
				}
			}
				
		}
	}
	
	
	/**
	 * @param ap
	 */
	private void moveApToDstFipsNetwork(MacAddress apMacAddress) {
		
		String dstNetworkName		= getDestinationNetworkName();
		MeshNetwork dstNetwork		= (MeshNetwork) hashNetworkList.get(dstNetworkName);
		MeshNetwork currentNetwork	= (MeshNetwork) hashNetworkList.get(currentNetworkName);
		boolean success				= currentNetwork.moveNode(apMacAddress, dstNetworkName, dstNetwork.getKey(), dstNetwork.getNetworkType());
		
		if(success == true) {
			//currentNetwork.rebootAccesspoint(apMacAddress);
		}
	
	}
	
	/**
	 * @param currentPage2
	 */
	private void selectPage(short pageIdentifier) {
		
		introductionPage.setVisible(false);
		networkSelectionPage.setVisible(false);
		apSelectionPage.setVisible(false);
		securitySettingsPage.setVisible(false);
		securityUpdateStatusPage.setVisible(false);
		fipsUpdateStatusPage.setVisible(false);
		
		switch(pageIdentifier) {

		case INTRODUCTION_PAGE:
			 introductionPage.initializePageData();
			 introductionPage.setVisible(true);
			 enablePreviousButton(false);
			 enableApplyButton(false);
			 break;
			 
		case NETWORK_SELECTION_PAGE:
			 networkSelectionPage.initializePageData();
			 networkSelectionPage.setVisible(true);
			 enablePreviousButton(true);
			 enableApplyButton(false);
			 break;
			 
		case AP_SELECTION_PAGE:
			apSelectionPage.initializePageData();
			apSelectionPage.setVisible(true);
			enableApplyButton(false);
			break;
			
		case SECURITY_SETTINGS_PAGE:
			securitySettingsPage.initializePageData();
			securitySettingsPage.setVisible(true);
			enableApplyButton(false);
			enableNextButton(true);
			break;
			
		case SECURITY_UPDATE_STATUS_PAGE:
			securityUpdateStatusPage.initializePageData();
			securityUpdateStatusPage.setVisible(true);
			enableApplyButton(true);
			enableNextButton(false);
			setApplyButtonText("Update");
			break;
		
       case FIPS_STATUS_PAGE:
       		fipsUpdateStatusPage.initializePageData();
       		fipsUpdateStatusPage.setVisible(true);
       		enableNextButton(false);
       		setApplyButtonText("Finish");
       		enableCancelButton(false);
       		break;
			 
		}
	}
	
	/**
	 * @return
	 */
	private boolean validatePagedata() {
		
		switch(currentPage) {

			case INTRODUCTION_PAGE:
				 return introductionPage.validatePageData();
				 
			case NETWORK_SELECTION_PAGE:
				 return networkSelectionPage.validatePageData();
				 
			case AP_SELECTION_PAGE:
				return apSelectionPage.validatePageData();
				
			case SECURITY_SETTINGS_PAGE:
				return securitySettingsPage.validatePageData();
				
			case SECURITY_UPDATE_STATUS_PAGE:	
				return securityUpdateStatusPage.validatePageData();
				
			case FIPS_STATUS_PAGE:
				return fipsUpdateStatusPage.validatePageData();
		}
		
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onPrevious()
	 */
	protected void onPrevious() {
		super.onPrevious();
		currentPage--;
		if(currentPage < INTRODUCTION_PAGE) {
			return;
		}
		selectPage(currentPage);
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	protected boolean onOk() {
		// TODO Auto-generated method stub
		return false;
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		
		switch(currentPage) {
		
		case SECURITY_UPDATE_STATUS_PAGE:
			if(securityUpdateStatusPage.validatePageData() == true) {
				onNext();
				updateFipsConfiguration();
			}
			break;
			
		case FIPS_STATUS_PAGE:
			rebootUpdatedNodes();
			clearPageData();
			return true;
		}
		return false;
	}
	
	private void rebootUpdatedNodes() {
/*		
		Enumeration enumApList	= getSelectedApList();
		
		while(enumApList.hasMoreElements()) {
			
			AccessPoint	ap = (AccessPoint)enumApList.nextElement();
			if(ap.getRuntimeApConfiguration().isRebootRequired() == true) {
				ap.reboot();
			}
		}
*/		
	}
	
	/**
	 * 
	 */
	private void clearPageData() {
		
		introductionPage.clearPageData();
		networkSelectionPage.clearPageData();
		apSelectionPage.clearPageData();
		securitySettingsPage.clearPageData();
		securityUpdateStatusPage.clearPageData();
		fipsUpdateStatusPage.clearPageData();
		
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	protected boolean onCancel() {
		clearPageData();
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Returns the networkNames
	 */
	Enumeration<MeshNetwork> getNetworks() {
		return hashNetworkList.elements();
	}
	
	MeshNetwork getCurrentNetwork() {
		return (MeshNetwork)hashNetworkList.get(currentNetworkName);
	}
	
	/**
	 * Returns the current selected network name
	 */
	String getCurrentNetworkName() {
		return currentNetworkName;
	}
	
	/**
	 * Returns the selected ap list from apSelectionPage
	 * where aps to be made fips enabled are selected from current network's apList.
	 */
	Enumeration<AccessPoint> getSelectedApList() {
		return apSelectionPage.getSelectedApList();
	}
	
	/**
	 * Returns the destination fips network in which aps are to be moved 
	 */
	String getDestinationNetworkName() {
		return networkSelectionPage.getSelectedNetworkName();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMessage#showMessage(java.lang.String, int)
	 */
	public int showMessage(String message, int style) {
	
		MessageBox msg = new MessageBox(new Shell(), style);
		msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		msg.setMessage(message);
		return msg.open();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IMessageHandler#showMessage(com.meshdynamics.meshviewer.mesh.IMessageHandler.IMessageSource, java.lang.String, java.lang.String, java.lang.String)
	 */
	public void showMessage(final IMessageSource msgSource, final String messageHeader, final String message, final String progress) {
		
		MeshViewerUI.display.asyncExec(new Runnable(){

			@Override
			public void run() {
				if(currentPage == SECURITY_UPDATE_STATUS_PAGE) {
					//security update
					securityUpdateStatusPage.updateStatus(msgSource, messageHeader, message);
				}else if(currentPage == FIPS_STATUS_PAGE) {
					//fips update
					fipsUpdateStatusPage.updateStatus(msgSource, messageHeader, message);
				}
			}
		});
		
	}
	
	MeshNetworkUI getCurrentNetworkUI() {
		Enumeration<MeshNetworkUI> e = meshViewerUI.getMeshNetworkUIs();
		while(e.hasMoreElements()) {
			MeshNetworkUI ui = e.nextElement();
			if(currentNetworkName.equalsIgnoreCase(ui.getNwName()) == true)
				return ui;
		}
		return null;
	}
	
	
	
}
