/**
 * MeshDynamics 
 * -------------- 
 * File     : SecurityPage.java
 * Comments : 
 * Created  : Jul 20, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Jul 20, 2007   | created                            		  | Abhishek     |
 * ----------------------------------------------------------------------------------*/	

package com.meshdynamics.nmsui.wizards.fips;


import java.util.Enumeration;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.nmsui.wizards.WizardPage;


public class SecurityPage extends WizardPage{

	private Group				grp;
	
	private Label				lblSecurityUpdate;
	
	private Table				tblApInfoList;
	private Button				btnSecurity;
	
	private int 				rel_x;
	private int 				rel_y;
	
	private	FipsWizardDialog 	parent;
	private IConfiguration		dlgConfig;
	
	public class RowWidget {
		AccessPoint		ap				= null;
		String  		model			= "";
		String  		fipsSecurity	= "";
	}
	
	public SecurityPage(FipsWizardDialog parent, Composite arg0, int arg1, short pageIdentifier) {
	
		super(arg0, arg1);
		this.parent			= parent;
		this.dlgConfig		= null;
		createControls();
	}
	
	
	
	private void createControls(){
		
		String txtLabel	= "Security Settings";
		
		rel_x	= 5;
		rel_y	= 5;
		
		grp	= new Group(this, SWT.NONE);
		grp.setBounds(rel_x,rel_y,360,275);
		grp.setText(txtLabel);
		
		rel_y += 25;
		rel_x += 10;
		
		tblApInfoList	= new Table(grp, SWT.BORDER |SWT.V_SCROLL|SWT.H_SCROLL);
		tblApInfoList.setBounds(rel_x+5, rel_y, 320, 170);
		tblApInfoList.setHeaderVisible(true);
		tblApInfoList.setLinesVisible(true);
		
		String [] headers	= {
								"Ds Mac Address",
								"Model",
								"FIPS Security"
							  };
		
		TableColumn col [] = new TableColumn[headers.length];
		int 		index;
		
		for(index = 0; index < headers.length; index++) {
			col[index]	= new TableColumn(tblApInfoList,SWT.BORDER| SWT.LEFT);
			col[index].setText(headers[index]);
		}
		col[0].setWidth(135);
		col[1].setWidth(95);
		col[2].setWidth(85);
		
		rel_y += 180;
		rel_x += 225;
		
		btnSecurity	= new Button(grp, SWT.NONE);
		btnSecurity.setText("Security");
		btnSecurity.setBounds(rel_x, rel_y, 100, 20);
		btnSecurity.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				
				Vector<String> 	vectIfNames		= getAvailableInterfaces();
				boolean  		isVlanPresent	= isVlansPresent(); 
				if(vectIfNames.size() == 0) {
					return;
				}
				ApSecurityConfigDlg apSecDlg	= new ApSecurityConfigDlg(dlgConfig, vectIfNames, isVlanPresent);

				apSecDlg.setTitle("Security Configuration");
				apSecDlg.show();
				if(apSecDlg.getStatus() != SWT.OK) {
					return;
				}
				dlgConfig = apSecDlg.getConfiguration();
				if(dlgConfig == null) {
					return;
				}
				setSecurityConfiguration(dlgConfig);
			}
		});
		
		rel_x = 15;
		rel_y += 35;
		
		lblSecurityUpdate	= new Label(grp, SWT.READ_ONLY);
		lblSecurityUpdate.setText("On next, configured security settings will be applied for all Nodes.");
		lblSecurityUpdate.setBounds(rel_x+5, rel_y, 310, 25);
		
	}

	
	/**
	 * @param hashSec
	 */
	protected void setSecurityConfiguration(IConfiguration configuration) {
		
		Enumeration<AccessPoint>	enumApList	= parent.getSelectedApList();
		
		while(enumApList.hasMoreElements()) {
			
			AccessPoint	ap	= (AccessPoint) enumApList.nextElement();
			if(ap == null) {
				continue;
			}
			TableItem[] lstAps	= tblApInfoList.getItems();
			
			for(int i = 0; i < lstAps.length; i++) {
				RowWidget rowData	= (RowWidget) lstAps[i].getData();
				if(rowData.ap == ap) {
					copySecurityConfiguration(configuration, ap);
					rowData.fipsSecurity = "Yes";
					lstAps[i].setText(2, "Yes");
				}
			}
		}
	}

	private void copySecurityConfiguration(IConfiguration srcConfiguration, AccessPoint ap) {
		
		if(ap == null) {
			return;
		}
		
		if(srcConfiguration == null) {
			return;
		}
		IInterfaceConfiguration	srcInterfaceConfiguration	= srcConfiguration.getInterfaceConfiguration();
		if(srcInterfaceConfiguration == null) {
			return;
		}
		copyInterfaceSecurityConfiguration(srcInterfaceConfiguration,ap);
		
		IVlanConfiguration	srcVlanConfiguration	= srcConfiguration.getVlanConfiguration();
		if(srcVlanConfiguration == null) {
			return;
		}
		copyVlanSecurityConfiguration(srcVlanConfiguration,ap);
	}

	/**
	 * @param hashSec
	 * @param ap
	 */
	private void copyInterfaceSecurityConfiguration(IInterfaceConfiguration srcInterfaceConfiguration, AccessPoint	ap) {
		
		int   ifCount;
		int   index;
		short comparison;
		
		IInterfaceInfo			srcIfInfo					= null;
		IInterfaceInfo			dstIfInfo					= null;
		ISecurityConfiguration	srcSecurityConfiguration	= null;
		ISecurityConfiguration	dstSecurityConfiguration	= null;
		
		if(srcInterfaceConfiguration == null) {
			return;
		}
		
		IConfiguration			dstconfiguration 			= ap.getConfiguration();
		if(dstconfiguration == null) {
			return;
		}
		IInterfaceConfiguration	dstInterfaceConfiguration	= dstconfiguration.getInterfaceConfiguration();
		if(dstInterfaceConfiguration == null) {
			return;
		}
		
		IConfigStatusHandler	updateStatusHandler			= ap.getUpdateStatusHandler();
		updateStatusHandler.clearStatus();
		
		ifCount	= dstInterfaceConfiguration.getInterfaceCount();
		
		for(index = 0; index < ifCount; index++) {
			
			dstIfInfo	= (IInterfaceInfo)dstInterfaceConfiguration.getInterfaceByIndex(index);
			if(dstIfInfo == null) {
				continue;
			}
			if(dstIfInfo.getMediumType() == Mesh.PHY_TYPE_802_11 &&
			   dstIfInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_WM) {
				
				srcIfInfo					= (IInterfaceInfo)srcInterfaceConfiguration.getInterfaceByName(dstIfInfo.getName());
				srcSecurityConfiguration	= (ISecurityConfiguration)srcIfInfo.getSecurityConfiguration();
				dstSecurityConfiguration	= (ISecurityConfiguration)dstIfInfo.getSecurityConfiguration();
				if(srcSecurityConfiguration == null || dstSecurityConfiguration == null) {
					continue;
				}
				comparison	= srcSecurityConfiguration.compare(dstSecurityConfiguration, updateStatusHandler);
				if(comparison == IComparable.CONFIG_CHANGED) {
					updateStatusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
					srcSecurityConfiguration.copyTo(dstSecurityConfiguration);	
				}
			}
		}
	}

	/**
	 * @param srcInterfaceConfiguration
	 * @param ap
	 */
	private void copyVlanSecurityConfiguration(IVlanConfiguration srcVlanConfiguration, AccessPoint ap) {
		
		int 	vlanCount;
		int 	index;
		short 	comparison;
		
		IConfiguration			dstconfiguration;
		IVlanConfiguration		dstVlanConfiguration;
		IVlanInfo				srcVlanInfo;
		IVlanInfo				dstVlanInfo;
		ISecurityConfiguration	srcVlanSecConfig;
		ISecurityConfiguration	dstVlanSecConfig;
		IConfigStatusHandler	updateStatusHandler;
			
		if(ap == null) {
			return;
		}
		
		if(srcVlanConfiguration == null) {
			return;
		}
		srcVlanInfo				= (IVlanInfo)srcVlanConfiguration.getVlanInfoByName("vlan");
		if(srcVlanInfo == null) {
			return;
		}
		srcVlanSecConfig		= (ISecurityConfiguration)srcVlanInfo.getSecurityConfiguration();
		if(srcVlanSecConfig == null) {
			return;
		}
		
		dstconfiguration		= ap.getConfiguration();
		if(dstconfiguration == null) {
			return;
		}
		dstVlanConfiguration	= dstconfiguration.getVlanConfiguration();
		if(dstVlanConfiguration == null) {
			return;
		}
		
		updateStatusHandler		= ap.getUpdateStatusHandler();
		
		vlanCount				= dstVlanConfiguration.getVlanCount();
		for(index = 0; index < vlanCount; index++) {
			dstVlanInfo			= (IVlanInfo)dstVlanConfiguration.getVlanInfoByIndex(index);
			if(dstVlanInfo == null) {
				continue;
			}
			dstVlanSecConfig	= (ISecurityConfiguration)dstVlanInfo.getSecurityConfiguration();
			if(dstVlanSecConfig == null) {
				return;
			}
			comparison	= srcVlanSecConfig.compare(dstVlanSecConfig, updateStatusHandler);
			if(comparison == IComparable.CONFIG_CHANGED) {
				updateStatusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
				srcVlanSecConfig.copyTo(dstVlanSecConfig);	
			}
			
		}
		
	}

	private Vector<String> getAvailableInterfaces() {
		
		Vector<String>	vectIfNames	= new Vector<String>();

		for(TableItem item : tblApInfoList.getItems()) {
			RowWidget rowData	= (RowWidget) item.getData();
			fillInterfaceList(vectIfNames, rowData.ap);
		}

		return vectIfNames;
	}

	private boolean isVlansPresent() {
		
		for(TableItem item : tblApInfoList.getItems()) {
			
			RowWidget rowData = (RowWidget)item.getData();
			if(rowData == null)
				continue;
			
			IConfiguration		configuration 		= rowData.ap.getConfiguration();
			if(configuration == null) {
				continue;
			}
			IVlanConfiguration	vlanConfiguration	= configuration.getVlanConfiguration();
			if(vlanConfiguration == null) {
				continue;
			}
			if(vlanConfiguration.getVlanCount() > 0) {
				return true;
			}
		}
		return false;
	}		
	
	/**
	 * @param vectIfNames
	 * @param ap
	 */
	private void fillInterfaceList(Vector<String> vectIfNames, AccessPoint ap) {
		
		IConfiguration	apConfiguration	= ap.getConfiguration();
		
		if(apConfiguration == null) {
			return;
		}
		IInterfaceConfiguration  ifConfig	= apConfiguration.getInterfaceConfiguration();
		int				 		 ifCount 	= ifConfig.getInterfaceCount();
		
		for(int i = 0; i < ifCount; i++) {
			
			IInterfaceInfo ifInfo = (IInterfaceInfo)ifConfig.getInterfaceByIndex(i);
			if(ifInfo == null) {
				continue;
			}
			if(ifInfo.getMediumType() == Mesh.PHY_TYPE_802_11 &&
			   ifInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_WM) {
				String	ifName	= ifInfo.getName();
				if(vectIfNames.contains(ifName) == false) {
					vectIfNames.add(ifName);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#validatePageData()
	 */
	public boolean validatePageData() {
		
		Enumeration<AccessPoint> enumApList	= parent.getSelectedApList();
		
		while(enumApList.hasMoreElements()) {
			AccessPoint	ap	= (AccessPoint) enumApList.nextElement();
			if(ap == null) {
				continue;
			}
			if(FipsSecurityHelper.
				isFipsCompliantConfiguration(ap.getConfiguration()) == false) {
				parent.showMessage("Security settings for "+ap.getDSMacAddress()+" are not FIPS 140-2 compliant.", SWT.ICON_INFORMATION | SWT.OK);
				return false;
			}
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#initializePageData()
	 */
	public boolean initializePageData() {
		
		refreshPage();
		Enumeration<AccessPoint> enumApList	= parent.getSelectedApList();
		
		while(enumApList.hasMoreElements()) {
			AccessPoint	ap	= (AccessPoint) enumApList.nextElement();
			if(ap == null) {
				continue;
			}
			fillItemData(ap);
		}
		return true;
	}


	/**
	 * 
	 */
	private void refreshPage() {
		
		TableItem[]	items	= tblApInfoList.getItems();
		
		for(int i = 0; i < items.length; i++) {
			
			RowWidget	rowData	= (RowWidget) items[i].getData();
			
			if(rowData != null) {
				rowData.ap 				= null;
				rowData.fipsSecurity 	= null;
				rowData.model		 	= null;	
			}
			items[i].dispose();
		}
		
	}



	/**
	 * @param ap
	 * 
	 */
	private void fillItemData(AccessPoint ap) {
		
		final String 	apMacAddress	= ap.getDSMacAddress();
		boolean			found			= false;
		
		for(TableItem	item : tblApInfoList.getItems()) {
			if(item.getText(1).trim().equals(apMacAddress)== true) {
				found	= true;
				break;
			}
		}
		if(found == true){
			return;
		}

		TableItem 	item 	= new TableItem(tblApInfoList,SWT.NONE);
		RowWidget 	rowItem	= new RowWidget();

		rowItem.ap		= ap;
		rowItem.model	= ap.getModel();
		
		if(FipsSecurityHelper.isFipsCompliantConfiguration(ap.getConfiguration())== true) {
			rowItem.fipsSecurity	= "Yes";	
		}else {
			rowItem.fipsSecurity	= "No";
		}
		
		item.setText(0, apMacAddress);
		item.setText(1, rowItem.model);
		item.setText(2, rowItem.fipsSecurity);
		
		item.setData(rowItem);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.Fips.WizardPage#clearPageData()
	 */
	public boolean clearPageData() {
		return true;
	}
}
