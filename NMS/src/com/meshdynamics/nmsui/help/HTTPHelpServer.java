/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : HTTPHelpServer.java
 * Comments : 
 * Created  : May 30, 2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |May 30, 2008 | Created                                         |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.help;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.nmsui.MeshViewerUI;
import com.meshdynamics.nmsui.util.MFile;

public class HTTPHelpServer extends Thread {

private ServerSocket 			socket;
private	static final String	INDEX_PAGE				= "/context_help.mdsp";
private static final String CMD_GET_WEB_PAGE 		= "GET /context_help.mdsp";
private static final String CMD_DEBUG_STR			= "GET /debug";
private static final String CMD_GET_PROPERTIES 		= "/getProperties";

private static final int	HELP_SERVER_PORT		= 55555;

/**
 * 
 */
public HTTPHelpServer(MeshViewerUI meshViewerUI) {
	super();
}

/* (non-Javadoc)
 * @see java.lang.Runnable#run()
 */
public void run() {
	try {
		socket = new ServerSocket(HELP_SERVER_PORT);
	} catch (UnknownHostException e) {
		System.out.println(e);
		return;
	} catch (BindException e) {
		Display.getDefault().asyncExec(new Runnable(){
			public void run() {
				Shell shell = new Shell(Display.getDefault());
				MessageBox messageBox = new MessageBox(shell,SWT.MULTI);
				messageBox.setText(" Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
				messageBox.setMessage("Another instance of NMS is already running..Please Exit.");
				messageBox.open();
				shell.dispose();
				return;
			}});
		return;
	} catch (IOException e) {
		System.out.println(e);
		return;
	}

	System.out.println("HELP Server running on " + socket.getInetAddress().toString() + ":" + socket.getLocalPort());
	
	while(true) {
		
		BufferedReader	clientReader;
		PrintStream		clientWriter;
		Socket			connection;		
		String 			clientText;
		
		try {

			connection		= null;
			connection 		= socket.accept();
			clientReader	= new BufferedReader(new InputStreamReader(connection.getInputStream()));
			clientWriter 	= new PrintStream(connection.getOutputStream());
			
			String text;
			clientText		= "";
			String outStr 	= null;
			
			while((text = clientReader.readLine()) != null) {
				if(text.startsWith(CMD_DEBUG_STR) == true) {
					String tokens[] = text.split(" ");
					System.out.println(tokens[1]);
				} else if(text.startsWith(CMD_GET_WEB_PAGE) == true) {
					outStr = getIndexPage(text);
				} else if(text.startsWith("GET") == true) {
					outStr = getJsonObj(text);
					if(outStr == null) {
						byte[] outBytes = getFile(text);
						if(outBytes != null) {
							clientWriter.write(outBytes);
						} 
					}
				}
				clientText += text;
				if(text.length() == 0) {
					break;
				}
			}
			if(outStr != null) {
				clientWriter.write(outStr.getBytes());
			}
			
			clientReader.close();
			clientWriter.close();
			connection.close();
		} catch (IOException e1) {
			System.out.println(e1);
			if(socket.isClosed() == true) {
				break;
			}
			continue;
		}
	}
	try {
		socket.close();
	} catch (IOException e1) {
		System.out.println(e1);
	}
}

private byte[] getBinaryFile(String fileName) {

	try {
		FileInputStream fStream = new FileInputStream(fileName);
		byte buffer[] = new byte[fStream.available()];
		
		try {
			
			fStream.read(buffer);
			fStream.close();
			return buffer;
			
		} catch (IOException e1) {
			System.out.println(e1);
		}
		
	} catch (Exception e) {
		System.out.println(e);
	}
	
	return null;
}

/**
 * @param string
 * @return
 */
private byte[] getTextFile(String fileName) {
	
	System.out.println("Serving " + fileName);
	String fileStr = "";
	try {
		FileReader 		fReader = new FileReader(fileName);
		BufferedReader	br 		= new BufferedReader(fReader);
		String 			line;
		try {
			while((line = br.readLine()) != null) {
				fileStr += (line + "\r\n");
			}
		
			br.close();
			fReader.close();
			
		} catch (IOException e1) {
			System.out.println(e1);
			return null;
		}
	} catch (Exception e) {
		System.out.println(e);
		return null;
	}
	
	return fileStr.getBytes();
}

/**
 * @param text
 * @return
 */
private byte[] getFile(String text) {
	
	String imageExt[] 	= {"gif", "jpg", "jpeg", "bmp", "png", "ico"};
	String scriptExt  	= "js";
	String tokens[] 	= text.split(" ");
	String fileExt 		= tokens[1].substring(tokens[1].indexOf(".")+1);
	String contentType	= "Content-Type: ";
	
	boolean	loadFile 	= false;
	byte[] 	buffer 		= null;
	
	for(int i=0;i<imageExt.length;i++) {
		if(imageExt[i].equalsIgnoreCase(fileExt) == true) {
			loadFile = true;
			contentType += " image/" + fileExt + ";";
			buffer = getBinaryFile(MFile.getWebPath() + tokens[1]);
			break;
		}
	}
	if(loadFile == false) {
		if(scriptExt.equalsIgnoreCase(fileExt) == true) {
			contentType += " text/javascript; charset=UTF-8";
			buffer = getTextFile(MFile.getWebPath() + tokens[1]);
		}
	}
	
	String outStr = "";
	
	if(buffer != null) {
		outStr = "HTTP/1.1 200 OK\r\n";
		outStr += contentType + "\r\n";
		outStr += "Cache-Control: no-cache\r\n";
		outStr += "Content-Length: " + buffer.length + "\r\n";
		outStr += "\r\n";

		byte outStrBytes[] 	= outStr.getBytes();
		byte outBuffer[] 	= new byte[outStrBytes.length + buffer.length];
		
		System.arraycopy(outStrBytes, 0, outBuffer, 0, outStrBytes.length);
		System.arraycopy(buffer, 0, outBuffer, outStrBytes.length, buffer.length);
		
		return outBuffer;
	}

	return null;
}

/**
 * @param text
 * @return
 */
private String getJsonObj(String text) {
	
	String tokens[] 	= text.split(" ");
	String jsonObjStr	= "";
	String outStr 		= "HTTP/1.1 200 OK\r\n";
	
	outStr += "Content-Type: text/plain; charset=UTF-8\r\n";
	outStr += "Cache-Control: no-cache\r\n";
	
	if(tokens[1].equalsIgnoreCase(CMD_GET_PROPERTIES) == true) {
		jsonObjStr = getProperties();
	} else {
		return null;
	}

	outStr += "Content-Length: " + jsonObjStr.length() + "\r\n";
	outStr += "\r\n";
	outStr += jsonObjStr;
	
	return outStr;
}

/**
 * @return
 */
private String getIndexPage(String httpCommand) {
	
	String tokens[] 	= httpCommand.split(" ");
	String pageTokens[] = tokens[1].split("\\?");
	String PageNumStr	= "var pageNum";
	String helpPath		= "var helpPath";
	String retStr		= "";
	String fileStr 		= "";
	
	if(pageTokens[0].equalsIgnoreCase(INDEX_PAGE) == false) {
		return null;
	}
	
	retStr = "HTTP/1.1 200 OK\r\n";
	retStr += "Content-Type: text/html; charset=UTF-8\r\n";
	retStr += "Cache-Control: no-cache\r\n";
	
	try {
		FileReader 		fReader = new FileReader(MFile.getWebPath() + INDEX_PAGE);
		BufferedReader	br 		= new BufferedReader(fReader);
		String 			line;
		try {
			while((line = br.readLine()) != null) {
				if(line.trim().startsWith(PageNumStr) == true) {
					fileStr += (PageNumStr + " = \"" + pageTokens[1] + "\";\r\n");
					continue;
				}
				
				if(line.trim().startsWith(helpPath) == true) {
					String path = MFile.getFileAbsolutePath();
					path = path.replace("\\", "\\\\");
					fileStr += (helpPath + " = \"" + path +"\\\\MD4000_NMSGUIDE.pdf" + "\";\r\n");
					continue;
				}
				fileStr += (line + "\r\n");
			}
		
			br.close();
			fReader.close();
			
		} catch (IOException e1) {
			e1.printStackTrace();
			return null;
		}
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}
	
	
	retStr += "Content-Length: " + fileStr.length() + "\r\n";
	retStr += "\r\n";
	retStr += fileStr;
	
	return retStr;
}

/**
 * @return
 */
private String getProperties() {
	File file 					= new File(".");
	String helpFilePath  		= file.getAbsoluteFile() + "//MD4000_NMSGUIDE.pdf";
	
	String properties 	= "{";
	properties 			+= "\"path\":" + helpFilePath + "\"";
	properties			+= "}";
	return properties;
}

public void stopServer() {
	
	if(socket != null) {
		try {
			socket.close();
			this.interrupt();
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}

}