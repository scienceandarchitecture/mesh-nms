/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ContextHelp.java
 * Comments : 
 * Created  : May 30, 2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |May 30, 2008	| Created                                         |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.help;

import java.util.Enumeration;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import com.meshdynamics.nmsui.MeshViewerUI;


public class ContextHelp {
	
	static 	Vector<ContextInfo>	contextHistory;
	ContextInfo					currentContext;
	static ContextHelpDlg		ctxhelpDlg;
	HTTPHelpServer				helpServer;
  	int							historyPtr;
  	
	public ContextHelp(MeshViewerUI meshViewerUI) {
		helpServer 		= new HTTPHelpServer(meshViewerUI);
		contextHistory 	= new Vector<ContextInfo>();
		currentContext	= null;
		historyPtr		= -1;	
		ctxhelpDlg		= new ContextHelpDlg(this);
		startServer();
	}

	public static void addContextHelpHandlerEx(Composite composite,String contextName) {

		ContextHelp.addContextHelpHandler(composite, contextName);
		
		for(Control child : composite.getChildren()) {
			if(child instanceof Composite)
				ContextHelp.addContextHelpHandlerEx((Composite)child, contextName);
			else
				ContextHelp.addContextHelpHandler(child, contextName);
		}
	}
	
	public static void addContextHelpHandler(Control composite,String contextName) {
		
		ContextInfo info = findContextInfo(contextName);
		if(info == null) return; 
		composite.setData("helpID",info);
		composite.setToolTipText(info.comments);

		Listener listener = new Listener(){
			public void handleEvent(Event e) {
				if(e.widget.isDisposed() == true)
					return;
				
				switch(e.type) {
					case SWT.Help:
						ContextInfo info = (ContextInfo)e.widget.getData("helpID");
						invokeContextHelp(info.contextName);
						break;
					}
			}
		};
		composite.addListener(SWT.Help, listener);
	}
	
	private static void invokeContextHelp(String contextName) {
		ContextInfo info = findContextInfo(contextName);
		if(info == null) {
			info = getFirstElement();
		}
		ctxhelpDlg.showHelpBrowser(info);
	}
	
	public void startServer(){
		helpServer.start();
	}
	
	public void stopServer() {
		ctxhelpDlg.onExit();
		helpServer.stopServer();
	}
	
	static ContextInfo getFirstElement(){
		
		Enumeration<ContextInfo> e =  ContextFileHandler.contextTable.elements();
		
		if(e.hasMoreElements()) {
			ContextInfo info  = (ContextInfo)e.nextElement();
			return info;
		}
		return null;
	}
	
	static ContextInfo findContextInfo(String contextName) {
		if(ContextFileHandler.contextTable == null)
			return null;
		return (ContextInfo)ContextFileHandler.contextTable.get(contextName);
	}
	
	public void addContextHistory(ContextInfo info) {
		contextHistory.add(info);
		currentContext	= info;
		historyPtr++;
	}
	
	public ContextInfo getBackHistory() {
		if(historyPtr > 0) {
			historyPtr--;
			currentContext = (ContextInfo) contextHistory.get(historyPtr);
		}
		return currentContext;
	}
	
	public ContextInfo getForwardHistory(){
		if(historyPtr < contextHistory.size() - 1 ){
			historyPtr++;
			currentContext = (ContextInfo) contextHistory.get(historyPtr);
		}
		return currentContext;
	}
	/**
	 * @return the currentContext
	*/
	public ContextInfo getCurrentContext() {
		return currentContext;
	}

	/**
	 * @param currentContext the currentContext to set
	*/
	public void setCurrentContext(ContextInfo currentContext) {
		this.currentContext = currentContext;
	}
	
}
