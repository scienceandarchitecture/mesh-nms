/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ContextHelpDlg.java
 * Comments : 
 * Created  : May 30, 2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |May 30, 2008	| Created                                        |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.help;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import com.meshdynamics.meshviewer.NMS_VERSION2;

public class ContextHelpDlg {
	
	private static final String URL 	= "http://localhost:55555/context_help.mdsp?";  
	private static final String BACK 	= "Back";
	private static final String FORWARD	= "Forward";
	
	
	
	Browser 			browser;
	Shell				shell;
	Display				display;
	int					pageNum;
	CoolBar				coolbar;
	ContextHelp			contextHelp;
	
	public ContextHelpDlg(ContextHelp contextHelp)	{
		display 			= Display.getDefault();
		pageNum				= 0;
		this.contextHelp	= contextHelp;
		
	}
	
	public void showHelpBrowser(ContextInfo info) {
		
		if(getPageNum() != info.pageNum) {
			contextHelp.addContextHistory(info);
		}
		setPageNum(info.pageNum);
		showControls();
	}
	
	void showControls() {
		
		boolean openShell = true;
		
		if(shell == null) {
			shell = new Shell(SWT.CLOSE|SWT.MAX|SWT.MIN);
		} else if(shell.isDisposed() == true) {
			shell = null;
			shell = new Shell(SWT.CLOSE|SWT.MAX|SWT.MIN);
		} else
			openShell = false;
		
		if(openShell == true) {
			shell.setText("Network Viewer Help " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			shell.addDisposeListener(new DisposeListener(){
	
				@Override
				public void widgetDisposed(DisposeEvent disposeevent) {
		            if(browser != null)
		            	browser.stop();
				}});
			
			shell.addShellListener(new ShellAdapter(){
		        public void shellClosed(ShellEvent arg0) {
		        }
		    });
			
			FormLayout layout 	= new FormLayout();
			shell.setLayout(layout);
		
			addCoolBar();
		}
		
		boolean newBrowser = true;
		
		if(browser == null) {
			browser = new Browser(shell,SWT.NONE);
		} else if(browser.isDisposed() == true) {
			browser = null;
			browser = new Browser(shell,SWT.NONE);
		} else
			newBrowser = false;
		
		if(newBrowser == true) {
			FormData textData 	= new FormData();
		    textData.left 		= new FormAttachment(0);
		    textData.right		= new FormAttachment(100);
		    textData.top 		= new FormAttachment(coolbar);
		    textData.bottom 	= new FormAttachment(100);
		    browser.setLayoutData(textData);
		}

		browser.setUrl(URL + getPageNum());
		
		if(openShell == true)
			shell.open();
		
		if(shell.getMinimized() == true){
			shell.setMinimized(false);
			shell.setActive();
		}

		try {
			while(!shell.isDisposed()){
				if(!display.isDisposed() && !display.readAndDispatch()){
					display.sleep();
				}
			}
		} catch(Exception e) {
			if(shell != null) {
				if(shell.isDisposed() == false)
					shell.dispose();
			}
			//e.printStackTrace();
		}
	}
	
	Composite addCoolBar() {
		
		CoolBar coolBar 			= new CoolBar(shell, SWT.NONE);
		CoolItem item 				= new CoolItem(coolBar, SWT.NONE);
		ToolBar tb 					= new ToolBar(coolBar, SWT.FLAT);

		ToolItem backButton = new ToolItem(tb, SWT.NONE);
		backButton.setText(BACK);
		backButton.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				cmdBackHandler();
			}});
		ToolItem forwardButton = new ToolItem(tb, SWT.NONE);
		forwardButton.setText(FORWARD);
		forwardButton.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				cmdForwardHandler();
			}}
		);
		
		Point p = tb.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		tb.setSize(p);
		Point p2 = item.computeSize(p.x, p.y);
		item.setControl(tb);
		item.setSize(p2);
		
		FormData coolData 	= new FormData();
	    coolData.left 		= new FormAttachment(0);
	    coolData.right 		= new FormAttachment(100);
	    coolData.top 		= new FormAttachment(0);
	    coolBar.setLayoutData(coolData);
	    coolBar.addListener(SWT.Resize, new Listener() {
	      public void handleEvent(Event event) {
	        shell.layout();
	      }
	    });
	    return coolBar;
	}
	
	private void cmdBackHandler() {
		ContextInfo info = contextHelp.getBackHistory();
		setPageNum(info.pageNum);
		showControls();
	}

	private void cmdForwardHandler() {
		ContextInfo info = contextHelp.getForwardHistory();
		setPageNum(info.pageNum);
		showControls();
	}
	
    public boolean onExit() {
    	
    	if(shell != null) {
    		shell.dispose();
    	}
    	return true;     
    }

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
}
