/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ContextFileHandler.java
 * Comments : 
 * Created  : May 30, 2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |May 30, 2008 | Created                                         |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.help;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;

import com.meshdynamics.nmsui.util.MFile;

public class ContextFileHandler {
	
	public static Hashtable<String, ContextInfo> contextTable = null;
	
	static {
		readTable();
	}
	
	public static void readTable() {
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(MFile.getConfigPath() + "/ContextHelpTable.res"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		String input = null;
		 try {
			while (( input = br.readLine()) != null){
				input = input.trim();
				if(input.equals(""))
					continue;
				if(input.charAt(0) == '#')
					continue;
				String args[] = input.split(",");
				addToTable(args);
			 }
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	private static void addToTable(String[] args) {
		
		if(contextTable == null)
			contextTable = new Hashtable<String, ContextInfo>();
		
		ContextInfo info = new ContextInfo(	args[0],
											Integer.parseInt(args[1].trim()),
											args[2]);
		contextTable.put(args[0], info);
	}
	
}
