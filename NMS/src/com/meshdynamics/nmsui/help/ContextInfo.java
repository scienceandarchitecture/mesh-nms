/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ContextInfo.java
 * Comments : 
 * Created  : May 30, 2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |May 30, 2008 |  Created                                        |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

 
package com.meshdynamics.nmsui.help;

public class ContextInfo {
	
	String	contextName;
	int		pageNum;
	String	comments;
	
	/**
	* @return the contextName
	*/
	public ContextInfo(String contextName,int pageNum,String comments) {
		this.contextName	= contextName;
		this.pageNum		= pageNum;	
		this.comments		= comments;
	}
	
	public String getContextName() {
		return contextName;
	}
	/**
	* @param contextName the contextName to set
	*/
	public void setContextName(String contextName) {
		this.contextName = contextName;
	}
	/**
	 * @return the pageNum
	 */
	public int getPageNum() {
		return pageNum;
	}
	/**
	 * @param pageNum the pageNum to set
	 */
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
};

