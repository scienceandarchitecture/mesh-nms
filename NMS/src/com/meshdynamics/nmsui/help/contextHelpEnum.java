package com.meshdynamics.nmsui.help;

public class contextHelpEnum {
	public static final String MAINWINDOW 				= "mainwindow";
	public static final String PROPERTIESWINDOW 		= "propertieswindow";
	public static final String ALERTSWINDOW				= "alertswindow";
	public static final String NETWORKSWINDOW			= "networkswindow";
	public static final String HEARTBEATWINDOW			= "heartbeatwindow";
	public static final String MACROSWINDOW				= "macroactionswindow";
	public static final String CLIENTACTIVITYWINDOW		= "clientactivitywindow";
	public static final String PBVWINDOW				= "pbvwindow";
	
	public static final String CONFIGTABGENERAL	 		= "configtabgeneral";
	public static final String CONFGTABINTERFACE 		= "configtabinterface";
	public static final String CONFIGTABSECURITY 		= "configtabsecurity";
	public static final String CONFIGTABVLAN 			= "configtabvlan";
	public static final String CONFIGTABACL 			= "configtabacl";
	
	public static final String ADVCONFIGTABIGMP			= "advconfigtabigmp";
	public static final String ADVCONFIGTABRFEDITOR		= "advconfigtabrfeditor";
	public static final String ADVCONFIGTABEFFISTREAM	= "advconfigtabeffistream";
	public static final String ADVCONFIGTABETHERNET		= "advconfigtabethernet";
	public static final String ADVCONFIGTAB80211E		= "advconfigtabdot11e";
	public static final String ADVCONFIGTABPBV			= "advconfigtabpbv";
	public static final String ADVCONFIGTABP3M			= "advconfigtabp3m";
	
	public static final String DLGEFFISTREAMADDRULE		= "dlgeffistreamaddrule";
	public static final String DLGEFFISTREAMADDACTION	= "dlgeffistreamaddaction";
	public static final String DLGNEWNETWORK			= "dlgnewnetwork";
	public static final String DLGRUNMACROPAGE1			= "dlgrunmacropage1";
	public static final String DLGRUNMACROPAGE2			= "dlgrunmacropage2";
	public static final String DLGVIEWNODESETTINGS		= "dlgviewnodesettings";
	public static final String DLGMGPROPERTIES			= "dlgmgproperties";
	public static final String DLGIPERFTEST				= "dlgiperf";
	public static final String DLGRFSPACEINFORMATION	= "dlgrfspaceinformation";
	public static final String DLGFIPSWIZARD			= "dlgfipswizard";
	public static final String DLGGPSWIZARD				= "dlggpswizard";

};


 