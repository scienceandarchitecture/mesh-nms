/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RFSpaceInfoDlg
 * Comments : RF Space Info Configuration Class for MeshViewer
 * Created  : May 11, 2006
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  16 | Jun 08, 2007 | Cancle button removed							  |Imran   |
 *  --------------------------------------------------------------------------------
 * |  15 | May 24, 2007 | DLSaturation Packet referances removed		  |Imran   | 
 * --------------------------------------------------------------------------------
 * |  14 | Mar 21, 2007 | Close button text changed						  |Imran   | 
 * --------------------------------------------------------------------------------
 * |  13 | Mar 20, 2007 | getMaxTxPercent modified for public safety cards|Imran   | 
 * --------------------------------------------------------------------------------
 * |  12 | Mar 14, 2007 | updateRequired label added				 	  |Imran   |
 * --------------------------------------------------------------------------------
 * |  12 | Mar 06, 2007 | rewrite				 		                  |Imran   |
 * --------------------------------------------------------------------------------
 * |  11 | Jun 20, 2006 | Change for b/g max saturation %                 |Bindu   |
 * --------------------------------------------------------------------------------
 * |  10 | Jun 19, 2006 | Change for b/g max saturation %                 |Bindu   |
 * --------------------------------------------------------------------------------
 * |  9  | Jun 13, 2006 | Misc changes for tabs                           |Bindu   |
 * --------------------------------------------------------------------------------
 * |  8  | Jun 13, 2006 | Misc changes for update btn                     |Bindu   |
 * --------------------------------------------------------------------------------
 * |  7  | Jun 12, 2006 | Misc changes                                    |Bindu   |
 * --------------------------------------------------------------------------------
 * |  6  | Jun 01, 2006 | Time Updation after all pkts recd               |Bindu   |
 * --------------------------------------------------------------------------------
 * |  5  | May 31, 2006 | Misc changes                                    |Bindu   |
 * --------------------------------------------------------------------------------
 * |  4  | May 23, 2006 | tab updation once for > 1 ifs(same subtype)     |Bindu   |
 * --------------------------------------------------------------------------------
 * |  3  | May 22, 2006 | Misc UI Fixes                                   |Bindu   |
 * --------------------------------------------------------------------------------
 * |  2  | May 22, 2006 | Misc UI Fixes                                   |Bindu   |
 * --------------------------------------------------------------------------------
 * |  1  | May 19, 2006 | UI Updation of Packet receive                   |Bindu   |
 * --------------------------------------------------------------------------------
 * |  0  | May 11, 2006 | Created                                         |Bindu   |
 * --------------------------------------------------------------------------------
**********************************************************************************/

package com.meshdynamics.nmsui.dialogs.rfspace;

import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceChannelConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.ISaturationInfo;
import com.meshdynamics.meshviewer.imcppackets.GenericCommandRequestResponse;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.PacketSender;
import com.meshdynamics.nmsui.PortInfo;
import com.meshdynamics.nmsui.PortModelInfo;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;


public class RFSpaceInfoDlg extends MeshDynamicsDialog{
	
	private static final double IF_MAXTX_PERCENT_A	= 46.2;		// PHY_SUBTYPE_A 	= 25/54 
	private static final double IF_MAXTX_PERCENT_B	= 54;		// PHY_SUBTYPE_B 	= 6/11
	private static final double IF_MAXTX_PERCENT_G	= 46.2;		// PHY_SUBTYPE_G 	= 23/54
	private static final double IF_MAXTX_PERCENT_BG	= 54;		// PHY_SUBTYPE_BG 	= 19/54
	
	private TabFolder							tabFolder;
	private Label								lblUpdateReq;
	private Button 								btnUpdate;	
	private Group 								group;
	private Hashtable<String, RFSpaceInfoTab>	tabContainer;
	private PacketSender						packetSender;
	private IConfiguration						iConfig;
	private MeshNetwork							network;
	private int									satInfoRecvdforall	= 0;
		
		
	public RFSpaceInfoDlg(MeshNetwork network, IConfiguration iConfig, PacketSender packetSender){
		
		super();
		this.network		= network;
		this.iConfig		= iConfig;	   
		this.packetSender	= packetSender;
		tabContainer		= new Hashtable<String, RFSpaceInfoTab>();
		       	    
		setTitle("RF Space Information for " + iConfig.getDSMacAddress().toString());
		super.setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK);
	}

	public void createControls(Canvas mainCnv) {
		
		lblUpdateReq		= new Label(mainCnv,SWT.NONE);
		lblUpdateReq.setText("Update Required");
		lblUpdateReq.setBounds(12,7,200,20);
		lblUpdateReq.setVisible(false);
		
		btnUpdate 			= new Button(mainCnv,SWT.PUSH);
		btnUpdate.setText("Update");				
		btnUpdate.setBounds(510, 10, 63, 20);
		btnUpdate.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				btnUpdate.setText("Updating...");
				btnUpdate.setEnabled(false);
				sendGenericCommandRequest();
				satInfoRecvdforall = 0;
				lblUpdateReq.setVisible(false);
		    }
		});
				
		tabFolder		=	new TabFolder(mainCnv, SWT.NONE);
		tabFolder.setBounds(10,30,563,220);
		
		group 			= new Group(mainCnv,SWT.SIMPLE);
		Rectangle rect1 = mainCnv.getBounds();		
		group.setBounds(4,rect1.y +rect1.height+13, mainCnv.getBounds().width -15, 2);		
	
        createTabs();
        super.setOkButtonText("Exit");
        ContextHelp.addContextHelpHandlerEx(mainCnv,contextHelpEnum.DLGRFSPACEINFORMATION);
	}
	
	public void createTabs(){
		
		String model 			= iConfig.getHardwareModel();
		PortModelInfo pmInfo	= new PortModelInfo(model, 
													iConfig.getDSMacAddress().toString(),
													iConfig.getInterfaceConfiguration());
		
		IInterfaceConfiguration	interfaceConfig	= iConfig.getInterfaceConfiguration();
		int ifCount 			= interfaceConfig.getInterfaceCount();
		
		for(int i=0;i<ifCount;i++){
			IInterfaceInfo	ifInfo	= (IInterfaceInfo)interfaceConfig.getInterfaceByIndex(i);
			if(ifInfo.getMediumType() == Mesh.PHY_TYPE_802_3)
				continue;  // skip ixp0 and ixp1
			
			if(	(ifInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_AMON)||
					(ifInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_PMON)||
						(ifInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_DS))
			    continue;  //skip scanner and uplink
					
			
			String ifName   		= ifInfo.getName();
			double pMaxTx 			= getMaxTxPercent(ifInfo.getMediumSubType());
			
			RFSpaceInfoTab  rfTab	= new RFSpaceInfoTab(tabFolder,ifName, pMaxTx, SWT.NONE);
			TabItem 		tabItem	=	new TabItem(tabFolder,SWT.LEFT);
			tabItem.setControl(rfTab);
			rfTab.setSaturationInfo((ISaturationInfo)ifInfo.getSaturationInfo());
			rfTab.setChannelInfo((IInterfaceChannelConfiguration)ifInfo.getSupportedChannelConfiguration());
			PortInfo 		pinfo 	= pmInfo.getPortInfoByName(ifName);
			String 			tabName	= pinfo.freqOfRadio + " " + pmInfo.getRadioType(ifName);
			tabName	= tabName + " (" + ifName + ")"; 	
			tabItem.setText(tabName);
			tabContainer.put(ifName, rfTab);
			refreshInformation(ifName);
		}
		
		
		
	}

	public boolean onOk() {
		return true;
	}

	public boolean onCancel() {
		return true;
	}

	public void refreshInformation(String ifName){
		
		if(this.iConfig != iConfig)
			return;
		
		satInfoRecvdforall++;
		if(satInfoRecvdforall == tabFolder.getItemCount()){
			
			btnUpdate.setEnabled(true);
			btnUpdate.setText("Update");
			satInfoRecvdforall = 0;
		}
		RFSpaceInfoTab 		rfTab	= (RFSpaceInfoTab)tabContainer.get(ifName);
		rfTab.refresh();
		
		manipulatelableUpdateRequired();
		
	}
	
	/**
	 * 
	 */
	private void manipulatelableUpdateRequired() {
		
		int infoAvailableForAllInterfaces = 0;
		
		Enumeration<String>	keys	= tabContainer.keys();
		while(keys.hasMoreElements()) {
			RFSpaceInfoTab 		rfTab	= (RFSpaceInfoTab)tabContainer.get(keys.nextElement());
			if(rfTab.isSaturationInfoAvailable() == true) {
				infoAvailableForAllInterfaces++;
			}
		}
		if(infoAvailableForAllInterfaces == tabContainer.size()) {
			lblUpdateReq.setVisible(false);
		} else {
			lblUpdateReq.setVisible(true);
		}
		
		if(btnUpdate.isEnabled() == false){
			lblUpdateReq.setVisible(false);
		}
		
	}

	private void sendGenericCommandRequest(){
	
		GenericCommandRequestResponse reqPacket = (GenericCommandRequestResponse) PacketFactory.getNewIMCPInstance(PacketFactory.GENERIC_COMMAND_REQUEST_RESPONSE_PACKET);		    			    	
		/**
		 * according to new IMCP structure 
		 */
		/*reqPacket.setGeneric_command_req_id(IMCPInfoIDS.IMCP_SNIP_PACKET_TYPE_GENERIC_COMMAND_REQ_RES);
		reqPacket.setGeneric_command_req_length(reqPacket.getPacketLength());*/
		reqPacket.setMeshId(network.getNwName());
    	reqPacket.setDsMacAddress(iConfig.getDSMacAddress());
   		reqPacket.setRequestResponseType(GenericCommandRequestResponse.TYPE_REQUEST);
   		reqPacket.setCommandType(GenericCommandRequestResponse.DN_LINK_SATURATION_REQUEST_RESPONSE);
    	reqPacket.setRequestResponseID(packetSender.getRequestId());
    	packetSender.sendPacket(reqPacket,network.getKey());	
	}
	
	private double getMaxTxPercent(short subType){
		
		if(subType == Mesh.PHY_SUB_TYPE_802_11_A)
			return IF_MAXTX_PERCENT_A;
		if(subType == Mesh.PHY_SUB_TYPE_802_11_B)
			return IF_MAXTX_PERCENT_B;
		if(subType == Mesh.PHY_SUB_TYPE_802_11_G)
			return IF_MAXTX_PERCENT_G;
		if(subType == Mesh.PHY_SUB_TYPE_802_11_B_G)
			return IF_MAXTX_PERCENT_BG;
		if(subType == Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ	||
				subType == Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ	||
				subType == Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ)
			return IF_MAXTX_PERCENT_A;
		
		return 0;

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 570;
		bounds.height	= 250;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}
}
