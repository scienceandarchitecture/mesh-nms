/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ActivityMapDlg
 * Comments : ActivityMap Control for RF Space Info
 * Created  : May 11, 2006
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  7  | May 24, 2007 | DLSaturation packet references removed		  |Imran   | 
 *  --------------------------------------------------------------------------------
 * |  6  | Mar 21, 2007 | Close button text changed						  |Imran   | 
 * --------------------------------------------------------------------------------
 * |  5  | Mar 19, 2007 | changes due to change in meshdynamicsdialog	  |Abhijit |
 * --------------------------------------------------------------------------------
 * |  4  | Mar 07, 2007 |  rewrite                                        | Imran  |
 * --------------------------------------------------------------------------------
 * |  3  | Jun 12, 2006 | UI changes for uncategorised                    |Bindu   |
 * --------------------------------------------------------------------------------
 * |  2  | Jun 08, 2006 | Non categorized added to last row               |Bindu   |
 * --------------------------------------------------------------------------------
 * |  1  | May 31, 2006 | Size changed, misc additions                    |Bindu   |
 * --------------------------------------------------------------------------------
 * |  0  | May 16, 2006 | Created                                         |Bindu   |
 * --------------------------------------------------------------------------------
**********************************************************************************/

package com.meshdynamics.nmsui.dialogs.rfspace;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.configuration.ISaturationInfo;
import com.meshdynamics.nmsui.controls.ActivityMapControl;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;

public class ActivityMapDlg extends MeshDynamicsDialog{
	
	private Table						tblAMInfo;
	private Group 						group;
	private Label 						lblChannel;
	private int 						channelIndex;
	private long						frequency;
	private ISaturationInfo				saturationInfo;
	private double						txPercentage;
	
	public ActivityMapDlg(int channelIndex,long frequency, ISaturationInfo saturationInfo, double txPercentage) {
		
		super();
		this.channelIndex	= channelIndex;
		this.frequency		= frequency;
		this.saturationInfo	= saturationInfo;
		this.txPercentage	= txPercentage;
		super.setTitle("Activity Map");
		super.setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK);
	}
	
	public void createControls(Canvas mainCnv) {
		
		lblChannel		=	new Label(mainCnv,SWT.NO);
		lblChannel.setText("Channel");
		lblChannel.setBounds(12,7,200,20);

		group 			= new Group(mainCnv,SWT.SIMPLE);				
		group.setBounds(8,25,530,190);		

		tblAMInfo = new Table(group,SWT.BORDER|SWT.V_SCROLL|SWT.SINGLE|SWT.FULL_SELECTION);
		tblAMInfo.setBounds(10,15,505,165);
		tblAMInfo.setLinesVisible(true);
		tblAMInfo.setHeaderVisible(true);		
		
		TableColumn tableColumn = new TableColumn(tblAMInfo,SWT.CENTER);
		tableColumn.setWidth(20);

		tableColumn = new TableColumn(tblAMInfo,SWT.CENTER);
		tableColumn.setText("BSSID");
		tableColumn.setWidth(115);
		
		tableColumn = new TableColumn(tblAMInfo,SWT.CENTER);
		tableColumn.setText("ESSID");
		tableColumn.setWidth(135);		
		
		tableColumn = new TableColumn(tblAMInfo,SWT.RIGHT);
		tableColumn.setText("Signal");
		tableColumn.setWidth(70);
		
		tableColumn = new TableColumn(tblAMInfo,SWT.RIGHT);
		tableColumn.setText("TX %");
		tableColumn.setWidth(70);

		tableColumn = new TableColumn(tblAMInfo,SWT.RIGHT);
		tableColumn.setText("Retry %");
		tableColumn.setWidth(90);
		
		group 			= new Group(mainCnv,SWT.SIMPLE);
		Rectangle rect1 = mainCnv.getBounds();		
		group.setBounds(4,rect1.y +rect1.height+13, mainCnv.getBounds().width -15, 2);		
	
	    setValues();
	    
	}
	
	public boolean onCancel() {
		return true;
	}
	
	public boolean onOk() {
		return true;
	}
	
	private void setValues() {
		
		String temp					= "";
		long totalChannelDuration 	= 0;
		long totalChannelTX			= 0;
		long apsChannelTX			= 0;
		long totalChannelRet		= 0;
		long apsChannelRet			= 0;
		DecimalFormat 	df			=  new DecimalFormat("##0.00");
				
		lblChannel.setText(""+frequency+" MHz CH " + saturationInfo.getChannel(channelIndex));

		int 		apCount 	= saturationInfo.getChannelAPCount(channelIndex);
		totalChannelDuration 	= saturationInfo.getChannelTotalDuration(channelIndex);
		totalChannelTX			= saturationInfo.getChannelTxDuration(channelIndex);
		totalChannelRet			= saturationInfo.getChannelRetryDuration(channelIndex);
		
		for(int j = 0; j < apCount; j++) {
			
			apsChannelTX			+= saturationInfo.getChannelAPTxDuration(channelIndex, j);
			apsChannelRet			+= saturationInfo.getChannelAPRetryDuration(channelIndex, j);
			
			TableItem 		item 	=  new TableItem(tblAMInfo,SWT.NONE);
			int 			colorIndex = j % ActivityMapControl.colorList.length; 
			setAPColor(colorIndex); 	//Setting Column 0 with corresponding AP Color
			item.setText(1,"" + saturationInfo.getChannelAPBssid(channelIndex, j).toString());
			item.setText(2,"" + saturationInfo.getChannelAPEssid(channelIndex, j));
			item.setText(3,"" + (saturationInfo.getChannelAPSignal(channelIndex, j) - 96) + " dBm");
			
			
			double apTxDurPercent 	= (double)((saturationInfo.getChannelAPTxDuration(channelIndex, j) * 100)/(double)saturationInfo.getChannelTotalDuration(channelIndex));
			double scaledAPTx 		= (apTxDurPercent * 100)/txPercentage;
			temp 					= df.format(scaledAPTx);
			item.setText(4,"" + temp + "%");
			
			double apRetDurPercent 	= (double)(((saturationInfo.getChannelAPRetryDuration(channelIndex, j) * 100)/(double)saturationInfo.getChannelTotalDuration(channelIndex)));
			double scaledAPRet 		= (apRetDurPercent * 100)/txPercentage;
			temp 					= df.format(scaledAPRet);
			item.setText(5,"" + temp + "%");
		}
		
		TableItem 	itemTotal 	= new TableItem(tblAMInfo,SWT.NONE);
		Color 		color       =  Display.getCurrent().getSystemColor(SWT.COLOR_GRAY);
		Label		lblColor	= new Label(tblAMInfo,SWT.NONE);
		lblColor.setBackground(color);
		TableEditor editor 		= new TableEditor(tblAMInfo);		
	    editor.grabHorizontal 	= editor.grabVertical = true;
	    editor.setEditor(lblColor, itemTotal, 0);

		itemTotal.setText(1,"-");
		itemTotal.setText(2,"Other (Not-categorized)");
		itemTotal.setText(3,"-");

		double unCatTx	= (double)(((totalChannelTX - apsChannelTX) * 100)/(double)totalChannelDuration);
		double unCatRet = (double)(((totalChannelRet - apsChannelRet) * 100)/(double)totalChannelDuration);
		
		unCatTx	 		= (unCatTx  * 100)/txPercentage;
		temp 			= df.format(unCatTx);
		itemTotal.setText(4,"" + temp + "%");
		
		unCatRet		= (unCatRet * 100)/txPercentage;
		temp 			= df.format(unCatRet);
		itemTotal.setText(5,"" + temp + "%");
				
	}
	
	private void setAPColor(int i) {
			
		TableItem	item 			= (TableItem)tblAMInfo.getItem(i);
		Label		lblColor		= new Label(tblAMInfo,SWT.NONE);
		lblColor.setBackground(ActivityMapControl.colorList[i]);
		TableEditor editor 			= new TableEditor(tblAMInfo);		
	    editor.grabHorizontal 		= editor.grabVertical = true;
	    editor.setEditor(lblColor, item, 0);
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 530;
		bounds.height	= 210;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

}
