/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RFSpaceInfoTab
 * Comments : RF Space Info Tab for 5G & 2.4G Configuration for MeshViewer
 * Created  : May 11, 2006
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  11 | May 24, 2007 | DLSaturation info packet references removed     |Imran   |
 * --------------------------------------------------------------------------------
 * |  10 | Mar 19, 2007 | Activitymap dialog call modified                |Abhijit |
 * --------------------------------------------------------------------------------
 * |  9  | Mar 06, 2007 | rewrite         			                      |Imran   |
 * --------------------------------------------------------------------------------
 * |  8  | Jun 13, 2006 | Misc changes for tab disabling                  |Bindu   |
 * --------------------------------------------------------------------------------
 * |  7  | Jun 13, 2006 | Misc changes for update btn                     |Bindu   |
 * --------------------------------------------------------------------------------
 * |  6  | Jun 12, 2006 | Full row selection                              |Bindu   |
 * --------------------------------------------------------------------------------
 * |  5  | May 31, 2006 | Scaling changes		                          |Bindu   |
 * --------------------------------------------------------------------------------
 * |  4  | May 23, 2006 | Misc UI Changes	    	                      |Bindu   |
 * --------------------------------------------------------------------------------
 * |  3  | May 23, 2006 | Row Addition Logic changed                      |Bindu   |
 * --------------------------------------------------------------------------------
 * |  2  | May 22, 2006 | Misc UI Fixes                                   |Bindu   |
 * --------------------------------------------------------------------------------
 * |  1  | May 19, 2006 | UI Updation of Packet receive                   |Bindu   |
 * --------------------------------------------------------------------------------
 * |  0  | May 11, 2006 | Created                                         |Bindu   |
 * --------------------------------------------------------------------------------
**********************************************************************************/

package com.meshdynamics.nmsui.dialogs.rfspace;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.configuration.IChannelInfo;
import com.meshdynamics.meshviewer.configuration.IInterfaceChannelConfiguration;
import com.meshdynamics.meshviewer.configuration.ISaturationInfo;
import com.meshdynamics.nmsui.controls.ActivityMapControl;


public class RFSpaceInfoTab extends Canvas {
	
	private Table							tblRFInfo;
	private Label							lblRFSpaceInfoStatus;
	private String							tabName;
	private ISaturationInfo					saturationInfo;
	private IInterfaceChannelConfiguration 	channelInfo;
	private double 							maxTxPercentage;
	private String[] tableHeaders = {"Channel", "Activity Map", "Saturation", "..."};
	

	public class RowWidget {
		int 				channelIndex;
		ActivityMapControl 	amc = null;
		Button				btn = null;
	}
	
	public RFSpaceInfoTab(Composite arg0, String tabName, double maxTxPercentage, int arg1) {		
		super(arg0, arg1);
		this.tabName 			= tabName;
		this.maxTxPercentage	= maxTxPercentage;
		saturationInfo			= null;
		channelInfo				= null;
		
		createControls();
	}
	
	private void createControls() {
				
		lblRFSpaceInfoStatus = new Label(this, SWT.NONE);
	//	lblRFSpaceInfoStatus.setBounds(10,20,250,25);
		
		tblRFInfo = new Table(this,SWT.BORDER|SWT.V_SCROLL|SWT.SINGLE);
		tblRFInfo.setBounds(10,45,535,125);
		tblRFInfo.setLinesVisible(true);
		tblRFInfo.setHeaderVisible(true);		
		
		TableColumn tableColumn = new TableColumn(tblRFInfo,SWT.LEFT);
		tableColumn.setText(tableHeaders[0]);
		tableColumn.setWidth(95);
		
		tableColumn = new TableColumn(tblRFInfo,SWT.CENTER);
		tableColumn.setText(tableHeaders[1]);
		tableColumn.setWidth(268);		
		
		tableColumn = new TableColumn(tblRFInfo,SWT.RIGHT);
		tableColumn.setText(tableHeaders[2]);
		tableColumn.setWidth(150);
		
		tableColumn = new TableColumn(tblRFInfo,SWT.CENTER);
		tableColumn.setWidth(18);
		
	}

	public void setSaturationInfo(ISaturationInfo saturationInfo) {
		this.saturationInfo = null;
		this.saturationInfo	= saturationInfo;
	}
	
	public void setChannelInfo(IInterfaceChannelConfiguration  channelInfo){
		this.channelInfo = channelInfo;
	}

	public void refresh() {
		updateUI();
		
	}
	
	private TableItem getTableItem(int channelNumber) {
	/*	for(int i = 0;i<tblRFInfo.getItemCount();i++){
			TableItem item = tblRFInfo.getItem(i);
			System.out.println(item.getData().toString());
			if(item.getData().toString().equalsIgnoreCase(Integer.toString(channelNumber)))
				return item;
		}*/
		TableItem item = new TableItem(tblRFInfo,SWT.NONE);
		return item;
	}
	
	private void updateUI(){
		
		if(saturationInfo.isAvailable() == false){
			tblRFInfo.setVisible(false);
			lblRFSpaceInfoStatus.setText("RF Space Information unavailable for this interface");
			lblRFSpaceInfoStatus.setBounds(150,80,250,25);
			return;
		}
		
		tblRFInfo.setVisible(true);
		lblRFSpaceInfoStatus.setText("RF Space Information updated at " +saturationInfo.getLastUpdateTime());
		lblRFSpaceInfoStatus.setBounds(10,20,250,25);
		
		DecimalFormat 	df 	= new DecimalFormat("##0.00");
			
		TableItem[] t	=	tblRFInfo.getItems();
		for(int i=0; i< t.length;i++){
			RowWidget widget = (RowWidget) t[i].getData();
			if(widget != null) {
				widget.amc.dispose();
				widget.amc = null;
				widget = null;
			}
			
			t[i].dispose();
		}
		
		for(int i=0; i<saturationInfo.getChannelCount(); i++){
		
			TableItem item			 = getTableItem(saturationInfo.getChannel(i));
			//item.setData(Short.toString(saturationInfo.getChannel(i)));
			IChannelInfo chInfo   = (IChannelInfo)channelInfo.getChannelInfoByChNumber(saturationInfo.getChannel(i));
			String			temp 	 = "";
			long			freq	 = 0;
			
			if(chInfo != null){
				freq	= chInfo.getFrequency();
				temp 	= ""+freq+" MHz";
			}
			
			temp 	 				 += " CH "+saturationInfo.getChannel(i);
				
			item.setText(0, temp);
			item.setData(""+i);
			
			double txPercentage 		= ((double)((saturationInfo.getChannelTxDuration(i) * 100 )/(double)saturationInfo.getChannelTotalDuration(i)));
			txPercentage 				= (txPercentage * 100)/maxTxPercentage;
			String strTxPercentage		= df.format(txPercentage);
			strTxPercentage 			= strTxPercentage + "% (";

			double retryPercentage		= (double)((saturationInfo.getChannelRetryDuration(i) * 100)/(double)saturationInfo.getChannelTotalDuration(i));
			retryPercentage 			= (retryPercentage * 100)/maxTxPercentage;
			String strRetryPercentage	= df.format(txPercentage);
			strTxPercentage 			+= strRetryPercentage + "% RETRY)";
			setRowWidgets(item,i, freq);
			item.setText(2, strTxPercentage);
		}
		
	}

	public String getTabName() {
		return tabName;
	}
		
	private void setRowWidgets(TableItem item, final int channelIndex,final long frequency) {

		int 		apCount					= saturationInfo.getChannelAPCount(channelIndex);
		short 		channelMaxSignal		= (short) (saturationInfo.getChannelMaxSignal(channelIndex) - 96);
		short 		channelAvgSignal		= (short) (saturationInfo.getChannelAvgSignal(channelIndex) - 96);		
		
		RowWidget	widget 			= new RowWidget();
		
		ActivityMapControl 	amc 	= new ActivityMapControl(tblRFInfo, saturationInfo, channelIndex,maxTxPercentage);
		
		amc.setToolTipText("" + apCount + " Access Points, " + channelMaxSignal + " dBm Max Power, " + channelAvgSignal + " dBm Avg Power");
		TableEditor editor 			= new TableEditor(tblRFInfo);
	    editor.grabHorizontal 		= editor.grabVertical = true;
	    editor.setEditor(amc, item, 1);
	    amc.rePaint();	    
	    
		Button		btnInfo		= new Button(tblRFInfo,SWT.PUSH);		
		btnInfo.setText("...");
		btnInfo.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				showActivityChannelActivityDialog(channelIndex,frequency);
			}
		});	
		
		editor 					= new TableEditor(tblRFInfo);		
	    editor.grabHorizontal 	= editor.grabVertical = true;
	    editor.setEditor(btnInfo, item, 3);
	    
	    widget.amc 				= amc;
	    widget.channelIndex 	= channelIndex;
	    widget.btn				= btnInfo;
	    
	    item.setData(widget);		
	}
	
	private void showActivityChannelActivityDialog(int channelIndex, long frequency) {
		ActivityMapDlg map = new ActivityMapDlg(channelIndex,frequency, saturationInfo, maxTxPercentage);
		map.show();
	}

	/**
	 * @return
	 */
	public boolean isSaturationInfoAvailable() {
		return saturationInfo.isAvailable();
	}
	

}
