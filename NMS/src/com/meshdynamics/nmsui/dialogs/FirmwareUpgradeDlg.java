package com.meshdynamics.nmsui.dialogs;

import java.io.File;
import java.util.Enumeration;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IApFwUpdateListener;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.Event;
import com.meshdynamics.util.MacAddress;

public class FirmwareUpgradeDlg extends MeshDynamicsDialog implements IApFwUpdateListener{
	
	private     Table                  table;
	private     Button			 	   btnStart;
	private     Label                  label;
	private     Text                   versionTxt;
	private     Label                  versionlbl;
	
	private     Enumeration<String>    enumMacStr;
	private     Vector<AccessPoint>	   apVector;
	private     MeshNetwork			   meshNetwork;
	private     AccessPoint			   currentAp;
 	private     TableItem			   currentTableItem;
	private     Event				   updateEvent;
	private     String                 version;
	
	public FirmwareUpgradeDlg(Enumeration<String> enumMacStr,MeshNetwork meshNetwork){
		//this.ap          = ap;
		this.enumMacStr  = enumMacStr;
		this.apVector    = new Vector<AccessPoint>();
		this.meshNetwork = meshNetwork;
		version          = "3.0";
		updateEvent		 = new Event();
		updateEvent.setEvent();

	}
	
	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x 		= 100;
		bounds.y 		= 100;
		bounds.width 	= 480;
		bounds.height 	= 350;	
	}

	@Override
	protected void createControls(Canvas mainCnv) {
		Group grp = new Group(mainCnv,SWT.NONE);
		grp.setBounds(10,5,475,350);
		
		label = new Label(grp,SWT.BOLD);
		label.setText("Press download to get Latest images.");
		label.setBounds(15, 13, 210, 20);
		
		versionlbl = new Label(grp,SWT.NO);
		versionlbl.setText("VERSION");
		versionlbl.setBounds(15, 33, 75, 22);
		
		versionTxt = new Text(grp,SWT.BORDER);
		versionTxt.setBounds(90, 33, 150, 22);
		
		
		btnStart = new Button(grp,SWT.PUSH);
		btnStart.setBounds(15, 60, 75, 22);
		btnStart.setText("DOWNLOAD");
		btnStart.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				String new_version=versionTxt.getText();
				downloadImages_first(new_version);				
			}	
		});
		
		/*downloadBtn = new Button(grp,SWT.PUSH);
		downloadBtn.setBounds(95, 33, 75, 22);
		downloadBtn.setText("DOWNLOAD IMAGE");
		downloadBtn.addSelectionListener(new SelectionAdapter(){
            public void widgetSelected(SelectionEvent arg0) {
               // startDownloading(apVector);
            }
		});*/
		
		table=new Table(grp,SWT.BORDER|SWT.V_SCROLL | SWT.SINGLE |SWT.FULL_SELECTION);	
		table.setBounds(15,100,445,200);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);	
		
		TableColumn tableColumn  = new TableColumn(table,SWT.LEFT);
		tableColumn.setText("Node MacAddress");
		tableColumn.setWidth(150);
		
		tableColumn=new TableColumn(table,SWT.LEFT | SWT.BUTTON1);
		tableColumn.setText("Image");
		tableColumn.setWidth(150);
		tableColumn.addSelectionListener(new SelectionAdapter() {
  	      public void widgetSelected(SelectionEvent e) {System.out.println("Listener called ");}});
		
		/*tableColumn=new TableColumn(table,SWT.LEFT);
		tableColumn.setText("version");
		tableColumn.setWidth(50);
		
		tableColumn=new TableColumn(table,SWT.LEFT);
		tableColumn.setText("DeviceType");
		tableColumn.setWidth(50);*/
		
		tableColumn=new TableColumn(table,SWT.LEFT);
		tableColumn.setText("update status");
		tableColumn.setWidth(100);
		setTitle("Over-the-Air Firmware Update [Verifying available units...]");
		setCursor(Display.getDefault().getSystemCursor(SWT.CURSOR_WAIT));
		//setAllControls(false);
		downloadImages();
		Display.getDefault().asyncExec(new Runnable(){
			public void run() {
				setupData();
			}
		});
	}

	private void setupData(){
		table.clearAll();
		//apVector.clear();		
		for(AccessPoint ap:apVector){			
			TableItem item = new TableItem(table,SWT.NONE);
			item.setText(0, ap.getDSMacAddress());
			String fileName=getUpdateFileName(ap.getDSMacAddress());				
			if(fileName == null) {
				item.setText(1, "Firmware file not downloaded.");
				item.setForeground(UIConstants.GRAY_COLOR);
				ap.showMessage(ap,Mesh.MACRO_FIRMWARE_UPDATE,Mesh.STATUS_FAILED, "Firmware file not found.");
				continue;
			}
			item.setText(1,fileName);
			item.setText(2,"ready for update");
			}
		setTitle("Over-the-Air Firmware Update [" + apVector.size() + " unit(s)]");
		//setAllControls(true);
		setCursor(Display.getDefault().getSystemCursor(SWT.CURSOR_ARROW));
		if(apVector.size() <= 0) {
			btnStart.setEnabled(false);
			enableOkButton(false);
			}
	}
	/*
	 * check images are available or not
	 */
	private void downloadImages(){
		apVector.clear();
		MacAddress	apMac = new MacAddress();
		while(enumMacStr.hasMoreElements()){			
			String strMacAddress = enumMacStr.nextElement(); 
			apMac.setBytes(strMacAddress);
			AccessPoint ap = meshNetwork.getAccessPoint(apMac);
			String model=ap.getModel();
			String deviceType="";
			if(model.startsWith("MD6"))
				deviceType="IMX";
			else
				deviceType="CNS";
			String fileName=getUpdateFileName(ap.getDSMacAddress());
		 if(fileName == null){					
							ap.buildImage(ap.getDSMacAddress(), FirmwareUpgradeDlg.this,deviceType,version);
				}
					 apVector.add(ap);
				}
		}
	
	protected void handleStartUpdate(Vector<AccessPoint> aps) {
		btnStart.setEnabled(false);
		setCancelButtonText("Cancel");
		notifyApFileUpdate(-1, 0);
		startUpdate(aps, this);
	}
	
	protected void startUpdate(final Vector<AccessPoint> aps, final IApFwUpdateListener listener) {
		for(AccessPoint ap:aps){
			setCurrentSelectedAp(ap);
			currentTableItem.setText(2, "upgrade started..");
			ap.firmwareUpgrade();
		}
		
	}

	protected void setCurrentSelectedAp(AccessPoint ap) {
		currentAp 			= ap;
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				for(TableItem item : table.getItems()) {
					if(item.getText().equals(currentAp.getDSMacAddress()) == true) {
						currentTableItem = item;						
						return;
					}
				}
			}
		});
	}

	/*private void setAllControls(boolean enableDisable) {
		for(Control c : mainCnv.getChildren()) {
			c.setEnabled(enableDisable);
		}
	}*/
	@Override
	protected boolean onOk() {
		 handleStartUpdate(apVector);
		return true;
	}

	@Override
	protected boolean onCancel() {
		// TODO Auto-generated method stub
		return true;
	}
	
	private String getUpdateFileName(String strMacAddress) {
		/*String fileName = strMacAddress.replace(':', '_');
		fileName += ".bin";
		if(deviceType.startsWith("CNS"))
		fileName = MFile.getUpdatesPath() + "/cns3xxx_md_3.0.1_"+fileName;
		else
			fileName = MFile.getUpdatesPath() + "/imx_md_3.0.1_"+fileName;*/
		String fileName=verifyUpdateFilePresent(strMacAddress);		
		return fileName;
	}

	private String verifyUpdateFilePresent(String strMacAddress) {
		
		/*String fileName = getUpdateFileName(strMacAddress,null);*/
		String fileName=null;
		String endName = strMacAddress.replace(':', '_');
		File f = new File(MFile.getUpdatesPath());
	    String[] fileList=f.list();
	    for(String name:fileList){	
		if(name.endsWith(endName+".bin"))
			fileName = MFile.getUpdatesPath()+"/"+name;
	}
		return fileName;
	}

	@Override
	public void notifyUpdateStatus(final int ret, final String status) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {				
				//showStatusText(status);				
				if(ret == Mesh.FW_UPDATE_RETURN_SUCCESS_UPDATE_STATUS)
					return;
				if(currentTableItem == null)
					return;
				if(currentTableItem.isDisposed() == true)
					return;
				currentTableItem.setText(2, status);
				System.out.println("status is : "+status);
			}
		});
	}

	@Override
	public void notifyApFileUpdate(int currentFileNo, int totalFileCount) {
		// TODO Auto-generated method stub
		
	}
	private void downloadImages_first(String new_version) {
		MacAddress	apMac = new MacAddress();
		if(apVector == null && apVector.size()<=0){
		while(enumMacStr.hasMoreElements()){			
			String strMacAddress = enumMacStr.nextElement(); 
			apMac.setBytes(strMacAddress);
			AccessPoint ap = meshNetwork.getAccessPoint(apMac);
			String model=ap.getModel();
			String deviceType="";
			if(model.startsWith("MD6"))
				deviceType="IMX";
			else
				deviceType="CNS";
			String fileName=getUpdateFileName(ap.getDSMacAddress());
			 if(fileName == null){					
							ap.buildImage(ap.getDSMacAddress(), FirmwareUpgradeDlg.this,deviceType,new_version);
							currentTableItem.setText(2, "Image Download started..");
						}
					
			apVector.add(ap);
				}
		
	}else{
		for(AccessPoint ap:apVector){
			String model=ap.getModel();
			String deviceType="";
			if(model.startsWith("MD6"))
				deviceType="IMX";
			else
				deviceType="CNS";
			String fileName=getUpdateFileName(ap.getDSMacAddress());
			 if(fileName == null){					
							ap.buildImage(ap.getDSMacAddress(), FirmwareUpgradeDlg.this,deviceType,version);
							//currentTableItem.setText(2, "Image Download started..");
						}
		}
	}
}
}
