/*
 * Created on Feb 21, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class UIConstants {
	
	
	public static Rectangle   minimumBounds  = 	new Rectangle(0,0,0,0);
	
	public static Rectangle getMinimumBtnBounds(){
	
		minimumBounds.height = 20;
		minimumBounds.width  = 60;
		return minimumBounds; 
	}
	
	public static Rectangle getMinimumCmbHeight(){
		
		minimumBounds.height = 20;
		minimumBounds.width  = 60;
		return minimumBounds; 
		
	}
	
	public static Image imgNew  		= new Image(null,UIConstants.class.getResourceAsStream("icons/new.gif"));
	public static Image imgDel  		= new Image(null,UIConstants.class.getResourceAsStream("icons/del.gif"));
	public static Image imgEdit 		= new Image(null,UIConstants.class.getResourceAsStream("icons/edit.gif"));
	public static Image imgSave 		= new Image(null,UIConstants.class.getResourceAsStream("icons/save.gif"));
	
	public static Image imgMeshNode 	= new Image(null,UIConstants.class.getResourceAsStream("icons/Meshnode.gif"));
	
	public static Color	BLUE_COLOR		= Display.getCurrent().getSystemColor(SWT.COLOR_BLUE);	 
	public static Color YELLOW_COLOR	= Display.getCurrent().getSystemColor(SWT.COLOR_YELLOW);
	public static Color	GRAY_COLOR 		= Display.getCurrent().getSystemColor(SWT.COLOR_GRAY);
	public static Color	WHITE_COLOR 	= Display.getCurrent().getSystemColor(SWT.COLOR_WHITE);
	public static Color BLACK_COLOR		= Display.getCurrent().getSystemColor(SWT.COLOR_BLACK);
	public static Color	RED_COLOR 		= Display.getCurrent().getSystemColor(SWT.COLOR_RED);
	public static Color	GREEN_COLOR 	= Display.getCurrent().getSystemColor(SWT.COLOR_GREEN);
	public static Color	ORANGE_COLOR	= new Color(null,255,128,64);
	
	
}
