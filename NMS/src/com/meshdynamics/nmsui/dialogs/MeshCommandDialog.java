/**
 * MeshDynamics 
 * -------------- 
 * File     : MeshCommandDialog
 * Comments : 
 * Created  : July 07, 2006
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ---------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 * ---------------------------------------------------------------------------------
 * |  6  |Oct 2, 2007  | process killing added					         | Imran   |
 * ---------------------------------------------------------------------------------
 * |  5  |Aug 20, 2007  | Changes for saving last entered command        | Imran   |
 * ---------------------------------------------------------------------------------
 * |  4  |May 1, 2007  | Changes due to MeshDynamicsDialog               |Abhishek|
 * --------------------------------------------------------------------------------
 * |  3  |Mar 26, 2007 | Command Combo writable,removed cmd txt clearing |  Bindu  |
 * ---------------------------------------------------------------------------------
 * |  2  |Mar 16, 2007 | changes due to meshdynamics dialog change		 |Abhijit  |
 * ---------------------------------------------------------------------------------
 * |  1  |Nov 27, 2006 | clean-up, adv user settings  			         |  Bindu  |
 * ---------------------------------------------------------------------------------
 * |  0  |July 07, 2006|  Created	                                     | Prachiti|
 * ----------------------------------------------------------------------------------
 * 
 */
package com.meshdynamics.nmsui.dialogs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.ICommandExecutionListener;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.miscd.MeshCommandHelper;
import com.meshdynamics.nmsui.MeshViewerProperties;
import com.meshdynamics.nmsui.MeshViewerUI;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.nmsui.util.MeshViewerGDIObjects;
import com.meshdynamics.util.IpAddress;

public class MeshCommandDialog extends MeshDynamicsDialog {
    
    private Text 		txtIPAddress;
    private Text 		txtCommand;
    private Text 		txtStatus;
    private Button		btnCommand,btnClearCommand;
    private Button		btnClear,btnCopyToClip;
    private Combo		cmbCommand;
    private IpAddress 	NodeIPAddr;
    private Shell		parentShell;
    private AccessPoint ap;
    
    public  static 	Image	imgClear;
    public  static 	Image	imgRun;
    
    private static Vector<String>	vectCommands;
    private MeshCommandHelper		commandHelper;
    private CmdExecutor				cmdExecutor;
    
    private class CmdExecutor extends Thread implements ICommandExecutionListener { 
    	
    	public String 	sCommand;
    	public boolean	running;
    	
    	public CmdExecutor() {
			sCommand 	= "";
			running		= false;
		}
    	
    	public void run() {
    		running = true;
			String cmdResult = "";
			
			if(ap == null) {				
					cmdResult = commandHelper.executeCommand(NodeIPAddr.getBytes(), sCommand);     			
			} else {
				cmdResult = ap.executeCommand(sCommand, this) + SWT.CR + SWT.LF;
			}
			
 			commandOutput(cmdResult);    			
			addCommand(sCommand);
			addToCombo(sCommand);
			running = false;
    	}

		@Override
		public void commandExecutionResponse(AccessPoint ap, String command, String responseStr) {
			commandOutput(responseStr);
		}
    	
    }
    
    static {
    	imgClear		= new Image(null,MeshCommandDialog.class.getResourceAsStream("icons/clear.gif"));
    	imgRun   		= new Image(null,MeshCommandDialog.class.getResourceAsStream("icons/ret.gif"));
    	vectCommands	= new Vector<String>();
    }
     
	private static void addCommand(String command) {
		if(vectCommands.contains(command) == false) {
			vectCommands.add(command);
		}
	}
	
    public static void main(String[] args) {
        IpAddress addr = new IpAddress();
        addr.setBytes("10.0.0.104"); 
        MeshCommandDialog meshCmd = new MeshCommandDialog(addr);
        meshCmd.show(); 
    }
    
    public MeshCommandDialog(AccessPoint ap) {
    	this.ap 		= ap;
   		NodeIPAddr 		= (ap != null) ? ap.getIpAddress() : null;
   		cmdExecutor 	= new CmdExecutor();
    	setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK);
    	commandHelper 	= new MeshCommandHelper();;
    	
    }
    
    public MeshCommandDialog(IpAddress ipAddr) {
    	NodeIPAddr		= ipAddr;
    	ap				= null;
    	cmdExecutor 	= new CmdExecutor();
    	commandHelper 	= new MeshCommandHelper();
    	setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK);
    }
    
    /**
	 * @return
	 */
	private boolean readConfigFile() {
		
		BufferedReader br = null;
		
		try {
			String input 	= null;
			br 				= new BufferedReader(new FileReader(MFile.getConfigPath() + "/meshcommand.txt"));			
		 
			while (( input = br.readLine()) != null) {
				cmbCommand.add(input);				
			}
			
		 }catch (IOException e1) {
			e1.printStackTrace();
			return false;
		}
		 
		 return true;
		
	}

	private void setDefaultValues() {
    	if(MeshViewerProperties.meshCommandAdvanced == 0) {
    		txtCommand.setVisible(true);
    		cmbCommand.setVisible(false);
    	} else if(MeshViewerProperties.meshCommandAdvanced == 1) {
    		boolean flag = readConfigFile();
    		if(flag == false || cmbCommand.getItemCount() == 0) {
        		txtCommand.setVisible(true);
        		cmbCommand.setVisible(false);
    		} else {
        		txtCommand.setVisible(false);
        		cmbCommand.setVisible(true);
        		cmbCommand.select(0);
        		cmbCommand.setFocus();
    		}
    	}

    	txtStatus.setText("#\n ");
    	if(NodeIPAddr == null) {
            NodeIPAddr = new IpAddress(); 
            txtIPAddress.setText("");
            txtIPAddress.setFocus(); 
        }else {	       
	        txtIPAddress.setText("" + NodeIPAddr);
        }    	
  }
	
	public void createControls(Canvas mainCnv) {
       
		int x = 7;
		int y = 5;
		parentShell	= mainCnv.getShell();
			
		Group mainGrp = new Group(mainCnv,SWT.NONE);
		mainGrp.setBounds(15,y,450,75);		

		Label lblCommand = new Label(mainGrp,SWT.NONE);
		lblCommand.setBounds(x + 10,y + 10,60,20);
		lblCommand.setText("IP Address");
		
		x = x + 67;
		y = y + 10;
		txtIPAddress = new Text(mainGrp,SWT.BORDER );
		txtIPAddress.setBounds(x+5,y,100,20);
		txtIPAddress.setFocus();
		
		y = y + 25;
		lblCommand = new Label(mainGrp,SWT.NONE);
		lblCommand.setBounds(x-55,y+10,50,20);
		lblCommand.setText("Command");		
		
		txtCommand = new Text(mainGrp,SWT.BORDER );
		txtCommand.setBounds(x+5,y+5,300,20);
		txtCommand.setFocus();
		txtCommand.addKeyListener(new KeyAdapter(){

            public void keyPressed(KeyEvent arg0) {
                if(arg0.keyCode == SWT.CR){
                    /*Send MeshCommand*/
                    runMeshCommand(); 
                	txtCommand.setFocus();
                }
            }
         });
		
		cmbCommand = new Combo(mainGrp, SWT.BORDER);
		cmbCommand.setBounds(x+5,y+5,300,20);
		cmbCommand.addKeyListener(new KeyAdapter(){

            public void keyPressed(KeyEvent arg0) {
                if(arg0.keyCode == SWT.CR){
                    /*Send MeshCommand*/
                    runMeshCommand(); 
                }
            }
         });

		x = x + 310;
		btnCommand = new Button(mainGrp,SWT.FLAT);
		btnCommand.setBounds(x,y+5,20,20);
		btnCommand.setImage(imgRun);
		btnCommand.setToolTipText("Run Command");
		btnCommand.addSelectionListener(new SelectionAdapter(){
            public void widgetSelected(SelectionEvent arg0) {               
            	runMeshCommand();            	
            }
		});
		setDefaultButton(btnCommand);
		
		x = x + 25;
		btnClearCommand = new Button(mainGrp,SWT.FLAT);
		btnClearCommand.setBounds(x,y+5,20,20);
		btnClearCommand.setToolTipText("Clear Command");
		btnClearCommand.setImage(imgClear);
		btnClearCommand.addSelectionListener(new SelectionAdapter(){
            public void widgetSelected(SelectionEvent arg0) {
                
            	txtCommand.setText("");
            	cmbCommand.setText("");
            	
                if(MeshViewerProperties.meshCommandAdvanced == 0)
                	txtCommand.setFocus();
                else if(MeshViewerProperties.meshCommandAdvanced == 1)
                	cmbCommand.setFocus();                
            }
		}); 
		
		y = y + 20;
		btnCopyToClip = new Button(mainCnv,SWT.NONE);
		btnCopyToClip.setBounds(540,y,50,20);
		btnCopyToClip.setText("Copy");
		btnCopyToClip.setToolTipText("Copy to Clipboard");
		btnCopyToClip.addSelectionListener(new SelectionAdapter(){
            public void widgetSelected(SelectionEvent arg0) {
                copyToClipBoard(); 
            }
		});
		
		btnClear = new Button(mainCnv,SWT.NONE);
		btnClear.setBounds(605,y,50,20);
		btnClear.setText("Clear");
		btnClear.setToolTipText("Clear Output");
		btnClear.addSelectionListener(new SelectionAdapter(){
            public void widgetSelected(SelectionEvent arg0) {
                clearOutput(); 
            }
		}); 

		y = y + 30;
		txtStatus = new Text(mainCnv,SWT.BORDER|SWT.MULTI|SWT.V_SCROLL|SWT.H_SCROLL|SWT.READ_ONLY);
		txtStatus.setBounds(15,y,640,315);
		txtStatus.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		txtStatus.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		
		setDefaultValues();
		fillLastEnteredCommand();
    }
    
    public void runMeshCommand() {
    	
    	if(cmdExecutor.running == true) {
    		MessageBox msgbox = new MessageBox(new Shell(SWT.OK | SWT.ICON_INFORMATION));
    		msgbox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
    		msgbox.setMessage("Command execution already in progress");
    		msgbox.open();
    		return;
    	}
    	
    	final String 	IPAddress	= txtIPAddress.getText().trim();
    	String 			command 	= "";
    	
    	if(MeshViewerProperties.meshCommandAdvanced == 0)
    		command = txtCommand.getText();
    	else {
    		if(cmbCommand.getSelectionIndex() == -1)
    			command = cmbCommand.getText();
    		else 
    			command	= cmbCommand.getItem(cmbCommand.getSelectionIndex());
    	}
       
    	if(!MeshValidations.isIpAddress(IPAddress)){
    		MessageBox msgbox = new MessageBox(new Shell(SWT.OK | SWT.ICON_INFORMATION));
    		msgbox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
    		msgbox.setMessage("IP Address invalid!!");
    		msgbox.open();
    		return;
    	}
    	NodeIPAddr.setBytes(IPAddress);
    	
    	command = command.trim();
    	if(command.equals(""))
    		return;
    	
    	Cursor waitCursor = new Cursor(Display.getDefault(), SWT.CURSOR_WAIT);
    	parentShell.setCursor(waitCursor);
    	
    	cmdExecutor 			= new CmdExecutor();
    	cmdExecutor.sCommand 	= command;
    	cmdExecutor.running 	= true;
    	cmdExecutor.start();
    }
    
    private void commandOutput(final String outputText) {
    	if(super.isShellDisposed()) {
    		return;
    	}
    	
        Display.getDefault().syncExec(new Runnable(){
            public void run() {
        		
            	txtStatus.setFont(MeshViewerGDIObjects.courierFont8);
                txtStatus.append(outputText);
                txtStatus.append("# ");
                parentShell.setCursor(MeshViewerGDIObjects.arrCursor);
            }
         });
    }
    
    private void clearOutput(){        
        
        if(MeshViewerProperties.meshCommandAdvanced == 0) {
        	txtCommand.setText("");
        	txtCommand.setFocus();
        } else if(MeshViewerProperties.meshCommandAdvanced == 1)
        	cmbCommand.setFocus();    

    	txtStatus.setText("# ");
    }
    
    private void copyToClipBoard(){
        
        Display display 			= parentShell.getDisplay();   
        final Clipboard cb 			= new Clipboard(display);
        String textData 			= txtStatus.getText();
        TextTransfer textTransfer	= TextTransfer.getInstance();
        cb.setContents(new Object[] { textData },
            new Transfer[] { textTransfer });
    }
    
    public boolean onOk() {
        return true;
    }

    public boolean onCancel() {
        return true;
    }

    public  boolean onShellExit() {
		return onCancel();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x	 	= 0;
		bounds.y	 	= 0;
		bounds.width 	= 660;
		bounds.height	= 410;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#isModal()
	 */
	protected boolean isModal() {
		return false;
	}
	
	private void fillLastEnteredCommand() {
		Enumeration<String>	enumCommands	= vectCommands.elements();
		while(enumCommands.hasMoreElements()) {
			addToCombo(enumCommands.nextElement());
		}
	}
	
	 /**
	 * 
	 */
	private void addToCombo(final String sCommand) {
		
		if(cmbCommand.isDisposed() == true) {
			return;
		}
		 MeshViewerUI.display.asyncExec (new Runnable () { 
	        public void run () {
	        	boolean	commandFound;
	    		String 	tempsCommand;
	    		int 	index;
	    		commandFound	= false;
	    		
	        	int count	= cmbCommand.getItemCount();
	    		for(index = 0; index < count; index++) {
	    			tempsCommand	= cmbCommand.getItem(index);
	    			if(sCommand.equalsIgnoreCase(tempsCommand)) {
	    				commandFound	= true;
	    			}
	    		}
	    		if(commandFound == false) {
	    			cmbCommand.add(sCommand);
	    		}
	        }
	    });
		
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onShellDispose()
	 */
	protected void onShellDispose() {
		if(cmdExecutor.running == true)
			cmdExecutor.interrupt();
	}
	
}
