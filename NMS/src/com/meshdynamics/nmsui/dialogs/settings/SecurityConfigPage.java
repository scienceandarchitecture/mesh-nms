/**
 * MeshDynamics 
 * -------------- 
 * File     : SecurityConfigPage.java
 * Comments : 
 * Created  : Feb 22, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Feb 22, 2007   | Created                        			  | Abhishek     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.dialogs.settings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Rectangle;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.PortInfo;
import com.meshdynamics.nmsui.PortModelInfo;
import com.meshdynamics.nmsui.dialogs.base.ConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.dialogs.frames.portmodeinfo.IPortModelInfoListener;
import com.meshdynamics.nmsui.dialogs.frames.portmodeinfo.PortModelInfoFrame;
import com.meshdynamics.nmsui.dialogs.frames.portmodeinfo.PortModelInfoSelectionEvent;
import com.meshdynamics.nmsui.dialogs.frames.security.ISecurityFrameListener;
import com.meshdynamics.nmsui.dialogs.frames.security.SecurityFrame;
import com.meshdynamics.nmsui.dialogs.frames.security.SecurityFrameSelectionEvent;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.nmsui.wizards.fips.FipsSecurityHelper;

public class SecurityConfigPage extends ConfigPage implements
		IPortModelInfoListener, ISecurityFrameListener {

	private PortModelInfoFrame portModelFrame;
	private SecurityFrame securityFrame;
	private PortModelInfo pmInfo;
	private int rel_x;
	private int rel_y;
	private boolean fipsEnabled;

	public SecurityConfigPage(IMeshConfigBook parent) {
		super(parent);
		fipsEnabled = false;
	}

	/**
	 * Function for creating various controls for SecurityConfigPage
	 */
	public void createControls(CTabFolder tabFolder) {

		super.createControls(tabFolder);

		rel_x = 10;
		rel_y = 10;

		createPortFrameControls();

		rel_y += 20;

		createSecurityFrameControls();

		ContextHelp.addContextHelpHandlerEx(this.canvas,
				contextHelpEnum.CONFIGTABSECURITY);
	}

	/**
	 * Function for creating PortModelFrame
	 */
	private void createPortFrameControls() {
		int radioCount;
		int radioIndex;

		IConfiguration config = parent.getConfiguration();
		if (config == null) {
			return;
		}

		pmInfo = new PortModelInfo(config.getHardwareModel(), config
				.getDSMacAddress().toString(),
				config.getInterfaceConfiguration());
		portModelFrame = new PortModelInfoFrame(canvas, SWT.NONE, pmInfo, this);
		Rectangle frmBounds = portModelFrame.getMinimumBounds();
		portModelFrame.setBounds(rel_x, rel_y, frmBounds.width,
				frmBounds.height);

		radioCount = portModelFrame.getRadioCount();
		for (radioIndex = 0; radioIndex < radioCount; radioIndex++) {

			PortInfo portInfo = portModelFrame.getPortInfo(radioIndex);
			if (portInfo == null) {
				continue;
			}

			if ((portInfo.typeOfRadio == PortModelInfo.TYPESCANNER)
					|| (portInfo.typeOfRadio == PortModelInfo.TYPE900MHZ)
					|| (portInfo.typeOfRadio == PortModelInfo.TYPENOTUSED)) {
				portModelFrame.enableRadio(radioIndex, false);
			}
		}

	}

	/**
	 * Function for creating Security Frame
	 */
	private void createSecurityFrameControls() {

		int disableControls = 0;
		fipsEnabled = false;
		IConfiguration configuration = parent.getConfiguration();
		if (configuration == null) {
			return;
		}
		this.fipsEnabled = configuration.isFipsEnabled();
		if (this.fipsEnabled == true) {
			disableControls |= SecurityFrame.DISABLE_WEP;
			disableControls |= SecurityFrame.DISABLE_WPA_ENTERPRISE;
			disableControls |= SecurityFrame.DISABLE_TKIP;
		}

		IVersionInfo versionInfo = parent.getVersionInfo();

		if (versionInfo.versionLessThan(IVersionInfo.MAJOR_VERSION_2,
				IVersionInfo.MINOR_VERSION_5, IVersionInfo.VARIANT_VERSION_30) == true) {
			disableControls |= SecurityFrame.DISABLE_WPA2_MODE;
			disableControls |= SecurityFrame.DISABLE_RAD_DECIDES_VLAN_CONTROLS;
		}

		if (versionInfo.versionLessThan(IVersionInfo.MAJOR_VERSION_2,
				IVersionInfo.MINOR_VERSION_5, IVersionInfo.VARIANT_VERSION_40) == true) {
			disableControls |= SecurityFrame.DISABLE_WEP_KEY1;
		}

		securityFrame = new SecurityFrame(canvas, this, disableControls,
				fipsEnabled, SecurityFrame.PARENT_SECURITY_CONFIG_PAGE);
		Rectangle temp = securityFrame.getMinimumBounds();
		securityFrame.setBounds(rel_x, rel_y + 40, temp.width, temp.height);
		securityFrame.setFrameTitle("Interface Security");
		securityFrame.showFrame(false);

	}

	public void selectionChanged(boolean selected) {

		int currentSelectedInterface;
		int userSays;

		if (selected == false) {

			PortInfo portInfo = portModelFrame.getPortInfo(portModelFrame
					.getLastSelectedRadioIndex());

			if ((portInfo.typeOfRadio == PortModelInfo.TYPESCANNER)
					|| (portInfo.typeOfRadio == PortModelInfo.TYPEUPLINK)
					|| (portInfo.typeOfRadio == PortModelInfo.TYPENOTUSED)) {
				return;
			}

			if (validateLocalData() == false) {
				stayOnSameTab();
				return;
			}

			if (compairConfigurationWithUI() == false) {

				userSays = parent
						.showMessage(
								"You have changed Security settings for interface. "
										+ SWT.LF
										+ "Do you want to save these changes?",
								SWT.YES | SWT.CANCEL | SWT.NO
										| SWT.ICON_QUESTION);

				if (userSays == SWT.YES) {
					getDataFromUIToConfiguration();
				}
				if (userSays == SWT.NO) {
					revertChanges();
				}
				if (userSays == SWT.CANCEL) {
					stayOnSameTab();
				}
			}
		} else {
			refreshConfiguration();
			currentSelectedInterface = portModelFrame
					.getCurrentSelectedRadioIndex();
			setInterfaceSecurity(currentSelectedInterface);
		}
	}

	public void initalizeLocalData() {

		int radioCount;
		int index;
		String interfaceName;

		radioCount = portModelFrame.getRadioCount();
		IConfiguration iConfig = parent.getConfiguration();
		if (iConfig == null) {
			return;
		}

		IInterfaceConfiguration interfaceConfig = iConfig
				.getInterfaceConfiguration();
		if (interfaceConfig == null) {
			return;
		}

		for (index = 0; index < radioCount; index++) {

			PortInfo portInfo = portModelFrame.getPortInfo(index);

			if ((portInfo.typeOfRadio == PortModelInfo.TYPESCANNER)
					|| (portInfo.typeOfRadio == PortModelInfo.TYPEUPLINK)
					|| (portInfo.typeOfRadio == PortModelInfo.TYPENOTUSED)) {
				continue;
			}

			interfaceName = portInfo.ifName;
			IInterfaceInfo interfaceInfo = (IInterfaceInfo) interfaceConfig
					.getInterfaceByName(interfaceName);
			if (interfaceInfo == null) {
				continue;
			}

			ISecurityConfiguration securityInfo = (ISecurityConfiguration) interfaceInfo
					.getSecurityConfiguration();
			if (securityInfo == null) {
				continue;
			}
			portModelFrame.setRadioDataByName(interfaceName, securityInfo);
		}

	}

	/**
	 * 
	 */
	private boolean compairConfigurationWithUI() {

		ISecurityConfiguration securityInfo = (ISecurityConfiguration) portModelFrame
				.getRadioDataByIndex(portModelFrame.getLastSelectedRadioIndex());
		if (securityInfo == null) {
			return false;
		}

		return securityFrame.compareConfigurationWithUI(securityInfo);

	}

	/**
	 * 
	 */
	private void refreshConfiguration() {
		initalizeLocalData();

	}

	public void radioSelected(PortModelInfoSelectionEvent event) {

		int userSays;

		PortInfo portInfo = portModelFrame.getPortInfo(portModelFrame
				.getLastSelectedRadioIndex());
		if ((portInfo.typeOfRadio == PortModelInfo.TYPESCANNER)
				|| (portInfo.typeOfRadio == PortModelInfo.TYPEUPLINK)
				|| (portInfo.typeOfRadio == PortModelInfo.TYPENOTUSED)) {

			setInterfaceSecurity(event.getRadioIndex());
			return;
		}

		if (validateLocalData() == false) {
			doNotSwitchRadio();
			return;
		}

		if (compairConfigurationWithUI() == false) {

			userSays = parent.showMessage("You have changed security settings"
					+ SWT.LF + "Do you want to save these changes?", SWT.YES
					| SWT.CANCEL | SWT.NO | SWT.ICON_QUESTION);

			if (userSays == SWT.YES) {
				getDataFromUIToConfiguration();
				setInterfaceSecurity(event.getRadioIndex());
			}
			if (userSays == SWT.NO) {
				revertChanges();
				setInterfaceSecurity(event.getRadioIndex());
			}
			if (userSays == SWT.CANCEL) {
				doNotSwitchRadio();
			}
		} else {
			setInterfaceSecurity(event.getRadioIndex());
		}

	}

	public void setModified() {

	}

	/**
	 * @param radioIndex
	 */
	private void setInterfaceSecurity(int radioIndex) {

		PortInfo portInfo = portModelFrame.getPortInfo(radioIndex);
		IInterfaceInfo info = parent.getInterfaceInfoByName(portInfo.ifName);
		if (info.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_24GHz_N
				|| info.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_5GHz_N
				|| info.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_AN
				|| info.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_BGN
				|| info.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_AC
				|| info.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_ANAC)
			securityFrame.disable_wep(SecurityFrame.DISABLE_WEP);
		else
			securityFrame.disable_wep(SecurityFrame.STATE_NO_SECURITY);

		if (portInfo.typeOfRadio == PortModelInfo.TYPEUPLINK) {
			securityFrame.setRadiosVisible(false);
			securityFrame.showUserMessage(SecurityFrame.UPLINK_MESSAGE);
		} else {
			securityFrame.setRadiosVisible(true);
			securityFrame.showUserMessage(SecurityFrame.NO_MESSAGE);
		}
		if ((portInfo.typeOfRadio == PortModelInfo.TYPESCANNER)
				|| (portInfo.typeOfRadio == PortModelInfo.TYPEUPLINK)
				|| (portInfo.typeOfRadio == PortModelInfo.TYPENOTUSED)) {

			securityFrame.clearAll();
			securityFrame.showFrame(false);
			return;
		} else {
			securityFrame.showFrame(true);
		}
		
		if (info.getMediumType() != Mesh.PHY_TYPE_802_11_VIRTUAL) {
			if (findLogicalInterface() == true
					&& portModelFrame.getPortInfo(portInfo.ifName).freqOfRadio
							.equals("5G")) {
				securityFrame.showFrame(false);
			}
		}
		ISecurityConfiguration securityInfo = (ISecurityConfiguration) portModelFrame
				.getRadioDataByIndex(radioIndex);

		if (securityInfo == null) {
			return;
		}

		securityFrame.setSecurityValues(securityInfo);
		securityFrame.setEssid(parent.getIfEssIdByName(portInfo.ifName));

	}

	public void btnChangePressed() {
	}

	public void securitySelected(SecurityFrameSelectionEvent sfsEvent) {
		// to do
	}

	public boolean validateLocalData() {
		return securityFrame.validateLocalData();
	}

	/**
	 * 
	 */
	private void doNotSwitchRadio() {

		int radioCount;
		int radioIndex;

		radioCount = portModelFrame.getRadioCount();

		for (radioIndex = 0; radioIndex < radioCount; radioIndex++) {
			if (radioIndex == portModelFrame.getLastSelectedRadioIndex()) {
				portModelFrame.setRadioSelection(radioIndex, true);
			} else {
				portModelFrame.setRadioSelection(radioIndex, false);

			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.meshdynamics.meshviewer.dialogs.IErrorMessage#showErrorMessage(java
	 * .lang.String, int)
	 */
	public int showMessage(String errorString, int style) {
		return parent.showMessage(errorString, style);
	}

	/**
	 * 
	 */
	public boolean getDataFromUIToConfiguration() {

		int previousSecurity;
		int newSecurity;

		PortInfo portInfo = portModelFrame.getPortInfo(portModelFrame
				.getLastSelectedRadioIndex());
		if ((portInfo.typeOfRadio == PortModelInfo.TYPESCANNER)
				|| (portInfo.typeOfRadio == PortModelInfo.TYPEUPLINK)
				|| (portInfo.typeOfRadio == PortModelInfo.TYPENOTUSED)) {
			return true;
		}

		ISecurityConfiguration securityInfo = (ISecurityConfiguration) portModelFrame
				.getRadioDataByIndex(portModelFrame.getLastSelectedRadioIndex());

		if (securityInfo == null) {
			return false;
		}
		previousSecurity = securityInfo.getEnabledSecurity();
		securityFrame.getDataFromUIToConfiguration(securityInfo);
		newSecurity = securityInfo.getEnabledSecurity();
		if (previousSecurity == ISecurityConfiguration.SECURITY_TYPE_WPA_PERSONAL
				&& newSecurity != ISecurityConfiguration.SECURITY_TYPE_WPA_PERSONAL) {
			parent.notifySecurityConfigurationChangedFor(portInfo.ifName);
		}/*
		 * this notification is to tell config dialog for changed security
		 * configuration in case of essId regeneration for wpaPersonal security
		 */
		portModelFrame.setRadioDataByIndex(
				portModelFrame.getLastSelectedRadioIndex(), null);
		portModelFrame.setRadioDataByIndex(
				portModelFrame.getLastSelectedRadioIndex(), securityInfo);

		return true;
	}

	/**
	 * 
	 */
	private void revertChanges() {
		setInterfaceSecurity(portModelFrame.getLastSelectedRadioIndex());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.meshviewer.dialogs.ConfigPage#isFipsFulfilled()
	 */
	public boolean isFipsFulfilled() {
		int radioCount;
		int index;
		String interfaceName;

		radioCount = portModelFrame.getRadioCount();
		IConfiguration iConfig = parent.getConfiguration();
		if (iConfig == null) {
			return false;
		}

		IInterfaceConfiguration interfaceConfig = iConfig
				.getInterfaceConfiguration();
		if (interfaceConfig == null) {
			return false;
		}

		for (index = 0; index < radioCount; index++) {

			PortInfo portInfo = portModelFrame.getPortInfo(index);

			if ((portInfo.typeOfRadio == PortModelInfo.TYPESCANNER)
					|| (portInfo.typeOfRadio == PortModelInfo.TYPEUPLINK)
					|| (portInfo.typeOfRadio == PortModelInfo.TYPENOTUSED)) {
				continue;
			}

			interfaceName = portInfo.ifName;
			IInterfaceInfo interfaceInfo = (IInterfaceInfo) interfaceConfig
					.getInterfaceByName(interfaceName);
			if (interfaceInfo == null) {
				continue;
			}

			ISecurityConfiguration securityInfo = (ISecurityConfiguration) interfaceInfo
					.getSecurityConfiguration();
			if (securityInfo == null) {
				continue;
			}
			if (FipsSecurityHelper.isSecurityFipsCompliant(securityInfo) == false) {
				portModelFrame.setRadioSelection(
						portModelFrame.getLastSelectedRadioIndex(), false);
				portModelFrame.setRadioSelection(index, true);
				setInterfaceSecurity(index);
				stayOnSameTab();

				parent.showMessage("This configuration is FIPS 140-2 enabled."
						+ SWT.LF + "Security is not enabled for "
						+ interfaceInfo.getName(), SWT.ICON_INFORMATION);
				return false;
			}

		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.meshviewer.dialogs.ISecurityFrameListener#
	 * notifyWPAPersonalKeyRegenerated()
	 */
	public void notifyWPAPersonalKeyRegenerated() {
		String ifName = portModelFrame.getLastSelectedRadioName();
		parent.notifyWPAPersonalKeyRegeneratedFor(ifName);
	}

	/*
	 * to find logical interface
	 */
	private boolean findLogicalInterface() {

		IConfiguration iConfig = parent.getConfiguration();
		boolean flag = false;
		int radioCount;
		String interfaceName;
		radioCount = portModelFrame.getRadioCount();
		IInterfaceConfiguration interfaceConfig = iConfig
				.getInterfaceConfiguration();
		if (interfaceConfig == null) {
			return false;
		}
		for (int i = 0; i < radioCount; i++) {
			PortInfo portInfo = portModelFrame.getPortInfo(i);
			interfaceName = portInfo.ifName;
			IInterfaceInfo interfaceInfo = (IInterfaceInfo) interfaceConfig
					.getInterfaceByName(interfaceName);
			if (interfaceInfo == null) {
				continue;
			}
			if (interfaceInfo.getMediumType() == 2)
				flag = true;
		}
		return flag;
	}

}
