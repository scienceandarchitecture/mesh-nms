/**
 * MeshDynamics 
 * -------------- 
 * File     : VLANConfigPage.java
 * Comments : 
 * Created  : Feb 22, 2007 
 * Author   : Imran 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No |Date            |  Comment                                   | Author       |
 * -----------------------------------------------------------------------------------
 * | 3  |Apr   10, 2007  | getDataFromUiToConfigutration added        | Imran        |
 * -----------------------------------------------------------------------------------
 * | 2  |May   12, 2007  | compareConfigurationWithUi added           | Imran        |
 * -----------------------------------------------------------------------------------
 * | 1  | Apr 05, 2007   | UI validation are added        			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 22, 2007   | Created                        			  | Abhijeet     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.dialogs.settings;


import java.util.Hashtable;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.controls.Slider;
import com.meshdynamics.nmsui.dialogs.UIConstants;
import com.meshdynamics.nmsui.dialogs.base.ConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.dialogs.frames.security.ISecurityFrameListener;
import com.meshdynamics.nmsui.dialogs.frames.security.SecurityFrame;
import com.meshdynamics.nmsui.dialogs.frames.security.SecurityFrameSelectionEvent;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.nmsui.wizards.fips.FipsSecurityHelper;
import com.meshdynamics.util.IStateEventHandler;
import com.meshdynamics.util.StateManager;

public class VLANConfigPage extends ConfigPage implements ISecurityFrameListener, IStateEventHandler{
	
	private ToolItem 					btnNew,btnEdit,btnSave,btnDelete;
	private List						lstVlan;
	private Text						txtVlanName,txtTagging,txtVlanEssid;
	private Slider						sld8021P;
	private Label						lbl8021P;
	private Button						btnDot11eEnabled;
	private Combo						cmbDot11eCategory;
	private SecurityFrame				securityFrame;
	private StateManager 				sMananger;
	private boolean						fipsEnabled;
	private Vector<String>				deletedVlans;
	private int 						lastSelectedVlanIndex;

	private Hashtable<String, WPAKeyGenerationStatusHolder> wpaGenerationStatus;
	
	private final int STATE_START		=	0;
	private final int STATE_NEW  		=	1;
	private final int STATE_EDIT 		=	2;
	private final int STATE_SAVE 		=	3;
	private final int STATE_DELETE		=	4;
	private final int STATE_SELECT		=   5; 
	
	private final int MAX_STATE_CNT		=	6;
	private final short VLAN_TAG_MAX_VALUE	= 	4095;
	private final String DEFAULT_VLAN_NAME	=	"default";
	
	private class WPAKeyGenerationStatusHolder{
		String vlanName;
		boolean keyRegenerated;
	}
	
	public VLANConfigPage(IMeshConfigBook parent) {
		super(parent);
		minimumBounds.width   	= 350;
		minimumBounds.height  	= 450;
		sMananger 			  	= new StateManager(this,MAX_STATE_CNT,STATE_START);
		deletedVlans		  	= new Vector<String>();
		wpaGenerationStatus		= new Hashtable<String, WPAKeyGenerationStatusHolder>();
		lastSelectedVlanIndex	= -1;
		
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IStateEventHandler#init()
	 */
	public void init() {
		sMananger.triggerState(STATE_START); 
	}
	
	public void createControls(CTabFolder parent) {
		super.createControls(parent);
		createVlanListcontrols();
		createVlanControls();
		createVlanSecurityControls();
		sMananger.triggerState(STATE_START);
		ContextHelp.addContextHelpHandlerEx(this.canvas,contextHelpEnum.CONFIGTABVLAN);
	}
	
	private void createVlanListcontrols() {
		
		int rel_y = 10;
		
		ToolBar toolBar = new ToolBar(canvas,SWT.FLAT);
		toolBar.setBounds(15,rel_y,100,25);		
              
		btnNew	= new ToolItem(toolBar,SWT.IMAGE_BMP);
		btnNew.setImage(UIConstants.imgNew);
		btnNew.setToolTipText("New");
		btnNew.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				notifyStateChanged(STATE_NEW);				
			}
		});

		btnEdit	= new ToolItem(toolBar,SWT.IMAGE_BMP);
		btnEdit.setImage(UIConstants.imgEdit); 
		btnEdit.setToolTipText("Edit");
		btnEdit.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				notifyStateChanged(STATE_EDIT);
			}
		});		
		
		btnSave	= new ToolItem(toolBar,SWT.IMAGE_BMP);
		btnSave.setImage(UIConstants.imgSave); 
		btnSave.setToolTipText("Save");
		btnSave.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				notifyStateChanged(STATE_SAVE);
			}
		});		
		
		btnDelete	= new ToolItem(toolBar,SWT.IMAGE_BMP);
		btnDelete.setToolTipText("Delete");
		btnDelete.setImage(UIConstants.imgDel);
		btnDelete.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				notifyStateChanged(STATE_DELETE);
			}
		});		
		
		rel_y += 25;
		lstVlan	=	new List(canvas,SWT.BORDER|SWT.SINGLE|SWT.V_SCROLL);
		lstVlan.setBounds(18,rel_y,110,105);
		lstVlan.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				lstVlanHandler();
			}
		});
	
	}
	
	private void lstVlanHandler() {
		int index;
		
		if(deletedVlans.size() > 0) {
			showMessage("One of the VLAN entry is deleted. "+SWT.LF+
					"You must save these changes", SWT.OK|SWT.ICON_ERROR);
			lstVlan.deselectAll();
			return;
		}
		index					= lstVlan.getSelectionIndex();
		if(lastSelectedVlanIndex == index) {
			return;
		}
		
		if(sMananger.getState() == STATE_EDIT || sMananger.getState() == STATE_NEW) {
			parent.showMessage("You must save currently selected VLAN entry. ",SWT.ICON_INFORMATION|SWT.OK);
			lstVlan.deselectAll();
			lstVlan.select(lastSelectedVlanIndex);
			return;
		}
		
		index					= lstVlan.getSelectionIndex(); 
		notifyStateChanged(STATE_SELECT);
		showVlanValues(index);
		lastSelectedVlanIndex	= index; 
	
	}		
		
	private void createVlanControls() {
		
		int rel_y 					= 35;
	    int rel_x					= 135;
	    int height					= 17;
	    int controlHeightSpacing	= 20;
	    
		Label lbl = new Label(canvas,SWT.NO);
		lbl.setBounds(rel_x,rel_y,50,height);
		lbl.setText("Name");
		
		txtVlanName	= new Text(canvas,SWT.BORDER);
		txtVlanName.setBounds(rel_x+100,rel_y,150,height);
		rel_y += controlHeightSpacing + 5;
		lbl	= new Label(canvas,SWT.NO);
		lbl.setBounds(rel_x,rel_y,55,height);
		lbl.setText("ESSID");
		
		txtVlanEssid = new Text(canvas,SWT.BORDER);
		txtVlanEssid.setBounds(rel_x+100,rel_y,150,height);
		txtVlanEssid.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
			    			
			}
	
			public void keyReleased(KeyEvent arg0) {
				securityFrame.setEssid(txtVlanEssid.getText());
				//securityFrame.generateWPAPersonalKey();
			}
	    });
		
		rel_y += controlHeightSpacing;
		lbl	= new Label(canvas,SWT.NO);
		lbl.setBounds(rel_x,rel_y,55,height);
		lbl.setText("Tagging");
		
		txtTagging	= new Text(canvas,SWT.BORDER);
		txtTagging.setBounds(rel_x+100,rel_y,70,height);
		txtTagging.setToolTipText("VLAN Tag range 0 to "+VLAN_TAG_MAX_VALUE+".");
	
		lbl	= new Label(canvas,SWT.NO);
		lbl.setBounds(rel_x+180,rel_y,65,height);
		lbl.setText("( 0 to "+VLAN_TAG_MAX_VALUE+" )");
		
		rel_y += controlHeightSpacing;
		lbl	= new Label(canvas,SWT.NO);
		lbl.setBounds(rel_x,rel_y,90,height);
		lbl.setText("802.11e Category");
		
		btnDot11eEnabled = new Button(canvas,SWT.CHECK);
		btnDot11eEnabled.setBounds(rel_x+100,rel_y,19,20);
		btnDot11eEnabled.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				if(btnDot11eEnabled.getSelection() == true) {
					btnDot11eEnabled.setSelection(true);
					cmbDot11eCategory.setEnabled(true);
					sld8021P.setEnabled(true);
					lbl8021P.setEnabled(true);
				} else {
					cmbDot11eCategory.select(0);
					cmbDot11eCategory.setEnabled(false);
					sld8021P.setEnabled(false);
					lbl8021P.setEnabled(false);
					sld8021P.setValue(0);
				}
			}			
		});

		cmbDot11eCategory = new Combo(canvas,SWT.READ_ONLY|SWT.BORDER);
		cmbDot11eCategory.setBounds(rel_x+130,rel_y,120,height);
		
		cmbDot11eCategory.add("Background");
		cmbDot11eCategory.add("Best Effort");
		cmbDot11eCategory.add("Video");
		cmbDot11eCategory.add("Voice");
		cmbDot11eCategory.select(0);

		rel_y += controlHeightSpacing+5;
		lbl	= new Label(canvas,SWT.NO);
		lbl.setBounds(rel_x,rel_y,70,height);
		lbl.setText("802.1P Priority");

		rel_y +=3;
		sld8021P = new Slider(canvas,SWT.FLAT);
		sld8021P.setBounds(rel_x+100,rel_y+3,150,height);
		sld8021P.setMaxValue(7);
		sld8021P.setMinValue(0);
		sld8021P.setChangeValue(2);
		sld8021P.addValueListener(new Listener(){
			public void handleEvent(Event arg0) {
				lbl8021P.setText(""+sld8021P.getValue() + " Units");				
			}			
		});

		lbl8021P = new Label(canvas,SWT.NO);
		lbl8021P.setBounds(rel_x+255,rel_y,35,height);
		lbl8021P.setText("0 Units");
	}
	
	public void createVlanSecurityControls(){
		
		short disableControls;
		
		IConfiguration	configuration	= parent.getConfiguration();
		if(configuration == null) {
			return;
		}
		this.fipsEnabled	= configuration.isFipsEnabled();
		
		disableControls	= 0;
		
		disableControls	|= SecurityFrame.DISABLE_WEP;
		
		IVersionInfo	versionInfo	= parent.getVersionInfo();
		if(versionInfo.versionLessThan(IVersionInfo.MAJOR_VERSION_2,
				IVersionInfo.MINOR_VERSION_5, IVersionInfo.VARIANT_VERSION_30) == true){
			disableControls	|= SecurityFrame.DISABLE_WPA2_MODE;
			disableControls	|= SecurityFrame.DISABLE_RAD_DECIDES_VLAN_CONTROLS;
			
		}
			
		securityFrame		= new SecurityFrame(canvas,this,disableControls, fipsEnabled, SecurityFrame.PARENT_VLAN_CONFIG_PAGE);
		Rectangle	temp	= securityFrame.getMinimumBounds();
		securityFrame.setBounds(10, 50, temp.width, temp.height-50);
		securityFrame.setFrameTitle("VLAN Security");
	}

	public void initalizeLocalData() {
		
		clearAllControls();
		lstVlan.removeAll();
		deletedVlans.clear();
		
		IConfiguration	iConfiguration	= parent.getConfiguration();
		if(iConfiguration == null){
			return;
		}
		
		IVlanConfiguration	vlanConfig	= iConfiguration.getVlanConfiguration();
		if(vlanConfig == null) {
			return;
		}
		
		int vlanCount	= vlanConfig.getVlanCount(); 
		
		for(int vlanIndex=0; vlanIndex<vlanCount; vlanIndex++) {
		
			IVlanInfo	vlanInfo	= (IVlanInfo)vlanConfig.getVlanInfoByIndex(vlanIndex);
			if(vlanInfo == null) {
				continue;
			}
			
			lstVlan.add(vlanInfo.getName());
			lstVlan.setData(vlanInfo.getName(),vlanInfo);
		}
		if(vlanCount == 0) {
			return;
		}
		showVlanValues(lastSelectedVlanIndex);
	}

	/**
	 * @param vlanInfo
	 */
	private void showVlanValues(int vlanIndex) {
		
		String vlanName;
		String vlanEssid;
		
		if(lstVlan.getItemCount() == 0) {
			clearAllControls();
			return;
		}
		
		if(vlanIndex == -1) {
			return;
		}
		
		lstVlan.select(vlanIndex);
				
		vlanName				= lstVlan.getItem(vlanIndex);
		
		IVlanInfo	vlanInfo	= (IVlanInfo)lstVlan.getData(vlanName);
		displayVlanData(vlanInfo);
		
		ISecurityConfiguration	vlanSecurity	= (ISecurityConfiguration)vlanInfo.getSecurityConfiguration();
		vlanEssid	= vlanInfo.getEssid();
		displayVlanSecurity(vlanSecurity,vlanEssid);
	
	}
	
	/**
	 * @param displays vlan information on the tab
	 */
	private void displayVlanData(IVlanInfo vlanInfo) {
		
		if(vlanInfo == null) {
			return;
		}
		
		txtVlanName.setText(vlanInfo.getName());
		txtTagging.setText(Integer.toString(vlanInfo.getTag()));
		sld8021P.setValue(vlanInfo.get8021pPriority());
		if(vlanInfo.get80211eEnabled() == (short)1) {
			btnDot11eEnabled.setSelection(true);
		}else {
			btnDot11eEnabled.setSelection(false);
		}
		cmbDot11eCategory.select(vlanInfo.get80211eCategoryIndex());
		txtVlanEssid.setText(vlanInfo.getEssid());
		
	}
	
	

	/**
	 * @param displays vlan security information on security frame
	 */
	private void displayVlanSecurity(ISecurityConfiguration vlanSecurity, String vlanEssid) {
		
		if(vlanSecurity == null) {
			return;
		}
		securityFrame.setSecurityValues(vlanSecurity);
		securityFrame.setEssid(vlanEssid);
	}

	public void selectionChanged(boolean selected) {
		
		if(selected == false){
			
			if((sMananger.getState() == STATE_START) ||
					(sMananger.getState() == STATE_SAVE) ||
					(sMananger.getState() == STATE_SELECT)) {
				return;
			}
			
			if(validateLocalData() == false){
				stayOnSameTab();
				return;
			}
			
			if(chkForEssid() == false) {
				stayOnSameTab();
				return;
			}
			
			if(sMananger.getState() == STATE_NEW) {
				int userSays	= parent.showMessage("Newly created VLAN entry is not saved. "+SWT.LF+
						"Do you want to save these changes? ", SWT.YES|SWT.CANCEL|SWT.NO|SWT.ICON_QUESTION);
	
				if(userSays == SWT.YES){
					getDataFromUiToLocalConfiguration();
					
				} else if(userSays == SWT.NO){
					notifyStateChanged(STATE_START);
				} else if(userSays == SWT.CANCEL){
					stayOnSameTab();
				}
				return;
			}
			
			if(compairConfigurationWithUI() == false){	
				int userSays	= parent.showMessage("You have changed VLAN settings. "+SWT.LF+
						"Do you want to save these changes? ", SWT.YES|SWT.CANCEL|SWT.NO|SWT.ICON_QUESTION);
	
				if(userSays == SWT.YES){
					//getDataFromUiToLocalConfiguration();
					notifyStateChanged(STATE_SAVE);
				} else if(userSays == SWT.NO){
					notifyStateChanged(STATE_START);
				} else if(userSays == SWT.CANCEL){
					stayOnSameTab();
				}
				return;
			}
			
			if(sMananger.getState() == STATE_EDIT) {	
				
				int userSays	= parent.showMessage("VLAN settings are not changed. "+SWT.LF+
						"Do you want to continue? ", SWT.YES|SWT.NO|SWT.ICON_QUESTION);
			
				if(userSays == SWT.YES) {
					notifyStateChanged(STATE_SAVE);
				} else if(userSays == SWT.NO) {
					stayOnSameTab();
				}
				return;
			}
		}else{
			refreshConfiguration();
		}
	}

	private boolean compairConfigurationWithUI() {
		int index;
		if(deletedVlans.size() > 0){
			return false;
		}
			
		index 				= lstVlan.getSelectionIndex();
	
		IVlanInfo vlanInfo	= (IVlanInfo)lstVlan.getData(lstVlan.getItem(index));
		
		if(vlanInfo == null) {
			return false;
		}
		if(vlanInfo.getName().equalsIgnoreCase(txtVlanName.getText().trim()) == false) {
			return false; 
		}
		if(vlanInfo.getTag() != Integer.parseInt(txtTagging.getText().trim())) {
			return false;
		}
		if(vlanInfo.get8021pPriority() != sld8021P.getValue()) {
			return false;
		}
		if(vlanInfo.getEssid().equalsIgnoreCase(txtVlanEssid.getText().trim()) == false) {
			return false;
		}
		short dot11eEnabled;
		if(btnDot11eEnabled.getSelection() == true) {
			dot11eEnabled	= 1;
		}else {
			dot11eEnabled	= 0;
		}
		if(vlanInfo.get80211eEnabled() != dot11eEnabled) {
			return false;
		}
		if(vlanInfo.get80211eCategoryIndex() != cmbDot11eCategory.getSelectionIndex()) {
			return false;
		}
		
		ISecurityConfiguration	vlanSecurityInfo	= (ISecurityConfiguration)vlanInfo.getSecurityConfiguration();
		if(vlanSecurityInfo == null) {
			return false; 
		}
		
		return securityFrame.compareConfigurationWithUI(vlanSecurityInfo);
			
	}
	
	/**
	 * 
	 */
	private void refreshConfiguration() {
		initalizeLocalData();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IStateEventHandler#notifyStateChanged(int)
	 */
	public void notifyStateChanged(int state) {
		sMananger.triggerState(state);  
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IStateEventHandler#stateFunction(int)
	 */
	public int stateFunction(int state) {
		
		switch(state){
		
			case STATE_START	:	executeStartState();
									break;
			
			case STATE_NEW		:   createNewVlan();
									break;
			
			case STATE_EDIT		:	editVlan();
									break;
			
			case STATE_SAVE		:	if(saveVlan() == false) {
										return sMananger.getState();
									}
									break;
			
			case STATE_DELETE	:	if(sMananger.getState() == STATE_NEW) {
										notifyStateChanged(STATE_START);
										return sMananger.getState();
									}
									if(delateVlan() == false) {
										return sMananger.getState();
									}
									break;
									
			case STATE_SELECT	:	selectVlan();
									break;
		}
		return state;
	}

	/**
	 * 
	 */
	private void selectVlan() {
		
		btnSave.setEnabled(false);
		btnDelete.setEnabled(true);
		btnEdit.setEnabled(true);
		btnNew.setEnabled(true);
		
		enableAllControls(false);
		
	}

	/**
	 * 
	 */
	private void executeStartState() {
		
		btnSave.setEnabled(false);
		btnDelete.setEnabled(false);
		btnEdit.setEnabled(false);
		btnNew.setEnabled(true);
		
		enableAllControls(false);
		lastSelectedVlanIndex	= -1;
	}

	/**
	 * 
	 */
	private boolean delateVlan() {
		
		clearAllControls();
		
		btnSave.setEnabled(true);
		btnDelete.setEnabled(false);
		btnEdit.setEnabled(false);
		btnNew.setEnabled(false);
		
		enableAllControls(false);
				
		return deleteVlanEntry();
		
	}
	
	private boolean deleteVlanEntry() {
		
		int 	index;
		String 	vlanName;
		
		index	= lstVlan.getSelectionIndex();
		
		vlanName				= lstVlan.getItem(index);
		IVlanInfo	vlanInfo	= (IVlanInfo)lstVlan.getData(vlanName);
		if(vlanInfo == null) {
			return false;
		}
		if(parent.canDeleteVlan(vlanInfo.getTag()) == false) {
			notifyStateChanged(STATE_SELECT);
			return false;
		}
		lstVlan.remove(index);
		deletedVlans.add(vlanName);
		return true;
	}

	/**
	 * 
	 */
	private void createNewVlan() {
		
		if(deletedVlans.size() > 0) {
			parent.showMessage("One or more VLAN entries are deleted."+SWT.LF+
					"You must save the changes. ",SWT.ICON_INFORMATION|SWT.OK);
			return;
		}
		clearAllControls();
		btnSave.setEnabled(true);
		btnDelete.setEnabled(true);
		btnEdit.setEnabled(false);
		btnNew.setEnabled(false);
		
		enableAllControls(true);
		txtVlanName.setFocus();
		lstVlan.deselectAll();
		lastSelectedVlanIndex	= -1;
		/* following line is  for the dependencies of btnDot11eEnabled on cmbDot11eCategory
		 * and btnDot11eEditCategory
		 */
		cmbDot11eCategory.setEnabled(false);
		sld8021P.setEnabled(false);
		lbl8021P.setEnabled(false);
		sld8021P.setValue(0);
		securityFrame.setEssid("");
	}
	
	private boolean saveVlan() {
		
		boolean vlanSaved;
		boolean vlanDeleted;
		
		vlanSaved	= false;
		vlanDeleted	= false;
		
		if(deletedVlans.size() > 0){
			vlanDeleted	= deleteVlan();
			if(vlanDeleted == false) {
				return false;
			}
		}else {
			vlanSaved	= addVlanEntry();
			if(vlanSaved == false) {
				return false;
			}
		}
		if(vlanSaved == true){
			btnSave.setEnabled(false);
			btnDelete.setEnabled(true);
			btnEdit.setEnabled(true);
			btnNew.setEnabled(true);
			enableAllControls(false);
		}else if(vlanDeleted == true) {
			btnSave.setEnabled(false);
			btnDelete.setEnabled(false);
			btnEdit.setEnabled(false);
			btnNew.setEnabled(true);
			enableAllControls(false);
		}
		return true;
	}

	/**
	 * 
	 */
	private boolean deleteVlan() {
		String vlanName;
		
		IConfiguration	iConfiguration	= parent.getConfiguration();
		if(iConfiguration == null){
			return false;
		}
		
		IVlanConfiguration	vlanConfig	= iConfiguration.getVlanConfiguration();
		if(vlanConfig == null) {
			return false;
		}
		for(int i=0;i<deletedVlans.size();i++) {
			vlanName	= (String) deletedVlans.get(i);
			vlanConfig.deleteVlanInfo(vlanName);
			
		}
		deletedVlans.removeAllElements();
		lastSelectedVlanIndex	= -1;
		return true;
	}

	/**
	 * 
	 */
	private boolean addVlanEntry() {
		
		if(validateLocalData() == false) {
			return false;
		}
		
		if(chkForEssid() == false) {
			return false;
		}
		getDataFromUiToLocalConfiguration();
		return true;
	}
	
	private void editVlan() {

		btnSave.setEnabled(true);
		btnDelete.setEnabled(false);
		btnEdit.setEnabled(false);
		btnNew.setEnabled(false);
		
		enableAllControls(true);
		txtVlanName.setFocus();
				
		/* following line is  for the dependencies of btnDot11eEnabled on cmbDot11eCategory
		 * and btnDot11eEditCategory
		 */
		if(btnDot11eEnabled.getSelection() == true) {
			cmbDot11eCategory.setEnabled(true);
		} else {
			cmbDot11eCategory.setEnabled(false);
		}
		
	}
	
	private void clearAllControls() {
		
		txtVlanName.setText("");
		txtTagging.setText("");
		txtVlanEssid.setText("");
		
		sld8021P.setValue(0);
		btnDot11eEnabled.setSelection(false);
		cmbDot11eCategory.select(0);
		cmbDot11eCategory.setEnabled(false);
		
		securityFrame.clearAll();
		securityFrame.setDefaultSettings();
		
	}

	private void enableAllControls(boolean enable) {
		
		btnDot11eEnabled.setEnabled(enable);
				
		txtVlanName.setEnabled(enable);
		txtTagging.setEnabled(enable);
		txtVlanEssid.setEnabled(enable);
		
		sld8021P.setEnabled(enable);
		lbl8021P.setEnabled(enable);
		cmbDot11eCategory.setEnabled(enable);
		
		securityFrame.showFrame(enable);
		securityFrame.enableFrame(enable);
	
	}

	public boolean validateLocalData() {
		
		if(sMananger.getState() == STATE_START			|| 
				sMananger.getState() == STATE_DELETE 	||
				sMananger.getState() == STATE_SELECT	||
				sMananger.getState() == STATE_SAVE){
			return true;
		}
		
		String vlanName = txtVlanName.getText().trim();
		
		if(vlanName.equalsIgnoreCase("")) {
			parent.showMessage("VLAN Name not entered. ",SWT.ICON_ERROR);
			txtVlanName.setFocus();	
			return false;
		} else if(vlanName.equalsIgnoreCase(DEFAULT_VLAN_NAME)) {
			parent.showMessage("VLAN Name 'default' not allowed. ",SWT.ICON_ERROR);
			txtVlanName.setFocus();	
			return false;
		}

		if(txtVlanEssid.getText().trim().equalsIgnoreCase("")){
			parent.showMessage("ESSID not entered. ",SWT.ICON_ERROR);			
			txtVlanEssid.setFocus();
			return false;		
		}
		try{
			
			int val = Integer.parseInt(txtTagging.getText().trim());
			if(MeshValidations.isIntegerInRange(val,0,VLAN_TAG_MAX_VALUE)!= true){
				parent.showMessage("VLAN Tag out of range. "+SWT.LF+
						"Valid range is in between 0 to "+VLAN_TAG_MAX_VALUE+"." ,SWT.ICON_ERROR);
				txtTagging.setFocus();
				return false;
			}
			
			if(txtTagging.getText().trim().equals("-0")) {
				parent.showMessage("VLAN Tag is incorrect. ",SWT.ICON_ERROR);
				txtTagging.setFocus();
				return false;
			}
			
		}catch(Exception e){
			parent.showMessage("VLAN Tag should be numeric. ",SWT.ICON_ERROR);			
			txtTagging.setFocus();			
			return false;
		}
	
		if(securityFrame.validateLocalData() == false){
			return false;
		}
			
		if(chkDuplicates() == false) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * @return
	 */
	private boolean chkDuplicates() {
		
		int 	vlanIndex;
		int 	index;
		String	vlanName;
		int		vlanTag;	
		String  vlanEssId;
		
		vlanName	=	txtVlanName.getText().trim();
		vlanTag		= 	Integer.parseInt(txtTagging.getText().trim());
		vlanEssId	=	txtVlanEssid.getText().trim();
		
		vlanIndex	= lstVlan.getSelectionIndex();
		
		for(index = 0; index < lstVlan.getItemCount(); index++){
			
			if(vlanIndex == index){
				continue;
			}
			
			IVlanInfo	vlanInfo	= (IVlanInfo)lstVlan.getData(lstVlan.getItem(index));
			if(vlanInfo == null) {
				continue;
			}
			
			if(vlanName.equalsIgnoreCase(vlanInfo.getName())){
				showMessage("Duplicate VLAN name.", SWT.ICON_ERROR);
				return false;
			}
			if(vlanTag == vlanInfo.getTag()){ 
				showMessage("Duplicate VLAN tag.", SWT.ICON_ERROR);
				return false;
			}
			if(vlanEssId.equalsIgnoreCase(vlanInfo.getEssid())){
				showMessage("Duplicate VLAN ESSID.", SWT.ICON_ERROR);
				return false;
			}
	
		}
		return true;
	}

	/**
	 * 
	 */
	public boolean getDataFromUIToConfiguration() {
		
		if(	sMananger.getState() == STATE_START ||
				sMananger.getState() == STATE_SELECT||
				sMananger.getState() == STATE_SAVE) {
			return true;
		}
	
		if(chkForEssid() == false) {
			//notifyStateChanged(sMananger.getState());
			return false;
		}
	
		notifyStateChanged(STATE_SAVE);
		return true;
	}

	/**
	 * @return
	 */
	private boolean chkForEssid() {
		
		if(sMananger.getState() == STATE_NEW) {
			return true;
		}
		if(securityFrame.getSelectedSecurity() != SecurityFrame.STATE_WPA_PERSONAL){
			return true;
		}
		
		String 		vlanName;
		String		strOldEssId;
		String  	strNewEssId;
		IVlanInfo	vlanInfo;
		
		vlanName	= txtVlanName.getText().trim();
		vlanInfo 	= getVlanInfoFromUI(vlanName);
		if(vlanInfo == null) {
			return false;
		}
		
		strOldEssId = vlanInfo.getEssid();
		strNewEssId = txtVlanEssid.getText().trim();
		if(strNewEssId.equalsIgnoreCase(strOldEssId) == false ) {
			
			if(wpaGenerationStatus.containsKey(vlanName) == false){
				WPAKeyGenerationStatusHolder status = new WPAKeyGenerationStatusHolder();
				status.vlanName			= vlanName;
				status.keyRegenerated	= false;
				wpaGenerationStatus.put(vlanName,status );
			}
			WPAKeyGenerationStatusHolder	 status = (WPAKeyGenerationStatusHolder) wpaGenerationStatus.get(vlanName);
			if(status == null) {
				return false;
			}
			if(status.keyRegenerated == false) {
				showMessage("EssId changed for VLAN "+ vlanName +SWT.LF+ 
						"You must regenerate WPA Personal security key for the same.",SWT.ICON_INFORMATION|SWT.OK);
				return false;
			}
		}
		return true;
	}

	private boolean getDataFromUiToLocalConfiguration() {
		
		int			index;
		String  	strNewEssId;
		String 		vlanName;
		IVlanInfo	vlanInfo;
		if(deletedVlans.size() > 0) {
			return deleteVlan();
		}
		
		vlanName	= txtVlanName.getText().trim();
		vlanInfo 	= getVlanInfoFromUI(vlanName);
		
		if(vlanInfo == null) {
			return false;
		}
		
		vlanInfo.setName(vlanName);
		
		strNewEssId = txtVlanEssid.getText().trim();
		vlanInfo.setEssid(strNewEssId);
		
		vlanInfo.setTag(Integer.parseInt(txtTagging.getText().trim()));
		
		if(btnDot11eEnabled.getSelection() == true) {
			vlanInfo.set80211eEnabled((short)1);
		}else {
			vlanInfo.set80211eEnabled((short)0);
		}
		vlanInfo.set80211eCategoryIndex((short)cmbDot11eCategory.getSelectionIndex());
		
		vlanInfo.set8021pPriority((short)sld8021P.getValue());
		
		
		ISecurityConfiguration	vlanSecurityInfo	= (ISecurityConfiguration)vlanInfo.getSecurityConfiguration(); 
		if(vlanSecurityInfo == null) {
			return false;
		}
		securityFrame.getDataFromUIToConfiguration(vlanSecurityInfo);
		
		index	= lstVlan.getSelectionIndex();
		if(index != -1){
			lstVlan.remove(index);
			lstVlan.add(vlanName,index);
			lstVlan.select(index);
			lstVlan.setData(vlanName,vlanInfo);
			lastSelectedVlanIndex	= index;
		}else{
			
			lstVlan.setData(vlanName,vlanInfo);
			lstVlan.add(vlanName);
			lstVlan.select(lstVlan.getItemCount()-1);
			lastSelectedVlanIndex	= lstVlan.getItemCount()-1;
		}
		if(wpaGenerationStatus.containsKey(vlanName)) {
			wpaGenerationStatus.remove(vlanName);
		}
		notifyStateChanged(STATE_SELECT);
		return true;
	}
	/*
	 * return currentSelected vlan or new vlan
	 */
	private IVlanInfo getVlanInfoFromUI(String vlanName) {
	
		IConfiguration	iConfiguration	= parent.getConfiguration();
		if(iConfiguration == null){
			return null;
		}
		
		IVlanConfiguration	vlanConfig	= iConfiguration.getVlanConfiguration();
		if(vlanConfig == null) {
			return null;
		}
		
		if(sMananger.getState() == STATE_NEW) {
			return (IVlanInfo)vlanConfig.addVlanInfo(vlanName);
		}
			
		int		index;
		index	= lstVlan.getSelectionIndex();
		return (IVlanInfo)lstVlan.getData(lstVlan.getItem(index));
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IErrorMessage#showErrorMessage(java.lang.String, int)
	 */
	public int showMessage(String errorString, int style) {
		return parent.showMessage(errorString, style);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.ConfigPage#isFipsFulfilled()
	 */
	public boolean isFipsFulfilled() {
		int index;
		String[]	vlanNames;
		
		vlanNames	= lstVlan.getItems();
		for(index = 0; index < vlanNames.length; index++) {
			IVlanInfo	vlanInfo	= (IVlanInfo)lstVlan.getData(vlanNames[index]);
			if(vlanInfo == null) {
				continue;
			}
			
			ISecurityConfiguration	securityConfig	= (ISecurityConfiguration)vlanInfo.getSecurityConfiguration();
			if(securityConfig == null) {
				continue;
			}
			if(FipsSecurityHelper.isSecurityFipsCompliant(securityConfig) == false) {
				stayOnSameTab();
				lstVlan.select(index);
				notifyStateChanged(STATE_SELECT);
				parent.showMessage("This configuration is FIPS 140-2 enabled." + SWT.LF +
						"Security is not enabled for VLAN "+ vlanInfo.getName(), SWT.ICON_INFORMATION);
				return false;
			}
		}
		return true;
	}

	public void securitySelected(SecurityFrameSelectionEvent sfsEvent) {
		//System.out.println(sfsEvent.getRadioIndex() + " selected");
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.ISecurityFrameListener#notifyWPAPersonalKeyRegenerated()
	 */
	public void notifyWPAPersonalKeyRegenerated() {
		if(sMananger.getState() == STATE_EDIT) {
		
			String vlanName = lstVlan.getItem(lstVlan.getSelectionIndex());
			if(wpaGenerationStatus.containsKey(vlanName)) {
				WPAKeyGenerationStatusHolder	 status = (WPAKeyGenerationStatusHolder) wpaGenerationStatus.get(vlanName);
				status.keyRegenerated	= true;
			}
		}
	}
	
}
