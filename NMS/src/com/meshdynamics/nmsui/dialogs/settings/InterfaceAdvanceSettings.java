package com.meshdynamics.nmsui.dialogs.settings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceAdvanceConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceInfo;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;

/*
 * Advance settings for interface
 */

public class InterfaceAdvanceSettings extends MeshDynamicsDialog {

	private final static int protocalid_N = 1;
	private final static int protocalid_AC = 2;
	private final static int protocalid_N_AC = 3;

	private Group group, groupN, groupAc;
	private Button ldpcButton, txSTBCButton, rxSTBCButton;
	private Button gfModeButton, frameAggregationButton;
	private Button coexistenceButton;
	private Button secondaryChannelPositionAboveButton,
			secondaryChannelPositionBelowButton;
	private Label lblsecondaryCannelPosition;
	/*private Button supportedPosition20Button,supportedPosition40Button,supportedPosition80Button;
	private Label lblsupportedPosition;*/
	private Label lblMaxAMPDU, lblMaxAMSDU;
	private Label lblMaxMPDU;
	private Label lblChannelBandwidth;
	private Label lblPreamble, lblGuardInterval_20,lblGuardInterval_40,lblGuardInterval_80;
	private Combo cmbChannelBandwidth;
	private Combo cmbPreamble, cmbGuardInterval_20,cmbGuardInterval_40,cmbGuardInterval_80;
	private Combo cmbMaxAMPDU, cmbMaxAMSDU, cmbMaxMPDU;

	private IConfiguration iConfiguration;
	private String ifName;
	private int protocolID;
	public final static String[] default_Values_PreambleAndGuardIntervel = {
			"auto", "short", "long" };
	public final static String[] default_Values_GuardIntervel = {"long","short"};

	String supportedProtocal;
	public InterfaceAdvanceSettings(String operationMode,
			IConfiguration iConfiguration, String ifName) {
		this.supportedProtocal = operationMode;
		this.iConfiguration = iConfiguration;
		this.ifName = ifName;
	}

	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x = 0;
		bounds.y = 0;
		bounds.width = 400;
		bounds.height = 450;

	}

	@Override
	protected void createControls(Canvas mainCnv) {
		group = new Group(mainCnv, SWT.NO);
		group.setText("Interface Advanced Settings for " + supportedProtocal);
		int rel_y = 20;
		group.setBounds(10, rel_y, 380, 450);
		rel_y = createGroup(rel_y);
		rel_y = createGroupForN(rel_y);
		createGroupForAc(rel_y);

		controllData(supportedProtocal);
		initializeData();

	}

	public int createGroup(int rel_y) {

		int rel_x = 20;
		lblChannelBandwidth = new Label(group, SWT.NO);
		lblChannelBandwidth.setText("Channel Bandwidth");
		lblChannelBandwidth.setBounds(rel_x, rel_y, 120, 15);

		cmbChannelBandwidth = new Combo(group, SWT.READ_ONLY | SWT.BORDER);
		cmbChannelBandwidth.setBounds(160, rel_y, 196, 15);
		cmbChannelBandwidth.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent arg0) {
				if (cmbChannelBandwidth.getText().equals("80 MHz")) {
					lblsecondaryCannelPosition.setEnabled(false);
					secondaryChannelPositionAboveButton.setEnabled(false);
					secondaryChannelPositionBelowButton.setEnabled(false);
					IInterfaceConfiguration iinterface = iConfiguration
							.getInterfaceConfiguration();
					InterfaceInfo ifInfoObject = (InterfaceInfo) iinterface
							.getInterfaceByName(ifName);
					InterfaceAdvanceConfiguration ifAdvance = ifInfoObject
							.getInterfaceAdvanceConfiguration();
					setGuardIntervalWithChannel_80(ifAdvance);
//					setSupportedBandWidth_80(ifAdvance);
				} else if (cmbChannelBandwidth.getText().equals("40 MHz")) {
					lblsecondaryCannelPosition.setEnabled(true);
					secondaryChannelPositionAboveButton.setEnabled(true);
					secondaryChannelPositionBelowButton.setEnabled(true);
					IInterfaceConfiguration iinterface = iConfiguration
							.getInterfaceConfiguration();
					InterfaceInfo ifInfoObject = (InterfaceInfo) iinterface
							.getInterfaceByName(ifName);
					InterfaceAdvanceConfiguration ifAdvance = ifInfoObject
							.getInterfaceAdvanceConfiguration();
					setSecondaryChannelPositionWithChannel(ifAdvance);
					setGuardIntervalWithChannel_40(ifAdvance);
//					setSupportedBandWidth_40(ifAdvance);
				} else if (cmbChannelBandwidth.getText().equals("20 MHz")) {
					lblsecondaryCannelPosition.setEnabled(false);
					secondaryChannelPositionAboveButton.setEnabled(false);
					secondaryChannelPositionBelowButton.setEnabled(false);
					IInterfaceConfiguration iinterface = iConfiguration
							.getInterfaceConfiguration();
					InterfaceInfo ifInfoObject = (InterfaceInfo) iinterface
							.getInterfaceByName(ifName);
					InterfaceAdvanceConfiguration ifAdvance = ifInfoObject
							.getInterfaceAdvanceConfiguration();
					setSecondaryChannelPositionWithChannel(ifAdvance);
					setGuardIntervalWithChannel_20(ifAdvance);
//					setSupportedBandWidth_20(ifAdvance);
				} else {
					lblsecondaryCannelPosition.setEnabled(false);
					secondaryChannelPositionAboveButton.setEnabled(false);
					secondaryChannelPositionBelowButton.setEnabled(false);
				}
			}
		});
		rel_y += 25;
		lblsecondaryCannelPosition = new Label(group, SWT.NO);
		lblsecondaryCannelPosition.setText("Secondary Channel Position");
		lblsecondaryCannelPosition.setBounds(rel_x, rel_y, 100, 15);
		// lblsecondaryCannelPosition.setVisible(false);
		secondaryChannelPositionAboveButton = new Button(group, SWT.CHECK);
		secondaryChannelPositionAboveButton.setText("above 40 MHz");
		// secondaryCannelPositionAboveButton.setVisible(false);
		secondaryChannelPositionAboveButton.setEnabled(false);
		secondaryChannelPositionAboveButton.setBounds(rel_x + 140, rel_y, 100,
				15);
		secondaryChannelPositionAboveButton
				.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent arg0) {
						if (secondaryChannelPositionAboveButton.getSelection() == true
								&& secondaryChannelPositionBelowButton
										.getSelection() == true) {
							secondaryChannelPositionAboveButton
									.setSelection(false);
							MessageBox msg = new MessageBox(
									new Shell(SWT.CLOSE), SWT.ICON_ERROR);
							msg.setMessage("Either above 40MHz or below 40MHz should be selected");
							msg.open();
							return;
						}
					}
				});
		secondaryChannelPositionBelowButton = new Button(group, SWT.CHECK);
		secondaryChannelPositionBelowButton.setText("below 40 MHz");
		// secondaryCannelPositionBelowButton.setVisible(false);
		secondaryChannelPositionBelowButton.setEnabled(false);
		secondaryChannelPositionBelowButton.setBounds(rel_x + 240, rel_y, 100,
				15);
		secondaryChannelPositionBelowButton
				.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent arg0) {
						if (secondaryChannelPositionAboveButton.getSelection() == true
								&& secondaryChannelPositionBelowButton
										.getSelection() == true) {
							secondaryChannelPositionBelowButton
									.setSelection(false);
							MessageBox msg = new MessageBox(
									new Shell(SWT.CLOSE), SWT.OK
											| SWT.ICON_ERROR);
							msg.setMessage("Either above 40MHz or below 40MHz should be selected");
							msg.open();
							return;
						}
					}
				});
		
		rel_y += 25;
		lblPreamble = new Label(group, SWT.NO);
		lblPreamble.setText("Preamble");
		lblPreamble.setBounds(rel_x, rel_y, 100, 15);
		lblPreamble.setEnabled(false);
		cmbPreamble = new Combo(group, SWT.READ_ONLY | SWT.BORDER);
		cmbPreamble.setBounds(160, rel_y, 196, 15);
		cmbPreamble.setEnabled(false);
		
		rel_y += 25;
		lblGuardInterval_20 = new Label(group, SWT.NO);
		lblGuardInterval_20.setText("Guard Interval (20 MHz)");
		lblGuardInterval_20.setBounds(rel_x, rel_y, 125, 15);
		cmbGuardInterval_20 = new Combo(group, SWT.READ_ONLY | SWT.BORDER);
		cmbGuardInterval_20.setBounds(160, rel_y, 196, 15);
		rel_y += 25;
		lblGuardInterval_40 = new Label(group, SWT.NO);
		lblGuardInterval_40.setText("Guard Interval (40 MHz)");
		lblGuardInterval_40.setBounds(rel_x, rel_y, 125, 15);
		cmbGuardInterval_40 = new Combo(group, SWT.READ_ONLY | SWT.BORDER);
		cmbGuardInterval_40.setBounds(160, rel_y, 196, 15);
		rel_y += 25;
		lblGuardInterval_80 = new Label(group, SWT.NO);
		lblGuardInterval_80.setText("Guard Interval (80 MHz)");
		lblGuardInterval_80.setBounds(rel_x, rel_y, 125, 15);
		cmbGuardInterval_80 = new Combo(group, SWT.READ_ONLY | SWT.BORDER);
		cmbGuardInterval_80.setBounds(160, rel_y, 196, 15);
//		cmbGuardInterval.setEnabled(false);
		rel_y += 25;
		ldpcButton = new Button(group, SWT.CHECK);
		ldpcButton.setText("LDPC");
		ldpcButton.setSelection(true);
		ldpcButton.setBounds(rel_x, rel_y, 100, 15);

		rel_y += 25;
		txSTBCButton = new Button(group, SWT.CHECK);
		txSTBCButton.setText("TxSTBC");
		txSTBCButton.setSelection(true);
		txSTBCButton.setBounds(rel_x, rel_y, 100, 15);

		rel_y += 25;
		rxSTBCButton = new Button(group, SWT.CHECK);
		rxSTBCButton.setText("RxSTBC");
		rxSTBCButton.setSelection(true);
		rxSTBCButton.setBounds(rel_x, rel_y, 100, 15);
		rel_y += 25;
		frameAggregationButton = new Button(group, SWT.CHECK);
		frameAggregationButton.setText("Frame Aggregation");
		frameAggregationButton.setSelection(true);
		frameAggregationButton.setBounds(rel_x, rel_y, 120, 15);
		frameAggregationButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				if (frameAggregationButton.getSelection() == false) {
					cmbMaxAMSDU.setEnabled(false);
					cmbMaxAMPDU.setEnabled(false);
				} else {
					cmbMaxAMSDU.setEnabled(true);
					cmbMaxAMPDU.setEnabled(true);
				}

			}
		});
		rel_y += 25;
		lblMaxAMPDU = new Label(group, SWT.NO);
		lblMaxAMPDU.setText("Max A-MPDU");
		lblMaxAMPDU.setBounds(rel_x, rel_y, 100, 15);

		cmbMaxAMPDU = new Combo(group, SWT.READ_ONLY | SWT.BORDER);
		cmbMaxAMPDU.setBounds(160, rel_y, 196, 15);

		rel_y += 30;
		lblMaxAMSDU = new Label(group, SWT.NO);
		lblMaxAMSDU.setText("Max A-MSDU");
		lblMaxAMSDU.setBounds(rel_x, rel_y, 100, 15);

		cmbMaxAMSDU = new Combo(group, SWT.READ_ONLY | SWT.BORDER);
		cmbMaxAMSDU.setBounds(160, rel_y, 196, 15);
		rel_y += 20;
		return rel_y;
	}

	public int createGroupForN(int rel_y) {
		int rel_x = 25;
		groupN = new Group(group, SWT.NO);
		groupN.setBounds(rel_x, rel_y, 350, 60);
		gfModeButton = new Button(groupN, SWT.CHECK);
		gfModeButton.setText("GFMode");
		gfModeButton.setBounds(10, 15, 100, 15);

		coexistenceButton = new Button(groupN, SWT.CHECK);
		coexistenceButton.setText("20/40MHz coexistence");
		coexistenceButton.setSelection(true);
		coexistenceButton.setBounds(10, 35, 140, 15);
		rel_y += 70;
		return rel_y;
	}

	public void createGroupForAc(int rel_y) {
		int rel_x = 25;
		groupAc = new Group(group, SWT.NO);
		groupAc.setBounds(rel_x, rel_y, 350, 40);
		lblMaxMPDU = new Label(groupAc, SWT.NO);
		lblMaxMPDU.setText("Max MPDU");
		lblMaxMPDU.setBounds(10, 12, 100, 15);
		cmbMaxMPDU = new Combo(groupAc, SWT.READ_ONLY | SWT.BORDER);
		cmbMaxMPDU.setBounds(130, 12, 196, 15);
	}

	private void controllData(String protocal) {
		if (protocal.equalsIgnoreCase("802.11n")
				|| protocal.equalsIgnoreCase("802.11a/n")
				|| protocal.equalsIgnoreCase("802.11b/n")
				|| protocal.equalsIgnoreCase("802.11b/g/n")) {
			groupAc.setVisible(false);
			cmbMaxMPDU.setVisible(false);
			lblMaxMPDU.setVisible(false);
			protocolID = InterfaceAdvanceSettings.protocalid_N;
		} else if (protocal.equalsIgnoreCase("802.11a/n/ac")) {
			// show all widgets
			protocolID = InterfaceAdvanceSettings.protocalid_N_AC;
		} else {
			groupN.setVisible(true);
			coexistenceButton.setVisible(true);
			protocolID = InterfaceAdvanceSettings.protocalid_AC;
		}
	}

	public void createGroupforN() {
		lblMaxMPDU.setVisible(false);
		cmbMaxMPDU.setVisible(false);
	}

	public void createGroupForAC(int rel_y) {

		// coexistenceButton.
	}

	/*
	 * set Data for Max MPDU
	 */
	public void setDataForChannelBandwidth() {
		String[] Default_Values_ChannelBandwidth = { "20 MHz", "40 MHz",
				"80 MHz" };
		cmbChannelBandwidth.removeAll();
		int count = 0;
		if (supportedProtocal.equalsIgnoreCase("802.11a/n/ac")
				|| supportedProtocal.equalsIgnoreCase("802.11ac")) {
			while (Default_Values_ChannelBandwidth.length > count) {
				cmbChannelBandwidth.add(Default_Values_ChannelBandwidth[count]);
				cmbChannelBandwidth.setData(
						Default_Values_ChannelBandwidth[count], count);
				count++;
			}
		} else {

			cmbChannelBandwidth.add(Default_Values_ChannelBandwidth[0]);
			cmbChannelBandwidth.setData(Default_Values_ChannelBandwidth[0], 0);
			cmbChannelBandwidth.add(Default_Values_ChannelBandwidth[1]);
			cmbChannelBandwidth.setData(Default_Values_ChannelBandwidth[1], 1);

		}
		cmbChannelBandwidth.select(0);
	}

	/*
	 * set Data for Preamble
	 */
	public void setDataForPreamble() {
		cmbPreamble.removeAll();
		int count = 0;
		while (InterfaceAdvanceSettings.default_Values_PreambleAndGuardIntervel.length > count) {
			cmbPreamble
					.add(InterfaceAdvanceSettings.default_Values_PreambleAndGuardIntervel[count]);
			cmbPreamble
					.setData(
							InterfaceAdvanceSettings.default_Values_PreambleAndGuardIntervel[count],
							count);
			count++;
		}
		cmbPreamble.select(0);
	}

	/*
	 * set Data for Guard Interval
	 */
	public void setDataForGuardInterval() {
		cmbGuardInterval_20.removeAll();
		cmbGuardInterval_20
				.add(InterfaceAdvanceSettings.default_Values_GuardIntervel[0]);
		cmbGuardInterval_20
				.setData(
						InterfaceAdvanceSettings.default_Values_GuardIntervel[0],
						0);
		cmbGuardInterval_20
				.add(InterfaceAdvanceSettings.default_Values_GuardIntervel[1]);
		cmbGuardInterval_20
				.setData(
						InterfaceAdvanceSettings.default_Values_GuardIntervel[1],
						2);
		
		cmbGuardInterval_20.select(2);
		
		// setting values gurard interval 40
		cmbGuardInterval_40.removeAll();
		cmbGuardInterval_40
				.add(InterfaceAdvanceSettings.default_Values_GuardIntervel[0]);
		cmbGuardInterval_40
				.setData(
						InterfaceAdvanceSettings.default_Values_GuardIntervel[0],
						0);
		cmbGuardInterval_40
				.add(InterfaceAdvanceSettings.default_Values_GuardIntervel[1]);
		cmbGuardInterval_40
				.setData(
						InterfaceAdvanceSettings.default_Values_GuardIntervel[1],
						2);
		cmbGuardInterval_40.select(2);
		
		// setting values gurard interval 80
				cmbGuardInterval_80.removeAll();
				cmbGuardInterval_80
						.add(InterfaceAdvanceSettings.default_Values_GuardIntervel[0]);
				cmbGuardInterval_80
						.setData(
								InterfaceAdvanceSettings.default_Values_GuardIntervel[0],
								0);
				cmbGuardInterval_80
						.add(InterfaceAdvanceSettings.default_Values_GuardIntervel[1]);
				cmbGuardInterval_80
						.setData(
								InterfaceAdvanceSettings.default_Values_GuardIntervel[1],
								2);
				cmbGuardInterval_80.select(2);
		
	}

	/*
	 * set Data for Max MPDU
	 */
	public void setDataForMaxMPDU() {
		int[] Default_Values_MaxMPDU = { 3895, 7991, 11454 };
		cmbMaxMPDU.removeAll();
		int count = 0;
		while (Default_Values_MaxMPDU.length > count) {
			cmbMaxMPDU.add("" + Default_Values_MaxMPDU[count] + " bytes");
			cmbMaxMPDU.setData("" + Default_Values_MaxMPDU[count] + " bytes",
					Default_Values_MaxMPDU[count]);
			count++;
		}
		cmbMaxMPDU.select(0);
	}

	/*
	 * set Data for Max A-MSDU
	 */
	public void setDataForMaxAMSDU() {
		String[] Default_Values_MaxAMSDU = { "3839 bytes", "7935 bytes" };
		cmbMaxAMSDU.removeAll();
		int count = 0;
		while (Default_Values_MaxAMSDU.length > count) {
			cmbMaxAMSDU.add(Default_Values_MaxAMSDU[count]);
			cmbMaxAMSDU.setData(Default_Values_MaxAMSDU[count], count);
			count++;
		}
		cmbMaxAMSDU.select(0);
	}

	/*
	 * set Data for Max A-MPDU
	 */
	public void setDataForMaxAMPDU() {
		int[] Default_Values_MaxAMPDU = { 8, 16, 32, 64 };
		cmbMaxAMPDU.removeAll();
		int count = 1;
		cmbMaxAMPDU.removeAll();
		cmbMaxAMPDU.add("" + Default_Values_MaxAMPDU[0] + " KB");
		cmbMaxAMPDU.setData("" + Default_Values_MaxAMPDU[0] + " KB", 8);

		while (Default_Values_MaxAMPDU.length > count) {
			cmbMaxAMPDU.add("" + Default_Values_MaxAMPDU[count] + " KB");
			cmbMaxAMPDU.setData("" + Default_Values_MaxAMPDU[count] + " KB",
					Default_Values_MaxAMPDU[count]);
			count++;
		}
		cmbMaxAMPDU.select(0);
	}

	private void setChannelBandwidth(long value) {

		if (value == 0) {
			cmbChannelBandwidth.select(0);
		} else {

			boolean found = false;
			int itemCount = cmbChannelBandwidth.getItemCount();
			int j;
			for (j = 1; j < itemCount; j++) {
				String strChannelBandwidth = cmbChannelBandwidth.getItem(j);
				int data = (int) cmbChannelBandwidth
						.getData(strChannelBandwidth);
				if (data == value) {
					cmbChannelBandwidth.select(j);
					found = true;
					break;
				}
			}
			if (found == false)
				cmbChannelBandwidth.select(0);
		}
	}

	private void setPreamble(long value) {

		if (value == 0) {
			cmbPreamble.select(0);
		} else {

			boolean found = false;
			int itemCount = cmbPreamble.getItemCount();
			int j;
			for (j = 1; j < itemCount; j++) {
				String strPreamble = cmbPreamble.getItem(j);
				int data = (int) cmbPreamble.getData(strPreamble);
				if (data == value) {
					cmbPreamble.select(j);
					found = true;
					break;
				}
			}
			if (found == false)
				cmbPreamble.select(0);
		}
	}

	private void setGuardInterval_40(long value) {

		boolean found = false;
		int itemCount = cmbGuardInterval_40.getItemCount();
		int j;
		
		for (j = 0; j < itemCount; j++) {
			String strGuardInterval = cmbGuardInterval_40.getItem(j);
			int data = (int) cmbGuardInterval_40.getData(strGuardInterval);
			if (data == value) {
				cmbGuardInterval_40.select(j);
				found = true;
				break;
			}
		}
		if (found == false)
			cmbGuardInterval_40.select(0);
	}
	
	private void setGuardInterval_20(long value) {

		boolean found = false;
		int itemCount = cmbGuardInterval_20.getItemCount();
		int j;
		
		for (j = 0; j < itemCount; j++) {
			String strGuardInterval = cmbGuardInterval_20.getItem(j);
			int data = (int) cmbGuardInterval_20.getData(strGuardInterval);
			if (data == value) {
				cmbGuardInterval_20.select(j);
				found = true;
				break;
			}
		}
		if (found == false)
			cmbGuardInterval_20.select(0);
	}
	
	private void setGuardInterval_80(long value) {

		boolean found = false;
		int itemCount = cmbGuardInterval_80.getItemCount();
		int j;
		
		for (j = 0; j < itemCount; j++) {
			String strGuardInterval = cmbGuardInterval_80.getItem(j);
			int data = (int) cmbGuardInterval_80.getData(strGuardInterval);
			if (data == value) {
				cmbGuardInterval_80.select(j);
				found = true;
				break;
			}
		}
		if (found == false)
			cmbGuardInterval_80.select(0);
	}

	/**
	 * Setting the MaxMPDU value in combo box
	 */
	private void setMaxMPDU(long value) {
		if (value == 3895) {
			cmbMaxMPDU.select(0);
		} else {
			boolean found = false;
			int itemCount = cmbMaxMPDU.getItemCount();
			int j;
			for (j = 1; j < itemCount; j++) {
				String strMaxMPDU = cmbMaxMPDU.getItem(j);
				int data = (int) cmbMaxMPDU.getData(strMaxMPDU);
				if (data == value) {
					cmbMaxMPDU.select(j);
					found = true;
					break;
				}
			}
			if (found == false)
				cmbMaxMPDU.select(0);
		}
	}

	/**
	 * Setting the Max A-MSDU value in combo box
	 */
	private void setMaxAMSDU(long value) {

		if (value == 0) {
			cmbMaxAMSDU.select(0);
		} else {

			boolean found = false;
			int itemCount = cmbMaxAMSDU.getItemCount();
			int j;
			for (j = 1; j < itemCount; j++) {
				String strMaxAMSDU = cmbMaxAMSDU.getItem(j);
				int data = (int) cmbMaxAMSDU.getData(strMaxAMSDU);
				if (data == value) {
					cmbMaxAMSDU.select(j);
					found = true;
					break;
				}
			}
			if (found == false)
				cmbMaxAMSDU.select(0);
		}
	}

	/**
	 * Setting the Max A-MPDU value in combo box
	 */
	private void setMaxAMPDU(long value) {

		if (value == 0) {
			cmbMaxAMPDU.select(0);
		} else {

			boolean found = false;
			int itemCount = cmbMaxAMPDU.getItemCount();
			int j;
			for (j = 1; j < itemCount; j++) {
				String strMaxAMPDU = cmbMaxAMPDU.getItem(j);
				int data = (int) cmbMaxAMPDU.getData(strMaxAMPDU);
				if (data == value) {
					cmbMaxAMPDU.select(j);
					found = true;
					break;
				}
			}
			if (found == false)
				cmbMaxAMPDU.select(0);
		}
	}

	@Override
	protected boolean onOk() {

		IInterfaceConfiguration iinterface = iConfiguration
				.getInterfaceConfiguration();
		InterfaceInfo ifInfoObject = (InterfaceInfo) iinterface
				.getInterfaceByName(ifName);
		InterfaceAdvanceConfiguration ifAdvance = ifInfoObject
				.getInterfaceAdvanceConfiguration();
		ifAdvance.setProtocolID(protocolID);
		/*
		 * if(secondaryChannelSelectedOrNot()) { return false; }else{ return
		 * getDataFromUIToConfiguration(ifAdvance); }
		 */
		
//		if(compareUIDataWithConfiguration(ifAdvance) == false){
//			
//			return false;
//		} 
		
//		String strGuardIntyerval = cmbGuardInterval.getItem(cmbGuardInterval.getSelectionIndex());
//		int guardInterval = (int) cmbGuardInterval .getData(strGuardIntyerval);
//		
//		if(guardInterval == 0 || guardInterval == 1){
//			MessageBox msg = new MessageBox(
//					new Shell(SWT.CLOSE), SWT.ICON_ERROR);
//			msg.setMessage("Guard Interval should be select short");
//			msg.open();
//			return false;
//		}
		
		return getDataFromUIToConfiguration(ifAdvance);
	}

	@Override
	protected boolean onCancel() {
		// TODO Auto-generated method stub
		return true;
	}

	private void initializeData() {
		setInterfaceValues(ifName);
	}

	private void setInterfaceValues(String ifName) {

		IInterfaceConfiguration iinterface = iConfiguration
				.getInterfaceConfiguration();
		InterfaceInfo ifInfoObject = (InterfaceInfo) iinterface
				.getInterfaceByName(ifName);
		InterfaceAdvanceConfiguration ifAdvance = ifInfoObject
				.getInterfaceAdvanceConfiguration();
	
		if (ifAdvance == null) {
			return;
		}
		// Data Initialization
		setDataForChannelBandwidth();
		setDataForPreamble();
		setDataForGuardInterval();
		setDataForMaxMPDU();
		setDataForMaxAMPDU();
		setDataForMaxAMSDU();
		setChannelBandwidth(ifAdvance.getChannelBandwidth());
		// set value for secondaryChannelPosition
		setSecondaryChannelPosition(ifAdvance);
		setGuardInterval(ifAdvance);
		setPreamble(ifAdvance.getPreamble());
		setMaxMPDU(ifAdvance.getMaxMPDU());
		setMaxAMSDU(ifAdvance.getMaxAMSDU());
		setMaxAMPDU(ifAdvance.getMaxAMPDU());
		if (ifAdvance.getLDPC() == 0)
			ldpcButton.setSelection(false);
		else
			ldpcButton.setSelection(true);
		if (ifAdvance.getTxSTBC() == 0)
			txSTBCButton.setSelection(false);
		else
			txSTBCButton.setSelection(true);
		if (ifAdvance.getRxSTBC() == 0)
			rxSTBCButton.setSelection(false);
		else
			rxSTBCButton.setSelection(true);
		if (ifAdvance.getGFMode() == 0)
			gfModeButton.setSelection(false);
		else
			gfModeButton.setSelection(true);
		if (ifAdvance.getMaxAMPDUEnabled() == 0) {
			frameAggregationButton.setSelection(false);
			cmbMaxAMPDU.setEnabled(false);
			cmbMaxAMSDU.setEnabled(false);
		} else {
			cmbMaxAMPDU.setEnabled(true);
			cmbMaxAMSDU.setEnabled(true);
			frameAggregationButton.setSelection(true);
		}
		if (ifAdvance.getCoexistence() == 0)
			coexistenceButton.setSelection(false);
		else
			coexistenceButton.setSelection(true);
	}

	private void setSecondaryChannelPosition(
			InterfaceAdvanceConfiguration advanceInfo) {
		if (advanceInfo.getChannelBandwidth() == 1
				|| advanceInfo.getChannelBandwidth() == 0) {
			setChannelBandwidth(1);
			secondaryChannelPosition(advanceInfo.getChannelBandwidth());
//			setGuardInterval_40(advanceInfo.getGuardInterval_40());
			
		} else if (advanceInfo.getChannelBandwidth() == 2) {
			setChannelBandwidth(0);
//			setGuardInterval_20(advanceInfo.getGuardInterval_20());
			
		} else if (advanceInfo.getChannelBandwidth() == 3) {
			setChannelBandwidth(2);
//			setGuardInterval_80(advanceInfo.getGuardInterval_80());
		}
		
	}

	/*
	 * compare UI data with local config
	 */
	public boolean compareUIDataWithConfiguration(
			InterfaceAdvanceConfiguration ifAdvance) {
		try {
			String strChannel = cmbChannelBandwidth.getItem(cmbChannelBandwidth
					.getSelectionIndex());
			int channel = (int) cmbChannelBandwidth.getData(strChannel);
			if (channel == 0) {
				String strGuardIntyerval = cmbGuardInterval_40
						.getItem(cmbGuardInterval_40.getSelectionIndex());
				int guardInterval = (int) cmbGuardInterval_40
						.getData(strGuardIntyerval);
				if (guardInterval != ifAdvance.getGuardInterval_20())
					return false;
			} else if (channel == 1) {
				String strGuardIntyerval = cmbGuardInterval_40
						.getItem(cmbGuardInterval_40.getSelectionIndex());
				int guardInterval = (int) cmbGuardInterval_40
						.getData(strGuardIntyerval);
				if (guardInterval != ifAdvance.getGuardInterval_40())
					return false;

				byte secondaryChannelAbove = 0;
				if (secondaryChannelPositionAboveButton.getSelection() == true) {
					secondaryChannelAbove = 1;
				}
				if (secondaryChannelAbove != ifAdvance
						.getSecondaryChannelPosition())
					return false;

				byte secondaryChannelbelow = 0;
				if (secondaryChannelPositionAboveButton.getSelection() == true) {
					secondaryChannelbelow = 1;
				}
				if (secondaryChannelbelow != ifAdvance
						.getSecondaryChannelPosition())
					return false;
			} else if (channel == 2) {
				String strGuardIntyerval = cmbGuardInterval_20
						.getItem(cmbGuardInterval_20.getSelectionIndex());
				int guardInterval = (int) cmbGuardInterval_20
						.getData(strGuardIntyerval);
				if (guardInterval != ifAdvance.getGuardInterval_80())
					return false;
			}
			
			
			/*
			 * String strGuardIntyerval = cmbGuardInterval
			 * .getItem(cmbGuardInterval.getSelectionIndex()); int guardInterval
			 * = (int) cmbGuardInterval .getData(strGuardIntyerval); if
			 * (guardInterval != ifAdvance.getGuardInterval_20()) return false;
			 */

			String strMaxAMPDU = cmbMaxAMPDU.getItem(cmbMaxAMPDU
					.getSelectionIndex());
			int MaxAMPDU = (int) cmbMaxAMPDU.getData(strMaxAMPDU);
			if (MaxAMPDU != ifAdvance.getMaxAMPDU())
				return false;

			String strMaxAMSDU = cmbMaxAMSDU.getItem(cmbMaxAMSDU
					.getSelectionIndex());
			int MaxAMSDU = (int) cmbMaxAMSDU.getData(strMaxAMSDU);
			if (MaxAMSDU != ifAdvance.getMaxAMSDU())
				return false;

			String strMaxMPDU = cmbMaxMPDU.getItem(cmbMaxMPDU
					.getSelectionIndex());
			int MaxMPDU = (int) cmbMaxMPDU.getData(strMaxMPDU);
			if (MaxMPDU != ifAdvance.getMaxMPDU())
				return false;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		byte ldbc = 0;
		if (ldpcButton.getSelection() == true)
			ldbc = 1;
		if (ifAdvance.getLDPC() != ldbc)
			return false;
		byte txStbc = 0;
		if (txSTBCButton.getSelection() == true)
			txStbc = 1;
		if (ifAdvance.getTxSTBC() != txStbc)
			return false;
		byte rxStbc = 0;
		if (rxSTBCButton.getSelection() == true)
			rxStbc = 1;
		if (ifAdvance.getRxSTBC() != rxStbc)
			return false;
		byte gfMode = 0;
		if (gfModeButton.getSelection() == true)
			gfMode = 1;
		if (ifAdvance.getGFMode() != gfMode)
			return false;
		byte maxMPDUEnabled = 0;
		if (frameAggregationButton.getSelection() == true)
			maxMPDUEnabled = 1;
		if (ifAdvance.getFrameAggregation() != maxMPDUEnabled)
			return false;
		byte coexistence = 0;
		if (coexistenceButton.getSelection() == true)
			coexistence = 1;
		if (ifAdvance.getCoexistence() != coexistence)
			return false;
		System.out.println();
		return true;
	}

	/*
	 * set Gui changes to interfaceAdvanceConfiguration
	 */
	private boolean getDataFromUIToConfiguration(
			InterfaceAdvanceConfiguration ifAdvance) {

		try {
			String strChannel = cmbChannelBandwidth.getItem(cmbChannelBandwidth
					.getSelectionIndex());
			int channel = (int) cmbChannelBandwidth.getData(strChannel);
			if (channel == 0) {
				ifAdvance.setChannelBandwidth((byte) 2);
			} else if (channel == 1) {
				if (secondaryChannelSelectedOrNot()) {
					return false;
				}
				if (secondaryChannelPositionAboveButton.getSelection() == true) {
					ifAdvance.setSecondaryChannelPosition((byte) 1);
					ifAdvance.setChannelBandwidth((byte) 0);
				}
				if (secondaryChannelPositionBelowButton.getSelection() == true) {
					// ifAdvance.setSecondaryChannelPosition((byte) 0);
					ifAdvance.setChannelBandwidth((byte) 1);
				}
			} else if (channel == 2) {

				ifAdvance.setChannelBandwidth((byte) 3);
			}

			String strGuardIntyerval_20 = cmbGuardInterval_20
					.getItem(cmbGuardInterval_20.getSelectionIndex());
			int guardInterval_20 = (int) cmbGuardInterval_20
					.getData(strGuardIntyerval_20);
			ifAdvance.setGuardInterval_20((byte)guardInterval_20);
			String strGuardIntyerval_40 = cmbGuardInterval_40
					.getItem(cmbGuardInterval_40.getSelectionIndex());
			int guardInterval_40 = (int) cmbGuardInterval_40
					.getData(strGuardIntyerval_40);
			ifAdvance.setGuardInterval_40((byte)guardInterval_40);
			String strGuardIntyerval_80 = cmbGuardInterval_80
					.getItem(cmbGuardInterval_80.getSelectionIndex());
			int guardInterval_80 = (int) cmbGuardInterval_80
					.getData(strGuardIntyerval_80);
			ifAdvance.setGuardInterval_80((byte)guardInterval_80);
			
			String strPreamble = cmbPreamble.getItem(cmbPreamble
					.getSelectionIndex());
			int preambleValue = (int) cmbPreamble.getData(strPreamble);
			ifAdvance.setPreamble((byte) preambleValue);

			String strMaxAMPDU = cmbMaxAMPDU.getItem(cmbMaxAMPDU
					.getSelectionIndex());
			int MaxAMPDU = (int) cmbMaxAMPDU.getData(strMaxAMPDU);
			ifAdvance.setMaxAMPDU((byte) MaxAMPDU);

			String strMaxAMSDU = cmbMaxAMSDU.getItem(cmbMaxAMSDU
					.getSelectionIndex());
			int MaxAMSDU = (int) cmbMaxAMSDU.getData(strMaxAMSDU);
			ifAdvance.setMaxAMSDU((byte) MaxAMSDU);

			String strMaxMPDU = cmbMaxMPDU.getItem(cmbMaxMPDU
					.getSelectionIndex());
			int MaxMPDU = (int) cmbMaxMPDU.getData(strMaxMPDU);
			ifAdvance.setMaxMPDU((short) MaxMPDU);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		if (ldpcButton.getSelection() == true)
			ifAdvance.setLDPC((byte) 1);
		else
			ifAdvance.setLDPC((byte) 0);

		if (txSTBCButton.getSelection() == true)
			ifAdvance.setTxSTBC((byte) 1);
		else
			ifAdvance.setTxSTBC((byte) 0);

		if (rxSTBCButton.getSelection() == true)
			ifAdvance.setRxSTBC((byte) 1);
		else
			ifAdvance.setRxSTBC((byte) 0);

		if (gfModeButton.getSelection() == true)
			ifAdvance.setGFMode((byte) 1);
		else
			ifAdvance.setGFMode((byte) 0);

		if (frameAggregationButton.getSelection() == true)
			ifAdvance.setMaxAMPDUEnabled((byte) 1);
		else
			ifAdvance.setMaxAMPDUEnabled((byte) 0);

		if (coexistenceButton.getSelection() == true)
			ifAdvance.setCoexistence((byte) 1);
		else
			ifAdvance.setCoexistence((byte) 0);

		return true;
	}

	/*
	 * set value for secondary channel position
	 */
	private void secondaryChannelPosition(int position) {
		lblsecondaryCannelPosition.setEnabled(true);
		secondaryChannelPositionAboveButton.setEnabled(true);
		secondaryChannelPositionBelowButton.setEnabled(true);
		if (position == 0) {
			secondaryChannelPositionAboveButton.setSelection(true);
		}
		if (position == 1) {
			secondaryChannelPositionBelowButton.setSelection(true);
		}

	}

	/***
	 * 
	 * @return
	 */
	private boolean secondaryChannelSelectedOrNot() {
		try {
			String strChannel = cmbChannelBandwidth.getItem(cmbChannelBandwidth
					.getSelectionIndex());
			int channel = (int) cmbChannelBandwidth.getData(strChannel);
			if (channel == 1) {
				if (!(secondaryChannelPositionAboveButton.getSelection() == true || secondaryChannelPositionBelowButton
						.getSelection() == true)) {
					MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),
							SWT.OK | SWT.ICON_ERROR);
					msg.setMessage("Should be selected any one secondary channel ");
					msg.setText("Network Viewer " + NMS_VERSION2.buildmajor
							+ "." + NMS_VERSION2.buildminor);
					msg.open();
					return true;
				}
			}
		} catch (Exception e) {
			return false;
		}

		return false;
	}

	/***
	 * 
	 * @param ifAdvance
	 */
	private void setSecondaryChannelPositionWithChannel(
			InterfaceAdvanceConfiguration ifAdvance) {
		if (ifAdvance.getSecondaryChannelPosition() == 1) {
			secondaryChannelPositionAboveButton.setSelection(true);
		}
		if (ifAdvance.getSecondaryChannelPosition() == 0) {
			secondaryChannelPositionBelowButton.setSelection(true);
		}
	}

	/***
	 * 
	 * @param ifAdvance
	 */
	private void setGuardIntervalWithChannel_40(
			InterfaceAdvanceConfiguration ifAdvance) {
		
		cmbGuardInterval_20.setEnabled(true);
		cmbGuardInterval_40.setEnabled(true);
		cmbGuardInterval_80.setEnabled(false);
		cmbGuardInterval_20.select(2);
		cmbGuardInterval_40.select(2);
		cmbGuardInterval_80.select(0);
		
	}

	private void setGuardIntervalWithChannel_20(
			InterfaceAdvanceConfiguration ifAdvance) {
		
		cmbGuardInterval_20.setEnabled(true);
		cmbGuardInterval_40.setEnabled(false);
		cmbGuardInterval_80.setEnabled(false);
		cmbGuardInterval_20.select(2);
		cmbGuardInterval_40.select(0);
		cmbGuardInterval_80.select(0);
	}

	private void setGuardIntervalWithChannel_80(
			InterfaceAdvanceConfiguration ifAdvance) {
		
		cmbGuardInterval_20.setEnabled(true);
		cmbGuardInterval_40.setEnabled(true);
		cmbGuardInterval_80.setEnabled(true);
		cmbGuardInterval_20.select(2);
		cmbGuardInterval_40.select(2);
		cmbGuardInterval_80.select(2);
		
	}
	
	private void setGuardInterval(InterfaceAdvanceConfiguration advanceInfo){
		
		if (advanceInfo.getChannelBandwidth() == 1
				|| advanceInfo.getChannelBandwidth() == 0) {
			cmbGuardInterval_20.setEnabled(true);
			cmbGuardInterval_40.setEnabled(true);
			cmbGuardInterval_80.setEnabled(false);
		} else if (advanceInfo.getChannelBandwidth() == 2) {
			cmbGuardInterval_20.setEnabled(true);
			cmbGuardInterval_40.setEnabled(false);
			cmbGuardInterval_80.setEnabled(false);
		} else if (advanceInfo.getChannelBandwidth() == 3) {
			cmbGuardInterval_20.setEnabled(true);
			cmbGuardInterval_40.setEnabled(true);
			cmbGuardInterval_80.setEnabled(true);
		}
		setGuardInterval_20(advanceInfo.getGuardInterval_20());
		setGuardInterval_40(advanceInfo.getGuardInterval_40());
		setGuardInterval_80(advanceInfo.getGuardInterval_80());
		
	}
	
	
}
