/**
 * MeshDynamics 
 * -------------- 
 * File     : GeneralConfigPage.java
 * Comments : 
 * Created  : Feb 21, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  --------------------------------------------------------------------------------
 * | 5  | Nov  5, 2007   |cCode check in updateCountryCodeInformation | Imran 		 |
 *  ----------------------------------------------------------------------------------
 * | 4  | May 03, 2007   | CmbCountryCodeHandler added    			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 3  | May 02, 2007   | Mobilty removed                			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 2  | Apr 16, 2007   | InitializeLocalData added      			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 1  | Apr 05, 2007   | UI validation are added        			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 21, 2007   | Created                        			  | Abhishek     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.dialogs.settings;



import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.configuration.IChannelInfo;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;
import com.meshdynamics.meshviewer.configuration.INetworkConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceCustomChannelConfig;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.util.ChannelListInfo;
import com.meshdynamics.meshviewer.util.CountryCodeComboHelper;
import com.meshdynamics.meshviewer.util.CountryData;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.meshviewer.util.MobilityInfo;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.controls.CountryCodeCombo;
import com.meshdynamics.nmsui.controls.Slider;
import com.meshdynamics.nmsui.dialogs.base.ConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;



public class GeneralConfigPage extends ConfigPage{

    private	static final int 		PRECISION 				= 6;
    private static final String		TEXT_MODE_STATIONARY 	= "Stationary";
    private static final String		TEXT_MODE_MOBILE_INFRA	= "Mobile Infrastructure";
    private static final String		TEXT_MODE_MOBILE 		= "Mobile";
    
	private Text				txtMacIdentifier;
	private Text 				txtModelNo;
	private Text				txtSoftwareVersionNo;
	private	Text				txtNodeName;
	private	Text				txtNodeDescription;
	private Text				txtLatitude;
	private Text 				txtLongitude;
	private Text				txtHostName;
	private Text				txtIpAddress;
	private Text 				txtSubnetMask;
	private Text				txtGateway;
	private Text				txtPreferredParent;
	
	private Label				lblHbiValue;
	private Label 				lblLatitude;
	private Label				lblLongitude;
	private Label				lblHopCostValue;
	private Label 				lblChangeResValue;
	private Label				lblHopCost;
	private Label				lblChangeRes;
	
	private Slider  			sldHbi;
	private Slider				sldHopCost;
	private Slider				sldChangeRes;
	
    private Group				grpTcpIp;
	private Group				grpMesh;
	private Group 				grpVersionSpecific;

	
	private Button				btnPrefParent;
	
	private Label				lblMobilityIndex;
    private Label				lblMobilityMode;
	private Label				lblMobilityIndexValue;
	private Combo				cmbMobilityMode;
	private Slider 				sldMobilityIndex;
	private Group 				grpMobility;
	private int					mobiltySetup;
	
	private int					rel_x;
	private int 				rel_y;
	
	private IConfiguration			configuration;
	private int 					countryCode;
	private MeshNetwork				meshNetwork;
	
	private CountryCodeCombo		cmbCountryCode;
	private CountryCodeComboHelper	cmbCountryCodeHelper;
	
	private IVersionInfo			versionInfo;
	
	public GeneralConfigPage(IMeshConfigBook parent) {
		
		super(parent);
		
		this.meshNetwork		= parent.getMeshNetWork();
		
		minimumBounds.width 	= 425;
		minimumBounds.height	= 500;
		
		configuration			= parent.getConfiguration();
		versionInfo				= parent.getVersionInfo();
		cmbCountryCodeHelper	= new CountryCodeComboHelper();
		
		mobiltySetup	= MobilityInfo.MOBILITY_SETUP_BASIC;
		
		/*
		 * Currently advanced mobility mode is not used
		 * 2 SEP 2008
		 */
/*		
		if( MeshViewerProperties.mobilityAdvanced == 1){
			mobiltySetup	= MobilityInfo.MOBILITY_SETUP_ADVANCED;	
		}else {
			mobiltySetup	= MobilityInfo.MOBILITY_SETUP_BASIC;
		}
*/
		
		setComboHelperValues();
	}


	/**
	 * 
	 */
	private void setComboHelperValues() {
		
		cmbCountryCodeHelper.setCountryCode(configuration.getCountryCode());
		cmbCountryCodeHelper.setHardwareModel(configuration.getHardwareModel());
		cmbCountryCodeHelper.setRegDomain(configuration.getRegulatoryDomain());
		cmbCountryCodeHelper.setCountryCodeChanged(false);
	}


	/**
	 * Function to create controls for Node Description
	 */
	private void createNodeDescription() {
		
		rel_x = 25;
		rel_y = 15;
		
		Label lab= 	new Label(canvas,SWT.NO);
		lab.setText("MAC Identifier");
		lab.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 140;
		
		txtMacIdentifier	= new Text(canvas,SWT.BORDER|SWT.READ_ONLY);
		txtMacIdentifier.setBounds(rel_x,rel_y,200,17);
		txtMacIdentifier.setFocus();
		
		rel_x  = 25;
		rel_y += 25;
		
		lab	= new Label(canvas,SWT.NO);
		lab.setText("Model Number");
		lab.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 140;
		
		txtModelNo	= new Text(canvas,SWT.BORDER |SWT.READ_ONLY);
		txtModelNo.setBounds(rel_x,rel_y,200,17);
				
		rel_x  = 25;
		rel_y += 25;
		
		lab	= new Label(canvas,SWT.NO);
		lab.setText("Firmware Version");
		lab.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 140;
		
		txtSoftwareVersionNo	= new Text(canvas,SWT.BORDER | SWT.READ_ONLY);
		txtSoftwareVersionNo.setBounds(rel_x,rel_y,70,17);		
		
		rel_x  = 25;
		rel_y += 25;
		
		lab	= new Label(canvas,SWT.NO);
		lab.setText("Node Name");
		lab.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 140;
		
		txtNodeName	= new Text(canvas,SWT.BORDER);
		txtNodeName.setBounds(rel_x,rel_y,200,17);
				
		rel_x  = 25;
		rel_y += 25;
		
		lab	= new Label(canvas,SWT.NO);
		lab.setText("Node Description");
		lab.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 140;
		
		txtNodeDescription	= new Text(canvas,SWT.BORDER);
		txtNodeDescription.setBounds(rel_x,rel_y,200,17);
				
		rel_x  = 25;
		rel_y += 25;
		
		lab	= new Label(canvas,SWT.NO);
		lab.setText("GPS Coordinates");
		lab.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 140;
		
		lblLatitude = new Label(canvas,SWT.NO);
		lblLatitude.setBounds(rel_x,rel_y,50,15);
		lblLatitude.setText("Latitude");
		
		rel_x += 50;
		
		txtLatitude	= new Text(canvas,SWT.BORDER);
		txtLatitude.setBounds(rel_x,rel_y,150,17);
		
		rel_x  = 165;
		rel_y += 25;
		
		lblLongitude	= new Label(canvas,SWT.NO);
		lblLongitude.setBounds(rel_x,rel_y,50,15);
		lblLongitude.setText("Longitude");
		
		rel_x += 50;
		
		txtLongitude	= new Text(canvas,SWT.BORDER);
		txtLongitude.setBounds(rel_x,rel_y,150,17);
		
		rel_x  = 25;
		rel_y += 25;
		
		Label label = new Label(canvas,SWT.NONE);
		label.setText("Country Code");
		label.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 140;
		
		cmbCountryCode	= new CountryCodeCombo(canvas,SWT.BORDER|SWT.READ_ONLY,cmbCountryCodeHelper);
		cmbCountryCode.setBounds(rel_x,rel_y,150,17);
		
		rel_y += 25;
	}
	
	/**
	 * Function to create TCP IP controls for general config page 
	 */
	private void createTCPIPSetting() {
		
		rel_y  = 215;
		rel_x  = 5;
		
		grpTcpIp	= new Group(canvas,SWT.NONE);
		grpTcpIp.setBounds(rel_x,rel_y,415,130);
		
		rel_y = 25;
		rel_x = 25;
		
		Label lab	= new Label(grpTcpIp,SWT.NO);
		lab.setText("Host Name");
		lab.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 135;
		
		txtHostName	= new Text(grpTcpIp,SWT.BORDER);
		txtHostName.setBounds(rel_x,rel_y,200,17);
		
		rel_x  = 25;
		rel_y += 25;
		
		lab	= new Label(grpTcpIp,SWT.NO);
		lab.setText("IP Address");
		lab.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 135;
			
		txtIpAddress	= new Text(grpTcpIp,SWT.BORDER);
		txtIpAddress.setBounds(rel_x,rel_y,200,17);
				
		rel_x  = 25;
		rel_y += 25;
		
		lab	= new Label(grpTcpIp,SWT.NO);
		lab.setText("Subnet Mask");
		lab.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 135;
		
		txtSubnetMask	= new Text(grpTcpIp,SWT.BORDER);
		txtSubnetMask.setBounds(rel_x,rel_y,200,17);
		
		
		rel_x  = 25;
		rel_y += 25;
		
		lab	= new Label(grpTcpIp,SWT.NO);
		lab.setText("Gateway");
		lab.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 135;
		
		txtGateway	= new Text(grpTcpIp,SWT.BORDER);
		txtGateway.setBounds(rel_x,rel_y,200,17);
	}
	
	/**
	 * Creates mobilty controls for MOBILITY SETUP BASIC
	 */
	private void createMobilitySetUpBasic() {
		
		rel_y = 15;		
	
		lblMobilityMode	= new Label(grpMobility,SWT.NO);
		lblMobilityMode.setText("Mobility Mode");
		lblMobilityMode.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 140;
		
		cmbMobilityMode	= new Combo(grpMobility,SWT.BORDER|SWT.READ_ONLY);
		cmbMobilityMode.setBounds(rel_x,rel_y,200,20);
		cmbMobilityMode.setVisible(true);
		cmbMobilityMode.select(0); 
	}
	
	/**
	 * Creates mobilty controls for MOBILITY SETUP ADVANCED
	 */
	private void createMobilitySetUpAdvanced() {
		
		rel_y = 50;		
		
		lblMobilityMode	= new Label(grpMobility,SWT.NO);
		lblMobilityMode.setText("Mobility Mode");
		lblMobilityMode.setBounds(rel_x,rel_y,100,15);
			
		rel_x += 132;
		
		
		cmbMobilityMode	= new Combo(grpMobility,SWT.BORDER|SWT.READ_ONLY);
		cmbMobilityMode.setBounds(rel_x,rel_y,200,20);
		cmbMobilityMode.setVisible(true);
		cmbMobilityMode.select(0); 
		cmbMobilityMode.addSelectionListener(new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
		
				if(cmbMobilityMode.getSelectionIndex() == IMeshConfiguration.MOBILITY_MODE_STATIONARY) {
					sldMobilityIndex.setEnabled(false);
					sldMobilityIndex.setValue(IMeshConfiguration.MOBILITY_MODE_STATIONARY);
				} else if(cmbMobilityMode.getSelectionIndex() == IMeshConfiguration.MOBILITY_MODE_MOBILE){
					sldMobilityIndex.setEnabled(true);
					sldMobilityIndex.setValue(IMeshConfiguration.MOBILITY_MODE_MOBILE);						
				} 
		 }
		});
		
		rel_y  += 30;	
		rel_x  = 25;
			
		lblMobilityIndex	= new Label(grpMobility, SWT.NO);
		lblMobilityIndex.setText("Mobility Index");
		lblMobilityIndex.setBounds(rel_x, rel_y, 100, 15);

		rel_x += 132;
		
		sldMobilityIndex	= new Slider(grpMobility,SWT.FLAT);
		sldMobilityIndex.setMaxValue(20);
		sldMobilityIndex.setMinValue(0);
		sldMobilityIndex.setBounds(rel_x,rel_y,200,20);
		sldMobilityIndex.addValueListener(new Listener(){
			public void handleEvent(Event arg0) {
				lblMobilityIndexValue.setText(" "+sldMobilityIndex.getValue());
			}			
		});
		
		lblMobilityIndexValue	= new Label(grpMobility,SWT.NO);
		lblMobilityIndexValue.setBounds(rel_x+205,rel_y-2,30,15);
		lblMobilityIndexValue.setText(" 0");
	}
	
	private void createMobilityControls() {
		
	    rel_y  = 415;
		rel_x  = 5;
		
		switch(mobiltySetup){
		
			case MobilityInfo.MOBILITY_SETUP_BASIC:
				grpMobility = new Group(canvas,SWT.NONE);
				grpMobility.setBounds(rel_x,rel_y,415,50);
				rel_x = 25;
				createMobilitySetUpBasic();
				break;
			case MobilityInfo.MOBILITY_SETUP_ADVANCED:
				grpMobility = new Group(canvas,SWT.NONE);
				grpMobility.setBounds(rel_x,rel_y,415,95);
				rel_x = 25;
				createMobilitySetUpAdvanced();
				break;
		}
	}
	
	/**
	 * Initializes mobility information according to the mobility setup.
	 * Mobility setup can be  MOBILITY_SETUP_NONE, MOBILITY_SETUP_BASIC,
	 * MOBILITY_SETUP_ADVANCED.
	 */
	private void initializeMobilityConfigData() {
		
	    IMeshConfiguration	meshConfig	= configuration.getMeshConfiguration();
		if(meshConfig == null) {
			return;
		}
		short 				mode		= meshConfig.getMobilityMode();
		
		if(mobiltySetup == MobilityInfo.MOBILITY_SETUP_BASIC) {
		
			cmbMobilityMode.removeAll();
			cmbMobilityMode.add(TEXT_MODE_STATIONARY);
			if(isScannerBoard() == true)
			    cmbMobilityMode.add(TEXT_MODE_MOBILE);
			else
			    cmbMobilityMode.add(TEXT_MODE_MOBILE_INFRA);
			
			if(mode == IMeshConfiguration.MOBILITY_MODE_STATIONARY) {
				cmbMobilityMode.select(IMeshConfiguration.MOBILITY_MODE_STATIONARY);
			} else if(mode == IMeshConfiguration.MOBILITY_MODE_MOBILE) {
			    cmbMobilityMode.select(IMeshConfiguration.MOBILITY_MODE_MOBILE);
			}
		}else if(mobiltySetup	== MobilityInfo.MOBILITY_SETUP_ADVANCED) {
			int	index	= meshConfig.getMobilityIndex(); 	
		
			cmbMobilityMode.removeAll();
			cmbMobilityMode.add(TEXT_MODE_STATIONARY);
			if(isScannerBoard() == true)
			    cmbMobilityMode.add(TEXT_MODE_MOBILE);
			else
			    cmbMobilityMode.add(TEXT_MODE_MOBILE_INFRA);
			
			if(mode == IMeshConfiguration.MOBILITY_MODE_STATIONARY) {
				sldMobilityIndex.setEnabled(false);
				sldMobilityIndex.setValue(0);
				cmbMobilityMode.select(IMeshConfiguration.MOBILITY_MODE_STATIONARY);
				
			} else if(mode == IMeshConfiguration.MOBILITY_MODE_MOBILE) {
				
				sldMobilityIndex.setValue(index);
			}
		}
	}

	/**
	 * Function to create Mesh controls for general config page
	 */
	private void createMeshParameters() {
		
		rel_y  = 340;
		rel_x  = 5;
		
		grpMesh = new Group(canvas,SWT.NO);
		grpMesh.setBounds(rel_x,rel_y,415,80);
		
		rel_y = 25;
		rel_x = 25;
		
		Label lab	= new Label(grpMesh,SWT.NO);
		lab.setText("Preferred Parent");
		lab.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 135;
		
		txtPreferredParent	= new Text(grpMesh,SWT.BORDER);
		txtPreferredParent.setBounds(rel_x,rel_y,140,17);
		txtPreferredParent.addKeyListener(new KeyAdapter(){
            public void keyPressed(KeyEvent arg0) {
               
            }
		});
		
		btnPrefParent = new Button(grpMesh,SWT.PUSH);
		btnPrefParent.setText("Browse");
		btnPrefParent.setBounds(rel_x + 145,rel_y,55,17);
		btnPrefParent.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				PreferredParentDialog	prefParentDlg	= new PreferredParentDialog(configuration, meshNetwork);
				prefParentDlg.show();
				if(prefParentDlg.getStatus() == SWT.OK) {
					txtPreferredParent.setText(prefParentDlg.getPrefParent());
				}
			}
		});
		
		rel_x   = 25;
		rel_y  += 25;
		
		lab	= new Label(grpMesh,SWT.NO);
		lab.setText("Heartbeat Interval");
		lab.setBounds(rel_x,rel_y,100,15);
		
		rel_x += 132;
		
		sldHbi	=	new Slider(grpMesh,SWT.FLAT);
		sldHbi.setBounds(rel_x,rel_y+5,200,15);
		sldHbi.setMinValue(1);
		sldHbi.setChangeValue(10);
		
		sldHbi.addValueListener(new Listener(){
			public void handleEvent(Event arg0) {
				lblHbiValue.setText(""+sldHbi.getValue() + " sec(s)");			
			}			
		});
		
		lblHbiValue	= new Label(grpMesh,SWT.NO);
		lblHbiValue.setBounds(rel_x+200,rel_y,45,15);
		lblHbiValue.setText(" 0 sec(s)");
		
		rel_x  = 25;
		rel_y += 25;		
				
	}
	
	/**
	 * Function to create various controls for General Config Page
	 */
	public void createControls(CTabFolder tabFolder) {
	
		super.createControls(tabFolder);
		createNodeDescription();
		createTCPIPSetting();
		createMeshParameters();
		createMobilityControls();
		createVersionSpecificControls(); 
		ContextHelp.addContextHelpHandlerEx(this.canvas,contextHelpEnum.CONFIGTABGENERAL);
	}

	private void createVersionSpecificControls() {

		if(versionInfo.versionLessThan(IVersionInfo.MAJOR_VERSION_2,
									   IVersionInfo.MINOR_VERSION_5,
									   IVersionInfo.VARIANT_VERSION_1)) {
			
			rel_y  = 415;
			rel_x  = 5;
			
			grpVersionSpecific	= new Group(canvas,SWT.NO);
			grpVersionSpecific.setBounds(rel_x,rel_y,415,95);
			
			rel_y = 25;
			rel_x = 25;
			
			lblHopCost	= new Label(grpVersionSpecific,SWT.NO);
			lblHopCost.setText("Hop Cost");
			lblHopCost.setBounds(rel_x,rel_y,100,15);
			
			rel_x += 132;
			
			sldHopCost	= new Slider(grpVersionSpecific,SWT.FLAT);
			sldHopCost.setBounds(rel_x,rel_y,200,15);
			sldHopCost.addValueListener(new Listener(){
				public void handleEvent(Event arg0) {
					lblHopCostValue.setText(""+sldHopCost.getValue() + " units");				
				}			
			});
			
			lblHopCostValue	= new Label(grpVersionSpecific,SWT.NO);
			lblHopCostValue.setBounds(rel_x+200,rel_y-3,40,15);
			lblHopCostValue.setText("0 units");
					
			rel_x =  25;
			rel_y += 25;
			
			lblChangeRes	= new Label(grpVersionSpecific,SWT.NO);
			lblChangeRes.setText("Change Resistance");
			lblChangeRes.setBounds(rel_x,rel_y,100,15);
			
			rel_x += 132;
				
			sldChangeRes	= new Slider(grpVersionSpecific,SWT.FLAT);
			sldChangeRes.setBounds(rel_x,rel_y,200,15);
			sldChangeRes.addValueListener(new Listener(){
				public void handleEvent(Event arg0) {
					lblChangeResValue.setText(""+sldChangeRes.getValue() + " units");
				}			
			});
			
			lblChangeResValue	= new Label(grpVersionSpecific,SWT.NO);
			lblChangeResValue.setBounds(rel_x+200,rel_y-3,40,15);
			lblChangeResValue.setText("0 units");
		}
	}


	public void selectionChanged(boolean selected) {
		
		if(selected	== true) {
			initalizeLocalData();
		}else {
			if(validateLocalData() == false) {
				stayOnSameTab();
				return;
			}
			if(compareUIwithLocalConfig()	== false){
				
				int result	= parent.showMessage("Do you want to save the changes ? ",SWT.YES|SWT.CANCEL|SWT.NO);
				if(result == SWT.CANCEL) {
					stayOnSameTab();
					return;
				}else if(result	== SWT.NO) {
					cmbCountryCodeHelper.setCountryCodeChanged(false);
					initalizeLocalData();
				}else if(result	== SWT.YES) {
					getDataFromUIToConfiguration();
				}	
			}
		}
	}

	
	private boolean compareMobilityConfigurationWithUI() {
		
		IMeshConfiguration	meshConfig	= configuration.getMeshConfiguration();
		
		if(meshConfig == null) {
			return false;
		}
		if(mobiltySetup == MobilityInfo.MOBILITY_SETUP_BASIC) {
				
				short 	mode	= meshConfig.getMobilityMode();
				
				if(mode	!= cmbMobilityMode.getSelectionIndex()) {
					return false;
				}
				if(mode == IMeshConfiguration.MOBILITY_MODE_STATIONARY) {
					return true;
				}
			return true;
			
			}else if(mobiltySetup == MobilityInfo.MOBILITY_SETUP_ADVANCED) {
				if(meshConfig.getMobilityIndex() != sldMobilityIndex.getValue()){
					return false;
				}
			}
			return true;
	}
	
	/**
	 * @return
	 */
	private boolean isScannerBoard() {
			
		IInterfaceConfiguration	interfaceConfiguration	= configuration.getInterfaceConfiguration();
		if(interfaceConfiguration == null) {
			return false;
		}
		int ifCnt	= interfaceConfiguration.getInterfaceCount();
		for(int i = 0; i < ifCnt; i++) {
			IInterfaceInfo interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(i);
			if(interfaceInfo == null) {
				continue;
			}
			if(interfaceInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_AMON ||
			   interfaceInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_PMON) {
				return true;
			}
		}
		return false;
	}


	/**
	 * @param meshConfig
	 */
	private boolean compareMeshConfigurationWithUI(IMeshConfiguration meshConfig) {
	
		if(meshConfig == null) {
			return false;
		}
		if(meshConfig.getHeartbeatInterval() != ((short)sldHbi.getValue())){
			return false;
		}
		
		return compareMobilityConfigurationWithUI();
	}

	/**
	 * @param networkConfig
	 */
	private boolean compareNetworkConfigurationWithUI(INetworkConfiguration networkConfig) {
	
		if(networkConfig == null) {
			return false;
		}
		if(networkConfig.getHostName().equals(txtHostName.getText().trim())	== false) {
			return false;
		}
		
		IpAddress ip = new IpAddress();
		ip.setBytes(txtIpAddress.getText());
		if(networkConfig.getIpAddress().equals(ip)	== false) {
			return false;
		}
		
		IpAddress subnet = new IpAddress();
		subnet.setBytes(txtSubnetMask.getText());
		if(networkConfig.getSubnetMask().equals(subnet)	== false) {
			return false;
		}
		
		IpAddress gateWay = new IpAddress();
		gateWay.setBytes(txtGateway.getText());
		if(networkConfig.getGateway().equals(gateWay)	== false)	{
			return false;
		}
		return true;
	}


	/**
	 * @return
	 */
	private boolean compareUIwithLocalConfig() {
		
		if(configuration == null) {
			return false; 
		}
			
		if(configuration.getFirmwareVersion().equals(txtSoftwareVersionNo.getText().trim()) == false){
			return false;
		}
		if(configuration.getNodeName().equals(txtNodeName.getText().trim())	== false) {
			return false;
		}
		if(configuration.getDescription().equals(txtNodeDescription.getText().trim()) == false){
			return false;
		}
		if(configuration.getLatitude().equals(txtLatitude.getText().trim())	== false) {
			return false;
		}
		if(configuration.getLongitude().equals(txtLongitude.getText().trim())	== false) {
			return false;
		}
	
	
		int cCode	= cmbCountryCode.getCountryCode();
		if(configuration.getCountryCode() != cCode) {
			cmbCountryCodeHelper.setCountryCodeChanged(true);
			return false;
		}
		
		INetworkConfiguration networkConfig	= configuration.getNetworkConfiguration();
		if(compareNetworkConfigurationWithUI(networkConfig)	== false) {
			return false;
		}
	
	 	IMeshConfiguration	  meshConfig	= configuration.getMeshConfiguration();
		if(compareMeshConfigurationWithUI(meshConfig) == false) {
			return false;
		}
		if(configuration.getPreferedParent().equals(txtPreferredParent.getText().trim())	== false) {
			return false;
		}
		if(compareVersionSpecificInfo() == false){
			return false;
		}
		return true;
	}

	
	/**
	 * @return
	 */
	private boolean compareVersionSpecificInfo() {
		
		
		if(versionInfo.versionLessThan(IVersionInfo.MAJOR_VERSION_2,
									   IVersionInfo.MINOR_VERSION_5,
									   IVersionInfo.VARIANT_VERSION_1)) {
	
			IMeshConfiguration	  meshConfiguration	= configuration.getMeshConfiguration();
			if(meshConfiguration == null) {
				return false;
			}
			if(meshConfiguration.getHopCost() != ((short)sldHopCost.getValue())){
				return false;
			}
			if(meshConfiguration.getChangeResistanceThreshold()	!= ((short)sldChangeRes.getValue())) {
				return false;
			}
		
		}
		return true; 
	}


	public void setModified() {
	}

	
	/**
	 *  Fills networkConfiguration Information. Information includes hostname, IP address,
	 *  gateway, subnet mask.
	 */
	private void initializeNetworkConfigData() {
		
		INetworkConfiguration networkConfig	=  configuration.getNetworkConfiguration();
		String 	  temp						= 	"";
		IpAddress ip;
		
		temp = networkConfig.getHostName();
		
		if(temp!=null) {
			txtHostName.setText(temp);
		}
		
		ip = networkConfig.getIpAddress();
		if(ip!=null) {
			txtIpAddress.setText(""+ip.toString());
		}
		
		ip = networkConfig.getGateway();
		if(ip!=null) {
			txtGateway.setText(""+ip.toString());
		}
		
		ip = networkConfig.getSubnetMask();
		if(ip!=null) {
			txtSubnetMask.setText(""+ip.toString());
		}
	}

	/**
	 *  Initializes Mesh Configuration in UI. It includes preferred parent mac address, heartbeat interval,
	 */
	private void initializeMeshConfigData() {
		
		IMeshConfiguration	meshConfig	= configuration.getMeshConfiguration();
		
		if(meshConfig == null)
			return; 
		
		String	mac	= configuration.getPreferedParent();
		
		if(mac!=null) {
			if(MeshValidations.isMacAddress(""+mac.toString())) {
				txtPreferredParent.setText(""+mac.toString());
			}
		}else {			   
			txtPreferredParent.setText("");
		}
		
		if(meshConfig.getHeartbeatInterval() > sldHbi.getMaxValue()) {
			sldHbi.setValue(sldHbi.getMaxValue());
		}else if(meshConfig.getHeartbeatInterval() < sldHbi.getMinValue()) {
			sldHbi.setValue(sldHbi.getMinValue());
		}else {
			sldHbi.setValue(meshConfig.getHeartbeatInterval());
		}
		
		short[] smap   = meshConfig.getSignalMap();
		String smapStr = "";
		int    i;		
		
		for(i = 0; i < 8; i++) {
			smapStr += smap[i];
			if(i < 7) {
				smapStr += ",";
			}
		}
		
	}
		/**
	 * 
	 */
	public void initalizeLocalData() {
		
		String	temp	= null;
		
		if(configuration == null) { 
			return;
		}	
				
		MacAddress	macAddress	= configuration.getDSMacAddress();
		if(!macAddress.equals(MacAddress.resetAddress)) { 
			txtMacIdentifier.setText(macAddress.toString());
		}else { 
			txtMacIdentifier.setText(MacAddress.resetAddress.toString());
		}
	    
		temp	= configuration.getHardwareModel();
		if(temp!=null) {
			txtModelNo.setText(temp);
		}else {
		    txtModelNo.setText("");
		}

		temp	= configuration.getFirmwareVersion();
		if(temp!=null) {
			txtSoftwareVersionNo.setText(temp);
		}else {
		    txtSoftwareVersionNo.setText("");
		}
		
		temp	= configuration.getNodeName();
		if(temp!=null) {
			txtNodeName.setText(temp);
		}else {
			txtNodeName.setText("");
		}
		
		temp	= configuration.getDescription();
		if(temp!=null){
			txtNodeDescription.setText(temp);
		}else {
			txtNodeDescription.setText("");
		}
					
		temp	= configuration.getLatitude(); 
		if(temp!=null) {
			txtLatitude.setText(temp);
		}else {
			txtLatitude.setText("");
		}
							
		temp	= configuration.getLongitude();
		if(temp!=null) {
			txtLongitude.setText(temp);
		}else {
			txtLongitude.setText("");
		}
		
		countryCode	= configuration.getCountryCode();
		//cmbCountryCodeHelper.setCountryCode(countryCode);
		cmbCountryCode.setCountryCodeData(countryCode);
		/**
		 * If rf configuration is changed and board is not rebooted
		 * or
		 * If Country code is changed and board is not rebooted
		 * user is not permitted to change the country code.
	 	 *//*
		if(parent.isRfConfigChanged()  		== true || 
		   parent.isCountryCodeChanged()	== true) {
			cmbCountryCode.setEnabled(false);
		}*/
		if(parent.isRfConfigChanged() == true) {
			cmbCountryCode.setEnabled(false);
		}
		initializeNetworkConfigData();
		initializeMeshConfigData();
		initializeMobilityConfigData();
		initalizeVersionSpecificData();
		
	}


	
	/**
	 * Initializes version specific information
	 */
	private void initalizeVersionSpecificData() {
		
		if(versionInfo.versionLessThan(IVersionInfo.MAJOR_VERSION_2,
									   IVersionInfo.MINOR_VERSION_5,
									   IVersionInfo.VARIANT_VERSION_1)) {
	
			IMeshConfiguration	  meshConfiguration	= configuration.getMeshConfiguration();
			if(meshConfiguration == null) {
				return;
			}	
			
			sldHopCost.setValue(meshConfiguration.getHopCost());
			lblHopCostValue.setText("" + sldHopCost.getValue() + " units");
			
			sldChangeRes.setValue(meshConfiguration.getChangeResistanceThreshold());
			lblChangeResValue.setText("" + sldChangeRes.getValue() + " units");
			
		}
	}


	private boolean validateNodeDescription() {

		String str  = "";
		str			= txtNodeName.getText().trim();
		
		if(str.equalsIgnoreCase("")){
			parent.showMessage("Node Name not entered. ",SWT.ICON_ERROR);
			txtNodeName.setFocus();
			return false;
		}
		
		if(str.length() > 256) {
			parent.showMessage("Node Name exceeds max character length. ",SWT.ICON_ERROR);
			txtNodeName.setFocus();
			return false;
		}
		
		str	= txtNodeDescription.getText();
		if(str.trim().equalsIgnoreCase("")){
			txtNodeDescription.setText("<<enter description>>");	//default value for node description
		}
		
		if(str.length() > 256) {
			parent.showMessage("Node Description exceeds max character length. ",SWT.ICON_ERROR);
			txtNodeDescription.setFocus();
			return false;
		}
		
		str	= txtLatitude.getText();
		if(str.equals("")){
			parent.showMessage("Latitude incorrect.",SWT.ICON_ERROR);
			txtLatitude.setFocus();
			return false;
		}
		
		try {
		    Double.parseDouble(str);
		}catch(Exception e){
		    parent.showMessage("Latitude incorrect.",SWT.ICON_ERROR);
			txtLatitude.setFocus();
			return false;
		}
		
		if(str.equals("0") == false) {
			String mantissa = str.substring(str.indexOf(".") + 1);
			if(mantissa.getBytes().length != PRECISION ) {
				parent.showMessage("Latitude Precision should be "+ PRECISION +" decimal places only ",SWT.ICON_ERROR);
				txtLatitude.setFocus();
				return false;
			}
		}
		str = txtLongitude.getText();
		if(str.equals("")){
			parent.showMessage("Longitude incorrect. ",SWT.ICON_ERROR);
			txtLongitude.setFocus();
			return false;
		}

		try {
		    Double.parseDouble(str);
		}catch(Exception e){
		    parent.showMessage("Longitude incorrect.",SWT.ICON_ERROR);
			txtLongitude.setFocus();
			return false;
		}
		
		if(str.equals("0") == false) {
			String mantissa = str.substring(str.indexOf(".") + 1);
			
			if(mantissa.getBytes().length != PRECISION ) {
				parent.showMessage("Longitude Precision should be "+ PRECISION +" decimal places only ",SWT.ICON_ERROR);
				txtLongitude.setFocus();
				return false;
			}
		}
		return true;
	}
	
	private boolean ipAddressGatewayCheck() {
		
		IpAddress ipAddress 	= new IpAddress();
		IpAddress subnetMask	= new IpAddress();
		IpAddress gateway 		= new IpAddress();
		
		ipAddress.setBytes	(txtIpAddress.getText().trim());
		subnetMask.setBytes	(txtSubnetMask.getText().trim());
		gateway.setBytes	(txtGateway.getText().trim());
		int i;
		
		for(i = 0; i < 4; i++){
			
			int ipSubnet 		= ipAddress.ipAddress[i] & subnetMask.ipAddress[i];
			int gatewaySubnet 	= gateway.ipAddress[i] 	 & subnetMask.ipAddress[i];
			if((ipSubnet ^ gatewaySubnet) != 0)
				return false;
		}
		return true;
	}
	
	private boolean validateTCPIPSettings() {

		String str	= "";
		str 	   	= 	txtHostName.getText().trim();
		if(str.equalsIgnoreCase("")){
			parent.showMessage("Hostname incorrect. ",SWT.ICON_ERROR);
			return false;
		}
		String tokens[] = str.split(" ");
		if(tokens.length > 1) {
			parent.showMessage("Hostname incorrect. ",SWT.ICON_ERROR);
			return false;
		}
		if(str.length() > 32){
			parent.showMessage("Hostname size should be below 32 charecters.",SWT.ICON_ERROR);
			return false;
		}
		
		str	= txtIpAddress.getText();
		if(MeshValidations.isIpAddress(str.trim())!= true){
			parent.showMessage("IPAddress incorrect. ",SWT.ICON_ERROR);
			txtIpAddress.setFocus();
			return false;			
		}
			
		str = txtSubnetMask.getText().trim();
		if(MeshValidations.isIpAddress(str)!= true){
			parent.showMessage("Subnet Mask incorrect. ",SWT.ICON_ERROR);
			txtSubnetMask.setFocus();
			return false;			
		}
				
		str = txtGateway.getText().trim();
		if(MeshValidations.isIpAddress(str)!= true){
			parent.showMessage("Gateway incorrect. ",SWT.ICON_ERROR);
			txtGateway.setFocus();
			return false;			
		}

		if(ipAddressGatewayCheck() == false){
			parent.showMessage("Gateway and IP Address not in the same subnet. ",SWT.ICON_ERROR);
			txtIpAddress.setFocus();
			return false;
		}
		return true;
	}
	
	
	public boolean validateLocalData() {
	
		if(validateNodeDescription() == false)
			return false;
		
		if(validateTCPIPSettings() == false)
			return false;
		
		String str	= txtPreferredParent.getText();
		if(MeshValidations.isMacAddress(str.trim())!=true){
			parent.showMessage("Preferred Parent incorrect.",SWT.ICON_ERROR);
			return false;			
		}

		return true;
	}
	
	
	
	private boolean getNetworkConfigurationFromUI(INetworkConfiguration networkConfig){
		
		if(networkConfig == null) {
			return false;
		}
		networkConfig.setHostName(txtHostName.getText().trim());
		
		IpAddress ip = new IpAddress();
		ip.setBytes(txtIpAddress.getText().trim());
		networkConfig.setIpAddress(ip);
		
		IpAddress subnet = new IpAddress();
		subnet.setBytes(txtSubnetMask.getText().trim());
		networkConfig.setSubnetMask(subnet);
		
		IpAddress gateWay = new IpAddress();
		gateWay.setBytes(txtGateway.getText().trim());
		networkConfig.setGateway(gateWay);
		
		return true;
	}

	private boolean getMobilityConfigurationFromUI(IMeshConfiguration meshConfig) {
		
		if(meshConfig == null) {
			return false;
		}
		if(mobiltySetup == MobilityInfo.MOBILITY_SETUP_BASIC) {
			short  mode			= (short)cmbMobilityMode.getSelectionIndex();
			meshConfig.setMobilityMode((short)mode);
		}else if(mobiltySetup == MobilityInfo.MOBILITY_SETUP_ADVANCED) {
			
			short  	mode	 = (short)cmbMobilityMode.getSelectionIndex();
			int 	mobility = (cmbMobilityMode.getSelectionIndex() == IMeshConfiguration.MOBILITY_MODE_MOBILE)? sldMobilityIndex.getValue():0;  
			
			meshConfig.setMobilityIndex((short)mobility);
			meshConfig.setMobilityMode((short)cmbMobilityMode.getSelectionIndex());
			
			/***
			 * These values are not set by user.
			 * These values are calculated depending upon the mobility index set by user.
			 */
			
			double 	speed			= MobilityInfo.speedMphValues[mobility];
			double 	distance		= MobilityInfo.distMileValues[mobility];
			int    	distanceUnit	= IMeshConfiguration.MOBILITY_DIST_MILES; 	
			int	  	speedUnit		= IMeshConfiguration.MOBILITY_SPEED_MPH;
				
			long  fragThreshold		= MobilityInfo.calulateFragThresholdValue((short) speed,distance,(byte) speedUnit,(byte) distanceUnit,mode);
			meshConfig.setFragThreshold(fragThreshold);	
		}
		return true;
	}
	/**
	 * @param meshConfig
	 */
	private boolean getMeshConfigurationFromUI(IMeshConfiguration meshConfig) {
		
		if(meshConfig == null) {
			return false;
		}
		meshConfig.setHeartbeatInterval((short)sldHbi.getValue());
		
		return getMobilityConfigurationFromUI(meshConfig);
	}
	
	public boolean getMobilityDataFromUIToConfiguration(){
		
		if(configuration == null) {
			return false; 
		}
		IMeshConfiguration	  meshConfig	= configuration.getMeshConfiguration();
		if(getMobilityConfigurationFromUI(meshConfig) == false) {
			return false;
		}
		return true;
	}
	
	public boolean getDataFromUIToConfiguration() {
		
		if(configuration == null) {
			return false; 
		}
		
		configuration.setNodeName(txtNodeName.getText());
		configuration.setDescription(txtNodeDescription.getText());
		configuration.setLatitude(txtLatitude.getText());
		configuration.setLongitude(txtLongitude.getText());
		
		int code		= cmbCountryCode.getCountryCode();
		cmbCountryCode.setCountryCodeData(code);
		INetworkConfiguration networkConfig	= configuration.getNetworkConfiguration();
		getNetworkConfigurationFromUI(networkConfig);
		
		IMeshConfiguration	  meshConfig	= configuration.getMeshConfiguration();
		getMeshConfigurationFromUI(meshConfig);
		
		getVersionSpecificData();
			
		
		configuration.setPreferedParent(txtPreferredParent.getText());

		int reg_domain  = configuration.getRegulatoryDomain();
			reg_domain &= 0xFF00;
		
		if(code == Mesh.CUSTOM_COUNTRY_CODE){
		
			/*
			 * dfs bit setting is taken care in
			 * rf editor tab
			 */
			reg_domain |= Mesh.REG_DOMN_CODE_CUSTOM;
			cmbCountryCodeHelper.setCountryCode(code);
			configuration.setCountryCode(Mesh.CUSTOM_COUNTRY_CODE);
			updateCustomCountryCodeInformation();
			configuration.setRegulatoryDomain(reg_domain);
	
		}else{
			
			CountryData cData   = cmbCountryCode.getCountryCodeData(""+code);
			cmbCountryCodeHelper.setCountryCode(code);
			cmbCountryCode.setCountryCodeData(code);
			if(code != configuration.getCountryCode()){
			   configuration.setCountryCode(code);
			   updateCountryCodeInformation();
			}
			int regDomainCode = cData.regDomnFld & 0X00FF;
			reg_domain |= regDomainCode;
			int dfsBit	= cData.regDomnFld & Mesh.DFS_BIT_SET;
			
			reg_domain &= 0XFEFF;// resetting the DFS bit
			
			reg_domain |= dfsBit;// setting the new DFS value
			
			configuration.setRegulatoryDomain(reg_domain);
 		}
		
		return getMobilityDataFromUIToConfiguration();
	}
	
	
	
	/**
	 * 
	 */
	private void getVersionSpecificData() {
		
		if(versionInfo.versionLessThan(IVersionInfo.MAJOR_VERSION_2,
									   IVersionInfo.MINOR_VERSION_5,
									   IVersionInfo.VARIANT_VERSION_1)) {
	
			IMeshConfiguration	  meshConfiguration	= configuration.getMeshConfiguration();
			if(meshConfiguration == null) {
				return;
			}
			meshConfiguration.setHopCost((short)sldHopCost.getValue());
			meshConfiguration.setChangeResistanceThreshold((short)sldChangeRes.getValue());
			
		}
	}
	private void updateCustomCountryCodeInformation() {
		
		IInterfaceConfiguration	interfaceConfiguration	= configuration.getInterfaceConfiguration();
		int 					ifCount					= interfaceConfiguration.getInterfaceCount();
		
		for(int j = 0; j < ifCount; j++) {
			
			IInterfaceInfo	interfaceInfo 	= null;
			interfaceInfo					= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(j);
			if(interfaceInfo == null) {
				return;
			}
			if(interfaceInfo.getMediumType() == Mesh.PHY_TYPE_802_3){ 
				continue;
			}
			InterfaceCustomChannelConfig customConfig	= (InterfaceCustomChannelConfig)interfaceInfo.getCustomChannelConfiguration();
			if(customConfig == null) {
				return;
			}
			try{
				
				int channelInfoCount	= customConfig.getChannelInfoCount();
				short[] dcaList = new short[channelInfoCount];
				int i;
				
				for(i = 0; i < channelInfoCount; i++) {
					
		            IChannelInfo  ch = (IChannelInfo) customConfig.getChannelInfo(i);
					if(ch == null) { 
						continue;
					}
					dcaList[i]	= ch.getChannelNumber();
					
				}
				interfaceInfo.setDcaListCount((short)dcaList.length);
				setInterfaceDCAList(interfaceInfo,dcaList);
				
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}


	private void updateCountryCodeInformation(){
		
		short[] dcaList = new short[0];
	   
		IInterfaceConfiguration	interfaceConfiguration	= configuration.getInterfaceConfiguration();
		if(interfaceConfiguration == null) {
			return;
		}
		int ifCount	=	interfaceConfiguration.getInterfaceCount();
		
		for(int ifNo=0 ; ifNo < ifCount; ifNo++) {
			
			IInterfaceInfo interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(ifNo);
			if(interfaceInfo == null) {
				continue;
			}
			if(interfaceInfo.getMediumType() == Mesh.PHY_TYPE_802_3 
			|| interfaceInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_DS){
				continue;
			}
			Vector<ChannelListInfo> channelList = cmbCountryCode.getChannelList();
			String ifName 						= interfaceInfo.getName();
			short  channelInfoCount				= (short)channelList.size();
			boolean	ifFound						= false;
			   
		    for(int i=0; i<channelInfoCount; i++) {
				
	            ChannelListInfo ch = (ChannelListInfo) channelList.get(i);
				if(ch == null) { 
					continue;
				}
				if(ifName.equalsIgnoreCase(ch.getInterfaceName()) == true) {
				
					short [] channels = ch.getChannels();
					interfaceInfo.setDcaListCount((short)channels.length);
					setInterfaceDCAList(interfaceInfo,channels);
					ifFound = true;
					break;
				}
			}
		    
		    if(ifFound == false) {
		    	setInterfaceDCAList(interfaceInfo,dcaList);
		    }
		    //setting to DCA to Auto 
		    if(cmbCountryCodeHelper.isCountryCodeChanged() == true) {
		    	interfaceInfo.setDynamicChannelAlloc((short)Mesh.ENABLED);
		    }
		}
	}

	/**
	 * @param interfaceInfo
	 * @param channels
	 */
	private void setInterfaceDCAList(IInterfaceInfo interfaceInfo, short[] channels) {
		
		short ifUsageType = interfaceInfo.getUsageType();
		    
		if( (ifUsageType == Mesh.PHY_USAGE_TYPE_DS)) {
				interfaceInfo.setDcaListCount((short) 0);
				return;
		}   
		for(int i = 0; i < channels.length; i++ ) {
			interfaceInfo.setDcaChannel(i,channels[i]);
		}
	}
	
}

