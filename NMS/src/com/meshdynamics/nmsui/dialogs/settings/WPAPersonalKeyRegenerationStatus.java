/*
 * Created on Jul 18, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.dialogs.settings;

import java.util.Enumeration;
import java.util.Vector;

/**
 * @author imran
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class WPAPersonalKeyRegenerationStatus {
	private Vector<String> interfaceStatusVector;
	
	public WPAPersonalKeyRegenerationStatus(){
		interfaceStatusVector	= new Vector<String>();
	}
	public void addEntry(String ifName) {
		if(interfaceStatusVector.contains(ifName) == false) {
			interfaceStatusVector.add(ifName);
		}
	}
	
	public void removeEntry(String ifName){
		interfaceStatusVector.remove(ifName);
	}
	
	public String getInterfaceList() {
		String	strIfList;
		strIfList	= "";
		
		Enumeration<String>	enmIfList	= interfaceStatusVector.elements();
		while(enmIfList.hasMoreElements()) {
			strIfList += (String)enmIfList.nextElement();
			if(enmIfList.hasMoreElements() == true) {
				strIfList	+= ", " ;
			}
		}
		return	strIfList;
	}
	
	public void	clearAll(){
		interfaceStatusVector.clear();
	}
	/**
	 * @return
	 */
	public boolean isEntryPresent() {
		if(interfaceStatusVector.size() > 0) {
			return true;
		}
		return false;
	}
}
