/**
 * MeshDynamics 
 * -------------- 
 * File     : InterfaceConfigPage.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * |  11 |NOV 2,2016     | HT and VHT capabilities added 			  |Venkat        | 
 * ----------------------------------------------------------------------------------
 * | 4  | May 03, 2007   | ViewSupportedChannels is added			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 3  | May 03, 2007   | add/remove channels from supported channels|				 |
 * |	|				 | list is added						  	  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 2  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.dialogs.settings;

import java.util.Iterator;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.configuration.IChannelInfo;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceChannelConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceAdvanceConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceCustomChannelConfig;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.meshviewer.util.ProtocolEntry;
import com.meshdynamics.meshviewer.util.TransmitRateTable;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.PortInfo;
import com.meshdynamics.nmsui.PortModelInfo;
import com.meshdynamics.nmsui.controls.Slider;
import com.meshdynamics.nmsui.dialogs.MeshviewerStringConstants;
import com.meshdynamics.nmsui.dialogs.base.ConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.dialogs.frames.portmodeinfo.IPortModelInfoListener;
import com.meshdynamics.nmsui.dialogs.frames.portmodeinfo.PortModelInfoFrame;
import com.meshdynamics.nmsui.dialogs.frames.portmodeinfo.PortModelInfoSelectionEvent;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.util.MacAddress;

public class InterfaceConfigPage extends ConfigPage implements
		IPortModelInfoListener {

	private Group grp, grpClientConnectivity, grpMode, grpDCA, grpPublicSafety,
			grpDCAInfoFrame;

	private Text txtEssid, txtManualChannel, txtAutoChannel;

	private Button btnHideESSID, btnClientConnectivity, btnClient11b;
	private Button btnClient11g, btnManualChannel, btnAutoChannel;
	private Button btnAddAutoChannel, btnRemoveAutoChannel,
			btnViewSupportedChannelInfo;
	private Button publicSafetyQuarterMode, publicSafetyHalfMode,
			publicSafetyFullMode;

	private Label lblEssid, lblFrag, lblFragThrValue, lblRts, lblAuto;
	private Label lblTransmitRate, lblPowerSetting, lblPowerSettingValue;
	private Label lblAckTimeout, lblAckTimeoutVal, lblClientMode;
	private Label lblRtsThrValue;
	private Label lblBandwidth;
	// new Field
	private Label lblOperationMode;
	private Label lblDcaInfoMode, lblDcaInfoModeName, lblDcaInfoChannel,
			lblDcaInfo, lblDcaInfoChannelNo;

	// new fields
	private Button interButton;

	private Combo cmbTransmitRate, cmbAutoChannels, cmbOperationMode;

	private Slider sldPowerSetting, sldAckTimeout, sldFragThr;
	private Slider sldRtsThr;

	private IConfiguration iConfiguration;
	private PortModelInfoFrame pmInfoFrame;
	private PortModelInfo pmInfo;
	private IVersionInfo versionInfo;

	public InterfaceConfigPage(IMeshConfigBook parent) {

		super(parent);
		minimumBounds.width = 435;
		minimumBounds.height = 500;
		iConfiguration = parent.getConfiguration();
		versionInfo = parent.getVersionInfo();
	}

	/**
	 * 
	 */
	private int createGroup1(int rel_y) {

		int rel_x = 25;

		lblTransmitRate = new Label(grp, SWT.NO);
		lblTransmitRate.setText("Max Transmit Rate");
		lblTransmitRate.setBounds(rel_x, rel_y, 100, 15);

		cmbTransmitRate = new Combo(grp, SWT.READ_ONLY | SWT.BORDER);
		cmbTransmitRate.setBounds(160, rel_y, 196, 15);
		
		// Temporal added this fun because guard interval in progress
		cmbTransmitRate.removeAll();
		cmbTransmitRate.add(MeshviewerStringConstants.TX_RATE_TYPE_AUTO);
		cmbTransmitRate.setData(MeshviewerStringConstants.TX_RATE_TYPE_AUTO,
				"" + 0);
		cmbTransmitRate.setEnabled(false);

		rel_y += 25;

		lblPowerSetting = new Label(grp, SWT.NO);
		lblPowerSetting.setText("Power Level Setting");
		lblPowerSetting.setBounds(rel_x, rel_y, 100, 15);

		sldPowerSetting = new Slider(grp, SWT.FLAT);
		sldPowerSetting.setBounds(158, rel_y + 5, 195, 15);
		sldPowerSetting.setMinValue(0);
		sldPowerSetting.setMaxValue(100);
		sldPowerSetting.addValueListener(new Listener() {

			public void handleEvent(Event arg0) {
				Integer val = (Integer) arg0.data;
				lblPowerSettingValue.setText("" + val.intValue() + " %");
			}
		});

		lblPowerSettingValue = new Label(grp, SWT.NO);
		lblPowerSettingValue.setBounds(355, rel_y, 35, 15);
		lblPowerSettingValue.setText("0 %");

		rel_y += 20;

		lblAckTimeout = new Label(grp, SWT.NONE);
		lblAckTimeout.setText("Ack Timeout");
		lblAckTimeout.setBounds(rel_x, rel_y, 100, 15);

		sldAckTimeout = new Slider(grp, SWT.FLAT);
		sldAckTimeout.setBounds(158, rel_y + 5, 195, 15);
		sldAckTimeout.setChangeValue(10);
		sldAckTimeout.setMinValue(4);
		sldAckTimeout.setMaxValue(200);
		sldAckTimeout.setChangeValue(1);
		sldAckTimeout.addValueListener(new Listener() {

			public void handleEvent(Event arg0) {
				Integer val = (Integer) arg0.data;
				lblAckTimeoutVal.setText("" + val.intValue() * 10
						+ " microsecs");
			}
		});

		lblAckTimeoutVal = new Label(grp, SWT.NONE);
		lblAckTimeoutVal.setBounds(355, rel_y, 60, 15);
		lblAckTimeoutVal.setText("0 microsecs");

		rel_y += 15;
		return rel_y;
	}

	/**
	 * 
	*/
	private int createGroup2(int rel_y) {

		int rel_x = 25;

		lblFrag = new Label(grp, SWT.NO);
		lblFrag.setText("Fragmentation Threshold");
		lblFrag.setBounds(rel_x, rel_y, 125, 15);

		sldFragThr = new Slider(grp, SWT.FLAT);
		sldFragThr.setBounds(158, rel_y + 5, 195, 15);
		sldFragThr.setMinValue(1);
		sldFragThr.setMaxValue(2346);
		sldFragThr.setChangeValue(25);
		sldFragThr.addValueListener(new Listener() {

			public void handleEvent(Event arg0) {
				Integer val = (Integer) arg0.data;
				lblFragThrValue.setText("" + val.intValue() + " bytes");
			}
		});

		lblFragThrValue = new Label(grp, SWT.NO);
		lblFragThrValue.setBounds(355, rel_y, 55, 15);
		lblFragThrValue.setText("0 bytes");

		rel_y += 25;

		lblRts = new Label(grp, SWT.NO);
		lblRts.setText("RTS Threshold");
		lblRts.setBounds(rel_x, rel_y, 90, 15);

		sldRtsThr = new Slider(grp, SWT.FLAT);
		sldRtsThr.setBounds(158, rel_y + 5, 195, 15);
		sldRtsThr.setMinValue(1);
		sldRtsThr.setMaxValue(2347);
		sldRtsThr.setChangeValue(25);
		sldRtsThr.addValueListener(new Listener() {

			public void handleEvent(Event arg0) {
				Integer val = (Integer) arg0.data;
				lblRtsThrValue.setText("" + val.intValue() + " bytes");
			}
		});

		lblRtsThrValue = new Label(grp, SWT.NO);
		lblRtsThrValue.setBounds(355, rel_y, 55, 15);
		lblRtsThrValue.setText("0 bytes");

		rel_y += 30;

		lblEssid = new Label(grp, SWT.NO);
		lblEssid.setText("ESSID");
		lblEssid.setBounds(rel_x, rel_y, 60, 20);

		txtEssid = new Text(grp, SWT.BORDER);
		txtEssid.setBounds(160, rel_y, 125, 20);

		btnHideESSID = new Button(grp, SWT.CHECK);
		btnHideESSID.setBounds(300, rel_y + 2, 100, 15);
		btnHideESSID.setText("Hide ESSID");
		rel_y += 25;
		lblOperationMode = new Label(grp, SWT.NO);
		lblOperationMode.setText("Supported Protocol");
		lblOperationMode.setBounds(rel_x, rel_y, 100, 15);

		cmbOperationMode = new Combo(grp, SWT.READ_ONLY | SWT.BORDER);
		cmbOperationMode.setBounds(160, rel_y, 196, 15);
		cmbOperationMode.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				if (cmbOperationMode.getText().equalsIgnoreCase("802.11n")
						|| cmbOperationMode.getText().equalsIgnoreCase(
								"802.11ac")
						|| cmbOperationMode.getText().equalsIgnoreCase(
								"802.11a/n")
						|| cmbOperationMode.getText().equalsIgnoreCase(
								"802.11a/n/ac")
						|| cmbOperationMode.getText().equalsIgnoreCase(
								"802.11b/g/n")) {

					interButton.setVisible(true);
					String protocolName = cmbOperationMode.getText();
					String selectedIfName = pmInfoFrame
							.getLastSelectedRadioName();
					short ifPhySubType = ProtocolEntry.getSubType(protocolName,
							pmInfoFrame.getPortInfo(selectedIfName).freqOfRadio);
					IInterfaceConfiguration iinterface = iConfiguration
							.getInterfaceConfiguration();
					InterfaceInfo ifInfoObject = (InterfaceInfo) iinterface
							.getInterfaceByName(selectedIfName);
					InterfaceAdvanceConfiguration advanceConfig = ifInfoObject
							.getInterfaceAdvanceConfiguration();
					int result = parent
							.showMessage(
									"Protocol change will cause default configs to be applied for HT or VHT ? ",
									SWT.YES | SWT.NO);
					if (result == SWT.CANCEL) {
						setOperationMode(ifInfoObject.getMediumSubType());
					} else if (result == SWT.NO) {
						setOperationMode(ifInfoObject.getMediumSubType());
					} else if (result == SWT.YES) {
						advanceConfig.clearData();
						if(cmbOperationMode.getText().equalsIgnoreCase(
								"802.11ac")
						|| cmbOperationMode.getText().equalsIgnoreCase(
								"802.11a/n/ac")){
							advanceConfig.setChannelBandwidth((byte)3);

						}
						
						if(cmbOperationMode.getText().equalsIgnoreCase(
								"802.11ac")
						|| cmbOperationMode.getText().equalsIgnoreCase(
								"802.11n")){
							advanceConfig.setTxSTBC((byte)0);
							advanceConfig.setRxSTBC((byte)0);
							advanceConfig.setLDPC((byte)0);

						}
						
						InterfaceAdvanceSettings interfaceAdvanceSettings = new InterfaceAdvanceSettings(
								cmbOperationMode.getText(), iConfiguration,
								selectedIfName);
						interfaceAdvanceSettings
								.setTitle("Interface Advance Settings");
						interfaceAdvanceSettings.show();
					}
					short guardInterval = findGuardInterval(advanceConfig);
					addTransmitRateData_advance(selectedIfName,
							advanceConfig.getChannelBandwidth(), ifPhySubType,
							guardInterval);

				} else {
					interButton.setVisible(false);
					String protocolName = cmbOperationMode.getText();
					String selectedIfName = pmInfoFrame
							.getLastSelectedRadioName();
					short ifPhySubType = ProtocolEntry.getSubType(protocolName,
							pmInfoFrame.getPortInfo(selectedIfName).freqOfRadio);
					addTransmitRateData(selectedIfName, ifPhySubType);
				}
			}
		});
		rel_y += 25;
		interButton = new Button(grp, SWT.PUSH);
		interButton.setText("Check for Advance Settings");
		interButton.setVisible(false);
		interButton.setBounds(rel_x, rel_y, 180, 25);
		interButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				String selectedIfName = pmInfoFrame.getLastSelectedRadioName();
				InterfaceAdvanceSettings interfaceAdvanceSettings = new InterfaceAdvanceSettings(
						cmbOperationMode.getText(), iConfiguration,
						selectedIfName);
				interfaceAdvanceSettings.setTitle("Interface Advance Settings");
				interfaceAdvanceSettings.show();
				IInterfaceConfiguration iinterface = iConfiguration
						.getInterfaceConfiguration();
				InterfaceInfo ifInfoObject = (InterfaceInfo) iinterface
						.getInterfaceByName(selectedIfName);
				InterfaceAdvanceConfiguration advanceConfig = ifInfoObject
						.getInterfaceAdvanceConfiguration();
				short guardInterval = findGuardInterval(advanceConfig);
				addTransmitRateData_advance(selectedIfName,
						advanceConfig.getChannelBandwidth(),
						ifInfoObject.getMediumSubType(), guardInterval);
			}
		});

		rel_y += 25;
		return rel_y;
	}

	/**
	 * 
	 */
	private int createGroup3(int rel_y) {

		grpClientConnectivity = new Group(grp, SWT.NONE);
		grpClientConnectivity.setBounds(5, rel_y - 10, 410, 35);

		int rel_x = 25;

		btnClientConnectivity = new Button(grpClientConnectivity, SWT.CHECK);
		btnClientConnectivity.setBounds(rel_x, 12, 130, 15);
		btnClientConnectivity.setSelection(false);
		btnClientConnectivity.setText("Allow Client Connection");
		rel_x += 170;

		grpMode = new Group(grp, SWT.NONE);
		grpMode.setBounds(5, rel_y + 25, 410, 35);

		lblClientMode = new Label(grpMode, SWT.NO);
		lblClientMode.setBounds(25, 12, 100, 15);
		lblClientMode.setText("Supported Protocol");
		lblClientMode.setVisible(false);

		rel_x += 20;

		btnClient11b = new Button(grpMode, SWT.CHECK);
		btnClient11b.setBounds(rel_x, 12, 40, 15);
		btnClient11b.setText("11B");
		btnClient11b.setVisible(false);
		btnClient11b.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent arg0) {
				if (btnClient11g.getSelection() == false
						&& btnClient11b.getSelection() == false) {

					parent.showMessage("Either 11B or 11G should be selected.",
							SWT.ICON_ERROR);
					btnClient11b.setSelection(true);
					return;
				}
			}
		});

		rel_x += 50;

		btnClient11g = new Button(grpMode, SWT.CHECK);
		btnClient11g.setBounds(rel_x, 12, 40, 15);
		btnClient11g.setText("11G");
		btnClient11g.setVisible(false);
		btnClient11g.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent arg0) {
				if (btnClient11g.getSelection() == false
						&& btnClient11b.getSelection() == false) {

					parent.showMessage("Either 11B or 11G should be selected.",
							SWT.ICON_ERROR);
					btnClient11g.setSelection(true);
					return;
				}

			}
		});

		rel_y += 55;
		return rel_y;
	}

	/**
	 * 
	 */
	private int createGroup5(int rel_y) {

		grpDCA = new Group(grp, SWT.NONE);
		grpDCA.setBounds(5, rel_y + 25, 415, 115);
		grpDCA.setText("Dynamic Channel Management");

		createDcaInfoFrame(rel_y);

		int xOffset = 65;
		int yOffset = 20;

		btnManualChannel = new Button(grpDCA, SWT.RADIO);
		btnManualChannel.setBounds(xOffset, yOffset, 60, 15);
		btnManualChannel.setText("Manual");
		btnManualChannel.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent arg0) {

				if (btnManualChannel.getSelection() == false) {
					txtManualChannel.setEnabled(false);
				} else {
					txtManualChannel.setEnabled(true);
					txtManualChannel.setFocus();
				}
			}
		});

		txtManualChannel = new Text(grpDCA, SWT.BORDER);
		txtManualChannel.setBounds(xOffset + 170, yOffset, 98, 20);

		yOffset += 30;

		lblAuto = new Label(grpDCA, SWT.NO);
		lblAuto.setBounds(30, 50, 40, 15);
		lblAuto.setText("Channel");
		lblAuto.setVisible(false);

		btnAutoChannel = new Button(grpDCA, SWT.RADIO);
		btnAutoChannel.setBounds(xOffset, yOffset, 40, 15);
		btnAutoChannel.setText("Auto");
		btnAutoChannel.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent arg0) {

				if (btnAutoChannel.getSelection() == true) {

					txtAutoChannel.setEnabled(true);
					txtAutoChannel.setFocus();
					btnAddAutoChannel.setEnabled(true);
					btnRemoveAutoChannel.setEnabled(true);
					cmbAutoChannels.setEnabled(true);

					if (cmbAutoChannels.getItemCount() > 0) {
						cmbAutoChannels.select(0);
					}

				} else {

					txtAutoChannel.setEnabled(false);
					btnAddAutoChannel.setEnabled(false);
					btnRemoveAutoChannel.setEnabled(false);
					cmbAutoChannels.setEnabled(false);
				}
			}
		});

		txtAutoChannel = new Text(grpDCA, SWT.BORDER);
		txtAutoChannel.setBounds(xOffset + 50, yOffset, 50, 20);

		btnAddAutoChannel = new Button(grpDCA, SWT.PUSH);
		btnAddAutoChannel.setText(" + ");
		btnAddAutoChannel.setBounds(xOffset + 115, yOffset, 20, 20);
		btnAddAutoChannel.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				validateAddChannel();
			}
		});

		btnRemoveAutoChannel = new Button(grpDCA, SWT.PUSH);
		btnRemoveAutoChannel.setText(" - ");
		btnRemoveAutoChannel.setBounds(xOffset + 140, yOffset, 20, 20);
		btnRemoveAutoChannel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				validateRemoveChannel();
			}
		});

		cmbAutoChannels = new Combo(grpDCA, SWT.BORDER | SWT.SINGLE
				| SWT.V_SCROLL | SWT.READ_ONLY);
		cmbAutoChannels.setBounds(xOffset + 170, yOffset, 99, 20);

		yOffset += 30;

		btnViewSupportedChannelInfo = new Button(grpDCA, SWT.PUSH);
		btnViewSupportedChannelInfo.setText("View Supported Channel Info");
		btnViewSupportedChannelInfo.setBounds(xOffset + 60, yOffset, 150, 20);
		btnViewSupportedChannelInfo
				.addSelectionListener(new SelectionAdapter() {

					public void widgetSelected(SelectionEvent arg0) {
						viewValidSupportedChannelList();
					}
				});
		rel_y += 110;
		return rel_y;
	}

	/**
	 * @param rel_y
	 *            If rf configuration is changed and board is not rebooted user
	 *            is not permitted to modify DCA list. So instead of grpDCA,
	 *            grpDCAInfoFrame is to be shown. Which gives info about dca
	 *            list, mode but its non editable.
	 */
	private void createDcaInfoFrame(int rel_y) {

		grpDCAInfoFrame = new Group(grp, SWT.NONE);
		grpDCAInfoFrame.setBounds(5, rel_y + 25, 415, 115);
		grpDCAInfoFrame.setText("Dynamic Channel Management");
		grpDCAInfoFrame.setVisible(false);

		int xOffset = 25;
		int yOffset = 25;

		lblDcaInfoMode = new Label(grpDCAInfoFrame, SWT.NO);
		lblDcaInfoMode.setBounds(xOffset, yOffset, 40, 15);
		lblDcaInfoMode.setText("Mode");

		lblDcaInfoModeName = new Label(grpDCAInfoFrame, SWT.NO);
		lblDcaInfoModeName.setBounds(xOffset + 120, yOffset, 140, 15);
		lblDcaInfoModeName.setText("-");

		yOffset += 25;

		lblDcaInfoChannel = new Label(grpDCAInfoFrame, SWT.NO);
		lblDcaInfoChannel.setBounds(xOffset, yOffset, 40, 15);
		lblDcaInfoChannel.setText("Channel");

		lblDcaInfoChannelNo = new Label(grpDCAInfoFrame, SWT.NO);
		lblDcaInfoChannelNo.setBounds(xOffset + 120, yOffset, 240, 15);
		lblDcaInfoChannelNo.setText("-");

		yOffset += 25;

		lblDcaInfo = new Label(grpDCAInfoFrame, SWT.NO);
		lblDcaInfo.setBounds(xOffset, yOffset, 375, 30);

	}

	/**
	 * Shows the channel information of supported channels
	 */
	private void viewValidSupportedChannelList() {

		String selectedIfName = pmInfoFrame.getLastSelectedRadioName();
		IInterfaceConfiguration interfaceConfiguration = iConfiguration
				.getInterfaceConfiguration();
		IInterfaceInfo ifInfo = (IInterfaceInfo) interfaceConfiguration
				.getInterfaceByName(selectedIfName);

		if (ifInfo == null) {
			return;
		}
		IInterfaceChannelConfiguration supportConfig = (IInterfaceChannelConfiguration) ifInfo
				.getSupportedChannelConfiguration();

		if (supportConfig == null) {
			parent.showMessage("Channel List not available.",
					SWT.ICON_INFORMATION);
			return;
		}
		IfChannelInfoViewdlg dlg = new IfChannelInfoViewdlg(supportConfig,
				selectedIfName);
		dlg.show();

	}

	/**
	 * 
	 */
	protected void validateRemoveChannel() {

		/*
		 * if(parent.getMode() == ConfigurationDlg.MODE_NODECONFIG &&
		 * parent.getAccessPoint().isCountryCodeChanged() == true){ MessageBox
		 * msg = new MessageBox(new Shell(SWT.OK | SWT.ICON_INFORMATION));
		 * msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." +
		 * NMS_VERSION2.buildminor); msg.setMessage(
		 * "Country Code changed ! Update and Reboot the board before you remove new Channels."
		 * ); msg.open(); txtDCAchannel.setText(""); return;
		 * 
		 * }
		 */

		int index = cmbAutoChannels.getSelectionIndex();
		if (index == -1) {
			return;
		}
		String selectedIfName = pmInfoFrame.getLastSelectedRadioName();
		int typeOfRadio = pmInfoFrame.getPortInfo(selectedIfName).typeOfRadio;

		if (versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,
				IVersionInfo.MINOR_VERSION_5, IVersionInfo.VARIANT_VERSION_39)
				&& cmbAutoChannels.getItemCount() > 0) {
			int i;
			int radioCount = pmInfoFrame.getRadioCount();
			boolean flag = true;
			for (i = 0; i < radioCount; i++) {
				PortInfo portInfo = pmInfoFrame.getPortInfo(i);
				if (portInfo.typeOfRadio == PortModelInfo.TYPESCANNER) {
					flag = false;
				}
			}
			if (cmbAutoChannels.getItemCount() <= 0 && isAdHocMode()
					&& (typeOfRadio == PortModelInfo.TYPEUPLINK)) {
				if (flag) {
					parent.showMessage("Channel List for " + selectedIfName
							+ " cannot be empty in ADHOC Mode.",
							SWT.ICON_INFORMATION);
					return;
				}
			}

			if (cmbAutoChannels.getItemCount() == 1 && isAdHocMode()
					&& (typeOfRadio == PortModelInfo.TYPESCANNER)) {
				parent.showMessage("Channel List for " + selectedIfName
						+ " cannot be empty in ADHOC Mode.",
						SWT.ICON_INFORMATION);
				return;
			}

			if (cmbAutoChannels.getItemCount() == 1
					&& (typeOfRadio != PortModelInfo.TYPESCANNER && typeOfRadio != PortModelInfo.TYPEUPLINK)) {

				int ans = parent.showMessage(
						"DCA list is empty.This interface will be disabled if DCA list is kept empty."
								+ SWT.LF + "" + "Do you want to continue?",
						SWT.ICON_INFORMATION | SWT.YES | SWT.NO);

				if (ans == SWT.NO) {
					return;
				}
			}
			cmbAutoChannels.remove(index);
			cmbAutoChannels.select(0);

		} else {

			if (typeOfRadio == PortModelInfo.TYPESCANNER
					|| typeOfRadio == PortModelInfo.TYPEUPLINK
					|| cmbAutoChannels.getItemCount() > 1) {

				cmbAutoChannels.remove(index);
				cmbAutoChannels.select(0);
			} else {
				parent.showMessage("Channel List cannot be empty.", SWT.OK
						| SWT.ICON_INFORMATION);
			}
		}
	}

	private boolean isAdHocMode() {
		return iConfiguration.getMeshConfiguration().getP3MMode();
	}

	private int createGroup4(int yValue) {

		yValue = yValue - 25;
		grpPublicSafety = new Group(grp, SWT.NONE);
		grpPublicSafety.setBounds(5, yValue, 415, 30);

		lblBandwidth = new Label(grpPublicSafety, SWT.NO);
		lblBandwidth.setText("Bandwidth");
		lblBandwidth.setBounds(25, 10, 60, 15);

		publicSafetyQuarterMode = new Button(grpPublicSafety, SWT.RADIO);
		publicSafetyQuarterMode.setBounds(160, 10, 60, 15);
		publicSafetyQuarterMode.setSelection(false);
		publicSafetyQuarterMode.setText("5 MHz");
		publicSafetyQuarterMode.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent arg0) {
				short ifPhySubType = getPublicSafetyPhyType();
				String selectedIfName = pmInfoFrame.getLastSelectedRadioName();
				addTransmitRateData(selectedIfName, ifPhySubType);
			}
		});

		publicSafetyHalfMode = new Button(grpPublicSafety, SWT.RADIO);
		publicSafetyHalfMode.setBounds(235, 10, 60, 15);
		publicSafetyHalfMode.setText("10 MHz");
		publicSafetyHalfMode.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent arg0) {
				short ifPhySubType = getPublicSafetyPhyType();
				String selectedIfName = pmInfoFrame.getLastSelectedRadioName();
				addTransmitRateData(selectedIfName, ifPhySubType);
			}
		});

		publicSafetyFullMode = new Button(grpPublicSafety, SWT.RADIO);
		publicSafetyFullMode.setBounds(310, 10, 60, 15);
		publicSafetyFullMode.setText("20 MHz");
		publicSafetyFullMode.addSelectionListener(new SelectionAdapter() {

			public void widgetSelected(SelectionEvent arg0) {
				short ifPhySubType = getPublicSafetyPhyType();
				String selectedIfName = pmInfoFrame.getLastSelectedRadioName();
				addTransmitRateData(selectedIfName, ifPhySubType);
			}
		});
		yValue = yValue + 15;
		return yValue;
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see com.meshdynamics.nmsui.dialogs.base.IMeshConfigPage#createControls(org.eclipse.swt.custom.CTabFolder)
	 */
	public void createControls(CTabFolder parent) {

		super.createControls(parent);
		pmInfo = new PortModelInfo(iConfiguration.getHardwareModel(),
				iConfiguration.getDSMacAddress().toString(),
				iConfiguration.getInterfaceConfiguration());
		pmInfoFrame = new PortModelInfoFrame(canvas, SWT.SIMPLE, pmInfo, this);
		Rectangle temp = pmInfoFrame.getMinimumBounds();
		pmInfoFrame.setBounds(10, 10, temp.width, temp.height);

		int rel_y = temp.height + 20;

		grp = new Group(canvas, SWT.NO);
		grp.setBounds(10, rel_y, 425, 425);

		rel_y = 25;
		rel_y = createGroup1(rel_y);
		rel_y = createGroup2(rel_y + 10);
		rel_y = createGroup3(rel_y + 10);
		rel_y = createGroup4(rel_y + 10);
		createGroup5(rel_y);

		ContextHelp.addContextHelpHandlerEx(this.canvas,
				contextHelpEnum.CONFGTABINTERFACE);
		int i;

		for (i = 0; i < pmInfo.getNumOfRadioSlots(); i++) {

			PortInfo pInfo = pmInfo.getPortInfo(i);

			if (pInfo == null) {
				continue;
			}
			if (pInfo.typeOfRadio == PortModelInfo.TYPEUPLINK) {
				showControls(pInfo);
				break;
			}
		}
	}

	/**
	 * Validates channels with supported channel list and then allows to add the
	 * channel
	 */
	protected void validateAddChannel() {

		String strDcaChannel = txtAutoChannel.getText().trim();

		int channel;
		try {
			channel = Short.parseShort(strDcaChannel);
		} catch (Exception e) {
			parent.showMessage("Channel is not valid.", SWT.ICON_ERROR);
			return;
		}

		if (!MeshValidations.isInt(strDcaChannel)) {
			parent.showMessage("Enter numeric value. ", SWT.ICON_ERROR);
			return;
		}
		String selectedIfName = pmInfoFrame.getLastSelectedRadioName();
		PortInfo pmo = pmInfoFrame.getPortInfo(selectedIfName);
		if (pmo == null) {
			parent.showMessage("Channel is not valid.", SWT.ICON_ERROR);
			return;
		}
		
		String[] strDCAList = cmbAutoChannels.getItems();
		for (int i = 0; i < strDCAList.length; i++) {

			int listChannel = Short.parseShort(strDCAList[i].trim());
			if (listChannel == channel || channel == 0) {
				parent.showMessage("Duplicate or Incorrect value.", SWT.OK
						| SWT.ICON_INFORMATION);
				txtAutoChannel.setText("");
				txtAutoChannel.setFocus();
				return;
			}
		}

		// if(parent.getMode() == ConfigurationDlg.MODE_NODECONFIG) {

		/*
		 * if(parent.getAccessPoint().isCountryCodeChanged() == true){
		 * parent.showMessage(
		 * "Country Code is changed. Update and Reboot the board before you add new Channels."
		 * ,SWT.OK | SWT.ICON_INFORMATION); txtAutoChannel.setText(""); return;
		 * }
		 */
		if (pmo.typeOfRadio != PortModelInfo.TYPEUPLINK
				&& pmo.typeOfRadio != PortModelInfo.TYPESCANNER) {
			if (checkInSupportedChannelList(channel, selectedIfName) == false) {
				parent.showMessage(
						"Channel not present in supported channel list.",
						SWT.ICON_ERROR);
				return;
			}
		}
		// }
		
		if (pmo.typeOfRadio == PortModelInfo.TYPESCANNER) {

			boolean flag = false;
			short[] aboveList = {36,40, 44, 48, 52, 56, 60, 64, 149, 153, 157, 161};
			for (short value : aboveList)
				if (channel == value)
					flag = true;
			if (!flag) {
				parent.showMessage(
						"For Scan Channel is not present in supported channel list Select from [36, 40, 44, 48, 52, 56, 60, 64, 149, 153, 157, 161]",
						SWT.ICON_ERROR);
				return;
			}
		}
		if (pmo.typeOfRadio == PortModelInfo.TYPEUPLINK){
			boolean flag = validateUplinkChannel(selectedIfName,channel);
			if(!flag)
				return;
		}
		
		cmbAutoChannels.add(strDcaChannel);
		txtAutoChannel.setText("");
		txtAutoChannel.setFocus();
		cmbAutoChannels.select(0);
	}

	/**
	 * @param channel
	 * @param interfaceName2
	 * @return
	 */
	private boolean checkInSupportedChannelList(int channel, String ifName) {

		IInterfaceConfiguration interfaceConfiguration = iConfiguration
				.getInterfaceConfiguration();
		IInterfaceInfo ifInfo = (IInterfaceInfo) interfaceConfiguration
				.getInterfaceByName(ifName);
		if (ifInfo == null) {
			return false;
		}
		IInterfaceChannelConfiguration supportConfig = (IInterfaceChannelConfiguration) ifInfo
				.getSupportedChannelConfiguration();
		if (supportConfig == null) {
			return false;
		}

		boolean isPresent = false;
		int count = supportConfig.getChannelInfoCount();
		int i;

		for (i = 0; i < count; i++) {

			IChannelInfo channelInfo = (IChannelInfo) supportConfig
					.getChannelInfo(i);
			if (channelInfo == null) {
				continue;
			}
			if (channelInfo.getChannelNumber() == channel) {
				isPresent = true;
				return true;
			}
		}

		if (isPresent == false) {
			txtAutoChannel.setText("");
			return false;
		}
		return false;
	}

	/**
	 * Returns public safety bandwidth value
	 */
	private short getPublicSafetyPhyType() {

		if (publicSafetyFullMode.getSelection() == true) {
			return Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ;
		} else if (publicSafetyHalfMode.getSelection() == true) {
			return Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ;
		} else if (publicSafetyQuarterMode.getSelection() == true) {
			return Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ;
		} else {
			return Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ;
		}
	}

	/**
	 * Adds
	 */
	private void add2GMode(PortInfo pInfo) {

		if (pInfo.freqOfRadio.equalsIgnoreCase(PortModelInfo.FREQ_FIVE)) {
			grpMode.setVisible(false);
			lblClientMode.setVisible(false);
			btnClient11b.setVisible(false);
			btnClient11g.setVisible(false);
			if (pInfo.typeOfRadio == PortModelInfo.TYPESCANNER) {
				lblOperationMode.setVisible(false);
				cmbOperationMode.setVisible(false);
				interButton.setVisible(false);
			} else {
				lblOperationMode.setVisible(true);
				cmbOperationMode.setVisible(true);
				interButton.setVisible(true);
			}
		} else if (pInfo.freqOfRadio.equalsIgnoreCase(PortModelInfo.FREQ_TWO)) {
			if (pInfo.typeOfRadio == PortModelInfo.TYPESCANNER) {
				lblOperationMode.setVisible(false);
				cmbOperationMode.setVisible(false);
				interButton.setVisible(false);
			} else {
				lblOperationMode.setVisible(true);
				cmbOperationMode.setVisible(true);
				interButton.setVisible(true);
			}
			grpMode.setEnabled(false);
			lblClientMode.setEnabled(false);
			btnClient11b.setEnabled(false);
			btnClient11g.setEnabled(false);
		}

	}

	/**
	 * Setting Tx power for power settings slider.
	 */
	private void setPowerSetting(short txPower) {

		if (txPower > sldPowerSetting.getMaxValue()) {
			sldPowerSetting.setValue(sldPowerSetting.getMaxValue());
		} else if (txPower < sldPowerSetting.getMinValue()) {
			sldPowerSetting.setValue(sldPowerSetting.getMinValue());
		} else {
			sldPowerSetting.setValue(txPower);
		}
	}

	/**
	 * Setting Ack timeout for Ack Timeout slider.
	 */
	private void setAckTimeout(short ackTimeout) {

		if (ackTimeout > sldAckTimeout.getMaxValue()) {
			sldAckTimeout.setValue(sldAckTimeout.getMaxValue());
		} else if (ackTimeout < sldAckTimeout.getMinValue()) {
			sldAckTimeout.setValue(sldAckTimeout.getMinValue());
		} else {
			sldAckTimeout.setValue(ackTimeout);
		}
	}

	/**
	 * Setting Fragmentation Threshold value for slider Fragmentation Threshold
	 */
	private void setFragThreshold(long fragThreshold) {

		if (fragThreshold > sldFragThr.getMaxValue()) {
			sldFragThr.setValue(sldFragThr.getMaxValue());
		} else if (fragThreshold < sldFragThr.getMinValue()) {
			sldFragThr.setValue(sldFragThr.getMinValue());
		} else {
			sldFragThr.setValue((int) fragThreshold);
		}
	}

	/**
	 * Setting RTS Threshold value for slider RTS Threshold
	 */
	private void setRTSThreshold(long rtsThreshold) {

		if (rtsThreshold > sldRtsThr.getMaxValue()) {
			sldRtsThr.setValue(sldRtsThr.getMaxValue());
		} else if (rtsThreshold < sldRtsThr.getMinValue()) {
			sldRtsThr.setValue(sldRtsThr.getMinValue());
		} else {
			sldRtsThr.setValue((int) rtsThreshold);
		}
	}

	/**
	 * Disables client Activity controls
	 */
	private void disableClientActivity() {

		lblOperationMode.setVisible(false);
		cmbOperationMode.setVisible(false);
		interButton.setVisible(false);
		grpMode.setVisible(false);
		lblClientMode.setVisible(false);

		btnClient11b.setSelection(false);
		btnClient11g.setSelection(false);
		btnClient11b.setVisible(false);
		btnClient11g.setVisible(false);

	}

	/**
	 * Setting Public Safety values, enabling the public safety controls public
	 * safety modes can be Quarter mode, Half mode, Full mode.
	 */
	private void setPublicSafety(short mediumSubType) {

		if (mediumSubType == Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ) {

			disableClientActivity();
			grpPublicSafety.setVisible(true);
			publicSafetyFullMode.setSelection(true);
			publicSafetyHalfMode.setSelection(false);
			publicSafetyQuarterMode.setSelection(false);

		} else if (mediumSubType == Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ) {

			disableClientActivity();
			grpPublicSafety.setVisible(true);
			publicSafetyFullMode.setSelection(false);
			publicSafetyHalfMode.setSelection(true);
			publicSafetyQuarterMode.setSelection(false);

		} else if (mediumSubType == Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ) {

			disableClientActivity();
			grpPublicSafety.setVisible(true);
			publicSafetyFullMode.setSelection(false);
			publicSafetyHalfMode.setSelection(false);
			publicSafetyQuarterMode.setSelection(true);

		} else {
			grpPublicSafety.setVisible(false);
		}
	}

	/**
	 * Set Client Connectivity UI controls
	 * 
	 * @param serviceType
	 * @param mediumSubtype
	 */

	private void set2GMode(int mediumSubtype, String ifName) {

		if (mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_B) {
			// btnClient11b.setSelection(true);
			// btnClient11g.setSelection(false);
			grpPublicSafety.setVisible(false);
			lblOperationMode.setVisible(true);
			cmbOperationMode.setVisible(true);
			interButton.setVisible(false);

		} else if (mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_G) {
			// btnClient11b.setSelection(false);
			// btnClient11g.setSelection(true);
			grpPublicSafety.setVisible(false);
			lblOperationMode.setVisible(true);
			cmbOperationMode.setVisible(true);
			interButton.setVisible(false);

		} else if (mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_B_G) {
			// btnClient11b.setSelection(true);
			// btnClient11g.setSelection(true);
			grpPublicSafety.setVisible(false);
			lblOperationMode.setVisible(true);
			cmbOperationMode.setVisible(true);
			interButton.setVisible(false);

		} else {
			btnClient11b.setSelection(false);
			btnClient11g.setSelection(false);
		}

	}

	private void setClientConnectivity(int serviceType) {

		if (serviceType == Mesh.IF_SERVICE_BACKHAUL_ONLY) {
			btnClientConnectivity.setSelection(false);
		} else {
			btnClientConnectivity.setSelection(true);
		}
	}

	/**
	 * Adds Transmitrate values in transmit rate combo
	 * 
	 */

	private void addTransmitRateData(String ifName, short ifPhySubType) {

		int countryCode = 0;
		int ifBandwidth = Mesh.CHANNEL_BANDWIDTH_20_MHZ;

		countryCode = iConfiguration.getCountryCode();

		if (countryCode == Mesh.CUSTOM_COUNTRY_CODE) {

			InterfaceInfo ifInfoObject = (InterfaceInfo) pmInfoFrame
					.getRadiodataByName(ifName);

			if (ifInfoObject == null) {
				return;
			}
			InterfaceCustomChannelConfig customConfig = (InterfaceCustomChannelConfig) ifInfoObject
					.getCustomChannelConfiguration();
			ifBandwidth = customConfig.getChannelBandwidth();

		} else {

			if (ifPhySubType == Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ) {
				ifBandwidth = Mesh.CHANNEL_BANDWIDTH_20_MHZ;
			}
			if (ifPhySubType == Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ) {
				ifBandwidth = Mesh.CHANNEL_BANDWIDTH_10_MHZ;
			}
			if (ifPhySubType == Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ) {
				ifBandwidth = Mesh.CHANNEL_BANDWIDTH_5_MHZ;
			}
		}

		String[] strTxRate;
		Vector<String> txRate;

		if (countryCode == Mesh.CUSTOM_COUNTRY_CODE) {

			strTxRate = TransmitRateTable.getCustomRateTableStr(ifPhySubType,
					ifBandwidth);
			txRate = TransmitRateTable.getCustomRateTable(ifPhySubType,
					ifBandwidth);

		} else {

			strTxRate = TransmitRateTable.getDefaultRateTableStr(ifPhySubType,
					ifBandwidth);
			txRate = TransmitRateTable.getDefaultRateTable(ifPhySubType,
					ifBandwidth);
		}

		if (txRate == null || strTxRate == null) {
			return;
		}

		Iterator<String> it = txRate.iterator();
		int count = 0;

		cmbTransmitRate.removeAll();
		cmbTransmitRate.add(MeshviewerStringConstants.TX_RATE_TYPE_AUTO);
		cmbTransmitRate.setData(MeshviewerStringConstants.TX_RATE_TYPE_AUTO,
				"" + 0);

		while (it.hasNext()) {
			cmbTransmitRate.add(strTxRate[count]);
			cmbTransmitRate.setData(strTxRate[count], it.next());
			count++;
		}
		cmbTransmitRate.select(0);
		txRate = null;
	}

	/***
	 * Tx rates
	 * 
	 */
	private void addTransmitRateData_advance(String ifName, short ifBandwidth,
			short ifPhySubType, short guardInterval) {
		String[] strTxRate;
		Vector<String> txRate;
		int countryCode = 0;

		countryCode = iConfiguration.getCountryCode();
		if (countryCode == Mesh.CUSTOM_COUNTRY_CODE) {

			strTxRate = TransmitRateTable.getCustomRateTableStr(ifPhySubType,
					ifBandwidth);
			txRate = TransmitRateTable.getCustomRateTable(ifPhySubType,
					ifBandwidth);

		} else {

			strTxRate = TransmitRateTable.getDefaultRateTableStr_Advance(
					ifPhySubType, ifBandwidth, guardInterval);
			txRate = TransmitRateTable.getDefaultRateTable_AdvanceSettings(
					ifPhySubType, ifBandwidth, guardInterval);
		}

		if (txRate == null || strTxRate == null) {
			return;
		}

		Iterator<String> it = txRate.iterator();
		int count = 0;

		cmbTransmitRate.removeAll();
		cmbTransmitRate.add(MeshviewerStringConstants.TX_RATE_TYPE_AUTO);
		cmbTransmitRate.setData(MeshviewerStringConstants.TX_RATE_TYPE_AUTO,
				"" + 0);

		while (it.hasNext()) {
			cmbTransmitRate.add(strTxRate[count]);
			cmbTransmitRate.setData(strTxRate[count], it.next());
			count++;
		}
		cmbTransmitRate.select(0);
		txRate = null;
	}

	/***
	 * Setting OperationMode Data
	 */
	private void setDataForOperationMode(String ifName, int mediamType) {
		String[] operationMode = null;
		PortInfo info = pmInfo.getPortInfoByName(ifName);
		// if(mediamType == Mesh.PHY_TYPE_802_11_VIRTUAL){
		// operationMode=ProtocolEntry.getProtocolEntry(info.phySubType,ifName);
		// }else{
		String model = pmInfo.gethwModel();
		String subModel = model.substring(model.indexOf("-") + 1);
		char ch = ProtocolEntry.getMaxSupportProtocol(ifName, subModel);
		int ifPhySubType = ProtocolEntry.getSubType_(ch);
		operationMode = ProtocolEntry.getProtocolEntry(ifPhySubType, ifName);
		// }
		if (operationMode != null && operationMode.length > 0) {
			cmbOperationMode.removeAll();
			cmbOperationMode.add(operationMode[0]);
			cmbOperationMode.setData(
					operationMode[0],
					""
							+ ProtocolEntry.getSubType(operationMode[0],
									info.freqOfRadio));
			int count = 1;
			while (operationMode.length > count) {
				cmbOperationMode.add(operationMode[count]);
				cmbOperationMode
						.setData(
								operationMode[count],
								""
										+ ProtocolEntry.getSubType(
												operationMode[count],
												info.freqOfRadio));
				count++;
			}
		}
		cmbOperationMode.select(0);
	}

	/**
	 * Setting the transmit rate value in combo box
	 */
	private void setTrasmitRate(long txRate) {

		if (txRate == 0) {
			cmbTransmitRate.select(0);
		} else {

			boolean found = false;
			int itemCount = cmbTransmitRate.getItemCount();
			int j;

			for (j = 1; j < itemCount; j++) {

				String strTxRate = cmbTransmitRate.getItem(j);
				String data = (String) cmbTransmitRate.getData(strTxRate);
				int listTxRate = Integer.parseInt(data);

				if (listTxRate == txRate) {
					cmbTransmitRate.select(j);
					found = true;
					break;
				}
			}
			if (found == false)
				cmbTransmitRate.select(0);
		}
	}

	/**
	 * set data for Operation Mode
	 */
	private void setOperationMode(int operationMode) {
		if (operationMode == 1 || operationMode == 2) {
			cmbOperationMode.select(0);
		} else {

			boolean found = false;
			int itemCount = cmbOperationMode.getItemCount();
			int j;

			for (j = 1; j < itemCount; j++) {

				String strProtocol = cmbOperationMode.getItem(j);
				String data = (String) cmbOperationMode.getData(strProtocol);
				int listOperationMode = Integer.parseInt(data);
				if (listOperationMode == operationMode) {
					cmbOperationMode.select(j);
					found = true;
					break;
				}
			}
			if (found == false)
				cmbOperationMode.select(0);
		}
	}

	/***
	 * Setting
	 * 
	 * @param dcaEnabled
	 * @param channel
	 */
	private void setDCA(int dcaEnabled, short channel) {

		if (dcaEnabled == 0) {

			btnManualChannel.setSelection(true);
			txtManualChannel.setEnabled(true);
			txtManualChannel.setText("" + channel);
			lblDcaInfoChannelNo.setText("" + channel);
			lblDcaInfoModeName.setText("Manual");

			btnAutoChannel.setSelection(false);
			txtAutoChannel.setEnabled(false);

			btnAddAutoChannel.setEnabled(false);
			btnRemoveAutoChannel.setEnabled(false);

			cmbAutoChannels.setEnabled(false);

		} else {

			btnManualChannel.setSelection(false);
			txtManualChannel.setEnabled(false);
			txtManualChannel.setText("");

			btnAutoChannel.setSelection(true);
			txtAutoChannel.setEnabled(true);

			btnAddAutoChannel.setEnabled(true);
			btnRemoveAutoChannel.setEnabled(true);

			cmbAutoChannels.setEnabled(true);

			String[] dcaListItems = cmbAutoChannels.getItems();
			if (dcaListItems.length > 0) {
				cmbAutoChannels.select(0);
			}

			for (int j = 0; j < dcaListItems.length; j++) {
				short ch = Short.parseShort(dcaListItems[j]);
				if (ch == channel) {
					cmbAutoChannels.select(j);
					break;
				}
			}
		}
	}

	/**
	 * Set interface values in UI for given interface name
	 * 
	 * @param i
	 */
	private void setInterfaceValues(String ifName) {
		if (ifName == null || ifName.equals("")) {
			InterfaceInfo info = (InterfaceInfo) pmInfoFrame
					.getRadioDataByIndex(pmInfoFrame
							.getCurrentSelectedRadioIndex());
			if (info == null) {
				return;
			}
			ifName = info.getName();
		}

		InterfaceInfo ifInfoObject = (InterfaceInfo) pmInfoFrame
				.getRadiodataByName(ifName);

		if (ifInfoObject == null) {
			return;
		}
		MacAddress ifMacAddress = ifInfoObject.getMacAddress();
		String msg = "Settings for (" + ifName + ") - ";

		if (ifMacAddress.equals(MacAddress.resetAddress)) {
			msg = msg + "(Inactive)"; // For roots
		} else {
			msg = msg + ifMacAddress;
		}

		grp.setText(msg);

		PortInfo portInfo = pmInfoFrame.getPortInfo(ifName);
		int typeOfRadio = portInfo.typeOfRadio;

		if (typeOfRadio == PortModelInfo.TYPEUPLINK
				|| typeOfRadio == PortModelInfo.TYPESCANNER) {
			btnAutoChannel.setSelection(true);
			lblDcaInfoModeName.setText("Auto");
			add2GMode(portInfo);
		} else {
			btnClientConnectivity.setEnabled(true);
		}

		cmbAutoChannels.removeAll();
		short[] temp = ifInfoObject.getDcaList();
		int j;
		String channel = " ";
		for (j = 0; j < temp.length; j++) {

			cmbAutoChannels.add("" + temp[j]);
			if (temp.length >= 1) {
				channel = channel + temp[j];
			}
			if (j < (temp.length - 1)) {
				channel = channel + " ,";
			}
		}
		if (temp.length > 0) {
			channel = channel + " ";
		}
		cmbAutoChannels.select(0);
		lblDcaInfoChannelNo.setText(channel);
		if (typeOfRadio != PortModelInfo.TYPESCANNER) {
			set2GMode(ifInfoObject.getMediumSubType(), ifInfoObject.getName());
		}

		if (typeOfRadio == PortModelInfo.TYPESCANNER) {
			return;
		}

		setPowerSetting(ifInfoObject.getTxPower());
		setAckTimeout(ifInfoObject.getAckTimeout());

		short ifPhySubType = ifInfoObject.getMediumSubType();

		setPublicSafety(ifPhySubType);
		/**
		 * If rf configuration is changed and board is not rebooted user is not
		 * permitted to modify bandwidth of public safety
		 */
		if (parent.isRfConfigChanged() == true) {
			grpPublicSafety.setVisible(false);
		}
		if (ifPhySubType == Mesh.PHY_SUB_TYPE_802_11_24GHz_N
				|| ifPhySubType == Mesh.PHY_SUB_TYPE_802_11_5GHz_N
				|| ifPhySubType == Mesh.PHY_SUB_TYPE_802_11_AC
				|| ifPhySubType == Mesh.PHY_SUB_TYPE_802_11_ANAC
				|| ifPhySubType == Mesh.PHY_SUB_TYPE_802_11_BGN
				|| ifPhySubType == Mesh.PHY_SUB_TYPE_802_11_AN) {
			InterfaceAdvanceConfiguration advanceConfig = ifInfoObject
					.getInterfaceAdvanceConfiguration();
			short guardInterval = findGuardInterval(advanceConfig);
			addTransmitRateData_advance(ifName,
					advanceConfig.getChannelBandwidth(), ifPhySubType,
					guardInterval);
		} else {
			addTransmitRateData(portInfo.ifName, ifPhySubType);
		}
		setDataForOperationMode(portInfo.ifName, portInfo.phySubType);
		setTrasmitRate(ifInfoObject.getTxRate());
		setOperationMode(ifInfoObject.getMediumSubType());
		setSupportedProtocol(portInfo.ifName, ifInfoObject.getMediumSubType());
		setFragThreshold(ifInfoObject.getFragThreshold());
		setRTSThreshold(ifInfoObject.getRtsThreshold());

		if (typeOfRadio == PortModelInfo.TYPEUPLINK) {
			return;
		}

		txtEssid.setText(ifInfoObject.getEssid().trim());

		if (ifInfoObject.getEssidHidden() == 0) {
			btnHideESSID.setSelection(false);
		} else {
			btnHideESSID.setSelection(true);
		}

		setClientConnectivity(ifInfoObject.getServiceType());
		setDCA(ifInfoObject.getDynamicChannelAlloc(), ifInfoObject.getChannel());
		
	}

	private short findGuardInterval(InterfaceAdvanceConfiguration advanceConfig) {
		if (advanceConfig.getChannelBandwidth() == 0
				|| advanceConfig.getChannelBandwidth() == 1) {
			return advanceConfig.getGuardInterval_20();
		} else if (advanceConfig.getChannelBandwidth() == 2) {
			return advanceConfig.getGuardInterval_40();
		} else if (advanceConfig.getChannelBandwidth() == 3) {
			return advanceConfig.getGuardInterval_80();
		}
		return 0;
	}

	/*
	 * @
	 */

	private void setSupportedProtocol(String ifName, short phySubType) {
		if (phySubType == Mesh.PHY_SUB_TYPE_802_11_24GHz_N
				|| phySubType == Mesh.PHY_SUB_TYPE_802_11_5GHz_N
				|| phySubType == Mesh.PHY_SUB_TYPE_802_11_AC
				|| phySubType == Mesh.PHY_SUB_TYPE_802_11_BGN
				|| phySubType == Mesh.PHY_SUB_TYPE_802_11_AN
				|| phySubType == Mesh.PHY_SUB_TYPE_802_11_ANAC) {
			interButton.setVisible(true);
		} else
			interButton.setVisible(false);
	}

	/**
	 * Initializes data in UI.
	 */
	public void initalizeLocalData() {

		int i;
		int radioCount = pmInfoFrame.getRadioCount();
		IInterfaceConfiguration interfaceConfig = iConfiguration
				.getInterfaceConfiguration();

		for (i = 0; i < radioCount; i++) {
			PortInfo portInfo = pmInfoFrame.getPortInfo(i);
			if (portInfo.typeOfRadio == PortModelInfo.TYPENOTUSED) {
				continue;
			}
			String ifName = portInfo.ifName;
			IInterfaceInfo interfaceInfo = (IInterfaceInfo) interfaceConfig
					.getInterfaceByName(ifName);
			if (interfaceInfo == null) {
				continue;
			}
			pmInfoFrame.setRadioDataByName(ifName, interfaceInfo);
		}
		setInterfaceValues(pmInfoFrame.getLastSelectedRadioName());
	}

	/**
	 * When tab is selected selection for tab becomes true
	 */
	public void selectionChanged(boolean selected) {

		String selectedIfName = pmInfoFrame.getLastSelectedRadioName();

		if (selected == true) {
			showControls(pmInfoFrame.getPortInfo(selectedIfName));
			if (interButton.getVisible() == true)
				interButton.setVisible(false);
			setInterfaceValues(selectedIfName);
		} else {

			if (validateLocalData() == false) {
				stayOnSameTab();
				return;
			}
			if (compareUIwithLocalConfig() == false) {

				int result = parent.showMessage(
						"Do you want to save the changes ? ", SWT.YES
								| SWT.CANCEL | SWT.NO);
				if (result == SWT.CANCEL) {
					stayOnSameTab();
					return;
				} else if (result == SWT.NO) {
					setInterfaceValues(selectedIfName);
				} else if (result == SWT.YES) {
					getDataFromUIToConfiguration();
				}
			}
		}
	}

	/**
	 * Action to be performed when radio button on PortModelFrame is selected
	 * Validate, Compare, Save
	 */
	public void radioSelected(PortModelInfoSelectionEvent event) {

		PortInfo currentPortInfo = event.getRadioInfo();

		int lastSelectedIndex = pmInfoFrame.getLastSelectedRadioIndex();

		if (validateLocalData() == false) {

			pmInfoFrame.setRadioSelection(lastSelectedIndex, true);
			pmInfoFrame.setRadioSelection(event.getRadioIndex(), false);
			return;
		}

		if (compareUIwithLocalConfig() == false) {

			int result = parent.showMessage(
					"Do you want to save the changes ? ", SWT.YES | SWT.CANCEL
							| SWT.NO);

			if (result == SWT.CANCEL) {

				pmInfoFrame.setRadioSelection(lastSelectedIndex, true);
				pmInfoFrame.setRadioSelection(event.getRadioIndex(), false);
				PortInfo lastPInfo = pmInfoFrame.getPortInfo(lastSelectedIndex);
				showControls(lastPInfo);
				return;

			} else if (result == SWT.NO) {

				showControls(currentPortInfo);
				setInterfaceValues(currentPortInfo.ifName);
//				return;

			} else if (result == SWT.YES) {

				getDataFromUIToConfiguration();
			}
		}
		showControls(currentPortInfo);
		setInterfaceValues(currentPortInfo.ifName);
		
		/*this is checking the virtual interface by commented by nagaraju from start here*/
		IInterfaceInfo info = parent.getInterfaceInfoByName(currentPortInfo.ifName);
		
		if (info.getMediumType() == Mesh.PHY_TYPE_802_11_VIRTUAL
				&& currentPortInfo.typeOfRadio == PortModelInfo.TYPEDOWNLINK) {
			if (findVirtualInterface() == true
					&& pmInfoFrame.getPortInfo(currentPortInfo.ifName).freqOfRadio
							.equals("5G")) {
				setEnableDisable(false);
			}else{
				setEnableDisable(true);
			}
		}else{
			setEnableDisable(true);
		}
		if (info.getMediumType() != Mesh.PHY_TYPE_802_11_VIRTUAL
				&& currentPortInfo.typeOfRadio == PortModelInfo.TYPEDOWNLINK) {
			if (findVirtualInterface() == true
					&& pmInfoFrame.getPortInfo(currentPortInfo.ifName).freqOfRadio
							.equals("5G")) {
				grpClientConnectivity.setEnabled(false);
			}else{
				grpClientConnectivity.setEnabled(true);
			}
		}else{
			grpClientConnectivity.setEnabled(true);
		}
		/*this is checking the virtual interface by commented by nagaraju from end here*/

		
	}

	/**
	 * Sets the visibility of UI controls.
	 */
	private void showHideAllControls(boolean flag) {

		lblEssid.setVisible(flag);
		txtEssid.setVisible(flag);
		btnHideESSID.setVisible(flag);

		lblTransmitRate.setVisible(flag);
		cmbTransmitRate.setVisible(flag);

		lblPowerSetting.setVisible(flag);
		sldPowerSetting.setVisible(flag);
		lblPowerSettingValue.setVisible(flag);

		lblAckTimeout.setVisible(flag);
		sldAckTimeout.setVisible(flag);
		lblAckTimeoutVal.setVisible(flag);
		// new field
		cmbOperationMode.setVisible(flag);
		lblOperationMode.setVisible(flag);

		grpClientConnectivity.setVisible(flag);
		grpMode.setVisible(false);

		lblFrag.setVisible(flag);
		sldFragThr.setVisible(flag);
		lblFragThrValue.setVisible(flag);

		lblRts.setVisible(flag);
		sldRtsThr.setVisible(flag);
		lblRtsThrValue.setVisible(flag);

		btnAutoChannel.setVisible(flag);
		lblAuto.setVisible(flag);
		txtAutoChannel.setVisible(flag);
		btnAddAutoChannel.setVisible(flag);
		btnRemoveAutoChannel.setVisible(flag);
		cmbAutoChannels.setVisible(flag);

		btnManualChannel.setVisible(flag);
		txtManualChannel.setVisible(flag);
		grpDCA.setVisible(flag);
		btnViewSupportedChannelInfo.setVisible(true);

	}

	/**
	 * Changes name of the grpDCA
	 */
	private void renameScanChannelList(int typeOfRadio) {

		if (typeOfRadio == PortModelInfo.TYPEUPLINK
				|| typeOfRadio == PortModelInfo.TYPESCANNER) {
			grpDCA.setText("Scan Channel List");
			grpDCAInfoFrame.setText("Scan Channel List");
			String str = "Scan Channel list will be accessible after reboot. ";
			lblDcaInfo.setText(str);
			lblDcaInfoMode.setVisible(false);
			lblDcaInfoModeName.setVisible(false);
		} else {
			grpDCA.setText("Dynamic Channel Management");
			grpDCAInfoFrame.setText("Dynamic Channel Management");
			String str = "DCA List will be accessible after reboot. ";
			lblDcaInfo.setText(str);
			lblDcaInfoMode.setVisible(true);
			lblDcaInfoModeName.setVisible(true);
		}
	}

	/**
	 * Shows uplink controls
	 */
	private void showControlsForUplink() {

		boolean flag = true;
		lblTransmitRate.setVisible(flag);
		cmbTransmitRate.setVisible(flag);
		cmbTransmitRate.setEnabled(flag);

		lblPowerSetting.setVisible(flag);
		sldPowerSetting.setVisible(flag);
		sldPowerSetting.setEnabled(flag);
		lblPowerSettingValue.setVisible(flag);

		lblAckTimeout.setVisible(flag);
		sldAckTimeout.setVisible(flag);
		sldAckTimeout.setEnabled(flag);
		lblAckTimeoutVal.setVisible(flag);
		// new field
		lblOperationMode.setVisible(flag);
		lblOperationMode.setEnabled(flag);
		cmbOperationMode.setVisible(flag);
		;
		cmbOperationMode.setEnabled(flag);

		grpMode.setVisible(true);
		lblAuto.setVisible(flag);
		txtAutoChannel.setVisible(true);
		btnAddAutoChannel.setVisible(true);
		btnRemoveAutoChannel.setVisible(true);
		cmbAutoChannels.setVisible(true);
		grpDCA.setVisible(flag);

		lblFrag.setVisible(flag);
		sldFragThr.setVisible(flag);
		lblFragThrValue.setVisible(flag);

		lblRts.setVisible(flag);
		sldRtsThr.setVisible(flag);
		lblRtsThrValue.setVisible(flag);

		lblAuto.setEnabled(true);
		txtAutoChannel.setEnabled(true);
		btnAddAutoChannel.setEnabled(true);
		btnRemoveAutoChannel.setEnabled(true);
		cmbAutoChannels.setEnabled(true);
		grpDCA.setEnabled(flag);
		btnViewSupportedChannelInfo.setVisible(false);

	}

	/**
	 * Shows Scanner controls
	 */
	private void showControlsForScanner() {

		grpDCA.setVisible(true);
		lblAuto.setVisible(true);
		txtAutoChannel.setVisible(true);
		btnAddAutoChannel.setVisible(true);
		btnRemoveAutoChannel.setVisible(true);
		cmbAutoChannels.setVisible(true);
		btnViewSupportedChannelInfo.setVisible(false);
		grpMode.setVisible(true);
		lblAuto.setEnabled(true);
		txtAutoChannel.setEnabled(true);
		btnAddAutoChannel.setEnabled(true);
		btnRemoveAutoChannel.setEnabled(true);
		cmbAutoChannels.setEnabled(true);
		lblOperationMode.setVisible(false);
		interButton.setVisible(false);
		cmbOperationMode.setVisible(false);
		grpPublicSafety.setVisible(false);
	}

	/**
	 * If rf configuration is changed and board is not rebooted user is not
	 * permitted to modify DCA list. So instead of grpDCA, grpDCAInfoFrame is
	 * shown.
	 */
	private void showAppropriateDCA() {

		if (parent.isRfConfigChanged() == true
				|| parent.isCountryCodeChanged() == true) {
			grpDCAInfoFrame.setVisible(true);
			grpDCA.setVisible(false);

		} else {
			grpDCAInfoFrame.setVisible(false);
			grpDCA.setVisible(true);
		}
	}

	private void showHide2GMode(PortInfo pInfo) {

		grpMode.setVisible(false);
		btnClientConnectivity.setVisible(false);
		btnClient11b.setVisible(false);
		btnClient11g.setVisible(false);
		lblClientMode.setVisible(false);
		if (pInfo.freqOfRadio.equalsIgnoreCase(PortModelInfo.FREQ_TWO) == false
				|| pInfo.typeOfRadio == PortModelInfo.TYPESCANNER) {
			return;
		}
		lblOperationMode.setVisible(true);
		cmbOperationMode.setVisible(true);
		interButton.setVisible(false);
		grpMode.setVisible(false);
		lblClientMode.setVisible(false);
		btnClient11b.setVisible(false);
		btnClient11g.setVisible(false);

	}

	private void showHideClientConnectivity(PortInfo pInfo) {

		grpClientConnectivity.setVisible(false);

		if (pInfo.typeOfRadio != PortModelInfo.TYPEDOWNLINK
				&& pInfo.typeOfRadio != PortModelInfo.TYPESECONDDOWNLINK
				&& pInfo.typeOfRadio != PortModelInfo.TYPECLIENTAP) {
			return;
		}

		grpClientConnectivity.setVisible(true);
		btnClientConnectivity.setVisible(true);

	}

	/**
	 * Shows UI controls depending on the type of radio.
	 */
	private void showControls(PortInfo pInfo) {

		showHideAllControls(false);

		switch (pInfo.typeOfRadio) {

		case PortModelInfo.TYPEUPLINK:
			showControlsForUplink();
			showAppropriateDCA();
			break;

		case PortModelInfo.TYPESCANNER:
			showControlsForScanner();
			showAppropriateDCA();
			break;

		case PortModelInfo.TYPEDOWNLINK:
			showHideAllControls(true);
			showAppropriateDCA();
			lblAuto.setVisible(false);
			break;

		case PortModelInfo.TYPECLIENTAP:
			showHideAllControls(true);
			showAppropriateDCA();
			lblAuto.setVisible(false);
			break;

		case PortModelInfo.TYPESECONDDOWNLINK:
			showHideAllControls(true);
			showAppropriateDCA();
			lblAuto.setVisible(false);
			break;
		case PortModelInfo.TYPEREGULARAP:
			showHideAllControls(true);
			showAppropriateDCA();
			lblAuto.setVisible(false);
			break;
		case PortModelInfo.TYPE900MHZ:
			showControlsForUplink();
			showAppropriateDCA();
			break;

		default:
			showHideAllControls(true);
			showAppropriateDCA();
			lblAuto.setVisible(false);
			break;
		}
		showHide2GMode(pInfo);
		showHideClientConnectivity(pInfo);
		renameScanChannelList(pInfo.typeOfRadio);
	}

	/**
	 * 
	 */
	private boolean compareUIwithLocalConfig() {

		int typeOfRadio = -1;
		IInterfaceInfo interfaceInfo = null;
		String selectedIfName = "";

		selectedIfName = pmInfoFrame.getLastSelectedRadioName();
		typeOfRadio = pmInfoFrame.getPortInfo(selectedIfName).typeOfRadio;
		interfaceInfo = (IInterfaceInfo) pmInfoFrame
				.getRadiodataByName(selectedIfName);

		switch (typeOfRadio) {

		case PortModelInfo.TYPEUPLINK:
			return compareUIDataForUplink(interfaceInfo);

		case PortModelInfo.TYPESCANNER:
			return compareUIDataForScanner(interfaceInfo);

		case PortModelInfo.TYPEDOWNLINK:
			return compareUIDataForDownlinkClientAP(interfaceInfo);

		case PortModelInfo.TYPECLIENTAP:
			return compareUIDataForDownlinkClientAP(interfaceInfo);

		case PortModelInfo.TYPESECONDDOWNLINK:
			return compareUIDataForDownlinkClientAP(interfaceInfo);

		default:
			return false;

		}
	}

	/**
	 * @param interfaceInfo
	 */
	private boolean compareUIDataForDownlinkClientAP(
			IInterfaceInfo interfaceInfo) {

		try {
			String strTxRate = cmbTransmitRate.getItem(cmbTransmitRate
					.getSelectionIndex());
			String txRate = (String) cmbTransmitRate.getData(strTxRate);
			long trasmitRate = Long.parseLong(txRate);
			
			if (trasmitRate != interfaceInfo.getTxRate()) {
				return false;
			}
//			if (selectedChannel != interfaceInfo.getChannel()) {
//				return false;
//			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (interfaceInfo.getTxPower() != ((short) sldPowerSetting.getValue())) {
			return false;
		}
		if (interfaceInfo.getAckTimeout() != ((short) sldAckTimeout.getValue())) {
			return false;
		}
		if (interfaceInfo.getFragThreshold() != ((long) sldFragThr.getValue())) {
			return false;
		}
		if (interfaceInfo.getRtsThreshold() != ((long) sldRtsThr.getValue())) {
			return false;
		}
		if (!interfaceInfo.getEssid().equals((txtEssid.getText().trim()))) {
			return false;
		}

		short hiddenEssid = 0;
		if (btnHideESSID.getSelection() == true) {
			hiddenEssid = 1;
		}
		if (interfaceInfo.getEssidHidden() != hiddenEssid) {
			return false;
		}

		short serviceType = 1;
		if (btnClientConnectivity.getSelection() == true) {
			serviceType = 0;
		}

		if (interfaceInfo.getServiceType() != serviceType) {
			return false;
		}

		short mediumSubtype = 0;
		mediumSubtype = compareMediumSubType(interfaceInfo);
		if (grpPublicSafety.getVisible() == true) {
			if (publicSafetyQuarterMode.getSelection() == true) {
				mediumSubtype = (short) Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ;
			} else if (publicSafetyHalfMode.getSelection() == true) {
				mediumSubtype = (short) Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ;
			} else if (publicSafetyFullMode.getSelection() == true) {
				mediumSubtype = (short) Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ;
			}
		}

		if (interfaceInfo.getMediumSubType() != mediumSubtype) {
			return false;
		}
		short dynamicChAlloc = 0;

		if (btnAutoChannel.getSelection() == true) {
			dynamicChAlloc = Mesh.ENABLED;
		} else {
			dynamicChAlloc = Mesh.DISABLED;
		}

		if (interfaceInfo.getDynamicChannelAlloc() != dynamicChAlloc) {
			return false;
		}
		if (btnAutoChannel.getSelection() == true) {
			if (compareChannelList(interfaceInfo) == false) {
				return false;
			}
		} else {
			try {
				short channel = Short.parseShort(txtManualChannel.getText()
						.trim());
				if (interfaceInfo.getChannel() != channel) {
					return false;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return true;
	}

	/**
	 * @param interfaceInfo
	 */
	private boolean compareChannelList(IInterfaceInfo interfaceInfo) {

		int i = 0;
		int j = 0;
		int configChangedFlag = 0;

		if (interfaceInfo.getDcaListCount() != ((short) cmbAutoChannels
				.getItemCount())) {
			parent.notifyDCAListChanged(interfaceInfo.getName());
			return false;
		}

		for (i = 0; i < cmbAutoChannels.getItemCount(); i++) {

			String strChannel = cmbAutoChannels.getItem(i);
			try {
				short channel = Short.parseShort(strChannel.trim());
				for (j = 0; j < interfaceInfo.getDcaListCount(); j++) {
					if (channel == interfaceInfo.getDcaChannel(i)) {
						configChangedFlag++;
						break;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			parent.notifyDCAListRemove(interfaceInfo.getName());
		}
		if (configChangedFlag != interfaceInfo.getDcaListCount()) {
			parent.notifyDCAListChanged(interfaceInfo.getName());
			return false;
		}
		return true;
	}

	/**
	 * @param interfaceInfo
	 */
	private boolean compareUIDataForScanner(IInterfaceInfo interfaceInfo) {
		return compareChannelList(interfaceInfo);
	}

	/**
	 * @param interfaceInfo
	 */
	private boolean compareUIDataForUplink(IInterfaceInfo interfaceInfo) {

		try {
			String strTxRate = cmbTransmitRate.getItem(cmbTransmitRate
					.getSelectionIndex());
			String txRate = (String) cmbTransmitRate.getData(strTxRate);
			long trasmitRate = Long.parseLong(txRate);

			if (trasmitRate != interfaceInfo.getTxRate()) {
				return false;
			}
			if (pmInfoFrame.getRadioCount() == Mesh.radioCount_6000) {
				short mediumSubType = compareMediumSubType(interfaceInfo);
				if (mediumSubType != interfaceInfo.getMediumSubType()) {
					return false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (interfaceInfo.getTxPower() != ((short) sldPowerSetting.getValue())) {
			return false;
		}
		if (interfaceInfo.getAckTimeout() != ((short) sldAckTimeout.getValue())) {
			return false;
		}
		if (interfaceInfo.getFragThreshold() != ((long) sldFragThr.getValue())) {
			return false;
		}
		if (interfaceInfo.getRtsThreshold() != ((long) sldRtsThr.getValue())) {
			return false;
		}
		short mediumSubtype = interfaceInfo.getMediumSubType();
		if (grpPublicSafety.getVisible() == true) {

			if (publicSafetyQuarterMode.getSelection() == true) {
				mediumSubtype = (short) Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ;
			} else if (publicSafetyHalfMode.getSelection() == true) {
				mediumSubtype = (short) Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ;
			} else if (publicSafetyFullMode.getSelection() == true) {
				mediumSubtype = (short) Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ;
			}
		}

		if (interfaceInfo.getMediumSubType() != mediumSubtype) {
			return false;
		}
		/**
		 * Scan channel list to be done
		 */
		return compareChannelList(interfaceInfo);

	}

	/*
	 * 
	 */
	private boolean validateValuesForScanner(String ifName) {

		IInterfaceInfo ifInfo = (IInterfaceInfo) pmInfoFrame
				.getRadiodataByName(ifName);

		if (ifInfo == null) {
			return false;
		}

		/*
		 * if(parent.isCountryCodeChanged() == true) { //if country code is
		 * changed then channelinfo list is for previous country //hence we
		 * cannot check new dca list against old channelinfo list return true; }
		 */

		int i;
		int radioCount = pmInfoFrame.getRadioCount();
		boolean flag = true;
		for (i = 0; i < radioCount; i++) {
			PortInfo portInfo = pmInfoFrame.getPortInfo(i);
			if (portInfo.typeOfRadio == PortModelInfo.TYPESCANNER) {
				flag = false;
			}
		}
		int typeOfRadio = pmInfoFrame.getPortInfo(ifName).typeOfRadio;
		if (cmbAutoChannels.getItemCount() <= 0 && isAdHocMode()
				&& (typeOfRadio == PortModelInfo.TYPEUPLINK)) {
			if (flag) {
				parent.showMessage("Channel List for " + ifInfo.getName()
						+ " cannot be empty in ADHOC Mode.",
						SWT.ICON_INFORMATION);
				return false;
			}
		}

		return true;
	}

	/**
	 * Validates the local data
	 */
	public boolean validateLocalData() {

		String selectedRadio = pmInfoFrame.getLastSelectedRadioName();
		IInterfaceInfo interfaceInfo = (IInterfaceInfo) pmInfoFrame
				.getRadiodataByName(selectedRadio);

		if (interfaceInfo == null) {
			return false;
		}

		if (interfaceInfo.getMediumType() != Mesh.PHY_TYPE_802_11 && interfaceInfo.getMediumType() != Mesh.PHY_TYPE_802_11_VIRTUAL) {
			return true;
		}

		short ifUsageType = interfaceInfo.getUsageType();

		if ((ifUsageType == Mesh.PHY_USAGE_TYPE_AMON)
				|| (ifUsageType == Mesh.PHY_USAGE_TYPE_PMON)) {
			return validateValuesForScanner(selectedRadio);
		}

		if (ifUsageType == Mesh.PHY_USAGE_TYPE_DS) {
			return validateValuesForScanner(selectedRadio);
		}

		if (txtEssid.getText().trim().equalsIgnoreCase("")) {
			txtEssid.setFocus();
			parent.showMessage("Essid incorrect. ", SWT.ICON_ERROR);
			return false;
		}
		String essid = txtEssid.getText();
		if (essid.length() > 32) {
			txtEssid.setFocus();
			parent.showMessage(
					"Essid Length should be less then 32 characters. ",
					SWT.ICON_ERROR);
			return false;
		}

		return validateDCAChannel();
	}

	/**
	 * Validates DCA Channel value
	 */
	private boolean validateDCAChannel() {

		short channel;
		String channelstr = "";
		String selectedIfName = pmInfoFrame.getLastSelectedRadioName();

		if (btnManualChannel.getSelection() == true) {

			channelstr = txtManualChannel.getText().trim();

			if (channelstr.equalsIgnoreCase("")) {
				parent.showMessage("Please enter manual channel. ",
						SWT.ICON_ERROR);
				return false;
			}
			if (MeshValidations.isInt(channelstr) == false) {
				parent.showMessage("Channel should be numeric.", SWT.ICON_ERROR);
				return false;
			}
			try {
				channel = Short.parseShort(channelstr);
			} catch (NumberFormatException nfe) {
				parent.showMessage("Manual channel incorrect for ",
						SWT.ICON_ERROR);
				return false;
			}
			IInterfaceInfo interfaceInfo = (IInterfaceInfo) pmInfoFrame
					.getRadiodataByName(selectedIfName);

			if (interfaceInfo == null) {
				return false;
			}
			PortInfo pmo = pmInfoFrame.getPortInfo(selectedIfName);
			if (pmo == null) {
				parent.showMessage("Channel is not valid.", SWT.ICON_ERROR);
				return false;
			}
			if (pmo.typeOfRadio != PortModelInfo.TYPEUPLINK
					&& pmo.typeOfRadio != PortModelInfo.TYPESCANNER) {
				/*if (checkInSupportedChannelList(channel, selectedIfName) == false) {
					 parent.showMessage("Manual Channel is not present in supported channel list.",SWT.ICON_ERROR);
					txtManualChannel.setFocus();
					return false;
				}*/
				
				/*boolean flag = false;
				short[] aboveList = Mesh._manual_ch;
				for (short value : aboveList)
					if (channel == value)
						flag = true;
				if (!flag) {
					parent.showMessage(
							"For Manual Channel is not present in supported channel list Select from [1, 6, 11, 36, 40, 44, 48, 52, 56, 60, 64, 149, 153, 157, 161]",
							SWT.ICON_ERROR);
					return false;
				}*/
			}
			if (pmo.typeOfRadio == PortModelInfo.TYPEDOWNLINK) {
				InterfaceAdvanceConfiguration advanceInfo = (InterfaceAdvanceConfiguration) interfaceInfo
						.getInterfaceAdvanceConfiguration();
				int channelBandwidth = advanceInfo.getChannelBandwidth();
				int mediumSubtype = 0;
				try {
					String strProtocol = cmbOperationMode
							.getItem(cmbOperationMode.getSelectionIndex());
					String protocol = (String) cmbOperationMode
							.getData(strProtocol);
					mediumSubtype = Short.parseShort(protocol);

				} catch (Exception e) {
					e.printStackTrace();
				}
				if(mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_A){
					boolean flag = false;
					short[] aboveList = Mesh._manual_ch;
					for (short value : aboveList)
						if (channel == value)
							flag = true;
					if (!flag) {
						parent.showMessage(
								"For Manual Channel is not present in supported channel list Select from [36, 40, 44, 48, 52, 56, 60, 64, 149, 153, 157, 161]",
								SWT.ICON_ERROR);
						return false;
					}
				}
				if(mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_B || mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_B_G){
					boolean flag = false;
					short[] aboveList = Mesh._manual_ch_24;
					for (short value : aboveList)
						if (channel == value)
							flag = true;
					if (!flag) {
						parent.showMessage(
								"For Manual Channel is not present in supported channel list Select from [1,6,11]",
								SWT.ICON_ERROR);
						return false;
					}
				}
				if (mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_24GHz_N
						|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_BGN) {
					
					boolean flag = false;
					short[] aboveList = Mesh._manual_ch_24;
					for (short value : aboveList)
						if (channel == value)
							flag = true;
					if (!flag) {
						parent.showMessage(
								"For Manual Channel is not present in supported channel list Select from [1,6,11]",
								SWT.ICON_ERROR);
						return false;
					}
					
					if (channelBandwidth == Mesh.channelList_40_Above) {
						parent.showMessage(
								"Channel should not support Below 40 MHz.Please select >=6",
								SWT.ICON_ERROR);
						txtManualChannel.setFocus();
						return false;
					}
					if (channelBandwidth == Mesh.channelList_40_Below) {
						parent.showMessage(
								"Channel should not support Above 40 MHz.Please select <=6",
								SWT.ICON_ERROR);
						txtManualChannel.setFocus();
						return false;
					}

				}
				if (mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_5GHz_N
						|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_AC
						|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_ANAC
						|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_AN) {
					boolean flag = false;
					if (channelBandwidth == Mesh.channelList_40_Above) {
						short[] aboveList = Mesh._40plus_ch;
						for (short value : aboveList)
							if (channel == value)
								flag = true;
						if (!flag) {
							parent.showMessage(
									"For Channel Bandwidth Above 40 MHz Select from [36, 44, 149, 157]",
									SWT.ICON_ERROR);
							return false;
						}
						return true;
					}
					if (channelBandwidth == Mesh.channelList_40_Below) {
						short[] belowList = Mesh._40minus_ch;
						for (short value : belowList)
							if (channel == value)
								flag = true;
						if (!flag) {
							parent.showMessage(
									"For Channel Bandwidth Below 40 MHz Select from [40, 48, 153,161]",
									SWT.ICON_ERROR);
							return false;
						}
						return true;
					}
					if (channelBandwidth == Mesh.channel_80MHz) {
						
						short[] belowList = Mesh._80_ch;
						for (short ch : belowList)
							if (channel == ch)
								flag = true;
						if (!flag) {
							parent.showMessage(
									"80 MHz Configure valid channels from this list [48,161]",
									SWT.ICON_ERROR);
							return false;
						}
						
						/*short dcaChannel = interfaceInfo.getDcaChannel(0);
						int value = validateChannel_80MHz(dcaChannel);
						if (value == 1) {
							short[] belowList = Mesh._40plus_ch;
							for (short ch : belowList)
								if (channel == ch)
									flag = true;
							if (!flag) {
								parent.showMessage(
										"Configure valid channels from this list [36, 44,149, 157]",
										SWT.ICON_ERROR);
								return false;
							}
						} else if (value == -1) {
							short[] belowList = Mesh._40minus_ch;
							for (short ch : belowList)
								if (channel == ch)
									flag = true;
							if (!flag) {
								parent.showMessage(
										"Configure valid channels from this list [40, 48, 153,161]",
										SWT.ICON_ERROR);
								return false;
							}
						}
						if (channel == 165) {
							parent.showMessage(
									"For Bandwidth 80 MHz channel 165 shoud not support",
									SWT.ICON_ERROR);
							return false;
						}*/
						return true;
					}
				}
				return true;
			}
			// channel validation w.r.t up link
			if (pmo.typeOfRadio == PortModelInfo.TYPEUPLINK) {
				InterfaceAdvanceConfiguration advanceInfo = (InterfaceAdvanceConfiguration) interfaceInfo
						.getInterfaceAdvanceConfiguration();
				int channelBandwidth = advanceInfo.getChannelBandwidth();
				int mediumSubtype = 0;
				try {
					String strProtocol = cmbOperationMode
							.getItem(cmbOperationMode.getSelectionIndex());
					String protocol = (String) cmbOperationMode
							.getData(strProtocol);
					mediumSubtype = Short.parseShort(protocol);

				} catch (Exception e) {
					e.printStackTrace();
				}
				if (mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_5GHz_N
						|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_AC
						|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_ANAC
						|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_AN) {
					boolean flag = false;
					if (channelBandwidth == Mesh.channelList_40_Above) {
						short[] aboveList = Mesh._40plus_ch;
						for (short value : aboveList)
							if (channel == value)
								flag = true;
						if (!flag) {
							parent.showMessage(
									"For Channel Bandwidth Above 40 MHz Select from [36, 44, 149, 157]",
									SWT.ICON_ERROR);
							return false;
						}
						return true;
					}
					if (channelBandwidth == Mesh.channelList_40_Below) {
						short[] belowList = Mesh._40minus_ch;
						for (short value : belowList)
							if (channel == value)
								flag = true;
						if (!flag) {
							parent.showMessage(
									"For Channel Bandwidth Below 40 MHz Select from [40, 48, 153,161]",
									SWT.ICON_ERROR);
							return false;
						}
						return true;
					}
					if (channelBandwidth == Mesh.channel_80MHz) {
						short[] belowList = Mesh._80_ch;
						for (short ch : belowList)
							if (channel == ch)
								flag = true;
						if (!flag) {
							parent.showMessage(
									"80 MHz Configure valid channels from this list [48,161]",
									SWT.ICON_ERROR);
							return false;
						}
						/*short dcaChannel = interfaceInfo.getDcaChannel(0);
						int value = validateChannel_80MHz(dcaChannel);
						if (value == 1) {
							short[] belowList = Mesh._40plus_ch;
							for (short ch : belowList)
								if (channel == ch)
									flag = true;
							if (!flag) {
								parent.showMessage(
										"Configure valid channels from this list [36, 44, 149, 157]",
										SWT.ICON_ERROR);
								return false;
							}
						} else if (value == -1) {
							short[] belowList = Mesh._40minus_ch;
							for (short ch : belowList)
								if (channel == ch)
									flag = true;
							if (!flag) {
								parent.showMessage(
										"Configure valid channels from this list [40, 48, 153,161]",
										SWT.ICON_ERROR);
								return false;
							}
						}
						if (channel == 165) {
							parent.showMessage(
									"For Bandwidth 80 MHz channel 165 shoud not support",
									SWT.ICON_ERROR);
							return false;
						}*/
						return true;
					} // 80 MHz
					return true;
				}
			}// if end
		}

		if (btnAutoChannel.getSelection() == true) {
			PortInfo pmo = pmInfoFrame.getPortInfo(selectedIfName);
			if (pmo == null) {
				parent.showMessage("Channel is not valid.", SWT.ICON_ERROR);
				return false;
			}
			IInterfaceInfo interfaceInfo = (IInterfaceInfo) pmInfoFrame
					.getRadiodataByName(selectedIfName);
			if (pmo.typeOfRadio == PortModelInfo.TYPEDOWNLINK) {
				InterfaceAdvanceConfiguration advanceInfo = (InterfaceAdvanceConfiguration) interfaceInfo
						.getInterfaceAdvanceConfiguration();
				int channelBandwidth = advanceInfo.getChannelBandwidth();
				if (btnAutoChannel.getSelection() == true) {
					int mediumSubtype = 0;
					try {
						String strProtocol = cmbOperationMode
								.getItem(cmbOperationMode.getSelectionIndex());
						String protocol = (String) cmbOperationMode
								.getData(strProtocol);
						mediumSubtype = Short.parseShort(protocol);

					} catch (Exception e) {
						Mesh.logException(e);
					}
					// sub type
					if (mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_24GHz_N
							|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_BGN) {
						return validatedownlinkDCAChannel(mediumSubtype,
								channelBandwidth);
					}
					if (mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_5GHz_N
							|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_AC
							|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_ANAC
							|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_AN) {
						return validationDCAChannel(interfaceInfo,
								channelBandwidth);
					}

				}
			}
			if (pmo.typeOfRadio == PortModelInfo.TYPEUPLINK) {
				InterfaceAdvanceConfiguration advanceInfo = (InterfaceAdvanceConfiguration) interfaceInfo
						.getInterfaceAdvanceConfiguration();
				int channelBandwidth = advanceInfo.getChannelBandwidth();
				if (btnAutoChannel.getSelection() == true) {
					int mediumSubtype = 0;
					try {
						String strProtocol = cmbOperationMode
								.getItem(cmbOperationMode.getSelectionIndex());
						String protocol = (String) cmbOperationMode
								.getData(strProtocol);
						mediumSubtype = Short.parseShort(protocol);

					} catch (Exception e) {
						Mesh.logException(e);
					}
					return validationDCAChannel(interfaceInfo, channelBandwidth);
				}
			}
		}
		return true;
	}

	public boolean validationDCAChannel(IInterfaceInfo interfaceInfo,
			int channelBandwidth) {

		if (channelBandwidth == Mesh.channelList_40_Above) {
			short[] aboveList = Mesh._40plus_ch;
			for (int i = 0; i < cmbAutoChannels.getItemCount(); i++) {
				String strChannel = cmbAutoChannels.getItem(i);
				try {
					boolean flag = false;
					short uiChannel = Short.parseShort(strChannel.trim());
					for (int k = 0; k < aboveList.length; k++) {
						if (uiChannel == aboveList[k]) {
							flag = true;
						}
					}
					if (!flag) {
						parent.showMessage(
								"For Channel Bandwidth Above 40 MHz Select from [36, 44, 149, 157]",
								SWT.ICON_ERROR);
						return false;
					}
				} catch (Exception e) {
					Mesh.logException(e);
				}
			}
			return true;
		}
		if (channelBandwidth == Mesh.channelList_40_Below) {
			short[] belowList = Mesh._40minus_ch;
			for (int i = 0; i < cmbAutoChannels.getItemCount(); i++) {
				String strChannel = cmbAutoChannels.getItem(i);
				try {
					boolean flag = false;
					short uiChannel = Short.parseShort(strChannel.trim());
					for (int k = 0; k < belowList.length; k++) {
						if (uiChannel == belowList[k]) {
							flag = true;
						}
					}
					if (!flag) {
						parent.showMessage(
								"For Channel Bandwidth Below 40 MHz Select from [40, 48, 153,161]",
								SWT.ICON_ERROR);
						return false;
					}
				} catch (Exception e) {
					Mesh.logException(e);
				}
			}
			return true;
		}
		// for 80MHz channel should not support
		if (channelBandwidth == Mesh.channel_80MHz) {
			
			short[] belowList = Mesh._80_ch;
			for (int i = 0; i < cmbAutoChannels.getItemCount(); i++) {
				String strChannel = cmbAutoChannels.getItem(i);
				try {
					boolean flag = false;
					short uiChannel = Short.parseShort(strChannel.trim());
					for (int k = 0; k < belowList.length; k++) {
						if (uiChannel == belowList[k]) {
							flag = true;
						}
					}
					if (!flag) {
						parent.showMessage(
								"80 MHz Configure valid channels from this list [48,161]",
								SWT.ICON_ERROR);
						return false;
					}
				} catch (Exception e) {
					Mesh.logException(e);
				}
			}
			
			
			/*short dcaChannel = interfaceInfo.getDcaChannel(0);
			int value = validateChannel_80MHz(dcaChannel);
			if (value == -1) {
				short[] belowList = Mesh._40minus_ch;
				for (int i = 0; i < cmbAutoChannels.getItemCount(); i++) {
					String strChannel = cmbAutoChannels.getItem(i);
					try {
						boolean flag = false;
						short uiChannel = Short.parseShort(strChannel.trim());
						for (int k = 0; k < belowList.length; k++) {
							if (uiChannel == belowList[k]) {
								flag = true;
							}
						}
						if (!flag) {
							parent.showMessage(
									"Configure valid channels from this list [40, 48, 153,161]",
									SWT.ICON_ERROR);
							return false;
						}
					} catch (Exception e) {
						Mesh.logException(e);
					}
				}
			} else if (value == 1) {
				short[] aboveList = Mesh._40plus_ch;
				for (int i = 0; i < cmbAutoChannels.getItemCount(); i++) {
					String strChannel = cmbAutoChannels.getItem(i);
					try {
						boolean flag = false;
						short uiChannel = Short.parseShort(strChannel.trim());
						for (int k = 0; k < aboveList.length; k++) {
							if (uiChannel == aboveList[k]) {
								flag = true;
							}
						}
						if (!flag) {
							parent.showMessage(
									"Configure valid channels from this list [36, 44, 149, 157]",
									SWT.ICON_ERROR);
							return false;
						}
					} catch (Exception e) {
						Mesh.logException(e);
					}
				}
				return true;
			}*/
			return true;
		}
		return true;
	}

	/**
	 * 
	 * @param channel
	 * @return
	 */
	private int validateChannel_80MHz(int channel) {
		short[] _40minus = Mesh._40minus_ch;
		short[] _40plus = Mesh._40plus_ch;
		for (int i = 0; i < _40minus.length; i++) {
			if (_40minus[i] == channel) {
				return -1;
			}
		}
		for (int i = 0; i < _40plus.length; i++) {
			if (_40plus[i] == channel) {
				return 1;
			}
		}
		return 0;
	}

	private boolean validatedownlinkDCAChannel(int phySubType,
			int channelBandwidth) {

		if (channelBandwidth == Mesh.channelList_40_Above) {
			for (int i = 0; i < cmbAutoChannels.getItemCount(); i++) {
				String strChannel = cmbAutoChannels.getItem(i);
				try {
					short uiChannel = Short.parseShort(strChannel.trim());
					if (uiChannel > Mesh.mediamChannel) {
						parent.showMessage(
								uiChannel
										+ " Channel should not support Above 40 MHz.Give <=6",
								SWT.ICON_ERROR);
						return false;
					}
				} catch (Exception e) {
					Mesh.logException(e);
				}
			}
			return true;
		}
		if (channelBandwidth == Mesh.channelList_40_Below) {
			for (int i = 0; i < cmbAutoChannels.getItemCount(); i++) {
				String strChannel = cmbAutoChannels.getItem(i);
				try {
					short uiChannel = Short.parseShort(strChannel.trim());
					if (uiChannel < Mesh.mediamChannel) {
						parent.showMessage(
								uiChannel
										+ " Channel should not support Below 40 MHz.Give >=6",
								SWT.ICON_ERROR);
						return false;
					}
				} catch (Exception e) {
					Mesh.logException(e);
				}
			}
			return true;
		}

		return true;
	}

	public boolean getDataFromUIToConfiguration() {

		int typeOfRadio = -1;
		String selectedIfName = "";
		IInterfaceInfo interfaceInfo = null;

		selectedIfName = pmInfoFrame.getLastSelectedRadioName();

		typeOfRadio = pmInfoFrame.getPortInfo(selectedIfName).typeOfRadio;
		interfaceInfo = (IInterfaceInfo) pmInfoFrame
				.getRadiodataByName(selectedIfName);

		switch (typeOfRadio) {

		case PortModelInfo.TYPEUPLINK:
			getDataForUplink(interfaceInfo);
			break;

		case PortModelInfo.TYPESCANNER:
			getDataForScanner(interfaceInfo);
			break;

		case PortModelInfo.TYPEDOWNLINK:
			getDataForDownlinkClientAP(interfaceInfo);
			break;

		case PortModelInfo.TYPECLIENTAP:
			getDataForDownlinkClientAP(interfaceInfo);
			break;

		case PortModelInfo.TYPESECONDDOWNLINK:
			getDataForDownlinkClientAP(interfaceInfo);
			break;

		default:
			break;

		}
		return true;
	}

	/**
	 * @param interfaceInfo
	 */
	private void getDataForScanner(IInterfaceInfo interfaceInfo) {

		int i = 0;

		if (btnClient11b.getSelection() == true
				&& btnClient11g.getSelection() == true) {
			interfaceInfo
					.setMediumSubType((short) Mesh.PHY_SUB_TYPE_802_11_B_G);
		} else if (btnClient11b.getSelection() == true) {
			interfaceInfo.setMediumSubType((short) Mesh.PHY_SUB_TYPE_802_11_B);
		} else if (btnClient11g.getSelection() == true) {
			interfaceInfo.setMediumSubType((short) Mesh.PHY_SUB_TYPE_802_11_G);
		}
		compareChannelList(interfaceInfo);
		interfaceInfo.setDcaListCount((short) cmbAutoChannels.getItemCount());

		for (i = 0; i < interfaceInfo.getDcaListCount(); i++) {
			String strChannel = cmbAutoChannels.getItem(i);
			try {
				short channel = Short.parseShort(strChannel.trim());
				interfaceInfo.setDcaChannel(i, channel);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 */
	private void getDataForDownlinkClientAP(IInterfaceInfo interfaceInfo) {

		String oldEssId;
		String newEssId;
		try {

			String strTxRate = cmbTransmitRate.getItem(cmbTransmitRate
					.getSelectionIndex());
			String txRate = (String) cmbTransmitRate.getData(strTxRate);
			interfaceInfo.setTxRate(Long.parseLong(txRate.trim()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		interfaceInfo.setTxPower((short) sldPowerSetting.getValue());
		interfaceInfo.setAckTimeout((short) sldAckTimeout.getValue());
		interfaceInfo.setFragThreshold((long) sldFragThr.getValue());
		interfaceInfo.setRtsThreshold((long) sldRtsThr.getValue());
		oldEssId = interfaceInfo.getEssid();
		newEssId = txtEssid.getText().trim();
		interfaceInfo.setEssid(newEssId);

		short hiddenEssid = 0;
		if (btnHideESSID.getSelection() == true) {
			hiddenEssid = 1;
		}
		interfaceInfo.setEssidHidden(hiddenEssid);

		short serviceType = 1;
		if (btnClientConnectivity.getSelection() == true) {
			serviceType = 0;
		}

		interfaceInfo.setServiceType(serviceType);
		setValuesForMediumSubType(interfaceInfo);// based on device, set medium
													// sub type
		if (grpPublicSafety.getVisible() == true) {
			interfaceInfo.setMediumSubType(getPublicSafetyPhyType());
		}
		if (btnAutoChannel.getSelection() == true) {

			int i = 0;
			interfaceInfo.setDynamicChannelAlloc((short) Mesh.ENABLED);
			compareChannelList(interfaceInfo);
			interfaceInfo.setDcaListCount((short) cmbAutoChannels
					.getItemCount());

			for (i = 0; i < interfaceInfo.getDcaListCount(); i++) {
				String strChannel = cmbAutoChannels.getItem(i);
				try {
					short channel = Short.parseShort(strChannel.trim());
					interfaceInfo.setDcaChannel(i, channel);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			Short selectedChannel = Short.parseShort(cmbAutoChannels
					.getItem(cmbAutoChannels.getSelectionIndex()));
			interfaceInfo.setChannel(selectedChannel);

		} else {
			interfaceInfo.setDynamicChannelAlloc((short) Mesh.DISABLED);
			try {
				short channel = Short.parseShort(txtManualChannel.getText()
						.trim());
				interfaceInfo.setChannel(channel);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (oldEssId.equalsIgnoreCase(newEssId) == false) {
			parent.essIdChangedForInterface(interfaceInfo.getName());
		}
		/*this is checking the virtual interface by commented by nagaraju from start here*/
		String selectedIfName = pmInfoFrame.getLastSelectedRadioName();
		int radioCount = pmInfoFrame.getRadioCount();
		for(int i = 0;i<radioCount;i++){
			PortInfo pi = pmInfoFrame.getPortInfo(i);
			if(pi.ifName.equals(selectedIfName)){
				IInterfaceInfo info = parent.getInterfaceInfoByName(selectedIfName);
				if (info.getMediumType() != Mesh.PHY_TYPE_802_11_VIRTUAL
						&& pi.typeOfRadio == PortModelInfo.TYPEDOWNLINK) {
					if (findVirtualInterface() == true
							&& pmInfoFrame.getPortInfo(selectedIfName).freqOfRadio
									.equals("5G")) {
						setValuesWLan0ToV_Wlan0(interfaceInfo);
					}
				}
			}
		}
		/*this is checking the virtual interface by commented by nagaraju from end here*/
	
	}

	/**
	 * @param interfaceInfo
	 * 
	 */
	private void getDataForUplink(IInterfaceInfo interfaceInfo) {

		try {
			String strTxRate = cmbTransmitRate.getItem(cmbTransmitRate
					.getSelectionIndex());
			String txRate = (String) cmbTransmitRate.getData(strTxRate);
			interfaceInfo.setTxRate(Long.parseLong(txRate.trim()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		interfaceInfo.setTxPower((short) sldPowerSetting.getValue());
		interfaceInfo.setAckTimeout((short) sldAckTimeout.getValue());
		interfaceInfo.setFragThreshold((long) sldFragThr.getValue());
		interfaceInfo.setRtsThreshold((long) sldRtsThr.getValue());
		setValuesForMediumSubType(interfaceInfo);
		if (grpPublicSafety.getVisible() == true) {
			interfaceInfo.setMediumSubType(getPublicSafetyPhyType());
		}
		/**
		 * Scan channel list to be done
		 */
		int i = 0;
		compareChannelList(interfaceInfo);
		interfaceInfo.setDcaListCount((short) cmbAutoChannels.getItemCount());

		for (i = 0; i < interfaceInfo.getDcaListCount(); i++) {
			String strChannel = cmbAutoChannels.getItem(i);
			try {
				short channel = Short.parseShort(strChannel.trim());
				interfaceInfo.setDcaChannel(i, channel);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
 * 
 */
	private void setValuesForMediumSubType(IInterfaceInfo interfaceInfo) {

		if (btnClient11b.getSelection() == true
				&& btnClient11g.getSelection() == true) {
			interfaceInfo
					.setMediumSubType((short) Mesh.PHY_SUB_TYPE_802_11_B_G);
		} else if (btnClient11b.getSelection() == true) {
			interfaceInfo.setMediumSubType((short) Mesh.PHY_SUB_TYPE_802_11_B);
		} else if (btnClient11g.getSelection() == true) {
			interfaceInfo.setMediumSubType((short) Mesh.PHY_SUB_TYPE_802_11_G);
		}
		try {
			String strProtocol = cmbOperationMode.getItem(cmbOperationMode
					.getSelectionIndex());
			String protocol = (String) cmbOperationMode.getData(strProtocol);
			interfaceInfo.setMediumSubType(Short.parseShort(protocol.trim()));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private short compareMediumSubType(IInterfaceInfo interfaceInfo) {
		short mediumSubtype = 0;

		if (btnClient11b.getSelection() == true
				&& btnClient11g.getSelection() == true) {
			mediumSubtype = (short) Mesh.PHY_SUB_TYPE_802_11_B_G;
		} else if (btnClient11b.getSelection() == true) {
			mediumSubtype = (short) Mesh.PHY_SUB_TYPE_802_11_B;
		} else if (btnClient11g.getSelection() == true) {
			mediumSubtype = (short) Mesh.PHY_SUB_TYPE_802_11_G;
		} else {
			mediumSubtype = (short) Mesh.PHY_SUB_TYPE_802_11_A;
		}

		try {
			String strProtocol = cmbOperationMode.getItem(cmbOperationMode
					.getSelectionIndex());
			String protocol = (String) cmbOperationMode.getData(strProtocol);
			mediumSubtype = Short.parseShort(protocol);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return mediumSubtype;
	}

	/*
	 * to find virtual interface
	 */
	private boolean findVirtualInterface() {

		IConfiguration iConfig = parent.getConfiguration();
		boolean flag = false;
		int radioCount;
		String interfaceName;
		radioCount = pmInfoFrame.getRadioCount();
		IInterfaceConfiguration interfaceConfig = iConfig
				.getInterfaceConfiguration();
		if (interfaceConfig == null) {
			return false;
		}
		for (int i = 0; i < radioCount; i++) {
			PortInfo portInfo = pmInfoFrame.getPortInfo(i);
			interfaceName = portInfo.ifName;
			IInterfaceInfo interfaceInfo = (IInterfaceInfo) interfaceConfig
					.getInterfaceByName(interfaceName);
			if (interfaceInfo == null) {
				continue;
			}
			if (interfaceInfo.getMediumType() == 2)
				flag = true;
		}
		return flag;
	}

	public void setValuesWLan0ToV_Wlan0(IInterfaceInfo interfaceInfo) {

		String selectedIfName = pmInfoFrame.getLastSelectedRadioName();
		IInterfaceConfiguration iinterface = iConfiguration
				.getInterfaceConfiguration();
		InterfaceInfo ifInfoObject = (InterfaceInfo) iinterface
				.getInterfaceByName(selectedIfName);
		InterfaceAdvanceConfiguration advanceConfig = ifInfoObject
				.getInterfaceAdvanceConfiguration();

		IInterfaceInfo vwlaninterfaceInf = null;
		
		int radioCount = pmInfoFrame.getRadioCount();
				for(int i = 0;i<radioCount;i++){
					PortInfo pi = pmInfoFrame.getPortInfo(i);
						IInterfaceInfo info = parent.getInterfaceInfoByName(pi.ifName);
						if (info.getMediumType() == Mesh.PHY_TYPE_802_11_VIRTUAL
								&& pi.typeOfRadio == PortModelInfo.TYPEDOWNLINK) {
							if (findVirtualInterface() == true
									&& pmInfoFrame.getPortInfo(pi.ifName).freqOfRadio
											.equals("5G")) {
								vwlaninterfaceInf =	(IInterfaceInfo) pmInfoFrame
								.getRadiodataByName(pi.ifName);
							break;
						}
					}
				}
		if(vwlaninterfaceInf == null)
			return;

		vwlaninterfaceInf.setTxRate(interfaceInfo.getTxRate());
		vwlaninterfaceInf.setTxPower(interfaceInfo.getTxPower());
		vwlaninterfaceInf.setAckTimeout(interfaceInfo.getAckTimeout());
		vwlaninterfaceInf.setFragThreshold(interfaceInfo.getFragThreshold());
		vwlaninterfaceInf.setRtsThreshold(interfaceInfo.getRtsThreshold());
//		vwlaninterfaceInf.setServiceType(interfaceInfo.getServiceType());
		vwlaninterfaceInf.setMediumSubType(interfaceInfo.getMediumSubType());
		vwlaninterfaceInf.setDynamicChannelAlloc(interfaceInfo
				.getDynamicChannelAlloc());
		vwlaninterfaceInf.setDcaListCount(interfaceInfo.getDcaListCount());
		
		for (int i = 0; i < interfaceInfo.getDcaListCount(); i++) {
			short strChannel = interfaceInfo.getDcaChannel(i);
			vwlaninterfaceInf.setDcaChannel(i, strChannel);
		}
		vwlaninterfaceInf.setChannel(interfaceInfo.getChannel());

		InterfaceAdvanceConfiguration ifAdvance = (InterfaceAdvanceConfiguration) vwlaninterfaceInf
				.getInterfaceAdvanceConfiguration();

		ifAdvance.setGuardInterval_20(advanceConfig.getGuardInterval_20());
		ifAdvance.setGuardInterval_40(advanceConfig.getGuardInterval_40());
		ifAdvance.setGuardInterval_80(advanceConfig.getGuardInterval_80());
		ifAdvance.setChannelBandwidth(advanceConfig.getChannelBandwidth());

		ifAdvance.setSecondaryChannelPosition(advanceConfig
				.getSecondaryChannelPosition());

		ifAdvance.setPreamble(advanceConfig.getPreamble());

		ifAdvance.setMaxAMPDU(advanceConfig.getMaxAMPDU());

		ifAdvance.setMaxAMSDU(advanceConfig.getMaxAMSDU());

		ifAdvance.setMaxMPDU(advanceConfig.getMaxMPDU());

		ifAdvance.setLDPC(advanceConfig.getLDPC());

		ifAdvance.setTxSTBC(advanceConfig.getTxSTBC());

		ifAdvance.setRxSTBC(advanceConfig.getRxSTBC());

		ifAdvance.setGFMode(advanceConfig.getGFMode());

		ifAdvance.setMaxAMPDUEnabled(advanceConfig.getMaxAMPDUEnabled());

		ifAdvance.setCoexistence(advanceConfig.getCoexistence());
	}
	
	public void setEnableDisable(boolean flag){
		
		cmbTransmitRate.setEnabled(flag);
		sldPowerSetting.setEnabled(flag);
		sldAckTimeout.setEnabled(flag);
		// new field
		cmbOperationMode.setEnabled(flag);
		grpMode.setEnabled(flag);
		sldFragThr.setEnabled(flag);
		sldRtsThr.setEnabled(flag);
		btnAutoChannel.setEnabled(flag);
		btnAddAutoChannel.setEnabled(flag);
		btnRemoveAutoChannel.setEnabled(flag);
		cmbAutoChannels.setEnabled(flag);
		grpClientConnectivity.setEnabled(flag); // client connectivity
		btnManualChannel.setEnabled(flag);
		grpDCA.setEnabled(flag);
		btnViewSupportedChannelInfo.setEnabled(flag);
		interButton.setEnabled(flag);
	}
	
	public boolean validateUplinkChannel(String selectedIfName,int channel){
		
		IInterfaceInfo interfaceInfo = (IInterfaceInfo) pmInfoFrame.getRadiodataByName(selectedIfName);
		InterfaceAdvanceConfiguration advanceInfo = (InterfaceAdvanceConfiguration) interfaceInfo
						.getInterfaceAdvanceConfiguration();
		int channelBandwidth = advanceInfo.getChannelBandwidth();
		int mediumSubtype = 0;
		try {
			String strProtocol = cmbOperationMode
					.getItem(cmbOperationMode.getSelectionIndex());
			String protocol = (String) cmbOperationMode
					.getData(strProtocol);
			mediumSubtype = Short.parseShort(protocol);

		} catch (Exception e) {
			e.printStackTrace();
		}
		boolean flagcheck = false;
		if(mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_A){
			boolean flag = false;
			short[] aboveList = {36,40, 44, 48, 52, 56, 60, 64, 149, 153, 157, 161};
			for (short value : aboveList)
				if (channel == value)
					flag = true;
			if (!flag) {
				parent.showMessage(
						"For Scan Channel is not present in supported channel list Select from [36, 40, 44, 48, 52, 56, 60, 64, 149, 153, 157, 161]",
						SWT.ICON_ERROR);
				return flag;
			}
		}
		if (mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_5GHz_N
				|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_AC
				|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_ANAC
				|| mediumSubtype == Mesh.PHY_SUB_TYPE_802_11_AN) {
			boolean flag = false;
			if (channelBandwidth == Mesh.channelList_40_Above) {
				short[] aboveList = Mesh._40plus_ch;
				for (short value : aboveList)
					if (channel == value)
						flag = true;
				if (!flag) {
					parent.showMessage(
							"For Channel Bandwidth Above 40 MHz Select from [36, 44, 149, 157]",
							SWT.ICON_ERROR);
					return false;
				}
				return true;
			}
			if (channelBandwidth == Mesh.channelList_40_Below) {
				short[] belowList = Mesh._40minus_ch;
				for (short value : belowList)
					if (channel == value)
						flag = true;
				if (!flag) {
					parent.showMessage(
							"For Channel Bandwidth Below 40 MHz Select from [40, 48, 153,161]",
							SWT.ICON_ERROR);
					return false;
				}
				return true;
			}
			if (channelBandwidth == Mesh.channel_80MHz) {
				short[] belowList = Mesh._80_ch;
				for (short ch : belowList)
					if (channel == ch)
						flag = true;
				if (!flag) {
					parent.showMessage(
							"80 MHz Configure valid channels from this list [48,161]",
							SWT.ICON_ERROR);
					return false;
				}
				return true;
			} // 80 MHz
			return true;
		}
		return flagcheck;
	}// if end
}
