/**
 * MeshDynamics 
 * -------------- 
 * File     : ACLConfigPage.java
 * Comments : 
 * Created  : Feb 22, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * -----------------------------------------------------------------------------------
 * | 8  |Jun   8, 2007   | duplicate ACl entries blocked	          | Imran        |
 * -----------------------------------------------------------------------------------
 * | 6  |May   3, 2007   | dot11 category dialogbox removed           | Imran        |
 * ----------------------------------------------------------------------------------
 * | 5  |May   2, 2007   | chkDuplicates modified                     | Imran        |
 * -----------------------------------------------------------------------------------
 * | 4  |May   2, 2007   | dot11e dialog made local                   | Imran        |
 * -----------------------------------------------------------------------------------
 * | 3  |Apr   10, 2007  | getDataFromUiToConfigutration added        | Imran        |
 * -----------------------------------------------------------------------------------
 * | 2  |May   12, 2007  | compareConfigurationWithUi added           | Imran        |
 * -----------------------------------------------------------------------------------
 * | 1  | Apr 05, 2007   | UI validation are added        			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 22, 2007   | Created                        			  | Abhijeet     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.dialogs.settings;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import com.meshdynamics.meshviewer.configuration.IACLConfiguration;
import com.meshdynamics.meshviewer.configuration.IACLInfo;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.meshviewer.configuration.impl.ACLInfo;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.nmsui.dialogs.UIConstants;
import com.meshdynamics.nmsui.dialogs.base.ConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.util.IStateEventHandler;
import com.meshdynamics.util.MacAddress;
import com.meshdynamics.util.StateManager;



public class ACLConfigPage extends ConfigPage implements IStateEventHandler{
	
	private Group						grp;
	private Text 						txtNodeMacAddress;
	private Combo						cmbNames;
	private Button 						btnDot11eEnabled;
	private Combo 						cmbDot11eCategory;
	private Button 						btnAllow;
	private Button 						btnBlock;
	private ToolBar						toolBar;
	private ToolItem 					btnNew,btnEdit,btnSave,btnDelete;
    private Table   					tblACL;
	private Button 						btnBlockAll;
		
	private int 						rel_x;
	private int 						rel_y;
	
	private final int					STATE_START		= 0;
	private final int 					STATE_NEW  		= 1;
	private final int 					STATE_EDIT 		= 2;
	private final int 					STATE_SAVE 		= 3;
	private final int 					STATE_DELETE	= 4;
	private final int 					STATE_SELECT	= 5; 
	
	private final int 					MAX_STATE_CNT	= 6;
	
	private final String				DEFAULT_TEXT	= "default";
	private final int					DEFAULT_TAG		= -1;
	
	private StateManager 				sMananger;
	private int 						lastSelectedAclIndex;
	private Vector<String>				deletedAclEntries;
	private IVlanConfiguration          vlanConfiguration;
	
	public ACLConfigPage(IMeshConfigBook parent) {
		super(parent);
		sMananger 			 	= new StateManager(this,MAX_STATE_CNT,STATE_START);
		lastSelectedAclIndex	= -1;
		deletedAclEntries		= new Vector<String>();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IStateEventHandler#init()
	 */
	public void init() {
		sMananger.triggerState(STATE_START); 
	}
	
	public void createControls(CTabFolder tabFolder) {
		
		super.createControls(tabFolder);
		
		grp = new Group(canvas,SWT.NONE);
		grp.setBounds(10,10,415,515);

		rel_x	= 20;
		rel_y	= 20;
		
		createACLControls();
		createACLListcontrols();
		ContextHelp.addContextHelpHandlerEx(this.canvas,contextHelpEnum.CONFIGTABACL);
		sMananger.triggerState(STATE_START);
	}
	
	
	private void createACLControls() {

		Label lbl = new Label(grp,SWT.NO);
		lbl.setBounds(rel_x,rel_y,100,17);
		lbl.setText("MAC Address");
		
		txtNodeMacAddress	= new Text(grp,SWT.BORDER);
		txtNodeMacAddress.setBounds(rel_x+160,rel_y,200,20);
						
		rel_y += 30;
		
		lbl	= new Label(grp,SWT.NO);
		lbl.setBounds(rel_x,rel_y,100,20);
		lbl.setText("VLAN");
		
		cmbNames	= new Combo(grp,SWT.READ_ONLY|SWT.BORDER);
		cmbNames.setBounds(rel_x+ 160,rel_y,200,15);
		cmbNames.addSelectionListener(new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				int 	index;
				String  vlanName;
				
				index		= cmbNames.getSelectionIndex();
				vlanName	= cmbNames.getItem(index);
				IVlanInfo vlanInfo	= (IVlanInfo)vlanConfiguration.getVlanInfoByName(vlanName);	
				if(vlanInfo != null) {
					if(vlanInfo.get80211eEnabled() == 1){
						btnDot11eEnabled.setSelection(true);
						//btnDot11eEnabledHandler();
						btnDot11eEnabled.setEnabled(false);
						cmbDot11eCategory.select(vlanInfo.get80211eCategoryIndex());
						//cmbDot11eCategory.select(0);
					}
				}else if(vlanName.equalsIgnoreCase(DEFAULT_TEXT)) {
					btnDot11eEnabled.setEnabled(true);
				}else {
					btnDot11eEnabled.setSelection(false);
					btnDot11eEnabledHandler();
					btnDot11eEnabled.setEnabled(false);
					//cmbDot11eCategory.select(0);
				}
			}
		});		
		
		rel_y += 30;
		
		lbl	= new Label(grp,SWT.NO);
		lbl.setBounds(rel_x,rel_y,100,15);
		lbl.setText("802.11e Category");
		
		btnDot11eEnabled = new Button(grp,SWT.CHECK);
		btnDot11eEnabled.setBounds(rel_x+160,rel_y,19,20);
		btnDot11eEnabled.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				btnDot11eEnabledHandler();
			}
		});
		
		cmbDot11eCategory	= new Combo(grp,SWT.READ_ONLY|SWT.BORDER);
		cmbDot11eCategory.setBounds(rel_x+180,rel_y,150,17);
		
		cmbDot11eCategory.add("Background");
		cmbDot11eCategory.add("Best Effort");
		cmbDot11eCategory.add("Video");
		cmbDot11eCategory.add("Voice");
		cmbDot11eCategory.select(0);
		cmbDot11eCategory.setEnabled(false);
			
		rel_y = rel_y + 30;
		
		lbl	=	new Label(grp,SWT.NO);
		lbl.setBounds(rel_x,rel_y,100,20);
		lbl.setText("Options");
		
		btnAllow	=	new Button(grp,SWT.RADIO);
		btnAllow.setBounds(rel_x + 160,rel_y,45,20);
		btnAllow.setText("Allow");
		btnAllow.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if(btnAllow.getSelection() == true) {
					cmbNames.setEnabled(true);
				}else {
					cmbNames.setEnabled(false);
				}
			}
		});
		
		btnBlock =	new Button(grp,SWT.RADIO);
		btnBlock.setBounds(rel_x + 260,rel_y,45,20);
		btnBlock.setText("Block");	
		btnBlock.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if(btnBlock.getSelection() == true) {
					cmbNames.setEnabled(true);
				}else {
					cmbNames.setEnabled(false);
				}
				/*if(btnBlock.getSelection() == true) {
					cmbNames.select(0);
					cmbNames.setEnabled(false);
					btnDot11eEnabled.setEnabled(false);
					btnDot11eEnabled.setSelection(false);
					btnDot11eEnabledHandler();
				}else {
					cmbNames.setEnabled(true);
					btnDot11eEnabled.setEnabled(true);
				}*/
			}
		});
		
		rel_y = rel_y + 30;
	}
	
	private void btnDot11eEnabledHandler() {
		if(btnDot11eEnabled.getSelection() == true){
			cmbDot11eCategory.setEnabled(true);
		} else {
			cmbDot11eCategory.setEnabled(false);
			cmbDot11eCategory.select(0);
		}
	}
	
	private void createACLListcontrols() {
		
		rel_y 	+= 15;
		rel_x	= 15;
		
		toolBar = new ToolBar(grp,SWT.FLAT);
		toolBar.setBounds(rel_x,rel_y,100,25);		
        
		btnNew	= new ToolItem(toolBar,SWT.IMAGE_BMP);
        txtNodeMacAddress.setEnabled(false);
		
		btnNew.setImage(UIConstants.imgNew); 
		btnNew.setToolTipText("New");
		btnNew.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				notifyStateChanged(STATE_NEW);
			}
		});
		
		btnEdit	= new ToolItem(toolBar,SWT.IMAGE_BMP);
		btnEdit.setImage(UIConstants.imgEdit); 
		btnEdit.setToolTipText("Edit");
		btnEdit.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				notifyStateChanged(STATE_EDIT);
			}
		});		
				
		btnSave	= new ToolItem(toolBar,SWT.IMAGE_BMP);
		btnSave.setImage(UIConstants.imgSave); 
		btnSave.setToolTipText("Save");
		btnSave.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				notifyStateChanged(STATE_SAVE);
			}
		});			
	
		btnDelete	= new ToolItem(toolBar,SWT.IMAGE_BMP);
		btnDelete.setToolTipText("Delete");
		btnDelete.setImage(UIConstants.imgDel); 
		btnDelete.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				notifyStateChanged(STATE_DELETE);
			}
		});		
		
		rel_y = rel_y + 30;
			
		tblACL = new Table (grp,SWT.BORDER|SWT.SINGLE|SWT.FULL_SELECTION);
		tblACL.setHeaderVisible(true);
		tblACL.setLinesVisible(true);
		tblACL.setBounds(rel_x,rel_y,385,270);
		String Headers[] = {"Mac Address","VLAN Name","Options"};
		TableColumn col;
		for(int i = 0;i<Headers.length;i++ ) {
			
			col  = new TableColumn(tblACL, SWT.NONE);
			col.setText(Headers[i]);
			col.setWidth(127);	
		}
				
		rel_y	+= 285;
		tblACL.addSelectionListener(new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				tableHandler();
			}
		});
		
		btnBlockAll = new Button(grp,SWT.CHECK);
		btnBlockAll.setBounds(rel_x,rel_y,200,20);
		btnBlockAll.setText("Reject clients not in list");
		btnBlockAll.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				btnBlockAllHandler();
			}			
		});
	}
	
	private void tableHandler(){
		int index;
		if(deletedAclEntries.size() > 0) {
			parent.showMessage("One of the ACL entry is deleted. "+SWT.LF+
					"You must save these changes.", SWT.OK|SWT.ICON_ERROR);
			tblACL.deselectAll();
			return;
		}
		
		index	= tblACL.getSelectionIndex(); 
		if(lastSelectedAclIndex == index) {
			return;
		}
		if(sMananger.getState() == STATE_EDIT || sMananger.getState() == STATE_NEW) {
			parent.showMessage("You must save currently selected ACL entry. ",SWT.ICON_INFORMATION|SWT.OK);
			tblACL.deselectAll();
			tblACL.select(lastSelectedAclIndex);
			return;
		}
		
		index					= tblACL.getSelectionIndex(); 
		notifyStateChanged(STATE_SELECT);
		showACLValues(index);
		lastSelectedAclIndex	= index; 
	
	}
	
	private void btnBlockAllHandler() {
		if(btnBlockAll.getSelection() == true){
			addBlockAllEntry();
		}else {
			removeBlockAllEntry();
		}
	}
	
	public void addBlockAllEntry(){
		
		String		strStaMac;
		MacAddress	staMac;
		
		IConfiguration	iConfiguration	= parent.getConfiguration();
		if(iConfiguration == null) {
			return ;
		}
		
		IACLConfiguration	aclConfig   = iConfiguration.getACLConfiguration();
		if(aclConfig == null) {
			return;
		}
		strStaMac			= "ff:ff:ff:ff:ff:ff";
		staMac				= new MacAddress();
		staMac.setBytes(strStaMac);
		if(aclConfig.containsAclEntryWithStaAddr(staMac) == true){
			return;
		}
		IACLInfo	aclInfo = (IACLInfo)aclConfig.addAclInfo(staMac); 
		aclInfo.setStaAddress(staMac);
		fillBolckAllACLInfo(aclInfo);
	}
	
	public void removeBlockAllEntry(){
		
		String	strStaMac;
		MacAddress	staMac;
		
		strStaMac			= "ff:ff:ff:ff:ff:ff";
		staMac				= new MacAddress();
		staMac.setBytes(strStaMac);
		IACLInfo	aclInfo = new ACLInfo(); 
		aclInfo.setStaAddress(staMac);
		fillBolckAllACLInfo(aclInfo);
						
		IConfiguration	iConfiguration	= parent.getConfiguration();
		if(iConfiguration == null) {
			return ;
		}
		
		IACLConfiguration	aclConfig   = iConfiguration.getACLConfiguration();
		if(aclConfig == null) {
			return;
		}
		aclConfig.deleteAclInfo(aclInfo);
	}
	
	/**
	 * @param aclInfo
	 */
	private void fillBolckAllACLInfo(IACLInfo aclInfo) {
		
		aclInfo.setVLANTag(DEFAULT_TAG);
		aclInfo.setAllow((short)0);
		aclInfo.setEnable80211eCategory((short)0);
		aclInfo.set80211eCategoryIndex((short)0);
		
	}

	public void initalizeLocalData() {
		
		clearAllControls();
		tblACL.removeAll();
		cmbNames.removeAll();
		deletedAclEntries.clear();
		
		IConfiguration	iConfiguration	= parent.getConfiguration();
		if(iConfiguration == null) {
			return;
		}
		
		IACLConfiguration	aclConfig   = iConfiguration.getACLConfiguration();
		if(aclConfig == null) {
			return;
		}
		
		vlanConfiguration	= iConfiguration.getVlanConfiguration();
		if(vlanConfiguration == null) {
			return;
		}
		
		fillVlanNamesCombo(vlanConfiguration);
		fillTableData(aclConfig);
		
		IACLInfo	aclInfo 		= new ACLInfo();
		String 		strStaMac		= "ff:ff:ff:ff:ff:ff";
		MacAddress	staMac			= new MacAddress();
		staMac.setBytes(strStaMac);
		aclInfo.setStaAddress(staMac);
		fillBolckAllACLInfo(aclInfo);
		if(aclConfig.containsAclEntry(aclInfo)) {
			btnBlockAll.setSelection(true);
		} 
		else{
			btnBlockAll.setSelection(false);
		}
		if(aclConfig.getCount() == 0) {
			btnBlockAll.setEnabled(false);
		}else{
			btnBlockAll.setEnabled(true);
		}
		
		if(aclConfig.getCount() == 0) {
			return;
		}
		
		showACLValues(lastSelectedAclIndex);
	}
	
	/**
	 * @param vlanConfig
	 */
	private void fillVlanNamesCombo(IVlanConfiguration vlanConfig) {
		
		int 		vlanCount;
		int 	    vlanTag;
		int 		index;
		String      vlanName;
		
		cmbNames.add(DEFAULT_TEXT);
		cmbNames.setData(""+DEFAULT_TAG, DEFAULT_TEXT);
		cmbNames.setData(DEFAULT_TEXT,""+DEFAULT_TAG);
				
		vlanCount	= vlanConfig.getVlanCount();
		
		for(index=1; index<=vlanCount; index++){
			
			IVlanInfo vlanInfo = (IVlanInfo)vlanConfig.getVlanInfoByIndex(index-1);
			if(vlanInfo == null) {
				continue;
			}
			
			vlanName	= vlanInfo.getName();
			cmbNames.add(vlanName);
			vlanTag     = vlanInfo.getTag();	
			cmbNames.setData("" + vlanTag,vlanName);
			cmbNames.setData(vlanName,"" + vlanTag);
		}
		
	}

	/**
	 * 
	 */
	private void fillTableData(IACLConfiguration aclConfig) {
		
		int 	aclCount;
		int 	index;
		String	vlanName;
		
		aclCount	= aclConfig.getCount();
		
		for(index=0; index<aclCount; index++) {
			
			IACLInfo	aclInfo	= (IACLInfo)aclConfig.getACLInfo(index);
		
			if(aclInfo == null) {
				continue; 
			}
			if(aclInfo.getStaAddress().toString().equalsIgnoreCase("ff:ff:ff:ff:ff:ff")) {
				continue;
			}
			
			TableItem 	item	= new TableItem(tblACL,SWT.NONE);
			item.setText(0, aclInfo.getStaAddress().toString());
			vlanName 			= (String)cmbNames.getData(""+aclInfo.getVLANTag());
			item.setText(1,vlanName);
			
			if(aclInfo.getAllow() == 1) {
				item.setText(2,"Allow");
			}
			else {
				item.setText(2,"Block");
			}
			tblACL.setData(aclInfo.getStaAddress().toString(), aclInfo);
		}
	
	}

	/**
	 * @param aclConfig
	 */
	private void showACLValues(int aclIndex) {
		
		String strKey;
			
		if(tblACL.getItemCount() == 0){
			clearAllControls();
			return;
		}
		
		if(aclIndex == -1) {
			return;
		}
		
		tblACL.select(aclIndex);
						
		TableItem 	item	=  tblACL.getItem(aclIndex);
		strKey				= item.getText(0);
		IACLInfo	aclInfo	= (IACLInfo)tblACL.getData(strKey);
		if(aclInfo == null) {
			return;
		}
		displayACLValues(aclInfo);
	}

	/**
	 * 
	 */
	private void displayACLValues(IACLInfo aclInfo) {
		
		int		vlanTag;
		int 	category;
		String	vlanName;
		String	cmbVlanName;
		
		if(aclInfo == null) {
			return;
		}
		clearAllControls();
		
		txtNodeMacAddress.setText(aclInfo.getStaAddress().toString());
		
		vlanTag		= aclInfo.getVLANTag();
		vlanName	= (String)cmbNames.getData(""+vlanTag);
		
		for(int index=0; index<cmbNames.getItemCount(); index++){
			
			cmbVlanName = cmbNames.getItem(index);
			
			if(vlanName.equalsIgnoreCase(cmbVlanName)) {
				cmbNames.select(index);
				break;
			}
		}
				
		if(aclInfo.getVLANTag() == -1){
			
			if(aclInfo.getEnable80211eCategory() == 0) {
				btnDot11eEnabled.setSelection(false);
			}
			else {
				btnDot11eEnabled.setSelection(true);
			}
			
			category = aclInfo.get80211eCategoryIndex();
	    	
			if(category == IACLInfo.DOT_11_E_CATEGORY_BACKGROUND) {			
	    		cmbDot11eCategory.select(IACLInfo.DOT_11_E_CATEGORY_BACKGROUND);
	    	} else if(category == IACLInfo.DOT_11_E_CATEGORY_BEST_EFFORT) {
	    			cmbDot11eCategory.select(IACLInfo.DOT_11_E_CATEGORY_BEST_EFFORT);
	    	} else if(category == IACLInfo.DOT_11_E_CATEGORY_VIDEO) {	
	    		cmbDot11eCategory.select(IACLInfo.DOT_11_E_CATEGORY_VIDEO);	
	    	} else if(category == IACLInfo.DOT_11_E_CATEGORY_VOICE) {
	    		cmbDot11eCategory.select(IACLInfo.DOT_11_E_CATEGORY_VOICE);		
	    	} 
		}
		
		if(aclInfo.getAllow() == 1) {
			btnAllow.setSelection(true);
			btnBlock.setSelection(false);
		}else {
			btnAllow.setSelection(false);
			btnBlock.setSelection(true);
		}
	}

	public void selectionChanged(boolean selected) {
		
		if(selected == false){
			
			if((sMananger.getState() == STATE_START) ||
					(sMananger.getState() == STATE_SAVE) ||
					(sMananger.getState() == STATE_SELECT)) {
				return;
			}
			
			if(validateLocalData() == false){
				stayOnSameTab();
				return;
			}
			
			if(sMananger.getState() == STATE_NEW) {
				int userSays	= parent.showMessage("Newly created ACL entry is not saved. "+SWT.LF+
						"Do you want to save these changes? ", SWT.YES|SWT.CANCEL|SWT.NO|SWT.ICON_QUESTION);
	
				if(userSays == SWT.YES){
					getDataFromUIToLocalConfiguration();
					
				} else if(userSays == SWT.NO){
					notifyStateChanged(STATE_START);
				} else if(userSays == SWT.CANCEL){
					stayOnSameTab();
				}
				return;
			}
			
			if(compairConfigurationWithUI() == false){	
				int userSays	= parent.showMessage("You have changed ACL settings. "+SWT.LF+
						"Do you want to save these changes? ", SWT.YES|SWT.CANCEL|SWT.NO|SWT.ICON_QUESTION);
	
				if(userSays == SWT.YES){
					//getDataFromUiToLocalConfiguration();
					notifyStateChanged(STATE_SAVE);
				} else if(userSays == SWT.NO){
					notifyStateChanged(STATE_START);
				} else if(userSays == SWT.CANCEL){
					stayOnSameTab();
				}
				return;
			}
			
			if(sMananger.getState() == STATE_EDIT) {	
				
				int userSays	= parent.showMessage("ACL settings are not changed. "+SWT.LF+
						"Do you want to continue? ", SWT.YES|SWT.NO|SWT.ICON_QUESTION);
			
				if(userSays == SWT.YES) {
					notifyStateChanged(STATE_SAVE);
				} else if(userSays == SWT.NO) {
					stayOnSameTab();
				}
				return;
			}
		}else{
			refreshConfiguration();
		}
	
	}

	/**
	 * @return
	 */
	private boolean compairConfigurationWithUI() {
		
		int 	index;
		int 	vlanTag;
		int 	selection;
		String 	newMac;
		String 	oldMac;
		String  strKey;
		String  vlanName;
		
		if(deletedAclEntries.size() > 0){
			return false;
		}
		
		if(tblACL.getItemCount() == 0) {
			return true;
		}
		
		index 				= tblACL.getSelectionIndex();
		if(index == -1) {
			return false;
		}
		TableItem   item 	= tblACL.getItem(index);
		vlanName			= item.getText(1);
		strKey				= item.getText(0);
				
		IACLInfo	aclInfo	= (IACLInfo)tblACL.getData(strKey);
	
		if(aclInfo == null) {
			return false;
		}
		
		newMac	= txtNodeMacAddress.getText().toString();
		oldMac	= aclInfo.getStaAddress().toString();
		if(newMac.equalsIgnoreCase(oldMac) == false){
			return false;
		}
		
		index		= cmbNames.getSelectionIndex();
		vlanName	= cmbNames.getItem(index);
				
		IConfiguration iConfig			= parent.getConfiguration();
		if(iConfig == null) {
			return false;
		}
		
		IVlanConfiguration  vlanConfig	= iConfig.getVlanConfiguration();
		if(vlanConfig == null) {
			return false;
		}
		
		if(vlanName.equalsIgnoreCase(DEFAULT_TEXT)) {
			vlanTag = vlanConfig.getDefaultTag();
		}else {
			IVlanInfo vlanInfo	= (IVlanInfo)vlanConfig.getVlanInfoByName(vlanName);	
			if(vlanInfo == null) {
				return false;
			}
			vlanTag	= vlanInfo.getTag();
		}
		
		if(vlanTag != aclInfo.getVLANTag()) {
			return false;
		}
		
		selection = (btnAllow.getSelection() == true) ? 1 : 0;
		
		if(selection != aclInfo.getAllow()) {
			return false;
		}
		
		if(vlanTag != -1) {
			return true;
		}
		
		selection = (btnDot11eEnabled.getSelection() == true) ? 1 : 0;
	
		if(selection != aclInfo.getEnable80211eCategory()) {
			return false;
		}
		index	= cmbDot11eCategory.getSelectionIndex();
		if(index != aclInfo.get80211eCategoryIndex()) {
			return false;
		}
	
		return true;
	}

	/**
	 * 
	 */
	private void refreshConfiguration() {
		initalizeLocalData();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IStateEventHandler#notifyStateChanged(int)
	 */
	public void notifyStateChanged(int state) {
		sMananger.triggerState(state);  
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IStateEventHandler#stateFunction(int)
	 */
	public int stateFunction(int state) {
		switch(state){
		
			case STATE_START	:	executeStartState();
									break;
			
			case STATE_NEW		:   createNewACL();
									break;
			
			case STATE_EDIT		:	editACL();
									break;
			
			case STATE_SAVE		:	if(saveACL() == false) {
										return sMananger.getState();
									}
									break;
			
			case STATE_DELETE	:	if(sMananger.getState() == STATE_NEW) {
										notifyStateChanged(STATE_START);
										return sMananger.getState();
									}
									if(delateACL() == false) {
										return sMananger.getState();
									}
									break;
									
			case STATE_SELECT	:	selectACL();
									break;
		}
		return state;
		
	}

	/**
	 * 
	 */
	private void selectACL() {
		
		btnSave.setEnabled(false);
		btnDelete.setEnabled(true);
		btnEdit.setEnabled(true);
		btnNew.setEnabled(true);
		
		enableAllControls(false);
	
	}

	/**
	 * 
	 */
	private void executeStartState() {
		
		btnSave.setEnabled(false);
		btnDelete.setEnabled(false);
		btnEdit.setEnabled(false);
		btnNew.setEnabled(true);
		
		enableAllControls(false);
		lastSelectedAclIndex = -1;
			
	}

	/**
	 * 
	 */
	private boolean delateACL() {
		
		clearAllControls();
		
		btnSave.setEnabled(true);
		btnDelete.setEnabled(false);
		btnEdit.setEnabled(false);
		btnNew.setEnabled(false);
		
		enableAllControls(false);
				
		return deleteACLEntry();
		
	}

	/**
	 * 
	 */
	private boolean deleteACLEntry() {
		
		int			itemIndex;
		String      strKey;
		
		itemIndex	= tblACL.getSelectionIndex();
	
		TableItem	item	= tblACL.getItem(itemIndex);
		strKey				= item.getText(0);
		tblACL.remove(itemIndex);
		deletedAclEntries.add(strKey);
		return true;
	}

	/**
	 * 
	 */
	private void createNewACL() {
		
		if(deletedAclEntries.size() > 0) {
			parent.showMessage("One or more ACL entries are deleted."+SWT.LF+
					"You must save the changes. ",SWT.ICON_INFORMATION|SWT.OK);
			return;
		}
		clearAllControls();
		btnSave.setEnabled(true);
		btnDelete.setEnabled(true);
		btnEdit.setEnabled(false);
		btnNew.setEnabled(false);
		
		enableAllControls(true);
		txtNodeMacAddress.setFocus();
		tblACL.deselectAll();
		lastSelectedAclIndex	= -1;
		/* following line is for the dependencie of btnDot11eEnabled on the selected vlan
		 * and for the dependencies of btnDot11eEnabled on cmbDot11eCategory
		 * and btnDot11eEditCategory
		 */
		cmbDot11eCategory.setEnabled(false);
		
	}

	/**
	 * 
	 */
	private boolean saveACL() {
		
		boolean aclSaved;
		boolean aclDeleted;
		
		aclSaved	= false;
		aclDeleted	= false;
		
		if(deletedAclEntries.size() > 0){
			aclDeleted	= deleteACL();
			if(aclDeleted == false) {
				return false;
			}
		}else {
			aclSaved	= addACLEntry();
			if(aclSaved == false) {
				return false;
			}
		}
		if(aclSaved == true){
			btnSave.setEnabled(false);
			btnDelete.setEnabled(true);
			btnEdit.setEnabled(true);
			btnNew.setEnabled(true);
			enableAllControls(false);
		}else if(aclDeleted == true) {
			btnSave.setEnabled(false);
			btnDelete.setEnabled(false);
			btnEdit.setEnabled(false);
			btnNew.setEnabled(true);
			enableAllControls(false);
		}
		if(tblACL.getItemCount() > 0){
			btnBlockAll.setEnabled(true);
			btnBlockAllHandler();
		} else {
			btnBlockAll.setEnabled(false);
			btnBlockAll.setSelection(false);
			btnBlockAllHandler();
		}
		return true;
	}
	
	/**
	 * 
	 */
	private boolean deleteACL() {
		String aclName;
		
		IConfiguration	iConfiguration	= parent.getConfiguration();
		if(iConfiguration == null){
			return false;
		}
		
		IACLConfiguration	aclConfig	= iConfiguration.getACLConfiguration();
		if(aclConfig == null) {
			return false;
		}
		
		for(int i=0;i<deletedAclEntries.size();i++) {
			aclName	= (String) deletedAclEntries.get(i);
			IACLInfo	aclInfo = (IACLInfo) tblACL.getData(aclName);
			if(aclInfo == null) {
				continue;
			}
			aclConfig.deleteAclInfo(aclInfo);
			
		}
		deletedAclEntries.removeAllElements();
		lastSelectedAclIndex	= -1;
		return true;
	}

	/**
	 * 
	 */
	private boolean addACLEntry() {
		
		if(validateLocalData() == false) {
			return false;
		}
		
		getDataFromUIToLocalConfiguration();
		return true;
	}

	/**
	 * 
	 */
	private void editACL() {

		btnSave.setEnabled(true);
		btnDelete.setEnabled(false);
		btnEdit.setEnabled(false);
		btnNew.setEnabled(false);
		
		enableAllControls(true);
		enableDot11eControlsforVlans();
		
		txtNodeMacAddress.setFocus();
	}
	
	private void clearAllControls() {
		
		txtNodeMacAddress.setText("");
		cmbNames.select(0);
		btnAllow.setSelection(true);
		btnBlock.setSelection(false);
		if(tblACL.getItemCount() <= 0) {
			btnBlockAll.setSelection(false);
			btnBlockAll.setEnabled(false);
		}else {
			btnBlockAll.setEnabled(true);
		}
	
	  	btnDot11eEnabled.setSelection(false);
    	cmbDot11eCategory.select(0);
 		
	}

	private void enableAllControls(boolean enable) {
		
		btnAllow.setEnabled(enable);
		btnBlock.setEnabled(enable);
		txtNodeMacAddress.setEnabled(enable);
		cmbNames.setEnabled(enable);
		cmbDot11eCategory.setEnabled(enable);
		btnDot11eEnabled.setEnabled(enable);
		if(enable == true){
			if(btnBlock.getSelection() == true) {
				cmbNames.setEnabled(false);
			}
		} else {
			cmbNames.setEnabled(enable);
		}
		
	}
	
	/* following function is for the dependencie of btnDot11eEnabled on the selected vlan
	 * and for the dependencies of btnDot11eEnabled on cmbDot11eCategory
	 * and btnDot11eEditCategory
	 */
	public void enableDot11eControlsforVlans() {
		
		int index;
		
		index	 		= tblACL.getSelectionIndex();
		TableItem item	= tblACL.getItem(index);
		if(item.getText(1).equalsIgnoreCase(DEFAULT_TEXT)) {
			btnDot11eEnabled.setEnabled(true);
		}else {
			btnDot11eEnabled.setEnabled(false);
		}
		if(btnDot11eEnabled.getSelection() == true) {
			cmbDot11eCategory.setEnabled(true);
		}else {
			cmbDot11eCategory.setEnabled(false);
		}

	}
	
	public boolean validateLocalData() {
		
		if(sMananger.getState() == STATE_START			|| 
				sMananger.getState() == STATE_DELETE 	||
				sMananger.getState() == STATE_SELECT	||
				sMananger.getState() == STATE_SAVE){
			return true;
		}
		
		String txtMacAddress;
		
		txtMacAddress	= txtNodeMacAddress.getText().trim();
		
		if(txtMacAddress.equalsIgnoreCase("")){
			parent.showMessage("Mac Address not entered.",SWT.ICON_ERROR);
			txtNodeMacAddress.setFocus();
			return false;
		}
		
		if(MeshValidations.isMacAddress(txtMacAddress) == false){
			parent.showMessage("Invalid Mac Address.",SWT.ICON_ERROR);
			txtNodeMacAddress.setFocus();
			return false;
		}
		
		MacAddress macAddress = new MacAddress();
		if(macAddress.setBytes(txtMacAddress) > 0) {
			parent.showMessage("Invalid Mac Address.",SWT.ICON_ERROR);
			txtNodeMacAddress.setFocus();
			return false;
		}
		
		if(macAddress.equals(MacAddress.resetAddress) || macAddress.equals(MacAddress.undefinedAddress)){
			parent.showMessage("Invalid Mac Address.",SWT.ICON_ERROR);
			txtNodeMacAddress.setFocus();
			return false;
		}
		
		if(chkDuplicates() == false) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * @return
	 */
	private boolean chkDuplicates() {

		int 	aclIndex;
		int 	index;
		String	staAddr;
		String	strKey;
		
		staAddr	=	txtNodeMacAddress.getText().trim();
		
		index		= cmbNames.getSelectionIndex();

		IConfiguration		iConfiguration		= parent.getConfiguration();
		if(iConfiguration == null){
			return false;
		}
		
		aclIndex	= tblACL.getSelectionIndex();
		
		for(index = 0; index < tblACL.getItemCount(); index++) {
			
			if(aclIndex == index){
				continue;
			}
			
			TableItem   item 	= tblACL.getItem(index);
			strKey				= item.getText(0);
			IACLInfo	aclInfo	= (IACLInfo)tblACL.getData(strKey);
			if(aclInfo == null){
				continue;
			}
			if(aclInfo.getStaAddress().toString().equals(staAddr)){
				parent.showMessage("Duplicate ACL entry.",SWT.ICON_ERROR);
				txtNodeMacAddress.setFocus();
				return false;
			}
			
		}
		return true;
	}

	public void setModified() {
	}
	
	public boolean getDataFromUIToConfiguration() {
		
		if(	sMananger.getState() == STATE_START ||
				sMananger.getState() == STATE_SELECT||
				sMananger.getState() == STATE_SAVE) {
			return true;
		}
	
		notifyStateChanged(STATE_SAVE);
		return true;
	}

	/**
	 * 
	 */
	public boolean getDataFromUIToLocalConfiguration() {
			
		int 		itemIndex;
		int			index;
		int 		vlanTag ;
		short 		selection;
		IACLInfo	aclInfo;
		String 		staAddr;
		String 		vlanName;
		String      strKey;
		
		if(deletedAclEntries.size() > 0) {
			return deleteACL();
		}
		
		IConfiguration	iConfiguration	= parent.getConfiguration();
		if(iConfiguration == null) {
			return false;
		}
		
		IACLConfiguration	aclConfig   = iConfiguration.getACLConfiguration();
		if(aclConfig == null) {
			return false;
		}
		
		itemIndex	= tblACL.getSelectionIndex();
		if(itemIndex != -1) {	//if existing acl saved
			
			TableItem   item 	= tblACL.getItem(itemIndex);
			staAddr				= item.getText(0);
			aclInfo				= (IACLInfo)tblACL.getData(staAddr);
			staAddr				= txtNodeMacAddress.getText().trim();
			MacAddress	staMac	= new MacAddress();
			staMac.setBytes(staAddr);
			aclInfo.setStaAddress(staMac);
		}else {
			staAddr				= txtNodeMacAddress.getText().trim();
			MacAddress	staMac	= new MacAddress();
			staMac.setBytes(staAddr);
			aclInfo = (IACLInfo)aclConfig.addAclInfo(staMac); 
		}
		
		if(aclInfo == null) {
			return false; 
		}
		
		index		= cmbNames.getSelectionIndex();
		vlanName	= cmbNames.getItem(index);
				
		if(vlanName.equalsIgnoreCase(DEFAULT_TEXT)) {
			vlanTag = Integer.parseInt((String)cmbNames.getData(DEFAULT_TEXT));
		}else {
			vlanTag	= Integer.parseInt((String)cmbNames.getData(vlanName));	
		}
		aclInfo.setVLANTag(vlanTag);
		
		if(btnDot11eEnabled.getSelection() == true){
			selection	= 1;
		}else {
			selection	= 0;
		}
		aclInfo.setEnable80211eCategory(selection);
		
		index	= cmbDot11eCategory.getSelectionIndex();
		aclInfo.set80211eCategoryIndex((short)index);
		
		if(btnAllow.getSelection() == true){
			selection	= 1;
		}else if(btnBlock.getSelection() == true){
			selection	= 0;
		}
		aclInfo.setAllow(selection);
		
		TableItem	item;
		if(itemIndex != -1) {   // if existing vlan saved
			item					= tblACL.getItem(itemIndex);
			lastSelectedAclIndex	= itemIndex;
			tblACL.select(itemIndex);
		}else {			// if new acl is created
			item	= new TableItem(tblACL,SWT.NONE);
			tblACL.select(tblACL.getItemCount()-1);
			lastSelectedAclIndex	= tblACL.getItemCount()-1;
		}
		item.setText(0,staAddr);
		item.setText(1,vlanName);
		
		if(btnAllow.getSelection() == true) {
			item.setText(2,"Allow");
		} else if(btnBlock.getSelection() == true) {
			item.setText(2,"Block");
		}
						
		strKey				=  item.getText(0);
		tblACL.setData(strKey, aclInfo);
		notifyStateChanged(STATE_SELECT);
		return true; 
	
	}
}
