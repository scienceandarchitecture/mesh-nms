
package com.meshdynamics.nmsui.dialogs.settings;

import java.util.Enumeration;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.HardwareInfo;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;

public class PreferredParentDialog extends MeshDynamicsDialog{

	private Table				tblLoggingAp;
	
	private IConfiguration		configuration;   
	private MeshNetwork			meshNetwork;
	private String				preferredParent;
	
	public PreferredParentDialog(IConfiguration configuration, MeshNetwork	meshNetwork){
		super();
		this.configuration	= configuration;
		this.meshNetwork	= meshNetwork;
		preferredParent		= "";
		setTitle("Preferred Parent");
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.NewMeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	=	400;
		bounds.height	=	230;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.NewMeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	protected void createControls(Canvas mainCnv) {
	 	
		Group	grp = new Group(mainCnv,SWT.NO);
        grp.setBounds(5,5,400,220);
       
        Label label = new Label(grp,SWT.NO);
        label.setText("Interfaces");
        label.setBounds(20,15,80,20);
         
        tblLoggingAp = new Table(grp,SWT.SIMPLE|SWT.BORDER|SWT.FULL_SELECTION|SWT.READ_ONLY|SWT.V_SCROLL);
        tblLoggingAp.setBounds(10,35,380,160);
        tblLoggingAp.setHeaderVisible(true);
        tblLoggingAp.setLinesVisible(true);
        tblLoggingAp.addMouseListener(new MouseAdapter() {

			public void mouseDoubleClick(MouseEvent arg0) {
				onOk();
				}
			});
                
        TableColumn	column  = new TableColumn(tblLoggingAp, SWT.LEFT, 0);
        column.setText("Downlink Address");
        column.setWidth(130);
        
        column  = new TableColumn(tblLoggingAp, SWT.LEFT, 1);
        column.setText("Interface Name");
        column.setWidth(115);
        
        column  = new TableColumn(tblLoggingAp, SWT.LEFT, 2);
        column.setText("Node Mac Address");
        column.setWidth(130);  
        
        setData();
	}

	private void setData() {
    	
		int 	subtype = 0;
		int 	type 	= 0;
		int 	i		= 0;
	
		IInterfaceConfiguration interfaceConfiguration	= configuration.getInterfaceConfiguration();
		
		if(interfaceConfiguration == null) {
			return;
		}
		
		Enumeration<AccessPoint>	apEnumeration	= meshNetwork.getAccessPoints();
    	
		/*to check root node which is selected node comment by nag*/
    	while(apEnumeration.hasMoreElements()){
           
    		AccessPoint ap	= (AccessPoint)apEnumeration.nextElement();
            if (ap.getDsMacAddress().toString().equals(configuration.getDSMacAddress().toString())) {
            	
            	if(ap.isRoot()){
            		return;
            	}
            }
    	}
    	/*to check root node which is selected node comment by nag*/
		
		int 	ifCount	= interfaceConfiguration.getInterfaceCount();
		
    	for(i = 0; i < ifCount; i++) {
    		
    		IInterfaceInfo interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(i);
    		if(interfaceInfo == null) {
    			continue;
    		}
    		
    		if(interfaceInfo.getMediumType() == Mesh.PHY_TYPE_802_3) {
    			continue;
    		}
    		
    		if(interfaceInfo.getMediumType() == Mesh.PHY_TYPE_802_11 	&& 
    		   interfaceInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_WM) {
    			
    			subtype = interfaceInfo.getMediumSubType();
				type 	= interfaceInfo.getMediumType();
				break;
    		}
    	}
    	
    	if(subtype == Mesh.PHY_SUB_TYPE_INGNORE) {
    		return;
    	}
    	Enumeration<AccessPoint>	apEnum	= meshNetwork.getAccessPoints();
    	
    	while(apEnum.hasMoreElements()){
           
    		AccessPoint ap	= (AccessPoint)apEnum.nextElement();
            if (ap.getDsMacAddress().toString().equals(configuration.getDSMacAddress().toString())) {
            	continue;
            }
            
            HardwareInfo hwInfo = ap.getHardwareInfo();
            if(hwInfo == null)
            	continue;
           
            HardwareInfo.InterfaceData[] data = hwInfo.getWmInterfaceInfo();            
            
            if(ap.getStatus() == AccessPoint.STATUS_ALIVE) {
            	
				for(i=0;i<ap.getHardwareInfo().getWmCount();i++) {
					
					if(data[i].getIfType() == type) {
					
						if(data[i].getIfSubType() == subtype ) {						
						
							TableItem itm = new TableItem(tblLoggingAp,SWT.SIMPLE);
							itm.setText(0,data[i].getMacAddr().toString());
							itm.setText(1,data[i].getName());
							itm.setText(2,ap.getDsMacAddress().toString());					
							
						} else if((data[i].getIfSubType()  == Mesh.PHY_SUB_TYPE_802_11_G ||data[i].getIfSubType()  == Mesh.PHY_SUB_TYPE_802_11_B || data[i].getIfSubType()  == Mesh.PHY_SUB_TYPE_802_11_B_G) &&
								  (subtype  == Mesh.PHY_SUB_TYPE_802_11_G ||subtype  == Mesh.PHY_SUB_TYPE_802_11_B || subtype  == Mesh.PHY_SUB_TYPE_802_11_B_G)) {
							
							TableItem itm = new TableItem(tblLoggingAp,SWT.SIMPLE);
							itm.setText(0,data[i].getMacAddr().toString());
							itm.setText(1,data[i].getName());
							itm.setText(2,ap.getDsMacAddress().toString());
						}else if((data[i].getIfSubType() == Mesh.PHY_SUB_TYPE_802_11_A || data[i].getIfSubType() == Mesh.PHY_SUB_TYPE_802_11_AN || data[i].getIfSubType() == Mesh.PHY_SUB_TYPE_802_11_AC || data[i].getIfSubType() == Mesh.PHY_SUB_TYPE_802_11_ANAC) && (subtype == Mesh.PHY_SUB_TYPE_802_11_A || subtype == Mesh.PHY_SUB_TYPE_802_11_AN || subtype == Mesh.PHY_SUB_TYPE_802_11_AC || subtype == Mesh.PHY_SUB_TYPE_802_11_ANAC)){
							TableItem itm = new TableItem(tblLoggingAp,SWT.SIMPLE);
							itm.setText(0,data[i].getMacAddr().toString());
							itm.setText(1,data[i].getName());
							itm.setText(2,ap.getDsMacAddress().toString());
						}
					}
				}
            }        	
        }
    }       
       
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.NewMeshDynamicsDialog#onOk()
	 */
	protected boolean onOk() {
		
		if(tblLoggingAp.getSelectionIndex() == -1) {
    		MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.OK);
    		msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
    		msg.setMessage("AccessPoint not selected." );
    		msg.open();
    		return false;
    	}
            
    	if(tblLoggingAp.getSelectionIndex() < 0)
    		return false;
    	TableItem item 		= tblLoggingAp.getItem(tblLoggingAp.getSelectionIndex());
    	preferredParent		= item.getText(0).trim();
        return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.NewMeshDynamicsDialog#onCancel()
	 */
	protected boolean onCancel() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.NewMeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.NewMeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @return
	 */
	public String getPrefParent() {
		return preferredParent;
	}

}
