package com.meshdynamics.nmsui.dialogs.settings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.configuration.IACLConfiguration;
import com.meshdynamics.meshviewer.configuration.IACLInfo;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.Configuration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationTextWriter;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigPage;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.MacAddress;

public class ConfigurationDialog extends MeshDynamicsDialog implements IMeshConfigBook {

	private int 				configMode;
	private int 				currentSelectedPage;
	private int 				lastSelectedPage;
	
	private Configuration		localConfig;
	private IConfiguration		originalConfig;
	private	IMeshConfigPage[]	pages;
	private CTabFolder			tabFolder;
	private Rectangle			minimumBounds;
	private	MeshNetwork			meshNetwork;
	private IVersionInfo 		versionInfo;
	
	private  WPAPersonalKeyRegenerationStatus	keyStatus;
	private  Vector<String>      vector;
	
	
	private static final int	GENERAL_PAGE_INDEX		=  0;
	private static final int	INTERFACE_PAGE_INDEX	=  1;
	private static final int	SECURITY_PAGE_INDEX		=  2;
	private static final int	VLAN_PAGE_INDEX			=  3;
	private static final int	ACL_PAGE_INDEX			=  4;
	
	private boolean				rfChanged;			// Flag is used to enable/disable ui controls
	private boolean				countryCodeChanged; // Flag is used to enable/disable ui controls

	private void initPages() {
		
		pages								= new IMeshConfigPage[5];
		pages[GENERAL_PAGE_INDEX]			= new GeneralConfigPage(this);
		pages[INTERFACE_PAGE_INDEX]			= new InterfaceConfigPage(this);
		pages[SECURITY_PAGE_INDEX]			= new SecurityConfigPage(this);
		pages[VLAN_PAGE_INDEX]				= new VLANConfigPage(this);
		pages[ACL_PAGE_INDEX]				= new ACLConfigPage(this);
	
		pages[GENERAL_PAGE_INDEX]	.setCaption("General"); //$NON-NLS-1$
		pages[INTERFACE_PAGE_INDEX]	.setCaption("InterfaceSettings"); //$NON-NLS-1$
		pages[SECURITY_PAGE_INDEX]	.setCaption("Security"); //$NON-NLS-1$
		pages[VLAN_PAGE_INDEX]		.setCaption("VLAN"); //$NON-NLS-1$
		pages[ACL_PAGE_INDEX]		.setCaption("ACL"); //$NON-NLS-1$
	
		currentSelectedPage	= 0;
		lastSelectedPage	= 0;
	}
	
	public ConfigurationDialog(int configMode,IConfiguration config, MeshNetwork meshNetwork, IVersionInfo	versionInfo) {
		
		this.configMode			= configMode;
		this.localConfig		= new Configuration();
		this.originalConfig		= config;
		this.meshNetwork		= meshNetwork;
		this.versionInfo		= versionInfo;
		this.keyStatus			= new WPAPersonalKeyRegenerationStatus();
		this.vector            =new Vector<String>();
		
		originalConfig.copyTo(localConfig);
		
		super.setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK_CANCEL);
		super.showSaveAsButton(false);
			
		rfChanged					 = false; 
		countryCodeChanged			 = false;
		
		initPages();
	}
	
	protected void createControls(Canvas mainCnv) {
		
		tabFolder             = new CTabFolder(mainCnv,SWT.BORDER);
		
		tabFolder.setSelectionForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		
		Display display = Display.getCurrent();
		
		tabFolder.setSelectionBackground(new Color[]{display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND), 
                					  display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND),
									  display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT), 
									  display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT)},
									  new int[] {40, 70, 100}, true);
		
		tabFolder.setBounds(5, 5, minimumBounds.width, minimumBounds.height);
		
		tabFolder.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
					
				pages[currentSelectedPage].selectionChanged(false);
				lastSelectedPage	= currentSelectedPage;
				currentSelectedPage = tabFolder.getSelectionIndex();
				if(currentSelectedPage!= lastSelectedPage) {
					pages[currentSelectedPage].selectionChanged(true);
				}
			}
			
		});
		
		for(int i=0;i<pages.length;i++) {
			pages[i].createControls(tabFolder);
		}
		
		initalizeLocalData();
		
		super.setOkButtonText("Update"); //$NON-NLS-1$
		
	}
	
	public void initalizeLocalData() {
		for(int i=0;i<pages.length;i++) {
			pages[i].initalizeLocalData();
		}
	}

	protected boolean onCancel() {
		for(int i=0;i<pages.length;i++) {
			pages[i].onCancel();
		}
		return true;
	}

	protected boolean onOk() {
		int pageIndex;
		int selectedPageNo;
		
		selectedPageNo	= tabFolder.getSelectionIndex();
		
		if(selectedPageNo > pages.length){
			return false;
		}
		if(pages[selectedPageNo].onOk() == false) {
			return false;
		}
		if(keyStatus.isEntryPresent() == true) {
			showErrorMessage("Essid changed for " + keyStatus.getInterfaceList() + "." +SWT.LF+ //$NON-NLS-1$ //$NON-NLS-2$
					"you must regenerate WPA Personal security key for the same ",SWT.OK|SWT.ICON_INFORMATION); //$NON-NLS-1$
			return false;
		}
		if(vector.size()> 0) {
			showErrorMessage("DCA List channel changes will take effect after reboot ",SWT.ICON_INFORMATION);
			}
		if(selectedPageNo == GENERAL_PAGE_INDEX) {
			boolean parentFlag = localConfig.getPreferedParent().equals(originalConfig.getPreferedParent());
			boolean hostFlag = localConfig.getNetworkConfiguration().getHostName().equals(originalConfig.getNetworkConfiguration().getHostName());
			if(parentFlag == false && hostFlag == false){
				showErrorMessage("Preferred parent and HostName changes will take effect after reboot ",SWT.ICON_INFORMATION); //$NON-NLS-1$
			}else if(parentFlag == false){
				showErrorMessage("Preferred parent changes will take effect after reboot ",SWT.ICON_INFORMATION); //$NON-NLS-1$
			}else if(hostFlag == false){
				showErrorMessage("HostName changes will take effect after reboot ",SWT.ICON_INFORMATION); //$NON-NLS-1$
			}
		}
				
		if(localConfig.isFipsEnabled() == false) {
			return true;
		}
		
		for(pageIndex = 0; pageIndex < pages.length; pageIndex++){
			if(pages[pageIndex].isFipsFulfilled() == false){
				return false;
			}
		}
			
		return true;
	}

	public IConfiguration getConfiguration() {
		return localConfig;
	}

	public int getConfigMode() {
		return configMode;
	}
	
	protected void modifyMinimumBounds(Rectangle bounds) {
		
		minimumBounds 			= new Rectangle(0,0,0,0);
		Rectangle pageBounds 	= null;
		for(int i=0;i<pages.length;i++) {
			
			pageBounds = pages[i].getMinimumBounds();
			if(pageBounds == null)
				continue;
		
			minimumBounds.width = (pageBounds.width > minimumBounds.width) ?
									pageBounds.width : minimumBounds.width;
			
			minimumBounds.height = (pageBounds.height > minimumBounds.height) ?
									pageBounds.height : minimumBounds.height;
			
		}

		minimumBounds.width 	+= 15;
		minimumBounds.height 	+= 15;

		bounds.width = (bounds.width > minimumBounds.width) ?
						bounds.width : minimumBounds.width;

		bounds.height = (bounds.height > minimumBounds.height) ?
						bounds.height : minimumBounds.height;
		
	}

	public void dataModified() {
		super.setIsDirty(true);
	}
	
	protected boolean onExport() {
		return true;
	}

	protected boolean onImport() {
		return true;
	}

	public int showMessage(String message, int swtIcon) {
		
		Shell shell = new Shell(Display.getDefault());
		MessageBox messageBox = new MessageBox(shell,SWT.MULTI|swtIcon);
		messageBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor); //$NON-NLS-1$ //$NON-NLS-2$
		messageBox.setMessage(message);
		return messageBox.open();
	}

	protected boolean onApply() {
		
		int pageNumber	=	tabFolder.getSelectionIndex();
		
		if(pages[pageNumber].validateLocalData() == false) {
			return false;
		}
		if(pages[pageNumber].getDataFromUIToConfiguration() == false) {
			return false;
		}
		return true;
	}

	public boolean canDeleteVlan(int tag) {
		
		int aclCount;
		int index;

		IACLConfiguration	aclConfig	= localConfig.getACLConfiguration();
		if(aclConfig == null){
			return false;
		}
		aclCount	= aclConfig.getCount();
		
		for(index = 0; index < aclCount; index++) {
			IACLInfo	aclInfo	= (IACLInfo)aclConfig.getACLInfo(index);
			if(aclInfo == null){
				continue;
			}
			if(aclInfo.getVLANTag() == tag){
				showMessage("This VLAN is used for one or more of the ACL configuration. " + //$NON-NLS-1$
						SWT.LF + "You cannot delete this VLAN. ", SWT.OK|SWT.ICON_INFORMATION); //$NON-NLS-1$
				
				return false;
			}
		}
		
		return true;
	}

	public boolean isRfConfigChanged(){
		return rfChanged;
	}
	
	public void setRfConfigChanged(boolean enable) {
		rfChanged	= enable;
	}
	
	public boolean isCountryCodeChanged(){
		return countryCodeChanged;
	}
	
	public void setCountryConfigChanged(boolean enable) {
		countryCodeChanged	= enable;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#getMeshNetWork()
	 */
	public MeshNetwork getMeshNetWork() {
		return meshNetwork;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#getIfEssIdByName(java.lang.String)
	 */
	public String getIfEssIdByName(String ifName) {
		IInterfaceConfiguration	ifConfig	= localConfig.getInterfaceConfiguration();
		if(ifConfig == null) {
			return ""; //$NON-NLS-1$
		}
		IInterfaceInfo			ifInfo		= (IInterfaceInfo)ifConfig.getInterfaceByName(ifName);
		if(ifInfo == null) {
			return ""; //$NON-NLS-1$
		}
		return ifInfo.getEssid();
		
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#getInterfaceinfoByName
	 */
	public IInterfaceInfo getInterfaceInfoByName(String ifName) {
		IInterfaceConfiguration	ifConfig	= localConfig.getInterfaceConfiguration();
		if(ifConfig == null) {
			return null; //$NON-NLS-1$
		}
		IInterfaceInfo			ifInfo		= (IInterfaceInfo)ifConfig.getInterfaceByName(ifName);
		if(ifInfo == null) {
			return null; //$NON-NLS-1$
		}
		return ifInfo;
		
	}

/*	
	private void disableRadDecidesVlanForAllInterfaces(){
		
		int index;
		int interfaceCount;
		
		IInterfaceConfiguration	ifConfig	= localConfig.getInterfaceConfiguration();
		if(ifConfig == null) {
			return;
		}
		interfaceCount	= ifConfig.getInterfaceCount();
		
		for(index = 0; index < interfaceCount; index++) {
			IInterfaceInfo	ifInfo	= (IInterfaceInfo)ifConfig.getInterfaceByIndex(index);
			if(ifInfo == null) {
				continue;
			}
			ISecurityConfiguration	securityConfig = (ISecurityConfiguration)ifInfo.getSecurityConfiguration();
			if(securityConfig == null) {
				continue;
			}
			IRadiusConfiguration	radiusConfig	= (IRadiusConfiguration)securityConfig.getRadiusConfiguration();
			if(radiusConfig == null) {
				continue;
			}
			radiusConfig.setWPAEnterpriseDecidesVlan((short)0);
		}
	}
*/
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#getVersionInfo()
	 */
	public IVersionInfo getVersionInfo() {
		return versionInfo;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onSaveAs()
	 */
	protected boolean onSaveAs() {
		if(onOk()) {
			if(writeConfigToFile() == true) {
				return true;
			}
		}
		return false;
	}
	
	 private boolean writeConfigToFile() {
		
    	File 		file			 = null;
		String 		fileName 		 = ""; //$NON-NLS-1$
		boolean 	overWrite 		 = true;
		FileDialog 	fd				 = new FileDialog(new Shell(), SWT.SAVE | SWT.CANCEL);
		
		fd.setText("NetworkViewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor); //$NON-NLS-1$ //$NON-NLS-2$
		
		while(overWrite){
		
			String mac	= getConfigurationFileName(localConfig.getDSMacAddress());
			fd.setFilterPath(MFile.getNodeFilePath());
			fd.setFileName(mac.substring(mac.length() - 22));
			fileName 	= fd.open();
			if (fileName == null)
				return false;
			
			int len	= fileName.length();  		
			if(!fileName.substring((len-5),len).equals(".conf")) //$NON-NLS-1$
				fileName = fileName + ".conf"; //$NON-NLS-1$
			
			file = new File(fileName);
			if(file.exists() == true) {

				MessageBox msg = new MessageBox(new Shell(), SWT.YES | SWT.NO | SWT.ICON_INFORMATION);
				msg.setText("NetworkViewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor); //$NON-NLS-1$ //$NON-NLS-2$
				msg.setMessage("Existing values will be overwritten " + SWT.LF + SWT.LF + "Do you want to continue ?"); //$NON-NLS-1$ //$NON-NLS-2$

				int ans = msg.open();
				if (ans == SWT.YES) {
					overWrite = false;
				}
			}
			else {
				overWrite = false;
			}
		}
			
		if (file.exists() == false) {
			try {
				file.createNewFile();
			} catch (Exception e) {
				return false;
			}
		}

		if (file.canWrite() == false) {
			MessageBox msgBx = new MessageBox(new Shell(SWT.CLOSE), SWT.OK | SWT.ICON_INFORMATION);
			msgBx.setText("NetworkViewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor); //$NON-NLS-1$ //$NON-NLS-2$
			msgBx.setMessage("Save Failed Templates are ReadOnly"); //$NON-NLS-1$
			msgBx.open();
			return false;
		}
		
		try {
			FileOutputStream 		fileStream   = new FileOutputStream(file);
			ConfigurationTextWriter configWriter = new ConfigurationTextWriter(fileStream);
			localConfig.save(configWriter);
			configWriter.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		MessageBox msgBx = new MessageBox(new Shell(SWT.CLOSE), SWT.OK | SWT.ICON_INFORMATION);
		msgBx.setText("NetworkViewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor); //$NON-NLS-1$ //$NON-NLS-2$
		msgBx.setMessage("FileSaved"); //$NON-NLS-1$
		msgBx.open();
		return true;
	}
	 
	 public String getConfigurationFileName(MacAddress macAddress) {
		String fileName = MFile.getNodeFilePath() + "/" +macAddress.toString().replace(':','_'); //$NON-NLS-1$
		fileName += ".conf"; //$NON-NLS-1$
		return fileName;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#essIdChangedForInterface(java.lang.String, java.lang.String)
	 */
	public void essIdChangedForInterface(String ifName) {
		if(localConfig.isFipsEnabled() == false) {
			checkInterfaceForWpaPersonalSecurity(ifName);
		}
	}
	private void checkInterfaceForWpaPersonalSecurity(String name) {
		
		ISecurityConfiguration	secConfig;
				
		IInterfaceConfiguration	interfaceConfig	= localConfig.getInterfaceConfiguration();
		if(interfaceConfig == null) {
			return;
		}
		
		IInterfaceInfo	ifInfo = (IInterfaceInfo)interfaceConfig.getInterfaceByName(name);
		if(ifInfo == null) {
			return;
		}
			
		secConfig	= (ISecurityConfiguration)ifInfo.getSecurityConfiguration();
		if(secConfig == null) {
			return;
		}
		
		if(secConfig.getEnabledSecurity() == ISecurityConfiguration.SECURITY_TYPE_WPA_PERSONAL) {
			keyStatus.addEntry(name);
			//showErrorMessage("EssId changed for interface " + name+ "."+SWT.LF+
			//				"You must regenerate WPA Personal security key for this interface.",SWT.OK|SWT.ICON_INFORMATION);
			
			//return;
		}
	
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#notifyWPAPersonalKeyRegenerated()
	 */
	public void notifyWPAPersonalKeyRegeneratedFor(String ifName) {
	   keyStatus.removeEntry(ifName);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#notifySecurityConfigurationChangedFor(java.lang.String)
	 */
	public void notifySecurityConfigurationChangedFor(String ifName) {
		keyStatus.removeEntry(ifName);
	}
	
	@Override
	protected boolean isModal() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void notifyDCAListChanged(String ifName) {
		vector.add(ifName);		
	}

	@Override
	public void notifyDCAListRemove(String ifName) {
		vector.remove(ifName);
	}
}
