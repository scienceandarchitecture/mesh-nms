/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : AboutDialog.java
 * Comments : About Dialog for MeshViewer
 * Created  : Oct 8, 2004
 * Author   : Sneha Puranam
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  9  |May 3, 2007  | Constructor modified                            |Abhishek|
 * --------------------------------------------------------------------------------
 * |  8  |May 3, 2007  | Setdata modified dur to new configuration       |Abhishek|
 * --------------------------------------------------------------------------------
 * |  7  |May 1, 2007  | Changes due to MeshDynamicsDialog               |Abhishek|
 * --------------------------------------------------------------------------------
 * |  6  | Mar 16, 2007| Bounds for dialog box changed                   |Abhishek|
 * --------------------------------------------------------------------------------
 * |  5  | Nov 27, 2006| clean-up					  			         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  4  |Oct 26, 2006 | Fixes for CCK/OFDM for 2.4G & 5G                | Bindu  |
 * --------------------------------------------------------------------------------
 * |  3  |Oct 13, 2006 | Changes for CCk/OFDM for 2.4G & 5G              | Bindu  |
 * --------------------------------------------------------------------------------
 * |  2  |Jun 12, 2006 | UI changes for uncategorised                    | Bindu  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 21, 2006 | Check boxes removed	                         | Mithil |
 * --------------------------------------------------------------------------------
 * |  0  |Dec 20, 2005 | Created                                         | Amit   |
 * --------------------------------------------------------------------------------
**********************************************************************************/
 

package com.meshdynamics.nmsui.dialogs.settings;


import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.configuration.IChannelInfo;
import com.meshdynamics.meshviewer.configuration.IInterfaceChannelConfiguration;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;

/**
 * @author sneha
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class IfChannelInfoViewdlg extends MeshDynamicsDialog {
	
	private static final int CHANNEL_CCK 				= 0x0020;
	private static final int CHANNEL_OFDM 				= 0x0040;
	private static final int CHANNEL_2GHZ 				= 0x0080;
	private static final int CHANNEL_5GHZ 				= 0x0100;

	private static final int COLUMN_CHANNEL_NUMBER 		= 0;
	private static final int COLUMN_CHANNEL_FREQUENCY 	= 1;
	private static final int COLUMN_MAX_REG_POWER	 	= 2;
	private static final int COLUMN_MAX_POWER	 		= 3;
	private static final int COLUMN_MIN_POWER	 		= 4;
	private static final int COLUMN_MOD_CCK		 		= 5;
	private static final int COLUMN_MOD_OFDM	 		= 6;
	private static final int COLUMN_FREQ_2GHZ	 		= 7;
	private static final int COLUMN_FREQ_5GHZ	 		= 8;
	
	private Table				tblChannelInfo;
	
	private String				intName;
	private IInterfaceChannelConfiguration supportedChannelconfig;
	
	private static final String[] tableHeaders = {"Channel","Frequency","Max Reg Power","Max Power","Min Power","CCK","OFDM","2GHz","5GHz"};
	
	/**
	 * @param supportConfig
	 */
	public IfChannelInfoViewdlg(IInterfaceChannelConfiguration supportConfig,String interfaceName) {
		super();
		supportedChannelconfig  = supportConfig;
		intName					= interfaceName;	
		setTitle("Channel Info for "+intName);
		setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK);
	}

	
	 /**
	  * 
	 */
	private void setData() {
		
		if(supportedChannelconfig == null){
			return;
		}
		
		int count	= supportedChannelconfig.getChannelInfoCount();
		int i;
		
		for(i = 0 ; i < count; i++){
			
			TableItem item = new TableItem(tblChannelInfo,SWT.NONE);
			IChannelInfo supportChannelInfo	= (IChannelInfo)supportedChannelconfig.getChannelInfo(i);
			if(supportChannelInfo == null) {
				continue;
			}
			item.setText(COLUMN_CHANNEL_NUMBER,		"" + supportChannelInfo.getChannelNumber());
			item.setText(COLUMN_CHANNEL_FREQUENCY,	"" + supportChannelInfo.getFrequency());
			item.setText(COLUMN_MAX_REG_POWER,		"" + supportChannelInfo.getMaxRegPower());
			item.setText(COLUMN_MAX_POWER,			"" + supportChannelInfo.getMaxPower());
			item.setText(COLUMN_MIN_POWER,			"" + supportChannelInfo.getMinPower());
			
			long flag = supportChannelInfo.getFlags();
			if((flag & CHANNEL_CCK) == 0) 
				item.setText(COLUMN_MOD_CCK,"No");
			else 
				item.setText(COLUMN_MOD_CCK,"Yes");
							
			if((flag & CHANNEL_OFDM) == 0)
				item.setText(COLUMN_MOD_OFDM,"No");
			else
				item.setText(COLUMN_MOD_OFDM,"Yes");
			
			if((flag & CHANNEL_2GHZ) == 0)
				item.setText(COLUMN_FREQ_2GHZ,"No");
			else
				item.setText(COLUMN_FREQ_2GHZ,"Yes");
			
			if((flag & CHANNEL_5GHZ) == 0)
				item.setText(COLUMN_FREQ_5GHZ,"No");
			else
				item.setText(COLUMN_FREQ_5GHZ,"Yes");
			}
	}		
	

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 540;	
		bounds.height	= 240;	
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	protected void createControls(Canvas mainCnv) {
		
		tblChannelInfo = new Table(mainCnv,SWT.BORDER|SWT.V_SCROLL|SWT.SINGLE|SWT.FULL_SELECTION);
		tblChannelInfo.setBounds(10,10,530,230);
		tblChannelInfo.setLinesVisible(true);
		tblChannelInfo.setHeaderVisible(true);
		
		TableColumn tableColumn = new TableColumn(tblChannelInfo,SWT.RIGHT);
		tableColumn.setText(tableHeaders[COLUMN_CHANNEL_NUMBER]);
		tableColumn.setWidth(58);
		
		tableColumn = new TableColumn(tblChannelInfo,SWT.RIGHT);
		tableColumn.setText(tableHeaders[COLUMN_CHANNEL_FREQUENCY]);
		tableColumn.setWidth(68);
		
		tableColumn = new TableColumn(tblChannelInfo,SWT.RIGHT);
		tableColumn.setText(tableHeaders[COLUMN_MAX_REG_POWER]);
		tableColumn.setWidth(93);
		
		tableColumn = new TableColumn(tblChannelInfo,SWT.RIGHT);
		tableColumn.setText(tableHeaders[COLUMN_MAX_POWER]);
		tableColumn.setWidth(70);
		
		tableColumn = new TableColumn(tblChannelInfo,SWT.RIGHT);
		tableColumn.setText(tableHeaders[COLUMN_MIN_POWER]);
		tableColumn.setWidth(66);
		
		tableColumn = new TableColumn(tblChannelInfo,SWT.LEFT);
		tableColumn.setText(tableHeaders[COLUMN_MOD_CCK]);
		tableColumn.setWidth(37);
		
		tableColumn = new TableColumn(tblChannelInfo,SWT.LEFT);
		tableColumn.setText(tableHeaders[COLUMN_MOD_OFDM]);
		tableColumn.setWidth(47);
		
		tableColumn = new TableColumn(tblChannelInfo,SWT.LEFT);
		tableColumn.setText(tableHeaders[COLUMN_FREQ_2GHZ]);
		tableColumn.setWidth(43);
		
		tableColumn = new TableColumn(tblChannelInfo,SWT.LEFT);
		tableColumn.setText(tableHeaders[COLUMN_FREQ_5GHZ]);
		tableColumn.setWidth(43);
		
		setData();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	protected boolean onOk() {
		// TODO Auto-generated method stub
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	protected boolean onCancel() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	public static void main(String[] args) {
		
	}
}
