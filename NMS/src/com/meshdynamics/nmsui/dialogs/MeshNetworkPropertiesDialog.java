/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshDynamicsDialog.java
 * Comments : Base Dialog for all dialogs
 * Created  : Sep 29, 2004
 * Author   : Sneha Puranam
 * 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History  
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  23 |May 1, 2007  | Changes due to MeshDynamicsDialog               |Abhishek|
 * --------------------------------------------------------------------------------
 * |  22 |Mar 16, 2007 | changes due to meshdynamics dialog				 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  21 |Mar 15, 2007 | neighbourline info fixed		                 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  20 |Jan 09, 2007 | showHealthMonitorSettings added                 |Abhishek|
 * --------------------------------------------------------------------------------
 * |  19 |Feb  8, 2007 | Button health monitor and its setting added     | Imran  |
 * --------------------------------------------------------------------------------
 * |  18 |Jan 15, 2007 | Degrees symbol added					  	 	 |Abhishek|
 * --------------------------------------------------------------------------------
 * |  17 |Dec 29,2006  |  btnNeighbourLines size increased               | Imran  |
 *  --------------------------------------------------------------------------------
 * |  16 |Dec 28, 2006 | add tristate checkbox for neighbourlines  	 	 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  15 |Nov 10, 2006| Misc Fixes, clean-up					  	 	 |Abhishek|
 * --------------------------------------------------------------------------------
 * |  14 |july 10, 2006|Default Color set						  	 	 |Prachiti|
 * ----------------------------------------------------------------------------------
 * |  13 |Mar 17, 2006 | Version in msg box generalised			  	 	 | Mithil |
 * ----------------------------------------------------------------------------------
 * |  12 |Mar 16, 2006 | Line Color Change			 				     | Mithil |
 * ----------------------------------------------------------------------------------
 * |  11 |Feb 16, 2006 | Grid Color Change			 				     | Mithil |
 * ----------------------------------------------------------------------------------
 * |  10 |Jan 06, 2006 | open file dialog made modal 				     | Mithil |
 * ----------------------------------------------------------------------------------
 * |  9  |Jan 06, 2006 | Table combos made uneditable 				     | Mithil |
 * ----------------------------------------------------------------------------------
 * |  8  |Jan 04, 2006 | Display Text Changes		  				     | Mithil |
 * ----------------------------------------------------------------------------------
 * |  7  |Jan 04, 2006 | Node Display Changes			                 | Mithil |
 *  --------------------------------------------------------------------------------
 * |  6  |Dec 30, 2005 | Downlink Rate changed to Uplink                 | Mithil |
 *  --------------------------------------------------------------------------------
 * |  5  |Oct 20, 2005 | messagbox changes                               | Sneha  |
 *  --------------------------------------------------------------------------------
 * |  4  | Oct 7,2005  | Nodedisplay load values fixed                   | Amit   |
 * --------------------------------------------------------------------------------
 * |  3  |Oct 5, 2005  | Changes in Validatevalues()                     | Sneha  |
 * --------------------------------------------------------------------------------
 * |  2  |Oct 4, 2005 | Removed buttons showProperty and 
 *                      automaticMeshNetworkAddition and nodename combo  | Sneha  |
 * --------------------------------------------------------------------------------
 * |  1  |Sep 25, 2005 | Changes made for version 3.0                    | Abhijit|
 * --------------------------------------------------------------------------------
 * |  0  |Sep 29, 2004 | Created                                         | Sneha  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/
package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.ColorDialog;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.nmsui.controls.CheckBox;
import com.meshdynamics.nmsui.controls.Slider;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.nmsui.mesh.MeshNetworkUIProperties;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.nmsui.util.MeshViewerGDIObjects;

public class MeshNetworkPropertiesDialog extends MeshDynamicsDialog{

	private Label   					lblGridSize;
	private Button						btnShowBackground;
	private Label						lblNodeWindow;
	private Label						lblGridColor;
	private Label						lblLineColor;
	private Label						lblKAPColor;
	private Label						lblLayoutGridSize;
    private Label						lblShow;
	
	private Canvas   					currentGridColor;
	private Canvas   					currentLineColor;
	private Canvas   					currentKAPColor;

	private Slider						sldGridSize;
	
	private Text						txtMapImgPath;
	
	private Button  					btnPath;
	private Button						btnGrid;
	
	private CheckBox					btnNeighbourLines;
	private CheckBox					btnAssociationLines;
    private CheckBox					btnRootLine;
	
	private Combo   					cmbNodeWindow;
	private Combo						cmbLineInfo;
	
	private MeshNetworkUIProperties 		properties;
	
	private Button                      btnEnabelHealthMonitor;  
	private Button   					btnViewHMSettings;
	private Button 						btnGridColor;
	private Button						btnLineColor;
	private Button						btnKAPColor;
	
	//private HealthMonitorSettingsDlg	healthMonitorSettingsDlg;
	
	private Group						grpMapProp, grpLine; 
	private Group						grpNodeSetting;	
	
	
	/**
	 * 
	 */
	public MeshNetworkPropertiesDialog(String meshNetworkName,MeshNetworkUIProperties networkProperties) {		
		super();
		properties 					= 	networkProperties;
		super.setTitle("View Settings for " + meshNetworkName + " network");
	}

	private void showBackgroundImage(boolean show) {
		btnShowBackground.setSelection(show);
		txtMapImgPath.setEnabled(show);
		btnPath.setEnabled(show);
		lblGridColor.setEnabled(show);
		btnGridColor.setEnabled(show);
		lblLineColor.setEnabled(show);
		btnLineColor.setEnabled(show);
		lblKAPColor.setEnabled(show);
		btnKAPColor.setEnabled(show);
	}
	
	private void updateControlsData(){
		
		if(properties == null) {
			return;
		}
/*		
		int temp = properties.getHealthMonitorStatus();
		if(temp == 1){
			btnEnabelHealthMonitor.setSelection(true);
			btnViewHMSettings.setEnabled(true);
			lblGridColor.setEnabled(false);
			btnGridColor.setEnabled(false);
		}
		
		else{
			btnEnabelHealthMonitor.setSelection(false);
			btnViewHMSettings.setEnabled(false);
			lblGridColor.setEnabled(true);
			btnGridColor.setEnabled(true);
		}
*/		
		sldGridSize.setValue(properties.getGridSize());
		lblGridSize.setText("" + sldGridSize.getValue());
		
		btnGrid.setSelection(properties.isShowGrid());

		int showNeighbourlines = properties.getNeighbourLines();
		switch(showNeighbourlines) {
			case MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_ALL:
				btnNeighbourLines.setSelection(true);
				cmbLineInfo.setEnabled(true);
				break;
			case MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_PARTIAL:
				btnNeighbourLines.setTriState(true);
				cmbLineInfo.setEnabled(true);
				break;
			case MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE:
				btnNeighbourLines.setSelection(false);
				cmbLineInfo.setEnabled(false);
				break;
		}
		
		int showAssocLines = properties.getAssociationLines();
		switch(showAssocLines) {
			case MeshNetworkUIProperties.ASSOCIATIONLINE_STATE_HIDE:
				btnAssociationLines.setSelection(false);
				break;
			case MeshNetworkUIProperties.ASSOCIATIONLINE_STATE_SHOW:
				btnAssociationLines.setSelection(true);
				break;
			
		}

        int showRootLine = properties.getRootLine();
        switch(showRootLine) {
            case MeshNetworkUIProperties.ROOTLINE_STATE_HIDE:
                btnRootLine.setSelection(false);
                break;
            case MeshNetworkUIProperties.ROOTLINE_STATE_SHOW:
                btnRootLine.setSelection(true);
                break;

        }

		int neighbourLineInfo	= properties.getNeighbourLineInfo();
	
		cmbLineInfo.select(neighbourLineInfo);
	
		cmbNodeWindow.select(properties.getNodeDisplayValue());

		Color colGrid	= properties.getGridLineColor();
		Color colLine	= properties.getWirelessLinkColor();
		Color colKAP	= properties.getKapLinkColor();
		
		btnGridColor.setData(colGrid);
		btnLineColor.setData(colLine);
		btnKAPColor.setData (colKAP);
		
		currentGridColor.setBackground(colGrid);
		currentLineColor.setBackground(colLine);
		currentKAPColor.setBackground(colKAP);

		String imagePath = properties.getMapImagePath();
		if(imagePath == null || imagePath.length() <= 0)
			showBackgroundImage(false);
		else {
			showBackgroundImage(true);
			txtMapImgPath.setText(imagePath);
		}
		
	}

	
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	public void createControls(Canvas mainCnv) {

		int rel_y = 10;
		int grp_y = 10;
		
		grpNodeSetting	= new Group(mainCnv,SWT.NONE);
		grpNodeSetting.setBounds(10,grp_y,400,50);
		grpNodeSetting.setFont(MeshViewerGDIObjects.boldVerdanaFont8);
		grpNodeSetting.setText("Node Display Settings");
		
		rel_y = 20;
		
		lblNodeWindow = new Label(grpNodeSetting,SWT.NONE);
		lblNodeWindow.setBounds(15,rel_y,120,20);
		lblNodeWindow.setText("Node Display Text");
		
		rel_y -= 1;
		
		cmbNodeWindow = new Combo(grpNodeSetting, SWT.SINGLE|SWT.BORDER|SWT.READ_ONLY);
		cmbNodeWindow.setBounds(170,rel_y,215,20);		
		cmbNodeWindow.add("Model Number");
		cmbNodeWindow.add("User defined Node Name");
		cmbNodeWindow.add("UpLink Transmit Rate(Mbps)");
		cmbNodeWindow.add("Parent Downlink Signal Strength(dBm)");
		cmbNodeWindow.add("Board Temperature(�C)");
		cmbNodeWindow.add("Voltage(V)");
		cmbNodeWindow.select(0);
		
		grp_y += 60;

		grpLine = new Group(mainCnv, SWT.NO);
		grpLine.setBounds(10, grp_y,400, 90);
		grpLine.setFont(MeshViewerGDIObjects.boldVerdanaFont8);
		grpLine.setText("Line properties");
		
		rel_y = 20;

        lblShow = new Label(grpLine,SWT.NONE);
        // Add a 2 px offset to the label to align with the checkbox text
        lblShow.setBounds(15,rel_y+2,35,20);
        lblShow.setText("Show");

        btnNeighbourLines = new CheckBox(grpLine);
		btnNeighbourLines.setBounds(50,rel_y,115,20);
		btnNeighbourLines.setText("Neighbour Lines");
		btnNeighbourLines.addSelectionListener(new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				if(btnNeighbourLines.getSelection() == false) {
					cmbLineInfo.setEnabled(false);
				}else {
					cmbLineInfo.setEnabled(true);
				}
			}
		});
		
		btnAssociationLines = new CheckBox(grpLine);
		btnAssociationLines.setBounds(165,rel_y,115,20);
		btnAssociationLines.setText("Association Lines");
		btnAssociationLines.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(btnAssociationLines.getSelection() == false) {
					cmbLineInfo.setEnabled(false);
                    btnRootLine.setEnabled(false);
				}else {
					cmbLineInfo.setEnabled(true);
                    btnRootLine.setEnabled(true);
				}
			}
		});

        btnRootLine = new CheckBox(grpLine);
        btnRootLine.setBounds(285,rel_y,110,20);
        btnRootLine.setText("Root Node Lines");

		rel_y += 40;
		
		Label lbl 		= new Label(grpLine,SWT.NORMAL); 
		lbl.setBounds(15,rel_y,140,20);
		lbl.setText("Information on the Line");
		
		cmbLineInfo		= new Combo(grpLine,SWT.SINGLE|SWT.BORDER|SWT.READ_ONLY);
		cmbLineInfo.setBounds(155,rel_y-5,130,20);
		
		cmbLineInfo.add("No Information");
		cmbLineInfo.add("Bitrate");
		cmbLineInfo.add("Signal");
		cmbLineInfo.add("All Information");
		
		cmbLineInfo.select(0);
		
		grp_y += 100;
		
		grpMapProp = new Group(mainCnv,SWT.NONE);
		grpMapProp.setBounds(10,grp_y,400,220);
		grpMapProp.setFont(MeshViewerGDIObjects.boldVerdanaFont8);
		grpMapProp.setText("Topology View Properties");
		
        rel_y = 25;

		btnGrid = new Button(grpMapProp,SWT.CHECK);
		btnGrid.setBounds(15,rel_y,75,20);
		btnGrid.setText("Show Grid");
		
		rel_y += 3;
		
		lblLayoutGridSize = new Label(grpMapProp,SWT.SIMPLE);
		lblLayoutGridSize.setText("Grid Size");
		lblLayoutGridSize.setBounds(105,rel_y,50,20); 

		rel_y += 5;
		
		sldGridSize = new Slider(grpMapProp,SWT.SIMPLE);
		sldGridSize.setBounds(165,rel_y,190,20);
		sldGridSize.setMinValue(1);
		sldGridSize.addValueListener(new Listener(){

			public void handleEvent(Event arg0) {
				Integer val = (Integer) arg0.data;
				lblGridSize.setText("" + val.intValue());
			}
			
		});
		lblGridSize = new Label(grpMapProp,SWT.SIMPLE);
		lblGridSize.setBounds(365,rel_y,25,15);

		rel_y += 25;
		
		btnEnabelHealthMonitor = new Button(grpMapProp, SWT.CHECK);
		btnEnabelHealthMonitor.setBounds(15,rel_y,140,20);
		btnEnabelHealthMonitor.setText("Enable Health Monitor");
		btnEnabelHealthMonitor.addSelectionListener(new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				
				if(btnEnabelHealthMonitor.getSelection() == false){
					btnViewHMSettings.setEnabled(false);
				}else {
					btnViewHMSettings.setEnabled(true);
				}
			}	
		});
		btnEnabelHealthMonitor.setEnabled(false);
		
		btnViewHMSettings	   = new Button(grpMapProp,SWT.SIMPLE);
		btnViewHMSettings.setBounds(170,rel_y,70,20);
		btnViewHMSettings.setText("Settings");
		btnViewHMSettings.addSelectionListener(new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				showHealthMonitorSettings();
			}	
		});
		btnViewHMSettings.setEnabled(false);
		
		rel_y += 35;
		
		btnShowBackground = new Button(grpMapProp,SWT.CHECK);
		btnShowBackground.setText("Background Image");
		btnShowBackground.setBounds(15,rel_y,125,20);
		btnShowBackground.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				boolean selection = btnShowBackground.getSelection();
				showBackgroundImage(selection);
				if(selection == true)
					txtMapImgPath.setText(properties.getMapImagePath());
				else
					txtMapImgPath.setText("");
			}

		});
		
		txtMapImgPath = new Text(grpMapProp,SWT.BORDER);
		txtMapImgPath.setBounds(170,rel_y,153,20);
		txtMapImgPath.setEditable(false);
		
		btnPath = new Button(grpMapProp,SWT.PUSH);
		btnPath.setText("...");
		btnPath.setBounds(335,rel_y,47,20);
		btnPath.addMouseListener(new MouseAdapter() {

			public void mouseUp(MouseEvent arg0) {
				
				FileDialog 	fileDialog 	= new FileDialog(new Shell());
				String[] 	ext 		= new String[4];
				ext[0] = "*.bmp";
				ext[1] = "*.jpg";
				ext[2] = "*.jpeg";    			
				ext[3] = "*.gif";				
				fileDialog.setFilterExtensions(ext);
				fileDialog.setFilterPath(MFile.getFileAbsolutePath());
				String mapImgPath = fileDialog.open();				
				if(mapImgPath != null){
					txtMapImgPath.setText(mapImgPath);
				}				
			}});
		
		rel_y += 35;
		
		lblGridColor = new Label(grpMapProp,SWT.NONE);
		lblGridColor.setBounds(15,rel_y,120,20);
		lblGridColor.setText("Grid Color");
		
		btnGridColor = new Button(grpMapProp, SWT.PUSH);
		btnGridColor.setBounds(170,rel_y-2,70,20);
		btnGridColor.addSelectionListener(new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				openColorDialog(btnGridColor, currentGridColor);
			}
		});
		btnGridColor.setText("Change...");
		
		currentGridColor = new Canvas(grpMapProp,SWT.NONE| SWT.BORDER);
		currentGridColor.setBounds(265,rel_y-2,20,20);
				
		rel_y += 35;
		
		lblLineColor = new Label(grpMapProp,SWT.NONE);
		lblLineColor.setBounds(15,rel_y,120,20);
		lblLineColor.setText("Parent Line Color");
		
		btnLineColor =  new  Button(grpMapProp, SWT.PUSH); 
		btnLineColor.setBounds(170,rel_y-2,70,20);
		btnLineColor.addSelectionListener(new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				openColorDialog(btnLineColor, currentLineColor);
			}
		});
		btnLineColor.setText("Change...");
		
		currentLineColor = new Canvas(grpMapProp,SWT.NONE| SWT.BORDER);
		currentLineColor.setBounds(265,rel_y-4,20,20);
		
		rel_y += 35;
		lblKAPColor = new Label(grpMapProp,SWT.NONE);
		lblKAPColor.setBounds(15,rel_y,140,20);
		lblKAPColor.setText("Neighbour Line Color");
		
		rel_y -=7;
		
		btnKAPColor = new Button(grpMapProp, SWT.PUSH);
		btnKAPColor.setBounds(170,rel_y,70,20);
		btnKAPColor.setText("Change...");
		btnKAPColor.addSelectionListener(new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				openColorDialog(btnKAPColor, currentKAPColor);				
			}
		});
		
		currentKAPColor = new Canvas(grpMapProp,SWT.NONE|SWT.BORDER);
		currentKAPColor.setBounds(265,rel_y-1,20,20);

		rel_y += 35;

		grp_y += 230;
        
		updateControlsData();
		ContextHelp.addContextHelpHandlerEx(mainCnv,contextHelpEnum.DLGVIEWNODESETTINGS);
	}

	protected void showHealthMonitorSettings() {
/*	
		HealthMonitorProperties hmSettings = properties.getHealthMonitorProperties();
		healthMonitorSettingsDlg = new HealthMonitorSettingsDlg(hmSettings);
		healthMonitorSettingsDlg.setTitle("Health Monitor Settings");
		healthMonitorSettingsDlg.show();
		
		if(healthMonitorSettingsDlg.getStatus()!= SWT.OK)
			return;
		
		hmSettings.setYellowAlertTime(healthMonitorSettingsDlg.getYellowAlertValue());
		hmSettings.setRedAlertTime(healthMonitorSettingsDlg.getRedAlertValue());
*/
	}

	
	private void openColorDialog(Button btn, Canvas currentColor) {
		
		ColorDialog cd 				= new ColorDialog(new Shell());
		Color		initColor		= (Color) btnGridColor.getData();
		if(initColor != null) {
			cd.setRGB(initColor.getRGB());
		}
		RGB newRGBValues			= new RGB(255, 255, 255);
		newRGBValues				= cd.open();	
		if(newRGBValues == null) {
			return;
		}
		Color	newColor			= new Color(null, newRGBValues);
		btn.setData(newColor);
		currentColor.setBackground(newColor);
	}
	
	private Color getCurrentSelectedColor(Button btn) {
		Color	newColor	= (Color) btn.getData();
		return newColor;		
	}
	
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	public boolean onOk() {

	    if(btnShowBackground.getSelection() == true) {
	    	if(txtMapImgPath.getText().trim().equalsIgnoreCase("") == true) {
	    		MessageBox msgBox = new MessageBox(new Shell(Display.getDefault()), SWT.ICON_ERROR);
	    		msgBox.setMessage("Please select background image");
	    		msgBox.open();
	    		return false;
	    	}
	    }
	    
		//properties.setHelthMonitorStatus(btnEnabelHealthMonitor.getSelection());
		properties.setGridSize(sldGridSize.getValue());
		properties.setNodeDisplayValue(cmbNodeWindow.getSelectionIndex());
	    properties.setShowGrid(btnGrid.getSelection());
		
	    properties.setNeighbourLineInfo((byte) cmbLineInfo.getSelectionIndex());
	    
	    if(btnNeighbourLines.getTriState() == CheckBox.TRISTATE_GRAYED) {
	    	properties.setNeighbourLines(MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_PARTIAL);
	    }else {
			if(btnNeighbourLines.getSelection() == true) {
			    properties.setNeighbourLines(MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_ALL);
			}else {
				properties.setNeighbourLines(MeshNetworkUIProperties.SHOW_NEIGHBOURLINE_STATE_NONE);
			}
	    }
	    
	    if(btnAssociationLines.getSelection() == true) {
		    properties.setAssociationLines(MeshNetworkUIProperties.ASSOCIATIONLINE_STATE_SHOW);
		}else {
			properties.setAssociationLines(MeshNetworkUIProperties.ASSOCIATIONLINE_STATE_HIDE);
		}

        if(btnRootLine.getSelection() == true) {
            properties.setRootLine(MeshNetworkUIProperties.ROOTLINE_STATE_SHOW);
        }else {
            properties.setRootLine(MeshNetworkUIProperties.ROOTLINE_STATE_HIDE);
        }

	    if(btnShowBackground.getSelection() == true) {
		    properties.setMapImagePath(txtMapImgPath.getText().trim());	    	
		 	Color gridLineColor = getCurrentSelectedColor(btnGridColor);
			properties.setGridLineColor(gridLineColor);
			
			Color wirelessLinkColor = getCurrentSelectedColor(btnLineColor);
			properties.setWirelessLinkColor(wirelessLinkColor);
				   
			Color kapLinkColor = getCurrentSelectedColor(btnKAPColor);
			properties.setKapLinkColor(kapLinkColor);
	    } else {
		    properties.setMapImagePath("");	    	
			properties.setGridLineColor(MeshNetworkUIProperties.DEFAULT_GRID_LINE_COLOR);
			properties.setWirelessLinkColor(MeshNetworkUIProperties.DEFAULT_WIRELESS_LINK_COLOR);
			properties.setKapLinkColor(MeshNetworkUIProperties.DEFAULT_KAP_LINK_COLOR);
	    }
	    
	    return true;		
	
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	public boolean onCancel() {
		return true;
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExit()
     */
    public boolean onExit() {        
        return true;
    }

    /**
     * @return
     * 
     */
    public MeshNetworkUIProperties getProperties() {
        return properties;
        
    }

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {

		bounds.x	 	= 100;
		bounds.y 	 	= 100;
		bounds.width	= 405;
		bounds.height	= 400;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}
	
}
