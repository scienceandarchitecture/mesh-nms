package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.impl.Configuration;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.dialogs.advancedsettings.SipConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;

public class SipConfigDlg extends MeshDynamicsDialog implements IMeshConfigBook {

	private CTabFolder			tabFolder;
	private SipConfigPage		sipPage;
	private Configuration		localConfig;
	private IConfiguration		originalConfig;
	private Rectangle			minimumBounds;
	
	public SipConfigDlg(IConfiguration configuration) {
		this.originalConfig = configuration;
		this.localConfig	= new Configuration();
		
		originalConfig.getSipConfiguration().copyTo(localConfig.getSipConfiguration());
		
		sipPage = new SipConfigPage(this);
		setTitle("PBV(tm) Settings");
	}
	
	@Override
	protected void createControls(Canvas mainCnv) {
		tabFolder             = new CTabFolder(mainCnv,SWT.BORDER);
		
		tabFolder.setSelectionForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		
		Display display = Display.getCurrent();
		
		tabFolder.setSelectionBackground(new Color[]{display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND), 
                					  display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND),
									  display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT), 
									  display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT)},
									  new int[] {40, 70, 100}, true);
		
		tabFolder.setBounds(5, 5, minimumBounds.width, minimumBounds.height);
	
		tabFolder.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				sipPage.selectionChanged(true);
			}
			
		});
		
		sipPage.createControls(tabFolder);
		sipPage.initalizeLocalData();
	}

	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		Rectangle pageBounds = sipPage.getMinimumBounds();
		
		minimumBounds	= new Rectangle(0,0,0,0);
		minimumBounds.width 	= pageBounds.width + 15;
		minimumBounds.height 	= pageBounds.height + 15;
		
		bounds.width = minimumBounds.width;
		bounds.height = minimumBounds.height;
	}

	@Override
	protected boolean onCancel() {
		sipPage.onCancel();
		return true;
	}

	@Override
	protected boolean onOk() {
		if(sipPage.onOk() == false)
			return false;
		localConfig.copyTo(originalConfig);
		return true;
	}

	@Override
	public boolean canDeleteVlan(int tag) {
		return false;
	}

	@Override
	public void dataModified() {
		super.setIsDirty(true);
	}

	@Override
	public void essIdChangedForInterface(String ifName) {
	}

	@Override
	public int getConfigMode() {
		return 0;
	}

	@Override
	public IConfiguration getConfiguration() {
		return localConfig;
	}

	@Override
	public String getIfEssIdByName(String ifName) {
		return null;
	}

	@Override
	public MeshNetwork getMeshNetWork() {
		return null;
	}

	@Override
	public IVersionInfo getVersionInfo() {
		return null;
	}

	@Override
	public boolean isCountryCodeChanged() {
		return false;
	}

	@Override
	public boolean isRfConfigChanged() {
		return false;
	}

	@Override
	public void notifySecurityConfigurationChangedFor(String ifName) {
	}

	@Override
	public void notifyWPAPersonalKeyRegeneratedFor(String ifName) {
	}

	@Override
	public int showMessage(String message, int style) {
		return 0;
	}

	@Override
	public void notifyDCAListChanged(String ifName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyDCAListRemove(String ifName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IInterfaceInfo getInterfaceInfoByName(String ifName) {
		// TODO Auto-generated method stub
		return null;
	}

}
