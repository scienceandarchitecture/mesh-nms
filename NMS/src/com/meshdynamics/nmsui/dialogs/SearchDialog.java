/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : SearchDialog.java
 * Comments : 
 * Created  : Apr 30, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |May 06, 2007 | Sta search added								 | Abhijit|
 * --------------------------------------------------------------------------------
 * |  0  |Apr 30, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.mesh.IAPSearchHelper;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.util.MacAddress;

public class SearchDialog extends MeshDynamicsDialog {

	private Label 			lblCaption;
	private Text			txtSearchString;
	private Button			optSearchByName;
	private Button			optSearchByAddr;
	private int				searchMode;
	private List			lstSearchResult;
	private Button			btnSearch;
	private IAPSearchHelper searchHelper;
	private Button			btnExpandAllNodes;
	
	private final int SEARCH_MODE_ANY			= 0;
	private final int SEARCH_MODE_ADDRESS		= 1;
	private final int SEARCH_MODE_NAME			= 2;
	
	public SearchDialog(IAPSearchHelper searchHelper) {
		super();
		this.searchHelper = searchHelper;
		searchMode = SEARCH_MODE_NAME;
	}

	public void createControls(Canvas mainCnv) {
		
		int yOffset = 10;

		Group grpOptions = new Group(mainCnv,SWT.NONE);
		grpOptions.setBounds(20,10,250,120);
		
		lblCaption = new Label(grpOptions,SWT.NONE);
		lblCaption.setText("Enter search string:");
		lblCaption.setBounds(10,yOffset,220,20);
		yOffset += 20;
		
		txtSearchString = new Text(grpOptions,SWT.BORDER);
		txtSearchString.setBounds(10,yOffset,230,20);
		yOffset += 30;

		optSearchByName = new Button(grpOptions,SWT.RADIO);
		optSearchByName.setText("Search By Name");
		optSearchByName.setBounds(10,yOffset,120,20);
		optSearchByName.addSelectionListener(new SelectionAdapter () {

			public void widgetSelected(SelectionEvent arg0) {
				searchMode = SEARCH_MODE_NAME;
				setModeData();
				super.widgetSelected(arg0);
			}
			
		});
	
		optSearchByAddr = new Button(grpOptions,SWT.RADIO);
		optSearchByAddr.setText("Search By Address");
		optSearchByAddr.setBounds(130,yOffset,120,20);
		optSearchByAddr.addSelectionListener(new SelectionAdapter () {

			public void widgetSelected(SelectionEvent arg0) {
				searchMode = SEARCH_MODE_ADDRESS;
				setModeData();
				super.widgetSelected(arg0);
			}
			
		});
		yOffset += 30;
		
		btnSearch	= new Button(grpOptions,SWT.PUSH);
		btnSearch.setText("Search");
		btnSearch.setBounds(190,yOffset,50,20);
		btnSearch.addSelectionListener(new SelectionAdapter () {

			public void widgetSelected(SelectionEvent arg0) {
				super.widgetSelected(arg0);
				onSearch();
			}
			
		});
		setDefaultButton(btnSearch);
		yOffset += 50;
		 
		Label lbl = new Label(mainCnv,SWT.NONE);
		lbl.setText("Search Results");
		lbl.setBounds(100,yOffset,250,15);
		yOffset += 20;

		lstSearchResult = new List(mainCnv,SWT.BORDER|SWT.MULTI|SWT.V_SCROLL);
		lstSearchResult.setBounds(20,yOffset,250,100);
		lstSearchResult.addMouseListener(new MouseAdapter(){

			public void mouseDoubleClick(MouseEvent arg0) {
				super.mouseDoubleClick(arg0);
				String[] selection = lstSearchResult.getSelection();
				if(selection == null || selection.length <= 0) {
					return;
				}
				AccessPoint ap = (AccessPoint) lstSearchResult.getData(selection[0]);
				if(ap != null) {
					searchHelper.selectAccesspoint(ap);
				}
			}
			
		});
	
		yOffset += 100;
		btnExpandAllNodes = new Button(mainCnv,SWT.CHECK);
		btnExpandAllNodes.setText("Expand all searched nodes");
		btnExpandAllNodes.setBounds(20,yOffset,150,20);
		
		setModeData();
	}

	private void onSearch() {
		
		if(validateSearchText() == false) {
			return;
		}
		lstSearchResult.removeAll();

		String 		searchString 	= txtSearchString.getText().trim();
		
		if(searchMode == SEARCH_MODE_ADDRESS) {

			MacAddress 	bssid	 		= new MacAddress();
			
			bssid.setBytes(searchString);
			
			if(searchAccesspoint(bssid) == true) {
				return;
			}
			if(searchSta(bssid) == true) {
				return;
			}

			showErrorMessage("Matching access point not found.", SWT.ICON_ERROR);
			return;
			
			
		} else if(searchMode == SEARCH_MODE_NAME) {
			boolean partialMatch = false;
			if(searchString.endsWith("*") == true) {
				partialMatch = true;
				searchString = searchString.substring(0, searchString.length()-1);
			}
			AccessPoint[] apList = searchHelper.getAccesspointsByName(searchString, partialMatch);
			if(apList == null) {
				showErrorMessage("Matching access points not found.", SWT.ICON_ERROR);
				return;
			}
			if(apList.length <= 0) {
				showErrorMessage("Matching access points not found.", SWT.ICON_ERROR);
				return;
			}
			
			for(int i=0;i<apList.length;i++) {
				lstSearchResult.add(apList[i].toString());
				lstSearchResult.setData(apList[i].toString(), apList[i]);
				if(btnExpandAllNodes.getSelection() == true) {
					searchHelper.selectAccesspoint(apList[i]);
				}
				
			}
		}
	}

	private boolean searchSta(MacAddress staAddress) {
		
		AccessPoint parentAp 	= searchHelper.getStaParent(staAddress);
		
		if(parentAp == null) {
			return false;
		}

		lstSearchResult.add(staAddress.toString());
		lstSearchResult.setData(staAddress.toString(), parentAp);
		
		if(btnExpandAllNodes.getSelection() == true) {
			searchHelper.selectAccesspoint(parentAp);
		}
		
		return true;
	}

	private boolean searchAccesspoint(MacAddress bssid) {
		
		AccessPoint ap 			= searchHelper.getAccessPointFromBssid(bssid);
	
		if(ap == null) {
			return false;
		}
		
		lstSearchResult.add(ap.toString());
		lstSearchResult.setData(ap.toString(), ap);
		IRuntimeConfiguration runtimeConfig	= ap.getRuntimeApConfiguration();
		if(runtimeConfig == null) {
			return false;
		}
		AccessPoint parentAp = searchHelper.getAccessPointFromBssid(runtimeConfig.getParentBssid());
		
		if(btnExpandAllNodes.getSelection() == true) {
			searchHelper.selectAccesspoint(ap);
			if(parentAp != null) {
				searchHelper.selectAccesspoint(parentAp);
			}
		}
		
		return true;
	}

	private boolean validateSearchText() {
		
		String searchString = txtSearchString.getText().trim();
		
		if(searchString.equals("") == true) {
			showErrorMessage("Please enter search string.", SWT.ICON_INFORMATION);
			return false;
		}
		
		if(searchMode == SEARCH_MODE_ANY) {
			showErrorMessage("Please select search mode.", SWT.ICON_INFORMATION);
			return false;
		}
		
		if(searchMode == SEARCH_MODE_ADDRESS) {
			MacAddress macAddress = new MacAddress();
			if(macAddress.setBytes(searchString) != 0) {
				showErrorMessage("Invalid mac address in search string.", SWT.ICON_ERROR);
				return false;
			}
			if(macAddress.equals(MacAddress.resetAddress) == true) {
				showErrorMessage("Invalid mac address in search string.", SWT.ICON_ERROR);
				return false;
			}
			if(macAddress.equals(MacAddress.undefinedAddress) == true) {
				showErrorMessage("Invalid mac address in search string.", SWT.ICON_ERROR);
				return false;
			}
		}
		
		return true;
	}

	private void setModeData() {
		if(searchMode == SEARCH_MODE_ADDRESS) {
			optSearchByAddr.setSelection(true);
			optSearchByName.setSelection(false);
			lblCaption.setText("Mac Address:");
		} else if(searchMode == SEARCH_MODE_NAME) {
			optSearchByName.setSelection(true);
			optSearchByAddr.setSelection(false);
			lblCaption.setText("Node Name (for partial search use * at end):");
		}
		btnExpandAllNodes.setSelection(true);
		txtSearchString.setFocus();
	}
	
	public boolean onCancel() {
		return true;
	}

	public boolean onOk() {
		return true;
	}

	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x = 0;
		bounds.y = 0;
		bounds.width = 280;
		bounds.height = 280;
	}

	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	protected boolean isModal() {
		// TODO Auto-generated method stub
		return false;
	}
}
