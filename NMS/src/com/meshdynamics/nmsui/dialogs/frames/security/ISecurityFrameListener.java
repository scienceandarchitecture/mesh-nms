/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ISecurityFrameListener.java
 * Comments : callback events interface for Security frame
 * Created  : Feb 20, 2007
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 20, 2007 | Created                                         | Imran   |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.dialogs.frames.security;

import com.meshdynamics.nmsui.dialogs.base.IMessage;



public interface ISecurityFrameListener extends IMessage{
	
	public static final int SEC_NOT_APPLICABLE	= 0;
	public static final int SEC_WEP				= 1;
	public static final int SEC_WPA_PERSONAL	= 2;
	public static final int SEC_WPA_ENTERPRISE	= 3;
	
	public void securitySelected(SecurityFrameSelectionEvent sfsEvent);
	
	public void notifyWPAPersonalKeyRegenerated();

}
