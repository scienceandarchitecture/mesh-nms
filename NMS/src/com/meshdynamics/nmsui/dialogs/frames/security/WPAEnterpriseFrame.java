/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : WPAEnterpriseFrame.java
 * Comments : creates WPAEnterpriseFrame for security.
 * Created  : Feb 21, 2007
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 21, 2007 | Created                                         | Imran   |
 * --------------------------------------------------------------------------------
 **********************************************************************************/
package com.meshdynamics.nmsui.dialogs.frames.security;


import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.configuration.IPSKConfiguration;
import com.meshdynamics.meshviewer.configuration.IRadiusConfiguration;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.util.IpAddress;

public class WPAEnterpriseFrame {
	
	protected 	Rectangle 		minimumBounds;
	private 	Group			parent;
	
	private 	Group 			grp;
	private 	Combo			cmbMode;
	private 	Text 			txtRadiusServer;
	private 	Text 			txtRadiusPort;
	private 	Text 			txtRadiusSecret;
	private 	Text 			txtGRP;
	private 	Button			rdoCCCMP;
	private 	Button			rdoTKIP;
	private 	Button			chkRADDecidesVLAN;
	private 	Label			lblMode;
	private		Label			lblRadiusServer;
	private 	Label			lblRadiusPort;
	private 	Label			lblRadiusSecrete;
	private 	Label			lblGRP;
	private 	Label			lblSecs;
	
		
	private 	SecurityFrame	securityFrame;
	private     boolean			fipsEnabled;
	private 	int 			disableControls;
	
	private static final int 	MAX_KEY_LENGTH_FOR_FIPPS	= 16;
	private static final int 	DEFAULT_GROUP_KEY_RENEWAL	= 30;
	private static final String DEFAULT_RADIUS_PORT			= "1812";
	
	
	public WPAEnterpriseFrame(Group parent, SecurityFrame secFrame, boolean fipsEnabled, int disableControls ){
		
		this.parent 			= parent;
		this.securityFrame		= secFrame;
		this.fipsEnabled		= fipsEnabled;
		this.disableControls	= disableControls;
		
		minimumBounds = new Rectangle(5,110,405,250);
		createControls();
	}
	
	private void createControls(){
		
		int rel_x  		= 5;
		int rel_y  		= 110;
		
        grp 			= new Group(parent,SWT.NO);
		grp.setText("WPA Enterprise");
				
		rel_x			+= 25;
		rel_y			-= 77;
        lblMode			=	new Label(grp,SWT.NO);
        lblMode.setText("Mode  :");        
        lblMode.setBounds(rel_x,rel_y,70,20);
             
        rel_x			+=115;
        rel_y			-=3;
        cmbMode			=	new Combo(grp,SWT.BORDER|SWT.READ_ONLY);        
        cmbMode.setBounds(rel_x,rel_y,210,20);
        cmbMode.add("WPA");
        if(((disableControls	& 4) 
        		== SecurityFrame.DISABLE_WPA2_MODE) == false){
        	cmbMode.add("WPA2/802.11i");
        }
        cmbMode.select(0);
              
        rel_x			-=115;
        rel_y 			+= 33;
        lblRadiusServer	= 	new Label(grp,SWT.NO);
        lblRadiusServer.setText("Radius Server  :");
        lblRadiusServer.setBounds(rel_x,rel_y,100,17);
             
        rel_x+=115;
        txtRadiusServer	=	new Text(grp,SWT.BORDER);
        txtRadiusServer.setBounds(rel_x,rel_y,210,17);
             
        rel_x			-=115;
        rel_y 			+= 33;
        lblRadiusPort				= 	new Label(grp,SWT.NO);
        lblRadiusPort.setText("Radius Port  :");
        lblRadiusPort.setBounds(rel_x,rel_y,100,17);
             
        rel_x			+=115;
        txtRadiusPort	=	new Text(grp,SWT.BORDER);
        txtRadiusPort.setBounds(rel_x,rel_y,210,17);
        txtRadiusPort.setText("1812");
              
        rel_x			 -=115;
        rel_y 			 += 33;
        lblRadiusSecrete = 	new Label(grp,SWT.NO);
        lblRadiusSecrete.setText("Radius Secret  :");
        lblRadiusSecrete.setBounds(rel_x,rel_y,100,17);
              
        rel_x+=115;
        txtRadiusSecret	=	new Text(grp,SWT.BORDER|SWT.BORDER|SWT.PASSWORD);
        txtRadiusSecret.setBounds(rel_x,rel_y,210,17);
               
        rel_x			-=115;
        rel_y			+=33;
        lblGRP				= 	new Label(grp,SWT.NO);
        lblGRP.setText("Group Key Renewal  :");
        lblGRP.setBounds(rel_x,rel_y,110,17);
          
        rel_x			+=115;
        txtGRP			=	new Text(grp,SWT.BORDER|SWT.BORDER);
        txtGRP.setBounds(rel_x,rel_y,210,17);
        txtGRP.setText("30");
               
        rel_x			+=215;
        rel_y			+=2;
        lblSecs				=	new Label(grp,SWT.NO); 
        lblSecs.setText("sec(s)");
        lblSecs.setBounds(rel_x,rel_y,35,20);
                       
        rel_x			-=330;
        rel_y 			+=27;
        
        rdoCCCMP			=	new Button(grp,SWT.RADIO);
        rdoCCCMP.setText(" Cipher CCMP");
        rdoCCCMP.setBounds(rel_x,rel_y,100,20); 
        rdoCCCMP.setSelection(true);
        
        rel_x += 120;
        rdoTKIP			=	new Button(grp,SWT.RADIO);
        rdoTKIP.setText(" Cipher TKIP");
        rdoTKIP.setBounds(rel_x,rel_y,100,20); 

        rel_x	-= 120;
        rel_y	+=25;
             
        chkRADDecidesVLAN	=	new Button(grp,SWT.CHECK);
        chkRADDecidesVLAN.setText("Radius Server decides VLAN Membership");
        chkRADDecidesVLAN.setBounds(rel_x,rel_y,250,20);
        
        if(((disableControls & 2) == 
        		SecurityFrame.DISABLE_RAD_DECIDES_VLAN_CONTROLS) == true){
           	chkRADDecidesVLAN.setVisible(false);
        }
             
	}

	public void setFrameVisible(boolean b) {
		grp.setVisible(b);
	}

	public Rectangle getMinimumBounds() {
		return minimumBounds;
	}

	public void setBounds(Rectangle Bounds) {
		grp.setBounds(minimumBounds);
	}
	
	public boolean validateLocalData() {
			
		if(MeshValidations.isIpAddress(txtRadiusServer.getText().trim()) == false){
			
			securityFrame.showMessage("Radius Server Address incorrect.",SWT.ICON_ERROR);
			txtRadiusServer.setFocus();
			return false;
		}
		
		if(MeshValidations.isLong(txtRadiusPort.getText().trim()) == false ||
				(txtRadiusPort.getText().trim().equals("0"))){
			
			securityFrame.showMessage("Radius Port incorrect.",SWT.ICON_ERROR);
			txtRadiusPort.setFocus();
			return false;
		}			
		
		if(MeshValidations.isString(txtRadiusSecret.getText().trim()) == false){
			
		    securityFrame.showMessage("Radius Secret incorrect.",SWT.ICON_ERROR);
			txtRadiusSecret.setFocus();
			return false;		
		}
		
		if(MeshValidations.isInt(txtGRP.getText().trim()) == false){
			
			securityFrame.showMessage("Radius Group Key Renewal incorrect.",SWT.ICON_ERROR);
			txtGRP.setFocus();
			return false;
		}

		if (fipsEnabled == true	) {

			String radSecretKey = txtRadiusSecret.getText().trim();
			if (radSecretKey.length() < MAX_KEY_LENGTH_FOR_FIPPS) {
				securityFrame.showMessage("RADIUS Secret should be atleast 16 characters. ",SWT.ICON_INFORMATION);
				return false;
			}
		}
		return true;
	}

	/**
	 * @param radiusConfig
	 */
	public void setValues(IRadiusConfiguration radiusConfig) {
		
		if(radiusConfig.getMode() == IRadiusConfiguration.WPA_PERSONAL_MODE_WPA) {
			cmbMode.select(0);
		}
		if(radiusConfig.getMode() == IRadiusConfiguration.WPA_PERSONAL_MODE_WPA2) {
			cmbMode.select(1);
		}
		
		txtRadiusServer.setText(radiusConfig.getServerIPAddress().toString());
		txtRadiusPort  .setText(Long.toString(radiusConfig.getPort()));
		txtRadiusSecret.setText(radiusConfig.getKey());
		txtGRP		   .setText(Integer.toString(radiusConfig.getGroupKeyRenewal()));
		
		if(radiusConfig.getCipherCCMP() == 1) {
			rdoCCCMP.setSelection(true);
			rdoTKIP.setSelection(false);
		} else if(radiusConfig.getCipherTKIP() == 1) {
			rdoCCCMP.setSelection(false);
			rdoTKIP.setSelection(true);
		}
		
		if(radiusConfig.getWPAEnterpriseDecidesVlan() == 1) {
			chkRADDecidesVLAN.setSelection(true);
		}else if (radiusConfig.getWPAEnterpriseDecidesVlan() == 0) {
			chkRADDecidesVLAN.setSelection(false);
		}
			
	}

	/**
	 * @param wpaEnterpriseInfo
	 */
	public void getDataFromUIToConfiguration(IRadiusConfiguration wpaEnterpriseInfo) {

		if(cmbMode.getSelectionIndex() == IPSKConfiguration.WPA_PERSONAL_MODE_WPA) {
			wpaEnterpriseInfo.setMode(IPSKConfiguration.WPA_PERSONAL_MODE_WPA);
		}
		else if(cmbMode.getSelectionIndex() == IPSKConfiguration.WPA_PERSONAL_MODE_WPA2) {
			wpaEnterpriseInfo.setMode(IPSKConfiguration.WPA_PERSONAL_MODE_WPA2);
		}
		IpAddress	serverIpAddr	= new IpAddress();
		serverIpAddr.setBytes(txtRadiusServer.getText().trim());
		wpaEnterpriseInfo.setServerIPAddress(serverIpAddr);
		
		wpaEnterpriseInfo.setPort(Long.parseLong(txtRadiusPort.getText().trim()));
		wpaEnterpriseInfo.setKey(txtRadiusSecret.getText().trim());
		wpaEnterpriseInfo.setGroupKeyRenewal(Integer.parseInt(txtGRP.getText().trim()));
			
		if(rdoCCCMP.getSelection() == true){
			wpaEnterpriseInfo.setCipherCCMP((short)1);
		}else {
			wpaEnterpriseInfo.setCipherCCMP((short)0);
		}

		if(rdoTKIP.getSelection() == true){
			wpaEnterpriseInfo.setCipherTKIP((short)1);
		}else {
			wpaEnterpriseInfo.setCipherTKIP((short)0);
		}
		
		if(chkRADDecidesVLAN.getSelection() == true){
			wpaEnterpriseInfo.setWPAEnterpriseDecidesVlan((short)1);
		}else {
			wpaEnterpriseInfo.setWPAEnterpriseDecidesVlan((short)0);
		}
	}
	
	/**
	 * 
	 */
	public void clearAll() {
		
		 cmbMode          .select(0);
		 txtRadiusServer  .setText("");
		 txtRadiusPort    .setText(DEFAULT_RADIUS_PORT);
		 txtRadiusSecret  .setText("");
		 txtGRP			  .setText(""+DEFAULT_GROUP_KEY_RENEWAL);
		 chkRADDecidesVLAN.setSelection(false);
	}

	/**
	 * @param wpaEnterpriseInfo
	 */
	public boolean compareConfigurationWithUI(IRadiusConfiguration wpaEnterpriseInfo) {
		
		int selection;
		
		if(wpaEnterpriseInfo.getMode() != cmbMode.getSelectionIndex()){
			return false;
		}
		
		if(wpaEnterpriseInfo.getServerIPAddress().toString().
				equalsIgnoreCase(txtRadiusServer.getText().trim()) == false){
			return false;
		}
		
		if(wpaEnterpriseInfo.getPort() != Long.parseLong(txtRadiusPort.getText().trim())){
			return false;
		}
		
		if(wpaEnterpriseInfo.getKey().equalsIgnoreCase(txtRadiusSecret.getText().trim()) == false){
			return false;
		}
		
		if(wpaEnterpriseInfo.getGroupKeyRenewal() != Integer.parseInt(txtGRP.getText().trim())){
			return false; 
		}
		
		if(chkRADDecidesVLAN.getSelection() == true){
			selection	= 1;
		}else {
			selection	= 0;
		} 
		if(wpaEnterpriseInfo.getWPAEnterpriseDecidesVlan() != selection){
			return false;
		}
		
		return true;
	}


	/**
	 * @param enable
	 * 
	 */
	public void enableFrame(boolean enable) {
		parent.setEnabled(enable);
		grp.setEnabled(enable);
		grp				 .setEnabled(enable);
		lblMode 		 .setEnabled(enable);
		cmbMode			 .setEnabled(enable);
		lblRadiusServer  .setEnabled(enable);
		txtRadiusServer  .setEnabled(enable);
		lblRadiusPort    .setEnabled(enable);
		txtRadiusPort    .setEnabled(enable);
		lblRadiusSecrete .setEnabled(enable);
		txtRadiusSecret  .setEnabled(enable);
		lblGRP 			 .setEnabled(enable);
		txtGRP		     .setEnabled(enable);
		lblSecs 		 .setEnabled(enable);
		chkRADDecidesVLAN.setEnabled(enable);
		
		
	}

}
