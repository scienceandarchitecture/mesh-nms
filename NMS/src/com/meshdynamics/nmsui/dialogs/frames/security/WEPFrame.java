/***********************************************************************************
 * MeshDynamics 
 * --------------
 * File     : WEPFrame.java
 * Comments : creates a WEP frame for secutity.
 * Created  : Feb 21, 2007
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ---------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 21, 2007 | Created                                         | Imran   |
 * --------------------------------------------------------------------------------
 **********************************************************************************/
package com.meshdynamics.nmsui.dialogs.frames.security;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.configuration.IWEPConfiguration;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.util.HexHelper;
import com.meshdynamics.util.WEP;

public class WEPFrame {
	
	
	protected 	Rectangle 		minimumBounds;
	private 	Group			parent;
	
	private		Group			grp;
	private 	Label 			lblStrength;
	private 	Combo 			cmbStrength;
	private 	Label 			lblPassphrase;
    private 	Text 			txtPassphrase;
    private     Button			btnGenerate;
    private 	Group 			grpKey;
    private 	Button 			btnKey1;
    private 	Button 			btnKey2;
    private 	Button 			btnKey3;
    private 	Button			btnKey4;
    private 	Text 			txtKey1;
    private 	Text 			txtKey2;
    private	    Text 			txtKey3;
    private     Text 			txtKey4;
    
    private		SecurityFrame	securityFrame;
    private 	int 			disableControls;
    private final int	 NO_OF_KEYS = 4; 
	
	private final int	 KEY_STRENGTH_64_BIT_INDEX	= 0;
	private final int	 KEY_STRENGTH_128_BIT_INDEX	= 1;
	
	private final short	 KEY1_INDEX	  				= 0;
	private final short	 KEY2_INDEX					= 1;
	private final short	 KEY3_INDEX 				= 2 ;
	private final short	 KEY4_INDEX					= 3;
	
 	
	public WEPFrame(Group parent, SecurityFrame secFrame, boolean fipsEnabled, int disableControls) {
		this.parent 			= parent;
		this.securityFrame		= secFrame;
		this.disableControls	= disableControls;
		minimumBounds 			= new Rectangle(5,110,405,250);
		createControls();
	}
	
	public void createControls(){
						
		int rel_x 	= 15;
	    int rel_y 	= 30;
	        
	    grp 			= new Group(parent,SWT.NO);
		grp.setText("WEP");
		
		rel_x			+= 25;
		rel_y			+= 5;
	    lblStrength 	= new Label(grp,SWT.BOLD);
        lblStrength.setBounds(rel_x,rel_y,60,15);
        lblStrength.setText("Strength :");
       			
		rel_x			+= 115;
		rel_y			-= 5;
		cmbStrength 	= new Combo(grp, SWT.SINGLE|SWT.BORDER|SWT.READ_ONLY);
		cmbStrength.setBounds(rel_x, rel_y, 210, 17);
		cmbStrength.add("40 / 64 Bit");
		cmbStrength.add("104 / 128 Bit");
		cmbStrength.setData(""+0,Integer.toString(IWEPConfiguration.KEY_STRENGTH_64_BIT));
		cmbStrength.setData(""+1,Integer.toString(IWEPConfiguration.KEY_STRENGTH_128_BIT));
		cmbStrength.select(0);
		cmbStrength.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				cmbStrengthHandler();
			}
		});
				
		rel_x			-= 115;
		rel_y 			+= 45;
		
		lblPassphrase 	= new Label(grp,SWT.BOLD);
		lblPassphrase.setBounds(rel_x,rel_y, 60, 15);
		lblPassphrase.setText("Passphrase :");		
		
		rel_x			+= 115;
		rel_y			-= 2;
		txtPassphrase 	= new Text(grp,SWT.BORDER|SWT.PASSWORD);
		txtPassphrase.setBounds(rel_x, rel_y, 140, 17);
		txtPassphrase.addKeyListener(new KeyListener(){
		
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
			}	
			
			public void keyReleased(KeyEvent arg0) {
				String	passPhrase	= txtPassphrase.getText().trim();
				if(passPhrase.equalsIgnoreCase("") || passPhrase == null) {
					btnGenerate.setEnabled(false);
				}else {
					btnGenerate.setEnabled(true);
				}		
			}
			
		});
		
		rel_x	+= 150;
	    btnGenerate	= new Button(grp, SWT.PUSH);
        btnGenerate.setBounds(rel_x,rel_y,60,17);
        btnGenerate.setText("Generate");
        btnGenerate.setEnabled(false);
        btnGenerate.addSelectionListener(new SelectionAdapter(){
        	public void widgetSelected(SelectionEvent se){
        		String	passPhrase	= txtPassphrase.getText().trim();
        		if(passPhrase.length() == Mesh.length_passphrase){
        			btnGenerateHandler();
        		 }else{
        			 securityFrame.showMessage("Passphrase shoud be 10 characters. ",SWT.ICON_ERROR);
        			 txtPassphrase.setFocus();
        		 }
        		
        	}
        });
        
		rel_x			-= 265;
		rel_y			+= 35;
	
		grpKey 	= new Group(grp, SWT.NONE);
		grpKey.setBounds(rel_x, rel_y, 325, 120);
		
		int rel_x1 		= 15;
		int rel_y1		= 20;
		
		btnKey1			 = new Button(grpKey, SWT.RADIO);		
		btnKey1.setBounds(rel_x1, rel_y1, 60, 15);
		btnKey1.setText("Key1:");		
		btnKey1.setSelection(true);
		
		rel_x1			+= 75;
		if(((disableControls	& 8) 
        		== SecurityFrame.DISABLE_WEP_KEY1) == true){
		 	txtKey1	=	new Text(grpKey,SWT.BORDER|SWT.READ_ONLY);
        } else {
        	txtKey1	=	new Text(grpKey,SWT.BORDER);
        }
		
		txtKey1.setBounds(rel_x1,rel_y1,220,17);
		txtKey1.setText("0000000000");
		txtKey1.setTextLimit(10);
		
		rel_x1			-=75;
		rel_y1			+=22;
		btnKey2			= new Button(grpKey, SWT.RADIO);		
		btnKey2.setBounds(rel_x1, rel_y1, 60, 15);
		btnKey2.setText("Key2:");
		
		rel_x1			+=75;
		txtKey2			=	new Text(grpKey,SWT.BORDER);
		txtKey2.setBounds(rel_x1,rel_y1,220,17);
		txtKey2.setText("0000000000");
		txtKey2.setTextLimit(10);
		
		rel_x1			-=75;
		rel_y1			+=22;
		btnKey3 		= new Button(grpKey, SWT.RADIO);		
		btnKey3.setBounds(rel_x1, rel_y1, 60, 15);
		btnKey3.setText("Key3:");	
		
		rel_x1			+=75;
		txtKey3			=	new Text(grpKey,SWT.BORDER);
		txtKey3.setBounds(rel_x1,rel_y1,220,17);
		txtKey3.setText("0000000000");
		txtKey3.setTextLimit(10);
		
		rel_x1			-=75;
		rel_y1			+=22;
		btnKey4			= new Button(grpKey, SWT.RADIO);		
		btnKey4.setBounds(rel_x1, rel_y1, 60, 15);
		btnKey4.setText("Key4:");	
		
		rel_x1			+=75;
		txtKey4			=	new Text(grpKey,SWT.BORDER);
		txtKey4.setBounds(rel_x1,rel_y1,220,17);
		txtKey4.setText("0000000000");
		txtKey4.setTextLimit(10);
		
	}
	
	private void btnGenerateHandler(){
		int 	index;
  		int 	length;
  		int 	ret;
  		int 	wepType;
  		char[] 	data; 
  		String  passPhrase;
  		String 	key 		= ""; 
  		String 	key1 		= "";
  		String 	key2		= "";
  		String 	key3		= "";
  		String 	key4 		= "";
  		WEP 	wep;
  	
  		wepType		= cmbStrength.getSelectionIndex();
  		passPhrase	= txtPassphrase.getText().trim();	
  		length		= passPhrase.length();
  		if(length == 0) {		// 2001/05/16 Edison: Don't allow zero length passphrase.
  			return;
  		}
  		  		
  		wep		= new WEP();
  		
  		if(wepType == KEY_STRENGTH_128_BIT_INDEX ){
  						
  			data 	= new char[13];
  			ret 	= wep.keyGenerate(passPhrase, length, wepType, data);
  			if(ret == -1) {
  				return;
  			}
  			
  			for(index = 0; index < 13; index++) {  				
  				key += data[index];  				
  			}  	
  			
 			key1 = HexHelper.toHexString(key);
 			key2 = HexHelper.toHexString(key);
  			key3 = HexHelper.toHexString(key);
  			key4 = HexHelper.toHexString(key);
  			
  		}else if (wepType == KEY_STRENGTH_64_BIT_INDEX) {
  			
  			data	= new char[20];
  			ret		= wep.keyGenerate(passPhrase, length, wepType, data);
  			if(ret == -1) {
  				return;
  			}
  			
  			for(index = 0;index < 20;index++) {  				
  				key	+= data[index];  				
  			}  		
  			
  			key1 = key.substring(0,   5);
  			key2 = key.substring(5,  10);
  			key3 = key.substring(10, 15);
  			key4 = key.substring(15);
  			
  			key1 = HexHelper.toHexString(key1);
  			key2 = HexHelper.toHexString(key2);
  			key3 = HexHelper.toHexString(key3);
  			key4 = HexHelper.toHexString(key4);
  		}	
		
		txtKey1.setText(key1.toUpperCase());
		txtKey2.setText(key2.toUpperCase());
		txtKey3.setText(key3.toUpperCase());
		txtKey4.setText(key4.toUpperCase());
		
	}
	
	private  void cmbStrengthHandler(){
					
		if(cmbStrength.getSelectionIndex() == 0) { 
			txtKey1.setTextLimit(10);
			txtKey1.setText("0000000000");
			txtKey2.setTextLimit(10);
			txtKey2.setText("0000000000");
			txtKey2.setEnabled(true);
			txtKey3.setTextLimit(10);
			txtKey3.setText("0000000000");
			txtKey3.setEnabled(true);
			txtKey4.setTextLimit(10);
			txtKey4.setText("0000000000");
			txtKey4.setEnabled(true);
			
			btnKey2.setEnabled(true);
			btnKey3.setEnabled(true);
			btnKey4.setEnabled(true);
			
		} else if(cmbStrength.getSelectionIndex() == 1){ 
			txtKey1.setTextLimit(26);
			txtKey1.setText("00000000000000000000000000");
			txtKey2.setTextLimit(26);
			txtKey2.setText("00000000000000000000000000");
			txtKey2.setEnabled(false);
			txtKey3.setTextLimit(26);
			txtKey3.setText("00000000000000000000000000");
			txtKey3.setEnabled(false);
			txtKey4.setTextLimit(26);
			txtKey4.setText("00000000000000000000000000");
			txtKey4.setEnabled(false);
			
			btnKey2.setEnabled(false);
			btnKey3.setEnabled(false);
			btnKey4.setEnabled(false);
		}
	}

	public Rectangle getMinimumBounds() {
		return minimumBounds;
	}

	public void setFrameVisible(boolean b) {
		grp.setVisible(b);
		
	}

	public void setBounds(Rectangle bounds) {
		grp.setBounds(bounds);
	}

	public boolean validateLocalData() {
		String          passPrase;
	/*	passPrase=txtPassphrase.getText().trim();
		if(passPrase == null){
			securityFrame.showMessage("Passphrase Should NOT EMPTY ", SWT.ICON_ERROR);
			return false;
		}*/
		
		String [] keys 	=	null;
		keys			= 	new String[NO_OF_KEYS];
		
		keys[0]			= 	txtKey1.getText().trim();
		keys[1]			= 	txtKey2.getText().trim();
		keys[2]			= 	txtKey3.getText().trim();
		keys[3]			= 	txtKey4.getText().trim();
		
		for(int i=0; i < NO_OF_KEYS; i++){
			String key= keys[i]; 
		    if(key.equals("0000000000") || key.equals("00000000000000000000000000")){
		    	securityFrame.showMessage("Passphrase Should NOT EMPTY ", SWT.ICON_ERROR);
			    return false;
		    }
			if( MeshValidations.isAlphanumeric(keys[i]) == false ||
					MeshValidations.isHex(keys[i].trim()) == false) {
					securityFrame.showMessage("WEP Key "+(i+1)+" is incorrect. ",SWT.ICON_ERROR);
					return false;
			}
				
			if(cmbStrength.getSelectionIndex() == 0) { 
				
				if( keys[i].length() > 0 	&& 
					keys[i].length() < 10 ) {
					securityFrame.showMessage("WEP Key "+(i+1)+" has incorrect length.",SWT.ICON_ERROR);
					return false;
				}
				
			}else if(cmbStrength.getSelectionIndex() == 1) {
				
				if( keys[i].length() > 0 	&& 
				    keys[i].length() < 26)  {
					securityFrame.showMessage("WEP Key "+(i+1)+" has incorrect length.",SWT.ICON_ERROR);
					return false;		
				}
			}
		}
				
		return true;	
	}

	public void clearAll() {
		
		cmbStrength.select(0);
		txtPassphrase.setText("");
		txtKey1.setText("0000000000");
		txtKey2.setText("0000000000");
		txtKey3.setText("0000000000");
		txtKey4.setText("0000000000");
		
	}

	/**
	 * @param wepConfig
	 */
	public void setValues(IWEPConfiguration wepConfig) {
		
		int strength;
		strength	= wepConfig.getKeyStrength();
		if(strength == IWEPConfiguration.KEY_STRENGTH_64_BIT) {
    		cmbStrength.select(0);
    		txtKey1.setTextLimit(10);
    		txtKey2.setTextLimit(10);
    		txtKey3.setTextLimit(10);
    		txtKey4.setTextLimit(10);
    	} else if(strength == IWEPConfiguration.KEY_STRENGTH_128_BIT) {
			cmbStrength.select(1);
			txtKey1.setTextLimit(26);
			txtKey2.setTextLimit(26);
			txtKey3.setTextLimit(26);
			txtKey4.setTextLimit(26); 
			
			txtKey2.setEnabled(false);
			txtKey3.setEnabled(false);
			txtKey4.setEnabled(false);
			
			btnKey2.setEnabled(false);
			btnKey3.setEnabled(false);
			btnKey4.setEnabled(false);
		}
    	
		selectKey((short)(wepConfig.getKeyIndex()));
		
		txtKey1.setText(HexHelper.toHex(wepConfig.getKey(0)));
		txtKey2.setText(HexHelper.toHex(wepConfig.getKey(1)));
		txtKey3.setText(HexHelper.toHex(wepConfig.getKey(2)));
		txtKey4.setText(HexHelper.toHex(wepConfig.getKey(3)));
		
	}
	
	/**
	 * @param keyIndex
	 */
	private void selectKey(short keyIndex) {
		
		if(keyIndex == KEY1_INDEX) {
			btnKey1.setSelection(true);
			btnKey2.setSelection(false);
			btnKey3.setSelection(false);
			btnKey4.setSelection(false);
		} else if(keyIndex == KEY2_INDEX ) {
			btnKey2.setSelection(true);
			btnKey1.setSelection(false);
			btnKey3.setSelection(false);
			btnKey4.setSelection(false);
		} else if(keyIndex == KEY3_INDEX) {
			btnKey3.setSelection(true);
			btnKey1.setSelection(false);
			btnKey2.setSelection(false);
			btnKey4.setSelection(false);
		} else if(keyIndex == KEY4_INDEX) {
			btnKey4.setSelection(true);
			btnKey1.setSelection(false);
			btnKey2.setSelection(false);
			btnKey3.setSelection(false);
		}
	}

	public void getDataFromUIToConfiguration(IWEPConfiguration wepConfig){
		
		int		strength;
		String	key;
		
		strength	= cmbStrength.getSelectionIndex();
		
		if(strength == KEY_STRENGTH_64_BIT_INDEX) {
			wepConfig.setKeyStrength(IWEPConfiguration.KEY_STRENGTH_64_BIT);
		} else if(strength == KEY_STRENGTH_128_BIT_INDEX) {
			wepConfig.setKeyStrength(IWEPConfiguration.KEY_STRENGTH_128_BIT);
		}
		
		wepConfig.setPassPhrase(txtPassphrase.getText().trim());
		
		key	= txtKey1.getText().trim();
		wepConfig.setKey(KEY1_INDEX, HexHelper.stringToHex(key));
		key	= txtKey2.getText().trim();
		wepConfig.setKey(KEY2_INDEX, HexHelper.stringToHex(key));
		key	= txtKey3.getText().trim();
		wepConfig.setKey(KEY3_INDEX, HexHelper.stringToHex(key));
		key	= txtKey4.getText().trim();
		wepConfig.setKey(KEY4_INDEX, HexHelper.stringToHex(key));
	
		if(btnKey1.getSelection() == true){
			wepConfig.setKeyIndex((short)(KEY1_INDEX));
		}
		if(btnKey2.getSelection() == true){
			wepConfig.setKeyIndex((short)(KEY2_INDEX));
		}
		if(btnKey3.getSelection() == true){
			wepConfig.setKeyIndex((short)(KEY3_INDEX));
		}
		if(btnKey4.getSelection() == true){
			wepConfig.setKeyIndex((short)(KEY4_INDEX));
		}
		
	}
	
	public void enableFrame(boolean enable){
		
		parent.setEnabled(enable);
		grp.setEnabled(enable);
		
		lblStrength.setEnabled(enable);
		cmbStrength.setEnabled(enable);
		lblPassphrase.setEnabled(enable);
	   	txtPassphrase.setEnabled(enable);
	   	grpKey.setEnabled(enable);
	    btnKey1.setEnabled(enable);
	    btnKey2.setEnabled(enable);
	    btnKey3.setEnabled(enable);
	    btnKey4.setEnabled(enable);
	    txtKey1.setEnabled(enable);
	    txtKey2.setEnabled(enable);
	    txtKey3.setEnabled(enable);
	    txtKey4.setEnabled(enable);
	}

	/**
	 * @param wepInfo
	 */
	public boolean compareConfigurationWithUI(IWEPConfiguration wepInfo) {
		
		int 	strength;
		int 	index;
		int 	selectedStrength;
		String	strOldKey;
		String  strNewKey;
		
		strength			= wepInfo.getKeyStrength();
		index				= cmbStrength.getSelectionIndex();
		selectedStrength	= Integer.parseInt((String)cmbStrength.getData(""+index));
		if(selectedStrength != strength) {
			return false;
		}
			
		if(wepInfo.getKeyIndex() != (getSelectedKeyIndex())){
			return false;
		}
		
		strOldKey	= HexHelper.toHex(wepInfo.getKey(KEY1_INDEX));
		strNewKey	= txtKey1.getText().trim();
		if(strOldKey.equalsIgnoreCase(strNewKey) == false) {
			return false;
		}
		
		strOldKey	= HexHelper.toHex(wepInfo.getKey(KEY2_INDEX));
		strNewKey	= txtKey2.getText().trim();
		if(strOldKey.equalsIgnoreCase(strNewKey) == false) {
			return false;
		}
		strOldKey	= HexHelper.toHex(wepInfo.getKey(KEY3_INDEX));
		strNewKey	= txtKey3.getText().trim();
		if(strOldKey.equalsIgnoreCase(strNewKey) == false) {
			return false;
		}
		strOldKey	= HexHelper.toHex(wepInfo.getKey(KEY4_INDEX));
		strNewKey	= txtKey4.getText().trim();
		if(strOldKey.equalsIgnoreCase(strNewKey) == false) {
			return false;
		}
	
		return	true;
	}
	
	private int getSelectedKeyIndex(){
		
		if(btnKey1.getSelection() == true){
			return KEY1_INDEX;
		}
		if(btnKey2.getSelection() == true){
			return KEY2_INDEX;
			
		}
		if(btnKey3.getSelection() == true){
			return KEY3_INDEX;
		}
		if(btnKey4.getSelection() == true){
			return KEY4_INDEX;
		}
		
		return 0;
	}
	

}
