/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : SecuriryFrame.java
 * Comments : Creates security frames for VLAN and Security tab
 * Created  : Feb 20, 2007
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 20, 2007 | Created                                         | Imran   |
 * --------------------------------------------------------------------------------
 **********************************************************************************/



package com.meshdynamics.nmsui.dialogs.frames.security;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import com.meshdynamics.meshviewer.configuration.IPSKConfiguration;
import com.meshdynamics.meshviewer.configuration.IRadiusConfiguration;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.IWEPConfiguration;
import com.meshdynamics.util.IStateEventHandler;
import com.meshdynamics.util.StateManager;


public class SecurityFrame extends Canvas implements IStateEventHandler{
	
	protected   Rectangle 				minimumBounds;
	
	private 	Group					grp;
	private 	Button[] 				btnRadios;
	private		WEPFrame				wepFrame; 
	private 	WPAPersonalFrame		wpaPersonalFrame;
	private 	WPAEnterpriseFrame		wpaEnterpriseFrame;
	private     Label					lblMessage;
	private 	ISecurityFrameListener 	listener;
	private 	StateManager			sManager;
	private		int 					disableControls;
	private     boolean					fipsEnabled;
	private 	short 					parentType;
	
	public  static final short PARENT_SECURITY_CONFIG_PAGE	= 0;
	public  static final short PARENT_VLAN_CONFIG_PAGE     	= 1;
	public  static final short PARENT_FIPS_WIZARD			= 2;	 
	
	public  static final int STATE_NO_SECURITY 		= 0;
	public  static final int STATE_WEP     			= 1;
	public  static final int STATE_WPA_PERSONAL 	= 2;
	public  static final int STATE_WPA_ENTERPRISE 	= 3;
	
	private final int MAX_STATE_CNT					= 4;
	
	public final static int DISABLE_WEP								= 1;
	public final static int DISABLE_RAD_DECIDES_VLAN_CONTROLS		= 2;
	public final static int DISABLE_WPA2_MODE						= 4;
	public final static int DISABLE_WEP_KEY1						= 8;
	public final static int DISABLE_WPA_ENTERPRISE					= 16;
	public final static int DISABLE_TKIP							= 32;
	
	public static final short NO_MESSAGE 		= 0;
	public static final short UPLINK_MESSAGE 	= 1;
	public static final short VLAN_MESSAGE 		= 2;
	
	private String messageForUplink = "The data link between Meshdynamics nodes " +
							 		  "is ALWAYS encrypted using\nhardware based AES-CCMP. " +
									  "This encryption is implicit and does not\nrequire any settings. " +
									  "The above options are for Laptops/PDAs and other\nclient devices.";
	
	private String messageForVlan 	= "The data link between Meshdynamics nodes " +
	 								  "is ALWAYS encrypted using\nhardware based AES-CCMP. " +
									  "This encryption is implicit and does not\nrequire any settings. " +
									  "The above options are for Laptops/PDAs and other\nclient devices.";

	

	public SecurityFrame(Composite parent,ISecurityFrameListener listener, int disableControls, boolean fipsEnabled, short parentType) {
		
		super(parent,SWT.NONE);
		sManager 			 	=  new StateManager(this,MAX_STATE_CNT,STATE_NO_SECURITY);
		this.listener 			= listener;
		
		minimumBounds   		= new Rectangle(2,50,415,550);
		this.disableControls 	= disableControls;
		this.fipsEnabled		= fipsEnabled;
		this.parentType			= parentType; 
		createControls();
	}

	private void createControls() {
				
		int 		rel_x;
        int 		rel_y; 
        int 		numRadios;
        int 		index;
        Rectangle	bounds;
         
        rel_x 			= 15;
        rel_y 			= 110;
        
        numRadios	= 4;
        
        grp 			= new Group(this,SWT.NONE);
        grp.setBounds(0,110,415,365); 
                        
        rel_x 			+= 295;
        rel_y			-= 85;
     
		SelectionAdapter selectionAdapter = new SelectionAdapter(){
			
			SecurityFrameSelectionEvent sfsEvent = new SecurityFrameSelectionEvent();
			public void widgetSelected(SelectionEvent arg0) {
				int radioIndex;
				
				if(listener == null)
					return;

				Button btn = (Button) arg0.getSource();				
				if(btn.getSelection() == false)
					return;
				
				for(radioIndex = 0; radioIndex < btnRadios.length; radioIndex++) {
					
					if(arg0.getSource().equals(btnRadios[radioIndex])) {
						sfsEvent.setSelectedSecurity(radioIndex);
						listener.securitySelected(sfsEvent);
						notifyStateChanged(radioIndex);
						break;
					}
				}
			}
		};
	    	    
		rel_x	  -= 290;
		btnRadios =  new Button[numRadios];
	  	    
	    for(index = 0;index < numRadios;index++) {
	    	
	    	btnRadios[index] = new Button(grp,SWT.RADIO);
			btnRadios[index].setBounds(rel_x,rel_y ,100,17);
			btnRadios[index].addSelectionListener(selectionAdapter);
			rel_y			 += 20;
			setButtonText(btnRadios[index],index);
			
	    }
	    setDefaultSelection();		
	    	    
	    wepFrame 			= new WEPFrame(grp, this, fipsEnabled, disableControls);
	    bounds				= wepFrame.getMinimumBounds();
	    wepFrame.setBounds(bounds);
	       
	    wpaPersonalFrame	= new WPAPersonalFrame(grp, this, fipsEnabled, disableControls);	
	    bounds				= wpaPersonalFrame.getMinimumBounds();
	    wpaPersonalFrame.setBounds(bounds);
	    	    
	    wpaEnterpriseFrame	= new WPAEnterpriseFrame(grp, this, fipsEnabled, disableControls);
	    bounds				= wpaEnterpriseFrame.getMinimumBounds();
	    wpaEnterpriseFrame.setBounds(bounds);
	   	    
	    //showFrames(0);
	   
	    lblMessage	= new Label(grp,SWT.NONE);
	    lblMessage.setBounds(30,145,360,75);
	  	    
	    stateFunction(STATE_NO_SECURITY);
	    
	}

	/**
	 * 
	 */
	private void setDefaultSelection() {
		btnRadios[0].setSelection(true);
	}

	private void setButtonText(Button btnRadio, int i) {
		
		String labelText = "";
		
		switch(i) {
		case 0:
			labelText = "No Security";
			break; 
		case 1:
			labelText = "WEP";
			break;
		case 2:
			labelText = "WPA Personal";
			break;
		case 3:
			labelText = "WPA Enterprise";
			break;
		}
		
		btnRadio.setText(labelText);
	}
    
    public void setRadioSelection(int index,boolean enabled) {
    	btnRadios[index].setSelection(enabled);
    }

	/**
	 * @return Returns the listener.
	 */
	public ISecurityFrameListener getUiListener() {
		return listener;
	}
	
	public Rectangle getMinimumBounds(){
		return minimumBounds; 
	}
	
	public void setFrameTitle(String frmTitle){
		grp.setText(frmTitle);
	}

	public void setMinimumBounds(Rectangle bounds) {
		this.setBounds(bounds);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.IStateEventHandler#stateFunction(int)
	 */
	public int stateFunction(int state) {
		
		if(state == STATE_NO_SECURITY){
			wepFrame.setFrameVisible(false);
			wpaPersonalFrame.setFrameVisible(false);
			wpaEnterpriseFrame.setFrameVisible(false);
		}
		if(state == STATE_WEP){
			wepFrame.setFrameVisible(true);
			wpaPersonalFrame.setFrameVisible(false);
			wpaEnterpriseFrame.setFrameVisible(false);
		}
		if(state == STATE_WPA_PERSONAL){
			wpaPersonalFrame.setFrameVisible(true);
			wepFrame.setFrameVisible(false);
			wpaEnterpriseFrame.setFrameVisible(false);
		}
		if(state == STATE_WPA_ENTERPRISE){
			wpaEnterpriseFrame.setFrameVisible(true);
			wepFrame.setFrameVisible(false);
			wpaPersonalFrame.setFrameVisible(false);
		}
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.IStateEventHandler#init()
	 */
	public void init() {
		/**
		 * Will be decided by configuration security.
		 */
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.IStateEventHandler#notifyStateChanged(int)
	 */
	public void notifyStateChanged(int state) {
		sManager.triggerState(state);  
	}

	public boolean validateLocalData() {
		
		int selectedSecurity	=	getSelectedSecurity();
		if(fipsEnabled == true	&&
				selectedSecurity == STATE_NO_SECURITY) {
			showMessage("This configuration is FIPS 140-2 compliant." + SWT.LF +
					"FIPS 140-2 compliant security must be enabled. ", SWT.ICON_INFORMATION);
			return false;
		}
		if (selectedSecurity	==	STATE_WEP) {
			return wepFrame.validateLocalData();
		}else if (selectedSecurity	==	STATE_WPA_PERSONAL) {
			return wpaPersonalFrame.validateLocalData();
		}else if (selectedSecurity	==	STATE_WPA_ENTERPRISE) {
			return wpaEnterpriseFrame.validateLocalData();
		} 
		return true;
		
	}
	
	/**
	 * @param string
	 * @param icon_error
	 */
	public void showMessage(String string, int icon_error) {
		listener.showMessage(string,icon_error);
	}

	/*
	 * 
	 */
	public void setSecurityValues(ISecurityConfiguration securityInfo){
		
		int	securityType;
		
		securityType	= securityInfo.getEnabledSecurity();
		
		if(securityType == ISecurityConfiguration.SECURITY_TYPE_NONE) {
		
			setNoneSecurityValues();
		} else if(securityType == ISecurityConfiguration.SECURITY_TYPE_WEP) {
			
			IWEPConfiguration		wepInfo			    = (IWEPConfiguration)securityInfo.getWEPConfiguration();
			if(wepInfo == null) { 
				return;
			}
			setWEPValues(wepInfo);
		} else if(securityType == ISecurityConfiguration.SECURITY_TYPE_WPA_PERSONAL) {
			
			IPSKConfiguration		wpaPersonalInfo		= (IPSKConfiguration)securityInfo.getPSKConfiguration();
			if(wpaPersonalInfo == null) {
				return;
			}
			setWPAPersonalValues(wpaPersonalInfo);
		} else if(securityType == ISecurityConfiguration.SECURITY_TYPE_WPA_ENTERPRISE) {
			
			IRadiusConfiguration	wpaEnterpriseInfo	= (IRadiusConfiguration)securityInfo.getRadiusConfiguration();
			if(wpaEnterpriseInfo == null) {
				return;
			}
			setWPAEnterpriseValues(wpaEnterpriseInfo);
		}
	}
	
	public void setDefaultSettings(){
		setNoneSecurityValues();
	}
	
	/**
	 * 
	 */
	private void setNoneSecurityValues() {
		
		clearAll();
				
		selectRadio(STATE_NO_SECURITY);

	 	wpaPersonalFrame  .setFrameVisible(false);
		wepFrame		  .setFrameVisible(false);
		wpaEnterpriseFrame.setFrameVisible(false);
		
	}
	
	private void setWEPValues(IWEPConfiguration wepConfig) {
		
		if(wepConfig == null) {
			return;
		}
			
		clearAll();
		
		selectRadio(STATE_WEP);
		
		wepFrame		  .setFrameVisible(true);
		wpaPersonalFrame  .setFrameVisible(false);
		wpaEnterpriseFrame.setFrameVisible(false);
		
		wepFrame.setValues(wepConfig);
		
	}
		
	/**
	 * @param configuration
	 */
	private void setWPAPersonalValues(IPSKConfiguration wpaPersonalconfig) {
		
		if(wpaPersonalconfig == null) {
			return;
		}
		
		clearAll();
		
		selectRadio(STATE_WPA_PERSONAL);
		
		wpaPersonalFrame  .setFrameVisible(true);
		wepFrame		  .setFrameVisible(false);
		wpaEnterpriseFrame.setFrameVisible(false);
		
		wpaPersonalFrame.setValues(wpaPersonalconfig);
		
	}

	/**
	 * @param radiusConfiguration
	 */
	private void setWPAEnterpriseValues(IRadiusConfiguration radiusConfig) {
		
		if(radiusConfig == null) {
			return;
		}
		
		clearAll();
		
		selectRadio(STATE_WPA_ENTERPRISE);
		
		wpaEnterpriseFrame.setFrameVisible(true);
		wpaPersonalFrame  .setFrameVisible(false);
		wepFrame		  .setFrameVisible(false);
				
		wpaEnterpriseFrame.setValues(radiusConfig);
	}

	/**
	 * 
	 */
	public void clearAll() {
		
		selectRadio(STATE_NO_SECURITY);
		
		wepFrame		  .clearAll();
		wpaPersonalFrame  .clearAll();
		wpaEnterpriseFrame.clearAll();
		
		wepFrame		  .setFrameVisible(false);
		wpaPersonalFrame  .setFrameVisible(false);
		wpaEnterpriseFrame.setFrameVisible(false);
		
	}
	
	private void selectRadio(int radioIndex){
		
		int index;
		
		for(index=0; index<btnRadios.length; index++) {
			
			if(index == radioIndex) {
				btnRadios[index].setSelection(true);
			}else {
				btnRadios[index].setSelection(false);
			}
				
		}
		
	}

	public void showFrame(boolean enable) {
		int index;
		
		grp.setEnabled(enable);
		
		for(index = 0; index < btnRadios.length; index++){
			btnRadios[index].setEnabled(enable);
		}
		if((disableControls	& SecurityFrame.DISABLE_WEP) !=  0) {
			btnRadios[1].setEnabled(false);
		}
		if((disableControls	& SecurityFrame.DISABLE_WPA_ENTERPRISE) !=  0) {
			btnRadios[3].setEnabled(false);
		}
		
		
	}
	
	public void enableFrame(boolean enable) {
		wepFrame.enableFrame(enable);
		wpaPersonalFrame.enableFrame(enable);
		wpaEnterpriseFrame.enableFrame(enable);
	}
	
	public int getSelectedSecurity(){
		
		if(btnRadios[0].getSelection() == true) {
			return STATE_NO_SECURITY;
		}
		if(btnRadios[1].getSelection() == true) {
			return STATE_WEP;
		}
		if(btnRadios[2].getSelection() == true) {
			return STATE_WPA_PERSONAL;
		}
		if(btnRadios[3].getSelection() == true) {
			return STATE_WPA_ENTERPRISE;
		}
		
		return 0;
	}

	/**
	 * @param securityInfo
	 */
	public boolean compareConfigurationWithUI(ISecurityConfiguration securityInfo) {
		
		int curentSecurity	= getSelectedSecurity();
		
		if(curentSecurity == STATE_NO_SECURITY){
			if(securityInfo.getEnabledSecurity() != ISecurityConfiguration.SECURITY_TYPE_NONE) {
				return false;
			}else {
				return true;
			}
		}else if(curentSecurity == STATE_WEP){
			if(securityInfo.getEnabledSecurity() != ISecurityConfiguration.SECURITY_TYPE_WEP) {
				return false;
			}else {
				IWEPConfiguration		wepInfo				= (IWEPConfiguration)securityInfo.getWEPConfiguration();
				return wepFrame.compareConfigurationWithUI(wepInfo);
			}
		}else if(curentSecurity == STATE_WPA_PERSONAL){
			if(securityInfo.getEnabledSecurity() != ISecurityConfiguration.SECURITY_TYPE_WPA_PERSONAL) {
				return false;
			}else {
				IPSKConfiguration		wpaPersonalInfo		= (IPSKConfiguration)securityInfo.getPSKConfiguration();
				return wpaPersonalFrame.compareConfigurationWithUI(wpaPersonalInfo);
			}
		}else if(curentSecurity == STATE_WPA_ENTERPRISE){
			if(securityInfo.getEnabledSecurity() != ISecurityConfiguration.SECURITY_TYPE_WPA_ENTERPRISE) {
				return false;
			} else {
				IRadiusConfiguration	wpaEnterpriseInfo	= (IRadiusConfiguration)securityInfo.getRadiusConfiguration();
				return wpaEnterpriseFrame.compareConfigurationWithUI(wpaEnterpriseInfo);
			}
		}
		
		return true;
		
	}

	public void getDataFromUIToConfiguration(ISecurityConfiguration securityInfo) {
		
		int curentSecurity;
		
		curentSecurity	= getSelectedSecurity();
		
		if(curentSecurity == SecurityFrame.STATE_NO_SECURITY){
			securityInfo.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_NONE);
		}else if(curentSecurity == SecurityFrame.STATE_WEP){
			IWEPConfiguration		wepInfo				= (IWEPConfiguration)securityInfo.getWEPConfiguration();
			wepFrame.getDataFromUIToConfiguration(wepInfo);
			securityInfo.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_WEP);
		}else if(curentSecurity == SecurityFrame.STATE_WPA_PERSONAL){
			IPSKConfiguration		wpaPersonalInfo		= (IPSKConfiguration)securityInfo.getPSKConfiguration();	
			wpaPersonalFrame.getDataFromUIToConfiguration(wpaPersonalInfo);
			securityInfo.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_WPA_PERSONAL);
		}else if(curentSecurity == SecurityFrame.STATE_WPA_ENTERPRISE){
			IRadiusConfiguration	wpaEnterpriseInfo	= (IRadiusConfiguration)securityInfo.getRadiusConfiguration();
			wpaEnterpriseFrame.getDataFromUIToConfiguration(wpaEnterpriseInfo);
			securityInfo.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_WPA_ENTERPRISE);
		}
	}

	/**
	 * @param essid
	 */
	public void setEssid(String essid) {
		wpaPersonalFrame.setEssid(essid);
		
	}

	/**
	 * 
	 */
	public void showUserMessage(short messageTpye) {
		
		if(messageTpye == UPLINK_MESSAGE) {
			lblMessage.setText(messageForUplink);
		}
		if(messageTpye == VLAN_MESSAGE) {
			lblMessage.setText(messageForVlan);
		}
		if(messageTpye == NO_MESSAGE) {
			lblMessage.setText("");
		}
		
	}
	
	
	
	/**
	 * @return Returns the parentType.
	 */
	public short getParentType() {
		return parentType;
	}

	/**
	 * 
	 */
	public void generateWPAPersonalKey() {
		if(getSelectedSecurity() !=  STATE_WPA_PERSONAL) {
			return;
		}
		wpaPersonalFrame.genrateKey();
		
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.swt.widgets.Control#setVisible(boolean)
	 */
	public void setRadiosVisible(boolean visible) {
		int index;
		
		for(index = 0; index < btnRadios.length; index++){
			btnRadios[index].setVisible(visible);
		}
	}

	public void disable_wep(int controll ){
		disableControls	= controll; 
	}
	/**
	 * 
	 */
	public void notifyWPAPersonalKeyRegenerated() {
		listener.notifyWPAPersonalKeyRegenerated();
	}
}
