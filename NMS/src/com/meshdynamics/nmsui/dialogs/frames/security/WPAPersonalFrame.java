/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : WPAPersonalFrame.java
 * Comments : Creates WPAPersonalFrame for security.
 * Created  : Feb 21, 2007
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 21, 2007 | Created                                         | Imran   |
 * --------------------------------------------------------------------------------
 **********************************************************************************/
package com.meshdynamics.nmsui.dialogs.frames.security;


import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.configuration.IPSKConfiguration;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.util.HexHelper;
import com.meshdynamics.util.RSN_PSK;


public class WPAPersonalFrame {
	
	protected 	Rectangle 	minimumBounds;
	private 	Group		parent;
	
	private 	Group 		grp;  
	private 	Combo		cmbMode;
	private 	Text 		txtPassphrase;
	private		Text 		txtKey;
	private 	Text 		txtGRP;
	private     Button		btnGenerate;
	private 	Button		rdoCCCMP;
	private 	Button		rdoTKIP;
	private     Label		lblMode;
	private     Label		lblPassPhrase;
	private     Label		lblKey;
	private     Label		lblGRP;
	private     Label		lblSecs;
	
	private     String		essId;
	
	private		SecurityFrame	securityFrame;
	private     boolean			fipsEnabled;
	private 	int 			disableControls;
	
	public WPAPersonalFrame(Group parent, SecurityFrame secFrame, boolean fipsEnabled, int disableControls ){
		
		this.parent 			= parent;
		this.securityFrame		= secFrame;
		this.essId				= "";
		this.fipsEnabled		= fipsEnabled;
		this.disableControls	= disableControls; 
		
		minimumBounds   = new Rectangle(5,110,405,250);
		createControls();
	}
	
	private void createControls(){
						
		int rel_x 	= 5;
	    int rel_y 	= 20;
	        
	    grp 		= new Group(parent,SWT.NO);
		grp.setText("WPA Personal");
		
		rel_x		+= 25;
		rel_y		+= 13;
		lblMode	    = new Label(grp,SWT.NO);
		lblMode.setText("Mode  :");
		lblMode.setBounds(rel_x,rel_y,70,17);
                
        rel_x		+= 115;
        rel_y		-= 3;
        cmbMode		 = new Combo(grp,SWT.BORDER|SWT.READ_ONLY);        
        cmbMode.setBounds(rel_x,rel_y,210,17);   
        cmbMode.add("WPA");
        if(((disableControls & SecurityFrame.DISABLE_WPA2_MODE) == SecurityFrame.DISABLE_WPA2_MODE) == false){
        	cmbMode.add("WPA2/802.11i");
        }
        
        cmbMode.select(0);
        
        rel_x 		  -= 115; 
        rel_y 		  += 40;
        lblPassPhrase =	new Label(grp,SWT.NO);
        lblPassPhrase.setText("Passphrase  :");
        lblPassPhrase.setBounds(rel_x,rel_y,70,17);
                      
        rel_x		+= 115;
        txtPassphrase	=	new Text(grp,SWT.BORDER|SWT.PASSWORD);
        txtPassphrase.setBounds(rel_x,rel_y,140,17);
        if(fipsEnabled == true) {
        	txtPassphrase.setEnabled(false);
        }else {
        	txtPassphrase.setEnabled(true);
        }
        txtPassphrase.addKeyListener(new KeyListener(){

			public void keyPressed(KeyEvent arg0) {
			    			
			}
	
			public void keyReleased(KeyEvent arg0) {
				String	passPhrase	= txtPassphrase.getText().trim();
				if(passPhrase.equalsIgnoreCase("") || passPhrase == null) {
					btnGenerate.setEnabled(false);
				}else {
					btnGenerate.setEnabled(true);
				}
			}
	    });
        
        rel_x		+= 150;
        btnGenerate	= new Button(grp, SWT.PUSH);
        btnGenerate.setBounds(rel_x,rel_y,60,17);
        btnGenerate.setText("Generate");
        btnGenerate.setEnabled(false);
        btnGenerate.addSelectionListener(new SelectionAdapter(){
        	public void widgetSelected(SelectionEvent se){
        		String	passPhrase	= txtPassphrase.getText().trim();
        		if(passPhrase.length() == Mesh.length_passphrase){
        			btnGenerateHandler();
        		 }else{
        			 securityFrame.showMessage("Passphrase shoud be 10 characters. ",SWT.ICON_ERROR);
        			 txtPassphrase.setFocus();
        		 }
        	}
        });
       
        rel_x		-= 265;
        rel_y 		+= 40;
        lblKey		= new Label(grp,SWT.NO);
        lblKey.setText("Key  :");
        lblKey.setBounds(rel_x,rel_y,70,17);
          
        rel_x		+= 115;
        if(fipsEnabled == true) {
        	txtKey	= new Text(grp,SWT.BORDER);
        	txtKey.setToolTipText("WPA Personal Key should be of 64 character length, and "+SWT.LF+
			"Key should contain only \" Hexadecimal \" chararacters. i.e. 0,1,...,9 and A,B..,F");
        }else {
        	txtKey	= new Text(grp,SWT.BORDER|SWT.READ_ONLY);
        }
        txtKey.setBounds(rel_x,rel_y,210,17);
        txtKey.setEchoChar('*');
        
        rel_x 		-= 115;
        rel_y		+= 43;
        lblGRP    	= new Label(grp,SWT.NO);
        lblGRP.setText("Group Key Renewal  :");
        lblGRP.setBounds(rel_x,rel_y,110,17);
              
        rel_x		+= 115;
        txtGRP		 =	new Text(grp,SWT.BORDER);
        txtGRP.setBounds(rel_x,rel_y,210,17);
        txtGRP.setText("30");
                         
        rel_x		+= 240;
        rel_y		+= 2;
        lblSecs  	=	new Label(grp,SWT.NO); 
        lblSecs.setText("sec(s)");
        lblSecs.setBounds(rel_x-25,rel_y,35,20);
               
        rel_y 		+= 40;
        rel_x		-= 355;             
        rdoCCCMP	 =	new Button(grp,SWT.RADIO);
        rdoCCCMP.setText(" Cipher CCMP");
        rdoCCCMP.setBounds(rel_x,rel_y,100,17); 
        rdoCCCMP.setSelection(true); //default selection
        
        rel_x		+= 125;             
        rdoTKIP	 =	new Button(grp,SWT.RADIO);
        rdoTKIP.setText(" Cipher TKIP");
        rdoTKIP.setBounds(rel_x,rel_y,100,17); 
        rdoTKIP.setSelection(false); //default selection
        
        if((disableControls & SecurityFrame.DISABLE_TKIP) != 0) {
        	rdoCCCMP.setSelection(true); //default selection
        	rdoTKIP.setSelection(false); //default selection
        	rdoTKIP.setEnabled(false);
        }
	}

	public void btnGenerateHandler() {
		String oldKey;
		String newKey;
		oldKey	 = txtKey.getText();
		if((essId.equalsIgnoreCase("")) && (securityFrame.getParentType() 
				== SecurityFrame.PARENT_VLAN_CONFIG_PAGE)) {
			securityFrame.showMessage("VLAN ESSID not entered. "+SWT.LF
					+"ESSID should be entered before setting security to the VLAN. ", SWT.ICON_INFORMATION|SWT.OK);
			return;
		}
		
		genrateKey();
		btnGenerate.setEnabled(false);
		txtPassphrase.setText("");
		newKey	= txtKey.getText();
		if(oldKey.equalsIgnoreCase(newKey) == false) {
			if(securityFrame.getParentType() == SecurityFrame.PARENT_VLAN_CONFIG_PAGE) {
				securityFrame.notifyWPAPersonalKeyRegenerated();
			}
		}
		
	}
	/**
	 * @param wpaPersonalconfig
	 */
	public void setValues(IPSKConfiguration wpaPersonalconfig) {
		
		if(wpaPersonalconfig.getPSKMode() == IPSKConfiguration.WPA_PERSONAL_MODE_WPA) {
			cmbMode.select(0);
		}
		if(wpaPersonalconfig.getPSKMode() == IPSKConfiguration.WPA_PERSONAL_MODE_WPA2) {
			cmbMode.select(1);
		}
		
		txtPassphrase.setText(wpaPersonalconfig.getPSKPassPhrase());
		txtKey		 .setText(HexHelper.toHex(wpaPersonalconfig.getPSKKey()));
		txtGRP		 .setText(Integer.toString(wpaPersonalconfig.getPSKGroupKeyRenewal()));
		
		if(wpaPersonalconfig.getPSKCipherCCMP() == 1) {
			rdoCCCMP.setSelection(true);
			rdoTKIP.setSelection(false);
		}
		else if(wpaPersonalconfig.getPSKCipherTKIP() == 1) {
			rdoCCCMP.setSelection(false);
			rdoTKIP.setSelection(true);
		}
		
	}
	
	/**
	 * 
	 */
	public void getDataFromUIToConfiguration(IPSKConfiguration	wpaPersonalInfo) {
		
		short[]	key;
		String 	strOldKey;
		String 	strNewKey;
		
		if(cmbMode.getSelectionIndex() == IPSKConfiguration.WPA_PERSONAL_MODE_WPA) {
			wpaPersonalInfo.setPSKMode(IPSKConfiguration.WPA_PERSONAL_MODE_WPA);
		}
		else if(cmbMode.getSelectionIndex() == IPSKConfiguration.WPA_PERSONAL_MODE_WPA2) {
			wpaPersonalInfo.setPSKMode(IPSKConfiguration.WPA_PERSONAL_MODE_WPA2);
		}
		wpaPersonalInfo.setPSKPassPhrase(txtPassphrase.getText().trim());
		
		strOldKey	= HexHelper.toHex(wpaPersonalInfo.getPSKKey());
		strNewKey 	= txtKey.getText().trim();
		if(strOldKey.equalsIgnoreCase(strNewKey) == false) {
			securityFrame.notifyWPAPersonalKeyRegenerated();
		}
		key			= HexHelper.stringToHex(strNewKey);
		wpaPersonalInfo.setPSKKeyLength(key.length);
		wpaPersonalInfo.setPSKKey(key);
		wpaPersonalInfo.setPSKGroupKeyRenewal(Integer.parseInt(txtGRP.getText().trim()));
		
		if(rdoCCCMP.getSelection() == true){
			wpaPersonalInfo.setPSKCipherCCMP((short)1);
		}else {
			wpaPersonalInfo.setPSKCipherCCMP((short)0);
		}

		if(rdoTKIP.getSelection() == true){
			wpaPersonalInfo.setPSKCipherTKIP((short)1);
		}else {
			wpaPersonalInfo.setPSKCipherTKIP((short)0);
		}
		
	}
	
	public void setFrameBounds() {
		//this.setBounds(minimumBounds);
		
	}

	public Rectangle getMinimumBounds() {
		return minimumBounds;
	}

	public void setFrameVisible(boolean b) {
		grp.setVisible(b);
		
	}

	public void setBounds(Rectangle bounds) {
		grp.setBounds(bounds);
	}
	
	public boolean validateLocalData() {
		
		String str	=	txtKey.getText().trim();
		
		if(str.equals("") == true) {
			securityFrame.showMessage("WPA Personal Key not Generated. ",SWT.ICON_ERROR);
			txtPassphrase.setFocus();
			return false;
		}
		if(MeshValidations.isAlphanumeric(str)!= true){
			securityFrame.showMessage("WPA Personal Key incorrect.",SWT.ICON_ERROR);
			txtPassphrase.setFocus();
			return false;
		}
		
		str	=	txtGRP.getText().trim();
		if(MeshValidations.isInt(str)!=true){			
			securityFrame.showMessage("Group Key Renewal incorrect.Valid range between 0 to "+Integer.MAX_VALUE,SWT.ICON_ERROR);
			txtGRP.setFocus();
			return false;
		}
		/***************** When FIPS 140-2 is detected  *************/
		
		if(fipsEnabled == true){			
			String enteredPSKKey	=	txtKey.getText().trim();
			if(MeshValidations.isHex(enteredPSKKey)== false){
				
				securityFrame.showMessage("WPA Personal Key should be ASCII-HEX.",SWT.ICON_INFORMATION);
				return false;
			}
			//valid length for PSKKey when FIPS 140-2 is detected
			if(enteredPSKKey.length()< 64 ||
			   enteredPSKKey.length() >64 ){
				
				securityFrame.showMessage("WPA Personal Key should be 64 ASCII-HEX characters.",SWT.ICON_INFORMATION );
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 */
	public void clearAll() {
		
		 cmbMode.select(0);
		 txtPassphrase.setText("");
		 txtKey.setText("");
		 txtGRP.setText("30");
		 rdoCCCMP.setSelection(true);
		 rdoTKIP.setSelection(false);
	}

	/**
	 * @param wpaPersonalInfo
	 */
	public boolean compareConfigurationWithUI(IPSKConfiguration wpaPersonalInfo) {

		String 	strOldKey;
		String 	strNewKey;
		
		if(wpaPersonalInfo.getPSKMode() != cmbMode.getSelectionIndex()){
			return false;
		}
				
		strOldKey	= HexHelper.toHex(wpaPersonalInfo.getPSKKey());
		strNewKey 	= txtKey.getText().trim();
		if(strOldKey.equalsIgnoreCase(strNewKey) == false){
			return false;
		}
		
		if(wpaPersonalInfo.getPSKGroupKeyRenewal() != Integer.parseInt(txtGRP.getText().trim())){
			return false;
		}
				
		return true;
	}

	/**
	 * @param essid
	 */
	public void setEssid(String essid) {
		this.essId = essid;
		
	}
	
	public void genrateKey() {
		
		String 		genstr	= txtPassphrase.getText().trim();
		RSN_PSK 	pskKey 	= new RSN_PSK();				
		byte[] 		data 	= null; 		
		short [] 	shkey 	= new short[32];
		data = pskKey.PSKKeyGenerate(genstr, essId, essId.length());
		if(data == null) {
			return;
		}
		
		for(int i = 0;i < 32;i++) {  				
			shkey[i] = data[i];  				
		}
		
		String key = HexHelper.toHex(shkey);
		
		txtKey.setText("");
		txtKey.setText(key.toUpperCase());
		
	}

	/**
	 * @param enable
	 * 
	 */
	public void enableFrame(boolean enable) {
		parent.setEnabled(enable);
		grp.setEnabled(enable);
		grp		     .setEnabled(enable);  
		lblMode      .setEnabled(enable);
		cmbMode		 .setEnabled(enable);
		lblPassPhrase.setEnabled(enable);
		txtPassphrase.setEnabled(enable);
		lblKey		 .setEnabled(enable);
		txtKey		 .setEnabled(enable);
		lblGRP		 .setEnabled(enable);
		txtGRP		 .setEnabled(enable);
		lblSecs		 .setEnabled(enable);	
		if( enable == true) {
			if(fipsEnabled == true) {
				txtPassphrase.setEnabled(false);
	        	txtKey.setEnabled(true);
			} else {
				txtPassphrase.setEnabled(true);
	        	txtKey.setEnabled(false);
			}
		}else {
			txtPassphrase.setEnabled(false);
        	txtKey.setEnabled(false);
		}
		
	}

}
