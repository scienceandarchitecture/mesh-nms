package com.meshdynamics.nmsui.dialogs.frames.portmodeinfo;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import com.meshdynamics.nmsui.Branding;
import com.meshdynamics.nmsui.PortInfo;
import com.meshdynamics.nmsui.PortModelInfo;
import com.meshdynamics.nmsui.dialogs.UIConstants;

public class PortModelInfoFrame extends Canvas{

	private PortModelInfo 			pmInfo;
	private IPortModelInfoListener 	uiListener;
	
	private 	Label 			lblModel;
	private 	Label			lblImg;
	private 	Button[] 		btnRadios;
	private 	Label[]			lblRadios;
	
	protected	Rectangle		minimumBounds;
	
	private     int				lastSelectedRadioIndex;
	private		String			lastSelectedRadioName;
	
	
	public PortModelInfoFrame(Composite parent, int style,PortModelInfo pmInfo,IPortModelInfoListener listener) {
	
		super(parent, style);
		minimumBounds   		= new Rectangle(0,0,435,160);
		this.pmInfo 			= pmInfo;
		this.uiListener 		= listener;
		lastSelectedRadioName	= null;
		createControls();
	}

    private void createImage() {

        final Image image	= UIConstants.imgMeshNode;
        Canvas imageCanvas = new Canvas(this,SWT.FLAT);
        imageCanvas.setBounds(5,20,image.getBounds().width,image.getBounds().height+1);

        imageCanvas.addPaintListener(new PaintListener(){
            public void paintControl(PaintEvent arg0) {
                arg0.gc.drawImage(image,1,1 );
            }
        });

        PortInfo portInfo = null;
        
        Label lblImage = new Label(imageCanvas,SWT.FLAT);
        lblImage.setText(" A ");
        lblImage.setBounds(14,3,21,21);
        portInfo = pmInfo.getPortInfo(0);
        lblImage.setBackground(getColorForRadio(portInfo));
        
        lblImage = new Label(imageCanvas,SWT.FLAT);
        lblImage.setText(" B");
        lblImage.setBounds(93,86,21,21);
        portInfo = pmInfo.getPortInfo(1);
        lblImage.setBackground(getColorForRadio(portInfo));

        lblImage = new Label(imageCanvas,SWT.FLAT);
        lblImage.setText(" C");
        lblImage.setBounds(12,87,21,21);
        portInfo = pmInfo.getPortInfo(2);
        lblImage.setBackground(getColorForRadio(portInfo));
       
        lblImage = new Label(imageCanvas,SWT.FLAT);
        lblImage.setText(" D");
        lblImage.setBounds(91,5,22,21);
        portInfo = pmInfo.getPortInfo(3);
        lblImage.setBackground(getColorForRadio(portInfo));
        lblImage = new Label(imageCanvas,SWT.FLAT);
        
        lblImage.setText("  E");
        lblImage.setBounds(105,45,21,21);
        portInfo = pmInfo.getPortInfo(4);
        lblImage.setBackground(getColorForRadio(portInfo));
        
        lblImage = new Label(imageCanvas,SWT.FLAT);
        lblImage.setText("  F");
        lblImage.setBounds(3,45,21,21);
        portInfo = pmInfo.getPortInfo(5);
        lblImage.setBackground(getColorForRadio(portInfo));
                
        if(Branding.isMeshDyanmics() == false){
    	
	        Image iconImage = Branding.getIconImage();
			
	        lblImg	 =	new Label(imageCanvas,SWT.NO);
	        lblImg.setBounds(47,42,35,25);
	        lblImg.setBackground(UIConstants.WHITE_COLOR); 
	        lblImg.setImage(iconImage);
	        
        }

    }

	private void createControls() {
		
		createImage();
		
		int rel_x 		= 150;
		int rel_y 		= 19;
		int yOffset 	= 20;
		int xOffset		= 220;
		int numRadios 	= pmInfo.getNumOfRadioSlots();
		
		lblModel = new Label(this,SWT.FLAT);
	    lblModel.setBounds(rel_x,rel_y,113,20);
	    lblModel.setText(pmInfo.gethwModel()); 
	    
	    rel_y 	+=5;
	    
		SelectionAdapter selectionAdapter = new SelectionAdapter(){
			PortModelInfoSelectionEvent pEvent = new PortModelInfoSelectionEvent();
			public void widgetSelected(SelectionEvent arg0) {

				if(uiListener == null)
					return;

				Button btn = (Button) arg0.getSource();				
				if(btn.getSelection() == false){
					saveLastSelectedRadioIndex(btn);
					return;
				}
				
				Button lastSelectedButton = btnRadios[lastSelectedRadioIndex];
				if(lastSelectedButton.equals(btn) == true) {
					return;
				}
				
				pEvent.setSelectionEvent(arg0);
				for(int i=0;i<lblRadios.length;i++) {
					if(arg0.getSource().equals(btnRadios[i])) {
						pEvent.setRadioIndex(i);
						pEvent.setRadioInfo((PortInfo) lblRadios[i].getData());
						pEvent.setRadioData(btn.getData());
						uiListener.radioSelected(pEvent);
						break;
					}
				}
				setLastSelectedRadioIndex(getCurrentSelectedRadioIndex());
				setLastSelectedRadioName(getCurrentSelectedRadioIndex());
			}
			
		};
	    
	    rel_y += yOffset;
	    
	    Label lblIfName;
	    btnRadios = new Button[numRadios];
	    lblRadios = new Label[numRadios];
	    
	    for(int i=0;i<numRadios;i++) {

			PortInfo portInfo = pmInfo.getPortInfo(i);
			if(portInfo == null)
				continue;
	    	
	    	lblRadios[i] = new Label(this,SWT.NONE);
			lblRadios[i].setBounds(rel_x,rel_y,113,20);
			lblIfName	 = new Label(this, SWT.NONE);
			lblIfName.setBounds(rel_x+120,rel_y,60,20);
			
			btnRadios[i] = new Button(this,SWT.RADIO);
			btnRadios[i].setBounds(rel_x + xOffset,rel_y,20,20);
			btnRadios[i].addSelectionListener(selectionAdapter);
			
			rel_y += yOffset;
			
			lblRadios[i].setData(portInfo);
			setLabelText(lblRadios[i],lblIfName,i,portInfo);
			setButtonState(btnRadios[i],i,portInfo);
	    }
	    setDefaultSelection();		
	}

	/**
	 * 
	 */
	private void setDefaultSelection() {
		btnRadios[0].setSelection(true);
	}

	private void setButtonState(Button btnRadio, int i, PortInfo portInfo) {
		
		String strTypeOfRadio = getStrRadioType(portInfo); 
		if(strTypeOfRadio.equalsIgnoreCase(" "))
			btnRadio.setEnabled(false);
	}

	private void setLabelText(Label lblRadio, Label lblIfName, int i, PortInfo portInfo) {
		
		String labelText = "";
		
		switch(i) {
		case 0:
			labelText = "A. ";
			break; 
		case 1:
			labelText = "B. ";
			break;
		case 2:
			labelText = "C. ";
			break;
		case 3:
			labelText = "D. ";
			break;
		case 4:
			labelText = "E. ";
			break;
		case 5:
			labelText = "F. ";
			break;
		}
		
		String strRadioType = getStrRadioType(portInfo);
		if(!strRadioType.trim().equalsIgnoreCase("")) {
			labelText += "" + portInfo.freqOfRadio + "  " + strRadioType ;
			lblIfName.setText("( "+portInfo.ifName+" )");	
		}
		lblRadio.setText(labelText);
		
	}
	
	private String getStrRadioType(PortInfo portInfo) {

		switch(portInfo.typeOfRadio) {
			case PortModelInfo.TYPEUPLINK: 			return PortModelInfo.uplink;
			case PortModelInfo.TYPEDOWNLINK: 		return PortModelInfo.dwnlink;
			case PortModelInfo.TYPESCANNER: 		return PortModelInfo.scanner;
			case PortModelInfo.TYPECLIENTAP: 		return PortModelInfo.cliAp;
			case PortModelInfo.TYPESECONDDOWNLINK: 	return PortModelInfo.scddwnlink;
			case PortModelInfo.TYPEREGULARAP:       return PortModelInfo.regularAP;
			case PortModelInfo.TYPE900MHZ:          return PortModelInfo.n_900MHZ;
			case PortModelInfo.TYPENOTUSED: 		return " ";
			default : 								return " ";
		}
	}
	
    private Color getColorForRadio(PortInfo portInfo){

    	switch(portInfo.colorOfRadio){
    		case PortModelInfo._GREY: 
    			return UIConstants.GRAY_COLOR;
    		case PortModelInfo._WHITE:
    			return UIConstants.WHITE_COLOR;
    		case PortModelInfo._ORANGE:
    			return UIConstants.ORANGE_COLOR;		//MAGNETA changed to ORANGE
    		case PortModelInfo._YELLOW:
    			return UIConstants.YELLOW_COLOR;
    	}

       return null;

    }

    public boolean setRadioDataByIndex(int index,Object data) {
    	
    	if(index < 0 || index > pmInfo.getNumOfRadioSlots())
    		return false;
    	
    	btnRadios[index].setData(data);
		return true;
    	
    }

    public Object getRadioDataByIndex(int index) {

    	if(index < 0 || index > pmInfo.getNumOfRadioSlots())
    		return null;

    	return btnRadios[index].getData();
    }
    
    public Object getRadiodataByName(String ifName) {
    	
    	for(int i=0;i<btnRadios.length;i++) {
    		PortInfo pInfo = (PortInfo)lblRadios[i].getData();
    		if(pInfo == null)
    			continue;
    		if(pInfo.ifName.equalsIgnoreCase(ifName)) { 
    			return btnRadios[i].getData();
    		}
    	}
		return null;
    }
    
    public boolean setRadioDataByName(String ifName,Object data) {
    	
    	for(int i=0;i<btnRadios.length;i++) {
    		PortInfo pInfo = (PortInfo)lblRadios[i].getData();
    		if(pInfo == null)
    			continue;
    		if(pInfo.ifName.equalsIgnoreCase(ifName)) { 
    			btnRadios[i].setData(data);
    			return true;
    		}
    	}
		return false;
    }
    
    
    public int getRadioCount() {
    	return pmInfo.getNumOfRadioSlots();
    }
    
    public void setRadioSelection(int index,boolean enabled) {
    	btnRadios[index].setSelection(enabled);
    }

	public IPortModelInfoListener getUiListener() {
		return uiListener;
	}
	
	public PortInfo getPortInfo(int index){
		return pmInfo.getPortInfo(index);  
	}
	
	public PortInfo getPortInfo(String interfaceName){
		
		for(int i =0; i<getRadioCount(); i++) {
			
			PortInfo	pInfo	=	pmInfo.getPortInfo(i);
			if(pInfo.ifName.equals(interfaceName)){
				return pInfo;
			}
		}
		return null;
	}
	
	public Rectangle getMinimumBounds(){
		return minimumBounds;
	}
	
	private void saveLastSelectedRadioIndex(Button btn){
		for(int i = 0;i<getRadioCount();i++){
			if(btnRadios[i].equals(btn)){
				this.lastSelectedRadioIndex = i;
				PortInfo pInfo = (PortInfo)lblRadios[i].getData();
	    		if(pInfo != null) {
	    			this.lastSelectedRadioName   = pInfo.ifName;
	    		}
			}
		}
	}
	
	
	/**
	 * @return Returns the lastSelectedRadioIndex.
	 */
	public int getLastSelectedRadioIndex() {
		return lastSelectedRadioIndex;
	}
	
	public String getLastSelectedRadioName() {
		
		if(lastSelectedRadioName == null) {
			
			int currentIndex		= getCurrentSelectedRadioIndex();
			PortInfo	info		= (PortInfo)getPortInfo(currentIndex);
			lastSelectedRadioName	= info.ifName; 
		}
		return lastSelectedRadioName;
	}
	
	/**
	 * @param lastSelectedRadioIndex The lastSelectedRadioIndex to set.
	 */
	private void setLastSelectedRadioIndex(int lastSelectedRadioIndex) {
		this.lastSelectedRadioIndex = lastSelectedRadioIndex;
	}
	
	private void setLastSelectedRadioName(int i) {
	
		PortInfo	pInfo	= getPortInfo(lastSelectedRadioIndex);
		if(pInfo != null) {
			this.lastSelectedRadioName	= pInfo.ifName;
		}
	}
	
	public int getCurrentSelectedRadioIndex(){
		int index;
		for(index = 0; index < btnRadios.length; index++) {
			if(btnRadios[index].getSelection() == true){
				return index;
			}
		}
		return 0;
	}

	/**
	 * @param i
	 * @param b
	 */
	public void enableRadio(int i, boolean b) {
		btnRadios[i].setEnabled(b);
	}
}
