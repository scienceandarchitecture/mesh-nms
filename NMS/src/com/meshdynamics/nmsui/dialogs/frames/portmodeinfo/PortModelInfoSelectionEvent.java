package com.meshdynamics.nmsui.dialogs.frames.portmodeinfo;

import org.eclipse.swt.events.SelectionEvent;

import com.meshdynamics.nmsui.PortInfo;

public class PortModelInfoSelectionEvent{

	private SelectionEvent 	selectionEvent;
	private int 			radioIndex;
	private PortInfo		radioInfo;
	
	
	public PortModelInfoSelectionEvent() {
		this.selectionEvent = null;
	}
	
	public int getRadioIndex() {
		return radioIndex;
	}

	public void setRadioIndex(int radioIndex) {
		this.radioIndex = radioIndex;
	}

	public PortInfo getRadioInfo() {
		return radioInfo;
	}

	public void setRadioInfo(PortInfo radioInfo) {
		this.radioInfo = radioInfo;
	}

	public SelectionEvent getSelectionEvent() {
		return selectionEvent;
	}
	
	public void setSelectionEvent(SelectionEvent event) {
		this.selectionEvent = event;
	}

	/**
	 * @param data
	 */
	public void setRadioData(Object data) {
		// TODO Auto-generated method stub
		
	}
	
}
