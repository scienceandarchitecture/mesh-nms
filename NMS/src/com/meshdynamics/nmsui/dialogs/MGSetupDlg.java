package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.nmsui.MeshViewerProperties.MGProperties;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;

public class MGSetupDlg extends MeshDynamicsDialog {
	
	private Button 	btnModeForward;
	private Button	btnModeManage;
	private Text	txtUserName;
	private Text	txtPassword;
	private Text	txtServer;
	private Text	txtPort;
	private Button	chkUseSSL;
	private Button	chkIgnoreLocalPackets;
	
	public MGSetupDlg() {
		super();
		setTitle("Management Gateway Settings");
	}

	@Override
	protected void createControls(Canvas mainCnv) {
		int x = 15;
		int y = 15;
		
		btnModeForward = new Button(mainCnv, SWT.RADIO);
		btnModeForward.setText("Forward packets to another network");
		btnModeForward.setBounds(x, y, 270, 20);
		btnModeForward.addSelectionListener(new SelectionAdapter(){

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if(btnModeForward.getSelection() == true) {
					chkIgnoreLocalPackets.setSelection(false);
					chkIgnoreLocalPackets.setEnabled(false);
				}
					
			}
		});
		y += 30;
		
		btnModeManage = new Button(mainCnv, SWT.RADIO);
		btnModeManage.setText("Manage packets from another network");
		btnModeManage.setBounds(x, y, 270, 20);
		btnModeManage.addSelectionListener(new SelectionAdapter(){

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if(btnModeManage.getSelection() == true) {
					chkIgnoreLocalPackets.setEnabled(true);
				}
					
			}
		});
		y += 30;
		
		Label lbl = new Label(mainCnv, SWT.SIMPLE);
		lbl.setText("Server Address");
		lbl.setBounds(x, y, 100, 20);
		x += 120;
		txtServer = new Text(mainCnv, SWT.BORDER);
		txtServer.setBounds(x, y, 150, 20);
		x -= 120;
		y += 30;

		lbl = new Label(mainCnv, SWT.SIMPLE);
		lbl.setText("Port");
		lbl.setBounds(x, y, 100, 20);
		x += 120;
		txtPort = new Text(mainCnv, SWT.BORDER);
		txtPort.setBounds(x, y, 150, 20);
		x -= 120;
		y += 30;

		lbl = new Label(mainCnv, SWT.SIMPLE);
		lbl.setText("UserName");
		lbl.setBounds(x, y, 100, 20);
		x += 120;
		txtUserName = new Text(mainCnv, SWT.BORDER);
		txtUserName.setBounds(x, y, 150, 20);
		x -= 120;
		y += 30;

		lbl = new Label(mainCnv, SWT.SIMPLE);
		lbl.setText("Password");
		lbl.setBounds(x, y, 100, 20);
		x += 120;
		txtPassword = new Text(mainCnv, SWT.BORDER);
		txtPassword.setBounds(x, y, 150, 20);
		txtPassword.setEchoChar('*');
		x -= 120;
		y += 30;

		chkUseSSL = new Button(mainCnv, SWT.CHECK);
		chkUseSSL.setText("Use SSL");
		chkUseSSL.setBounds(x, y, 270, 20);
		y += 30;
		
		chkIgnoreLocalPackets = new Button(mainCnv, SWT.CHECK);
		chkIgnoreLocalPackets.setText("Ignore local incoming packets");
		chkIgnoreLocalPackets.setBounds(x, y, 270, 20);
		
		ContextHelp.addContextHelpHandler(mainCnv ,contextHelpEnum.DLGMGPROPERTIES);
		
		setDefaultValues();
		
	}

	private void setDefaultValues() {
		if(MGProperties.mode == MGProperties.MODE_FORWARDER) { 
			btnModeForward.setSelection(true);
			chkIgnoreLocalPackets.setEnabled(false);
		} else {
			btnModeManage.setSelection(true);
			chkIgnoreLocalPackets.setEnabled(true);
		}
		
		txtServer.setText(MGProperties.server);
		txtPort.setText(""+MGProperties.port);
		txtUserName.setText(MGProperties.userName);
		txtPassword.setText(MGProperties.password);
		chkUseSSL.setSelection(MGProperties.useSSL);
		chkIgnoreLocalPackets.setSelection(MGProperties.ignoreLocalPackets);
	}

	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x 		= 0;
		bounds.y		= 0;
		bounds.width 	= 300;
		bounds.height 	= 250;
	}

	@Override
	protected boolean onCancel() {
		return true;
	}

	@Override
	protected boolean onOk() {
		
		if(doValidations() == false)
			return false;
		
		MGProperties.mode 		= (btnModeForward.getSelection() == true) ?
							MGProperties.MODE_FORWARDER : MGProperties.MODE_REMOTE_MANAGER; 
		MGProperties.userName 	= txtUserName.getText().trim();
		MGProperties.password 	= txtPassword.getText().trim();
		MGProperties.server		= txtServer.getText().trim();
		MGProperties.port		= Integer.parseInt(txtPort.getText().trim());
		MGProperties.useSSL		= chkUseSSL.getSelection();
		MGProperties.ignoreLocalPackets = (MGProperties.mode == MGProperties.MODE_REMOTE_MANAGER) ?
											chkIgnoreLocalPackets.getSelection() : false;
		MGProperties.writeSettings();									
		return true;
	}

	private boolean doValidations() {
		if(btnModeForward.getSelection() == false &&
			btnModeManage.getSelection() == false) {
			showErrorMessage("Please select mode of operation", 0);
			return false;
		}
		
		String txt;
		
		txt = txtUserName.getText(); 
		if(txt == null) {
			showErrorMessage("Please enter username.", 0);
			return false;
		} else if(txt.trim().equals("") == true) {
			showErrorMessage("Please enter username.", 0);
			return false;
		}
		
		txt = txtServer.getText(); 
		if(txt == null) {
			showErrorMessage("Please enter server address.", 0);
			return false;
		} else if(txt.trim().equals("") == true) {
			showErrorMessage("Please enter server address.", 0);
			return false;
		}

		txt = txtPort.getText(); 
		if(txt == null) {
			showErrorMessage("Please enter port.", 0);
			return false;
		} else if(txt.trim().equals("") == true) {
			showErrorMessage("Please enter port.", 0);
			return false;
		}
		
		try {
			Integer i = Integer.parseInt(txt.trim());
			if(i < 0) {
				showErrorMessage("Please enter port > 0.", 0);
				return false;
			}
		} catch(Exception e) {
			showErrorMessage("Please enter port as a number.", 0);
			return false;
		}
		
		return true;
	}

}
