package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.nmsui.MDBuildServer;
import com.meshdynamics.nmsui.MDManServer;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;

public class ServerSetupDlg extends MeshDynamicsDialog {

	private  Button  btnMDBuild;
	private  Button  btnMDMan;
	private  Text	 txtServer;
	private  Text	 txtPort;  
	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x 		= 0;
		bounds.y		= 0;
		bounds.width 	= 300;
		bounds.height 	= 250;

	}

	@Override
	protected void createControls(Canvas mainCnv) {
	
		int x = 15;
		int y = 15;
		
		btnMDBuild = new Button(mainCnv, SWT.RADIO);
		btnMDBuild.setText("Server Settings for MDBuild Server");
		btnMDBuild.setBounds(x, y, 270, 20);
		btnMDBuild.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				txtServer.setText(MDBuildServer.server);
				txtPort.setText(""+MDBuildServer.port);
				}});
		y += 40;

		btnMDMan = new Button(mainCnv, SWT.RADIO);
		btnMDMan.setText("Server Settings for MDMan Server");
		btnMDMan.setBounds(x, y, 270, 20);
		btnMDMan.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				txtServer.setText(MDManServer.server);
				txtPort.setText(""+MDManServer.port);
				}});
		y += 40;
		Label lbl = new Label(mainCnv, SWT.SIMPLE);
		lbl.setText("Server Address");
		lbl.setBounds(x, y, 100, 20);
		x += 120;
		txtServer = new Text(mainCnv, SWT.BORDER);
		txtServer.setBounds(x, y, 150, 20);
		x -= 120;
		y += 30;

		lbl = new Label(mainCnv, SWT.SIMPLE);
		lbl.setText("Port");
		lbl.setBounds(x, y, 100, 20);
		x += 120;
		txtPort = new Text(mainCnv, SWT.BORDER);
		txtPort.setBounds(x, y, 150, 20);
		ContextHelp.addContextHelpHandler(mainCnv ,contextHelpEnum.DLGMGPROPERTIES);
		setDefaultValues();
	}
	private void setDefaultValues() {
		btnMDBuild.setSelection(true);
		txtServer.setText(MDBuildServer.server);
		txtPort.setText(""+MDBuildServer.port);
		}
	@Override
	protected boolean onOk() {
		if(doValidations() == false)
			return false;
		if(btnMDBuild.getSelection() == true){
			MDBuildServer.server		= txtServer.getText().trim();
			MDBuildServer.port		= Integer.parseInt(txtPort.getText().trim());
			MDBuildServer.writeSettings();
		}else{
			MDManServer.server		= txtServer.getText().trim();
			MDManServer.port		= Integer.parseInt(txtPort.getText().trim());
			MDManServer.writeSettings();
		}
		return true;
	}

	@Override
	protected boolean onCancel() {
		
		return true;
	}
	private boolean doValidations() {
				
		String txt;		
		txt = txtServer.getText(); 
		if(txt == null) {
			showErrorMessage("Please enter server address.", 0);
			return false;
		} else if(txt.trim().equals("") == true) {
			showErrorMessage("Please enter server address.", 0);
			return false;
		}

		txt = txtPort.getText(); 
		if(txt == null) {
			showErrorMessage("Please enter port.", 0);
			return false;
		} else if(txt.trim().equals("") == true) {
			showErrorMessage("Please enter port.", 0);
			return false;
		}
		
		try {
			Integer i = Integer.parseInt(txt.trim());
			if(i < 0) {
				showErrorMessage("Please enter port > 0.", 0);
				return false;
			}
		} catch(Exception e) {
			showErrorMessage("Please enter port as a number.", 0);
			return false;
		}
		
		return true;
	}

}
