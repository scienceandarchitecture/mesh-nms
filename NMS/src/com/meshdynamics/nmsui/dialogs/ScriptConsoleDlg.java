package com.meshdynamics.nmsui.dialogs;

import java.io.PrintStream;
import java.io.StringReader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.api.script.ScriptExecEnv;
import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.nmsui.MeshViewerUI;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;

public class ScriptConsoleDlg extends MeshDynamicsDialog {
	
	private Button			optJavaScript;
	private Button			optRuby;
	private Text 			txtEditor;
	private Text			txtOutConsole;
	private Text			txtErrConsole;
	private Button			btnRun;
	private TabFolder		consoleFolder;
	private ScriptExecEnv	execEnv;
	private String 			engineName;
	private String 			scriptText;
	private Button			btnClear;
	private Button			btnClearConsole;
	
	public ScriptConsoleDlg() {
		super();
		setTitle("Script Execution Console");
		setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK);
		ignoreDefaultReturnKeyPress(true);
		execEnv = null;
	}
	
	@Override
	protected void createControls(Canvas mainCnv) {
		
		int relX = 15;
		int relY = 15;
		
		optJavaScript = new Button(mainCnv, SWT.RADIO);
		optJavaScript.setText("Java Script");
		optJavaScript.setBounds(relX, relY, 100, 20);
		optJavaScript.setSelection(true);
		
		optRuby = new Button(mainCnv, SWT.RADIO);
		optRuby.setText("Ruby");
		optRuby.setBounds(relX+120, relY, 100, 20);
		
		btnRun = new Button(mainCnv, SWT.PUSH);
		btnRun.setText("Run Script");
		btnRun.setBounds(425, relY, 70, 20);
		btnRun.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				engineName = (optJavaScript.getSelection() == true) ? "js" : "jruby";
				scriptText = txtEditor.getText();
				onRun();
			}
			
		});
	
		btnClear = new Button(mainCnv, SWT.PUSH);
		btnClear.setText("Clear");
		btnClear.setBounds(350, relY, 70, 20);
		btnClear.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				txtEditor.setText("");
			}
		});
		
		relY += 25;

		txtEditor = new Text(mainCnv, SWT.MULTI|SWT.BORDER|SWT.H_SCROLL|SWT.V_SCROLL|SWT.WRAP);
		txtEditor.setBounds(relX, relY, 480, 400);
		relY += 410;
	
		btnClearConsole = new Button(mainCnv, SWT.PUSH);
		btnClearConsole.setText("Clear Output Console");
		btnClearConsole.setBounds(375, relY, 120, 20);
		btnClearConsole.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				txtOutConsole.setText("");
				txtErrConsole.setText("");
			}
			
		});
		relY += 5;
		
		consoleFolder = new TabFolder(mainCnv, SWT.SIMPLE);
		consoleFolder.setBounds(relX, relY, 480, 200);
		relY += 160;
		
		txtOutConsole = new Text(consoleFolder, SWT.MULTI|SWT.BORDER|SWT.V_SCROLL|SWT.H_SCROLL);
		TabItem tabItem = new TabItem(consoleFolder, SWT.SIMPLE);
		tabItem.setText("System.Out");
		tabItem.setControl(txtOutConsole);
		
		txtErrConsole = new Text(consoleFolder, SWT.MULTI|SWT.BORDER|SWT.V_SCROLL|SWT.H_SCROLL);
		tabItem = new TabItem(consoleFolder, SWT.SIMPLE);
		tabItem.setText("System.Err");
		tabItem.setControl(txtErrConsole);
		
		setDefaultButton(btnRun);
	}

	private void onRun() {
		
		PrintStream originalSysOut = System.out;
		
		System.setOut(new PrintStream(System.out){
			@Override
			public void print(final String s) {
				MeshViewerUI.display.asyncExec(new Runnable(){
					@Override
					public void run() {
						txtOutConsole.append(s + "\n");
					}
				});
			}
		});
		
		PrintStream originalSysErr = System.err;
		
		System.setErr(new PrintStream(System.err){
			@Override
			public void print(final String s) {
				MeshViewerUI.display.asyncExec(new Runnable(){
					@Override
					public void run() {
						txtErrConsole.append(s + "\n");
					}
				});
			}
		});

		try {
			execEnv 			= new ScriptExecEnv(engineName);
			StringReader reader = new StringReader(scriptText);
			execEnv.execute(reader);
		} catch (Exception e) {
			Shell shell = new Shell(Display.getDefault());
			MessageBox messageBox = new MessageBox(shell, SWT.OK|SWT.ICON_ERROR);
			messageBox.setText(" Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			messageBox.setMessage(e.getMessage());
			messageBox.open();
		}
		
		System.setOut(originalSysOut);
		System.setErr(originalSysErr);
	}

	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width 	= 500;
		bounds.height 	= 650;
	}

	@Override
	protected boolean onCancel() {
		return true;
	}

	@Override
	protected boolean onOk() {
		return true;
	}

}
