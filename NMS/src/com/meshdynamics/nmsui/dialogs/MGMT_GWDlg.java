package com.meshdynamics.nmsui.dialogs;

import java.util.Enumeration;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.util.MacAddress;

public class MGMT_GWDlg extends MeshDynamicsDialog{

	private Button   mgmt_gw_Enabled;
	private Text	 txtServer;
	private Button   wsButton;
	private Button   httpButton;
	private Button   httpsButton;
	
	private String  gatewayAddress;
	private boolean enabled;
	private Enumeration<String>    aps;
	private MeshNetwork            meshNetwork;
	public MGMT_GWDlg(MeshNetwork meshNetwork,Enumeration<String> aps){
		super();
		this.aps         = aps;
		this.meshNetwork = meshNetwork;
		setTitle("Management Gateway Credentials");
	}
	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x 		= 0;
		bounds.y		= 0;
		bounds.width 	= 300;
		bounds.height 	= 180;
	}

	@Override
	protected void createControls(Canvas mainCnv) {
		
		int x = 20;
		int y = 15;
		mgmt_gw_Enabled =new Button(mainCnv,SWT.CHECK);
		mgmt_gw_Enabled.setBounds( x, y, 140, 20);
		mgmt_gw_Enabled.setText("Enable Remote NMS");
		mgmt_gw_Enabled.setSelection(true);
		y += 30;
		Label lbl = new Label(mainCnv, SWT.SIMPLE);
		lbl.setText("Server Address");
		lbl.setBounds(x, y, 80, 25);
		x += 80;
		txtServer = new Text(mainCnv, SWT.BORDER);
		txtServer.setBounds(x, y, 200, 25);
       	y += 30; 
		x -=80;
		wsButton =new Button(mainCnv,SWT.RADIO);
		wsButton.setBounds( x, y, 100, 25);
		wsButton.setText("use WS");
		wsButton.setSelection(true);
		wsButton.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(httpButton.getSelection() == false &&
						wsButton.getSelection() == false &&
						httpsButton.getSelection() ==false) {				
					showErrorMessage("Either WS or HTTP or HTTPS should be selected.",SWT.ICON_ERROR);
					wsButton.setSelection(true);
					return;
				}
			}
		});
		y +=30;
		httpButton =new Button(mainCnv,SWT.RADIO);
		httpButton.setBounds( x, y, 100, 20);
		httpButton.setText("use HTTP");
		httpButton.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(httpButton.getSelection() == false &&
						wsButton.getSelection() == false &&
						httpsButton.getSelection() ==false) {				
					showErrorMessage("Either WS or HTTP or HTTPS should be selected.",SWT.ICON_ERROR);
					httpButton.setSelection(true);
					return;
				}
			}
		});
		y +=30;
		httpsButton =new Button(mainCnv,SWT.RADIO);
		httpsButton.setBounds( x, y, 100, 20);
		httpsButton.setText("use HTTPS");
		httpsButton.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(httpButton.getSelection() == false &&
						wsButton.getSelection() == false &&
						httpsButton.getSelection() ==false) {				
					showErrorMessage("Either WS or HTTP or HTTPS should be selected.",SWT.ICON_ERROR);
					httpsButton.setSelection(true);
					return;
				}
			}
		});
		/*Label expath = new Label(mainCnv, SWT.SIMPLE);
		expath.setText("eg:ws://<IP>/path or http://<IP>/path");
		expath.setBounds(x, y, 100, 25);*/
		/*MessageBox msgBox = new MessageBox(new Shell(), SWT.OK);
		msgBox.setText("No accesspoint available");
		msgBox.setMessage("Selection does not contain accesspoints available for Remote.");
		msgBox.open();*/
		return;
	}

	@Override
	protected boolean onOk() {
		if(doValidations() == false)
			return false;	
		String serverAddress=getDatafromUI();
		  setManagementgateway(serverAddress,isEnabled());
		return true;
	}

	@Override
	protected boolean onCancel() {		
		return true;
	}
	private boolean doValidations(){
		String txt=null;
		if(mgmt_gw_Enabled.getSelection() == true){
		txt = txtServer.getText(); 
		/*if(validate(txt))
			showErrorMessage("Please enter correct proper IP Address.", 0);*/
		if(txt == null) {
			showErrorMessage("Please enter server address.", 0);
			return false;
		} else if(txt.trim().equals("") == true) {
			showErrorMessage("Please enter server address.", 0);
			return false;
		}
		}
		return true;
	}
	public String getGatewayAddress() {
		return gatewayAddress;
	}
	public void setGatewayAddress(String gatewayAddress) {
		this.gatewayAddress = gatewayAddress;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	private String  getDatafromUI(){
		String serevrAddrress=null;
		      gatewayAddress = txtServer.getText();
		  if(mgmt_gw_Enabled.getSelection() == true)
			  setEnabled(true);
		  else 
			  setEnabled(false);
		  if(wsButton.getSelection() == true)
			  serevrAddrress="ws://"+gatewayAddress+"/ws";
		  else if(httpButton.getSelection() == true)
			  serevrAddrress="http://"+gatewayAddress+"/ws";
		  else if(httpsButton.getSelection() == true)
			  serevrAddrress="https://"+gatewayAddress+"/ws";
			  
		  
		  return serevrAddrress;
	}
	private void setManagementgateway(String gateway,boolean enabled){
		MacAddress macAddress=new MacAddress();
		 while(aps.hasMoreElements()) { 			
	 			String strMacAddr	= (String)aps.nextElement();
	 			macAddress.resetAddress();    	 
	 			macAddress.setBytes(strMacAddr);
	 			AccessPoint accessPoint=meshNetwork.getAccessPoint(macAddress);
	 			accessPoint.moveNode_localNMSTOremoteNMS(gateway, enabled);
	    	 }
	}
	
	private static final Pattern PATTERN = Pattern.compile(
	        "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

	public static boolean validate(final String ip) {
	    return PATTERN.matcher(ip).matches();
	} 
}
