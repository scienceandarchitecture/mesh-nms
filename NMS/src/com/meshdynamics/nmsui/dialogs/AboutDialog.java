
/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : AboutDialog.java
 * Comments : About Dialog for MeshViewer
 * Created  : Oct 8, 2004
 * Author   : Sneha Puranam
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  7  |Feb 23, 2009 | Changed minor version			                 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  6  |May 1, 2007  | Changes due to MeshDynamicsDialog               |Abhishek|
 * --------------------------------------------------------------------------------
 * |  5  |Jan 4, 2007  | Branding- copyrights problem fixed              |Abhishek|
 * --------------------------------------------------------------------------------
 * |  4  |Jul 10, 2006 | Content changed.            					 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  3  |Mar 25, 2005 | Images moved to branding native dll.            | Anand  |
 * --------------------------------------------------------------------------------
 * |  2  |Feb 02, 2005 | Changes for Customized MeshViewer               | Anand  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 01, 2005 | Icons moved as resource                         | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 8, 2004  | Created                                         | Sneha  |
 * --------------------------------------------------------------------------------
**********************************************************************************/
 
package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.nmsui.Branding;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.util.MeshViewerGDIObjects;

public class AboutDialog extends MeshDynamicsDialog {

    private int 		leftIndent;
   
    public AboutDialog(){
		super();   
	    super.setTitle(Branding.getBrandTitle() + " Network Viewer");
	    super.setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_NONE);
    }
	
	private void addData(Canvas mainCanvas) {
		
	    int y 				= 50;
	    int verticalSpacing = 20;
	    int controlHeight 	= 20;
	    
	    leftIndent 			= 0;
	    
		final Image image = Branding.getAboutImage();
		Canvas imageCanvas = new Canvas(mainCanvas,SWT.NONE);
		imageCanvas.setBounds(leftIndent,0, 126,400);
		imageCanvas.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		imageCanvas.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent arg0) {
				arg0.gc.drawImage(image,0,0 );
		   }
		});
	    
		leftIndent += 126 + 15;
		
		Label label = new Label(mainCanvas,SWT.BOLD);
		label.setText(Branding.getBrandTitle() + " Network Viewer");
		label.setBounds(leftIndent, y, 370, controlHeight);
		label.setFont(MeshViewerGDIObjects.boldVerdanaFont12);
		
		y += verticalSpacing + 10;
		
		label = new Label(mainCanvas,SWT.BOLD);
		label.setText("Version : " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor + "5");
		label.setBounds(leftIndent, y, 70, controlHeight);
		
		y += verticalSpacing;
		
		label = new Label(mainCanvas,SWT.BOLD);
		label.setText("Build     : " + NMS_VERSION2.buildid);
		label.setBounds(leftIndent, y, 70, controlHeight);
	
		y += verticalSpacing;
	
		Text txtLicense = new Text(mainCanvas,SWT.BORDER|SWT.MULTI|SWT.V_SCROLL|SWT.READ_ONLY);
		txtLicense.setBounds(leftIndent,y,350,150);

		String str = "This computer program and underlying concepts are protected by\n";
		str += "patent and copyright laws.Unauthorized reproduction or\n";
		str += "distribution of this program,or any portion of it, may result in\n";
		str += "severe civil and criminal penalties and will be prosecuted\n";
		str += "to the maximum extent.\n\n";
		str += "The software is provided \"as is\", without warranty of any kind,\n";
		str += "express or implied,including but not limited to the warranties of\n";
		str += "merchantability,";
		str += "fitness for a particular purpose and \n";
		str += "noninfringement.In no event shall the contibutors or copyright\n";
		str += "holders be liable for any claim, damages or other liability, \n";
		str += "whether in an action of contract, tort or otherwise, arising from,\n";
		str += "out of or in connection with the software or the use or other\n";
		str += "dealings in the software.\n\n\n"; 
		str += "Copyright \u00A9 2004-2014 MeshDynamics Inc. All Rights Reserved.\n\n\n";
		str += "This product uses Java\u00AE.\n";
		str += "Java\u00AE is a registered trademark of Sun Microsystems.\n";
		str += "For more information please visit http://java.sun.com\n\n\n";
		str += "This product uses IPerf.\n";
		str += "IPerf is copyrighted by the University of Illinois.\n";
		str += "Copyright \u00A9 1999-2008, The Board of Trustees of the University\n";
		str += "of Illinois.For more information please visit \n";
		str += "http://dast.nlanr.net/Projects/Iperf/ui_license.html \n\n\n";
		str += "This product uses OpenStreetMap.\n";
		str += "OpenStreetMap is a free editable map of the whole world.\n";
		str += "For more information please visit\n";
		str += "http://openstreetmap.org";

		txtLicense.setText(str);
		Group sepGrp	= new Group(mainCanvas,SWT.BORDER);
		
		if(Branding.isMeshDyanmics() == false){
			txtLicense.setBounds(leftIndent,y,350,250);
			y += 150 + verticalSpacing;
			sepGrp.setBounds(leftIndent,356,385,2);
			return;
		}
		y += 150 + verticalSpacing;
		sepGrp.setBounds(leftIndent,355,385,2);
		/*If isMeshdyanmics is true then only display the following information*/
		label = new Label(mainCanvas,SWT.BOLD);
		label.setText("Copyright \u00A9 2014 " + Branding.getBrandTitle());
		label.setBounds(leftIndent, y, 360, controlHeight);
		
		y += verticalSpacing;
	
			label = new Label(mainCanvas,SWT.BOLD);
			label.setText("Powered by Meshdynamics StructuredMesh \u2122");
			label.setBounds(leftIndent, y, 420, controlHeight);
			label.setFont(MeshViewerGDIObjects.boldVerdanaFont8);
		
		
		y += verticalSpacing;
		
			
		final Label hyperLink = new Label(mainCanvas,SWT.BOLD);
		hyperLink.setText("Visit : http://www.meshdynamics.com");
		hyperLink.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
		hyperLink.setBounds(leftIndent,y,220,controlHeight);
		hyperLink.setCursor(new Cursor(null,SWT.CURSOR_HAND));
		
		hyperLink.addMouseListener(new MouseAdapter(){

			public void mouseDown(MouseEvent arg0) {
	
				try {
					String cmd = new String("rundll32 url.dll,FileProtocolHandler http://www.meshdynamics.com");				
					Runtime.getRuntime().exec(cmd);
				} catch (Exception e) {
					Mesh.logException(e);
				}				
				
			}

		});

		}		
	
    
    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
     */
    public void createControls(Canvas mainCnv) {
		addData(mainCnv);
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
     */
    public boolean onOk() {
        // TODO Auto-generated method stub
        return true;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
     */
    public boolean onCancel() {
        // TODO Auto-generated method stub
        return true;
    }

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 510;
		bounds.height	= 400;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

}
