package com.meshdynamics.nmsui.dialogs;

import java.net.InetAddress;
import java.util.Enumeration;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.ICommandExecutionListener;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.util.Event;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

public class GPSUpdateDlg extends MeshDynamicsDialog implements ICommandExecutionListener {

    private Button			 	btnStart;
    private Table				tblAPs;
    private Label               label;
    private ProgressBar			pbTotal;
    private Label 				lblTotalProgress;
    private Button				btnRetry;
    private Button				chkEnableGPS;
    private Group 				grp;	
	private Vector<AccessPoint>	apVector;
	private MeshNetwork			meshNetwork;
	private Enumeration<String> enumMacStr;
	private Canvas				mainCnv;
 	private Thread				updaterThread;
 	private boolean				stopUpdate;
 	
 	private AccessPoint			currentAp;
 	private TableItem			currentTableItem;
 	private Event				updateEvent;
 	private boolean				enableGPS;
 	
	static final String 	ALCONFSET_CMD_ENABLE	= "alconfset gps 1 /dev/ttyS0 0.0.0.0 0 0";
	static final String 	ALCONFSET_CMD_DISABLE	= "alconfset gps 0";
	static final String 	ALCONFSET_SAVE			= "alconfset save";
 	
	public GPSUpdateDlg(MeshNetwork meshNetwork, Enumeration<String> enumMacStr) {
		this.meshNetwork 		= meshNetwork;
		this.enumMacStr			= enumMacStr;
		apVector 				= new Vector<AccessPoint>();
		updateEvent				= new Event();
		enableGPS				= true;
		updateEvent.setEvent();
		showStatusbar(true);
	}
	
	@Override
	protected void createControls(Canvas mainCnv) {
		
		this.mainCnv = mainCnv;
		enableOkButton(false);
		setCancelButtonText("Close");
		
		grp = new Group(mainCnv,SWT.NONE);
		grp.setBounds(10,5,475,280);
		
		label = new Label(grp,SWT.BOLD);
		label.setText("Press Start to begin.");
		label.setBounds(15, 13, 210, 20);
		
		btnStart = new Button(grp,SWT.PUSH);
		btnStart.setBounds(15, 33, 75, 22);
		btnStart.setText("Start");
		btnStart.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				handleStartUpdate(apVector);
			}
		});
		
		btnRetry = new Button(grp,SWT.PUSH);
		btnRetry.setBounds(95, 33, 75, 22);
		btnRetry.setText("Retry");
		btnRetry.addSelectionListener(new SelectionAdapter(){
            public void widgetSelected(SelectionEvent arg0) {
                handleRetry();
            }
		});
		
		chkEnableGPS = new Button(grp, SWT.CHECK);
		chkEnableGPS.setBounds(180, 33, 75, 22);
		chkEnableGPS.setText("Enable GPS");
		chkEnableGPS.addSelectionListener(new SelectionAdapter(){
            public void widgetSelected(SelectionEvent arg0) {
                enableGPS = chkEnableGPS.getSelection();
            }
		});
		chkEnableGPS.setSelection(true);
		
		tblAPs = new Table(grp,SWT.BORDER|SWT.V_SCROLL|SWT.SINGLE|SWT.FULL_SELECTION);
		tblAPs.setBounds(15,75,445,150);
		tblAPs.setLinesVisible(true);
		tblAPs.setHeaderVisible(true);		
		
		TableColumn tableColumn = new TableColumn(tblAPs,SWT.LEFT);
		tableColumn.setText("Node Address");
		tableColumn.setWidth(150);
		
		tableColumn = new TableColumn(tblAPs,SWT.LEFT);
		tableColumn.setText("IP Address");
		tableColumn.setWidth(120);
		
		tableColumn = new TableColumn(tblAPs,SWT.LEFT);
		tableColumn.setText("Status");
		tableColumn.setWidth(200);		
		
		label = new Label(grp,SWT.BOLD);
		label.setText("Total");
		label.setBounds(15,240,30,20);
		
		pbTotal = new ProgressBar(grp, SWT.NONE);
		pbTotal.setBounds(50, 240, 375, 20);		
	
		lblTotalProgress = new Label(grp,SWT.BOLD|SWT.RIGHT);
		lblTotalProgress.setText("0%");
		lblTotalProgress.setBounds(425, 240, 30, 20);
		
		ContextHelp.addContextHelpHandlerEx(mainCnv,contextHelpEnum.DLGGPSWIZARD);
		
		setTitle("GPS settings update [Verifying available units...]");
		setAllControls(false);
		Display.getDefault().asyncExec(new Runnable(){
			public void run() {
				setCursor(Display.getDefault().getSystemCursor(SWT.CURSOR_WAIT));
				setupData();
				setCursor(Display.getDefault().getSystemCursor(SWT.CURSOR_ARROW));
			}
		});
		
	}

	protected void handleRetry() {
	
		if(tblAPs.getSelectionCount() <= 0) {
			handleStartUpdate(apVector);
			return;
		}

		if(tblAPs.getSelectionCount() <= 0) {
			MessageBox msgBox = new MessageBox(new Shell(), SWT.OK);
			msgBox.setText("No accesspoint selected");
			msgBox.setMessage("Please select accesspoint for retrying.");
			msgBox.open();
			return;
		}
		
		Vector<AccessPoint> aps = new Vector<AccessPoint>();
		for(TableItem item : tblAPs.getSelection()) {
			AccessPoint ap = (AccessPoint)item.getData();
			if(ap == null)
				continue;
			
			aps.add(ap);
		}
		
		if(aps.size() > 0) {
			handleStartUpdate(aps);
			return;
		}

		MessageBox msgBox = new MessageBox(new Shell(), SWT.OK);
		msgBox.setText("No accesspoint available");
		msgBox.setMessage("Selection does not contain accesspoints available for update.");
		msgBox.open();
		return;
		
	}

	protected void handleStartUpdate(Vector<AccessPoint> aps) {
		btnStart.setEnabled(false);
		btnRetry.setEnabled(false);
		enableOkButton(false);
		setCancelButtonText("Cancel");
		notifyTotalUpdate(-1, aps.size());
		startUpdate(aps, this);
	}

	protected void incrementTotalUpdateCount() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
	    		pbTotal.setSelection(pbTotal.getSelection() + 1);
	    		lblTotalProgress.setText((pbTotal.getSelection() * 100)/pbTotal.getMaximum() + "%");
			}
		});
	}
	
	protected void startUpdate(final Vector<AccessPoint> aps, final ICommandExecutionListener listener) {
		
		stopUpdate = false;
		updateEvent.resetEvent();
		
		updaterThread = new Thread(new Runnable() {

			@Override
			public void run() {
				int apNo = 0;
				
				for(AccessPoint ap :aps) {
					
					setCurrentSelectedAp(ap);
					int result 	= ap.updateGps(enableGPS, listener);
					
					if(result == Mesh.GPS_UPDATE_RETURN_SUCCESS) {
						notifyUpdateStatus("Successful");
						ap.showMessage(ap,"GPS Update["+ap.getDSMacAddress()+"]", "Successful", "");
					} else if(result == Mesh.GPS_UPDATE_RETURN_FAILED) {
						notifyUpdateStatus("Failed");
						ap.showMessage(ap,"GPS Update["+ap.getDSMacAddress()+"]", "Failed", "");
					} 
					
					notifyTotalUpdate(apNo++, aps.size());
					
					if(stopUpdate == true) {
						break;
					}
				}
				
				notifyUpdatesDone();
				updateEvent.setEvent();
			}
		});
		
		updaterThread.start();
		
	}

	protected void setCurrentSelectedAp(AccessPoint ap) {
		currentAp 			= ap;
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				for(TableItem item : tblAPs.getItems()) {
					if(item.getText().equals(currentAp.getDSMacAddress()) == true) {
						currentTableItem = item;
						item.setText(2, "Updating...");
						return;
					}
				}
			}
		});
	}

	private void setAllControls(boolean enableDisable) {
		for(Control c : grp.getChildren()) {
			c.setEnabled(enableDisable);
		}
		
		for(Control c : mainCnv.getChildren()) {
			c.setEnabled(enableDisable);
		}
	}
	
    private void setupData() {

    	tblAPs.clearAll();
    	apVector.clear();
    	
		MacAddress	apMac = new MacAddress();
		while(enumMacStr.hasMoreElements()) {
			
			String strMacAddress = enumMacStr.nextElement(); 
			apMac.setBytes(strMacAddress);
			AccessPoint ap = meshNetwork.getAccessPoint(apMac);
			if(ap == null)
				continue;

			if(ap.isRunning() == false)
				continue;
			
			TableItem item = new TableItem(tblAPs,SWT.NONE);
			item.setText(0, ap.getDSMacAddress());
			
			boolean isReachable = true;
			String 	txt = "";
			if(ap.isLocal() == false) {
				txt = "(Remote) " + ap.getIpAddress().toString();
			} else {
				IpAddress nodeIpAddress = ap.getIpAddress();
				try {
					InetAddress inetAddress = InetAddress.getByAddress(nodeIpAddress.getBytes());
					isReachable = inetAddress.isReachable(10000);
				} catch (Exception e) {
					System.out.println(e.getMessage());
					isReachable = false;
				}
				
				if(isReachable == false) {
					item.setText(1, "Unreachable");
					item.setText(2, "IP Address not reachable.");
					item.setForeground(UIConstants.GRAY_COLOR);
					ap.showMessage(ap, "GPS Update", Mesh.STATUS_FAILED, "IP Address Unreachable.");
					continue;
				}
				
				txt = nodeIpAddress.toString();
			}
			item.setText(1, txt);
			
			IVersionInfo versionInfo = ap.getVersionInfo();
			if(versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,
												IVersionInfo.MINOR_VERSION_5,
												IVersionInfo.VARIANT_VERSION_70) == false) {
				item.setText(2, "GPS update not supported.");
				item.setForeground(UIConstants.GRAY_COLOR);
				ap.showMessage(ap,"GPS Update", Mesh.STATUS_FAILED, "GPS not supported.");
				continue;
			}

			item.setText(2, "Ready for Update.");
			item.setData(ap);

			apVector.add(ap);
		}
    	
		setTitle("GPS settings update [" + apVector.size() + " unit(s)]");
		setAllControls(true);
		if(apVector.size() <= 0) {
			btnStart.setEnabled(false);
			btnRetry.setEnabled(false);
		}
	}
	
	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x 		= 100;
		bounds.y 		= 100;
		bounds.width 	= 480;
		bounds.height 	= 280;
	}

	@Override
	protected boolean onCancel() {
		if(updateEvent.WaitForSingleObject(1) == Event.NOTIFY)
			return true;
		
		stopUpdate = true;
		
		MessageBox msgBox = new MessageBox(new Shell(), SWT.OK);
		msgBox.setText("Cancelling further updates");
		msgBox.setMessage("Current update cannot be cancelled.Further updates will be cancelled after current update is finished.");
		msgBox.open();
		return false;
	}

	@Override
	protected boolean onOk() {
		return true;
	}

	void notifyUpdateStatus(final String status) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				
				showStatusText(status);
				
				if(currentTableItem == null)
					return;
				if(currentTableItem.isDisposed() == true)
					return;
				currentTableItem.setText(2, status);
				
			}
		});
	}

	private void notifyTotalUpdate(final int currentApNo, final int totalApCount) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				if(currentApNo == -1) {
		    		pbTotal.setMinimum(0);
		    		pbTotal.setMaximum(totalApCount);
		    		pbTotal.setSelection(0);
				}
				
				pbTotal.setSelection(currentApNo + 1);
				lblTotalProgress.setText((pbTotal.getSelection() * 100)/pbTotal.getMaximum() + "%");
			}
		});
	}
	
	private void notifyUpdatesDone() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				enableOkButton(true);
				btnRetry.setEnabled(true);
				btnStart.setEnabled(true);
				btnStart.setText("Restart All");
			}
		});
	}

	@Override
	public void commandExecutionResponse(AccessPoint ap, String command, String responseStr) {
		// TODO Auto-generated method stub
		
	}
	
}
