/*
 * Created on Nov 7, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.dialogs.iperf;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;



/**
 * @author Abhijit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Graph extends Canvas {

	private volatile double 	xGap;
	private volatile int		yGap;
	private volatile int		maxDataSize;
	private double				minVal;
	private double 				maxVal;
	private int 				currentRecNo;
	private Vector<ColumnData>	columns;
	private Image				backBuffer;
	private GC					backGC;
	private String				srcName;
	private String 				dstName;
	
	private static int HORIZONTAL_MARGIN	= 35;
	private static int VERTICAL_MARGIN		= 35;

	private class ColumnData {
		public Vector<Double>		data;
		public double 				avgVal;
		public double 				minVal;
		public double 				maxVal;
		public Color				avgLineColor;
		public Color				dataLineColor;
		
		ColumnData(Color dataLineColor, Color avgLineColor) {
			this.dataLineColor	= dataLineColor;
			this.avgLineColor	= avgLineColor;
			
			data 	= new Vector<Double>();
			reset();
		}
		
		void reset() {
			data.clear();
			avgVal	= 0;
			maxVal	= 0;
			minVal	= 0;
		}
		
		void addValue(double newValue) {
			data.add(new Double(newValue));
			if(minVal == 0) {
				minVal = newValue;
			} else if(newValue < minVal)
				minVal = newValue;
			
			if(newValue > maxVal)
				maxVal = newValue;
			
			avgVal = 0;
			for(Double d : data) {
				avgVal += d;
			}
			avgVal /= data.size();
			avgVal = roundDouble(avgVal, 1);
		}
	}
	
	/**
	 * @param arg0
	 * @param arg1
	 */
	public Graph(Composite arg0, int arg1) {
		super(arg0, arg1);
		
        backBuffer				= null;
        backGC					= null;
		this.addPaintListener(new PaintListener(){

			public void paintControl(PaintEvent arg0) {
				
				if(backBuffer == null) {
			        backBuffer 	= new Image(Display.getDefault(), getClientArea());
			        backGC		= new GC(backBuffer);
				}
					
				backGC.fillRectangle(backBuffer.getBounds());
				paintAxis(backGC);
				paintData(backGC);
				arg0.gc.drawImage(backBuffer,0,0);
				
			}
		});
		
		columns = new Vector<ColumnData>();
		this.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_LIST_BACKGROUND));
		srcName	= "Viewer";
		dstName	= "Node";
	}

	private void paintAxis(GC gc) {
					
		Rectangle 	r = getClientArea();
		
		gc.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GREEN));
	
		int x1 = HORIZONTAL_MARGIN;
		int y1 = r.height - VERTICAL_MARGIN;
		int x2 = r.width - HORIZONTAL_MARGIN;
		int y2 = VERTICAL_MARGIN;
		gc.drawLine(x1, y1, x2, y1);
		gc.drawLine(x1, y1, x1, y2);
		
		x1 = HORIZONTAL_MARGIN + (int)Math.round(xGap);
		y2 = y1 - 10;
		
		double dX1 = HORIZONTAL_MARGIN; 
		for(int i=0;i<maxDataSize;i++) {
			
			x1 = (int)Math.round(dX1);
			gc.drawLine(x1, y1, x1, y2);
			dX1 += xGap;
			if(x1 > x2)
				break;
		}

		gc.drawText("Rec No. "+(currentRecNo), x2-40, y1+5);
		
		x1 = HORIZONTAL_MARGIN;
		x2 = HORIZONTAL_MARGIN + 15;
		y1 = VERTICAL_MARGIN;
		
		double gapVal 	= 0;
		double val 		= 0;
		
		if(maxVal > 0 && minVal > 0) {
			gapVal 	= (maxVal - minVal) / 10;
			val 	= maxVal;
		}
		
		for(int i=0;i<10;i++) {
			gc.drawLine(x1, y1, x2, y1);
			if(gapVal != 0) {
				gc.drawText(""+val, x1-30, y1-7);
				val = roundDouble((val-gapVal), 2); 
			}
			y1 += yGap;
		}
		
		if(gapVal != 0)
			gc.drawText(""+minVal, 5, y1-7);
		
	}
	
	private double roundDouble(double d, int places) {
        return Math.round(d * Math.pow(10, (double) places)) / Math.pow(10,
            (double) places);
    }
	
	private void paintData(GC gc) {
		
		Rectangle 	r 		= getClientArea();
		int graphHeight		= (r.height - (VERTICAL_MARGIN*2));
		
		double 	ratio 	= graphHeight / (maxVal - minVal);
		
		
		int avgXPos = VERTICAL_MARGIN+5;
		int avgYPos = 5;
		
		for(int colNo=0;colNo<columns.size();colNo++) {

			ColumnData 	cData 	= columns.get(colNo);
			Double 		dVal 	= null; 
			double		x		= HORIZONTAL_MARGIN;
			int 		y		= r.height - VERTICAL_MARGIN; 
			
			Vector<Double> data = cData.data;
			
			gc.setForeground(cData.dataLineColor);
			
			int 	y1 		= 0;
			for(int i=0;i<data.size();i++) {
	
				dVal = data.get(i);
				
				y1 = (r.height - VERTICAL_MARGIN) - (int)((dVal-minVal) * ratio);
				if(i > 0) {
					double nextX = x + xGap; 
					gc.drawLine((int)x, y, (int)Math.round(nextX), y1);
					x 	+= xGap;
				}
				
				y	= y1;
				
			}
			
			if(cData.avgVal > 0) {
				String avgText = "Min : " + cData.minVal + "Mbps, Max : " + cData.maxVal + "Mbps, Avg : " + cData.avgVal + "Mbps";
				if(colNo == 0)
					avgText += ", [Direction : " + srcName + "-->" + dstName + "]";
				else if(colNo == 1)
					avgText += ", [Direction : " + dstName + "-->" + srcName + "]";
				
				gc.setForeground(cData.avgLineColor);
				gc.drawText(avgText, avgXPos, avgYPos);
				avgYPos += 15;
				y1 = (r.height - VERTICAL_MARGIN) - (int)((cData.avgVal-minVal) * ratio);
				gc.drawLine(HORIZONTAL_MARGIN, y1, r.width-HORIZONTAL_MARGIN, y1);
				gc.drawText(""+cData.avgVal, r.width-HORIZONTAL_MARGIN+5, y1-5);
			}
			
		}
		
		
	}
	
	public void showAverage(int columnIndex, double avg) {
		ColumnData data = columns.get(columnIndex);
		data.avgVal = roundDouble(avg, 1);
		redraw();
	}
	
	public void addValue(int columnIndex, double newValue) {
		
		double newVal = roundDouble(newValue, 2);
		
		ColumnData cData = columns.get(columnIndex);
		cData.addValue(newVal);

		int dataSize = cData.data.size(); 
		currentRecNo = dataSize;
		
		if(maxDataSize < dataSize) {
			maxDataSize	= dataSize;
			xGap		= (double)(getBounds().width - (2*HORIZONTAL_MARGIN)) / (double)maxDataSize;
		}
		
		if(newVal > maxVal)
			maxVal = newVal;
		
		if(minVal == 0)
			minVal = newVal;
		else if(newVal < minVal)
			minVal = newVal;
		
		yGap = (getBounds().height - (2*VERTICAL_MARGIN)) / 10;
		
		redraw();
		
	}
	
	public void addColumn(Color dataLineColor, Color avgLineColor) {
		columns.add(new ColumnData(dataLineColor, avgLineColor));
	}
	
	public void reset() {
		columns.clear();
		xGap			= 0;
		maxDataSize		= 0;
		maxVal 			= 0;
		minVal 			= 0;
		currentRecNo	= 0;
		redraw();
	}

	public void setSrcName(String srcName) {
		this.srcName = srcName;
	}

	public void setDstName(String dstName) {
		this.dstName = dstName;
	}
}
