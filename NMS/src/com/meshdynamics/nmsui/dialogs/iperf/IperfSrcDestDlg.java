package com.meshdynamics.nmsui.dialogs.iperf;

/*
 * Created on Feb 16, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

import java.io.BufferedReader;
import java.io.StringReader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.ICommandExecutionListener;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.nmsui.util.MeshViewerGDIObjects;

/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class IperfSrcDestDlg extends MeshDynamicsDialog implements ICommandExecutionListener{

	private Group 		grpMain;
    private Label 		lblSrcIPAddress;
    private Label 		lblDestIPAddress;
    private Label		lblRecords;
    private Text 		txtSrcIPAddress;
    private Text 		txtDestIPAddress;
    private Text		txtRecords;
    private Button		btnStart;
    private Button		btnClear;
    private	Graph		graph;
    private Button		btnSwitchPeers;
    
    private int 		timeInterval;
    private int			records;
    
    private Cursor 		waitCursor;
    
    private AccessPoint 	peer1;
    private AccessPoint 	peer2;
    
	public IperfSrcDestDlg(AccessPoint peer1, AccessPoint peer2) {
		super();
		
		this.peer1 	= peer1;
		this.peer2	= peer2;
		setButtonDisplayMode(BUTTON_DISPLAY_MODE_OK);
		showStatusbar(true);
        waitCursor	= new Cursor(Display.getCurrent(), SWT.CURSOR_WAIT);
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	protected void createControls(Canvas mainCnv) {
			
		 int rel_x	=	15;
		 int rel_y	=	15;
		 
		 grpMain = new Group(mainCnv, SWT.NONE);
         grpMain.setBounds(rel_x,rel_y, 535, 460);
         
         rel_x	=	15; 
         rel_y	+=	10;
         
         lblSrcIPAddress = new Label(grpMain, SWT.NONE);
         lblSrcIPAddress.setText("Source Node IP Address");
         lblSrcIPAddress.setBounds(rel_x, rel_y, 120, 14);
     
         rel_x		+=	150;		
		 
         txtSrcIPAddress = new Text(grpMain, SWT.BORDER);
         txtSrcIPAddress.setBounds(rel_x, rel_y, 140, 21);
         txtSrcIPAddress.setFocus();
         txtSrcIPAddress.setEnabled(false);
         
         rel_x 		=	15;
         rel_y		+=	25;
         
         lblDestIPAddress = new Label(grpMain, SWT.NONE);
         lblDestIPAddress.setText("Destination Node IP Address");
         lblDestIPAddress.setBounds(rel_x, rel_y, 140, 14);
     
         rel_x		+=	150;		
		 
         txtDestIPAddress = new Text(grpMain, SWT.BORDER);
         txtDestIPAddress.setBounds(rel_x, rel_y, 140, 21);
         txtDestIPAddress.setEnabled(false);
         
         
         btnSwitchPeers = new Button(grpMain, SWT.PUSH);
         btnSwitchPeers.setText("Switch ends");
         btnSwitchPeers.setBounds(rel_x+165, rel_y-10, 70, 20);
         btnSwitchPeers.addSelectionListener(new SelectionAdapter(){

			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				AccessPoint ap 	= peer1;
				peer1 			= peer2;
				peer2			= ap;
				
				txtSrcIPAddress.setText(peer1.getIpAddress().toString());
				txtDestIPAddress.setText(peer2.getIpAddress().toString());

			}});
         
         rel_x 		=	15;
         rel_y		+=	25;
         
         lblRecords		 = new Label(grpMain, SWT.NONE);
         lblRecords.setText("Records");
         lblRecords.setBounds(rel_x, rel_y, 120, 14);
     
         rel_x		+=	150;
         
         txtRecords		 = new Text(grpMain, SWT.BORDER);
         txtRecords.setBounds(rel_x, rel_y, 140, 21);
         txtRecords.setText("15");
         
         rel_x 		+=	225;
         
         btnStart		 = new Button(grpMain,SWT.PUSH); 	
         btnStart.setText("Start");
         btnStart.setBounds(rel_x, rel_y, 60, 24);
         btnStart.addSelectionListener(new SelectionListener(){

			public void widgetSelected(SelectionEvent arg0) {
				runMeshCommand();
			}
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
         });
         rel_x 		+=	65;
         
         btnClear		 = new Button(grpMain,SWT.PUSH); 	
         btnClear.setText("Clear");
         btnClear.setBounds(rel_x, rel_y, 60, 24);
         btnClear.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				graph.reset();
			}
         });
         
         rel_x 		=	15;
         rel_y		+=	40;
        
 	    graph = new Graph(grpMain, SWT.SIMPLE);  
	    graph.setBounds(rel_x,rel_y,500,330);
         
        ContextHelp.addContextHelpHandlerEx(mainCnv,contextHelpEnum.DLGIPERFTEST);
         
        txtSrcIPAddress.setText(peer1.getIpAddress().toString());
        txtDestIPAddress.setText(peer2.getIpAddress().toString());
	}

	/**
	 * 
	 */
	protected void runMeshCommand() {
    	
    	final String 	destIPAddress	= txtDestIPAddress.getText().trim();
    	
    	if(validateValues() == false)
    		return;
    	
    	records   		= Integer.parseInt(txtRecords.getText().trim());
    	timeInterval 	= 1;
    	final String command = "iperf -c "+ destIPAddress + " -t "+ records + " -i "+ timeInterval;      	 
    
    	super.setCursor(waitCursor);
    	btnStart.setEnabled(false);
    	btnClear.setEnabled(false);
    	btnSwitchPeers.setEnabled(false);
    	txtRecords.setEnabled(false);
        graph.reset();
    	graph.setSrcName("Src Node");
    	graph.setDstName("Dst Node");
       
    	Display d = Display.getDefault();
    	graph.addColumn(d.getSystemColor(SWT.COLOR_BLUE), d.getSystemColor(SWT.COLOR_DARK_BLUE));
    	Runnable r = new Runnable() {
    		public void run() {
    			startTest(command);
    		}
    	};
       
    	Thread thread = new Thread(r);
    	thread.start();
	}
	
	
	protected void startTest(String command) {
		String result = peer1.executeCommand(command, this);
		parseCommandResponse(result);
		
		if(isShellDisposed() == true)
			return;
		
        Display.getDefault().asyncExec(new Runnable(){
            public void run() {
        		if(isShellDisposed() == true)
        			return;
        		try {
	            	setCursor(MeshViewerGDIObjects.arrCursor);
	            	btnStart.setEnabled(true);
	            	btnClear.setEnabled(true);
	            	btnSwitchPeers.setEnabled(true);
	            	txtRecords.setEnabled(true);
        		} catch(Exception e) {
        		}
           	}
        });
		
		
 	}

	/**
	 * 
	 */
	private boolean validateValues() {
		
		if(txtSrcIPAddress.getText().trim().equals(""))
		{
			MessageBox msg = new MessageBox(new Shell(), SWT.OK|SWT.ICON_ERROR);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Source IPAddress is blank.");
			txtSrcIPAddress.setFocus();
			msg.open();
			return false;
		}
		if(txtDestIPAddress.getText().trim().equals(""))
		{
			MessageBox msg = new MessageBox(new Shell(), SWT.OK|SWT.ICON_ERROR);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Destination IPAddress is blank.");
			txtDestIPAddress.setFocus();
			msg.open();
			return false;
		}
    	if(txtRecords.getText().trim().equals(""))
		{
			MessageBox msg = new MessageBox(new Shell(), SWT.OK|SWT.ICON_ERROR);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Records are blank.");
			txtRecords.setFocus();
			msg.open();
			return false;
		}
    	
    	try {
			  records   = Integer.parseInt(txtRecords.getText().trim());
		    }
		catch(Exception e)
		{
			MessageBox msg = new MessageBox(new Shell(), SWT.OK|SWT.ICON_ERROR);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Records should be numeric!");
			txtRecords.setFocus();
			msg.open();
			return false;
		}
		return true;
	}

	private void parseCommandResponse(String responseStr) {
		
        if(super.isShellDisposed())
            return;

        try {
        	BufferedReader 	br = new BufferedReader(new StringReader(responseStr));
        	String 			line;
        	 
        	 while((line = br.readLine())!=null) {
        		 
                 if(line.startsWith("[") == false) {
                	final String statusText = line;  
          	        Display.getDefault().asyncExec(new Runnable(){
         	            public void run() {
         	            	showStatusText(statusText);
         	            }
         	        });
                 	continue;
                 }
        		 
                 try {
         	    	String[] tokens = line.split(" ");
         	    	
         	    	if(tokens.length <= 7) {
         	    		return;
         	    	}
         	    	
         	    	double val = Double.parseDouble(tokens[tokens.length-2]);
         	    	if(tokens[tokens.length-1].startsWith("Kbits")) 
         	    		val /= 1024;
         	
         	    	final double value = val;
         	        Display.getDefault().asyncExec(new Runnable(){
         	            public void run() {
         	                if(graph.isDisposed() == false)
         	                    graph.addValue(0, value);
         	            }
         	        });
                 } catch(Exception e) {
                 }

       	 	}
        	 
        } catch(Exception e) {
          System.out.println(e.getMessage());  
        }
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.height	= 465;
		bounds.width	= 550;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	protected boolean onOk() {
		// TODO Auto-generated method stub
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	protected boolean onCancel() {
		// TODO Auto-generated method stub
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void commandExecutionResponse(AccessPoint ap, String command, final String responseStr) {
        Display.getDefault().asyncExec(new Runnable(){
            public void run() {
            	parseCommandResponse(responseStr);
            }
        });
    	return;
	}
}

	

	

	
