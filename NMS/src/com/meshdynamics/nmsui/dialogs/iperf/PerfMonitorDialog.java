/**
 * MeshDynamics 
 * -------------- 
 * File     : PerfMonitorDialog.java
 * Comments : 
 * Created  : Jul 6, 2006
 * Author   : Abhijit Ayarekar 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * |  0  |Jul 6, 2006  |  Created	                                  	 | Abhijit 	 |
 * ----------------------------------------------------------------------------------
 * |  2  |May 1, 2007  | Changes due to MeshDynamicsDialog               |Abhishek   |
 * --------------------------------------------------------------------------------
 * |  1  |Jul 24, 2006  |  Enhancement	                             	 | Prachiti  |
 * ----------------------------------------------------------------------------------
 * 
 */

package com.meshdynamics.nmsui.dialogs.iperf;


import java.io.BufferedReader;
import java.io.StringReader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.api.NMS;
import com.meshdynamics.iperf.IperfListener;
import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.nmsui.MeshViewerUI;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.util.IpAddress;

public class PerfMonitorDialog extends MeshDynamicsDialog implements IperfListener {
    	
    private Group 			grpMain;
    private Label 			lblIPAddress;
    private Text 			txtIPAddress;
    private Label 			lblBandwidth;
    private Button 			optSimultaneous;
    private Button 			optIndividual;
    private Composite		composite1;
    private Button 			optDual;
    private Button 			optSingle;
    private Label 			label2;
    private Composite	 	cmpType;
    private Label 			lblProtocol;
    private Label 			lblMbps;
    private Text 			txtBandwidth;
    private Button 			optUDP;
    private Button 			optTCP;
    private Composite 		cmpProtocol;
    private Button 			btnStart;
    private Text 			txtRecords;
    private Label 			lblRecords;
    private Graph			graph;
    
    private boolean 		boolTestRunning;
    private IpAddress		nodeIpAddress;
    
    private int 			columnIndex;
    private String			previousRecNo;
	private AccessPoint		ap;
	private int 			recordCount;
	private short			type;
	private short			protocol;
	private int 			udpBandWidth;
	
    /**
     * 
     */
    public PerfMonitorDialog(MeshViewerUI meshViewerUI, AccessPoint ap) {
    	super();
        super.setTitle("Performance Test");
        setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK);
        showStatusbar(true);
        this.ap				= ap;
        boolTestRunning 	= false;
        this.nodeIpAddress 	= ap.getIpAddress();
        this.columnIndex	= 0;
        this.previousRecNo	= "";
        this.recordCount	= 15;
        this.type			= NMS.PERFORMANCE_TYPE_SINGLE;
        this.protocol		= NMS.PERFORMANCE_PROTOCOL_TCP;
        this.udpBandWidth	= 0;
    }


    /**
     * 
     */
    private void setDefaultValues() {
        if(nodeIpAddress != null)
            txtIPAddress.setText(nodeIpAddress.toString());
        
        txtRecords.setText(""+recordCount); 
        optTCP.setSelection(true);
        optSingle.setSelection(true);
        optIndividual.setSelection(true);
        setTypeControlsVisible(false);
    }
   
    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
     */
    public void createControls(Canvas mainCnv) {
    	
		grpMain = new Group(mainCnv, SWT.NONE);
		grpMain.setBounds(14, 3, 494, 526);
		
		lblIPAddress = new Label(grpMain, SWT.NONE);
		lblIPAddress.setText("Node IP Address");
		lblIPAddress.setBounds(21, 32, 84, 14);
		
		txtIPAddress = new Text(grpMain, SWT.BORDER);
		txtIPAddress.setBounds(112, 25, 140, 21);
		
		lblRecords = new Label(grpMain, SWT.NONE);
		lblRecords.setText("Records");
        lblRecords.setBounds(273, 32, 49, 14);
  
        txtRecords = new Text(grpMain, SWT.BORDER);
        txtRecords.setBounds(329, 25, 112, 21);
        
        btnStart = new Button(grpMain, SWT.PUSH | SWT.CENTER);
        btnStart.setText("Start");
	    btnStart.setBounds(418, 186, 60, 24);
	    btnStart.addSelectionListener(new SelectionAdapter(){
	        public void widgetSelected(SelectionEvent arg0) {
	
	            if(boolTestRunning) {
	                stopTest();
	            }else {
	                runTest();
	            }
	        }
	    });
	    
	    setDefaultButton(btnStart);
		  
	    cmpProtocol = new Group(grpMain, SWT.NONE);
	    cmpProtocol.setBounds(14, 50, 466, 56);
		
	    optTCP = new Button(cmpProtocol, SWT.RADIO | SWT.LEFT);
	    optTCP.setText("TCP");
	    optTCP.setBounds(91, 14, 42, 28);
	    optTCP.addSelectionListener(new SelectionAdapter(){
	    	
		    public void widgetSelected(SelectionEvent arg0) {
		    	optDual.setEnabled(true);
		    	optIndividual.setEnabled(true);
		    	optSimultaneous.setEnabled(true);
		        txtBandwidth.setEnabled(false);
		    }
		});
		  
	    optUDP = new Button(cmpProtocol, SWT.RADIO | SWT.LEFT);
	    optUDP.setText("UDP");
	    optUDP.setBounds(196, 14, 42, 28);
	    optUDP.addSelectionListener(new SelectionAdapter(){
		
	    	public void widgetSelected(SelectionEvent arg0) {
		    	optDual.setEnabled(false);
		    	optIndividual.setEnabled(false);
		    	optSimultaneous.setEnabled(false);
		        txtBandwidth.setEnabled(true);
	    	}
    	});
		  
	    lblBandwidth = new Label(cmpProtocol, SWT.NONE);
	    lblBandwidth.setText("Bandwidth");
	    lblBandwidth.setBounds(259, 22, 56, 14);
		  
	    txtBandwidth = new Text(cmpProtocol, SWT.BORDER);
	    txtBandwidth.setBounds(315, 20, 42, 21);
		txtBandwidth.setEnabled(false);
		
	    lblMbps = new Label(cmpProtocol, SWT.NONE);
	    lblMbps.setText("Mbps");
	    lblMbps.setBounds(364, 22, 35, 14);
		  
	    lblProtocol = new Label(cmpProtocol, SWT.NONE);
	    lblProtocol.setText("Protocol");
	    lblProtocol.setBounds(7, 21, 63, 14);
		  
	    cmpType = new Group(grpMain, SWT.NONE);
	    cmpType.setBounds(14, 102, 466, 82);
	    
	    label2 = new Label(cmpType, SWT.NONE);
	    label2.setText("Type");
	    label2.setBounds(7, 14, 63, 14);
		  
	    optSingle = new Button(cmpType, SWT.RADIO | SWT.LEFT);
	    optSingle.setText("Single");
	    optSingle.setBounds(91, 7, 63, 28);
	    optSingle.addSelectionListener(new SelectionAdapter(){
	    	public void widgetSelected(SelectionEvent arg0) {
	    		setTypeControlsVisible(false);
	    	}
	    });
		
	    optDual = new Button(cmpType, SWT.RADIO | SWT.LEFT);
	    optDual.setText("Dual");
	    optDual.setBounds(196, 7, 49, 28);
	    optDual.addSelectionListener(new SelectionAdapter(){
	    	public void widgetSelected(SelectionEvent arg0) {
	    		setTypeControlsVisible(true);
	    	}
	    });
		  
	    composite1 = new Composite(cmpType, SWT.NONE);
	    composite1.setBounds(196, 35, 154, 42);
		  
	    optIndividual = new Button(composite1, SWT.RADIO  | SWT.LEFT);
	    optIndividual.setText("Individual");
	    optIndividual.setBounds(7, 0, 70, 21);
		  
	    optSimultaneous = new Button(composite1, SWT.RADIO | SWT.LEFT);
	    optSimultaneous.setText("Simultaneous");
	    optSimultaneous.setBounds(7, 21, 84, 21);

	    graph = new Graph(grpMain, SWT.SIMPLE);  
	    graph.setBounds(14, 217, 466, 300);
	    
	    setDefaultValues();
    }

    /**
     * @param b
     */
    protected void setTypeControlsVisible(boolean b) {
        optIndividual.setEnabled(b);
        optSimultaneous.setEnabled(b); 
    }

    private boolean createCommand() {
     
        if(checkInputs() == false)
            return false;

        recordCount 	= Integer.parseInt(txtRecords.getText().trim());
        protocol 		= (optTCP.getSelection() == true) ? 
        					NMS.PERFORMANCE_PROTOCOL_TCP : NMS.PERFORMANCE_PROTOCOL_UDP;
        
        if(optSingle.getSelection() == true)
        	type = NMS.PERFORMANCE_TYPE_SINGLE;
        else {
        	if(optIndividual.getSelection() == true)
        		type = NMS.PERFORMANCE_TYPE_DUAL_INDIVIDUAL;
        	else
        		type = NMS.PERFORMANCE_TYPE_DUAL_SIMULTANEOUS;
        }
        
        udpBandWidth = (protocol == NMS.PERFORMANCE_PROTOCOL_UDP) ?
        				Integer.parseInt(txtBandwidth.getText().trim()) : 0;
 
        return true;
    }
    
    private boolean runTest() {
        
        if(createCommand() == false)
            return false;
     
        graph.reset();
    	Display d = Display.getDefault();
    	graph.addColumn(d.getSystemColor(SWT.COLOR_BLUE), d.getSystemColor(SWT.COLOR_DARK_BLUE));
        columnIndex = 0;
        
        if(optDual.getSelection() == true) {
        	graph.addColumn(d.getSystemColor(SWT.COLOR_RED), d.getSystemColor(SWT.COLOR_DARK_RED));
        }
        
        Thread t = new Thread(new Runnable(){
			@Override
			public void run() {
				ap.executePerformanceTest(recordCount, type, protocol, udpBandWidth, PerfMonitorDialog.this);
			}
		});
        t.start();
        
        return true;
    }

	private boolean checkInputs() {
        
        String strIpAddress = txtIPAddress.getText().trim();
        
        if(strIpAddress.equals("")) {
            MessageBox msgBox = new MessageBox(new Shell());
            msgBox.setMessage("Please enter IP address of the node.");
	        msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
	        msgBox.open();
	        return false;
        }
        
        IpAddress ipAddress = new IpAddress();
        if(ipAddress.setBytes(strIpAddress) != 0) {
            MessageBox msgBox = new MessageBox(new Shell());
            msgBox.setMessage("Invalid IP address.");
            msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
	        msgBox.open();
	        return false;
        }
        
        String strRecordCount = txtRecords.getText().trim();
        if(strRecordCount.equals("")) {
            MessageBox msgBox = new MessageBox(new Shell());
            msgBox.setMessage("Please enter number of record's to be transferred for performance test.");
            msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
	        msgBox.open();
	        return false;
        }
        
        if(MeshValidations.isLong(strRecordCount) == false) {
            MessageBox msgBox = new MessageBox(new Shell());
            msgBox.setMessage("Invalid Record Count.");
            msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
	        msgBox.open();
	        return false;
        }
        if(txtBandwidth.getEnabled() == false)
            return true;
        
        String strBandwidth = txtBandwidth.getText().trim();
        if(strBandwidth.equals("")) {
            MessageBox msgBox = new MessageBox(new Shell());
            msgBox.setMessage("Bandwidth incorrect!!!");
            msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
	        msgBox.open();
	        return false;
        }
        
        if(MeshValidations.isLong(strBandwidth) == false) {
            MessageBox msgBox = new MessageBox(new Shell());
            msgBox.setMessage("Bandwidth incorrect!!!");
            msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
	        msgBox.open();
	        txtBandwidth.setFocus(); 
	        return false;
        }
        return true;
    }

    private boolean stopTest() {
        
        if(boolTestRunning == true)
           testEnded();
        
        return true;
    }
    
    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
     */
    public boolean onOk() {
        // TODO Auto-generated method stub
        return true;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
     */
    public boolean onCancel() {
        // TODO Auto-generated method stub
        return true;
    }

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 510;
		bounds.height	= 530;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void appendResult(final String appendText) {
        
		if(super.isShellDisposed())
            return;

        if(appendText.startsWith("[") == false) {
            Display.getDefault().asyncExec(new Runnable(){
                public void run() {
                	showStatusText(appendText);
                }
            });
        	return;
        }
        
        if(appendText.startsWith("[SUM]") == false) {
        	return;
        }
        
    	String[] tokens = appendText.split(" ");
    	
    	if(tokens.length <= 7) {
    		return;
    	}
    	
    	double val = Double.parseDouble(tokens[tokens.length-2]);
    	if(tokens[tokens.length-1].startsWith("Kbits")) 
    		val /= 1024;

		if(type == NMS.PERFORMANCE_TYPE_DUAL_SIMULTANEOUS) {
    		String recNo = "";
    		if(tokens.length == 9)
    			recNo = tokens[1];
    		else if(tokens.length == 10) {
    			if(tokens[1].trim().equals(""))
    				recNo = tokens[2];
    			else
    				recNo = tokens[1];
    		} else
    			recNo = tokens[2];
    		
    		String recTokens[] = recNo.split("-");
    		recNo = recTokens[0];
    		
	    	if(previousRecNo.equalsIgnoreCase(recNo) == true) {
	    		columnIndex = 1;
	    	} else {
	    		previousRecNo = recNo;
	    		columnIndex = 0;
	    	}
			
		}  else if(type == NMS.PERFORMANCE_TYPE_DUAL_INDIVIDUAL) {
			
	    	if(tokens[2].startsWith("0.0")) {
	    		if(tokens.length == 10)
            		columnIndex = (columnIndex == 0) ? 1 : 0;
	    		else if(tokens.length == 11 && tokens[2].endsWith("-") == false)
	    			columnIndex = (columnIndex == 0) ? 1 : 0;

	    		return;
	    	}
			
		}
    			
    	final double 	value 	= val;
    	final int 		cIndex 	= columnIndex;
    	
        Display.getDefault().asyncExec(new Runnable(){
            public void run() {
                if(graph.isDisposed() == false) {
                    graph.addValue(cIndex, value);
                }
            }
        });
	}


	@Override
	public void testEnded() {
        if(super.isShellDisposed())
            return;
        
        Display.getDefault().asyncExec(new Runnable(){

            public void run() {
                if(btnStart.isDisposed() == false) {
                    boolTestRunning = false;
                    btnStart.setEnabled(true);
                }
            }
        });
	}


	@Override
	public void testStarted() {
        if(super.isShellDisposed())
            return;
        
        Display.getDefault().asyncExec(new Runnable(){

            public void run() {
                if(btnStart.isDisposed() == false) {
                    boolTestRunning = true;
                    btnStart.setEnabled(false);
                }
            }
        });
	}


	@Override
	public void testResult(final String output) {
		
        if(super.isShellDisposed())
            return;

        Display.getDefault().asyncExec(new Runnable(){

            public void run() {
            	
        		try {
        			BufferedReader reader = new BufferedReader(new StringReader(output));
        			String line = null;
        			while((line = reader.readLine()) != null) {
        				appendResult(line);	
        			}
        		} catch(Exception e) {
                    MessageBox msgBox = new MessageBox(new Shell());
                    msgBox.setMessage(e.getMessage());
                    msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
                    msgBox.open(); 
        		}
            }
        });
	}

}
