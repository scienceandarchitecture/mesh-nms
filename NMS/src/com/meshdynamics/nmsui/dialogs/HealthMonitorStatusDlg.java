/**
 * MeshDynamics 
 * -------------- 
 * File     : HealthMonitorStatusDlg.java
 * Comments : 
 * Created  : Feb 08, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 1   |May 1, 2007    | Changes due to MeshDynamicsDialog          |Abhishek      |
 * --------------------------------------------------------------------------------
 * | 0  | DEC 14, 2006   | created									  | Abhishek     |
 * ----------------------------------------------------------------------------------*/
 

package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.healthmonitor.APHealthStatus;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.util.DateTime;

/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class HealthMonitorStatusDlg extends MeshDynamicsDialog {

	private Table					tblStatus;
	private Group					grp;
	private MeshNetwork 			meshNetwork;
	
	public HealthMonitorStatusDlg(MeshNetwork currentNetwork){
		super();
		meshNetwork  = currentNetwork;
		setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK);
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	public void createControls(Canvas mainCnv) {
		
		grp = new Group(mainCnv,SWT.NO);
		grp.setBounds(5,5,455,265);

		int rel_y = 25;
		int rel_x = 15;
		
		tblStatus				= new Table(grp,SWT.V_SCROLL|SWT.BORDER|SWT.SINGLE|SWT.FULL_SELECTION);
		tblStatus.setBounds(rel_x,rel_y,425, 220); 	
		tblStatus.setHeaderVisible(true);
		tblStatus.setLinesVisible(true);
		
		String Headers[] = {"Node Name","Mac Address","Dead Since...","Dead Interval"};
		
		TableColumn col;
		
		for(int i = 0;i<Headers.length;i++ )
		{
			col  = new TableColumn(tblStatus, SWT.NONE);
			col.setText(Headers[i]);
		}
		
		col =  tblStatus.getColumn(0);
		col.setWidth(100);
		col =  tblStatus.getColumn(1);
		col.setWidth(120);
		col =  tblStatus.getColumn(2);
		col.setWidth(110);
		col =  tblStatus.getColumn(3);
		col.setWidth(90);

		setTableValues();
	}

	private void setTableValues()
	{
		APHealthStatus[] apStatusList = meshNetwork.getAPHealthStatus();
		if(apStatusList == null)
			return;
		
		for(int apNo=0;apNo<apStatusList.length;apNo++) {
		
			APHealthStatus apStatus = apStatusList[apNo];
			if(apStatus == null)
				continue;
			
			TableItem newItem;
			newItem = new TableItem(tblStatus,SWT.NONE);
			
			AccessPoint ap = apStatus.getAccessPoint();
			if(ap == null)
				continue;
			
			String 	macAddr				= ap.getDsMacAddress().toString();
			String 	apName				= ap.getName();
			long 	deathNotifyTime 	= apStatus.getDeathNotifyTime();
			long 	deadTimeInMillis 	= (System.currentTimeMillis() - deathNotifyTime);

			newItem.setText(0,""+apName);
			newItem.setText(1,""+macAddr);
			newItem.setText(2,DateTime.getDateTime(deathNotifyTime));
			newItem.setText(3,""+DateTime.getTimeSpan(deadTimeInMillis));
		}
		
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	public boolean onOk() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	public boolean onCancel() {
		return true;
	}

	public boolean onShellExit() {
		return true;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 455;
		bounds.height	= 265;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
