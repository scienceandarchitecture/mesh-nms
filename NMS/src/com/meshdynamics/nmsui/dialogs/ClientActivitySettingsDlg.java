/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ClientActivitySettingsDlg.java
 * Comments : 
 * Created  : Feb 15, 2006
 * Author   : Mithil Wane 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  4  |Jun 12, 2007 | Extended from MeshdynamicsDialog	 	  	 	 |Abhishek |
 * ----------------------------------------------------------------------------------
 * |  3  |Jul 03, 2006 | Multiple selection						  	 	 | Mithil |
 * ----------------------------------------------------------------------------------
 * |  2  |Mar 17, 2006 | Version in msg box generalised	+ sriram cd merge| Mithil |
 * ----------------------------------------------------------------------------------
 * |  1  |Feb 17, 2006 | Msg changed for Msg Box                         | Mithil |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 15, 2006 | Created                                         | Mithil |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.dialogs;

import java.util.Enumeration;
import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;


public class ClientActivitySettingsDlg extends MeshDynamicsDialog{

    private List							lstNonLoggingAp,lstLoggingAp ;
    private Button							btnAdd;
	private Button							btnRemove;
	private Hashtable<String, AccessPoint>	apList;
	private boolean 						clientActivityForNetwork;	
	
	public ClientActivitySettingsDlg(Hashtable<String, AccessPoint> apTable){
        super();
        apList	= apTable;
        super.setTitle("Show Client Activity");
    	super.setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK_CANCEL);
    	clientActivityForNetwork = false;
    }
	
    public void showDlg(){
    	super.show();
	}
    
   
    /**
     * 
     */
    private void setData() {
        Enumeration<AccessPoint> enm = apList.elements();
        while(enm.hasMoreElements()){
            AccessPoint ap	= (AccessPoint)enm.nextElement();
            if(ap.isStaActivityEnabled()) {
            	lstLoggingAp.add(ap.getDsMacAddress().toString());
            	clientActivityForNetwork = true;
            } else {
            	lstNonLoggingAp.add(ap.getDsMacAddress().toString());
            }
        }       
       
    }

   
    /**
     * 
     */
    private void saveDataToAp() {
    	
    	String[] items = lstLoggingAp.getItems();
    	
    	for(int i=0;i<items.length;i++){          
    		String apMac = items[i];
    		AccessPoint ap 	= (AccessPoint)apList.get(apMac);
            if(apMac.equalsIgnoreCase(ap.getDsMacAddress().toString())){
                ap.setStaActivityEnabled(true);
            }                        
        }
   
    	for(int i=0;i<lstNonLoggingAp.getItemCount();i++){
            String apMac = lstNonLoggingAp.getItem(i);
            AccessPoint ap = (AccessPoint)apList.get(apMac);
            if(apMac.equalsIgnoreCase(ap.getDsMacAddress().toString())){
                ap.setStaActivityEnabled(false);
            }                   
        }
        
        if(lstLoggingAp.getItemCount() > 0 ) {
        	clientActivityForNetwork = true;
        }else {
        	clientActivityForNetwork = false;
        }
    }

    private void removeFromList() {
        if(lstLoggingAp.getSelectionIndex() == -1){
            MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.OK);
            msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
            msg.setMessage("No nodes selected.");
            msg.open();
            return ;
        }
        
        if(lstLoggingAp.getSelectionCount() > 1){
        	int[] index = lstLoggingAp.getSelectionIndices();
        	String[] apMac = new String[index.length];
        	for(int i=0;i<index.length;i++){
        		apMac[i] = lstLoggingAp.getItem(index[i]);
            }
        	
        	for(int i=0;i<apMac.length;i++){
        		lstLoggingAp.remove(apMac[i]);
        		lstNonLoggingAp.add(apMac[i]);
        	}
        }else{
        	int index = lstLoggingAp.getSelectionIndex();
            String apMac = lstLoggingAp.getItem(index);
            lstLoggingAp.remove(index);
            lstNonLoggingAp.add(apMac);
        }
    }

    /**
     * 
     */
    private void addToList() {      
        
        if(validate() == false)
            return;   
        
        if(lstNonLoggingAp.getSelectionCount() > 1){
        	int[] index = lstNonLoggingAp.getSelectionIndices();
        	String[] apMac = new String[index.length];
        	for(int i=0;i<index.length;i++){
        		apMac[i] 	= lstNonLoggingAp.getItem(index[i]);
            }
        	
        	for(int i=0;i<apMac.length;i++){
        		lstNonLoggingAp.remove(apMac[i]);
        		lstLoggingAp.add(apMac[i]);
        	}
        }
        else{
        	int index 		= lstNonLoggingAp.getSelectionIndex();
        	String apMac 	= lstNonLoggingAp.getItem(index);
        
        	lstNonLoggingAp.remove(index);
        	lstLoggingAp.add(apMac);
        }
    }

    /**
     * @return
     */
    private boolean validate() {
        
    	if(lstNonLoggingAp.getSelectionIndex() == -1){
            MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.OK);
            msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
            msg.setMessage("No nodes selected for Monitoring Client Activity.");
            msg.open();
            return false;
        }
   
        return true;
    }

    /* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 320;
		bounds.height	= 215;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	protected void createControls(Canvas mainCnv) {
		
		Group	grp = new Group(mainCnv,SWT.NO);
        grp.setBounds(5,5,325,220);
       
        Label label = new Label(grp,SWT.NO);
        label.setText("Client Activity Disabled");
        label.setBounds(15,15,120,20);
        
        label = new Label(grp,SWT.NO);
        label.setText("Client Activity Enabled");
        label.setBounds(195,15,120,20);
         
        lstNonLoggingAp = new List(grp,SWT.BORDER|SWT.V_SCROLL|SWT.MULTI);
        lstNonLoggingAp.setBounds(10,35,120,160);
     
        lstLoggingAp = new List(grp,SWT.BORDER|SWT.V_SCROLL|SWT.MULTI);
        lstLoggingAp.setBounds(190,35,120,160);
                
        btnAdd = new Button(grp,SWT.PUSH);
        btnAdd.setBounds(145,75,30,20);
        btnAdd.setText("+");
        btnAdd.setToolTipText("Adds the selected node");
        btnAdd.addSelectionListener(new SelectionAdapter(){
            public void widgetSelected(SelectionEvent arg0) {  
                addToList();
            }   
        });
        
        btnRemove = new Button(grp,SWT.PUSH);
        btnRemove.setBounds(145,105,30,20);
        btnRemove.setText("-");
        btnRemove.setToolTipText("Removes the selected node");
        btnRemove.addSelectionListener(new SelectionAdapter(){
            public void widgetSelected(SelectionEvent arg0) {
                removeFromList();
            }   
        });
        setData();
        ContextHelp.addContextHelpHandlerEx(mainCnv,contextHelpEnum.CLIENTACTIVITYWINDOW);
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	protected boolean onOk() {
        saveDataToAp();
        return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	protected boolean onCancel() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		return false;
	}
	
	@Override
	protected boolean isModal() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @return the clientActivityForNetwork
	 */
	public boolean isClientActivityForNetwork() {
		return clientActivityForNetwork;
	}
}
