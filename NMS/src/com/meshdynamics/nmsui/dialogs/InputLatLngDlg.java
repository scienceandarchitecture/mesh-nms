/*
 * Created on Dec 27, 2007
 *
 */
package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;

/**
 * @author prachiti
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class InputLatLngDlg extends MeshDynamicsDialog {

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
     */
    public static final String TITLE = "Set Latitude/Longitude for Node:";
    
    Text 		txtLatitude;
    Text 		txtLongitude;
    String 		latitude;
    String 		longitude;
    boolean		ret;
    
    public InputLatLngDlg(String latitude,String longitude) {
        this.latitude  	= latitude;
        this.longitude 	= longitude;
        ret 			= false;
    }
    
    protected void modifyMinimumBounds(Rectangle bounds) {
        bounds.x = 0;
		bounds.y = 0;
		bounds.width = 220;
		bounds.height = 100;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
     */
    protected void createControls(Canvas mainCnv) {

        int yOffset = 10;

		Group grpOptions = new Group(mainCnv,SWT.NONE);
		grpOptions.setBounds(20,10,200,90);
		
		Label lblCaption = new Label(grpOptions,SWT.NONE);
		lblCaption.setText("Latitude:");
		lblCaption.setBounds(10,yOffset,60,20);
		
		txtLatitude = new Text(grpOptions,SWT.BORDER);
		txtLatitude.setBounds(70,yOffset,120,20);
		yOffset += 30;
		
		lblCaption = new Label(grpOptions,SWT.NONE);
		lblCaption.setText("Longitude:");
		lblCaption.setBounds(10,yOffset,60,20);
		
		txtLongitude = new Text(grpOptions,SWT.BORDER);
		txtLongitude.setBounds(70,yOffset,120,20);
		yOffset += 30;
		
		setDeFaultValues();
    }

    public boolean open() {
        super.show();
        return ret;
        
    }
    
    private void setDeFaultValues() {
        setTitle(TITLE);
        txtLatitude.setText(latitude);
        txtLongitude.setText(longitude);
    }
    
    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
     */
    protected boolean onOk() {
        latitude  = txtLatitude.getText();
        longitude = txtLongitude.getText();
        ret = true;
        return true;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
     */
    protected boolean onApply() {
        // TODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
     */
    protected boolean onCancel() {
        ret = false;
        return true;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
     */
    protected boolean onImport() {
        // TODO Auto-generated method stub
        return false;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
     */
    protected boolean onExport() {
        // TODO Auto-generated method stub
        return false;
    }

    public static void main(String args[]) {
        InputLatLngDlg inputBox = new InputLatLngDlg("37.0000","-121.435345");
        if(inputBox.open() == true) {
            System.out.println(inputBox.getLatitude());
            System.out.println(inputBox.getLongitude());
        }
    }
    
    public String getLatitude() {
        return latitude;
    }
    public String getLongitude() {
        return longitude;
    }
}
