package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;

public class AdminPasswordInputDlg extends MeshDynamicsDialog {

	Text 		txtEnterPass;
	String 		password;
		
	
	public AdminPasswordInputDlg(String password) {
		this.password = password;
		setTitle("Enter Password to UnLock this Network");
	}
	
	@Override
	protected void createControls(Canvas mainCnv) {
		
		int rel_x = 10;
	    int rel_y = 30;
	    
		Label lblEnterPass = new Label(mainCnv,SWT.NO);
		lblEnterPass.setText("Enter Password");
		lblEnterPass.setBounds(rel_x,rel_y,80,20);	
		
		rel_x	+= 90;
		
		txtEnterPass =  new Text(mainCnv,SWT.BORDER|SWT.PASSWORD);		
		txtEnterPass.setBounds(rel_x,rel_y,300,20);
		txtEnterPass.setFocus();
	}

	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width 	= 400;
		bounds.height 	= 50;
	}

	@Override
	protected boolean onApply() {
		
		return false;
	}

	@Override
	protected boolean onCancel() {
		return true;
	}

	@Override
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean onOk() {
		
		if(txtEnterPass.getText().equals(password)) {
			return true;
		}
		
		Shell shell = new Shell(Display.getDefault());
		MessageBox messageBox = new MessageBox(shell,SWT.MULTI);
		messageBox.setText(" Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		messageBox.setMessage("Invalid password");
		messageBox.open();
		shell.dispose();
		
		return false;
	}
	
}
