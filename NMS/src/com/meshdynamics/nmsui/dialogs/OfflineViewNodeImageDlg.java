package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.nmsui.util.MeshViewerGDIObjects;

public class OfflineViewNodeImageDlg extends MeshDynamicsDialog {

	private Group	grpOfflineSettings;
	private Text	txtOfflineOnImage;
	private Text	txtOfflineOffImage;
	private Text	txtOfflineScanImage;
	private Text	txtOfflineHbMissImage;
	private Button	btnOfflineOnImage;
	private Button	btnOfflineOffImage;
	private Button	btnOfflineScanImage;
	private Button	btnOfflineHbMissImage;
 
	private String 	onImgPath;
	private String 	offImgPath;
	private String 	scanImgPath;
	private String 	hbMissImgPath;
	
	@Override
	protected void createControls(Canvas mainCnv) {
		
		setTitle("Offline Vew Node Images");
		int grp_y = 10;
		
		grpOfflineSettings = new Group(mainCnv,SWT.NONE);
		grpOfflineSettings.setBounds(10,grp_y,330,140);
		grpOfflineSettings.setFont(MeshViewerGDIObjects.boldVerdanaFont8);
		
        int rel_y = 25;
        Label lbl = new Label(grpOfflineSettings, SWT.SIMPLE);
        lbl.setText("On Image");
        lbl.setBounds(15, rel_y, 120, 20);
        txtOfflineOnImage = new Text(grpOfflineSettings, SWT.BORDER);
        txtOfflineOnImage.setBounds(140, rel_y, 150, 20);
        btnOfflineOnImage = new Button(grpOfflineSettings, SWT.PUSH);
        btnOfflineOnImage.setText("...");
        btnOfflineOnImage.setBounds(295, rel_y, 20, 20);
        btnOfflineOnImage.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				handleImagePathSelection(txtOfflineOnImage);			
			}
		});
        
        rel_y += 25;
        lbl = new Label(grpOfflineSettings, SWT.SIMPLE);
        lbl.setText("Off Image");
        lbl.setBounds(15, rel_y, 120, 20);
        txtOfflineOffImage = new Text(grpOfflineSettings, SWT.BORDER);
        txtOfflineOffImage.setBounds(140, rel_y, 150, 20);
        btnOfflineOffImage = new Button(grpOfflineSettings, SWT.PUSH);
        btnOfflineOffImage.setText("...");
        btnOfflineOffImage.setBounds(295, rel_y, 20, 20);
        btnOfflineOffImage.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				handleImagePathSelection(txtOfflineOffImage);			
			}
		});

        rel_y += 25;
        lbl = new Label(grpOfflineSettings, SWT.SIMPLE);
        lbl.setText("Scan Image");
        lbl.setBounds(15, rel_y, 120, 20);
        txtOfflineScanImage = new Text(grpOfflineSettings, SWT.BORDER);
        txtOfflineScanImage.setBounds(140, rel_y, 150, 20);
        btnOfflineScanImage = new Button(grpOfflineSettings, SWT.PUSH);
        btnOfflineScanImage.setText("...");
        btnOfflineScanImage.setBounds(295, rel_y, 20, 20);
        btnOfflineScanImage.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				handleImagePathSelection(txtOfflineScanImage);			
			}
		});

        rel_y += 25;
        lbl = new Label(grpOfflineSettings, SWT.SIMPLE);
        lbl.setText("HB Miss Image");
        lbl.setBounds(15, rel_y, 120, 20);
        txtOfflineHbMissImage = new Text(grpOfflineSettings, SWT.BORDER);
        txtOfflineHbMissImage.setBounds(140, rel_y, 150, 20);
        btnOfflineHbMissImage = new Button(grpOfflineSettings, SWT.PUSH);
        btnOfflineHbMissImage.setText("...");
        btnOfflineHbMissImage.setBounds(295, rel_y, 20, 20);
        btnOfflineHbMissImage.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				handleImagePathSelection(txtOfflineHbMissImage);			
			}
		});
        
	}

	private void handleImagePathSelection(Text txtPath) {
		FileDialog 	fileDialog 	= new FileDialog(new Shell());
		String[] 	ext 		= new String[3];
		ext[0] = "*.gif";
		ext[1] = "*.jpg,*.jpeg";    			
		ext[2] = "*.bmp";
		
		String folderPath = MFile.getIconsPath();
		fileDialog.setFilterExtensions(ext);
		fileDialog.setFilterPath(folderPath);
		String imgPath = fileDialog.open();
		
		if(imgPath == null)
			return;
		if(imgPath.trim().equals(""))
			return;
		
		try {
			Image img = new Image(null, imgPath);
			img.dispose();
			img = null;
		} catch(Exception e) {
			MessageBox msgBox = new MessageBox(new Shell(), SWT.OK);
			msgBox.setText(" Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msgBox.setMessage("Selected image file s not a valid image.");
			msgBox.open();
			txtPath.setText("");
			return;
		}
		
		txtPath.setText(imgPath);
	}
	
	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x = 0;
		bounds.y = 0;
		bounds.width = 340;
		bounds.height = 155;
	}

	@Override
	protected boolean onCancel() {
		return true;
	}

	@Override
	protected boolean onOk() {
	    onImgPath 		= txtOfflineOnImage.getText().trim(); 
	    offImgPath 		= txtOfflineOffImage.getText().trim(); 
	    scanImgPath 	= txtOfflineScanImage.getText().trim();
	    hbMissImgPath	= txtOfflineHbMissImage.getText().trim();
		return true;
	}

	public String getOnImgPath() {
		return onImgPath;
	}

	public String getOffImgPath() {
		return offImgPath;
	}

	public String getScanImgPath() {
		return scanImgPath;
	}

	public String getHbMissImgPath() {
		return hbMissImgPath;
	}
	
}
