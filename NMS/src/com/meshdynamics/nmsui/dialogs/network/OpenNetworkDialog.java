/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : OpenNetworkDialog.java
 * Comments : 
 * Created  : May 17, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |Jul 30, 2007 | changes related to fips network creation        | Imran  |
 * --------------------------------------------------------------------------------
 * |  0  |May 17, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.dialogs.network;

import java.io.File;
import java.io.FileInputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.aes.AES;
import com.meshdynamics.hmacsha1.HMAC_SHA1;
import com.meshdynamics.meshviewer.MeshViewer;
import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.Bytes;

public class OpenNetworkDialog extends MeshDynamicsDialog {

    public  static final int MAX_TIMEOUT_SECS 	= 30;
    
	private List			lstNetList;
	private Text			txtKey;

	private String 			selectedNetworkName;
	private String 			networkKey;
	private short			networkType;
	private String			networkFilename;
	int						attempts;
	int						timeout;
	private MeshViewer		meshViewer;
	private	Shell			shell;
    private Runnable 		timerThread;
    private int 			tryCounter;
    
	
	private class NetworkData{
		public byte[] 	key;
		public byte  	type;
		public String 	fileName;
	}
	
	public OpenNetworkDialog(MeshViewer meshViewer) {
		super();
		
		this.meshViewer = meshViewer;
	    attempts		= 0;
	    tryCounter		= 0;
		timerThread 	= new Runnable() {
		      public void run() {
		          
		         //Dialog disposes after MAX_TIMEOUT_SECS
	             handleTimeout("Open Dialog Expired,Please try after sometime.");
	             return;
		     }
		};
		
		Display.getDefault().timerExec(MAX_TIMEOUT_SECS * 1000,timerThread);
	}

	public void  handleTimeout(String msg) {
	    
	    if(shell == null)
	        return;
	    
	    MessageBox msgBx = new MessageBox(new Shell(SWT.CLOSE), SWT.OK | SWT.ICON_INFORMATION);
		msgBx.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		msgBx.setMessage(msg);
		msgBx.open();
		attempts++;
		
		onCancel();
		
	}
	
	protected void createControls(Canvas mainCnv) {
		
	    shell = mainCnv.getShell();
	    
		Label lab = new Label(mainCnv,SWT.NO);
		lab.setText("Network List");
		lab.setBounds(15,25,80,15);
		
		lstNetList = new List(mainCnv,SWT.SINGLE|SWT.BORDER|SWT.V_SCROLL|SWT.H_SCROLL);
		lstNetList.setBounds(95,25,200,80);		
		lstNetList.addMouseListener(new MouseAdapter(){
			public void mouseDoubleClick(MouseEvent arg0) {
				txtKey.setFocus();				
			}
		});
		lstNetList.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {			  
				txtKey.setText("");
				txtKey.setFocus();
			}
		});
		
		Group sepGrp = new Group(mainCnv,SWT.NO);
		sepGrp.setBounds(2,115,305,4);
		
		lab = new Label(mainCnv,SWT.NO);
		lab.setText("Network Key ");
		lab.setBounds(15,140,80,20);

		txtKey = new Text(mainCnv,SWT.BORDER|SWT.PASSWORD);
		txtKey.setBounds(95,135,200,20);	
	
		fillNetworkList();
		
	}

	private void fillNetworkList() {
		
		File 		file 	= new File(MFile.getNetworkPath());
		String[] 	files 	= file.list();
		
		if(files.length <= 0) {
			return;
		}
		
		for(int i=0;i<files.length;i++) {
			
			if(files[i].endsWith(".net") == false) {
				continue;
			}
			
			String networkName 	= files[i].substring(0,files[i].length()-4);
			if(meshViewer.getMeshNetworkByName(networkName) != null) {
				continue;
			}
			
			NetworkData	nwData	= getMeshNetworkData(files[i]);
			if(nwData == null)
				continue;
			
			lstNetList.add(networkName);
			lstNetList.setData(networkName, nwData);
		}
		
		if(lstNetList.getItemCount() > 0) {
			lstNetList.select(0);
			selectedNetworkName = lstNetList.getItem(0);
		}
		
		txtKey.setFocus();
	}
	
    /**
	 * @return
	 */
	private NetworkData getMeshNetworkData(String fileName) {
		
		try {
			NetworkData		nwData		= new NetworkData();
			FileInputStream in 			= new FileInputStream(MFile.getNetworkPath() +"/"+fileName);
			byte[] 			fourBytes 	= new byte[4];
			byte[]			oneByte		= new byte[1];
	    	in.read(fourBytes);
	    	
	    	byte[] mac = new byte[6];
	    	mac[0] = fourBytes[0];
	    	mac[1] = fourBytes[3];
	    	mac[2] = fourBytes[2];
	    	mac[3] = fourBytes[1];
	    	mac[4] = fourBytes[3];
	    	mac[5] = fourBytes[1];
	    		
	    	byte[] buffer = new byte[Bytes.bytesToInt(fourBytes)];
	    	in.read(buffer);
    	
	    	in.read(fourBytes);
	    	buffer = new byte[Bytes.bytesToInt(fourBytes)];
	    	in.read(buffer); // reads network key
	    	
	    	in.read(oneByte); // reads network type
	    	nwData.type	= oneByte[0]; 
	    	
	    	if(nwData.type == MeshNetwork.NETWORK_REGULAR) {
		    	AES aes = new AES();
		    	buffer = aes.aesdecodeKey(buffer,buffer.length,mac);
	    	}
    		nwData.key	= new byte[buffer.length];
    		System.arraycopy(buffer, 0, nwData.key, 0, buffer.length);
	    	
			in.close();
			
			nwData.fileName = MFile.getNetworkPath() +"/"+fileName;
			
			return nwData;
			
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}
	
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x 		= 0;
		bounds.y		= 0;
		bounds.width	= 300;
		bounds.height	= 160;
	}

	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}
	
	private void cancelTimer() {
	    Display.getDefault().timerExec(-1,timerThread);
	}
	
	protected boolean onCancel() {
	    cancelTimer();
	    shell.dispose();
		return false;
	}

	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	protected boolean onOk() {
	    
		if(tryCounter >= 3) {
			handleTimeout("Only 3 attempts allowed in 1 minute,Please try after sometime.");
			return false;
		}
			
		if(validateValues()	== true) {
			NetworkData	nwData	= (NetworkData)lstNetList.getData(selectedNetworkName);
			this.networkType		= nwData.type;
			this.networkFilename	= nwData.fileName;
			if(this.networkType == MeshNetwork.NETWORK_FIPS_ENABLED)
				this.networkKey  		= new String(Mesh.convertTo16Bytes(txtKey.getText().trim()));
			else
				this.networkKey  		= new String(txtKey.getText().trim());
			
			cancelTimer();
			return true;
		}
		
		tryCounter++;
		return false;
	}
	
	private boolean validateValues() {
		
		int index = lstNetList.getSelectionIndex();
		if(index < 0) {
			MessageBox msg  = new MessageBox(new Shell(SWT.CLOSE),SWT.ICON_WARNING|SWT.ICON_ERROR);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Network not selected");
			msg.open();		
			return false;
		}
		
		selectedNetworkName = lstNetList.getItem(index);
		String networkKey	= txtKey.getText().trim();
		
		if(networkKey.equals("") == true) {
			MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
		    msg.setMessage("Enter Network key");
		    msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		    msg.open();							
            txtKey.setFocus();
			return false;
		}

		NetworkData	nwData		= (NetworkData)lstNetList.getData(selectedNetworkName);
		
		byte[]		keyBytes	= null;
		
		if(nwData.type == MeshNetwork.NETWORK_FIPS_ENABLED) {
			
			if(networkKey.length() != Mesh.FIPS_VALID_NW_KEY_SIZE) {
				MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_INFORMATION);
				msg.setMessage("Network key should be 32 ASCII-HEX");
				msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
				msg.open();							
				txtKey.setFocus();
				return false;
			}

			if(MeshValidations.isHex(networkKey) == false){
				MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
				msg.setMessage( Mesh.FIPS_NETWORK_KEY_TOOLTIP );
				msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
				msg.open();							
				txtKey.setFocus();
				return false;
			}
			
			keyBytes			= new byte[HMAC_SHA1.DIGEST_LENGTH];
    		byte[] mac 			= new byte[6];
    		byte[] fourBytes	= new byte[4];
    		
    		Bytes.intToBytes(selectedNetworkName.length(),fourBytes);
    		mac[0] = fourBytes[0];
    		mac[1] = fourBytes[3];
    		mac[2] = fourBytes[2];
    		mac[3] = fourBytes[1];
    		mac[4] = fourBytes[3];
    		mac[5] = fourBytes[1];
    		networkKey  	= new String(Mesh.convertTo16Bytes(txtKey.getText().trim()));
			HMAC_SHA1.getInstance().generateHMAC(networkKey.getBytes(), mac, keyBytes);
		} else {
			keyBytes = networkKey.getBytes();
		}
		
		for(int i=0;i<nwData.key.length;i++) {
			
			if(nwData.key[i] != keyBytes[i]) {
				MessageBox msg  = new MessageBox(new Shell(SWT.CLOSE),SWT.ICON_ERROR);
				msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
				msg.setMessage("Invalid network key, Cannot open network  " + "' "+selectedNetworkName+" '");
				msg.open();	
				txtKey.setFocus();
				return false;				
			}
		}

		return true;
	}

	public String getNetworkKey() {
		return networkKey;
	}

	public String getSelectedNetworkName() {
		return selectedNetworkName;
	}
	
	public short getNetworkType(){
		return networkType;
	}

	public String getNetworkFilename() {
		return networkFilename;
	}

    public int getAttempts() {
        return attempts;
    }
}
