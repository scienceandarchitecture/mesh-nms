
/********************************************************************************
 * MeshDynamics
 * --------------
 * File     : UpdateConfigDialog.java
 * Comments :
 * Created  : May 3, 2005
 * Author   : Bindu Khare
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History
 * -------------------------------------------------------------------------------
 * | No  |Date        |  Comment                                        | Author |
 * -------------------------------------------------------------------------------
 * |  3  |Jun 12, 2007 | Extended from MeshdynamicsDialog	 	  	 	|Abhishek |
 * ----------------------------------------------------------------------------------
 * |  2  |May 31, 2007| btn1 and btn2 length increased	             	|  Imran |
 * -------------------------------------------------------------------------------
 * |  1  |Nov 27, 2006| Misc Fixes				  			         	|  Bindu |
 * -------------------------------------------------------------------------------
 * |  0  |May 3, 2005 | Created                                         | Bindu   |
 * -------------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.nmsui.dialogs.network;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Label;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;


public class OptionMessageBox extends MeshDynamicsDialog{
	
	private  static final int BTN1_SELECTED = 1;
	private  static final int BTN2_SELECTED = 2;

	private Label				message;
	
	private String				strMessage;
	private String				strBtn1Msg;
	private String				strBtn2Msg;
	
	private Button 				btn1;
	private Button 				btn2;
	
	private int					btnSelected = 1;
	
	/**
	 *
	 */
	public OptionMessageBox(String message, String btn1Msg, String btn2Msg) {
		
		super();
		super.setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK_CANCEL);
		super.setTitle("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		strMessage	= message;
		strBtn1Msg	= btn1Msg;
		strBtn2Msg	= btn2Msg;
	}
	
	public void show() {
		super.show();
	}
	
	public int getBtnSelected() {
		return btnSelected;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		
		bounds.x		= 10;
		bounds.y		= 10;
		bounds.height	= 100;
		bounds.width	= 300;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	protected void createControls(Canvas mainCnv) {
		
		int x = 15;
		int y = 15;

		message			= new Label(mainCnv ,SWT.BOLD);
		message.setBounds(x, y, 400, 25);		
		message.setText(strMessage);
		
		x = x + 25;
		y = y + 25;
		btn1 = new Button(mainCnv,SWT.RADIO);	
		btn1.setText(strBtn1Msg);
		btn1.setBounds(x, y, 200, 25);
		btn1.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(btn1.getSelection()){
					btnSelected = BTN1_SELECTED;
					btn2.setSelection(false);
				}				
			}
		});
		btn1.setSelection(true);
		y = y + 25;		
		btn2 = new Button(mainCnv,SWT.RADIO);
		btn2.setText(strBtn2Msg);
		btn2.setBounds(x, y, 200, 25);
		btn2.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(btn2.getSelection()){
					btnSelected = BTN2_SELECTED;
					btn1.setSelection(false);
				}
			}
		});
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	protected boolean onOk() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		return false;
	}
	/**
	 * 
	 */
	protected boolean onCancel() {
		btnSelected = -1;
		return true;
	}
}