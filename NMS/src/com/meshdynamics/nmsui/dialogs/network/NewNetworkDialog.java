
/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : NewNetworkDialog.java
 * Comments : 
 * Created  : May 15, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |Jul 23, 2007 | changes related to fips network creation        | Imran  |
 *  -------------------------------------------------------------------------------
 * |  0  |May 15, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.dialogs.network;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;


public class NewNetworkDialog extends MeshDynamicsDialog {
    
	private Text	           	txtName;
	private Text 	           	txtKey;
	private Text 	           	txtConfirmKey;
	private Label   			lblName;
	private Label				lblKey;	
	private Label				lblConfirmKey;
	private Button				btnNetworkType;
	private String 				networkName;
	private String 				networkKey;
	private byte				newtworkType;
	
	/**
	 * @param arg0
	 * @param arg1
	 */
	public NewNetworkDialog() {
		newtworkType	= MeshNetwork.NETWORK_REGULAR;
	}
	
	protected void createControls(Canvas mainCnv) {
		
		int rel_x = 25;
	    int rel_y = 20;
	    
		lblName = new Label(mainCnv,SWT.NO);
		lblName.setText("Network Name");
		lblName.setBounds(rel_x,rel_y,80,20);	
		
		rel_x	+= 80;
		txtName	=	new Text(mainCnv,SWT.BORDER);		
		txtName.setBounds(rel_x,rel_y,180,20);
		
		rel_x	-= 80;
		rel_y 	+= 30;
		lblKey = new Label(mainCnv,SWT.NO);
		lblKey.setText("Network Key");
		lblKey.setBounds(rel_x,rel_y,80,20);
		
		rel_x	+= 80;
		txtKey	=	new Text(mainCnv,SWT.BORDER|SWT.PASSWORD);
		txtKey.setBounds(rel_x,rel_y,180,20);
		txtKey.setTextLimit(200);
			
		rel_x	-= 80;
		rel_y 	+= 30;
		lblConfirmKey = new Label(mainCnv,SWT.NO);
		lblConfirmKey.setText("Confirm Key");
		lblConfirmKey.setBounds(rel_x,rel_y,60,20);
		
		rel_x	+= 80;
		txtConfirmKey	=	new Text(mainCnv,SWT.BORDER|SWT.PASSWORD);
		txtConfirmKey.setBounds(rel_x,rel_y,180,20);
		txtConfirmKey.setTextLimit(200);
		
		rel_y 	+= 30;
		btnNetworkType	= new Button(mainCnv, SWT.CHECK);
		btnNetworkType.setText("FIPS 140-2 compliant");
		btnNetworkType.setBounds(rel_x, rel_y,180, 20);
		btnNetworkType.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				if(btnNetworkType.getSelection() == true) {
					txtKey.setTextLimit(Mesh.FIPS_VALID_NW_KEY_SIZE);
					txtConfirmKey.setTextLimit(Mesh.FIPS_VALID_NW_KEY_SIZE);
					txtKey.setToolTipText( Mesh.FIPS_NETWORK_KEY_TOOLTIP );
					txtConfirmKey.setToolTipText( Mesh.FIPS_NETWORK_KEY_TOOLTIP );
				}else {
					txtKey.setTextLimit(200);
					txtConfirmKey.setTextLimit(200);
					txtKey.setToolTipText("");
					txtConfirmKey.setToolTipText("");
				}
			}
		});
		
		txtName.setFocus();
		ContextHelp.addContextHelpHandlerEx(mainCnv,contextHelpEnum.DLGNEWNETWORK);
	}

	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x 		= 0;
		bounds.y		= 0;
		bounds.width	= 300;
		bounds.height	= 120;
	}

	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	protected boolean onCancel() {
		// TODO Auto-generated method stub
		return true;
	}

	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	protected boolean onOk() {
		if(validateValues() == false) {
			return false;
		}
        	   
		networkName = txtName.getText().trim();
				
		if(btnNetworkType.getSelection() == true) {
			networkKey  	= new String(Mesh.convertTo16Bytes(txtKey.getText().trim()));			
			newtworkType	= MeshNetwork.NETWORK_FIPS_ENABLED;
		} else {
			networkKey 		= txtKey.getText().trim();
			newtworkType	= MeshNetwork.NETWORK_REGULAR;
		}
		
	    return true;
	}

    /**
     * @return
     */
	 private boolean validateValues() {

		 String networkName 		= txtName.getText();
		 String networkKey			= txtKey.getText();
		 String networkConfirmKey	= txtConfirmKey.getText();
		 
		 if(networkName.equals("") == true) {
			 MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
			 msg.setMessage("Network Name not entered");
			 msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			 msg.open();			
			 txtName.setFocus();
			 return false;
		 }
		 if(networkName.length() > 32) {
			 MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
			 msg.setMessage("Network Name length cannot be more than 32 characters");
			 msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			 msg.open();			
			 txtName.setFocus();
			 return false;
		 }
		 
		 Pattern pattern = Pattern.compile("\\s");
		 Matcher matcher = pattern.matcher(networkName);
		 if(matcher.find() == true) {
			 MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
			 msg.setMessage("White spaces not allowed in network name.");
			 msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			 msg.open();							
			 txtName.setFocus();
			 return false;
		 }
		 if(networkKey.equals("") == true) {
			 MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
			 msg.setMessage("Network Key not entered");
			 msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			 msg.open();							
			 txtKey.setFocus();
			 return false;
		 }
		 if(networkConfirmKey.equals("") == true) {
			 MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
			 msg.setMessage("Please confirm network key");
			 msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			 msg.open();							
			 txtKey.setFocus();
			 return false;
		 }
		 
		 if(btnNetworkType.getSelection() == true) {
		 	 if(isKeyFipsCompatible(networkKey) == false) {
		 	 	return false;
		 	 }
		 } else {

			 pattern = Pattern.compile("\\s");
			 matcher = pattern.matcher(networkKey);
			 if(matcher.find() == true) {
				 MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
				 msg.setMessage("White spaces not allowed in network key.");
				 msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
				 msg.open();							
				 txtKey.setFocus();
				 return false;
			 }
			 
			 if(networkKey.length() > 16) {
				 MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
				 msg.setMessage("Network key length cannot be more than 16 characters");
				 msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
				 msg.open();							
				 txtKey.setFocus();
				 return false;
			 }

		 }
		 
		 if(networkKey.equals(networkConfirmKey) == false) {
			 MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
			 msg.setMessage("New Network Key & Confirm Key don't match");
			 msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			 msg.open();							
			 txtConfirmKey.setFocus();
			 return false;
		 }
		
		 return true;
	 }

	/**
	 * 
	 */
	private boolean isKeyFipsCompatible(String nwKey) {
		
		if(nwKey.length() != Mesh.FIPS_VALID_NW_KEY_SIZE) {
			 MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_INFORMATION);
			 msg.setMessage("Network key should be 16 ASCII-HEX");
			 msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			 msg.open();							
			 txtConfirmKey.setFocus();
			 return false;
		 }
		
		 if(MeshValidations.isHex(nwKey) == false){
			 MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|SWT.ICON_ERROR);
			 msg.setMessage( Mesh.FIPS_NETWORK_KEY_TOOLTIP );
			 msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			 msg.open();							
			 txtConfirmKey.setFocus();
			 return false;
		 }
		 return true;
		
	}
	
	public String getNetworkKey() {
		return networkKey;
	}

	public String getNetworkName() {
		return networkName;
	}
    
	/**
	 * @return Returns the newtworkType.
	 */
	public byte getNewtworkType() {
		return newtworkType;
	}
}
