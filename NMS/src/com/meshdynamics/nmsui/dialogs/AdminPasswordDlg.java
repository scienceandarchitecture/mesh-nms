package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;

public class AdminPasswordDlg extends MeshDynamicsDialog {
	
	private Text 	           	txtOldKey;
	private Text 	           	txtKey;
	private Text 	           	txtConfirmKey;
	private Label				lblOldKey;
	private Label				lblKey;	
	private Label				lblConfirmKey;
	
	private boolean				firstTimeKey;
	
	MeshNetworkUI meshNetworkUI;
	
	public AdminPasswordDlg(MeshNetworkUI meshNetworkUI) {
		
		firstTimeKey = true;
		
		this.meshNetworkUI = meshNetworkUI;
		
		if(meshNetworkUI.getSuPassword().length() > 0 ){
			firstTimeKey = false;
		}
		
		setTitle("Set Admin password for "+ meshNetworkUI.getNwName());
	}
	
	@Override
	protected void createControls(Canvas mainCnv) {
		
		int rel_x = 15;
	    int rel_y = 30;
	    
		lblOldKey = new Label(mainCnv,SWT.NO);
		lblOldKey.setText("Old Password");
		lblOldKey.setBounds(rel_x,rel_y,100,20);	
		
		rel_x	+= 100;
		
		txtOldKey	=  new Text(mainCnv,SWT.BORDER|SWT.PASSWORD);		
		txtOldKey.setBounds(rel_x,rel_y,180,20);
		
		if(firstTimeKey == true) {
			lblOldKey.setEnabled(false);
			txtOldKey.setEnabled(false);
		}
		else {
			lblOldKey.setEnabled(true);
			txtOldKey.setEnabled(true);
		}
		
		rel_x	-= 100;
		rel_y 	+= 45;
		
		lblKey = new Label(mainCnv,SWT.NO);
		lblKey.setText("New Password");
		lblKey.setBounds(rel_x,rel_y,100,20);
		
		rel_x	+= 100;
		txtKey	=	new Text(mainCnv,SWT.BORDER|SWT.PASSWORD);
		txtKey.setBounds(rel_x,rel_y,180,20);
		txtKey.setTextLimit(200);
			
		rel_x	-= 100;
		rel_y 	+= 45;
		lblConfirmKey = new Label(mainCnv,SWT.NO);
		lblConfirmKey.setText("Confirm Password");
		lblConfirmKey.setBounds(rel_x,rel_y,100,20);
	
		rel_x	+= 100;
		
		txtConfirmKey	=	new Text(mainCnv,SWT.BORDER|SWT.PASSWORD);
		txtConfirmKey.setBounds(rel_x,rel_y,180,20);
		txtConfirmKey.setTextLimit(200);
				
		txtOldKey.setFocus();
	}

	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 300;
		bounds.height	= 150;
	}

	@Override
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean onCancel() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	protected boolean onOk() {
		
		if(validate() == false)
			return false;
		
		meshNetworkUI.setSuPassword(txtKey.getText());
		
		return true;
	}

	private boolean validate() {
		
		String prevKey = meshNetworkUI.getSuPassword();
		String errMsg = "";
		try {
		if(firstTimeKey == false && prevKey.length() > 0) {
			if(prevKey.equals(txtOldKey.getText()) == false) {
				errMsg = "Old Key doesnt match new key";
				throw new Exception();
			}
		}
		
		if(txtKey.getText().length() == 0) {
			errMsg = "Key cannot be emptys";
			throw new Exception();
		}
			
		if(txtKey.getText().equals(txtConfirmKey.getText()) == false) {
			errMsg = "New Key doesnt match confirm key";
			throw new Exception();
		}
		
		}catch (Exception ex) {
			Shell shell = new Shell(Display.getDefault());
			MessageBox messageBox = new MessageBox(shell,SWT.MULTI);
			messageBox.setText(" Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			messageBox.setMessage(errMsg);
			messageBox.open();
			shell.dispose();
			return false;
		}
		return true;
	}
}
