/*
 * Created on Mar 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.dialogs.advancedsettings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.util.MacAddress;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SipEntryDialog extends MeshDynamicsDialog {
	
	public static final int DIALOG_MODE_NEW_ENTRY	= 0;
	public static final int DIALOG_MODE_EDIT_ENTRY	= 1;
	
	private Combo		cboExtn;
	private Text		txtMacAddress;
	private int[]		arrExistingExtensions;
	private String[]	arrExistingMacs;
	private int 		dialogMode;
	private String 		editExtension;
	private String		editMacAddress;
	private String 		updatedExtension;
	private String 		updatedMacAddress;
	
	/**
	 * 
	 */
	public SipEntryDialog() {
		super();
		arrExistingExtensions 	= null;
		arrExistingMacs			= null;
		dialogMode				= DIALOG_MODE_NEW_ENTRY;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 300;
		bounds.height	= 60;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	protected void createControls(Canvas mainCnv) {
		int x	= 20;
		int y	= 20;
		
		Label lbl = new Label(mainCnv, SWT.SIMPLE);
		lbl.setText("Extension");
		lbl.setBounds(x, y, 100, 20);
		x += 120;
	
		cboExtn = new Combo(mainCnv, SWT.BORDER|SWT.READ_ONLY);
		cboExtn.setBounds(x, y, 150, 20);
		x = 	20;
		y += 	25;
		
		lbl = new Label(mainCnv, SWT.SIMPLE);
		lbl.setText("Mac Address");
		lbl.setBounds(x, y, 100, 20);
		x += 120;
	
		txtMacAddress = new Text(mainCnv, SWT.BORDER);
		txtMacAddress.setBounds(x, y, 150, 20);

		for(int i=1;i<=256;i++) {
			
			if(arrExistingExtensions == null) {
				cboExtn.add(""+i);
				continue;
			}
			
			boolean found = false;
			for(int j=0;j<arrExistingExtensions.length;j++) {
				if(arrExistingExtensions[j] == i) {
					found = true;
					break;
				}
			}
			if(found == false) 
				cboExtn.add(""+i);
			else if(dialogMode == DIALOG_MODE_EDIT_ENTRY) {
				if(editExtension.equals(""+i) == true) {
					cboExtn.add(""+i);
				}
			}
			
		}
		
		if(dialogMode == DIALOG_MODE_EDIT_ENTRY) {
			cboExtn.setText(editExtension);
			txtMacAddress.setText(editMacAddress);
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	protected boolean onOk() {
		updatedExtension 	= cboExtn.getText();
		
		if(MacAddress.tempAddress.setBytes(txtMacAddress.getText()) != 0) {
			MessageBox msgBox = new MessageBox(new Shell(), SWT.ICON_ERROR);
			msgBox.setMessage("Invalid mac address");
			msgBox.open();
			return false;
		}
	
		updatedMacAddress = txtMacAddress.getText();
		if(arrExistingMacs == null)
			return true;
			
		for(int i=0;i<arrExistingMacs.length;i++) {
			boolean checkMacAddress = true;
			if(dialogMode == DIALOG_MODE_EDIT_ENTRY) {
				if(updatedMacAddress.equalsIgnoreCase(editMacAddress) == true)
					checkMacAddress = false;
			}
			if(checkMacAddress == true) {
				if(updatedMacAddress.equalsIgnoreCase(arrExistingMacs[i]) == true) {
					MessageBox msgBox = new MessageBox(new Shell(), SWT.ICON_ERROR);
					msgBox.setMessage("Entered mac address is already in use");
					msgBox.open();
					return false;
				}
			}
		}
	
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	protected boolean onCancel() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	public void setExistingExtensionsList(int[] extensions) {
		this.arrExistingExtensions = new int[extensions.length];
		System.arraycopy(extensions, 0, arrExistingExtensions, 0, extensions.length);
	}
	
	/**
	 * @param dialogMode The dialogMode to set.
	 */
	public void setDialogMode(int dialogMode) {
		this.dialogMode = dialogMode;
		if(dialogMode == DIALOG_MODE_NEW_ENTRY)
			setTitle("Add new extension");
		else
			setTitle("Edit extension");
	}
	
	/**
	 * @param editExtension The editExtension to set.
	 */
	public void setEditExtension(String editExtension) {
		this.editExtension = editExtension;
	}
	
	/**
	 * @param editMacAddress The editMacAddress to set.
	 */
	public void setEditMacAddress(String editMacAddress) {
		this.editMacAddress = editMacAddress;
	}
	
	public String getUpdatedExtension() {
		return updatedExtension; 
	}
	
	public String getUpdatedMacAddress() {
		return updatedMacAddress;
	}
	
	/**
	 * @param arrExistingMacs The arrExistingMacs to set.
	 */
	public void setExistingMacList(String[] arrExistingMacs) {
		this.arrExistingMacs = new String [arrExistingMacs.length];
		System.arraycopy(arrExistingMacs, 0, this.arrExistingMacs, 0, arrExistingMacs.length);
	}
}
