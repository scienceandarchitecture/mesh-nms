/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamActionDialog.java
 * Comments : 
 * Created  : Feb 28, 2007
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author   |
 * ----------------------------------------------------------------------------------
 * | 1   |May 1, 2007   | Changes due to MeshDynamicsDialog		          |Abhishek  |
 * --------------------------------------------------------------------------------
 * |  0  | Feb 28, 2007 | Created                                         |  Bindu   |
 * ----------------------------------------------------------------------------------
 **********************************************************************************/


package com.meshdynamics.nmsui.dialogs.advancedsettings;

import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;

import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.util.TransmitRateTable;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;


public class EffistreamActionDialog extends MeshDynamicsDialog{
	
	private Group 				group;
	private Label 				lblTransmitRate;
	private Label				lblTxRateUnit;
	private Button 				btnNoAck;
	private Button 				btnQueuedRetry;
	private Button 				btnDropPacket;
	private Button 				btn80211Category;
	private Combo				cmbDot11eCategory;
	private Combo				cmbBitrate;
	
	
	private EffistreamActionForUI 		action;
	private IVersionInfo				versionInfo;
	private IInterfaceConfiguration		interfaceConfigInfo;
	
	public class EffistreamActionForUI {
		
		short 	dot11eCategory;
		short 	noAck;
		short 	dropPacket;
		short	bitRate;
		short	queuedRetry;
		
		EffistreamActionForUI() {
			noAck			= 0;
			dropPacket 		= 0;
			bitRate			= 0;
			queuedRetry		= 0;
			dot11eCategory	= 0xFF;
		}
	}
	
	 public EffistreamActionDialog(IVersionInfo versionInfo,IInterfaceConfiguration interfaceConfigInfo) {
	 	super();
	 	super.setTitle("Action Properties");	
	 	this.versionInfo			= versionInfo;
	 	this.interfaceConfigInfo	= interfaceConfigInfo;
	 	
	 	action	= new EffistreamActionForUI();
	 }
	 
	public void createControls(Canvas mainCnv) {

		int		x;
		int		y;
		group = new Group(mainCnv, SWT.NONE);
		group.setBounds(10, 5, 250, 150);
		
		x = 15;
		y = 15;

		btnDropPacket = new Button(group, SWT.CHECK);
		btnDropPacket.setBounds(x, y, 150, 20);
		btnDropPacket.setText("Drop Packet");
		btnDropPacket.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				
				if(btnDropPacket.getSelection() == true) {
					
					btnNoAck.setEnabled(false);
					cmbDot11eCategory.setEnabled(false);
					btn80211Category.setEnabled(false);
					
					if(versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,
													  IVersionInfo.MINOR_VERSION_5,
													  IVersionInfo.VARIANT_VERSION_40)) {
						lblTransmitRate.setEnabled(false);
						lblTxRateUnit.setEnabled(false);
						cmbBitrate.setEnabled(false);
						btnQueuedRetry.setEnabled(false);
					}
					
				} else {
								
					btnNoAck.setEnabled(true);
					btn80211Category.setEnabled(true);
					if(btn80211Category.getSelection() == true) {
						cmbDot11eCategory.setEnabled(true);
					}
					if(versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,
															 IVersionInfo.MINOR_VERSION_5,
															 IVersionInfo.VARIANT_VERSION_40)) {
						lblTransmitRate.setEnabled(true);
						lblTxRateUnit.setEnabled(true);
						cmbBitrate.setEnabled(true);
						btnQueuedRetry.setEnabled(true);

					}
				}
			}
			
		});

		y = y + 25;
		
		btnNoAck = new Button(group, SWT.CHECK);
		btnNoAck.setBounds(x, y, 150, 20);
		btnNoAck.setText("No ACK");

		y = y + 25;

		btnQueuedRetry = new Button(group, SWT.CHECK);
		btnQueuedRetry.setBounds(x, y, 150, 20);
		btnQueuedRetry.setText("Queued Retry");


		y = y + 25;

		btn80211Category = new Button(group, SWT.CHECK);
		btn80211Category.setBounds(x, y, 110, 20);
		btn80211Category.setText("802.11e Category");
		
		btn80211Category.addSelectionListener(new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				
				if(btn80211Category.getSelection() == false) {
					cmbDot11eCategory.setEnabled(false);
				}else {
					cmbDot11eCategory.setEnabled(true);
				}
			}
		});
		
		cmbDot11eCategory =	new Combo(group, SWT.READ_ONLY|SWT.BORDER);
		cmbDot11eCategory.setBounds(130, y-3, 100, 15);
		
		cmbDot11eCategory.add("None");		
		cmbDot11eCategory.add("Background");		
		cmbDot11eCategory.add("Best Effort");		
		cmbDot11eCategory.add("Video");
		cmbDot11eCategory.add("Voice");
		
		cmbDot11eCategory.setData("" + 0, new Short((short)0xFF));
		cmbDot11eCategory.setData("" + 1, new Short((short)0));
		cmbDot11eCategory.setData("" + 2, new Short((short)1));
		cmbDot11eCategory.setData("" + 3, new Short((short)2));
		cmbDot11eCategory.setData("" + 4, new Short((short)3));
		cmbDot11eCategory.select(0);
		
		cmbDot11eCategory.setEnabled(false);
		
		y = y + 30;
		
		lblTransmitRate	= new Label(group, SWT.NO);
		lblTransmitRate.setBounds(x, y, 95, 15);
		lblTransmitRate.setText("Transmit Rate");
		
		cmbBitrate = new Combo(group,SWT.READ_ONLY|SWT.BORDER);
		cmbBitrate.setBounds(130, y-3, 50, 20);
		cmbBitrate.add("0");
		addTransmitRate();
		
		x = x + 180;
		
		lblTxRateUnit	= new Label(group, SWT.NO);
		lblTxRateUnit.setBounds(x, y, 50, 15);
		lblTxRateUnit.setText("Mbps");
		
		if(versionInfo.versionLessThanEqualTo(IVersionInfo.MAJOR_VERSION_2,
				  IVersionInfo.MINOR_VERSION_5,
				  IVersionInfo.VARIANT_VERSION_40)) {
				lblTransmitRate.setEnabled(false);
				lblTxRateUnit.setEnabled(false);
				cmbBitrate.setEnabled(false);
			}
		
		ContextHelp.addContextHelpHandlerEx(mainCnv,contextHelpEnum.DLGEFFISTREAMADDACTION);
		mainCnv.setFocus();
	}
	
	public void addTransmitRate()
	{
	    Vector<String> transmitRate 	= new Vector<String>();
		
		Vector<String> txRate  		= TransmitRateTable.getDefaultRateTable(Mesh.PHY_SUB_TYPE_802_11_A,0);
		addToVector(transmitRate,txRate);
		txRate  			= TransmitRateTable.getDefaultRateTable(Mesh.PHY_SUB_TYPE_802_11_B,0);
		addToVector(transmitRate,txRate);
		txRate  			= TransmitRateTable.getDefaultRateTable(Mesh.PHY_SUB_TYPE_802_11_G,0);
		addToVector(transmitRate,txRate);
		txRate  			= TransmitRateTable.getDefaultRateTable(Mesh.PHY_SUB_TYPE_802_11_B_G,0);
		addToVector(transmitRate,txRate);
		
		int count = interfaceConfigInfo.getInterfaceCount();
		for(int i = 0;i<count;i++)
		{
		    IInterfaceInfo ifInfo =  (IInterfaceInfo)interfaceConfigInfo.getInterfaceByIndex(i);
		    short subType = ifInfo.getMediumSubType();
		    	switch(subType) {
			    	case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ:
			    	    txRate  			= TransmitRateTable.getDefaultRateTable(subType,Mesh.CHANNEL_BANDWIDTH_5_MHZ);
			    	    break;
			    	case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ:
			    	    txRate  			= TransmitRateTable.getDefaultRateTable(subType,Mesh.CHANNEL_BANDWIDTH_10_MHZ);
		    	    	break;
			    	case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ:
			    	    txRate  			= TransmitRateTable.getDefaultRateTable(subType,Mesh.CHANNEL_BANDWIDTH_20_MHZ);
		    	    	break; 
		    	}
		    	addToVector(transmitRate,txRate);
		}
		
		Collections.sort(transmitRate,new CompareTxRate());
		Enumeration<String> e 		= transmitRate.elements();
		
		while(e.hasMoreElements())
		{
		   cmbBitrate.add((String)e.nextElement());
		}
		cmbBitrate.select(0);
	}
	
	private void addToVector(Vector<String> dstTxRate, Vector<String> srcTxRate) {
        
        String strRate = "";
        
        Enumeration<String> e =  srcTxRate.elements();
        while(e.hasMoreElements()){
            strRate = (String)e.nextElement();
            if(dstTxRate.contains(strRate) == false)
            {
                dstTxRate.add(strRate);
            }
        }
    }

    public boolean onCancel() {		
		return true;
	}

	public boolean onOk() {
		
		if(validateData() == false) {
			return false;
		}
		
		int selectedIndex;		
		if(btnQueuedRetry.getSelection() == true){
		    action.queuedRetry = 1;
		}else {
		    action.queuedRetry = 0;
		}
		
		if(btnDropPacket.getSelection() == true) {
			action.dropPacket = 1;
		}
		if(btnDropPacket.getSelection() == false ){
		
			if(btn80211Category.getSelection() == true) {
				selectedIndex 			= cmbDot11eCategory.getSelectionIndex();
				action.dot11eCategory 	= ((Short)(cmbDot11eCategory.getData("" + selectedIndex))).shortValue();
			}
		
			String strBitRate 	= cmbBitrate.getText().trim();
			action.bitRate 		= (strBitRate.equals("") == true) ? 0 : Short.parseShort(strBitRate);
			
			if(btnNoAck.getSelection() == false) {
				action.noAck	= 0;
			}else {
				action.noAck	= 1;
			}
		}
		return true;
	}

	private boolean validateData() {
		
		if(btnDropPacket.getSelection() == true) {
			return true;
		}	
		
		if(btnDropPacket.getSelection()    		   == false &&
				   btn80211Category.getSelection() == false &&
				   btnNoAck.getSelection()		   == false &&
				   cmbBitrate.getText().equals("") == true) {
					
					showErrorMessage("Invalid Action.", SWT.ICON_ERROR);
					return false;
		}
		if(versionInfo.versionLessThanEqualTo(IVersionInfo.MAJOR_VERSION_2,
				  IVersionInfo.MINOR_VERSION_5,
				  IVersionInfo.VARIANT_VERSION_40)) {
			return true;
		}
		
		
		short bitRate = 0;
		
		if(btnDropPacket.getSelection() == false) {
			if(cmbBitrate.getText().trim().equals("") == true) {
				showErrorMessage("Transmit Rate is blank.", SWT.ICON_ERROR);
				cmbBitrate.setFocus();
				return false;
			}
		}
		try {
			bitRate	=	Short.parseShort(cmbBitrate.getText().trim());
		}catch(Exception e) {
			showErrorMessage("Transmit Rate is invalid.", SWT.ICON_ERROR);
			cmbBitrate.setFocus();
			return false;
		}
		
		if(bitRate < 0 || bitRate > 100) {
			showErrorMessage("Transmit Rate should be between 0 Mbps to 100 Mbps." + SWT.CR +
			        			"Where 0 is Auto", SWT.ICON_ERROR);
			cmbBitrate.setFocus();
			return false;
		}
		
		return true;
	}

	public EffistreamActionForUI getValues() {
		return action;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 255;
		bounds.height	= 150;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	class CompareTxRate implements Comparator<Object>  {  
		
		public int compare(Object o1, Object o2) {
			
			int t1 = Integer.parseInt((String)o1);
			int t2 = Integer.parseInt((String)o2);
			
			return  t1 - t2;
		}  
	}  
}
