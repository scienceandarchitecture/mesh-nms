/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamRulePropertiesDialog.java
 * Comments : 
 * Created  : Feb 26, 2007
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author   |
 * ----------------------------------------------------------------------------------
 * |  4  |jul 10,2007   | Changes due to IEffistreamRuleCriteria          | Imran    |
 * ----------------------------------------------------------------------------------
 * |  3  |jul 09,2007   | Changes due to IEffistreamRule				  |Abhishek  |
 * --------------------------------------------------------------------------------
 * |  2  |May 1, 2007   | Changes due to MeshDynamicsDialog 	          |Abhishek  |
 * --------------------------------------------------------------------------------
 * |  1  | Apr 02, 2007 | IP TOS Calculation Fix                          |  Bindu   |
 * ----------------------------------------------------------------------------------
 * |  0  | Feb 26, 2007 | Created                                         |  Bindu   |
 * ----------------------------------------------------------------------------------
 **********************************************************************************/


package com.meshdynamics.nmsui.dialogs.advancedsettings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.configuration.IEffistreamRule;
import com.meshdynamics.meshviewer.imcppackets.helpers.EffistreamHelper;
import com.meshdynamics.meshviewer.util.EffistreamCriteria;
import com.meshdynamics.meshviewer.util.EffistreamXMLReader;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;
import com.meshdynamics.util.Range;

public class EffistreamRuleDialog extends MeshDynamicsDialog  {
	
	private static final int 	ETH_TYPE_LABEL 				= 0;
	private static final int 	ETH_DST_LABEL 				= 0;
	private static final int 	ETH_SRC_LABEL 				= 0;	
	private static final int 	IP_TOS_PRECEDENCE_LABEL		= 0;
	private static final int 	IP_TOS_DELAY_LABEL			= 1;
	private static final int 	IP_TOS_THROUGHPUT_LABEL		= 2;
	private static final int 	IP_TOS_RELIABILITY_LABEL	= 3;
	private static final int 	IP_TOS_MONETARY_COST_LABEL	= 4;
	private static final int 	IP_DIFFSERVICES_LABEL		= 0;
	private static final int 	IP_SRC_LABEL				= 0;
	private static final int 	IP_DST_LABEL				= 0;
	private static final int 	IP_PROTO_LABEL				= 0;
	private static final int 	UDP_SRC_PORT_MIN_LABEL		= 0;
	private static final int 	UDP_SRC_PORT_MAX_LABEL		= 1;
	private static final int 	UDP_DST_PORT_MIN_LABEL		= 0;
	private static final int 	UDP_DST_PORT_MAX_LABEL		= 1;
	private static final int 	UDP_LENGTH_MIN_LABEL		= 0;
	private static final int 	UDP_LENGTH_MAX_LABEL		= 1;
	private static final int 	TCP_SRC_PORT_MIN_LABEL		= 0;
	private static final int 	TCP_SRC_PORT_MAX_LABEL		= 1;
	private static final int 	TCP_DST_PORT_MIN_LABEL		= 0;
	private static final int 	TCP_DST_PORT_MAX_LABEL		= 1;
	private static final int 	TCP_LENGTH_MIN_LABEL		= 0;
	private static final int 	TCP_LENGTH_MAX_LABEL		= 1;
	private static final int 	RTP_VERSION_LABEL			= 0;
	private static final int 	RTP_PAYLOAD_LABEL			= 0;	
	private static final int 	RTP_LENGTH_MIN_LABEL		= 0;
	private static final int 	RTP_LENGTH_MAX_LABEL		= 1;
	private static final int 	MAX_LABELS					= 5;
	
	private static final int 	ETH_TYPE_COMBO 				= 0;
	private static final int 	ETH_DST_COMBO 				= 0;
	private static final int 	IP_TOS_PRECEDENCE_COMBO		= 0;
	private static final int 	IP_TOS_DELAY_COMBO			= 1;
	private static final int 	IP_TOS_THROUGHPUT_COMBO		= 2;
	private static final int 	IP_TOS_RELIABILITY_COMBO	= 3;
	private static final int 	IP_TOS_MONETARY_COST_COMBO	= 4;
	private static final int 	IP_DIFFSERVICES_COMBO		= 0;
	private static final int 	IP_PROTO_COMBO				= 0;
	private static final int 	RTP_VERSION_COMBO			= 0;
	private static final int 	RTP_PAYLOAD_COMBO			= 0;
	private static final int 	MAX_COMBO_BOXES				= 5;
	
	private static final int 	ETH_SRC_TEXT_BOX			= 0;
	private static final int 	IP_SRC_TEXT_BOX				= 0;
	private static final int 	IP_DST_TEXT_BOX				= 0;
	private static final int 	UDP_SRC_PORT_MIN_TEXT_BOX	= 0;
	private static final int 	UDP_SRC_PORT_MAX_TEXT_BOX	= 1;
	private static final int 	UDP_DST_PORT_MIN_TEXT_BOX	= 0;
	private static final int 	UDP_DST_PORT_MAX_TEXT_BOX	= 1;	
	private static final int 	UDP_LENGTH_MIN_TEXT_BOX		= 0;
	private static final int 	UDP_LENGTH_MAX_TEXT_BOX		= 1;
	private static final int 	TCP_SRC_PORT_MIN_TEXT_BOX	= 0;
	private static final int 	TCP_SRC_PORT_MAX_TEXT_BOX	= 1;
	private static final int 	TCP_DST_PORT_MIN_TEXT_BOX	= 0;
	private static final int 	TCP_DST_PORT_MAX_TEXT_BOX	= 1;	
	private static final int 	TCP_LENGTH_MIN_TEXT_BOX		= 0;
	private static final int 	TCP_LENGTH_MAX_TEXT_BOX		= 1;
	private static final int 	RTP_LENGTH_MIN_TEXT_BOX		= 0;
	private static final int 	RTP_LENGTH_MAX_TEXT_BOX		= 1;
	private static final int 	MAX_TEXT_BOXES				= 2;		
	
	private Group 				group;
	private Label 				label;
	private Combo				cmbCriteria;
	private Group 				seperatorGroup;
	private Label[] 			labels;
	private Combo[] 			comboBoxes;
	private Text[] 				textBoxes;	
	
	private EffistreamXMLReader xmlReader;
	private	IEffistreamRule		parentRule;
	private EffistreamRuleForUI uiRule;
	
	public class EffistreamRuleForUI{
		public int 		criteriaId;
		public Object	value;
		public int 		childCount;
		IEffistreamRule	parent;
			
		public EffistreamRuleForUI(){
			criteriaId	= 0;
			value		= new Object();
			childCount	= 0;
			parent		= null; 
		}
	}
	
	public EffistreamRuleDialog(IEffistreamRule parentRule) {
		xmlReader 		 = new EffistreamXMLReader(MFile.getConfigPath() + "/EffistreamCriteria.xml");
		uiRule		     = new EffistreamRuleForUI();
		this.parentRule	 = parentRule;
		super.setTitle("Rule Properties");
	}
	
	public void createControls(Canvas mainCnv) {
		
		int		i;
		int		x;
		int		y;
		int		yBase;
		
		group = new Group(mainCnv, SWT.NONE);
		group.setBounds(10, 5, 270, 225);

		x = 15;
		y = 24;

		label	=	new Label(group, SWT.NO);
		label.setBounds(x, y, 75, 15);
		label.setAlignment(SWT.LEFT);
		label.setText("Criteria");

		cmbCriteria	=	new Combo(group, SWT.READ_ONLY|SWT.BORDER);
		cmbCriteria.setBounds(95, 20, 150, 15);
		cmbCriteria.addSelectionListener(new SelectionAdapter(){
            public void widgetSelected(SelectionEvent arg0) {
            	
            	int	matchId;
            	
            	matchId = cmbCriteria.getSelectionIndex()+1;
            	
            	enableCriteriaControls(matchId);
           	}            		    
		});		

		seperatorGroup = new Group(group, SWT.NO);
		seperatorGroup.setBounds(5, 50, 335, 4);

		yBase = y + 55;
		
		labels		= new Label[MAX_LABELS];
		comboBoxes	= new Combo[MAX_COMBO_BOXES];
		textBoxes	= new Text[MAX_TEXT_BOXES];
		
		for(i = 0, y = yBase; i < MAX_LABELS; i++) {
			labels[i]	= new Label(group, SWT.NO);
			labels[i].setBounds(x, y+4, 75, 15);
			labels[i].setAlignment(SWT.LEFT);
			y = y + 25;			
		}
		
		for(i = 0, y = yBase; i < MAX_COMBO_BOXES; i++) {
			comboBoxes[i]	= new Combo(group, SWT.BORDER);
			comboBoxes[i].setBounds(95, y, 150, 15);
			y = y + 25;			
		}
		
		for(i = 0, y = yBase; i < MAX_TEXT_BOXES; i++) {
			textBoxes[i]	= new Text(group, SWT.BORDER);
			textBoxes[i].setBounds(95, y, 150, 20);
			y = y + 25;			
		}
		
		ContextHelp.addContextHelpHandlerEx(mainCnv,contextHelpEnum.DLGEFFISTREAMADDRULE);
		fillCriteriaList();
		
	}
	
	private void fillCriteriaList() {
		
		for(int i = 0; i < EffistreamConfigPage.strCriteria.length; i++) {
			cmbCriteria.add(EffistreamConfigPage.strCriteria[i]);			
		}		
		
		cmbCriteria.select(EffistreamHelper.ID_TYPE_ETH_TYPE - 1);
		enableCriteriaControls(EffistreamHelper.ID_TYPE_ETH_TYPE);
		cmbCriteria.setFocus();
	}
	
	private void hideAllControls() {
		int	i;
		
		for(i = 0 ; i < MAX_LABELS; i++) {
			labels[i].setVisible(false);			
		}
		
		for(i = 0 ; i < MAX_COMBO_BOXES; i++) {
			comboBoxes[i].setVisible(false);			
		}
		
		for(i = 0; i < MAX_TEXT_BOXES; i++) {
			textBoxes[i].setVisible(false);			
		}		
	}
	
	private void fillCriteriaSubControls(int matchId, EffistreamCriteria	criteria) {
		int 	i;

		switch(matchId) {
		case EffistreamHelper.ID_TYPE_ETH_TYPE:			
			
			labels[ETH_TYPE_LABEL].setText(criteria.fields[0].uiLabel);    		
    		
			comboBoxes[ETH_TYPE_COMBO].removeAll();    		
    		for(i = 0; i < criteria.fields[0].kvPairs.length; i++) {
    			comboBoxes[ETH_TYPE_COMBO].add(criteria.fields[0].kvPairs[i].key);
    			comboBoxes[ETH_TYPE_COMBO].setData("" + i, criteria.fields[0].kvPairs[i].value);
    		}
    		
    		comboBoxes[ETH_TYPE_COMBO].select(0);
			break;
			
		case EffistreamHelper.ID_TYPE_ETH_DST:
			
			labels[ETH_DST_LABEL].setText(criteria.fields[0].uiLabel);
	    		
			comboBoxes[ETH_DST_COMBO].removeAll();
    		for(i = 0; i < criteria.fields[0].kvPairs.length; i++) { 
    			comboBoxes[ETH_DST_COMBO].add(criteria.fields[0].kvPairs[i].key);
    			comboBoxes[ETH_DST_COMBO].setData("" + i, criteria.fields[0].kvPairs[i].value);
    		}   		
    		
    		comboBoxes[ETH_DST_COMBO].select(0);
    		break;
	
		case EffistreamHelper.ID_TYPE_ETH_SRC:
			
    		labels[ETH_SRC_LABEL].setText("Mac Address");    		
    		textBoxes[ETH_DST_COMBO].setText("");
			break;

		case EffistreamHelper.ID_TYPE_IP_TOS:
			
    		labels[IP_TOS_PRECEDENCE_LABEL].setText(criteria.fields[0].uiLabel);
    		labels[IP_TOS_DELAY_LABEL].setText(criteria.fields[1].uiLabel);
    		labels[IP_TOS_THROUGHPUT_LABEL].setText(criteria.fields[2].uiLabel);
    		labels[IP_TOS_RELIABILITY_LABEL].setText(criteria.fields[3].uiLabel);
    		labels[IP_TOS_MONETARY_COST_LABEL].setText(criteria.fields[4].uiLabel);
    		
    		comboBoxes[IP_TOS_PRECEDENCE_COMBO].removeAll();
    		comboBoxes[IP_TOS_DELAY_COMBO].removeAll();
    		comboBoxes[IP_TOS_THROUGHPUT_COMBO].removeAll();
    		comboBoxes[IP_TOS_RELIABILITY_COMBO].removeAll();
    		comboBoxes[IP_TOS_MONETARY_COST_COMBO].removeAll();

    		for(i = 0; i < criteria.fields[0].kvPairs.length; i++) { 
    			comboBoxes[IP_TOS_PRECEDENCE_COMBO].add(criteria.fields[0].kvPairs[i].key);
    			comboBoxes[IP_TOS_PRECEDENCE_COMBO].setData("" + i, criteria.fields[0].kvPairs[i].value);
    		}

    		for(i = 0; i < criteria.fields[1].kvPairs.length; i++) { 
    			comboBoxes[IP_TOS_DELAY_COMBO].add(criteria.fields[1].kvPairs[i].key);
    			comboBoxes[IP_TOS_DELAY_COMBO].setData("" + i, criteria.fields[1].kvPairs[i].value);
    		}
    		
    		for(i = 0; i < criteria.fields[2].kvPairs.length; i++) { 
    			comboBoxes[IP_TOS_THROUGHPUT_COMBO].add(criteria.fields[2].kvPairs[i].key);
    			comboBoxes[IP_TOS_THROUGHPUT_COMBO].setData("" + i, criteria.fields[2].kvPairs[i].value);
    		}
    		
    		for(i = 0; i < criteria.fields[3].kvPairs.length; i++) { 
    			comboBoxes[IP_TOS_RELIABILITY_COMBO].add(criteria.fields[3].kvPairs[i].key);
    			comboBoxes[IP_TOS_RELIABILITY_COMBO].setData("" + i, criteria.fields[3].kvPairs[i].value);
    		}
    		
    		for(i = 0; i < criteria.fields[4].kvPairs.length; i++) { 
    			comboBoxes[IP_TOS_MONETARY_COST_COMBO].add(criteria.fields[4].kvPairs[i].key);
    			comboBoxes[IP_TOS_MONETARY_COST_COMBO].setData("" + i, criteria.fields[4].kvPairs[i].value);
    		}

    		comboBoxes[IP_TOS_PRECEDENCE_COMBO].select(0);
    		comboBoxes[IP_TOS_DELAY_COMBO].select(0);
    		comboBoxes[IP_TOS_THROUGHPUT_COMBO].select(0);
    		comboBoxes[IP_TOS_RELIABILITY_COMBO].select(0);
    		comboBoxes[IP_TOS_MONETARY_COST_COMBO].select(0);
			break;
		
		case EffistreamHelper.ID_TYPE_IP_DIFFSRV:
    		labels[IP_DIFFSERVICES_LABEL].setText(criteria.fields[0].uiLabel);
		
			comboBoxes[IP_DIFFSERVICES_COMBO].removeAll();

			for(i = 0; i < criteria.fields[0].kvPairs.length; i++) { 
				comboBoxes[IP_DIFFSERVICES_COMBO].add(criteria.fields[0].kvPairs[i].key);
				comboBoxes[IP_DIFFSERVICES_COMBO].setData("" + i, criteria.fields[0].kvPairs[i].value);
			}
			
			comboBoxes[IP_DIFFSERVICES_COMBO].select(0);
			break;
		
		case EffistreamHelper.ID_TYPE_IP_SRC:

			labels[IP_SRC_LABEL].setText("IP Address");    		
    		textBoxes[IP_SRC_TEXT_BOX].setText("");   	
			break;
		
		case EffistreamHelper.ID_TYPE_IP_DST:
    		labels[IP_DST_LABEL].setText("IP Address");    		
    		textBoxes[IP_DST_TEXT_BOX].setText("");   	
			break;
		
		case EffistreamHelper.ID_TYPE_IP_PROTO:
    		
			labels[IP_PROTO_LABEL].setText(criteria.fields[0].uiLabel);
    		
    		comboBoxes[IP_PROTO_COMBO].removeAll();
    		for(i = 0; i < criteria.fields[0].kvPairs.length; i++) { 
    			comboBoxes[IP_PROTO_COMBO].add(criteria.fields[0].kvPairs[i].key);
    			comboBoxes[IP_PROTO_COMBO].setData("" + i, criteria.fields[0].kvPairs[i].value);
    		}
    		
    		comboBoxes[IP_PROTO_COMBO].select(0);

    		break;

		case EffistreamHelper.ID_TYPE_UDP_SRC_PORT:
			labels[UDP_SRC_PORT_MIN_LABEL].setText("Minimum");
    		labels[UDP_SRC_PORT_MAX_LABEL].setText("Maximum");  	    		
    		 
    		textBoxes[UDP_SRC_PORT_MIN_TEXT_BOX].setText("");    		
    		textBoxes[UDP_SRC_PORT_MAX_TEXT_BOX].setText("");    		
	
			break;
		
		case EffistreamHelper.ID_TYPE_UDP_DST_PORT:			
    		labels[UDP_DST_PORT_MIN_LABEL].setText("Minimum");
    		labels[UDP_DST_PORT_MAX_LABEL].setText("Maximum");    		    		
    		 
    		textBoxes[UDP_DST_PORT_MIN_TEXT_BOX].setText("");    		
    		textBoxes[UDP_DST_PORT_MAX_TEXT_BOX].setText("");    		
    		
    		break;
		
		case EffistreamHelper.ID_TYPE_UDP_LENGTH:			
    		labels[UDP_LENGTH_MIN_LABEL].setText("Minimum");
    		labels[UDP_LENGTH_MAX_LABEL].setText("Maximum");    		    		
    		 
    		textBoxes[UDP_LENGTH_MIN_TEXT_BOX].setText("");    		
    		textBoxes[UDP_LENGTH_MAX_TEXT_BOX].setText("");    		
    		break;
		
		case EffistreamHelper.ID_TYPE_TCP_SRC_PORT:
    		labels[TCP_SRC_PORT_MIN_LABEL].setText("Minimum");
    		labels[TCP_SRC_PORT_MAX_LABEL].setText("Maximum");    		    		
    		 
    		textBoxes[TCP_SRC_PORT_MIN_TEXT_BOX].setText("");    		
    		textBoxes[TCP_SRC_PORT_MAX_TEXT_BOX].setText("");    		

			break;
		
		case EffistreamHelper.ID_TYPE_TCP_DST_PORT:
    		labels[TCP_DST_PORT_MIN_LABEL].setText("Minimum");
    		labels[TCP_DST_PORT_MAX_LABEL].setText("Maximum");    		    		
    		 
    		textBoxes[TCP_DST_PORT_MIN_TEXT_BOX].setText("");    		
    		textBoxes[TCP_DST_PORT_MAX_TEXT_BOX].setText("");    		

			break;
		
		case EffistreamHelper.ID_TYPE_TCP_LENGTH :
			labels[TCP_LENGTH_MIN_LABEL].setText("Minimum");
    		labels[TCP_LENGTH_MAX_LABEL].setText("Maximum");    		    		
    		 
    		textBoxes[TCP_LENGTH_MIN_TEXT_BOX].setText("");    		
    		textBoxes[TCP_LENGTH_MAX_TEXT_BOX].setText("");    
    		break;
		
		case EffistreamHelper.ID_TYPE_RTP_VERSION:
			
    		labels[RTP_VERSION_LABEL].setText("Version");    		

    		comboBoxes[RTP_VERSION_COMBO].removeAll();    		
    		comboBoxes[RTP_VERSION_COMBO].add("2");
    		comboBoxes[ETH_TYPE_COMBO].setData("" + 0, "2");
    		comboBoxes[ETH_TYPE_COMBO].select(0);    		
    		break;
		
		case EffistreamHelper.ID_TYPE_RTP_PAYLOAD:
			
    		labels[RTP_PAYLOAD_LABEL].setText(criteria.fields[0].uiLabel);
    		
    		comboBoxes[RTP_PAYLOAD_COMBO].removeAll();    		
    		for(i = 0; i < criteria.fields[0].kvPairs.length; i++) {
    			comboBoxes[RTP_PAYLOAD_COMBO].add(criteria.fields[0].kvPairs[i].key);
    			comboBoxes[RTP_PAYLOAD_COMBO].setData("" + i, criteria.fields[0].kvPairs[i].value);
    		}
    		
    		comboBoxes[RTP_PAYLOAD_COMBO].select(0);

    		break;
		
		case EffistreamHelper.ID_TYPE_RTP_LENGTH:
			labels[RTP_LENGTH_MIN_LABEL].setText("Minimum");
    		labels[RTP_LENGTH_MAX_LABEL].setText("Maximum");    		    		
    		 
    		textBoxes[RTP_LENGTH_MIN_TEXT_BOX].setText("");    		
    		textBoxes[RTP_LENGTH_MAX_TEXT_BOX].setText("");    
    		
    		break;
		}
	}
	
	private void enableCriteriaControls(int matchId) {
		EffistreamCriteria	criteria;
		int					i;
		
		criteria 	= null;
		
		hideAllControls();
		
		if(xmlReader == null)
			xmlReader = new EffistreamXMLReader(MFile.getConfigPath() + "/EffistreamCriteria.xml");
		
		for(i = 0; i < xmlReader.topLevelCriterias.length; i++) {
			
			if(matchId != xmlReader.topLevelCriterias[i].matchId)
				continue;			
			
			criteria = xmlReader.topLevelCriterias[i];
		}		
		
    	switch(matchId) {
    	
    	case EffistreamHelper.ID_TYPE_ETH_TYPE:      		    		
    		labels[ETH_TYPE_LABEL].setVisible(true);
    		comboBoxes[ETH_TYPE_COMBO].setVisible(true);    		
    		break;
    		
    	case EffistreamHelper.ID_TYPE_ETH_DST:    		
    		labels[ETH_DST_LABEL].setVisible(true);
    		comboBoxes[ETH_DST_COMBO].setVisible(true);    		
    		break;
    		
    	case EffistreamHelper.ID_TYPE_ETH_SRC:
    		labels[ETH_SRC_LABEL].setVisible(true);
    		textBoxes[ETH_SRC_TEXT_BOX].setVisible(true);
    		break;
    		
    	case EffistreamHelper.ID_TYPE_IP_TOS:
    		labels[IP_TOS_PRECEDENCE_LABEL].setVisible(true);
    		comboBoxes[IP_TOS_PRECEDENCE_COMBO].setVisible(true);

    		labels[IP_TOS_DELAY_LABEL].setVisible(true);
    		comboBoxes[IP_TOS_DELAY_COMBO].setVisible(true);

    		labels[IP_TOS_THROUGHPUT_LABEL].setVisible(true);
    		comboBoxes[IP_TOS_THROUGHPUT_COMBO].setVisible(true);

    		labels[IP_TOS_RELIABILITY_LABEL].setVisible(true);
    		comboBoxes[IP_TOS_RELIABILITY_COMBO].setVisible(true);

    		labels[IP_TOS_MONETARY_COST_LABEL].setVisible(true);
    		comboBoxes[IP_TOS_MONETARY_COST_COMBO].setVisible(true);
    		break;
    	
    	case EffistreamHelper.ID_TYPE_IP_DIFFSRV:
    		labels[IP_DIFFSERVICES_LABEL].setVisible(true);
    		comboBoxes[IP_DIFFSERVICES_COMBO].setVisible(true);
    		break;
    	
    	case EffistreamHelper.ID_TYPE_IP_SRC:
    		labels[IP_SRC_LABEL].setVisible(true);
    		textBoxes[IP_SRC_LABEL].setVisible(true);
    		break;
    	
    	case EffistreamHelper.ID_TYPE_IP_DST:
    		labels[IP_DST_LABEL].setVisible(true);
    		textBoxes[IP_DST_LABEL].setVisible(true);
    		break;
    	
    	case EffistreamHelper.ID_TYPE_IP_PROTO:
    		labels[IP_PROTO_LABEL].setVisible(true);
    		comboBoxes[IP_PROTO_COMBO].setVisible(true);
    		break;
    		
    	case EffistreamHelper.ID_TYPE_UDP_SRC_PORT:
    		labels[UDP_SRC_PORT_MIN_LABEL].setVisible(true);
    		labels[UDP_SRC_PORT_MAX_LABEL].setVisible(true);
    		textBoxes[UDP_SRC_PORT_MIN_TEXT_BOX].setVisible(true);
    		textBoxes[UDP_SRC_PORT_MAX_TEXT_BOX].setVisible(true);
    		break;
    	
    	case EffistreamHelper.ID_TYPE_UDP_DST_PORT:
    		labels[UDP_DST_PORT_MIN_LABEL].setVisible(true);
    		labels[UDP_DST_PORT_MAX_LABEL].setVisible(true);
    		textBoxes[UDP_DST_PORT_MIN_TEXT_BOX].setVisible(true);
    		textBoxes[UDP_DST_PORT_MAX_TEXT_BOX].setVisible(true);
    		break;
    	
    	case EffistreamHelper.ID_TYPE_UDP_LENGTH:
    		labels[UDP_LENGTH_MAX_LABEL].setVisible(true);
    		labels[UDP_LENGTH_MIN_LABEL].setVisible(true);
    		textBoxes[UDP_LENGTH_MIN_TEXT_BOX].setVisible(true);
    		textBoxes[UDP_LENGTH_MAX_TEXT_BOX].setVisible(true);
    		break;

    	case EffistreamHelper.ID_TYPE_TCP_SRC_PORT:
    		labels[TCP_SRC_PORT_MIN_LABEL].setVisible(true);
    		labels[TCP_SRC_PORT_MAX_LABEL].setVisible(true);
    		textBoxes[TCP_SRC_PORT_MIN_TEXT_BOX].setVisible(true);
    		textBoxes[TCP_SRC_PORT_MAX_TEXT_BOX].setVisible(true);
    		break;
    	
    	case EffistreamHelper.ID_TYPE_TCP_DST_PORT:
    		labels[TCP_DST_PORT_MIN_LABEL].setVisible(true);
    		labels[TCP_DST_PORT_MAX_LABEL].setVisible(true);
    		textBoxes[TCP_DST_PORT_MIN_TEXT_BOX].setVisible(true);
    		textBoxes[TCP_DST_PORT_MAX_TEXT_BOX].setVisible(true);
    		break;
    	
    	case EffistreamHelper.ID_TYPE_TCP_LENGTH :
    		labels[TCP_LENGTH_MAX_LABEL].setVisible(true);
    		labels[TCP_LENGTH_MIN_LABEL].setVisible(true);
    		textBoxes[TCP_LENGTH_MIN_TEXT_BOX].setVisible(true);
    		textBoxes[TCP_LENGTH_MAX_TEXT_BOX].setVisible(true);
    		break;
    	
    	case EffistreamHelper.ID_TYPE_RTP_VERSION:
    		labels[RTP_VERSION_LABEL].setVisible(true);
    		comboBoxes[RTP_VERSION_COMBO].setVisible(true);
    		break;
    		
    	case EffistreamHelper.ID_TYPE_RTP_PAYLOAD:
    		labels[RTP_PAYLOAD_LABEL].setVisible(true);
    		comboBoxes[RTP_PAYLOAD_COMBO].setVisible(true);
    		break;
    		
    	case EffistreamHelper.ID_TYPE_RTP_LENGTH:
    		labels[RTP_LENGTH_MIN_LABEL].setVisible(true);
    		labels[RTP_LENGTH_MAX_LABEL].setVisible(true);
    		textBoxes[RTP_LENGTH_MIN_TEXT_BOX].setVisible(true);
    		textBoxes[RTP_LENGTH_MAX_TEXT_BOX].setVisible(true);
    		break;		
    	}  
    	
    	fillCriteriaSubControls(matchId, criteria);
    }	


	public EffistreamRuleForUI getValues() {
		return uiRule;
	}
	
	public boolean onCancel() {
		return true;
	}

	public boolean onOk() {
		
		short 				matchId;				
		String 				selectionValue;
		int 				comboSelectionIndex;
		Long 				longValue;
		MacAddress 			macAddress;
		IpAddress			ipAddress;
		Range				range;
		long				rangeLowerLimit;
		long   				rangeUpperLimit;

		macAddress		= new MacAddress();
		ipAddress		= new IpAddress();
		range			= new Range();
		selectionValue	= "";
		rangeLowerLimit	= 0;
		rangeUpperLimit	= 0;
		
		uiRule.parent		= this.parentRule; 
		matchId 			= (short)(cmbCriteria.getSelectionIndex() + 1);
		uiRule.criteriaId	= matchId;
		
		try {
		switch(matchId) {
		
		case EffistreamHelper.ID_TYPE_ETH_TYPE:
			
			comboSelectionIndex = comboBoxes[ETH_TYPE_COMBO].getSelectionIndex();
			if(comboSelectionIndex == -1) {
				selectionValue 	= comboBoxes[ETH_TYPE_COMBO].getText().trim();				
				longValue		= new Long(selectionValue); 
				uiRule.value	= longValue;
			} else { 
				selectionValue	= (String)comboBoxes[ETH_TYPE_COMBO].getData("" + comboSelectionIndex);
				longValue		= new Long(selectionValue); 
				uiRule.value	= longValue;
			}
			break;
	    
		case EffistreamHelper.ID_TYPE_ETH_DST:
			comboSelectionIndex = comboBoxes[ETH_DST_COMBO].getSelectionIndex();
			if(comboSelectionIndex == -1) {
				selectionValue = comboBoxes[ETH_DST_COMBO].getText().trim();
				
				if(macAddress.setBytes(selectionValue) != 0)
					throw new Exception();
			} else { 
				selectionValue	= (String)comboBoxes[ETH_DST_COMBO].getData("" + comboSelectionIndex);
				macAddress.setBytes(selectionValue);
			}
			
			uiRule.value	= macAddress;
			break;
		
		case EffistreamHelper.ID_TYPE_ETH_SRC:

			selectionValue 	= textBoxes[ETH_SRC_TEXT_BOX].getText().trim();
			if(macAddress.setBytes(selectionValue) != 0)
				throw new Exception();
			
			uiRule.value	= macAddress;
			break;
	    
		case EffistreamHelper.ID_TYPE_IP_TOS:
			short 	tos;
			int 	partialTosValue;
			
			tos 			= 0;
			partialTosValue = 0;
			
			comboSelectionIndex = comboBoxes[IP_TOS_PRECEDENCE_COMBO].getSelectionIndex();
			if(comboSelectionIndex == -1) 
				selectionValue = comboBoxes[IP_TOS_PRECEDENCE_COMBO].getText().trim();
			else 
				selectionValue	= (String)comboBoxes[IP_TOS_PRECEDENCE_COMBO].getData("" + comboSelectionIndex);	
			
			partialTosValue = Integer.parseInt(selectionValue);			
			tos  			|= (partialTosValue << 5);			
			
			comboSelectionIndex = comboBoxes[IP_TOS_DELAY_COMBO].getSelectionIndex();
			if(comboSelectionIndex == -1) 
				selectionValue = comboBoxes[IP_TOS_DELAY_COMBO].getText().trim();
			else 
				selectionValue	= (String)comboBoxes[IP_TOS_DELAY_COMBO].getData("" + comboSelectionIndex);

			partialTosValue 	= Integer.parseInt(selectionValue);			
			tos  	|= (partialTosValue << 4);

			comboSelectionIndex = comboBoxes[IP_TOS_THROUGHPUT_COMBO].getSelectionIndex();
			if(comboSelectionIndex == -1) 
				selectionValue 	= comboBoxes[IP_TOS_THROUGHPUT_COMBO].getText().trim();
			else 
				selectionValue	= (String)comboBoxes[IP_TOS_THROUGHPUT_COMBO].getData("" + comboSelectionIndex);

			partialTosValue = Integer.parseInt(selectionValue);			
			tos  			|= (partialTosValue << 3);			

			comboSelectionIndex = comboBoxes[IP_TOS_RELIABILITY_COMBO].getSelectionIndex();
			if(comboSelectionIndex == -1) 
				selectionValue 	= comboBoxes[IP_TOS_RELIABILITY_COMBO].getText().trim();
			else 
				selectionValue	= (String)comboBoxes[IP_TOS_RELIABILITY_COMBO].getData("" + comboSelectionIndex);

			partialTosValue = Integer.parseInt(selectionValue);			
			tos  			|= (partialTosValue << 2);			

			comboSelectionIndex = comboBoxes[IP_TOS_MONETARY_COST_COMBO].getSelectionIndex();
			if(comboSelectionIndex == -1) 
				selectionValue 	= comboBoxes[IP_TOS_MONETARY_COST_COMBO].getText().trim();
			else 
				selectionValue	= (String)comboBoxes[IP_TOS_MONETARY_COST_COMBO].getData("" + comboSelectionIndex);

			partialTosValue	= Integer.parseInt(selectionValue);			
			tos  			|= (partialTosValue << 1);			
			tos 			|= 0;
				
			longValue		= new Long(tos); 
			uiRule.value	= longValue;
			break;
		
		case EffistreamHelper.ID_TYPE_IP_DIFFSRV:
			comboSelectionIndex = comboBoxes[IP_DIFFSERVICES_COMBO].getSelectionIndex();
			if(comboSelectionIndex == -1) {
				selectionValue 	= comboBoxes[IP_DIFFSERVICES_COMBO].getText().trim();
				longValue		= new Long(selectionValue); 
				uiRule.value	= longValue;
			} else { 
				selectionValue	= (String)comboBoxes[IP_DIFFSERVICES_COMBO].getData("" + comboSelectionIndex);
				longValue		= new Long(selectionValue); 
				uiRule.value	= longValue;
			}
			break;
	    
		case EffistreamHelper.ID_TYPE_IP_SRC:
			selectionValue 	= textBoxes[IP_SRC_TEXT_BOX].getText().trim();			
			if(MeshValidations.isIpAddress(selectionValue) == false) {
				throw new Exception();
			}
			if(ipAddress.setBytes(selectionValue) != 0)
				throw new Exception();
			
			uiRule.value	= ipAddress;
			break;
		
		case EffistreamHelper.ID_TYPE_IP_DST:
			selectionValue 	= textBoxes[IP_DST_TEXT_BOX].getText().trim();			
			if(MeshValidations.isIpAddress(selectionValue) == false) {
				throw new Exception();
			}
			if(ipAddress.setBytes(selectionValue) != 0)
				throw new Exception();
			
			uiRule.value	= ipAddress;
			break;
	    
		case EffistreamHelper.ID_TYPE_IP_PROTO:
			comboSelectionIndex = comboBoxes[IP_PROTO_COMBO].getSelectionIndex();
			if(comboSelectionIndex == -1) { 
				selectionValue 	= comboBoxes[IP_PROTO_COMBO].getText().trim();
				longValue		= new Long(selectionValue); 
				uiRule.value	= longValue;
			} else { 
				selectionValue	= (String)comboBoxes[IP_PROTO_COMBO].getData("" + comboSelectionIndex);
				longValue		= new Long(selectionValue); 
				uiRule.value	= longValue;
			}
			
			break;
		
		case EffistreamHelper.ID_TYPE_UDP_SRC_PORT:
			
			selectionValue 	=  textBoxes[UDP_SRC_PORT_MIN_TEXT_BOX].getText().trim();
			selectionValue 	+= ":" + textBoxes[UDP_SRC_PORT_MAX_TEXT_BOX].getText().trim();
			
			rangeLowerLimit	= Long.parseLong(textBoxes[UDP_SRC_PORT_MIN_TEXT_BOX].getText().trim());
			rangeUpperLimit	= Long.parseLong(textBoxes[UDP_SRC_PORT_MAX_TEXT_BOX].getText().trim());
			
			if(validateRangeData(rangeLowerLimit, rangeUpperLimit) == false) {
				return false;
			}
			
			range.setLowerLimit(rangeLowerLimit);
			range.setUpperLimit(rangeUpperLimit);
			uiRule.value 	= range;
			break;
	    
		case EffistreamHelper.ID_TYPE_UDP_DST_PORT:
			selectionValue 	=  textBoxes[UDP_DST_PORT_MIN_TEXT_BOX].getText().trim();
			selectionValue 	+= ":" + textBoxes[UDP_DST_PORT_MAX_TEXT_BOX].getText().trim();
			
			rangeLowerLimit	= Long.parseLong(textBoxes[UDP_DST_PORT_MIN_TEXT_BOX].getText().trim());
			rangeUpperLimit	= Long.parseLong(textBoxes[UDP_DST_PORT_MAX_TEXT_BOX].getText().trim());
			
			if(validateRangeData(rangeLowerLimit, rangeUpperLimit) == false) {
				return false;
			}
			range.setLowerLimit(rangeLowerLimit);
			range.setUpperLimit(rangeUpperLimit);
			uiRule.value 	= range;
			break;
		
		case EffistreamHelper.ID_TYPE_UDP_LENGTH:
			selectionValue 	=  textBoxes[UDP_LENGTH_MIN_TEXT_BOX].getText().trim();
			selectionValue 	+= ":" + textBoxes[UDP_LENGTH_MAX_TEXT_BOX].getText().trim();

			rangeLowerLimit	= Long.parseLong(textBoxes[UDP_LENGTH_MIN_TEXT_BOX].getText().trim());
			rangeUpperLimit	= Long.parseLong(textBoxes[UDP_LENGTH_MAX_TEXT_BOX].getText().trim());
			
			if(validateRangeData(rangeLowerLimit, rangeUpperLimit) == false) {
				return false;
			}
			range.setLowerLimit(rangeLowerLimit);
			range.setUpperLimit(rangeUpperLimit);
			uiRule.value 	= range;
			break;
	    
		case EffistreamHelper.ID_TYPE_TCP_SRC_PORT:
			selectionValue 	=  textBoxes[TCP_SRC_PORT_MIN_TEXT_BOX].getText().trim();
			selectionValue 	+= ":" + textBoxes[TCP_SRC_PORT_MAX_TEXT_BOX].getText().trim();

			rangeLowerLimit	= Long.parseLong(textBoxes[TCP_SRC_PORT_MIN_TEXT_BOX].getText().trim());
			rangeUpperLimit	= Long.parseLong(textBoxes[TCP_SRC_PORT_MAX_TEXT_BOX].getText().trim());
			
			if(validateRangeData(rangeLowerLimit, rangeUpperLimit) == false) {
				return false;
			}
			range.setLowerLimit(rangeLowerLimit);
			range.setUpperLimit(rangeUpperLimit);
			uiRule.value 	= range;
			
			break;
		
		case EffistreamHelper.ID_TYPE_TCP_DST_PORT:
			selectionValue 	=  textBoxes[TCP_DST_PORT_MIN_TEXT_BOX].getText().trim();
			selectionValue 	+= ":" + textBoxes[TCP_DST_PORT_MAX_TEXT_BOX].getText().trim();
			
			rangeLowerLimit	= Long.parseLong(textBoxes[TCP_DST_PORT_MIN_TEXT_BOX].getText());
			rangeUpperLimit	= Long.parseLong(textBoxes[TCP_DST_PORT_MAX_TEXT_BOX].getText());
			
			if(validateRangeData(rangeLowerLimit, rangeUpperLimit) == false) {
				return false;
			}
			range.setLowerLimit(rangeLowerLimit);
			range.setUpperLimit(rangeUpperLimit);
			uiRule.value 	= range;
			
			break;
	    
		case EffistreamHelper.ID_TYPE_TCP_LENGTH :
			selectionValue 	=  textBoxes[TCP_LENGTH_MIN_TEXT_BOX].getText().trim();
			selectionValue 	+= ":" + textBoxes[TCP_LENGTH_MAX_TEXT_BOX].getText().trim();
			
			rangeLowerLimit	= Long.parseLong(textBoxes[TCP_LENGTH_MIN_TEXT_BOX].getText().trim());
			rangeUpperLimit	= Long.parseLong(textBoxes[TCP_LENGTH_MAX_TEXT_BOX].getText().trim());
			
			if(validateRangeData(rangeLowerLimit, rangeUpperLimit) == false) {
				return false;
			}
			range.setLowerLimit(rangeLowerLimit);
			range.setUpperLimit(rangeUpperLimit);
			uiRule.value 	= range;
			break;
		
		case EffistreamHelper.ID_TYPE_RTP_VERSION:
			comboSelectionIndex = comboBoxes[RTP_VERSION_COMBO].getSelectionIndex();
			if(comboSelectionIndex == -1) {
				selectionValue 	= comboBoxes[RTP_VERSION_COMBO].getText().trim();
				longValue		= new Long(selectionValue); 
				uiRule.value	= longValue;
			} else {
				selectionValue	= (String)comboBoxes[RTP_VERSION_COMBO].getData("" + comboSelectionIndex);
				longValue		= new Long(selectionValue); 
				uiRule.value	= longValue;
			}
			
			break;
	    
		case EffistreamHelper.ID_TYPE_RTP_PAYLOAD:
			comboSelectionIndex = comboBoxes[RTP_PAYLOAD_COMBO].getSelectionIndex();
			if(comboSelectionIndex == -1) {
				selectionValue 	= comboBoxes[RTP_PAYLOAD_COMBO].getText().trim();
				longValue		= new Long(selectionValue); 
				uiRule.value	= longValue;
			} else {
				selectionValue	= (String)comboBoxes[RTP_PAYLOAD_COMBO].getData("" + comboSelectionIndex);
				longValue		= new Long(selectionValue); 
				uiRule.value	= longValue;
			}
			
			break;
	    
		case EffistreamHelper.ID_TYPE_RTP_LENGTH:
			selectionValue 	=  textBoxes[RTP_LENGTH_MIN_TEXT_BOX].getText().trim();
			selectionValue 	+= ":" + textBoxes[RTP_LENGTH_MAX_TEXT_BOX].getText().trim();			
			
			rangeLowerLimit = Long.parseLong(textBoxes[RTP_LENGTH_MIN_TEXT_BOX].getText().trim());
			rangeUpperLimit = Long.parseLong(textBoxes[RTP_LENGTH_MAX_TEXT_BOX].getText().trim());
			
			if(validateRangeData(rangeLowerLimit, rangeUpperLimit) == false) {
				return false;
			}
			range.setLowerLimit(rangeLowerLimit);
			range.setUpperLimit(rangeUpperLimit);
			uiRule.value 	= range;
			break;
		}
		} catch (Exception e) {
			showErrorMessage("Invalid Value entered.",SWT.ICON_ERROR);
			return false;
		}
		return true;		
	}

	

	/**
	 * @param rangeLowerLimit
	 * @param rangeUpperLimit
	 */
	private boolean validateRangeData(long rangeLowerLimit, long rangeUpperLimit) {
		
		if(rangeLowerLimit > rangeUpperLimit) {
			showErrorMessage("Minimum value cannot be greater than maximum value.", SWT.ICON_INFORMATION);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 275;
		bounds.height	= 220;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}	
	
}
