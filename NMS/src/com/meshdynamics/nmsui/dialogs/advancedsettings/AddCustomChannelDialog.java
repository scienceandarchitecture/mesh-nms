/**
 * MeshDynamics 
 * -------------- 
 * File     : AddCustomChannelDialog.java
 * Comments : 
 * Created  :  DEC 14, 2006
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Apr 18, 2006   | created									  | Abhishek    |
 * ----------------------------------------------------------------------------------*/
 
package com.meshdynamics.nmsui.dialogs.advancedsettings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.nmsui.PortModelInfo;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;



public class AddCustomChannelDialog extends MeshDynamicsDialog {
	    
	  	private Button		btnDFS;
		private Label 		lblChannelNo;
		private Label 		lblChannelFreq;
		private Label		lblDFS;
		private Text 		txtChannelNo;
		private Text		txtChannelFreq;
		private Group		grp;
		
		private short		channelNo;
		private int			channelFreq;	
		private short		privateChannelFlag;
		
		private String 		radioType;
		private int			channelBandwidth;
		
	    public AddCustomChannelDialog(){
	        super();
	    }		
	  
	  
		/* (non-Javadoc)
		 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
		 */
		public void createControls(Canvas mainCnv){
			
			int rel_x = 5;
			int rel_y = 5;
			
			grp = new Group(mainCnv,SWT.NONE);
			grp.setBounds(rel_x,rel_y,245,100);
			
			rel_x = 15;
			rel_y = 20;
			
		    lblChannelNo = new Label(grp,SWT.NONE);
		    lblChannelNo.setText("Channel Number");
		    lblChannelNo.setBounds(rel_x,rel_y,100,20);
		    
		    rel_x = rel_x + 100;
		    
		    txtChannelNo = new Text(grp,SWT.BORDER);
		    txtChannelNo.setBounds(rel_x,rel_y,100,20);
		    txtChannelNo.setFocus();
		    
		    rel_x = 15;
		    rel_y = rel_y + 25;
		    
		    lblChannelFreq  = new Label(grp,SWT.NONE);
		    lblChannelFreq.setText("Channel Frequency");
		    lblChannelFreq.setBounds(rel_x,rel_y,100,20);
		   
		    rel_x = rel_x + 100;
		    
		    txtChannelFreq = new Text(grp,SWT.BORDER);
		    txtChannelFreq.setBounds(rel_x,rel_y,100,20);
		    
		    rel_x = 15;
			rel_y = rel_y + 25;
			
			lblDFS		  = new Label(grp,SWT.NONE);
			lblDFS.setBounds(rel_x,rel_y,100,20);
			lblDFS.setText("DFS");
		
			rel_x = rel_x + 100;
			
			btnDFS 		 = new Button(grp,SWT.CHECK);
			btnDFS.setBounds(rel_x,rel_y,60,20);
		
			super.setOkButtonText("OK");
			
		}
		
		private boolean getValues()
		{
			if(txtChannelNo.getText().trim().equals(""))
			{
				super.showErrorMessage("Channel number is blank.",SWT.ICON_ERROR);
				txtChannelNo.setFocus();
				return false;
			}
			try {
				  channelNo   = Short.parseShort(txtChannelNo.getText().trim());
			    }
			catch(Exception e)
			{
				super.showErrorMessage("Channel number should be numeric.",SWT.ICON_ERROR);
				txtChannelNo.setFocus();  
				return false;
			}
			if(txtChannelFreq.getText().trim().equals(""))
			{
				super.showErrorMessage("Channel frequency is blank.",SWT.ICON_ERROR);
				txtChannelFreq.setFocus();
				return false;
			}
			try
			{
				channelFreq = Integer.parseInt(txtChannelFreq.getText().trim());
			}
			catch(Exception e){
				
				super.showErrorMessage("Channel Frequency should be numeric.",SWT.ICON_ERROR);
				txtChannelFreq.setFocus();  
				return false;
			}
			
			if(btnDFS.getSelection() ==	true)
				privateChannelFlag =	1;
			if(btnDFS.getSelection() ==	false)
				privateChannelFlag =	0;
		
			return true;
		}
		
		
		/**
		 * 
		 */
		private boolean validateValues() {
			
			if(channelNo  <= 0 || channelNo > 255)	{
				super.showErrorMessage("Invalid Channel number.",SWT.ICON_ERROR);
				txtChannelNo.setFocus();
				return false;
			}
			
			int lowerVal	=	channelFreq - (getSelectedChannelBandWidth()/2);
			int upperVal	=	channelFreq + (getSelectedChannelBandWidth()/2);
		
			if(getRadioType().equals(PortModelInfo.FREQ_FIVE))
			{		
				if(channelFreq < 4899 || channelFreq   > 5999) {
					super.showErrorMessage("Invalid Channel Frequency.",SWT.ICON_ERROR);
					txtChannelFreq.setFocus();
					return false;
				}
				
				if(lowerVal > 5999 || lowerVal < 4899 ||
				   upperVal > 5999 || upperVal < 4899){
					
					super.showErrorMessage("Channel Frequency not within limit for "+getSelectedChannelBandWidth()+"MHz.",SWT.ICON_INFORMATION);
					txtChannelFreq.setFocus();
					return false;
				}
			
			}
			if(getRadioType().equals(PortModelInfo.FREQ_TWO)){
				
				if(channelFreq  < 2299 || channelFreq  > 2499){
					super.showErrorMessage("Invalid Channel Frequency.", SWT.OK|SWT.ICON_WARNING);
					txtChannelFreq.setFocus();
					return false;
			}
				if(lowerVal > 2499 || lowerVal < 2299 ||
				   upperVal > 2499 || upperVal < 2299){
				
					super.showErrorMessage("Channel Frequency not within limit for "+getSelectedChannelBandWidth()+"MHz.",SWT.ICON_INFORMATION);
					txtChannelFreq.setFocus();
					return false;
			   }
			}
			
			if(channelFreq < 4800){
				
					if(((channelFreq - 2192) % 5) == 0) {
						return true;
					}else if(((channelFreq - 2224) % 5) == 0){
						return true;
					}else{
							super.showErrorMessage("Invalid Channel Frequency.",SWT.ICON_ERROR);
							txtChannelFreq.setFocus();
							return false;
					}
			}else if(((channelFreq % 5) == 2) && (channelFreq <= 5435)) {
						return true;
					}else if ((channelFreq % 20) == 0 && channelFreq >= 5120) {
						return true;
					} else if ((channelFreq % 10) == 0) {
						return true;
					} else if ((channelFreq % 5) == 0) {
						return true;
				 }
					
			super.showErrorMessage("Invalid Channel Frequency.",SWT.ICON_ERROR);
			txtChannelFreq.setFocus();
			return false;	
		}

		
		/**
		 * @return Returns the channelFreq.
		 */
		public int getChannelFreq() {
			return channelFreq;
		}
		
		/**
		 * @return Returns the channelNo.
		 */
		public short getChannelNo() {
			return channelNo;
		}
		
		/**
		 * @return Returns the radioType.
		 */
		private String getRadioType() {
			return radioType;
		}

		/**
		 * @param radioType The radioType to set.
		 */
		public void setRadioType(String radioType) {
			this.radioType = radioType;
		}
		
		/**
		 * @return Returns the radioType.
		 */
		private int getSelectedChannelBandWidth() {
			return channelBandwidth;
		}

		/**
		 * @param channelWidth
		 */
		public void setSelectedChannelBandWidth(short channelWidth) {
			this.channelBandwidth = channelWidth; 
		}
		/**
		 * @return Returns the privateChannelFlag.
		 */
		public short getPrivateChannelFlag() {
			return privateChannelFlag;
		}

		/* (non-Javadoc)
		 * @see com.meshdynamics.meshviewer.dialogs.NewMeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
		 */
		protected void modifyMinimumBounds(Rectangle bounds) {
			bounds.width 	= 240;
			bounds.height	= 100;
		}

		/* (non-Javadoc)
		 * @see com.meshdynamics.meshviewer.dialogs.NewMeshDynamicsDialog#onImport()
		 */
		protected boolean onImport() {
			// TODO Auto-generated method stub
			return false;
		}

		/* (non-Javadoc)
		 * @see com.meshdynamics.meshviewer.dialogs.NewMeshDynamicsDialog#onExport()
		 */
		protected boolean onExport() {
			// TODO Auto-generated method stub
			return false;
		}


		/* (non-Javadoc)
		 * @see com.meshdynamics.meshviewer.dialogs.NewMeshDynamicsDialog#onCancel()
		 */
		protected boolean onCancel() {
			return true;
		}


		/* (non-Javadoc)
		 * @see com.meshdynamics.meshviewer.dialogs.NewMeshDynamicsDialog#onOk()
		 */
		protected boolean onOk() {
		
			if(getValues()== false) {
				return false;
			}
			if(validateValues()) {
				return true;
			}
			return false;
		}


		/* (non-Javadoc)
		 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
		 */
		protected boolean onApply() {
			// TODO Auto-generated method stub
			return false;
		}
}
