/**
 * MeshDynamics 
 * -------------- 
 * File     : AdvancedSettingsDlg.java
 * Comments : 
 * Created  : May 2, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date          |  Comment                                   | Author       |
 *   ----------------------------------------------------------------------------------
 * | 1  | Jun 7, 2007   | Save as functionality added      			 | Imran   		|
 *  ----------------------------------------------------------------------------------
 * | 0  | May 2, 2007   | Created                        			 | Abhishek     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.dialogs.advancedsettings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.impl.Configuration;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceCustomChannelConfig;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationTextWriter;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.MeshViewerProperties;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigPage;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.MacAddress;

public class AdvancedSettingsConfigDlg  extends MeshDynamicsDialog implements IMeshConfigBook {

	private Configuration		localConfig;
	private IConfiguration		originalConfig;
	private	IMeshConfigPage[]	pages;
	private CTabFolder			tabFolder;
	private Rectangle			minimumBounds;
	private Rectangle			minimumGrpBounds;
	
	private int 				currentSelectedPage;
	private int 				configMode;
	private int					lastSelectedPage;

	private IVersionInfo		versionInfo;
	private int					rfPageIndex;
	private	byte				pageFlags[];
	
	private static final byte	PAGE_FLAG_ENABLED	= 0x01;
	
	private static final int 	PAGE_COUNT			= 7;
	
	private void initPages() {
		
		pageFlags			= new byte[PAGE_COUNT];
		pages				= new IMeshConfigPage[PAGE_COUNT];
		int		pageNo		= 0;
		
		pages[pageNo]	= new IGMPPage(this);
		if(versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2, IVersionInfo.MINOR_VERSION_5,IVersionInfo.VARIANT_VERSION_50) == true) {
			pageFlags[pageNo] = PAGE_FLAG_ENABLED;
		}
		pages[pageNo++].setCaption("IGMP");
		
		pages[pageNo]	= new RfConfigPage(this);
		rfPageIndex		= pageNo;
		if((versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2, IVersionInfo.MINOR_VERSION_5,IVersionInfo.VARIANT_VERSION_30) == true) &&
				   MeshViewerProperties.rfEditorEnabled == Mesh.ENABLED) {
			pageFlags[pageNo] = PAGE_FLAG_ENABLED;
		}
		pages[pageNo++].setCaption("RF Editor(tm)");
		
		pages[pageNo]	= new EffistreamConfigPage(this);
		if(versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2, IVersionInfo.MINOR_VERSION_5,IVersionInfo.VARIANT_VERSION_39) == true) {
			pageFlags[pageNo] = PAGE_FLAG_ENABLED;
		}
		pages[pageNo++].setCaption("Effistream(tm)");

		pages[pageNo]	= new EthernetConfigPage(this);
		if(versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2, IVersionInfo.MINOR_VERSION_5,IVersionInfo.VARIANT_VERSION_35) == true) {
			pageFlags[pageNo] = PAGE_FLAG_ENABLED;
		}
		pages[pageNo++].setCaption("Ethernet");

		pages[pageNo]		= new Dot11eCategoryConfigPage(this);
		pageFlags[pageNo] 	= PAGE_FLAG_ENABLED;
		pages[pageNo++].setCaption("802.11e Category");

		pages[pageNo]	= new SipConfigPage(this);
		pageFlags[pageNo] = ~PAGE_FLAG_ENABLED;
/*		
		if(versionInfo.supportsSIP() == true) {
			pageFlags[pageNo] = PAGE_FLAG_ENABLED;
		}
*/		
		pages[pageNo++].setCaption("PBV(tm)");
		
		pages[pageNo]	= new P3MConfigPage(this);
		if(versionInfo.supportsP3M() == true) {
			pageFlags[pageNo] = PAGE_FLAG_ENABLED;
		}
		pages[pageNo++].setCaption("P3M(tm)");
		
		currentSelectedPage	= 0;
		lastSelectedPage	= 0;
	}
	
	/**
	 * @param configMode
	 * @param config
	 * @param versionInfo
	 */
	public AdvancedSettingsConfigDlg(int configMode,IConfiguration config, IVersionInfo versionInfo) {
		
		this.configMode			= configMode;
		this.versionInfo		= versionInfo;
		this.localConfig		= new Configuration();
		this.originalConfig		= config;
		
		originalConfig.copyTo(localConfig);
		
		super.setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK_CANCEL);
		super.showSaveAsButton(false);
		
		initPages();
	}
	
	protected void createControls(Canvas mainCnv) {
		
		tabFolder = new CTabFolder(mainCnv,SWT.BORDER);
		
		tabFolder.setSelectionForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		
		Display display = Display.getCurrent();
		
		tabFolder.setSelectionBackground(new Color[]{display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND), 
                					  display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND),
									  display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT), 
									  display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT)},
									  new int[] {40, 70, 100}, true);
		
		tabFolder.setBounds(5, 5, minimumBounds.width, minimumBounds.height);
		
		tabFolder.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
					
				pages[currentSelectedPage].selectionChanged(false);
				lastSelectedPage	= currentSelectedPage;
				currentSelectedPage = tabFolder.getSelectionIndex();
				if(currentSelectedPage!= lastSelectedPage) {
					pages[currentSelectedPage].selectionChanged(true);
				}
			}
			
		});
		
		for(int i=0;i<pages.length;i++) {
			pages[i].createControls(tabFolder);
			pages[i].setGrpBounds(minimumGrpBounds);
			pages[i].initalizeLocalData();
			if((pageFlags[i] & PAGE_FLAG_ENABLED) == 0)
				pages[i].setUIAccess(false);
		}

		super.setOkButtonText("Update");
		
	}

	protected boolean onCancel() {
		for(int i=0;i<pages.length;i++) {
			pages[i].onCancel();
		}
		return true;
	}

	protected boolean onOk() {
		
	    for(int i=0;i<pages.length;i++) {
		
			if(pages[i].onOk() == false) {
				return false;
			}
			if(i == rfPageIndex) {
				if(validateCustomChannels() == false){
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 
	 */
	private boolean validateCustomChannels() {
		
		if(localConfig.getCountryCode() != Mesh.CUSTOM_COUNTRY_CODE){
			return true;
		}
		
		IInterfaceConfiguration	interfaceConfiguration	= localConfig.getInterfaceConfiguration();
		int 	ifCount	= interfaceConfiguration.getInterfaceCount();
		
		for(int j = 0; j < ifCount; j++) {
			
			IInterfaceInfo	interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(j);
			if(interfaceInfo == null) {
				return false;
			}
			if(interfaceInfo.getMediumType() == Mesh.PHY_TYPE_802_3){
				continue;
			}
			InterfaceCustomChannelConfig customConfig	= (InterfaceCustomChannelConfig)interfaceInfo.getCustomChannelConfiguration();
			if(customConfig == null) {
				return false;
			}
			int channelInfoCount	= customConfig.getChannelInfoCount(); 
			if(channelInfoCount == 0){
				showErrorMessage("Custom Channels List empty for "+interfaceInfo.getName(),SWT.ICON_ERROR);
				return false;
			}
		}
		return true;
	}

	public IConfiguration getConfiguration() {
		return localConfig;
	}

	public int getConfigMode() {
		return configMode;
	}

	
	protected void modifyMinimumBounds(Rectangle bounds) {
		
		minimumBounds 		= new Rectangle(0,0,0,0);
		minimumGrpBounds	= new Rectangle(0,0,0,0);
		
		Rectangle pageBounds 	= null;
		for(int i=0;i<pages.length;i++) {
			
			pageBounds = pages[i].getMinimumBounds();
			if(pageBounds == null)
				continue;
		
			minimumBounds.width = (pageBounds.width > minimumBounds.width) ?
									pageBounds.width : minimumBounds.width;
			
			minimumBounds.height = (pageBounds.height > minimumBounds.height) ?
									pageBounds.height : minimumBounds.height;
			
			
		}

		minimumGrpBounds.width 	= minimumBounds.width;
		minimumGrpBounds.height = minimumBounds.height;
		
		minimumBounds.width 	+= 15;
		minimumBounds.height 	+= 15;
		
		bounds.width = (bounds.width > minimumBounds.width) ?
						bounds.width : minimumBounds.width;

		bounds.height = (bounds.height > minimumBounds.height) ?
						bounds.height : minimumBounds.height;
		
	}

	public void dataModified() {
		super.setIsDirty(true);
	}

	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	public int showMessage(String message, int swtIcon) {
		
		Shell shell = new Shell(Display.getDefault());
		MessageBox messageBox = new MessageBox(shell,SWT.MULTI|swtIcon);
		messageBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		messageBox.setMessage(message);
		return messageBox.open();
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.NewMeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		
		int pageNumber	=	tabFolder.getSelectionIndex();
		
		if(pages[pageNumber].validateLocalData() == false) {
			return false;
		}
		if(pages[pageNumber].getDataFromUIToConfiguration() == false) {
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#canDeleteVlan(int)
	 */
	public boolean canDeleteVlan(int tag) {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#isRadDecidesVlan()
	 */
	public boolean isRadDecidesVlan() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#calculateRadDecidesVlan()
	 */
	public void calculateRadDecidesVlan() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#getMeshNetWork()
	 */
	public MeshNetwork getMeshNetWork() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getIfEssIdByName(String ifName){
		return null;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#canAddVlan()
	 */
	public boolean canAddVlan() {
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#getVersionInfo()
	 */
	public IVersionInfo getVersionInfo() {
		return versionInfo;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onSaveAs()
	 */
	protected boolean onSaveAs() {
		if(onOk()) {
			if(writeConfigToFile() == true) {
				return true;
			}
		}
		return false;
	}
	
	private boolean writeConfigToFile() {
		
    	File 		file			 = null;
		String 		fileName 		 = "";
		boolean 	overWrite 		 = true;
		FileDialog 	fd				 = new FileDialog(new Shell(), SWT.SAVE | SWT.CANCEL);
		
		fd.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		
		while(overWrite){
		
			String mac	= getConfigurationFileName(localConfig.getDSMacAddress());
			fd.setFilterPath(MFile.getNodeFilePath());
			fd.setFileName(mac.substring(mac.length() - 21));
			fileName 	= fd.open();
			if (fileName == null)
				return false;
			
			int len	= fileName.length();  		
			if(!fileName.substring((len-5),len).equals(".conf"))
				fileName = fileName + ".conf";
			
			file = new File(fileName);
			if(file.exists() == true) {

				MessageBox msg = new MessageBox(new Shell(), SWT.YES | SWT.NO | SWT.ICON_INFORMATION);
				msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
				msg.setMessage("Existing values will be overwritten. " + SWT.LF + SWT.LF + "Do you want to continue?");

				int ans = msg.open();
				if (ans == SWT.YES) {
					overWrite = false;
				}
			}
			else {
				overWrite = false;
			}
		}
			
		if (file.exists() == false) {
			try {
				file.createNewFile();
			} catch (Exception e) {
				return false;
			}
		}

		if (file.canWrite() == false) {
			MessageBox msgBx = new MessageBox(new Shell(SWT.CLOSE), SWT.OK | SWT.ICON_INFORMATION);
			msgBx.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msgBx.setMessage("Save Failed. Templates are ReadOnly.");
			msgBx.open();
			return false;
		}
	
		try {
			FileOutputStream 		fileStream   = new FileOutputStream(file);
			ConfigurationTextWriter configWriter = new ConfigurationTextWriter(fileStream);
			localConfig.save(configWriter);
			configWriter.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		MessageBox msgBx = new MessageBox(new Shell(SWT.CLOSE), SWT.OK | SWT.ICON_INFORMATION);
		msgBx.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		msgBx.setMessage("File Saved.");
		msgBx.open();
		return true;
	}
	 
	 public String getConfigurationFileName(MacAddress macAddress) {
		String fileName = MFile.getNodeFilePath() + "/" +macAddress.toString().replace(':','_');
		fileName += ".conf";
		return fileName;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#isRfConfigChanged()
	 */
	public boolean isRfConfigChanged() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#essIdChangedForInterface(java.lang.String)
	 */
	public void essIdChangedForInterface(String ifName) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#isCountryCodeChanged()
	 */
	public boolean isCountryCodeChanged() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#notifyWPAPersonalKeyRegenerated()
	 */
	public void notifyWPAPersonalKeyRegeneratedFor(String name) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigBook#notifySecurityConfigurationChangedFor(java.lang.String)
	 */
	public void notifySecurityConfigurationChangedFor(String ifName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyDCAListChanged(String ifName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyDCAListRemove(String ifName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IInterfaceInfo getInterfaceInfoByName(String ifName) {
		// TODO Auto-generated method stub
		return null;
	}
}
