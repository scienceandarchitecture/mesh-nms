/**
 * MeshDynamics 
 * -------------- 
 * File     : RFProfileDlg.java
 * Comments : 
 * Created  : Oct 31, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Oct 31, 2007   | Created                        			  | Abhishek     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.dialogs.advancedsettings;

import java.util.Enumeration;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.configuration.IChannelInfo;
import com.meshdynamics.meshviewer.configuration.impl.ChannelInfo;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceCustomChannelConfig;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.nmsui.PortModelInfo;
import com.meshdynamics.nmsui.dialogs.base.IMessage;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.util.MFile;

public class RfProfileDlg extends MeshDynamicsDialog implements IMessage {

	private 	Group							grpMain;
	private 	Group							grpProfile;
	
	private 	int								rel_x;
	private 	int								rel_y;
	private 	List							lstProfiles;
	
	private 	Button							btnAdd;
	private 	Button							btnRemove;
	private 	Button 							btnViewValidChannels;
	private 	Button							btnAddProfile;
	private 	Button							btnDelProfile;
		
	private 	Table							tblChannels;
	
	private 	Label							lblChannels;
	private 	Label							lblProfileName;
	private 	Label							lblMediumSubType;
	private 	Label							lblMedType;
	
	private 	RFProfile						rfProfile;
	private 	int 							lastSelectedIndex;
	private 	Enumeration<RFProfile.Profile> 	enumProfiles;
	private 	RFProfile.Profile 				profile;
	private 	String							fileName;
	
	private 	short 							channelWidth;
	
	public RfProfileDlg(String fileName, short width) {

		this.fileName		= fileName;
		lastSelectedIndex	= -1;
		setButtonDisplayMode(BUTTON_DISPLAY_MODE_OK_CANCEL);
		setTitle("RF Profile");
		channelWidth		= width;	
	}
	
	
	/**
	 * 
	 */
	private void initializeData() {
		
		rfProfile		= new RFProfile();
		
		if(fileName != null) {
			enumProfiles	= rfProfile.getProfiles(fileName);
		}
		
		if(rfProfile.getProfileCount() == 0) {
			enableControls(false);
			return;
		}
		while(enumProfiles.hasMoreElements()) {
			
			RFProfile.Profile profile	=  (RFProfile.Profile)enumProfiles.nextElement();
			lstProfiles.add(profile.profileName);
			lstProfiles.setData(profile.profileName, profile);
		}		
		lstProfiles.select(0);
		lastSelectedIndex = 0;
		setProfileData(0);
		
	}

	/**
	 * @param b
	 */
	private void enableControls(boolean enable) {
		
		btnAdd.setEnabled(enable);
		btnRemove.setEnabled(enable);
		btnViewValidChannels.setEnabled(enable);
		btnDelProfile.setEnabled(enable);
	}

	/**
	 * Clearing the values of RF 
	 */
	public void clearAllValues() {
		tblChannels.removeAll();	
		lblMediumSubType.setText("");
	}
	/**
	 * @param i
	 */
	private void setProfileData(int i) {
		
		clearAllValues();
		if(i < 0) {
			return;
		}
		String 				itemName	= lstProfiles.getItem(i);
		RFProfile.Profile 	profile		= (RFProfile.Profile)lstProfiles.getData(itemName);
	
		int 	chCount	= profile.customChannelConfig.getChannelInfoCount();
		
		for(i =0;i<chCount;i++)
		{
			IChannelInfo	channelInfo	= (IChannelInfo)profile.customChannelConfig.getChannelInfo(i);
			setTableValues(channelInfo);
		}
		String subType	= "802.11 "+getMediumSubTypeName(profile.mediumSubType);
		lblMediumSubType.setText(subType);		
	}

	/**
	 * @param mediumSubType
	 * @return
	 */
	private String getMediumSubTypeName(short mediumSubType) {
		
		switch(mediumSubType) {

		case Mesh.PHY_SUB_TYPE_802_11_A:
			return RFProfile.PHY_SUB_TYPE_802_11_A;
		
		case Mesh.PHY_SUB_TYPE_802_11_B:
			return RFProfile.PHY_SUB_TYPE_802_11_B;
		
		case Mesh.PHY_SUB_TYPE_802_11_B_G:
			return RFProfile.PHY_SUB_TYPE_802_11_B_G;
		
		case Mesh.PHY_SUB_TYPE_802_11_G:
			return RFProfile.PHY_SUB_TYPE_802_11_G;
		
		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ:
			return RFProfile.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ;
		
		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ:
			return RFProfile.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ;
		
		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ:
			return RFProfile.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ;				
		}
		return RFProfile.PHY_SUB_TYPE_INGNORE;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.height	= 380;
		bounds.width	= 600;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	protected void createControls(Canvas mainCnv) {
		
		rel_x	= 15;
		rel_y	= 15;
		
		grpMain	= new Group(mainCnv, SWT.NONE);
		grpMain.setBounds(rel_x, rel_y,590,370);
		
		rel_y	= 15;
	
		lblProfileName	= new Label(grpMain,SWT.NO);
		lblProfileName.setText("Profiles");
		lblProfileName.setBounds(rel_x, rel_y,80,20);
		
		rel_y += 20;
		
		lstProfiles	= new List(grpMain,SWT.V_SCROLL|SWT.H_SCROLL|SWT.BORDER);
		lstProfiles.setBounds(rel_x, rel_y+5, 150,270);
		lstProfiles.addSelectionListener(new SelectionListener(){

			public void widgetSelected(SelectionEvent arg0) {
				
				if(compareDataWithUI() == false) {
					
					int result	= showMessage("Do you want to save the changes ? ",SWT.YES|SWT.CANCEL|SWT.NO);
					
					if(result == SWT.CANCEL) {
						
						lstProfiles.select(lastSelectedIndex);
						return;
						
					}else if(result	== SWT.NO) {
						setProfileData(lastSelectedIndex);			
						
					}else if(result	== SWT.YES) {
						
						getDataFromUIToConfiguration();
					}	
				}
				lastSelectedIndex	= lstProfiles.getSelectionIndex(); 
				setProfileData(lastSelectedIndex);
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				
			}
			
		});
		rel_x	+= 160;
		
		rel_y -= 20;
		
		grpProfile	= new Group(grpMain, SWT.NONE);
		grpProfile.setBounds(rel_x, rel_y,400,340);
		
		createRfChannelControls();
		
		rel_x = 18;
		rel_y = 325;
		
		btnAddProfile	= new Button(grpMain,SWT.PUSH);
		btnAddProfile.setText("Add");
		btnAddProfile.setBounds(rel_x,rel_y,70,20);
		btnAddProfile.addSelectionListener(new SelectionListener(){

			public void widgetSelected(SelectionEvent arg0) {
				addNewProfile();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		rel_x += 75;
		
		btnDelProfile	= new Button(grpMain,SWT.PUSH);
		btnDelProfile.setText("Delete");
		btnDelProfile.setBounds(rel_x,rel_y,70,20);
		btnDelProfile.addSelectionListener(new SelectionListener(){

			public void widgetSelected(SelectionEvent arg0) {
				deleteProfile();
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});

		initializeData();		
	}

	/**
	 * 
	 */
	protected void deleteProfile() {
		
		int index	= lstProfiles.getSelectionIndex();
		if(index < 0) {
			return;			
		}
		String 			  profName		= lstProfiles.getItem(index);
		
		rfProfile.deleteProfile(profName);
		lstProfiles.remove(index);
		
		lastSelectedIndex	= index-1;
		if(lastSelectedIndex < 0) {
			if(lstProfiles.getItemCount() != 0){
				lastSelectedIndex = 0;
				lstProfiles.select(lastSelectedIndex);
				setProfileData(lastSelectedIndex);
				return;
			}
			if(lstProfiles.getItemCount() == 0) {
				enableControls(false);
			}
		}
		lstProfiles.select(lastSelectedIndex);
		setProfileData(lastSelectedIndex);
	}

	/**
	 * 
	 */
	protected void addNewProfile() {
		
		NewProfileDlg newProfileDlg	= new NewProfileDlg();
		newProfileDlg.show();
		
		if(newProfileDlg.getStatus() != SWT.OK){
			return;
		}
		String profileName		= newProfileDlg.getProfileName();
		short  mediumSubType	= newProfileDlg.getMediumSubType();
		
		if(checkForRepeatedProfileNames(profileName) == false) {
			return; 
		}
		
		RFProfile.Profile newProfile	= rfProfile.addNewProfile(profileName, mediumSubType);
		lstProfiles.add(profileName);
		lstProfiles.setData(profileName, newProfile);
		
		int items	= lstProfiles.getItemCount();
		lstProfiles.select(items-1);
		
		getDataFromUIToConfiguration();
		
		lastSelectedIndex = items-1;
		setProfileData(lastSelectedIndex);
		enableControls(true);
	}

	/**
	 * @param profileName
	 * @return
	 */
	private boolean checkForRepeatedProfileNames(String profileName) {
		
		int cnt	= lstProfiles.getItemCount();
		for(int i = 0; i < cnt; i++) {
			String itemName	= lstProfiles.getItem(i);
			if(itemName.equals(profileName)) {
				showErrorMessage("Profile '"+profileName+"' already exists.", SWT.ICON_INFORMATION| SWT.OK);
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 */
	protected void getDataFromUIToConfiguration() {
		
		if(lstProfiles.getItemCount() == 0) {
			return;
		}		
		if(lastSelectedIndex < 0) {
			return;
		}
		String 			  profName		= lstProfiles.getItem(lastSelectedIndex);
		RFProfile.Profile lastProfile 	= (RFProfile.Profile)lstProfiles.getData(profName);
		InterfaceCustomChannelConfig customConfig	= lastProfile.customChannelConfig;
		
		if(customConfig == null) {
			return;
		}
		try{
			
			customConfig.setChannelBandwidth(channelWidth);
			customConfig.setChannelInfoCount(tblChannels.getItemCount());
			
			int i = 0;
			for(i = 0; i < customConfig.getChannelInfoCount(); i++) {
			
				IChannelInfo custChInfo	= (IChannelInfo)customConfig.getChannelInfo(i);
				if(custChInfo == null) {
					continue;
				}
				TableItem item	= tblChannels.getItem(i);
				String	chNo	= item.getText(0).trim();
				String 	chFreq	= item.getText(1).trim();
				String	chDfs	= item.getText(3).trim();
				
				short channelNo		= Short.parseShort(chNo);
				long  channelFreq 	= Long.parseLong(chFreq);
				short channelDfs	= 0;
					
				if(chDfs.equals(RfConfigPage.YES)) {
					channelDfs	= 1;
				}
				custChInfo.setChannelNumber(channelNo);
				custChInfo.setFrequency(channelFreq);
				custChInfo.setPrivateChannelFlags(channelDfs);
				
			}
			int channelInfoCount	= customConfig.getChannelInfoCount();
			short[] dcaList = new short[channelInfoCount];
			
			for(i = 0; i < channelInfoCount; i++) {
				
	            IChannelInfo  ch = (IChannelInfo) customConfig.getChannelInfo(i);
				if(ch == null) { 
					continue;
				}
				dcaList[i]	= ch.getChannelNumber();				
			}			
		}catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}

	/**
	 * @return
	 */
	protected boolean compareDataWithUI() {
	
		String 			  profName		= lstProfiles.getItem(lastSelectedIndex);
		RFProfile.Profile lastProfile 	= (RFProfile.Profile)lstProfiles.getData(profName);
		
		if(compareChannelInfo(lastProfile.customChannelConfig) == false) {
			return false;
		}
		return true;
	}

	/**
	 * @param customConfig
	 * @param interfaceInfo
	 */
	private boolean compareChannelInfo(InterfaceCustomChannelConfig customConfig) {
		
		int i					= 0;
		int	j					= 0;
		int configChangedFlag	= 0;
		
		int itemCount			= tblChannels.getItemCount();
		int customChCount		= customConfig.getChannelInfoCount();
		
		if(customChCount != itemCount) {
			return false;
		}
		
		for(i = 0; i < itemCount; i++) {
			
			TableItem item	= tblChannels.getItem(i);
			String	chNo	= item.getText(0).trim();
			String 	chFreq	= item.getText(1).trim();
			String	chDfs	= item.getText(3).trim();
			
			try{
				short channelNo		= Short.parseShort(chNo);
				long  channelFreq 	= Long.parseLong(chFreq);
				short channelDfs	= 0;
				
				if(chDfs.equals(RfConfigPage.YES)) {
					channelDfs	= 1;
				}
				
				for(j = 0; j < customChCount; j++) {
					
					IChannelInfo customChInfo	= (IChannelInfo)customConfig.getChannelInfo(j);
					if(customChInfo == null) {
						continue;
					}
					
					if(customChInfo.getChannelNumber() 		== channelNo		&&
					   customChInfo.getFrequency()	   		== channelFreq 		&&
					   customChInfo.getPrivateChannelFlags()== channelDfs) {
						
						configChangedFlag++;
						break;
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(configChangedFlag != customChCount) {
			return false;
		}
		return true;
	}
	

	/**
	 *   
	 */
	private void createRfChannelControls() {
		
		rel_x = 15;
		rel_y = 25;
		
		lblMedType	= new Label(grpProfile,SWT.NO);
		lblMedType.setText("Medium Subtype");
		lblMedType.setBounds(rel_x, rel_y,90,20);
		
		rel_x = rel_x + 110;
		
		lblMediumSubType	= new Label(grpProfile,SWT.NO);
		lblMediumSubType.setBounds(rel_x, rel_y,100,20);
		
		rel_y += 25;
		rel_x = 15;
		
		rel_x = 15;
		
		lblChannels	= new Label(grpProfile,SWT.NONE);
		lblChannels.setBounds(rel_x,rel_y,100,20);
		lblChannels.setText("Channels Description");
	
		rel_x = 15;
		rel_y = rel_y + 25;
			
		tblChannels	= new Table(grpProfile,SWT.V_SCROLL|SWT.BORDER|SWT.SINGLE|SWT.FULL_SELECTION);
		tblChannels.setBounds(rel_x,rel_y,304,218); 	
		tblChannels.setHeaderVisible(true);
		tblChannels.setLinesVisible(true);
		
		String Headers[]	= {"Channel","Center Freq","Occupied Range","DFS"};
		
		TableColumn col;
		int i;
		
		for(i = 0; i < Headers.length; i++ )
		{
			col	=	new TableColumn(tblChannels, SWT.NONE);
			col.setText(Headers[i]);
		}
		
		col	= tblChannels.getColumn(0);
		col.setWidth(55);
		col = tblChannels.getColumn(1);
		col.setWidth(75);
		col = tblChannels.getColumn(2);
		col.setWidth(120);
		col = tblChannels.getColumn(3);
		col.setWidth(50);

		rel_x = rel_x + 315;
		rel_y += 70;
		btnAdd	= new Button(grpProfile,SWT.PUSH);
		btnAdd.setText("Add");
		btnAdd.setBounds(rel_x,rel_y,60,20);
		btnAdd.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				addCustomChannels();
			}
		}); 
		
		rel_y = rel_y + 25;
		
		btnRemove	= new Button(grpProfile,SWT.PUSH);
		btnRemove.setText("Remove");
		btnRemove.setBounds(rel_x,rel_y,60,20);
		btnRemove.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				 removeFromChannelsTable();				// Removing the selected entry from the channels table
			}
		});
		
		rel_y = rel_y + 25;
		
		btnViewValidChannels	= new Button(grpProfile,SWT.PUSH);
		btnViewValidChannels.setText("Valid Freq");
		btnViewValidChannels.setBounds(rel_x,rel_y,60,20);
		btnViewValidChannels.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				viewValidFrequencies();
			}
		});		
	}


	private void viewValidFrequencies() {
		
		int 				index		= lstProfiles.getSelectionIndex();
		if(index < 0) {
			return;
		}
		String 				itemName	= lstProfiles.getItem(index);
		RFProfile.Profile 	profile		= (RFProfile.Profile)lstProfiles.getData(itemName);
		
		String strRadioType	= getRadioType(profile.mediumSubType);
		
		short   			width		= profile.customChannelConfig.getChannelBandwidth();
		short 				medType		= profile.mediumSubType;
		
		ValidChannelFrequencyDlg 	dlg 			= new ValidChannelFrequencyDlg((short)medType,width);
		dlg.setTitle("Valid Frequencies for "+strRadioType);
		dlg.show();
	}
	
	/**
	 * Function to remove the selected entry from the channels table
	 */
	private void removeFromChannelsTable() {
		
		int index = tblChannels.getSelectionIndex();
		if(index < 0)
			return ;
		
		TableItem item 	= tblChannels.getItem(tblChannels.getSelectionIndex());
			
		if(item != null) {
			item.dispose();
		}
		
		if(tblChannels.getItemCount() > 0){
			if(index > 0)
				tblChannels.setSelection(index - 1);
			else if(index == 0)
				tblChannels.setSelection(index);
		}
		else if(tblChannels.getItemCount() == 0){
			tblChannels.setSelection(-1);			
		}		
	}
	
	

	
	/**
	 * Adding custom channels to Table
	 */
	private void addCustomChannels(){
	
		if(lastSelectedIndex < 0){
			return;
		}
		String name	= lstProfiles.getItem(lastSelectedIndex);
		RFProfile.Profile profile	= (RFProfile.Profile) lstProfiles.getData(name);
		
		String strRadioType	= getRadioType(profile.mediumSubType);
		
		AddCustomChannelDialog	objRFChannel	= new AddCustomChannelDialog();
		objRFChannel.setRadioType(strRadioType); 		// setting radiotype for adding channels to 
														// selected interface and to check the center frequency
		objRFChannel.setTitle("Add Channel for "+strRadioType);
		objRFChannel.setSelectedChannelBandWidth(channelWidth); 
		objRFChannel.show();
		
		if(objRFChannel.getStatus()!= SWT.OK)
			return;
		
		short chPriFlag = 0;
		short chNo   	= objRFChannel.getChannelNo();
		long  chFreq 	= objRFChannel.getChannelFreq();
		chPriFlag		= (short)(chPriFlag | (objRFChannel.getPrivateChannelFlag()));
		
		IChannelInfo uiChannelInfo	=	new ChannelInfo();
		uiChannelInfo.setChannelNumber(chNo);
		uiChannelInfo.setFrequency(chFreq);
		uiChannelInfo.setPrivateChannelFlags(chPriFlag);
		
		if(checkRepeatedValues(uiChannelInfo) == false) {
			return;
		}
		setTableValues(uiChannelInfo);
	}

	/**
	 * @param mediumSubType
	 * @return
	 */
	private String getRadioType(short mediumSubType) {
		
		switch(mediumSubType) {
			
			case Mesh.PHY_SUB_TYPE_802_11_A:
				return PortModelInfo.FREQ_FIVE;
			
			case Mesh.PHY_SUB_TYPE_802_11_B:
			case Mesh.PHY_SUB_TYPE_802_11_B_G:
			case Mesh.PHY_SUB_TYPE_802_11_G:
				return PortModelInfo.FREQ_TWO;
			
			case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ:
			case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ:	
			case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ:
				return PortModelInfo.FREQ_FOUR_DOT_NINE;
		}
		return null;
	}

	/**
	 * Setting the values of channel in table
	 */
	private void setTableValues(IChannelInfo channelInfo) {
		
		TableItem newItem;
		
		newItem 				= new TableItem(tblChannels,SWT.NONE);
		
		short 	channelNo    	= channelInfo.getChannelNumber();
		long  	channelFreq  	= channelInfo.getFrequency();
		short 	chPriFlag		= channelInfo.getPrivateChannelFlags();
		String 	dfsFlag 		= RfConfigPage.NO;

		if(chPriFlag  == 1) {
			dfsFlag	   =  RfConfigPage.YES;
		}

		newItem.setText(0,""+channelNo);
		newItem.setText(1,""+channelFreq);
		newItem.setText(3,""+dfsFlag);

		try
		{
			float upperVal=0,lowerVal=0;
			
			upperVal = channelFreq + (channelWidth/2);
			lowerVal = channelFreq - (channelWidth/2);
			
			newItem.setText(2," "+lowerVal+" to "+upperVal);			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * Function to check the repeated values before adding to the table
	 * checks for the channel number, channel frequency. 
	 */
	private boolean checkRepeatedValues(IChannelInfo channelInfo) 
	{
		TableItem item;
		
		short 	channelNo    	= channelInfo.getChannelNumber();
		long  	channelFreq  	= channelInfo.getFrequency();
		
		for(int i=0; i < tblChannels.getItemCount();i++)
		{
			item = tblChannels.getItem(i);
			if(Short.parseShort(item.getText(0).trim())	== 	channelNo)
			{
				btnAdd.setFocus();
				super.showErrorMessage("Channel Number already present. ",SWT.OK|SWT.ICON_ERROR);
				return false;
			}
			if(Integer.parseInt(item.getText(1).trim())	==	channelFreq)
			{
				btnAdd.setFocus();
				super.showErrorMessage("Channel Frequency already present",SWT.OK|SWT.ICON_ERROR);
				return false;
			}
		}
		return true;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	protected boolean onOk() {
	
		getDataFromUIToConfiguration();
		
		if(fileName == null) {
			
			FileDialog fileDlg	= new FileDialog(new Shell());
			fileDlg.setFileName(MFile.getConfigPath());
			fileDlg.open();
			fileName	= fileDlg.getFilterPath()+"\\"+fileDlg.getFileName();
			if (fileName == null) {
				return false;
			}
		}
		rfProfile.save(fileName);
		
		if(lastSelectedIndex < 0) {
			return true;
		}
		String	itemName	= lstProfiles.getItem(lastSelectedIndex);
		profile				= (RFProfile.Profile)lstProfiles.getData(itemName);
	
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	protected boolean onCancel() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		
		
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMessage#showMessage(java.lang.String, int)
	 */
	public int showMessage(String message, int style) {
		
		Shell shell = new Shell(Display.getDefault());
		MessageBox messageBox = new MessageBox(shell,SWT.MULTI|style);
		messageBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		messageBox.setMessage(message);
		return messageBox.open();	
	}

	/**
	 * @return
	 */
	public RFProfile.Profile getProfileToBeImported() {
		return profile;
	}
}
