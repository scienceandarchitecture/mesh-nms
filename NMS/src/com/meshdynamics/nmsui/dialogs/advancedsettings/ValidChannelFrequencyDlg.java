/**
 * MeshDynamics 
 * -------------- 
 * File     : ValidChannelFrequencyDlg.java
 * Comments : 
 * Created  : Jan 5, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author     |
 *  -------------------------------------------------------------------------------
 * | 2  |May 1, 2007     | Changes due to MeshDynamicsDialog          |Abhishek    |
 * --------------------------------------------------------------------------------
 * | 1  | Mar 16, 2007   | PhySubtype chk added(for 4.9PublicSafety)  | Abhishek   |
 * ----------------------------------------------------------------------------------
 * | 0  | DEC 14, 2006   | created									  | Abhishek   |
 * --------------------------------------------------------------------------------*/
 

package com.meshdynamics.nmsui.dialogs.advancedsettings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.util.ValidFreqChecker;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;

public class ValidChannelFrequencyDlg extends MeshDynamicsDialog {

	
	private	Text 		txtValidFreq;
	//private String		radioType;
	private short		phySubType;
	private int 		bandWidth;
	
	public ValidChannelFrequencyDlg(short phySubType, int bandWidth){
		super();
		//this.radioType	= radioType;
		this.phySubType	= phySubType;
		this.bandWidth	= bandWidth;
		setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_OK);
	}
	

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	public void createControls(Canvas mainCnv) {
		
		int rel_x = 10;
		int rel_y = 15;
		
		txtValidFreq = new Text(mainCnv ,SWT.BORDER|SWT.MULTI|SWT.V_SCROLL|SWT.READ_ONLY);
		txtValidFreq.setBounds(rel_x,rel_y,340,200);
		
		setFrequencies();
	}
	
	private void setFrequencies(){
		 
		ValidFreqChecker obj	=	new ValidFreqChecker();
			
		if(phySubType == Mesh.PHY_SUB_TYPE_802_11_A){
			
			int lowerVal	=	4899 + (bandWidth/2);
			int upperVal	=	5999 - (bandWidth/2);
			obj.freqChecker(lowerVal,upperVal);
			
		}
		else if(phySubType == Mesh.PHY_SUB_TYPE_802_11_B ||
				phySubType == Mesh.PHY_SUB_TYPE_802_11_G ||
				phySubType == Mesh.PHY_SUB_TYPE_802_11_B_G 
				){
			
			int lowerVal	=	2299 + (bandWidth/2);
			int upperVal	=	2499 - (bandWidth/2);
			obj.freqChecker(lowerVal,upperVal);
			
		}else{

			String str = "Valid list of frequencies is not available.";
			txtValidFreq.setText(str);
			return;
		}
		
		int size 	=	obj.getValidChLength(); 
		String str = "\n";
		int j = 0;
		
		for(int i=0; i <size-1 ; i++){
		
			int chFreq		=   obj.getValidChannel(i);
			str = str + chFreq  + ", ";
			j++;
			
			if(j == 10){
				j=0;
				str =  str + "\n" ;
			}
		}
		
		str = str + obj.getValidChannel(size-1)+ ".\n";
		
		txtValidFreq.setText(str);
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	public boolean onOk() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	public boolean onCancel() {
		return true;
	}

	/**
	 * 
	 */
	public boolean onExit() {
		 return true;   
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 345;
		bounds.height	= 210;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

}
