/**
 * MeshDynamics 
 * -------------- 
 * File     : NewProfileDlg.java
 * Comments : 
 * Created  : Nov 02, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Nov 02, 2007   | Created                        			  | Abhishek     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.dialogs.advancedsettings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;


public class NewProfileDlg extends MeshDynamicsDialog {

	private Group		grpMain;
	
	private int			rel_x;
	private int			rel_y;

	private Text		txtProfileName;
	private Combo 		cmbMediumSubType;
	
	private Label		lblProfileName;
	private Label		lblMediumSubType;
	
	private String		profileName;
	private short		mediumSubType;	
	
	public NewProfileDlg() {
		setButtonDisplayMode(BUTTON_DISPLAY_MODE_OK_CANCEL);
		setTitle("New RF Profile");
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 325;
		bounds.height	= 100;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	protected void createControls(Canvas mainCnv) {
		
		rel_x	= 15;
		rel_y	= 15;
		
		grpMain	= new Group(mainCnv, SWT.NONE);
		grpMain.setBounds(rel_x, rel_y,310,90);
		
		rel_y += 5;
		
		lblProfileName	= new Label(grpMain,SWT.NO);
		lblProfileName.setText("Profile Name");
		lblProfileName.setBounds(rel_x, rel_y,100,20);
		
		rel_x += 110;
		
		txtProfileName	= new Text(grpMain, SWT.BORDER);
		txtProfileName.setBounds(rel_x, rel_y, 100, 20);
		
		rel_x = 15;
		rel_y += 30;
		
		lblMediumSubType = new Label(grpMain,SWT.NO);
		lblMediumSubType.setText("Medium Subtype");
		lblMediumSubType.setBounds(rel_x, rel_y,100,20);
		
		rel_x += 110;
		
		cmbMediumSubType	= new Combo(grpMain, SWT.BORDER|SWT.READ_ONLY);
		cmbMediumSubType.setBounds(rel_x, rel_y, 175, 20);
		
		cmbMediumSubType.add("Select");
		cmbMediumSubType.add("802.11 A");
		cmbMediumSubType.add("802.11 B");
		cmbMediumSubType.add("802.11 G");
		cmbMediumSubType.add("802.11 BG");
//		cmbMediumSubType.add("802.11 PUBLIC SAFETY 5 MHZ");
//		cmbMediumSubType.add("802.11 PUBLIC SAFETY 10 MHZ");
//		cmbMediumSubType.add("802.11 PUBLIC SAFETY 20 MHZ");
		
		cmbMediumSubType.select(0);
		txtProfileName.setFocus();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	protected boolean onOk() {
		if(validateData() == false) {
			return false;
		}
		return true;
	}

	/**
	 * @return
	 */
	private boolean validateData() {
		
		profileName	=	txtProfileName.getText().trim();
		
		if(profileName.equals("")){
			super.showErrorMessage("Profile Name is blank.", SWT.ICON_INFORMATION);
			return false;
		}
		mediumSubType	= (short)cmbMediumSubType.getSelectionIndex();
		
		if(mediumSubType <= 0) {
			super.showErrorMessage("Medium Subtype is not selected.", SWT.ICON_INFORMATION);
			return false;
		}		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	protected boolean onCancel() {
		// TODO Auto-generated method stub
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}
	

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @return
	 */
	public String getProfileName() {
		return profileName;
	}

	public short getMediumSubType(){
		return mediumSubType;
	}
}
