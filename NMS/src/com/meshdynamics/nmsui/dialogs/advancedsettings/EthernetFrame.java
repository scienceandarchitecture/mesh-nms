/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EthernetFrame.java
 * Comments : Creates frame on the Ethernet Tab.
 * Created  : Feb 21, 2007
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 23, 2009 | Fix for single radio configuration				 | Abhijit |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 21, 2007 | Created                                         | Imran   |
 * --------------------------------------------------------------------------------
 **********************************************************************************/
package com.meshdynamics.nmsui.dialogs.advancedsettings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;

public class EthernetFrame extends Canvas{
	
	private 	IEthernetFrameListener 	listener;
	private		String					frmTitle; 
	private		Group					grp;
	private		Button[]				btnRadios;
	private		Combo					cmbVLANList;
	private     EthernetInfo		    ethernetInfo;
	
	private static  final	int		ALLOW_ALL_VLANS			 	= 0;
	private static  final 	int		DISALLOW_ALL_VLANS		 	= 1;
	private static  final 	int		ALLOW_SELECTED_VLANS	 	= 2;
	
	private static  final 	String	STR_ALLOW_ALL_VLANS		 	= "Allow all VLANs";
	private static  final 	String	STR_DISALLOW_ALL_VLANS  	= "Disallow all VLANs";
	private static  final 	String	STR_ALLOW_SELECTED_VLANS 	= "Allow Selected VLAN";
	
	
	public  static	final 	String 	VLAN_ESSID_DISALLOW_VLANS	= "MD-PRIV-SSID-NO-VLAN";
	public  static  final 	String	VLAN_ESSID_ALLOW_VLANS		= "MD-PRIV-SSID-ALL-VLAN";
	public  static  final 	String	VLAN_NAME_NONE				= "VLAN-NONE";
	
	public EthernetFrame(Composite parent,IEthernetFrameListener listener,String frmTitle){
		
		super(parent,SWT.NONE);	
		
		this.listener 	= listener;
		this.frmTitle	= frmTitle;
		
		createControls();
		
	}
	
	public void createControls(){
		
		int rel_x;
		int rel_y;
		int numRadios;
		int index;
		
		rel_x		= 15;
		rel_y		= 15;
		numRadios	= 3;
		
		grp = new Group(this,SWT.NO);
		grp.setBounds(rel_x,rel_y,375,135);
		grp.setText(frmTitle);
		
		SelectionAdapter selectionAdapter = new SelectionAdapter(){
			EthernetFrameSelectionEvent efsEvent = new EthernetFrameSelectionEvent();
			public void widgetSelected(SelectionEvent arg0) {
				int i;
				
				if(listener == null)
					return;

				Button btn = (Button) arg0.getSource();				
				if(btn.getSelection() == false)
					return;
								
				for(i = 0; i < btnRadios.length; i++) {
					if(arg0.getSource().equals(btnRadios[i])) {
						efsEvent.setSelectedVlanConfig(i);
						listener.selectedConfiguration(efsEvent);
						enableControls(arg0.getSource());
						break;
					}
				}
			}
		};
		
		rel_x 		-=5;
		rel_y	 	+=5;
		btnRadios 	= new Button[numRadios];
	  	    
	    for(index = 0; index < numRadios; index++) {
	    	
	    	btnRadios[index] = new Button(grp,SWT.RADIO);
			btnRadios[index].setBounds(rel_x,rel_y,125,20);
			btnRadios[index].addSelectionListener(selectionAdapter);
			setButtonText(btnRadios[index],index+1);
			rel_y			 += 25;
		}
		    	
	    rel_x		+= 140;
	    rel_y		-= 25;
	    cmbVLANList		= new Combo(grp,SWT.BORDER|SWT.READ_ONLY); 
		cmbVLANList.setBounds(rel_x,rel_y,150,20);
		cmbVLANList.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				listener.selectedVlan("vlanName");
			}
		});
			
	}
	
	public void initialiseEthernetInfo(IInterfaceInfo ifInfo, IVlanConfiguration vlanConfig) {
		
		int 	vlanCount;
		int 	index;
		String	essId;
				
		fillVlanList(vlanConfig);
		
		essId					= ifInfo.getEssid();
		vlanCount				= vlanConfig.getVlanCount();
		ethernetInfo		 	= new EthernetInfo();
		ethernetInfo.setIfInfo(ifInfo);
				
		if((essId.equals(VLAN_ESSID_DISALLOW_VLANS)) || (essId.equals(VLAN_ESSID_ALLOW_VLANS))) {
			ethernetInfo.setVlanName(VLAN_NAME_NONE);
		}else {
			for(index=0; index<vlanCount; index++) {
				IVlanInfo	vlanInfo	= (IVlanInfo)vlanConfig.getVlanInfoByIndex(index);
				if(vlanInfo.getEssid().equals(essId)) {
					ethernetInfo.setVlanName(vlanInfo.getName());
				}
			}
		}
		displayEthernetValues();
	}
	
	private void fillVlanList(IVlanConfiguration vlanConfig) {
		int			vlanCount;
		int 		index;
		
		cmbVLANList.removeAll();
		vlanCount	= vlanConfig.getVlanCount();
		for(index = 0; index < vlanCount; index++) {
			IVlanInfo	vlanInfo	= (IVlanInfo)vlanConfig.getVlanInfoByIndex(index);
			if(vlanInfo == null) {
				continue;
			}
			cmbVLANList.add(vlanInfo.getName());
			cmbVLANList.setData(vlanInfo.getName(), vlanInfo.getEssid());
		}
		if(vlanCount == 0) {
			btnRadios[ALLOW_SELECTED_VLANS].setEnabled(false);
		}else {
			btnRadios[ALLOW_SELECTED_VLANS].setEnabled(true);
			cmbVLANList.select(0);
		}
	}
		
	private void displayEthernetValues() {
		
		boolean 	vlanFound;
		int			index;
		
		vlanFound	= false;	
		
		if(ethernetInfo.getEssid().equals(VLAN_ESSID_ALLOW_VLANS)) {
			selectRadio(ALLOW_ALL_VLANS);
			cmbVLANList.setEnabled(false);
		}
		else if(ethernetInfo.getEssid().equals(VLAN_ESSID_DISALLOW_VLANS)) {
			selectRadio(DISALLOW_ALL_VLANS);
			cmbVLANList.setEnabled(false);
		}
		else {
			for(index = 0; index < cmbVLANList.getItemCount(); index++) {
				if(ethernetInfo.getVlanName().equals(cmbVLANList.getItem(index))) {
					cmbVLANList.select(index);
					vlanFound = true;
					break;
				}				
			}
			if(vlanFound == false) {
				if(cmbVLANList.getItemCount() > 0) {
					cmbVLANList.select(0);
				}
				cmbVLANList.setEnabled(false);
				selectRadio(ALLOW_ALL_VLANS);
			} else if(vlanFound == true) {	
				selectRadio(ALLOW_SELECTED_VLANS);
				cmbVLANList.setEnabled(true);
			}		
		}
	}
	
	public void setRadioSelection(int index,boolean enabled) {
	    btnRadios[index].setSelection(enabled);
	}
	
	/**
	 * 
	 */
	private void selectRadio(int radioIndex) {
		int index;
		for(index=0; index<btnRadios.length;index++) {
			if(index == radioIndex) {
				btnRadios[index].setSelection(true);
			}else {
				btnRadios[index].setSelection(false);
			}
		}
	}

	public void getDataFromUIToConfiguration() {
	
		int		index;
		String 	vlanName;
		String 	essId;
		
		if(btnRadios[ALLOW_ALL_VLANS].getSelection() == true) {
			ethernetInfo.setEssid(VLAN_ESSID_ALLOW_VLANS);
		}
		else if(btnRadios[DISALLOW_ALL_VLANS].getSelection()	== true) {
			ethernetInfo.setEssid(VLAN_ESSID_DISALLOW_VLANS);
		}
		else if(btnRadios[ALLOW_SELECTED_VLANS].getSelection()	== true) {
			index	= cmbVLANList.getSelectionIndex();
			vlanName	= cmbVLANList.getItem(index);
			essId		= (String)cmbVLANList.getData(vlanName);
			ethernetInfo.setEssid(essId);
		} 
	}
	
	private	void enableControls(Object objRadioButton){
		String	radioText;
		
		Button  radioButton	= (Button)objRadioButton;
		radioText			= radioButton.getText();
		
		if(radioText.equalsIgnoreCase(STR_ALLOW_ALL_VLANS)) {
			if(radioButton.getSelection() == true) {
				cmbVLANList.setEnabled(false);
			}
		}
		else if(radioText.equalsIgnoreCase(STR_DISALLOW_ALL_VLANS)) {
			if(radioButton.getSelection() == true) {
				cmbVLANList.setEnabled(false);
			}
		}
		else if(radioText.equalsIgnoreCase(STR_ALLOW_SELECTED_VLANS)) {
			if(radioButton.getSelection() == true) {
				cmbVLANList.setEnabled(true);
			}
		}
	}
	
	private void setButtonText(Button button, int i) {
		String labelText = "";
		
		switch(i) {
		case 1:
			labelText = STR_ALLOW_ALL_VLANS;
			break; 
		case 2:
			labelText = STR_DISALLOW_ALL_VLANS;
			break;
		case 3:
			labelText = STR_ALLOW_SELECTED_VLANS;
			break;
		}
		
		button.setText(labelText);
	}
	public boolean compareConfigurationWithUI() {
		
		int		index;
		String	vlanName;
		String 	essId;
		
		if(btnRadios[ALLOW_SELECTED_VLANS].getSelection() == true) {
			index		= cmbVLANList.getSelectionIndex();
			vlanName	= cmbVLANList.getItem(index); 
			essId		= (String)cmbVLANList.getData(vlanName);
			if(ethernetInfo.getEssid().equalsIgnoreCase(essId) == false) {
				return false;
			}
		}
		/*
		if(btnRadios[ALLOW_ALL_VLANS].getSelection() == true) {
			if(ethernetInfo.getEssid().equalsIgnoreCase(VLAN_ESSID_ALLOW_VLANS) == false) {
				return false;
			}
		}*/
		
		if(btnRadios[DISALLOW_ALL_VLANS].getSelection() == true) {
			if(ethernetInfo.getEssid().equalsIgnoreCase(VLAN_ESSID_DISALLOW_VLANS) == false){
				return false;
			}
		}
		
		
		return true;
	}

	public void setFrmTitle(String frmTitle) {
		this.frmTitle = frmTitle;
		if(grp != null)
			grp.setText(frmTitle);
	}
	
}

