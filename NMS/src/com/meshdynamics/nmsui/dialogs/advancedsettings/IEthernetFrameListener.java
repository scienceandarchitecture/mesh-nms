/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : IEthernetFrameListener.java
 * Comments : callback events interface for Ethernet tab
 * Created  : Feb 21, 2007
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 21, 2007 | Created                                         | Imran   |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.dialogs.advancedsettings;

public interface IEthernetFrameListener {
	public static final int ALLOW_ALL_VLANS			= 0;
	public static final int DISALLOW_ALL_VLANS		= 1;
	public static final int ALLOW_SELECTED_VLAN		= 2;

	
	public void selectedConfiguration(EthernetFrameSelectionEvent efsEvent);
	public void selectedVlan(String vlanName);
	

}
