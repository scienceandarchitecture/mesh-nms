/**
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamConfigPage.java
 * Comments : 
 * Created  : Jul 08, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 4  | Jul 08, 2007   |  Created                       			  | Abhishek     |
 * ----------------------------------------------------------------------------------*/
 
package com.meshdynamics.nmsui.dialogs.advancedsettings;


import java.util.Enumeration;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IEffistreamAction;
import com.meshdynamics.meshviewer.configuration.IEffistreamConfiguration;
import com.meshdynamics.meshviewer.configuration.IEffistreamRule;
import com.meshdynamics.meshviewer.configuration.IEffistreamRuleCriteria;
import com.meshdynamics.meshviewer.configuration.impl.EffistreamAction;
import com.meshdynamics.meshviewer.configuration.impl.EffistreamRule;
import com.meshdynamics.meshviewer.imcppackets.helpers.EffistreamHelper;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.dialogs.advancedsettings.EffistreamActionDialog.EffistreamActionForUI;
import com.meshdynamics.nmsui.dialogs.advancedsettings.EffistreamRuleDialog.EffistreamRuleForUI;
import com.meshdynamics.nmsui.dialogs.base.ConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.util.IStateEventHandler;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;
import com.meshdynamics.util.Range;
import com.meshdynamics.util.StateManager;

public class EffistreamConfigPage extends ConfigPage implements IStateEventHandler{

	private Tree				treeControl;
	private TreeItem 			rulesRootItem; 			// Tree Contol Item
	
	private Button 				btnRuleAdd;
	private Button 				btnRuleDelete;
	private Button 				btnActionAdd;
	private Button 				btnActionDelete;
	private Group				grp;
	private StateManager 		sMananger;
	private IConfiguration		configuration;
	private IEffistreamRule		lastSavedRulesRoot;
	
	private final int STATE_START			= 0;
	private final int STATE_SELECTION		= 1;
	
	private final int MAX_STATE_CNT			= 2;
	
	public static String[] strCriteria = {
			
		"Ethernet Type",
		"Ethernet Destination",
		"Ethernet Source",
		"IP Type of Service",
		"IP Differential Services",
		"IP Source",
		"IP Destination",
		"IP Protocol",
		"UDP Source Port",
		"UDP Destination Port",
		"UDP Length",
		"TCP Source Port",
		"TCP Destination Port",
		"TCP Length",
		"RTP Version",
		"RTP Payload",
		"RTP Length"
	};
	
	public static final short	CONFIG_CHANGED		=	0;
	public static final short	CONFIG_NOT_CHANGED	=	1;
	public static final short	CONFIG_NULL			=	2;
	
	public EffistreamConfigPage(IMeshConfigBook parent) {
		super(parent);
		sMananger	   		    = new StateManager(this,MAX_STATE_CNT,STATE_START);
		minimumBounds.width 	= 425;
		minimumBounds.height	= 450;
		configuration			= parent.getConfiguration();
		createLastSavedRoot();
	}

	/**
	 * @param configuration2
	 * 
	 */
	private void createLastSavedRoot() {
		IEffistreamConfiguration	effistreamConfiguration = configuration.getEffistreamConfiguration();
		if(effistreamConfiguration == null) {
			return;
		}
		IEffistreamRule	parentRule	= (IEffistreamRule)effistreamConfiguration.getRulesRoot();
		if(parentRule == null) {
			return;
		}
		lastSavedRulesRoot			= (IEffistreamRule)parentRule.createRule((short)0);
		lastSavedRulesRoot.setRoot(true);
	}

	/**
	 * Function to create various controls for General Config Page
	 */
	public void createControls(CTabFolder tabFolder) {
	
		super.createControls(tabFolder);
		
		grp = new Group(canvas,SWT.NONE);
		grp.setBounds(10,10,400,425);
		
		TreeItem[] 	selected;
		int 		x;
		int 		y;
		
		x = 7;
		y = 20;
		
		treeControl = new Tree(grp, SWT.BORDER|SWT.SINGLE);
		treeControl.setBounds(x, y, 400, 330);
		treeControl.setLayoutData(new RowData(-1, 300));
		treeControl.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				notifyStateChanged(STATE_SELECTION);
			}
		});
		
		rulesRootItem = new TreeItem(treeControl, SWT.NONE);			
		rulesRootItem.setText("Rules");
		rulesRootItem.setExpanded(true);
		
		y = y + 350;
		x = x + 5;
		
		btnRuleAdd = new Button(grp, SWT.PUSH);		
		btnRuleAdd.setText("Add Rule");
		btnRuleAdd.setBounds(x, y, 75, 25);
		btnRuleAdd.setToolTipText("Add Rule");
		btnRuleAdd.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				addRule();
			}
		});

		x = x + 105;
		
		btnRuleDelete = new Button(grp, SWT.PUSH);		
		btnRuleDelete.setText("Delete Rule");
		btnRuleDelete.setBounds(x, y, 75, 25);
		btnRuleDelete.setToolTipText("Delete Rule");
		btnRuleDelete.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				deleteRule();
			}			
		});

		x = x + 105;
		
		btnActionAdd = new Button(grp, SWT.PUSH);		
		btnActionAdd.setText("Add Action");
		btnActionAdd.setBounds(x, y, 75, 25);
		btnActionAdd.setToolTipText("Add Action");
		btnActionAdd.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				addAction();
			}

		});
		
		x = x + 105;
		
		btnActionDelete = new Button(grp, SWT.PUSH);		
		btnActionDelete.setText("Delete Action");
		btnActionDelete.setBounds(x, y, 75, 25);
		btnActionDelete.setToolTipText("Delete Action");
		btnActionDelete.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent arg0) {
				deleteAction();
			}			
		});
		
		selected 	= new TreeItem[1];
		selected[0] = rulesRootItem;
		treeControl.setSelection(selected);
		ContextHelp.addContextHelpHandlerEx(this.canvas,contextHelpEnum.ADVCONFIGTABEFFISTREAM);
		init();
	}

	private TreeItem getSelectedTreeItem() {
		TreeItem[]	selectedTreeItems;		
		
		selectedTreeItems 	= treeControl.getSelection();
		
		if(selectedTreeItems.length <= 0)
			return null;
		
		return selectedTreeItems[0];

	}
	
	
	
	private void addRule() {
		
		Object					object;
		IEffistreamRule			parentRule;
		EffistreamRuleDialog	dlgRuleDialog;
		TreeItem				treeItem;
		TreeItem				parentTreeItem;
		
		treeItem		= null;

		parentTreeItem 	= getSelectedTreeItem();	
		object 			= parentTreeItem.getData();
		if(object == null) {
			return;
		}
		parentRule		= (IEffistreamRule)object;
		
		dlgRuleDialog 	= new EffistreamRuleDialog(parentRule);
		dlgRuleDialog.show();
		
		if(dlgRuleDialog.getStatus() == SWT.OK) {		
			
			EffistreamRuleDialog.EffistreamRuleForUI	newRule 
					= dlgRuleDialog.getValues();
			
			if(checkForRepeatedRuleInChildrenList(parentRule, newRule) == true) {
				return;
			}else if(chkForRepeatedRuleInParentList(parentRule, newRule) == true) {
				return;
			}
			IEffistreamRule	ruleToBeAdded	= (IEffistreamRule)parentRule.createRule((short)newRule.criteriaId);
			copyRule(newRule, ruleToBeAdded);
			parentRule.addRule(ruleToBeAdded);
			
			treeItem = new TreeItem(parentTreeItem, SWT.NONE);
			treeItem.setText(getCriteriaUIName(ruleToBeAdded.getCriteriaId()) + "=" + getValueUIFromId(ruleToBeAdded));
			treeItem.setData(ruleToBeAdded);
			
			parentTreeItem.setExpanded(true);			
		}					
		if(treeItem != null) {
			setSelectedTreeItem(treeItem);
			executeSelectionState();
		}
	}
	
	/**
	 * @param newRule
	 * @param ruleToBeAdded
	 */
	private void copyRule(EffistreamRuleForUI srcRule, IEffistreamRule dstRule) {
		IEffistreamRuleCriteria	dstValue	= (IEffistreamRuleCriteria)dstRule.getCriteriaValue();
		EffistreamHelper.copyValueFromId(dstValue, srcRule.value, (short) srcRule.criteriaId);
		
	}

	/**
	 * Function to check whether newRule i.e. rule to be added is already present 
	 * in same branch of tree
	 * @param 
	 */
	private boolean checkForRepeatedRuleInChildrenList(IEffistreamRule parentRule, 
			EffistreamRuleDialog.EffistreamRuleForUI newRule) {
		
		boolean 	found ;
		String      ruleIdentifier;
		
		found										= false;
		Enumeration<String>	enumChildrenIdentifiers = parentRule.getChildrenIdentifiers();
		
		while(enumChildrenIdentifiers.hasMoreElements()) {
			ruleIdentifier					= (String)enumChildrenIdentifiers.nextElement();
			IEffistreamRule	childElement	= (IEffistreamRule)parentRule.getChild(ruleIdentifier);
			if(childElement == null) {
				continue;
			}
			if(childElement.getCriteriaId() == newRule.criteriaId) {
				IEffistreamRuleCriteria	childValue = (IEffistreamRuleCriteria)childElement.getCriteriaValue();
				Object					newValue   = newRule.value; 
				
				if(compareDuplicateEffistreamRuleCriteria(childValue, newValue) == true){
					found	= true;
					this.parent.showMessage("Rule with criteria "+getCriteriaUIName(newRule.criteriaId)+ " is already present as child.", SWT.ICON_ERROR);
					break;
				}
			}
		}
		return found;
	}
	/**
	 * @param childValue
	 * @param newValue
	 * @return
	 */
	private boolean compareDuplicateEffistreamRuleCriteria(
			IEffistreamRuleCriteria srcValue, Object newValue) {
		
		int criteriaId	= srcValue.getCriteria();
		
		switch(criteriaId) {
		
			case EffistreamHelper.ID_TYPE_ETH_TYPE:	
			case EffistreamHelper.ID_TYPE_IP_TOS:
			case EffistreamHelper.ID_TYPE_IP_DIFFSRV:
			case EffistreamHelper.ID_TYPE_IP_PROTO:
			case EffistreamHelper.ID_TYPE_RTP_VERSION:
			case EffistreamHelper.ID_TYPE_RTP_PAYLOAD:
				if(srcValue.getLongValue().toString().equalsIgnoreCase(
						((Long)newValue).toString()) == false) {
					return false;
				}
				break;
				
			case EffistreamHelper.ID_TYPE_ETH_DST:
			case EffistreamHelper.ID_TYPE_ETH_SRC:
				if(srcValue.getMacAddressValue().toString().equalsIgnoreCase(
						((MacAddress)newValue).toString()) == false) {
					return false;
				}
				break;
				
			case EffistreamHelper.ID_TYPE_IP_SRC:
			case EffistreamHelper.ID_TYPE_IP_DST:
				if(srcValue.getIPAddressValue().toString().equalsIgnoreCase(
						((IpAddress)newValue).toString()) == false) {
					return false;
				}
				break;
				
			case EffistreamHelper.ID_TYPE_UDP_SRC_PORT:
			case EffistreamHelper.ID_TYPE_UDP_DST_PORT:
			case EffistreamHelper.ID_TYPE_UDP_LENGTH:
			case EffistreamHelper.ID_TYPE_TCP_SRC_PORT:
			case EffistreamHelper.ID_TYPE_TCP_DST_PORT:
			case EffistreamHelper.ID_TYPE_TCP_LENGTH:
			case EffistreamHelper.ID_TYPE_RTP_LENGTH:
				Range	newRange	= (Range)newValue;
				if(srcValue.getRangeValue().getLowerLimit() != newRange.getLowerLimit()	||
						srcValue.getRangeValue().getUpperLimit() != newRange.getUpperLimit()) {
					return false;
				}
				break;
			}
		return true;
	}

	/**
	 * Function to check whether newRule i.e. rule to be added is already present 
	 * as one of the parent
	 * @param 
	 */
	private boolean chkForRepeatedRuleInParentList(IEffistreamRule parentRule, 
			EffistreamRuleDialog.EffistreamRuleForUI newRule) {
	
		boolean	found	= false;
		
		if(parentRule.isRoot()!= true) {
			
			if(parentRule.getCriteriaId()	== newRule.criteriaId) {
				found	= true;
				this.parent.showMessage("Rule with criteria "+getCriteriaUIName(newRule.criteriaId)+ " is already present as one of its parent.", SWT.ICON_ERROR);
				return true;
			}
			IEffistreamRule	rule	= (IEffistreamRule)parentRule.getParent();
			if(rule == null) {
				return false;
			}
			found = chkForRepeatedRuleInParentList(rule, newRule);
			if(found == true) {
				return true;
			}
		}
		return false;
	}

	private void deleteTree(TreeItem treeItem) {
		TreeItem[] items;
		
		items = treeItem.getItems();
		for(int i = 0; i < items.length; i++) {			
			deleteTree(items[i]);
			items[i].dispose();
		}
	}
	
	private void deleteRule() {
		
		TreeItem		treeItem;
		
		treeItem	= getSelectedTreeItem();
		
		if(treeItem == null) {
			return;
		}
		
		if(treeItem == rulesRootItem) {
			parent.showMessage("Root rule cannot be delected.", SWT.OK);			
			return;
		}
		IEffistreamRule	rule 	  = (IEffistreamRule)treeItem.getData();
		
		IEffistreamRule parent	  = (IEffistreamRule)rule.getParent();
		String strIdentifier	  = EffistreamHelper.generateKey((IEffistreamRuleCriteria)rule.getCriteriaValue()); 
		parent.deleteRule(strIdentifier);
		
		deleteTree(treeItem);
		treeItem.dispose();		
	}
	
	private void addAction() {
		
		EffistreamActionDialog 	dlgAction;
		TreeItem				treeItem;
		String 					strAction;
		TreeItem				parentTreeItem;
		
		strAction			= "";

		parentTreeItem		= getSelectedTreeItem();
		
		if(parentTreeItem == null) {
			return;
		}
		
		if(parentTreeItem == rulesRootItem || (parentTreeItem.getItemCount() > 0)) {
			parent.showMessage("Invalid Rule Selection.", SWT.OK);			
			return;
		}
		IVersionInfo	versionInfo	= parent.getVersionInfo();
		dlgAction = new EffistreamActionDialog(versionInfo,configuration.getInterfaceConfiguration());
		dlgAction.show();
		
		if(dlgAction.getStatus() == SWT.OK) {
			
			EffistreamActionDialog.EffistreamActionForUI	uiAction	
											= dlgAction.getValues();		
			IEffistreamRule		rule	= (EffistreamRule)parentTreeItem.getData();
			
			IEffistreamAction newAction	= (IEffistreamAction)rule.addAction(IEffistreamAction.ACTION_TYPE_DEFAULT);
			copyAction(uiAction, newAction);
			
			treeItem 	= new TreeItem(parentTreeItem, SWT.NONE);
			strAction	+= getActionString(newAction);
			treeItem.setText(strAction);
			
			parentTreeItem.setExpanded(true);
			setSelectedTreeItem(treeItem);
			executeSelectionState();
		}
	}	
	
	/**
	 * @param uiAction
	 * @param newAction
	 */
	private void copyAction(EffistreamActionForUI uiAction, IEffistreamAction newAction) {
		newAction.setDropPacket(uiAction.dropPacket);
		newAction.setNoAck(uiAction.noAck);
		newAction.setDot11eCategory(uiAction.dot11eCategory);
		newAction.setBitRate(uiAction.bitRate);
		newAction.setQueuedRetry(uiAction.queuedRetry);
		
	}

	private void deleteAction() {
		
		TreeItem			parentTreeItem;
		TreeItem			selectedTreeItem;
		Object 				object;
	
		selectedTreeItem 	= getSelectedTreeItem();
		
		object	= selectedTreeItem.getData();
		if(object != null) {
			parent.showMessage("Selected item is not an action item.", SWT.ICON_ERROR);
			return;
		}
				
		parentTreeItem			= getParentTreeItem();
		IEffistreamRule	parent	= (IEffistreamRule) parentTreeItem.getData();
		
		if(parent == null) {
			return;
		}
		if(parent.getAction() == null) { 
			return;			
		}
		parent.deleteAction();
		selectedTreeItem.dispose();
		
	}
	
	private TreeItem getParentTreeItem() {
		TreeItem[]	selectedTreeItems;		
		selectedTreeItems 	= treeControl.getSelection();
		if(selectedTreeItems.length <= 0)
			return null;
		return selectedTreeItems[0].getParentItem();
	}

	private void setSelectedTreeItem(TreeItem selectedTreeItem) {
		TreeItem[]	selectedTreeItems;
		selectedTreeItems = new TreeItem[1];
		selectedTreeItems[0] = selectedTreeItem;
		treeControl.setSelection(selectedTreeItems);
	}
	
	public void selectionChanged(boolean selected) {
		
		if(selected	== true) {
			initalizeLocalData();
			canvas.setFocus();
		}else {
			if(validateLocalData() == false) {
				stayOnSameTab();
				return;
			}
			if(compareUIwithLocalConfig()	== false){
				
				int result	= parent.showMessage("Do you want to save the changes ? ",SWT.YES|SWT.CANCEL|SWT.NO);
				if(result == SWT.CANCEL) {
					stayOnSameTab();
					return;
				}else if(result	== SWT.NO) {
					
					IEffistreamConfiguration	effistreamConfiguration	= configuration.getEffistreamConfiguration();
					IEffistreamRule 			configRuleRoot			= (IEffistreamRule)effistreamConfiguration.getRulesRoot();
					configRuleRoot.clearData();
					lastSavedRulesRoot.copyTo(configRuleRoot);
					
				}else if(result	== SWT.YES) {
					getDataFromUIToConfiguration();
				}	
			}
		}
	}

	/**
	 * @return
	 */
	private boolean compareUIwithLocalConfig() {
			
		boolean		comparison;
		String  ruleIdentifier; 
		
		comparison = true;
		
		IEffistreamRule	rulesRoot		= (IEffistreamRule) rulesRootItem.getData();
			
		if(rulesRoot.getChildCount() != lastSavedRulesRoot.getChildCount()) {
			return false;
		}
		
		Enumeration<String>	enumChildrenIdentifiers	= rulesRoot.getChildrenIdentifiers();
		
		while(enumChildrenIdentifiers.hasMoreElements()) {
			ruleIdentifier				= (String)enumChildrenIdentifiers.nextElement();
			
			IEffistreamRule	srcEffiRule	= (IEffistreamRule)rulesRoot.getChild(ruleIdentifier);
			if(srcEffiRule == null) {
				continue;
			}
			
			IEffistreamRule	dstEffiRule	= (IEffistreamRule)lastSavedRulesRoot.getChild(ruleIdentifier);
			if(dstEffiRule == null) {
				return false;
			}
			comparison	= compareRule(srcEffiRule, dstEffiRule);
			if(comparison == false) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param srcEffiRule
	 * @param dstEffiRule
	 * @return
	 */
	private boolean compareRule(IEffistreamRule srcEffiRule, IEffistreamRule dstEffiRule) {
	
		boolean		comparison;
		String  	ruleIdentifier;
				
		
		if(srcEffiRule == null || dstEffiRule == null) {
			return false;
		}
				
		if(srcEffiRule.getChildCount() != dstEffiRule.getChildCount()) {
			return false;
		}
		if(srcEffiRule.getCriteriaId() != dstEffiRule.getCriteriaId()) {
			return false;
		}
		IEffistreamRuleCriteria	srcCriteriaValue	= (IEffistreamRuleCriteria)srcEffiRule.getCriteriaValue();
		IEffistreamRuleCriteria	dstCriteriaValue	= (IEffistreamRuleCriteria)dstEffiRule.getCriteriaValue();
		
		if(compareEffistreamRuleCriteria(srcCriteriaValue, dstCriteriaValue) == false) {
			return false;
		}
		
		Enumeration<String>	enumSrcChildrenIdentifiers	= srcEffiRule.getChildrenIdentifiers();
			
		while(enumSrcChildrenIdentifiers.hasMoreElements()) {
			ruleIdentifier						= (String)enumSrcChildrenIdentifiers.nextElement();
			IEffistreamRule	srcEffiRuleChild	= (IEffistreamRule)srcEffiRule.getChild(ruleIdentifier);
			if(srcEffiRuleChild == null) {
				continue;
			}
			IEffistreamRule	dstEffiRuleChild	= (IEffistreamRule)dstEffiRule.getChild(ruleIdentifier);
			if(dstEffiRuleChild == null ) {
				return false; 
			}
			if(srcEffiRule.getChildCount() > 0) {
				comparison	= compareRule(srcEffiRuleChild, dstEffiRuleChild);
				if(comparison == false) {
					return false;
				}
			}
		}
			
		if(srcEffiRule.getChildCount() == 0){
			
			IEffistreamAction	srcAction	= (IEffistreamAction)srcEffiRule.getAction();
			IEffistreamAction	dstAction	= (IEffistreamAction)dstEffiRule.getAction();
			if(srcAction == null || dstAction == null) {
				return false;
			}
			comparison	= compareAction(srcAction, dstAction);
			if(comparison == false) {
				return false;
			}
		}
		
		return true;
	}

	/**
	 * @param srcCriteriaValue
	 * @param dstCriteriaValue
	 * @return
	 */
	private boolean compareEffistreamRuleCriteria(IEffistreamRuleCriteria srcValue, IEffistreamRuleCriteria dstValue) {

		int criteriaId	= srcValue.getCriteria();
		
		switch(criteriaId) {
		
			case EffistreamHelper.ID_TYPE_ETH_TYPE:	
			case EffistreamHelper.ID_TYPE_IP_TOS:
			case EffistreamHelper.ID_TYPE_IP_DIFFSRV:
			case EffistreamHelper.ID_TYPE_IP_PROTO:
			case EffistreamHelper.ID_TYPE_RTP_VERSION:
			case EffistreamHelper.ID_TYPE_RTP_PAYLOAD:
				if(srcValue.getLongValue().toString().equalsIgnoreCase
						(dstValue.getLongValue().toString()) == false) {
					return false;
				}
				break;
				
			case EffistreamHelper.ID_TYPE_ETH_DST:
			case EffistreamHelper.ID_TYPE_ETH_SRC:
				if(srcValue.getMacAddressValue().toString().equalsIgnoreCase(dstValue.getMacAddressValue().toString()) == false) {
					return false;
				}
				break;
				
			case EffistreamHelper.ID_TYPE_IP_SRC:
			case EffistreamHelper.ID_TYPE_IP_DST:
				if(srcValue.getIPAddressValue().toString().equalsIgnoreCase(dstValue.getIPAddressValue().toString()) == false) {
					return false;
				}
				break;
				
			case EffistreamHelper.ID_TYPE_UDP_SRC_PORT:
			case EffistreamHelper.ID_TYPE_UDP_DST_PORT:
			case EffistreamHelper.ID_TYPE_UDP_LENGTH:
			case EffistreamHelper.ID_TYPE_TCP_SRC_PORT:
			case EffistreamHelper.ID_TYPE_TCP_DST_PORT:
			case EffistreamHelper.ID_TYPE_TCP_LENGTH:
			case EffistreamHelper.ID_TYPE_RTP_LENGTH:
				Range	dstRange	= dstValue.getRangeValue();
				if(srcValue.getRangeValue().getLowerLimit() != dstRange.getLowerLimit()	||
						srcValue.getRangeValue().getUpperLimit() != dstRange.getUpperLimit()) {
					return false;
				}
				break;
			}
		return true;
	}

	private boolean compareAction(IEffistreamAction srcAction, IEffistreamAction dstAction) {
		
		if(srcAction.getDot11eCategory()   != dstAction.getDot11eCategory()) {
			return false;
		}else if(srcAction.getDropPacket() != dstAction.getDropPacket()) {
			return false;
		}else if(srcAction.getNoAck()	  != dstAction.getNoAck()) {
			return false;
		}else if(srcAction.getBitRate()	  != dstAction.getBitRate()) {
			return false;
		}else if(srcAction.getQueuedRetry()  != dstAction.getQueuedRetry()) {
			return false;
		}
		
		return true;
	}

	public void setModified() {
	}

	public void initalizeLocalData() {
		
		IEffistreamRule 		parentRule;
		
		IEffistreamConfiguration	effistreamConfiguration	= configuration.getEffistreamConfiguration();
		if(effistreamConfiguration == null) {
			return;
		}
		
		parentRule	= (IEffistreamRule)effistreamConfiguration.getRulesRoot();
		rulesRootItem.setData(parentRule);
		
		//Updating last saved copy
		lastSavedRulesRoot.clearData();
		parentRule.copyTo(lastSavedRulesRoot);
		
		setTreeValues(parentRule,rulesRootItem);
		 
	}
	
	private void setTreeValues(IEffistreamRule parentRule, TreeItem parentTreeItem) {
		
		TreeItem		treeItem;
		TreeItem		actionTreeItem;
		String			strAction;
		String			ruleIdentifier;		
		if(parentRule == null) {
			return;
		}
		Item	[] items	= parentTreeItem.getItems();
		for(int i = 0; i < items.length; i++) {
			items[i].dispose();
		}
	
		Enumeration<String>	enumChildrenIdentifiers	= parentRule.getChildrenIdentifiers();
		
		while(enumChildrenIdentifiers.hasMoreElements()) {
			ruleIdentifier	= (String)enumChildrenIdentifiers.nextElement();
			IEffistreamRule	effiRule	= (IEffistreamRule)parentRule.getChild(ruleIdentifier);
			if(effiRule == null) {
				continue;
			}
			
			treeItem 					= new TreeItem(parentTreeItem, SWT.NONE);	
			treeItem.setText(getCriteriaUIName(effiRule.getCriteriaId()) + "=" + getValueUIFromId(effiRule));
			treeItem.setData(effiRule);
			
			if(effiRule.getChildCount() > 0) {
				setTreeValues(effiRule, treeItem);
			}else {
			
				IEffistreamAction	action	= (IEffistreamAction)effiRule.getAction(); 
				if(action != null) {
					actionTreeItem 	= new TreeItem(treeItem, SWT.NONE);
					strAction	= "";
					
					strAction	+= getActionString(action);	
					actionTreeItem.setText(strAction);	
					actionTreeItem.setExpanded(true);
				}	
		    }
			parentTreeItem.setExpanded(true);
			treeItem.setExpanded(true);
		}		
	}
	
	private String getActionString(IEffistreamAction action) {
		
		String strAction = "";
		if(action.getDropPacket() == 1)
			strAction	+= "Drop All Packets. ";
		
		if(action.getNoAck() == 1)
			strAction	+= "No ACK is set for Packets. ";
		
		if(action.getQueuedRetry() == 1)
			strAction	+= "Queued Retry is set for Packets. ";
		
		short dot11Category	= action.getDot11eCategory(); 
		switch(dot11Category) {
		
		case EffistreamAction.AC_BK:
			strAction	+= "802.11e Category is Background. ";
			break;
		
		case EffistreamAction.AC_BE:
			strAction	+= "802.11e Category is Best Effort. ";
			break;
		
		case EffistreamAction.AC_VO:
			strAction	+= "802.11e Category is Voice. ";
			break;
			
		case EffistreamAction.AC_VI:
			strAction	+= "802.11e Category is Video. ";
			break;
		}
		short rate	= action.getBitRate();
		if(rate > 0) {
			strAction	+= "Transmit Rate is set to "+rate+" Mbps. ";
		}
		if(strAction.equals("") ==  true) {
			if(dot11Category == EffistreamAction.AC_NN) {
				strAction	= "802.11e Category is None.";
			}
		}
		return strAction;
	}
	
	private String getCriteriaUIName(int criteriaId) {	
		
		return strCriteria[criteriaId-1];
	}
	
	public void setGrpBounds(Rectangle minimumGrpBounds) {
		grp.setBounds(10,10,minimumGrpBounds.width-10,minimumGrpBounds.height-25);
		treeControl.setBounds(10,20,minimumGrpBounds.width-30,325);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.IStateEventHandler#stateFunction(int)
	 */
	public int stateFunction(int state) {
		
		switch(state){
		
			case STATE_START		: executeStartState();
									  break;
			case STATE_SELECTION	: executeSelectionState();
									  break;
		}
		return state;
	}

	/**
	 * 
	 */
	private void executeSelectionState() {
		
		TreeItem		selectedTreeItem;
		IEffistreamRule rule;
		
		selectedTreeItem = getSelectedTreeItem();
		
		if(selectedTreeItem == null)
			return;
		
		rule	= (IEffistreamRule)selectedTreeItem.getData();
		
		if(selectedTreeItem == rulesRootItem) {
				/**
				 * Root
				 */
				btnRuleAdd.setEnabled(true);
				btnRuleDelete.setEnabled(false);
				btnActionAdd.setEnabled(false);
				btnActionDelete.setEnabled(false);				
		}else {
			/**
			 * action
			 */
			if(rule == null){
				
				btnRuleAdd.setEnabled(false);
				btnRuleDelete.setEnabled(false);
				btnActionAdd.setEnabled(false);
				btnActionDelete.setEnabled(true);
				
			}else if(rule.getChildCount() > 0) {
				
				/**
				 * rule with rule
				 */
				btnRuleAdd.setEnabled(true);
				btnRuleDelete.setEnabled(true);
				btnActionAdd.setEnabled(false);
				btnActionDelete.setEnabled(false);
				
			}else if(rule.getAction() == null) {
				/**
				 * Rule without action
				 */
				btnRuleAdd.setEnabled(true);
				btnRuleDelete.setEnabled(true);	
				btnActionAdd.setEnabled(true);
				btnActionDelete.setEnabled(false);
				
			}else {
				/**
				 * Rule with action
				 */
				btnRuleAdd.setEnabled(false);
				btnRuleDelete.setEnabled(true);	
				btnActionAdd.setEnabled(false);
				btnActionDelete.setEnabled(false);
				
			}
		}
	}

	private void executeStartState() {
		
		btnRuleAdd.setEnabled(true);
		btnRuleDelete.setEnabled(false);
		btnActionAdd.setEnabled(false);
		btnActionDelete.setEnabled(false);
	}

	public void init() {
		sMananger.triggerState(STATE_START); 
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.IStateEventHandler#notifyStateChanged(int)
	 */
	public void notifyStateChanged(int state) {
		sMananger.triggerState(state);  
	}
	public boolean validateLocalData() {
	
		IEffistreamConfiguration effistreamConfiguration	= configuration.getEffistreamConfiguration();
		if(effistreamConfiguration == null) {
			return false;
		}
		IEffistreamRule rulesRoot	= (IEffistreamRule)effistreamConfiguration.getRulesRoot();
		if(validateTreeValues(rulesRoot) == false) {
			parent.showMessage("Incomplete Effistream Rule found. Please check.", SWT.ICON_ERROR);
			return false;
		}
		return true;
	}
	
	
	private boolean validateTreeValues(IEffistreamRule tree) {
		String ruleIdentifier;
		Enumeration<String> enumTreeElements	= tree.getChildrenIdentifiers();
		
		while(enumTreeElements.hasMoreElements()) {
			ruleIdentifier	= (String) enumTreeElements.nextElement();
			IEffistreamRule	treeElement	= (IEffistreamRule)tree.getChild(ruleIdentifier);
			if(treeElement == null) {
				continue;
			}
			if(treeElement.getChildCount() > 0) {
				if(validateTreeValues(treeElement) == false)
					return false;
			} else {
				if(treeElement.getAction() == null) {
					return false;
				}
			}
		}
		return true;
	}
	
	public String getValueUIFromId(IEffistreamRule rule) {
		
		short	criteriaId	= rule.getCriteriaId();
		IEffistreamRuleCriteria	value		= (IEffistreamRuleCriteria)rule.getCriteriaValue();
		if(value == null) {
			return "";
		}
		switch(criteriaId) {
		
		case EffistreamHelper.ID_TYPE_ETH_TYPE:
		case EffistreamHelper.ID_TYPE_IP_PROTO:
		case EffistreamHelper.ID_TYPE_IP_TOS:
		case EffistreamHelper.ID_TYPE_IP_DIFFSRV:
		case EffistreamHelper.ID_TYPE_RTP_VERSION:
		case EffistreamHelper.ID_TYPE_RTP_PAYLOAD:
			return value.getLongValue().toString();
			
		case EffistreamHelper.ID_TYPE_ETH_DST:
		case EffistreamHelper.ID_TYPE_ETH_SRC:
			return value.getMacAddressValue().toString();
			
		case EffistreamHelper.ID_TYPE_IP_SRC:
		case EffistreamHelper.ID_TYPE_IP_DST:
			return value.getIPAddressValue().toString();
			
		case EffistreamHelper.ID_TYPE_UDP_SRC_PORT:
		case EffistreamHelper.ID_TYPE_UDP_DST_PORT:
		case EffistreamHelper.ID_TYPE_UDP_LENGTH:
		case EffistreamHelper.ID_TYPE_TCP_SRC_PORT:
		case EffistreamHelper.ID_TYPE_TCP_DST_PORT:
		case EffistreamHelper.ID_TYPE_TCP_LENGTH:
		case EffistreamHelper.ID_TYPE_RTP_LENGTH:
			
			Range range = value.getRangeValue();
			return range.getLowerLimit() + ":" + range.getUpperLimit();
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.ConfigPage#getDataFromUIToConfiguration()
	 */
	public boolean getDataFromUIToConfiguration() {
		
		
		IEffistreamRule rulesRoot		= (IEffistreamRule)rulesRootItem.getData();
	
		lastSavedRulesRoot.clearData();
		
		rulesRoot.copyTo(lastSavedRulesRoot);
		
		return true;
	}
}
