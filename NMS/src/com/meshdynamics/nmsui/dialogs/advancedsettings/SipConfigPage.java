/*
 * Created on Mar 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.dialogs.advancedsettings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipStaInfo;
import com.meshdynamics.nmsui.dialogs.UIConstants;
import com.meshdynamics.nmsui.dialogs.base.ConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.util.IStateEventHandler;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;
import com.meshdynamics.util.StateManager;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SipConfigPage extends ConfigPage implements IStateEventHandler {
	
	private Button				btnEnabled;
	private Button				btnEnableInStarfishMode; 	
	private Group				grp;
	private Text				txtServerIpAddress;
	private Text				txtPort;
	private ToolBar				toolBar;
	private ToolItem 			btnNew,btnEdit,btnSave,btnDelete;
	private Table   			tblStaEntries;
	
	private StateManager		stateManager;
	private	ISipConfiguration	iSipConfig;
	
	private int 				rel_x;
	private int 				rel_y;

	private final int			STATE_START		= 0;
	private final int 			STATE_NEW  		= 1;
	private final int 			STATE_EDIT 		= 2;
	private final int 			STATE_SAVE 		= 3;
	private final int 			STATE_DELETE	= 4;
	private final int 			STATE_SELECT	= 5; 

	private final int 			MAX_STATE_CNT	= 6;
	
	/**
	 * @param parent
	 */
	public SipConfigPage(IMeshConfigBook parent) {
		super(parent);
		minimumBounds.width  	= 425;
		minimumBounds.height 	= 450;
		stateManager			= new StateManager(this, MAX_STATE_CNT, STATE_START);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#createControls(org.eclipse.swt.custom.CTabFolder)
	 */
	public void createControls(CTabFolder parent) {
		super.createControls(parent);

		btnEnabled	= new Button(canvas, SWT.CHECK);
		btnEnabled.setText("Enable SIP server functionality.");
		btnEnabled.setBounds(10, 10, 175, 20);
		btnEnabled.addSelectionListener(new SelectionListener(){

			public void widgetSelected(SelectionEvent arg0) {
				boolean enabled = btnEnabled.getSelection(); 
				if(toggleConfigUI(enabled) == false) {
					arg0.doit = false;
					btnEnabled.setSelection(!enabled);
				}
			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				
			}});

		grp = new Group(canvas,SWT.NONE);
		grp.setBounds(10,30,415,395);

		rel_x	= 20;
		rel_y	= 20;
		
		createGeneralControls();
		createStaTableControls();
		
		ContextHelp.addContextHelpHandlerEx(this.canvas,contextHelpEnum.ADVCONFIGTABPBV);
	}

	/**
	 * 
	 */
	private void createStaTableControls() {
		rel_y 	+= 25;
		rel_x	= 15;
		
		toolBar = new ToolBar(grp,SWT.FLAT);
		toolBar.setBounds(rel_x,rel_y,100,25);		
        
		btnNew	= new ToolItem(toolBar,SWT.IMAGE_BMP);
		
		btnNew.setImage(UIConstants.imgNew); 
		btnNew.setToolTipText("New");
		btnNew.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				stateManager.triggerState(STATE_NEW);
			}
		});
		
		btnEdit	= new ToolItem(toolBar,SWT.IMAGE_BMP);
		btnEdit.setImage(UIConstants.imgEdit); 
		btnEdit.setToolTipText("Edit");
		btnEdit.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				stateManager.triggerState(STATE_EDIT);
			}
		});		
				
		btnSave	= new ToolItem(toolBar,SWT.IMAGE_BMP);
		btnSave.setImage(UIConstants.imgSave); 
		btnSave.setToolTipText("Save");
		btnSave.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				stateManager.triggerState(STATE_SAVE);
			}
		});			
	
		btnDelete	= new ToolItem(toolBar,SWT.IMAGE_BMP);
		btnDelete.setToolTipText("Delete");
		btnDelete.setImage(UIConstants.imgDel); 
		btnDelete.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				stateManager.triggerState(STATE_DELETE);
			}
		});		
		
		rel_y = rel_y + 25;
			
		tblStaEntries = new Table (grp,SWT.BORDER|SWT.SINGLE|SWT.FULL_SELECTION);
		tblStaEntries.setHeaderVisible(true);
		tblStaEntries.setLinesVisible(true);
		tblStaEntries.setBounds(rel_x,rel_y,390,250);
		tblStaEntries.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				if(btnEnabled.getSelection() == false)
					return;
				
				if(stateManager.getState() == STATE_START)
					stateManager.triggerState(STATE_SELECT);
			}

		});
		
		TableColumn col = new TableColumn(tblStaEntries, SWT.NONE);
		col.setText("Extension");
		col.setWidth(50);	
		col = new TableColumn(tblStaEntries, SWT.NONE);
		col.setText("MacAddress");
		col.setWidth(150);	
		
		rel_y	+= 285;
	}

	/**
	 * 
	 */
	private void createGeneralControls() {

		btnEnableInStarfishMode	= new Button(grp, SWT.CHECK);
		btnEnableInStarfishMode.setText("Enable only in P3M(tm) mode.");
		btnEnableInStarfishMode.setBounds(rel_x, rel_y, 175, 20);
		btnEnableInStarfishMode.addSelectionListener(new SelectionListener(){

			public void widgetSelected(SelectionEvent arg0) {

			}

			public void widgetDefaultSelected(SelectionEvent arg0) {
				
			}
		});
		
		rel_y += 30;
		
		Label lbl = new Label(grp,SWT.NO);
		lbl.setBounds(rel_x,rel_y,100,17);
		lbl.setText("Server IpAddress");
		
		txtServerIpAddress	= new Text(grp,SWT.BORDER);
		txtServerIpAddress.setBounds(rel_x+160,rel_y,200,20);
						
		rel_y += 30;

		lbl = new Label(grp,SWT.NO);
		lbl.setBounds(rel_x,rel_y,100,17);
		lbl.setText("Server Port");
		
		txtPort	= new Text(grp,SWT.BORDER);
		txtPort.setBounds(rel_x+160,rel_y,200,20);
		
	}
	
	public void setGrpBounds(Rectangle minimumGrpBounds) {
		Rectangle 	grpBounds 	= grp.getBounds();
		int 		heightDiff	= grpBounds.height;
		grp.setBounds(10,30,minimumGrpBounds.width-10,minimumGrpBounds.height-45);
		grpBounds 	= grp.getBounds();
		heightDiff 	= grpBounds.height-heightDiff;
		
		Rectangle tblBounds = tblStaEntries.getBounds();
		tblBounds.height	+= heightDiff;
		tblStaEntries.setBounds(tblBounds);
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#initalizeLocalData()
	 */
	public void initalizeLocalData() {
		super.initalizeLocalData();
		IConfiguration	config = parent.getConfiguration();
		if(config == null)
			return;
		iSipConfig = config.getSipConfiguration();
		if(iSipConfig == null)
			return;

		btnEnableInStarfishMode.setSelection((iSipConfig.getEnabledInP3M() == 0) ? false : true);
		txtServerIpAddress.setText(iSipConfig.getServerIpAddress().toString());
		txtPort.setText(""+iSipConfig.getPort());
		
		tblStaEntries.removeAll();
		for(int i=0;i<iSipConfig.getStaInfoCount();i++) {
			ISipStaInfo staConfig 	= iSipConfig.getStaInfoByIndex(i);
			addTableEntry(""+staConfig.getExtension(), staConfig.getMacAddress().toString());
		}
		
		if(iSipConfig.getEnabled() == 0) {
			btnEnabled.setSelection(false);
			toggleConfigUI(false);
		} else {
			btnEnabled.setSelection(true);
			toggleConfigUI(true);
		}
		
	}

	private TableItem addTableEntry(String extn, String macAddress) {

		TableItem itm = new TableItem(tblStaEntries, SWT.NONE);
		itm.setText(0, extn);
		itm.setText(1, macAddress);
		return itm;
	}
	
	/**
	 * @param b
	 */
	private boolean toggleConfigUI(boolean enable) {
		if(enable == true)
			stateManager.init();
		else {
			if(stateManager.getState() == STATE_SAVE) {
				MessageBox msgBox = new MessageBox(new Shell(), SWT.OK|SWT.ICON_INFORMATION);
				msgBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
				msgBox.setMessage("Please save STA entry transaction.");
				msgBox.open();
				return false;
			}

			btnDelete.setEnabled(enable);
			btnEdit.setEnabled(enable);
			btnNew.setEnabled(enable);
			btnSave.setEnabled(enable);
		}

		btnEnableInStarfishMode.setEnabled(enable);
		txtPort.setEnabled(enable);
		txtServerIpAddress.setEnabled(enable);
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#selectionChanged(boolean)
	 */
	public void selectionChanged(boolean selected) {
		
		super.selectionChanged(selected);
		if(selected == true) {
			initalizeLocalData();
			canvas.setFocus();
			return;
		}
		
		if(validateLocalData() == false) {
			stayOnSameTab();
			return;
		}
		
		boolean askForSaveChanges = false;
		if(stateManager.getState() == STATE_SAVE) {
			askForSaveChanges = true;
		} else if(txtPort.getText().equalsIgnoreCase(""+iSipConfig.getPort()) == false) {
			askForSaveChanges = true;
		} else if(txtServerIpAddress.getText().equalsIgnoreCase(iSipConfig.getServerIpAddress().toString()) == false) {
			askForSaveChanges = true;
		} 
		
		if(askForSaveChanges == true) {
			MessageBox msgBox = new MessageBox(new Shell(), SWT.ICON_QUESTION|SWT.YES|SWT.NO|SWT.CANCEL);
			msgBox.setMessage("Do you want to save changes?");
			int ret = msgBox.open();
			if(ret == SWT.CANCEL) {
				super.stayOnSameTab();
				return;
			}
			
			if(ret == SWT.NO) {
				initalizeLocalData();
				return;
			}

		}
		
		getDataFromUIToConfiguration();
		return;
		
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#validateLocalData()
	 */
	public boolean validateLocalData() {
		
		if(btnEnabled.getSelection() == false)
			return true;
		
		boolean showErrorMessage = false;
		try {
			if(Integer.parseInt(txtPort.getText().trim()) < 0)
				showErrorMessage = true;
		} catch(Exception e) {
			showErrorMessage = true;
		}
		
		if(showErrorMessage == true) {
			MessageBox msgBox = new MessageBox(new Shell(), SWT.ICON_INFORMATION|SWT.OK);
			msgBox.setMessage("Enter port number between 1 to 65535.");
			msgBox.open();
			return false;
		}
		
		showErrorMessage = false;
		try {
			if(IpAddress.tempAddress.setBytes(txtServerIpAddress.getText().trim()) != 0)
				showErrorMessage = true;
		} catch(Exception e) {
			showErrorMessage = true;
		}
		if(showErrorMessage == true) {
			MessageBox msgBox = new MessageBox(new Shell(), SWT.ICON_INFORMATION|SWT.OK);
			msgBox.setMessage("Enter ip address in the format xxx.xxx.xxx.xxx");
			msgBox.open();
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.IStateEventHandler#stateFunction(int)
	 */
	public int stateFunction(int state) {
		int nextState = state;
		
		switch(state) {
			case STATE_START:
				nextState = handleStateStart();
				break;
			case STATE_NEW:
				nextState = handleStateNew();
				break;
			case STATE_EDIT:
				nextState = handleStateEdit();
				break;
			case STATE_DELETE:
				nextState = handleStateDelete();
				break;
			case STATE_SAVE:
				nextState = handleStateSave();
				break;
			case STATE_SELECT:
				nextState = handleStateSelect();
				break;
		}
		
		return nextState;
	}

	/**
	 * 
	 */
	private int handleStateSelect() {
		btnNew.setEnabled(true);
		btnEdit.setEnabled(true);
		btnSave.setEnabled(false);
		btnDelete.setEnabled(true);
		return STATE_SELECT;
	}

	/**
	 * 
	 */
	private int handleStateSave() {
		stateManager.triggerState(STATE_START);
		return STATE_START;		
	}

	/**
	 * 
	 */
	private int handleStateDelete() {
		
		int selectionIndex = tblStaEntries.getSelectionIndex();
		if(selectionIndex < 0)
			return gotoStateSelect();
		
		TableItem itm = tblStaEntries.getItem(selectionIndex);
		if(itm == null)
			return STATE_START;
		
		tblStaEntries.remove(tblStaEntries.getSelectionIndex());
		return gotoStateSave();
	}

	/**
	 * 
	 */
	private int handleStateEdit() {
		
		int selectionIndex = tblStaEntries.getSelectionIndex();
		
		if(selectionIndex < 0)
			return gotoStateSelect();
		
		int 	itemCount	= tblStaEntries.getItemCount();
		int 	existExt[] 	= new int[itemCount];
		String 	existMac[]	= new String[itemCount];
		TableItem itm 		= null;
		
		for(int i=0;i<itemCount;i++) {
			itm 			= tblStaEntries.getItem(i);
			existExt[i] 	= Integer.parseInt(itm.getText(0));
			existMac[i]		= itm.getText(1);
		}
		
		itm = tblStaEntries.getItem(selectionIndex);
		SipEntryDialog editEntry = new SipEntryDialog();
		editEntry.setDialogMode(SipEntryDialog.DIALOG_MODE_EDIT_ENTRY);
		editEntry.setEditExtension(itm.getText(0));
		editEntry.setEditMacAddress(itm.getText(1));
		editEntry.setExistingExtensionsList(existExt);
		editEntry.setExistingMacList(existMac);
		editEntry.show();
		if(editEntry.getStatus() != SWT.OK)
			return gotoStateSelect();
		
		itm.setText(0, editEntry.getUpdatedExtension());
		itm.setText(1, editEntry.getUpdatedMacAddress());

		return gotoStateSave();
	}

	private int gotoStateSelect() {
		btnNew.setEnabled(true);
		btnEdit.setEnabled(true);
		btnSave.setEnabled(false);
		btnDelete.setEnabled(true);
		return STATE_SELECT;
	}
	
	private int gotoStateSave() {
		btnNew.setEnabled(false);
		btnEdit.setEnabled(false);
		btnSave.setEnabled(true);
		btnDelete.setEnabled(false);
		return STATE_SAVE;
	}
	
	/**
	 * 
	 */
	private int handleStateNew() {
		
		int 	itemCount	= tblStaEntries.getItemCount();
		int 	existExt[] 	= new int[itemCount];
		String 	existMac[]	= new String[itemCount];
		
		for(int i=0;i<itemCount;i++) {
			TableItem itm 	= tblStaEntries.getItem(i);
			existExt[i] 	= Integer.parseInt(itm.getText(0));
			existMac[i]		= itm.getText(1);
		}
		
		SipEntryDialog newEntry = new SipEntryDialog();
		newEntry.setDialogMode(SipEntryDialog.DIALOG_MODE_NEW_ENTRY);
		newEntry.setExistingExtensionsList(existExt);
		newEntry.setExistingMacList(existMac);
		newEntry.show();
		if(newEntry.getStatus() != SWT.OK)
			return STATE_START;

		addTableEntry(newEntry.getUpdatedExtension(), newEntry.getUpdatedMacAddress());
		return gotoStateSave();
		
	}

	/**
	 * 
	 */
	private int handleStateStart() {
		btnNew.setEnabled(true);
		btnEdit.setEnabled(false);
		btnSave.setEnabled(false);
		btnDelete.setEnabled(false);
		tblStaEntries.setSelection(-1);
		return STATE_START;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.IStateEventHandler#init()
	 */
	public void init() {
		stateManager.triggerState(STATE_START);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.IStateEventHandler#notifyStateChanged(int)
	 */
	public void notifyStateChanged(int state) {
		stateManager.triggerState(state);
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#getDataFromUIToConfiguration()
	 */
	public boolean getDataFromUIToConfiguration() {
		if(btnEnabled.getSelection() == true)
			iSipConfig.setEnabled((short)1);
		else
			iSipConfig.setEnabled((short)0);

 		if(btnEnableInStarfishMode.getSelection() == true)
			iSipConfig.setEnabledInP3M((short)1);
		else
			iSipConfig.setEnabledInP3M((short)0);
		
		iSipConfig.setPort(Integer.parseInt(txtPort.getText().trim()));
		IpAddress.tempAddress.setBytes(txtServerIpAddress.getText().trim());
		iSipConfig.setServerIpAddress(IpAddress.tempAddress.getBytes());
		
		short staCount = (short) tblStaEntries.getItemCount();
		iSipConfig.setStaInfoCount(staCount);
		
		for(int i=0;i<staCount;i++) {
			TableItem	itm 	= tblStaEntries.getItem(i);
			ISipStaInfo staInfo = iSipConfig.getStaInfoByIndex(i);
			staInfo.setExtension(Short.parseShort(itm.getText(0)));
			MacAddress.tempAddress.setBytes(itm.getText(1));
			staInfo.setMacAddress(MacAddress.tempAddress.getBytes());
		}
		
		return true;
	}

	@Override
	public void setUIAccess(boolean enabled) {

		if(enabled == false) {
			
			btnDelete.setEnabled(false);
			btnEdit.setEnabled(false);
			btnNew.setEnabled(false);
			btnSave.setEnabled(false);

			btnEnableInStarfishMode.setEnabled(false);
			txtPort.setEnabled(false);
			txtServerIpAddress.setEnabled(false);
			grp.setEnabled(false);
			for(Control c : this.grp.getChildren()) {
				c.setEnabled(false);
			}
			
		}

		super.setUIAccess(enabled);
	}
}
