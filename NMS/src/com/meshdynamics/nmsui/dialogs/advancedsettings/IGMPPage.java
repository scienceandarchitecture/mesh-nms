
package com.meshdynamics.nmsui.dialogs.advancedsettings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;
import com.meshdynamics.nmsui.dialogs.base.ConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;


public class IGMPPage extends ConfigPage {
    
	private Label				lblIGMPInformation;
	private Button				btnIGMP;
    
	private IConfiguration 		iConfiguration;
	
	/**
	 * @param parent
	 * @param versionInfo
	 */
	public IGMPPage(IMeshConfigBook parent) {
		
		super(parent);
		
		minimumBounds.width 	= 425;
		minimumBounds.height	= 145;
		iConfiguration			= parent.getConfiguration();
	}
		
	
	/**
	 * Function to create various controls for General Config Page
	 */
	public void createControls(CTabFolder tabFolder) {
	
		super.createControls(tabFolder);
		
		btnIGMP = new Button(canvas, SWT.CHECK);
		btnIGMP.setBounds(25, 25, 100, 25);
		btnIGMP.setText("IGMP Snooping");
		
		lblIGMPInformation = new Label(canvas, SWT.NO);
		lblIGMPInformation.setBounds(25, 60, 390,110);
		String text = "Without IGMP snooping, multicast traffic is treated in the same manner as"+SWT.CR+
					   "broadcast traffic. (ie. It is forwarded to all ports)"+SWT.CR+SWT.CR+
					   "With IGMP snooping, multicast traffic of a group is only forwarded to ports"+SWT.CR+
					   "that have membership of that group. IGMP Snooping generates no additional"+SWT.CR+
					   "network traffic, allowing you to significantly reduce multicast traffic passing"+SWT.CR+
					   "through your switch.";
		lblIGMPInformation.setText(text);

		ContextHelp.addContextHelpHandlerEx(this.canvas,contextHelpEnum.ADVCONFIGTABIGMP);
	}
		
	private boolean compareMeshConfigurationWithUI() {
		
		IMeshConfiguration	meshConfig	= iConfiguration.getMeshConfiguration();
		
		if(meshConfig == null) {
			return false;
		}
		
		return (btnIGMP.getSelection() == meshConfig.getIGMPEnabled()); 
	}

	private void initializeIGMPData() {
		
		IConfiguration		configuration	= parent.getConfiguration();
		IMeshConfiguration 	meshConfig		= configuration.getMeshConfiguration();
		btnIGMP.setSelection(meshConfig.getIGMPEnabled());
	}
	
	public void initalizeLocalData() {
		
		if(iConfiguration == null) { 
			return;
		}
		initializeIGMPData();

	}
	
	public boolean getDataFromUIToConfiguration(){
		if(iConfiguration == null) {
			return false; 
		}
		IMeshConfiguration	  meshConfig	= iConfiguration.getMeshConfiguration();
		meshConfig.setIGMPEnabled(btnIGMP.getSelection());
		return true;
	}
		
	public boolean validateLocalData() {
		
		return true;
	}
		
	public void selectionChanged(boolean selected) {
		
		if(selected	== true) {
			initalizeLocalData();
			canvas.setFocus();
		}else {
			if(validateLocalData() == false) {
				stayOnSameTab();
				return;
			}
			if(compareMeshConfigurationWithUI()	== false){
				
				int result	= parent.showMessage("Do you want to save the changes ? ",SWT.YES|SWT.CANCEL|SWT.NO);
				if(result == SWT.CANCEL) {
					stayOnSameTab();
					return;
				}else if(result	== SWT.NO) {
					initalizeLocalData();
				}else if(result	== SWT.YES) {
					getDataFromUIToConfiguration();
				}	
			}
		}
	}

	public void setGrpBounds(Rectangle minimumGrpBounds) {
		//grp.setBounds(10,10,minimumGrpBounds.width-10,minimumGrpBounds.height-25);
		
	}

}
