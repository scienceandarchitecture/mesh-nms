/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EtharenetConfigPage.java
 * Comments : Create Ethernet Tab in configuratuion dialog.
 * Created  : Sep 21, 2007
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 23, 2009 | Fix for single radio configuration				 | Abhijit |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 21, 2007 | Created                                         | Imran   |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.dialogs.advancedsettings;


import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Group;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.nmsui.dialogs.base.ConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;


public class EthernetConfigPage extends ConfigPage implements IEthernetFrameListener{

	private Group			grp; 
	private EthernetFrame	frmIxp0;
	private EthernetFrame	frmIxp1;
	
	
	
	public EthernetConfigPage(IMeshConfigBook parent) {
		super(parent);
		minimumBounds.width  = 425;
		minimumBounds.height = 315;
	}
	
	public void createControls(CTabFolder parent) {
		
		super.createControls(parent);
		
		grp = new Group(canvas,SWT.NO);
		grp.setBounds(10,10,400,290);
		
		String frmTitle = "";
		frmIxp0	= new EthernetFrame(grp,this,frmTitle);
		frmIxp0.setBounds(5,10,405,160);
		
		frmTitle = "";
		frmIxp1	= new EthernetFrame(grp,this,frmTitle);
		frmIxp1.setBounds(5,170,405,160);
		ContextHelp.addContextHelpHandlerEx(this.canvas, contextHelpEnum.ADVCONFIGTABETHERNET);
	}

	public void selectionChanged(boolean selected) {
		
		if(selected == false){
			if(compareConfigurationWithUI() == false){	
				
				int userSays	= parent.showMessage("You have changed Ethernet settings. "+SWT.LF+
						"Do you want to save these changes? ", SWT.YES|SWT.CANCEL|SWT.NO|SWT.ICON_QUESTION);
	
				if(userSays == SWT.YES){
					getDataFromUIToConfiguration();
				} else if(userSays == SWT.NO){
					revertEthernetSettings();
				} else if(userSays == SWT.CANCEL){
					stayOnSameTab();
				}
			}
		} else {
			initalizeLocalData();
			canvas.setFocus();
		}
	}

	private void revertEthernetSettings() {
		initalizeLocalData();
	}

	private boolean compareConfigurationWithUI() {
		
		boolean comparisonDone = false;
		
		if(frmIxp0.getEnabled() == true) {
			if(frmIxp0.compareConfigurationWithUI() == false)
				return false;
			
			comparisonDone = true;
		}
		
		if(frmIxp1.getEnabled() == true) {
			if(frmIxp1.compareConfigurationWithUI() == false)
				return false;
			
			comparisonDone = true;
		}
		
		return comparisonDone;
	}

	public void initalizeLocalData() {
		 
		IConfiguration 			iconfig			= parent.getConfiguration();
		if(iconfig == null) {
			return;
		}
		
		IVlanConfiguration		vlanConfig		= iconfig.getVlanConfiguration();
		if(vlanConfig == null) {
			return;
		}
		
		IInterfaceConfiguration	interfaceConfig	= iconfig.getInterfaceConfiguration();
		if(interfaceConfig == null) {
			return;
		}
		
		int ethCount = 0;
		for(int i=0;i<interfaceConfig.getInterfaceCount();i++) {
			IInterfaceInfo	ifInfo	= (IInterfaceInfo)interfaceConfig.getInterfaceByIndex(i);
			
			if(ifInfo.getMediumType() != Mesh.PHY_TYPE_802_3)
				continue;
		
			ethCount++;
			
			String info = "Port " + ifInfo.getName();
			if(ifInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_DS) 
				info += " (Uplink)";
			else if(ifInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_WM)
				info += " (Downlink)";
				
			if(ethCount == 1) {
				frmIxp0.initialiseEthernetInfo(ifInfo, vlanConfig);
				frmIxp0.setFrmTitle(info);
			} else {
				frmIxp1.initialiseEthernetInfo(ifInfo, vlanConfig);
				frmIxp1.setFrmTitle(info);
			}
			
		}
		
		if(ethCount == 0) {
			frmIxp0.setFrmTitle("Not Available");
			frmIxp0.setEnabled(false);	
			frmIxp1.setFrmTitle("Not Available");
			frmIxp1.setEnabled(false);
		} else if(ethCount == 1) {
			frmIxp1.setFrmTitle("Not Available");
			frmIxp1.setEnabled(false);
		}
	}
	
	
	public void selectedConfiguration(EthernetFrameSelectionEvent efsEvent) {
				
	}

	public void selectedVlan(String vlanName) {
			
	}

	public boolean getDataFromUIToConfiguration() {
		if(frmIxp0.getEnabled() == true)
			frmIxp0.getDataFromUIToConfiguration();
		
		if(frmIxp1.getEnabled() == true)
			frmIxp1.getDataFromUIToConfiguration();
		
		return true;
	}
	
	public void setGrpBounds(Rectangle minimumGrpBounds) {
		grp.setBounds(10,10,minimumGrpBounds.width-10,minimumGrpBounds.height-25);
		frmIxp0.setBounds(10,10,minimumGrpBounds.width-30,160);
		frmIxp1.setBounds(10,170,minimumGrpBounds.width-30,160);
	}
}
