/*
 * Created on Apr 18, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.dialogs.advancedsettings;

import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;

/**
 * @author imran
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EthernetInfo {
	
	private IInterfaceInfo	ifInfo;
	private String  		vlanName;
	
	public EthernetInfo() {
		ifInfo		= null;
		vlanName 	= "";
	}
	
	public String getEssid() {
		return ifInfo.getEssid();
	}
	
	public void setEssid(String essid) {
		ifInfo.setEssid(essid);
	}
	
	public String getName() {
		return ifInfo.getName();
	}
	
	public String getVlanName() {
		return vlanName;
	}

	public void setVlanName(String vlanName) {
		this.vlanName = vlanName;
	}
	

	/**
	 * @return Returns the ifInfo.
	 */
	public IInterfaceInfo getIfInfo() {
		return ifInfo;
	}
	/**
	 * @param ifInfo The ifInfo to set.
	 */
	public void setIfInfo(IInterfaceInfo ifInfo) {
		this.ifInfo = ifInfo;
	}
}
