package com.meshdynamics.nmsui.dialogs.advancedsettings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.nmsui.dialogs.base.ConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;

public class P3MConfigPage extends ConfigPage {

	private Button 	chkEnableP3M;
	private Button	chkBeginInfraMode;
	private Button	chkEnableSectoredUsage;
	private Label	lblInfo;
	
	private boolean	enableP3M;
	private boolean beginInfra;
	private boolean enableSector;
	private boolean	uplinkDCAListOk;
	
	private IMeshConfiguration meshConfig;
	
	P3MConfigPage(IMeshConfigBook parent) {
		super(parent);
		minimumBounds.width  = 425;
		minimumBounds.height = 315;
	}

	@Override
	public void createControls(CTabFolder parent) {
		super.createControls(parent);
		
		int relY = 15;
		
		chkEnableP3M = new Button(canvas, SWT.CHECK);
		chkEnableP3M.setBounds(15, relY, 200, 20);
		chkEnableP3M.setText("Activate P3M(tm) mode.");
		chkEnableP3M.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				enableP3M = chkEnableP3M.getSelection();
				if(enableP3M == false) {
					chkBeginInfraMode.setSelection(false);
					chkEnableSectoredUsage.setSelection(false);
				}
				chkBeginInfraMode.setEnabled(enableP3M);
				chkEnableSectoredUsage.setEnabled(enableP3M);
			}
		});

		relY += 30;
		chkBeginInfraMode = new Button(canvas, SWT.CHECK);
		chkBeginInfraMode.setBounds(15, relY, 200, 20);
		chkBeginInfraMode.setText("Startup in infrastructure mode.");
		chkBeginInfraMode.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				beginInfra = chkBeginInfraMode.getSelection();
			}
		});
		
		relY += 30;
		chkEnableSectoredUsage = new Button(canvas, SWT.CHECK);
		chkEnableSectoredUsage.setBounds(15, relY, 200, 20);
		chkEnableSectoredUsage.setText("Enable sectored antenna mode.");
		chkEnableSectoredUsage.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent selectionevent) {
				enableSector = chkEnableSectoredUsage.getSelection();
			}
		});
		
		relY += 30;
		lblInfo = new Label(canvas, SWT.NONE);
		lblInfo.setBounds(15, relY, 400, 400);
		
		String str = "P3M(tm) mode requirements \n";
		str += "If scanner interface is not available then ";
		str += "scan channel list of uplink interface should " + SWT.CR;
		str += "be non empty.";
		lblInfo.setText(str);
		
		ContextHelp.addContextHelpHandlerEx(this.canvas,contextHelpEnum.ADVCONFIGTABP3M);
	}

	@Override
	public void selectionChanged(boolean selected) {
		super.selectionChanged(selected);
		if(selected == false) {
			
			if(validateLocalData() == false) {
				stayOnSameTab();
				return;
			}
			
			if(enableP3M != meshConfig.getP3MMode() ||
				beginInfra != meshConfig.getP3MModeInfraStartup() ||
				enableSector != meshConfig.getP3MModeSectoredUse()) {
				MessageBox msgBox = new MessageBox(new Shell(), SWT.ICON_QUESTION|SWT.YES|SWT.NO|SWT.CANCEL);
				msgBox.setMessage("Do you want to save changes?");
				int ret = msgBox.open();
				if(ret == SWT.CANCEL) {
					super.stayOnSameTab();
					return;
				}
				
				if(ret == SWT.NO) {
					initalizeLocalData();
					return;
				}
			}
			
			getDataFromUIToConfiguration();
			return;
		}

		if(uplinkDCAListOk == false) {
			chkEnableP3M.setSelection(false);
			chkBeginInfraMode.setSelection(false);
			chkEnableSectoredUsage.setSelection(false);
			String str = "P3M(tm) mode requirements " + SWT.CR + SWT.CR;
			str += "Scan channel list of uplink interface is empty. ";
			str += "P3M(tm) mode cannot be activated in" + SWT.CR;
			str += "this condition.Please fill scan channel list for uplink.";
			lblInfo.setText(str);
			setUIAccess(false);
			lblInfo.setEnabled(true);
		}
		
		if(enableP3M == true) {
			chkEnableP3M.setSelection(true);
			chkBeginInfraMode.setEnabled(true);
			chkEnableSectoredUsage.setEnabled(true);
		} else {
			chkEnableP3M.setSelection(false);
			chkBeginInfraMode.setEnabled(false);
			chkEnableSectoredUsage.setEnabled(false);
		}
		
		if(beginInfra == true)
			chkBeginInfraMode.setSelection(true);
		else
			chkBeginInfraMode.setSelection(false);
		
		if(enableSector == true)
			chkEnableSectoredUsage.setSelection(true);
		else
			chkEnableSectoredUsage.setSelection(false);

		canvas.setFocus();
	}

	@Override
	public boolean validateLocalData() {
		return super.validateLocalData();
	}

	@Override
	public boolean getDataFromUIToConfiguration() {
		meshConfig.setP3MMode(enableP3M);
		meshConfig.setP3MModeInfraStartup(beginInfra);
		meshConfig.setP3MModeSectoredUse(enableSector);
		return true;
	}

	@Override
	public void initalizeLocalData() {
		super.initalizeLocalData();
		meshConfig 		= parent.getConfiguration().getMeshConfiguration(); 
		enableP3M 		= meshConfig.getP3MMode();
		beginInfra 		= meshConfig.getP3MModeInfraStartup();
		enableSector	= meshConfig.getP3MModeSectoredUse();

		IInterfaceConfiguration ifConfig = parent.getConfiguration().getInterfaceConfiguration();
		boolean scannerAvailable = false;
		for(int i=0;i<ifConfig.getInterfaceCount();i++) {
			IInterfaceInfo ifInfo = ifConfig.getInterfaceByIndex(i);
			if(ifInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_AMON ||
				ifInfo.getUsageType() == Mesh.PHY_USAGE_TYPE_PMON)
				scannerAvailable = true;
		}
		
		if(scannerAvailable == true) {
			uplinkDCAListOk = true;
			return;
		}
		
		uplinkDCAListOk = true;
		
		for(int i=0;i<ifConfig.getInterfaceCount();i++) {
			IInterfaceInfo ifInfo = ifConfig.getInterfaceByIndex(i);
			if(ifInfo.getMediumType() != Mesh.PHY_TYPE_802_11 ||
				ifInfo.getUsageType() != Mesh.PHY_USAGE_TYPE_DS)
				continue;
			
			if(ifInfo.getDcaListCount() <= 0) {
				uplinkDCAListOk = false;
				return;
			}
				
		}
		
		
	}
	
	
}
