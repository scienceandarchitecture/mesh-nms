/*
 * Created on May 3, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.nmsui.dialogs.advancedsettings;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.configuration.I80211eCategoryConfiguration;
import com.meshdynamics.meshviewer.configuration.I80211eCategoryInfo;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.nmsui.controls.Slider;
import com.meshdynamics.nmsui.dialogs.base.ConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;

/**
 * @author imran
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Dot11eCategoryConfigPage extends ConfigPage{

	private	Group		grp;
	private Group 		grp2; 
	private Label		lbl;
	private Text		txtBurstTime;
	private Label 		lblACWMin;
	private Label 		lblACWMax;
	private Text 		txtAifsn;
	private Button 		btnDisableBackoff;
	private Slider		sldCWMin;
	private Slider		sldCWMax;
	private	int			lastSelectedCategoryIndex;
	
	private Button[]	btnRadios;     					
	
	private final	String	CATEGORY_NAME_BACKGROUND	= "ac_bk";
	private final	String	CATEGORY_NAME_BEST_EFFORT	= "ac_be";
	private final	String	CATEGORY_NAME_VOICE			= "ac_vo";
	private final	String	CATEGORY_NAME_VIDEO			= "ac_vi";
	
	private final	String	MAPPED_CATEGORY_NAME_BACKGROUND		= "Background";
	private final	String	MAPPED_CATEGORY_NAME_BEST_EFFORT	= "Best Effort";
	private final	String	MAPPED_CATEGORY_NAME_VOICE			= "Voice";
	private final	String	MAPPED_CATEGORY_NAME_VIDEO			= "Video";
	
	public Dot11eCategoryConfigPage(IMeshConfigBook parent) {
		super(parent);
		minimumBounds.width  = 425;
		minimumBounds.height = 345;
		lastSelectedCategoryIndex	= 0;
	}
	
	public void createControls(CTabFolder parent) {
		
		super.createControls(parent);
		
		int rel_x		  = 10;
		int rel_y		  = 10;
		int width		  = 90;
		int inc			  = 30;
		int numRadios	  = 4;
		int index;		
		
		
		grp = new Group(canvas,SWT.SIMPLE);
		grp.setBounds(rel_x,rel_y,400,320);
		
		rel_x	+= 30;
		rel_y	+= 20;
			
		SelectionAdapter selectionAdapter = new SelectionAdapter(){
			
			public void widgetSelected(SelectionEvent arg0) {
				
				int radioIndex;
				
				Button btn = (Button) arg0.getSource();				
				if(btn.getSelection() == false)
					return;
				
				for(radioIndex = 0; radioIndex < btnRadios.length; radioIndex++) {
					
					if(arg0.getSource().equals(btnRadios[radioIndex])) {
						radioSelected(radioIndex);
						break;
					}
				}
			}
		};
		
		btnRadios =  new Button[numRadios];
  	    for(index = 0;index < numRadios;index++) {
	       	btnRadios[index] = new Button(grp,SWT.RADIO);
			btnRadios[index].setBounds(rel_x,rel_y ,100,17);
			btnRadios[index].addSelectionListener(selectionAdapter);
			rel_y			 += 25;
			setButtonText(btnRadios[index],index);
		}
		
		rel_x		 	 -= 25;
		rel_y			 += 5;
		grp2 			 =  new Group(grp,SWT.NONE);
		grp2.setBounds(rel_x,rel_y,395,180);
		
		rel_x 		  	 += 20;
		rel_y			 -= 110;
		lbl				  =	new Label(grp2,SWT.NO);
		lbl.setBounds(rel_x,rel_y,width,15);
		lbl.setText("ACWmin");
		
		rel_y			 += 2;
		sldCWMin 		  = new Slider(grp2,SWT.FLAT);
		sldCWMin.setBounds(rel_x+width+inc,rel_y,150,20);
		sldCWMin.setMinValue(0);
		sldCWMin.setMaxValue(1023);
		sldCWMin.addValueListener(new Listener(){
			public void handleEvent(Event arg0) {
				lblACWMin.setText("  " + sldCWMin.getValue() + " Unit");
			}			
		});
		
		rel_y			 -= 5;
		lblACWMin = new Label(grp2, SWT.NO);
		lblACWMin.setBounds(rel_x+width+inc+150,rel_y,60,20);
				
		rel_y 			 += 33;		
		lbl				  =	new Label(grp2,SWT.NO);
		lbl.setBounds(rel_x,rel_y,width,15);
		lbl.setText("ACWmax");

		rel_y			 += 2;
		sldCWMax 		  = new Slider(grp2,SWT.FLAT);
		sldCWMax.setBounds(rel_x+width+inc,rel_y,150,20);
		sldCWMax.setMinValue(0);
		sldCWMax.setMaxValue(1023);
		sldCWMax.addValueListener(new Listener(){
			public void handleEvent(Event arg0) {
				lblACWMax.setText("  " + sldCWMax.getValue() + " Unit");
			}			
		});
	
		rel_y			 -= 5;
		lblACWMax 		  = new Label(grp2, SWT.NO);
		lblACWMax.setBounds(rel_x+width+inc+150,rel_y,60,20);
		
		
		rel_y 			 += 35;		
		lbl				  =	new Label(grp2,SWT.NO);
		lbl.setBounds(rel_x,rel_y,width,15);
		lbl.setText("AIFSN");
		
		rel_y			 -= 5;
		txtAifsn 		  = new Text(grp2, SWT.BORDER);
		txtAifsn.setBounds(rel_x+width+inc,rel_y,150,20);
			
		rel_x			 += 160;
		rel_y			 += 5;
		lbl				  =	new Label(grp2,SWT.NO);
		lbl.setBounds(rel_x+width+inc,rel_y,60,20);
		lbl.setText(" Units");
		
		rel_x			 -= 160;
		rel_y 			 += 30;
		lbl				 =	new Label(grp2,SWT.NO);
		lbl.setBounds(rel_x,rel_y,width,15);
		lbl.setText("Burst Time(TXOP)");

		rel_y			 -= 5;
		txtBurstTime 	  = new Text(grp2, SWT.BORDER);
		txtBurstTime.setBounds(rel_x+width+inc,rel_y,150,20);

		rel_x			 += 160;
		rel_y			 += 5;
		lbl				  =	new Label(grp2,SWT.NO);
		lbl.setBounds(rel_x+width+inc,rel_y,60,15);
		lbl.setText("microsecs");
		
		rel_x			 -= 160;
		rel_y			 += 30;
		lbl				  =	new Label(grp2,SWT.NO);
		lbl.setBounds(rel_x,rel_y,width+15,15);
		lbl.setText("Disable Backoff");		
		
		rel_y			 -= 5;
		btnDisableBackoff = new Button(grp2,SWT.CHECK);		
		btnDisableBackoff.setBounds(rel_x+width+inc,rel_y,150,20);
		
		ContextHelp.addContextHelpHandlerEx(this.canvas,contextHelpEnum.ADVCONFIGTAB80211E);
	}
	
	private void setButtonText(Button button, int index) {
		
		String labelText = "";
		
		switch(index) {
		case 0:
			labelText = MAPPED_CATEGORY_NAME_BACKGROUND;
			break; 
		case 1:
			labelText = MAPPED_CATEGORY_NAME_BEST_EFFORT;
			break;
		case 2:
			labelText = MAPPED_CATEGORY_NAME_VIDEO;
			break;
		case 3:
			labelText = MAPPED_CATEGORY_NAME_VOICE;
			break;
		}
		
		button.setText(labelText);
	}

	private void radioSelected(int radioIndex) {
		
		int	userSays;
		
		if(validateLocalData() == false) {
			doNotSwitchRadio();
			return;
		}
			
		if(compairConfigurationWithUI() == false){
			
			userSays	= parent.showMessage("You have changed Dot11e category settings"+SWT.LF+
					"Do you want to save these changes?",SWT.YES|SWT.CANCEL|SWT.NO|SWT.ICON_QUESTION);
			
			if(userSays == SWT.YES){
				getDataFromUIToConfiguration();
				displayCategoryValues(radioIndex);
				lastSelectedCategoryIndex = radioIndex;
			}
			if(userSays == SWT.NO){
				displayCategoryValues(radioIndex);
				lastSelectedCategoryIndex = radioIndex;
			}
			if(userSays == SWT.CANCEL){
				doNotSwitchRadio();
			}
		}else{
			displayCategoryValues(radioIndex);
			lastSelectedCategoryIndex = radioIndex;
		}
	}

	private void doNotSwitchRadio() {
		int radioIndex;
		
		for(radioIndex = 0; radioIndex < btnRadios.length; radioIndex++){
			if(radioIndex == lastSelectedCategoryIndex) {
				btnRadios[radioIndex].setSelection(true);
			}
			else {
				btnRadios[radioIndex].setSelection(false);
			}
		}
	}

	public void selectionChanged(boolean selected) {
		
		if(selected == false) {
			
			if(validateLocalData() == false) {
				stayOnSameTab();
				return;
			}
			
			if(compairConfigurationWithUI() == false){	
				int userSays	= parent.showMessage("You have changed Dot11e Category settings. "+SWT.LF+
						"Do you want to save these changes? ", SWT.YES|SWT.CANCEL|SWT.NO|SWT.ICON_QUESTION);

				if(userSays == SWT.YES){
					getDataFromUIToConfiguration();
				} else if(userSays == SWT.NO){
					//displayCategoryValues(getSelectedRadioIndex());
					return;
				} else if(userSays == SWT.CANCEL){
					selectRadio(lastSelectedCategoryIndex);
					stayOnSameTab();
				}
			}
		} else {
			initalizeLocalData();
			canvas.setFocus();
		}
	}
	
	/**
	 * 
	 */
	private boolean compairConfigurationWithUI() {
		
		int 	selection;
				
		I80211eCategoryInfo	 categoryInfo	= (I80211eCategoryInfo)btnRadios[lastSelectedCategoryIndex].getData();
		if(categoryInfo == null) {
			return false;
		}
		
		if(categoryInfo.getACWMin() != sldCWMin.getValue()) {
			return false;
		}
		if(categoryInfo.getACWMax() != sldCWMax.getValue()) {
			return false;
		}
		if(categoryInfo.getAIFSN() != Integer.parseInt(txtAifsn.getText().trim())) {
			return false;
		}
		if(categoryInfo.getBurstTime() != 
			Integer.parseInt(txtBurstTime.getText().trim())) {
			return false;
		}
		
		if(btnDisableBackoff.getSelection() == true) {
			selection = 1;
		} else {
			selection = 0;
		}
		if(categoryInfo.getDisableBackoff() != selection) {
			return false;
		}
		return true;
		
	}

	public void initalizeLocalData() {

		int		index;
		int 	categoryCount;
		String  categoryName;
				
		IConfiguration	iConfiguration					= parent.getConfiguration();
		if(iConfiguration == null) {
			return;
		}
		
		I80211eCategoryConfiguration	categoryConfig	= iConfiguration.
														  get80211eCategoryConfiguration();
		if(categoryConfig == null) {
			return;
		}
		
		categoryCount	= categoryConfig.getCount();
		for(index = 0; index < categoryCount; index++) {
			
			categoryName							= getCategoryNameByIndex(index);	
			I80211eCategoryInfo	categoryInfo		= (I80211eCategoryInfo)categoryConfig.getCategoryInfoByName(categoryName);
			if(categoryInfo == null) {
				continue;
			}
			btnRadios[index].setData(categoryInfo);
		}
		
		selectRadio(lastSelectedCategoryIndex);
		displayCategoryValues(lastSelectedCategoryIndex);
	}

	/**
	 * @param index
	 * @return
	 */
	private String getCategoryNameByIndex(int index) {
		
		switch(index) {
		case 0:
			return CATEGORY_NAME_BACKGROUND;
		case 1:
			return CATEGORY_NAME_BEST_EFFORT;
		case 2:
			return CATEGORY_NAME_VIDEO;
		case 3:
			return CATEGORY_NAME_VOICE;
		}
		return null;
	}

	/**
	 * 
	 */
	private void selectRadio(int radioIndex) {
		int index;
		for(index=0; index<btnRadios.length; index++) {
			if(index == radioIndex) {
				btnRadios[index].setSelection(true);
			}else {
				btnRadios[index].setSelection(false);
			}
		}
	}

	/**
	 * 
	 */
	private void displayCategoryValues(int radioIndex) {
		
		int 	selection;
				
		I80211eCategoryInfo	 categoryInfo	= (I80211eCategoryInfo)btnRadios[radioIndex].getData();
		if(categoryInfo == null) {
			return;
		}
		
		grp2.setText(btnRadios[radioIndex].getText());
		
		sldCWMin.setValue(categoryInfo.getACWMin());
		sldCWMax.setValue(categoryInfo.getACWMax());
		txtAifsn.setText(""+categoryInfo.getAIFSN());
		txtBurstTime.setText(""+categoryInfo.getBurstTime());
		
		selection	= categoryInfo.getDisableBackoff();
		if(selection == 1) {
			btnDisableBackoff.setSelection(true);
		} else {
			btnDisableBackoff.setSelection(false);
		}
	}

	public boolean	getDataFromUIToConfiguration(){
		
		I80211eCategoryInfo	 categoryInfo	= (I80211eCategoryInfo)btnRadios[lastSelectedCategoryIndex].getData();
		if(categoryInfo == null) {
			return false;
		}
		
		categoryInfo.setACWMin(sldCWMin.getValue());
		categoryInfo.setACWMax(sldCWMax.getValue() );
		categoryInfo.setAIFSN(Short.parseShort(txtAifsn.getText().trim()));
		categoryInfo.setBurstTime(Long.parseLong(txtBurstTime.getText().trim()));
		
		
		if(btnDisableBackoff.getSelection() == true) {
			categoryInfo.setDisableBackoff((short)1);
		} else {
			categoryInfo.setDisableBackoff((short)0);
		}
		return true;
	
	}
	
	public void setGrpBounds(Rectangle minimumGrpBounds) {
		grp.setBounds(10,10,minimumGrpBounds.width-10,minimumGrpBounds.height-25);
		grp2.setBounds(10,130,minimumGrpBounds.width-30,180);		
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.ConfigPage#validateLocalData()
	 */
	public boolean validateLocalData() {
		
		int 	aifsn; 
		long	burstTime;	
		String  strAifsn;
		String  strBurstTIme;
		strAifsn		= txtAifsn.getText().trim();
		strBurstTIme	= txtBurstTime.getText().trim();
		int  acwmin = sldCWMin.getValue();
		int acwmax = sldCWMax.getValue();
		
		if(acwmin > acwmax){
			parent.showMessage("ACWmax should be > ACWmin",SWT.ICON_INFORMATION|SWT.OK); 
			return false;
		}
		
		switch(lastSelectedCategoryIndex) {
		case 0:
			if(acwmin != 15 && acwmin != 31 && acwmin != 63 && acwmin != 127 && acwmin != 255 && acwmin != 511 && acwmin != 1023){
				parent.showMessage("For Background ACWmin Select from [15, 31, 63, 127, 255, 511, 1023]",SWT.ICON_INFORMATION|SWT.OK); 
				return false;
			}
			if(acwmax != 15 && acwmax != 31 && acwmax != 63 && acwmax != 127 && acwmax != 255 && acwmax != 511 && acwmax != 1023){
				parent.showMessage("For Background ACWmax Select from [15, 31, 63, 127, 255, 511, 1023]",SWT.ICON_INFORMATION|SWT.OK); 
				return false;
			}
		case 1:
			if(acwmin != 15 && acwmin != 31 && acwmin != 63 && acwmin != 127 && acwmin != 255 && acwmin != 511 && acwmin != 1023){
				parent.showMessage("For Best Effort  ACWmin Select from [15, 31, 63, 127, 255, 511, 1023]",SWT.ICON_INFORMATION|SWT.OK); 
				return false;
			}
			if(acwmax != 15 && acwmax != 31 && acwmax != 63 && acwmax != 127 && acwmax != 255 && acwmax != 511 && acwmax != 1023){
				parent.showMessage("For Best Effort  ACWmax Select from [15, 31, 63, 127, 255, 511, 1023]",SWT.ICON_INFORMATION|SWT.OK); 
				return false;
			}
		case 2:
			if(acwmin != 7 && acwmin != 15 && acwmin != 31 && acwmin != 63 && acwmin != 127 && acwmin != 255 && acwmin != 511 && acwmin != 1023){
				parent.showMessage("For Video ACWmin Select from [7, 15, 31, 63, 127, 255, 511, 1023]",SWT.ICON_INFORMATION|SWT.OK); 
				return false;
			}
			if(acwmax != 7 && acwmax != 15 && acwmax != 31 && acwmax != 63 && acwmax != 127 && acwmax != 255 && acwmax != 511 && acwmax != 1023){
				parent.showMessage("For Video ACWmax Select from [7, 15, 31, 63, 127, 255, 511, 1023]",SWT.ICON_INFORMATION|SWT.OK); 
				return false;
			}
		case 3:
			if(acwmin != 3 && acwmin != 7 && acwmin != 15 && acwmin != 31 && acwmin != 63 && acwmin != 127 && acwmin != 255 && acwmin != 511 && acwmin != 1023){
				parent.showMessage("For Voice ACWmin Select from [3, 7, 15, 31, 63, 127, 255, 511, 1023]",SWT.ICON_INFORMATION|SWT.OK); 
				return false;
			}
			if(acwmax != 3 && acwmax != 7 && acwmax != 15 && acwmax != 31 && acwmax != 63 && acwmax != 127 && acwmax != 255 && acwmax != 511 && acwmax != 1023){
				parent.showMessage("For Voice ACWmax Select from [3, 7, 15, 31, 63, 127, 255, 511, 1023]",SWT.ICON_INFORMATION|SWT.OK); 
				return false;
			}
		}
		
		if(MeshValidations.isInt(strAifsn) == false) {
			parent.showMessage("AIFSN invalid. ",SWT.ICON_INFORMATION|SWT.OK);
			return false;
		}
		
		if(MeshValidations.isLong(strBurstTIme) == false) {
			parent.showMessage("Burst Time invalid. ",SWT.ICON_INFORMATION|SWT.OK);
			return false;
		}
		
		aifsn  		= Integer.parseInt(txtAifsn.getText().trim());
		burstTime	= Long.parseLong(txtBurstTime.getText().trim());
		
		if(aifsn < 1 || aifsn > 254) {
			parent.showMessage("AIFSN incorrect. "+SWT.LF+
					"Valid range is in between 1 to 254.",SWT.ICON_ERROR);
			return false;
		}
		
		if(burstTime < 0 || burstTime > 32767) {
			parent.showMessage("Burst Time incorrect. "+SWT.LF+
					"Valid range is in between 0 to 32767.",SWT.ICON_ERROR);
			return false;
		}
		return true;
	}
	
}
