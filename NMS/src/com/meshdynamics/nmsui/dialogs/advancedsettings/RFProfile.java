/**
 * MeshDynamics 
 * -------------- 
 * File     : RFProfile.java
 * Comments : 
 * Created  : Oct 31, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Oct 31, 2007   | Created                        			  | Abhishek     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.dialogs.advancedsettings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.configuration.IChannelInfo;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceCustomChannelConfig;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationTextReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationTextWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.nmsui.dialogs.advancedsettings.RfConfigPage;


public class RFProfile {
	

	public static final String 	PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ = "psf";
	public static final String 	PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ = "psh";
	public static final String 	PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ 	= "psq";
	public static final String 	PHY_SUB_TYPE_802_11_B_G					= "bg";
	public static final String 	PHY_SUB_TYPE_802_11_G					= "g";
	public static final String 	PHY_SUB_TYPE_802_11_B					= "b";
	public static final String 	PHY_SUB_TYPE_802_11_A					= "a";
	public static final String 	PHY_SUB_TYPE_INGNORE					= "-";

	private ConfigurationTextReader configReader;
	
	
	public class Profile {
		
		public InterfaceCustomChannelConfig	customChannelConfig;
		public String 						profileName;
		public short						mediumSubType;	
		
		public Profile(){
			customChannelConfig = new InterfaceCustomChannelConfig();
		}
	}
	
	private Vector<Profile>		vectProfiles;
	
	public RFProfile() {
		vectProfiles	= new Vector<Profile>();
	}
	
	public RFProfile(String fileName) {
	    vectProfiles   = new Vector<Profile>();
	    getProfiles(fileName);
	}
	
    public int getProfileCount(){
        return vectProfiles.size();
    }
    
    public Profile getProfile(int index) {
        
        if(index < 0 || index >= vectProfiles.size())
            return null;
        
        return vectProfiles.get(index);
    }
	
	public Enumeration<Profile> getProfiles(String fileName) {
		
		try{
			
			File 				configFile  = new File(fileName);
			FileInputStream		fileStream  = new FileInputStream(configFile);
			configReader					= new ConfigurationTextReader(fileStream);  
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	
		Profile newProfile 		= null;
		short 	mediumSubtype 	= 0;
		String 	profileName		= null;
		
		try {
			while (configReader.getNextToken() == true){
				
				int currentTokenType	=	configReader.getCurrentTokenType();
	
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
					
					int currentHeader   =   configReader.getCurrentHeader();
					
					if(currentHeader == IConfigurationIODefs.RF_PROFILE_INFO_START){
						
						newProfile					= new Profile();
						newProfile.mediumSubType	= mediumSubtype;
						newProfile.profileName		= profileName;
					
						loadProfileInfo(configReader, newProfile);
					}
				}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
					
					String tokenValue	=	configReader.getCurrentTokenValue();
					
					if(configReader.getCurrentToken() == IConfigurationIODefs.RF_PROFILE_NAME){
						profileName	= tokenValue;
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_MEDIUM_SUB_TYPE){
						try{
							mediumSubtype	= getMediumSubType(tokenValue);							
						}catch(Exception e){
							e.printStackTrace();						
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_CONFORMANCE){
						try{
							getConformanceId(tokenValue);							
						}catch(Exception e){
							e.printStackTrace();						
						}
					}
				}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_UNDEFINED){
					
				}	
			}			
		}catch (Exception e1) {
			e1.printStackTrace();
		}
		return vectProfiles.elements();		
	}

	/**
	 * @param newProfile
	 * @param configReader2
	 * @param profile
	 * @param phySubType
	 */
	private void loadProfileInfo(ConfigurationTextReader configReader, Profile newProfile) {
		
		while (configReader.getNextToken() == true){
			
			int currentTokenType	=	configReader.getCurrentTokenType();
		
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				
				if(configReader.getCurrentHeader()==IConfigurationIODefs.RF_PROFILE_INFO_END){
					vectProfiles.add(newProfile);
					return;
				}else if(configReader.getCurrentHeader()==IConfigurationIODefs.RF_PROFILE_INFO_START){
					newProfile	= new Profile();	 
				}else if(configReader.getCurrentHeader()==IConfigurationIODefs.CONFIG_HEADER_CHANNEL_INFO_START) {
					for(int i = 0; i < newProfile.customChannelConfig.getChannelInfoCount(); i++) {
						IChannelInfo	channelInfo	= (IChannelInfo)newProfile.customChannelConfig.getChannelInfo(i);
						loadRFInfo(channelInfo, configReader);
					}
				}
			}else if(currentTokenType	==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
				
				String tokenValue	=	configReader.getCurrentTokenValue();
				 if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_COUNT){
					try{
						int count    	 =	Integer.parseInt(tokenValue.trim());
						newProfile.customChannelConfig.setChannelInfoCount(count);						
					}catch(Exception e){
						e.printStackTrace();						
					}
				}
			}
		}
	}

	
	/**
	 * @param tokenValue
	 * @return
	 */
	private short getMediumSubType(String strType) {
		
		if(strType.equalsIgnoreCase(PHY_SUB_TYPE_INGNORE)) {
			return Mesh.PHY_SUB_TYPE_INGNORE;
		}else if(strType.equalsIgnoreCase(PHY_SUB_TYPE_802_11_A)) {
			return Mesh.PHY_SUB_TYPE_802_11_A;
		}else if(strType.equalsIgnoreCase(PHY_SUB_TYPE_802_11_B)) {
			return Mesh.PHY_SUB_TYPE_802_11_B;
		}else if(strType.equalsIgnoreCase(PHY_SUB_TYPE_802_11_B_G)) {
			return Mesh.PHY_SUB_TYPE_802_11_B_G;
		}else if(strType.equalsIgnoreCase(PHY_SUB_TYPE_802_11_G)) {
			return Mesh.PHY_SUB_TYPE_802_11_G;
		}else if(strType.equalsIgnoreCase(PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ)) {
			return Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ;
		}else if(strType.equalsIgnoreCase(PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ)) {
			return Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ;
		}else if(strType.equalsIgnoreCase(PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ)) {
			return Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ;
		}
		return Mesh.PHY_SUB_TYPE_INGNORE;
	}

	/**
	 * @param tokenValue
	 * @return
	 */
	private short getConformanceId(String ctl) {
		
		if(ctl.equalsIgnoreCase(RfConfigPage.NONE)) {
			return RfConfigPage.NONE_VALUE;
		}else if(ctl.equalsIgnoreCase(RfConfigPage.FCC)) {
			return RfConfigPage.FCC_VALUE;
		}else if(ctl.equalsIgnoreCase(RfConfigPage.ETSI)) {
			return RfConfigPage.ETSI_VALUE;
		}else if(ctl.equalsIgnoreCase(RfConfigPage.MKK)) {
			return RfConfigPage.MKK_VALUE;
		}		
		return -1;
	}

	/**
	 * @param profileInfo2
	 * @param configReader2
	 */
	private void loadRFInfo(IChannelInfo channelInfo, ConfigurationTextReader configReader) {
		
		while (configReader.getNextToken() == true){
			
			int currentTokenType	=	configReader.getCurrentTokenType();
		
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				
				if(configReader.getCurrentHeader()==IConfigurationIODefs.CONFIG_HEADER_CHANNEL_INFO_END){
					return;
				}
				
			}else if(currentTokenType	==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
				
				String tokenValue	=	configReader.getCurrentTokenValue();
				
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_NUMBER){
					try{
						short channel	= Short.parseShort(tokenValue);
						channelInfo.setChannelNumber(channel);						
					}catch(Exception e) {
						e.printStackTrace();
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_CENTER_FREQUENCY){
					try{
						int freq		  = Integer.parseInt(tokenValue);
						channelInfo.setFrequency(freq);
					}catch(Exception e) {
						e.printStackTrace();
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_FLAGS){
					try{
						long flags    	 =	Long.parseLong(tokenValue.trim());
						channelInfo.setPrivateChannelFlags((short)flags);
					}catch(Exception e){
						e.printStackTrace();						
					}
				}
			}
		}
	}

	
	public void save(String fileName) {
		
		try {
			FileOutputStream 		fileStream   = new FileOutputStream(fileName);
			ConfigurationTextWriter configWriter = new ConfigurationTextWriter(fileStream);
		
			Enumeration<Profile> enumeration	= vectProfiles.elements();
			while(enumeration.hasMoreElements()) {
				Profile	profile	= (Profile)enumeration.nextElement();
				if(profile == null) {
					continue;
				}
				saveProfile(configWriter, profile);
			}
			configWriter.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		MessageBox msgBx = new MessageBox(new Shell(SWT.CLOSE), SWT.OK | SWT.ICON_INFORMATION);
		msgBx.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		msgBx.setMessage("File "+fileName+" saved.");
		msgBx.open();
		
	}
	/**
	 * @param configWriter
	 * @param profile
	 */
	private void saveProfile(ConfigurationTextWriter configWriter, Profile profile) {
		
		configWriter.writeHeader(IConfigurationIODefs.RF_PROFILE_HEADER_START);
		
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_MEDIUM_SUB_TYPE, getMediumSubTypeName(profile.mediumSubType));
		configWriter.writeEntry(IConfigurationIODefs.RF_PROFILE_NAME, profile.profileName);
		
		configWriter.writeHeader(IConfigurationIODefs.RF_PROFILE_INFO_START);
		int chCount	= profile.customChannelConfig.getChannelInfoCount();
		
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_COUNT, ""+chCount);
		for(int i = 0; i < chCount; i++) {
			
			IChannelInfo	channelInfo	= (IChannelInfo)profile.customChannelConfig.getChannelInfo(i);
			if(channelInfo == null) {
				continue;
			}
			saveChannelInfo(configWriter, channelInfo);
		}
		configWriter.writeHeader(IConfigurationIODefs.RF_PROFILE_INFO_END);
		
		configWriter.writeHeader(IConfigurationIODefs.RF_PROFILE_HEADER_END);
		
	}

	/**
	 * @param configWriter
	 * @param channelInfo
	 */
	private void saveChannelInfo(ConfigurationTextWriter configWriter, IChannelInfo channelInfo) {
		
		if(configWriter == null) {
			return;
		}
		if(channelInfo == null) {
			return;
		}
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_CHANNEL_INFO_START);
		
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_NUMBER,""+channelInfo.getChannelNumber());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_CENTER_FREQUENCY,""+channelInfo.getFrequency());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_FLAGS,""+channelInfo.getPrivateChannelFlags());
				
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_CHANNEL_INFO_END);
	}

	/**
	 * @param ctl
	 * @return
	 */
/*	
	private String getConformanceName(short ctl) {
		
		switch(ctl) {
		
			case RfConfigPage.NONE_VALUE:
				return RfConfigPage.NONE;
			
			case RfConfigPage.FCC_VALUE:
				return RfConfigPage.FCC;
			
			case RfConfigPage.ETSI_VALUE:
				return RfConfigPage.ETSI;
			
			case RfConfigPage.MKK_VALUE:
				return RfConfigPage.MKK;			
		}
		return RfConfigPage.NONE;
	}
*/
	
	/**
	 * @param mediumSubType
	 * @return
	 */
	private String getMediumSubTypeName(short mediumSubType) {
		
		switch(mediumSubType) {

		case Mesh.PHY_SUB_TYPE_802_11_A:
			return PHY_SUB_TYPE_802_11_A;
		
		case Mesh.PHY_SUB_TYPE_802_11_B:
			return PHY_SUB_TYPE_802_11_B;
		
		case Mesh.PHY_SUB_TYPE_802_11_B_G:
			return PHY_SUB_TYPE_802_11_B_G;
		
		case Mesh.PHY_SUB_TYPE_802_11_G:
			return PHY_SUB_TYPE_802_11_G;
		
		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ:
			return PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ;
		
		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ:
			return PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ;
		
		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ:
			return PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ;
				
		}
		return PHY_SUB_TYPE_INGNORE;
	}

	
	/**
	 * @param profileName
	 * @param mediumSubType
	 */
	public RFProfile.Profile addNewProfile(String profileName, short mediumSubType) {
		
		Profile	newProfile			= new Profile();
		
		newProfile.mediumSubType	= mediumSubType;
		newProfile.profileName		= profileName;
		
		vectProfiles.add(newProfile);
		return newProfile;
	}

	/**
	 * @param profName
	 */
	public void deleteProfile(String profName) {
		for(int i = 0; i < vectProfiles.size(); i++) {
			
			Profile	profile	= (Profile) vectProfiles.get(i);
			if(profile == null) {
				continue;
			}
			if(profile.profileName.equals(profName)) {
				vectProfiles.remove(i);
				break;
			}
		}		
	}

}
