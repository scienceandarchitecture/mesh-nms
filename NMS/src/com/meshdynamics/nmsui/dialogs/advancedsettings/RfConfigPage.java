/**
 * MeshDynamics 
 * -------------- 
 * File     : RFConfigPage.java
 * Comments : 
 * Created  : Feb 22, 2007 
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * -----------------------------------------------------------------------------------
 * | 3  | Oct 31, 2007   | import profile added						  | Abhishek     |
 * -----------------------------------------------------------------------------------	
 * | 2  | May 02, 2007   | getDataFromUIToConfiguration added		  | Abhishek     |
 * -----------------------------------------------------------------------------------	
 * | 1  | Apr 05, 2007   | UI validation are added        			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 22, 2007   | Created                        			  | Abhishek     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.dialogs.advancedsettings;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.configuration.IChannelInfo;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.impl.ChannelInfo;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceCustomChannelConfig;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.util.ChannelListInfo;
import com.meshdynamics.meshviewer.util.CountryCodeComboHelper;
import com.meshdynamics.meshviewer.util.CountryData;
import com.meshdynamics.meshviewer.util.ValidFreqChecker;
import com.meshdynamics.nmsui.Branding;
import com.meshdynamics.nmsui.PortInfo;
import com.meshdynamics.nmsui.PortModelInfo;
import com.meshdynamics.nmsui.controls.CountryCodeCombo;
import com.meshdynamics.nmsui.dialogs.base.ConfigPage;
import com.meshdynamics.nmsui.dialogs.base.IMeshConfigBook;
import com.meshdynamics.nmsui.dialogs.frames.portmodeinfo.IPortModelInfoListener;
import com.meshdynamics.nmsui.dialogs.frames.portmodeinfo.PortModelInfoFrame;
import com.meshdynamics.nmsui.dialogs.frames.portmodeinfo.PortModelInfoSelectionEvent;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.MacAddress;



public class RfConfigPage extends ConfigPage implements IPortModelInfoListener {
	
	private 	Button					btnSelectCustomCountry;
	private 	Button					btnAdd;
	private 	Button					btnRemove;
	private 	Button 					btnViewValidChannels;
	private 	Button					btnImportProfile;
	private 	Button[]				btnChannelWidth;
	
	private 	Label					lblChannelWidth;
	private 	Label					lblConformance;
	private		Label					lblMaxPower;
	private 	Label					lblChannels;
	private 	Label					lblDbm;	
	private 	Label					lblDbi;
	private 	Label					lblNote;
	private 	Label					lblSelectCountry;
	
	private 	Text					txtMaxPower;
	private 	Text					txtMaxAntennaGain;
	
	private 	Group					grp;
	private 	Group					grp1;
	private     Group					grpCountryCode;	
	private		Group					grpNote;
	
	private 	Combo					cmbConformance;
	
	private 	Table					tblChannels;
	
	private 	int						rel_x;
	private     int 					rel_y;
	
	private 	CountryCodeCombo		cmbCountryCode;
	private 	PortModelInfoFrame		portModelFrame;
	private 	PortModelInfo			pmInfo;
	private 	IConfiguration 			iConfiguration;
	
	public static final String				NONE						= "NONE";
	public static final String				FCC	    					= "FCC";
	public static final String				ETSI						= "ETSI";
	public static final String				MKK 						= "MKK";
	
	public static final short 				NONE_VALUE					= 0;
	public static final short 				FCC_VALUE					= 1;
	public static final short 				ETSI_VALUE					= 2;
	public static final short 				MKK_VALUE					= 3;
	
	public static final String				YES 						= "Yes";
	public static final String				NO  						= "No";
	
	public static final short 				BTN_BANDWIDTH_5_MHZ			= 0;
	public static final short 				BTN_BANDWIDTH_10_MHZ		= 1;
	public static final short 				BTN_BANDWIDTH_20_MHZ		= 2;
	public static final short 				BTN_BANDWIDTH_40_MHZ		= 3;
	
	public static final short 				CHANNEL_BANDWIDTH_5_MHZ		= 5;
	public static final short 				CHANNEL_BANDWIDTH_10_MHZ	= 10;
	public static final short 				CHANNEL_BANDWIDTH_20_MHZ	= 20;
	public static final short 				CHANNEL_BANDWIDTH_40_MHZ	= 40;
	
	public final short 						DEFAULT_ANTENNA_POWER		= 30;
	
	private 	boolean						initBtnPrivChannelState;
	private 	CountryCodeComboHelper		cmbCountryCodeHelper;

	public RfConfigPage(IMeshConfigBook parent) {
		
		super(parent);
		
		minimumBounds.width 	= 435;
		minimumBounds.height	= 540;
		
		iConfiguration			= parent.getConfiguration();
		cmbCountryCodeHelper	= new CountryCodeComboHelper();
		
		initBtnPrivChannelState = false;
		
		setComboHelperValues();
	}
	
	

	/**
	 * 
	 */
	private void setComboHelperValues() {
		
		cmbCountryCodeHelper.setCountryCode(Mesh.DEFAULT_COUNTRY_CODE);
		cmbCountryCodeHelper.setHardwareModel(iConfiguration.getHardwareModel());
		cmbCountryCodeHelper.setRegDomain(iConfiguration.getRegulatoryDomain());
		cmbCountryCodeHelper.setCountryCodeChanged(false);
		
	}
	
	/**
	 * Function for creating various controls for the RF Config Page
	 */
	public void createControls(CTabFolder tabFolder) {
		
		super.createControls(tabFolder);

		rel_x = 10;
		rel_y = 10;
		
		createPrivateSelectionControls();
		
		rel_y = 35;
		
		createPortFrameControls();
		
		rel_y += portModelFrame.getMinimumBounds().height + 10;
		
		createRfChannelControls();
	
		grpCountryCode.setVisible(false);
		grpNote.setVisible(true);
		
		ContextHelp.addContextHelpHandlerEx(this.canvas,contextHelpEnum.ADVCONFIGTABRFEDITOR);
		
		if(iConfiguration.getCountryCode() == Mesh.CUSTOM_COUNTRY_CODE) {
			
			portModelFrame.setVisible(true);
			grp.setVisible(true);
			btnSelectCustomCountry.setSelection(true);
			
		}else {
			
			portModelFrame.setVisible(false);
			grp.setVisible(false);
			btnSelectCustomCountry.setSelection(false);
		}
	}

	
	/**
	 * 
	 */
	private void createPrivateSelectionControls() {
		
		btnSelectCustomCountry	= new Button(canvas, SWT.CHECK);
		btnSelectCustomCountry.setBounds(rel_x,rel_y,250,25);
		btnSelectCustomCountry.setText("Enable Custom Channel Configuration ");
		btnSelectCustomCountry.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				
				if(btnSelectCustomCountry.getSelection() == true){
					
					int choice	= parent.showMessage("Do you want enable private channels ?" ,SWT.YES|SWT.NO);
					if(choice == SWT.YES){
						
						if(verifyCountryCodeXML() == false) {
							int ans = parent.showMessage("On enabling private channels, you will no longer be able " +SWT.CR+
														 "to change country code. "+SWT.CR+
														 "Do you still want to continue?" ,SWT.YES|SWT.NO);
							if(ans == SWT.NO) {
								btnSelectCustomCountry.setSelection(false);
								return;
							}
							
						}
						portModelFrame.setVisible(true);
						grp.setVisible(true);
						grpCountryCode.setVisible(false);
						grpNote.setVisible(false);
						btnSelectCustomCountry.setSelection(true);
						
						
					}else {
						
						portModelFrame.setVisible(false);
						grp.setVisible(false);
						btnSelectCustomCountry.setSelection(false);
						
					}
				}
				if(btnSelectCustomCountry.getSelection() == false){
						
						if(verifyCountryCodeXML() == false) {
							parent.showMessage("You cannot change the country code."+SWT.CR+
											   "Contact "+Branding.getBrandTitle()+" Technical Support Team to resolve the problem.",SWT.OK);
							btnSelectCustomCountry.setSelection(true);
							return;
						}
						
						portModelFrame.setVisible(false);
						grp.setVisible(false);
						grpNote.setVisible(true);
						
						if(iConfiguration.getCountryCode() == Mesh.CUSTOM_COUNTRY_CODE) {
							
							cmbCountryCodeHelper.setCountryCodeChanged(true);
							grpCountryCode.setVisible(true);
							grpNote.setVisible(false);
							cmbCountryCode.setCountryCodeData(Mesh.DEFAULT_COUNTRY_CODE);
							
						}else if(cmbCountryCodeHelper.isCountryCodeChanged() == true) {
							grpCountryCode.setVisible(true);
							grpNote.setVisible(false);
						}
				   }
			}
		});
		
		rel_y += 35; 
		
		grpCountryCode	= new Group(canvas,SWT.NONE);
		grpCountryCode.setBounds(rel_x,rel_y,425,75);
		
		rel_x	= 25;
		rel_y	= 25;
		
		String strMsg	 = "Country code";
		
		lblSelectCountry = new Label(grpCountryCode, SWT.NO);
		lblSelectCountry.setBounds(rel_x,rel_y,150,15);
		lblSelectCountry.setText(strMsg);
		
		cmbCountryCode	= new CountryCodeCombo(grpCountryCode,SWT.BORDER|SWT.READ_ONLY,cmbCountryCodeHelper);
		cmbCountryCode.setBounds(rel_x +230,rel_y,150,17);
		
		rel_x	= 10;
		rel_y	= 35;
		
		grpNote			= new Group(canvas, SWT.NO);
		grpNote.setBounds(rel_x,rel_y,425,490);
		
		lblNote			= new Label(grpNote, SWT.NO);
		lblNote.setBounds(rel_x,rel_y,405,180);
		
		String 	strNote	= "MeshDynamics provides the RF Editor capability \"as is\" without warranty of any kind,\n" + 
						  "either expressed or implied, including, but not limited to, the implied warranties\n" +
						  "of merchantability and fitness for a particular purpose. The entire risk as to the \n" +
						  "quality,legality, and performance of RF editor is with the user. Should the RF Editor\n" + 
						  "prove defective, or cause MeshDynamics nodes to generate RF in an unavailable or \n" + 
						  "illegal frequency in a particular region, the user will assume the cost of all \n" +
						  "necessary repair, correction, or legal action.\n";
		
		lblNote.setText(strNote);
		
		rel_x = 10;
		rel_y = 10;
	
	}


	/**
	 * @return
	 */
	protected boolean verifyCountryCodeXML() {
		
		Vector<ChannelListInfo> temp	 = cmbCountryCode.getChannelList();
		if(temp == null || temp.size() == 0) {
			return false;	
		}
		return true;
	}



	/**
	 *   Function to create RF Channel Controls
	 */
	private void createRfChannelControls() {
		
		grp = new Group(canvas,SWT.NO);
		grp.setBounds(rel_x,rel_y,425,360);
		
		rel_y = 25;
		rel_x = 25;
		
		lblChannelWidth	= new Label(grp,SWT.NO);
		lblChannelWidth.setText("Channel Width");
		lblChannelWidth.setBounds(rel_x,rel_y,80,20);
		
		rel_x = rel_x + 100;
		
		btnChannelWidth	= new Button[pmInfo.getNumOfRadioSlots()];
		
		for(int i=0; i<pmInfo.getNumOfRadioSlots() ;i++)
		{
			btnChannelWidth[i] 	= new Button(grp,SWT.RADIO);
			btnChannelWidth[i].setBounds(rel_x,rel_y,60,20);
			rel_x = rel_x + 70;
		}
		
		btnChannelWidth[0].setText("5 MHz");
		btnChannelWidth[1].setText("10 MHz");
		btnChannelWidth[2].setText("20 MHz");
		btnChannelWidth[3].setText("40 MHz");
		
		btnChannelWidth[2].setSelection(true);		//Default selection 
		
		rel_x = 25;
		rel_y = rel_y + 30;
		
		lblConformance	= new Label(grp,SWT.NONE);
		lblConformance.setBounds(rel_x,rel_y,100,20);
		lblConformance.setText("Conformance");
				
		rel_x = rel_x + 100;
		
		cmbConformance	= new Combo(grp,SWT.BORDER|SWT.READ_ONLY); 
		cmbConformance.setBounds(rel_x,rel_y,100,20);
		
		cmbConformance.add(NONE);
		cmbConformance.add(FCC);
		cmbConformance.add(ETSI);
		cmbConformance.add(MKK);
		cmbConformance.select(0);		
		
		rel_x = 25;
		rel_y = rel_y + 30;
		
		lblMaxPower	= new Label(grp,SWT.NONE);
		lblMaxPower.setBounds(rel_x,rel_y,75,20);
		lblMaxPower.setText("Max Power");
		
		rel_x = rel_x + 100;
		
		txtMaxPower	= new Text(grp,SWT.BORDER);
		txtMaxPower.setBounds(rel_x,rel_y,50,20);
		
		rel_x = rel_x + 55;

		lblDbm	= new Label(grp,SWT.NONE);
		lblDbm.setBounds(rel_x,rel_y+3,25,20);
		lblDbm.setText("dBm");				
				
		rel_x = rel_x + 40;
			
		lblMaxPower	= new Label(grp,SWT.NONE);
		lblMaxPower.setBounds(rel_x,rel_y,90,20);
		lblMaxPower.setText("Max Antenna Gain");
		
		rel_x = rel_x + 95;
		
		txtMaxAntennaGain	= new Text(grp,SWT.BORDER);
		txtMaxAntennaGain.setBounds(rel_x,rel_y,50,20);
		
		rel_x = rel_x + 55;
		
		lblDbi	= new Label(grp,SWT.NONE);
		lblDbi.setBounds(rel_x,rel_y+3,15,20);
		lblDbi.setText("dBi");
		
		rel_x = 25;
		rel_y = rel_y + 35;

		grp1	= new Group(grp,SWT.NO);
		grp1.setBounds(rel_x-25,rel_y,429,2);

		rel_x = 25;
		rel_y = rel_y + 15;

		lblChannels	= new Label(grp,SWT.NONE);
		lblChannels.setBounds(rel_x,rel_y,100,20);
		lblChannels.setText("Channels Description");
	
		rel_x = 25;
		rel_y = rel_y + 25;
			
		tblChannels	= new Table(grp,SWT.V_SCROLL|SWT.BORDER|SWT.SINGLE|SWT.FULL_SELECTION);
		tblChannels.setBounds(rel_x,rel_y,304,176); 	
		tblChannels.setHeaderVisible(true);
		tblChannels.setLinesVisible(true);
		
		String Headers[]	= {"Channel","Center Freq","Occupied Range","DFS"};
		
		TableColumn col;
		int i;
		
		for(i = 0; i < Headers.length; i++ )
		{
			col	=	new TableColumn(tblChannels, SWT.NONE);
			col.setText(Headers[i]);
		}
		
		col	= tblChannels.getColumn(0);
		col.setWidth(55);
		col = tblChannels.getColumn(1);
		col.setWidth(75);
		col = tblChannels.getColumn(2);
		col.setWidth(120);
		col = tblChannels.getColumn(3);
		col.setWidth(50);

		rel_x = rel_x + 315;
		rel_y += 60;
		btnAdd	= new Button(grp,SWT.PUSH);
		btnAdd.setText("Add");
		btnAdd.setBounds(rel_x,rel_y,60,20);
		btnAdd.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				addCustomChannels();
			}
		}); 
		
		rel_y = rel_y + 25;
		
		btnRemove	= new Button(grp,SWT.PUSH);
		btnRemove.setText("Remove");
		btnRemove.setBounds(rel_x,rel_y,60,20);
		btnRemove.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				 removeFromChannelsTable();				// Removing the selected entry from the channels table
			}
		});
		
		rel_y = rel_y + 25;
		
		btnViewValidChannels	= new Button(grp,SWT.PUSH);
		btnViewValidChannels.setText("Valid Freq");
		btnViewValidChannels.setBounds(rel_x,rel_y,60,20);
		btnViewValidChannels.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				viewValidFrequencies();
			}
			});
		
		
		rel_y = rel_y + 45;
		
		btnImportProfile	= new Button(grp,SWT.PUSH);
		btnImportProfile.setText("Import Profile");
		btnImportProfile.setBounds(rel_x,rel_y,75,20);
		btnImportProfile.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
					importProfile();
				}
			});
		grp.setVisible(true);
		
		ContextHelp.addContextHelpHandlerEx(canvas,contextHelpEnum.ADVCONFIGTABRFEDITOR);
	}
	

	/**
	 * 
	 */
	protected void importProfile() {
	    
	    RFProfile          rfProfiles;
	    RFProfile.Profile  profile;
		
		FileDialog fileDlg	= new FileDialog(new Shell());
		fileDlg.setFilterPath(MFile.getConfigPath());
		fileDlg.open();
		
		String fileName 	= null;
		fileName 			= fileDlg.getFileName();
		
		if (fileName.equals("") || fileName == null) {
			return;
		}

		fileName	= fileDlg.getFilterPath() + "\\" + fileName;
		
		/**
		 * Load the file and always take the first profile.
		 */
		
		rfProfiles = new RFProfile(fileName);
		
		profile    = rfProfiles.getProfile(0);
		
		if(profile == null) {
		    		    
		    parent.showMessage("The file you selected does not contain any Meshdynamics RF-Editor profiles.", SWT.OK);
		    
		    return;		    
		}

		if(checkForValidProfile(profile) == false) {
			return;
		}
		
		int chCount	= profile.customChannelConfig.getChannelInfoCount();
		
		if(chCount > 32) {
		    
            parent.showMessage("There is a limit of 32 channels per radio interface. Only the first 32 channels of the selected profile will be impoted.", SWT.OK);
		    
		}
		
		for(int i = 0; i < chCount && i < 32; i++) {
		
			IChannelInfo profileChannelInfo = (IChannelInfo)profile.customChannelConfig.getChannelInfo(i);

			if(checkRepeatedValues(profileChannelInfo) == false) {
				continue;
			}
			setTableValues(profileChannelInfo);
		}
		
	}



	/**
	 * @param profile
	 * 
	 */
	private boolean checkForValidProfile(RFProfile.Profile profile) {
		
		String 			selectedIfName	= portModelFrame.getLastSelectedRadioName();
		InterfaceInfo	ifInfoObject	= (InterfaceInfo)portModelFrame.getRadiodataByName(selectedIfName);
		
		if(ifInfoObject	== null) {
			return false;
		}
		
		short profileMediumSubType	= profile.mediumSubType;
		short ifMediumSubType		= ifInfoObject.getMediumSubType();
		
		if(ifMediumSubType == Mesh.PHY_SUB_TYPE_802_11_A &
		   profileMediumSubType != Mesh.PHY_SUB_TYPE_802_11_A) {
		
			parent.showMessage("Selected profile "+profile.profileName+" is not a valid profile for the current interface.",SWT.ICON_INFORMATION| SWT.OK);
			return false;
			
		}
		if((ifMediumSubType == Mesh.PHY_SUB_TYPE_802_11_G   ||
			ifMediumSubType == Mesh.PHY_SUB_TYPE_802_11_B_G ||
			ifMediumSubType == Mesh.PHY_SUB_TYPE_802_11_B )
			&&
			(profileMediumSubType == Mesh.PHY_SUB_TYPE_802_11_A 				  ||	
			profileMediumSubType  == Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ ||
			profileMediumSubType  == Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ ||
			profileMediumSubType  == Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ)) {
			
			parent.showMessage("Selected profile "+profile.profileName+" is not a valid profile for the current interface.",SWT.ICON_INFORMATION| SWT.OK);
			return false;
						
		}
		ValidFreqChecker validFreqChecker = new ValidFreqChecker();
		validFreqChecker.updateValidFreq(profileMediumSubType, getChannelWidth());
		int 			 chCount		  = profile.customChannelConfig.getChannelInfoCount();
		
		for(int i = 0; i < chCount; i++) {
			IChannelInfo chInfo	= (IChannelInfo)profile.customChannelConfig.getChannelInfo(i);
			if(chInfo == null) {
				continue;
			}
			int chFreq	= (int)chInfo.getFrequency();
			if(validFreqChecker.isValidFrequency(chFreq) == false) {
				
				parent.showMessage("Frequency "+chFreq+" is not valid for selected interface.",SWT.ICON_INFORMATION| SWT.OK);
				return false;
			}
		}
		return true;
	}

	


	private void viewValidFrequencies() {
		
		String 			selectedIfName	= portModelFrame.getLastSelectedRadioName();
		InterfaceInfo	ifInfoObject	= (InterfaceInfo)portModelFrame.getRadiodataByName(selectedIfName);
		if(ifInfoObject	== null) {
			return;
		}
		InterfaceCustomChannelConfig	customConfig	= (InterfaceCustomChannelConfig)ifInfoObject.getCustomChannelConfiguration();
		short   						width			= customConfig.getChannelBandwidth();
		short 							phySubType		= ifInfoObject.getMediumSubType();
		String 							strRadioType 	= portModelFrame.getPortInfo(selectedIfName).freqOfRadio ;
		
		ValidChannelFrequencyDlg 	dlg 			= new ValidChannelFrequencyDlg(phySubType,width);
		dlg.setTitle("Valid Frequencies for "+strRadioType);
		dlg.show();
	}
	
	/**
	 *   Function to create PortModelFrame
	 */
	private void createPortFrameControls() {
		
		pmInfo						= new PortModelInfo(iConfiguration.getHardwareModel(), 
														iConfiguration.getDSMacAddress().toString(),
														iConfiguration.getInterfaceConfiguration());
		
		portModelFrame 				= new PortModelInfoFrame(canvas,SWT.NONE,pmInfo,this);
		Rectangle	temp			= portModelFrame.getMinimumBounds();
		portModelFrame.setBounds(rel_x,rel_y,temp.width,temp.height);
	}

	
	public void selectionChanged(boolean selected) {
		
		String	selectedIfName	= portModelFrame.getLastSelectedRadioName();
		
		if(selected	== true) {
			setInterfaceValues(selectedIfName);
			canvas.setFocus();
		} else {
			if(validateLocalData() == false){
				stayOnSameTab();
				return;
			}
			if(comparePrivateChannelSelection() == false ||
			   compareUIwithLocalConfig() 		== false){
		
				int result	= parent.showMessage("Do you want to save the changes ? ",SWT.YES|SWT.CANCEL|SWT.NO);
				if(result == SWT.CANCEL) {
					stayOnSameTab();
					return;
				}else if(result	== SWT.NO) {
					initalizeLocalData();					
				}else if(result	== SWT.YES) {
					getDataFromUIToConfiguration();
				}	
			}
		}
	}

	
	public void setModified() {
	}

	
	public void initalizeLocalData() {
	
		int i;
		int 					radioCount		= portModelFrame.getRadioCount();
		IInterfaceConfiguration	interfaceConfig	= iConfiguration.getInterfaceConfiguration();
		
		btnSelectCustomCountry.setSelection(initBtnPrivChannelState);
		if(initBtnPrivChannelState == false) {
			grp.setVisible(false);
			grpNote.setVisible(true);
		}
		
		if(iConfiguration.getCountryCode() == Mesh.CUSTOM_COUNTRY_CODE) {
			
			btnSelectCustomCountry.setSelection(true);
			initBtnPrivChannelState = true;
			grpCountryCode.setVisible(false);
			grpNote.setVisible(false);
			grp.setVisible(true);
			portModelFrame.setVisible(true);
			
		}else {
			cmbCountryCode.setCountryCodeData(iConfiguration.getCountryCode());
		}
		
		for(i=0; i < radioCount; i++) {
			
			PortInfo	portInfo	= portModelFrame.getPortInfo(i);
			
			if(portInfo.typeOfRadio == PortModelInfo.TYPENOTUSED) {
				continue;
			}
			String			ifName			= portInfo.ifName;
			IInterfaceInfo	interfaceInfo	= (IInterfaceInfo)interfaceConfig.getInterfaceByName(ifName);
			if(interfaceInfo == null) {
				continue;
			}
			portModelFrame.setRadioDataByName(ifName,interfaceInfo);
		}
		setInterfaceValues(portModelFrame.getLastSelectedRadioName());
	}

	
	/**
	 * Clearing the values of RF Editor tab
	 */
	public void clearAllValues() {
	
		txtMaxPower.setText(""+DEFAULT_ANTENNA_POWER);
		txtMaxAntennaGain.setText("0");
		cmbConformance.select(0);
		tblChannels.removeAll(); 
		defaultBtnSelection(2);
	}

	/**
	 *  Function to setting values in Rf editor tab
	 *    
	 */
	public void setInterfaceValues(String ifName) {
		
		clearAllValues(); 
		if(ifName == null || ifName.equals("")) {
			
			InterfaceInfo info	= (InterfaceInfo) portModelFrame.
								   getRadioDataByIndex(portModelFrame.getCurrentSelectedRadioIndex());
			ifName				= info.getName();
		}
		
		InterfaceInfo	ifInfoObject	= (InterfaceInfo)portModelFrame.getRadiodataByName(ifName);
		
		if(ifInfoObject	== null) {
			return;
		}
		MacAddress ifMacAddress	= ifInfoObject.getMacAddress();
    	String msg	= "Settings for ("+ifName +") - ";
    	
    	if(ifMacAddress.equals(MacAddress.resetAddress)){
    		msg = msg + "(Inactive)";	// For roots
    	}else {
    		msg = msg +  ifMacAddress;
    	}
    	grp.setText(msg);
    	
		InterfaceCustomChannelConfig	customConfig	= (InterfaceCustomChannelConfig)ifInfoObject.getCustomChannelConfiguration();
		
		short   width	= customConfig.getChannelBandwidth();
		boolean flag 	= false;
		
		if(customConfig.getChannelInfoCount() == 0) {
			flag = true;
		}
		int j;
		for(j = 0; j < 4 ; j++)
		{
			btnChannelWidth[j].setSelection(false);
			btnChannelWidth[j].setEnabled(flag); 
		}
		/*If phy type is B then enable 20mhz bandwidth only*/
		if(ifInfoObject.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_B) {
			defaultBtnSelection(2); //20 mhz
		}
		else if(width == CHANNEL_BANDWIDTH_5_MHZ) {
		
			btnChannelWidth[0].setSelection(true);
			btnChannelWidth[0].setEnabled(true);
			
		}else if(width == CHANNEL_BANDWIDTH_10_MHZ) {
			
			btnChannelWidth[1].setSelection(true); 
			btnChannelWidth[1].setEnabled(true);
			
		}else if(width == CHANNEL_BANDWIDTH_20_MHZ) {
			
			btnChannelWidth[2].setSelection(true);
			btnChannelWidth[2].setEnabled(true);
			
		}else if(width == CHANNEL_BANDWIDTH_40_MHZ) {
			
			btnChannelWidth[3].setSelection(true);
			btnChannelWidth[3].setEnabled(true);
			
		}else{
			defaultBtnSelection(2);// 20 MHz selection
	 }
		
		cmbConformance.select(customConfig.getCtl());
		txtMaxPower.setText(""+customConfig.getAntennaPower());
		txtMaxAntennaGain.setText(""+customConfig.getAntennaGain());
	
		int channelCount = customConfig.getChannelInfoCount(); 
		
		for(int i =0;i<channelCount;i++)
		{
			IChannelInfo	channelInfo	= (IChannelInfo)customConfig.getChannelInfo(i);
			setTableValues(channelInfo);
		}
	}
	
	
	/**
	 *  Function to get channel bandwidth 
	 */
	private short getChannelWidth()
	{
		if(btnChannelWidth[0].getSelection() == true){
			return CHANNEL_BANDWIDTH_5_MHZ ;
		}
		if(btnChannelWidth[1].getSelection() == true){
			return CHANNEL_BANDWIDTH_10_MHZ;
		}
		if(btnChannelWidth[2].getSelection() == true){
			return CHANNEL_BANDWIDTH_20_MHZ;
		}
		if(btnChannelWidth[3].getSelection() == true){
			return CHANNEL_BANDWIDTH_40_MHZ;
		}
		return 0;
	}

	
	/**
	 * Setting the values of channel in table
	 */
	private void setTableValues(IChannelInfo channelInfo) {
		
		TableItem newItem;
		
		newItem 				= new TableItem(tblChannels,SWT.NONE);
		
		short 	channelNo    	= channelInfo.getChannelNumber();
		long  	channelFreq  	= channelInfo.getFrequency();
		short 	chPriFlag		= channelInfo.getPrivateChannelFlags();
		String 	dfsFlag 		= NO;

		if(chPriFlag  == 1) {
			dfsFlag	   =  YES;
		}

		newItem.setText(0,""+channelNo);
		newItem.setText(1,""+channelFreq);
		newItem.setText(3,""+dfsFlag);

		try
		{
			float channelWidth = getChannelWidth();  
			float upperVal=0,lowerVal=0;
			
			upperVal = channelFreq + (channelWidth/2);
			lowerVal = channelFreq - (channelWidth/2);
			
			newItem.setText(2," "+lowerVal+" to "+upperVal);
			
			if(tblChannels.getItemCount() > 0) {
				
				for(int i = 0 ; i < 4; i++) {
					btnChannelWidth[i].setEnabled(false);
					btnChannelWidth[i].setSelection(false);
				}
				int pos	= getBtnChannelWidthPosition((short)channelWidth);
				btnChannelWidth[pos].setSelection(true);
				btnChannelWidth[pos].setEnabled(true);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}
	}

	/**
	 * Function to set default button for channel width 
	 */
	private void defaultBtnSelection(int position){
		
		int j;
		for(j=0; j < 4 ; j++)
		{
			btnChannelWidth[j].setSelection(false);
			btnChannelWidth[j].setEnabled(false); 
		}
		
		btnChannelWidth[position].setSelection(true);
		btnChannelWidth[position].setEnabled(true); 
	}

	/**
	 * 
	 */
	public void radioSelected(PortModelInfoSelectionEvent event) {
	
		if(validateLocalData() == false){
			return;
		}
		if(compareUIwithLocalConfig() == false){
			
			int result	= parent.showMessage("Do you want to save the changes ? ",SWT.YES|SWT.CANCEL|SWT.NO);
			
			if(result == SWT.CANCEL) {
				
				portModelFrame.setRadioSelection(portModelFrame.getLastSelectedRadioIndex(),true);
				portModelFrame.setRadioSelection(event.getRadioIndex(),false);
				return;
				
			}else if(result	== SWT.NO) {
			
				setInterfaceValues(portModelFrame.getLastSelectedRadioName());
				
			}else if(result	== SWT.YES) {
				
				getDataFromUIToConfiguration();
			}	
		}
		setInterfaceValues(event.getRadioInfo().ifName);
	}
	
	
	/**
	 * @param customConfig
	 * @param interfaceInfo
	 */
	private boolean compareChannelInfo(InterfaceCustomChannelConfig customConfig) {
		
		int i					= 0;
		int	j					= 0;
		int configChangedFlag	= 0;
		
		int itemCount			= tblChannels.getItemCount();
		int customChCount		= customConfig.getChannelInfoCount();
		
		if(customChCount != itemCount) {
			return false;
		}
		
		for(i = 0; i < itemCount; i++) {
			
			TableItem item	= tblChannels.getItem(i);
			String	chNo	= item.getText(0).trim();
			String 	chFreq	= item.getText(1).trim();
			String	chDfs	= item.getText(3).trim();
			
			try{
				short channelNo		= Short.parseShort(chNo);
				long  channelFreq 	= Long.parseLong(chFreq);
				short channelDfs	= 0;
				
				if(chDfs.equals(YES)) {
					channelDfs	= 1;
				}
				
				for(j = 0; j < customChCount; j++) {
					
					IChannelInfo customChInfo	= (IChannelInfo)customConfig.getChannelInfo(j);
					if(customChInfo == null) {
						continue;
					}
					
					if(customChInfo.getChannelNumber() 		== channelNo		&&
					   customChInfo.getFrequency()	   		== channelFreq 		&&
					   customChInfo.getPrivateChannelFlags()== channelDfs) {
						
						configChangedFlag++;
						break;
					}
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		if(configChangedFlag != customChCount) {
			return false;
		}
		return true;
	}

	private boolean comparePrivateChannelSelection(){
		if(btnSelectCustomCountry.getSelection() != initBtnPrivChannelState) {
			return false;
		}
		return true;
	}
	/**
	 * @return
	 */
	private boolean compareUIwithLocalConfig() {
		
		if(grpCountryCode.getVisible() == true) {
			
			if(iConfiguration.getCountryCode() != cmbCountryCode.getCountryCode()) {
				cmbCountryCodeHelper.setCountryCodeChanged(true);
				return false;
			}
		}
		String			selectedIfName	= portModelFrame.getLastSelectedRadioName();
		IInterfaceInfo	interfaceInfo 	= null;
		interfaceInfo					= (IInterfaceInfo)portModelFrame.getRadiodataByName(selectedIfName);
		
		if(interfaceInfo == null) {
			return false;
		}
		InterfaceCustomChannelConfig customConfig	= (InterfaceCustomChannelConfig)interfaceInfo.getCustomChannelConfiguration();
		if(customConfig == null) {
			return false;
		}
		try{
			int antGain	= Integer.parseInt(txtMaxAntennaGain.getText().trim());
			if(customConfig.getAntennaGain() != antGain) {
				return false;
			}
			int antPower = Integer.parseInt(txtMaxPower.getText().trim());
			if(customConfig.getAntennaPower() != antPower) {
				return false;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		if(customConfig.getCtl() != cmbConformance.getSelectionIndex()) {
			return false;
		}
		if(customConfig.getChannelBandwidth() != getChannelWidth()) {
			return false;
		}
		if(compareChannelInfo(customConfig) == false){
			return false;
		}
		return true;
	}
	


	/**
	 * Function to check the repeated values before adding to the table
	 * checks for the channel number, channel frequency. 
	 */
	private boolean checkRepeatedValues(IChannelInfo channelInfo) 
	{
		TableItem item;
		
		short 	channelNo    	= channelInfo.getChannelNumber();
		long  	channelFreq  	= channelInfo.getFrequency();
		
		for(int i=0; i < tblChannels.getItemCount();i++)
		{
			item = tblChannels.getItem(i);
			if(Short.parseShort(item.getText(0).trim())	== 	channelNo)
			{
				btnAdd.setFocus();
				parent.showMessage("Channel Number "+channelNo+" already present. Cannot be added. ",SWT.OK|SWT.ICON_ERROR);
				return false;
			}
			if(Integer.parseInt(item.getText(1).trim())	==	channelFreq)
			{
				btnAdd.setFocus();
				parent.showMessage("Channel Frequency "+channelFreq+" already present. Cannot be added.",SWT.OK|SWT.ICON_ERROR);
				return false;
			}
		}
		return true;
	}
	
	/*
	 * Adding custom channels to Table
	 */
	private void addCustomChannels(){
		
		String					selectedIfName	= portModelFrame.getLastSelectedRadioName();				
		String 				   	strRadioType	= portModelFrame.getPortInfo(selectedIfName).freqOfRadio ;
		AddCustomChannelDialog	objRFChannel	= new AddCustomChannelDialog();
		
		objRFChannel.setRadioType(strRadioType); 		// setting radiotype for adding channels to 
														// selected interface and to check the center frequency
		objRFChannel.setTitle("Add Channel for "+strRadioType);
		objRFChannel.setSelectedChannelBandWidth(getChannelWidth()); 
		objRFChannel.show();
		
		if(objRFChannel.getStatus()!= SWT.OK)
			return;
		
		short chPriFlag = 0;
		short chNo   	= objRFChannel.getChannelNo();
		long  chFreq 	= objRFChannel.getChannelFreq();
		chPriFlag		= (short)(chPriFlag | (objRFChannel.getPrivateChannelFlag()));
		
		IChannelInfo uiChannelInfo	=	new ChannelInfo();
		uiChannelInfo.setChannelNumber(chNo);
		uiChannelInfo.setFrequency(chFreq);
		uiChannelInfo.setPrivateChannelFlags(chPriFlag);
		
		if(checkRepeatedValues(uiChannelInfo) == false) {
			return;
		}
		setTableValues(uiChannelInfo);
	}

	/**
	 * @return Returns the btn position.
	 */
	private short getBtnChannelWidthPosition(short width)
	{
		if(width == Mesh.CHANNEL_BANDWIDTH_5_MHZ)
			return BTN_BANDWIDTH_5_MHZ;
		else if(width == Mesh.CHANNEL_BANDWIDTH_10_MHZ)
			return BTN_BANDWIDTH_10_MHZ;
		else if(width == Mesh.CHANNEL_BANDWIDTH_20_MHZ)
			return BTN_BANDWIDTH_20_MHZ;
		else if(width == Mesh.CHANNEL_BANDWIDTH_40_MHZ)
			return BTN_BANDWIDTH_40_MHZ;
		else 
			return BTN_BANDWIDTH_20_MHZ; // default position	
	}
	
	
	/**
	 * Function to remove the selected entry from the channels table
	 */
	private void removeFromChannelsTable() {
		
		int index = tblChannels.getSelectionIndex();
		if(index < 0)
			return ;
		
		TableItem item 	= tblChannels.getItem(tblChannels.getSelectionIndex());
			
		if(item != null) {
			item.dispose();
		}
		
		if(tblChannels.getItemCount() > 0){
			if(index > 0)
				tblChannels.setSelection(index - 1);
			else if(index == 0)
				tblChannels.setSelection(index);
		}
		else if(tblChannels.getItemCount() == 0){
			
			tblChannels.setSelection(-1);
			short width = getChannelWidth();  
			
			if(checkForPhySubtypeB() == false) {
				
				for(int i = 0 ; i < 4; i++) {
					btnChannelWidth[i].setEnabled(true);
					btnChannelWidth[i].setSelection(false);
				}
				btnChannelWidth[getBtnChannelWidthPosition(width)].setSelection(true);
			}
		}
		
	}

	/**
	 * @return
	 */
	private boolean checkForPhySubtypeB() {
		
		String 			radioName		= portModelFrame.getLastSelectedRadioName();	
		InterfaceInfo	ifInfoObject	= (InterfaceInfo)portModelFrame.getRadiodataByName(radioName);
		if(ifInfoObject	== null) {
			return false;
		}
		/*If phy type is B then enable 20mhz bwidth only*/
		if(ifInfoObject.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_B) {
			defaultBtnSelection(2); //20 mhz
			return true;
		}
		return false;
	}



	/**
	 * Validates Local Data from UI
	 */
	public boolean validateLocalData() {
		
		short maxPower,maxAntennaGain;
		
		try{
			maxPower	=	Short.parseShort(txtMaxPower.getText().trim());
		}catch(Exception e){
			
			parent.showMessage("Max power field should be numeric. ",SWT.ICON_ERROR);
			txtMaxPower.setFocus();  
			return false;
		}
		
		if(txtMaxPower.getText().trim().equals("")){
			
			parent.showMessage("Max power field is empty. ",SWT.ICON_ERROR);
			txtMaxPower.setFocus();  
			return false;
			
		}else if(maxPower < 0 || maxPower > 30 ){
			
			parent.showMessage("Max power is out of limit.\n" +
      						   "Enter the max power between 0 to 30.",SWT.ICON_ERROR);
			txtMaxPower.setFocus();  
			return false;
		}
		
		try{
			maxAntennaGain	=	Short.parseShort(txtMaxAntennaGain.getText().trim());
		}catch(Exception e){
			
			parent.showMessage("Max Antenna Gain field should be numeric.",SWT.ICON_ERROR);
			txtMaxAntennaGain.setFocus();  
			return false;
		}
		if(txtMaxAntennaGain.getText().trim().equals("")){
			parent.showMessage("Max Antenna Gain field is empty.",SWT.ICON_ERROR);
			txtMaxAntennaGain.setFocus();  
			return false;
		}else if(maxAntennaGain < 0 || maxAntennaGain > 36 ){
			parent.showMessage("Max Antenna Gain is out of limit. \n" +
							   "Enter the max antenna gain between 0 to 36.",SWT.ICON_ERROR);
			txtMaxAntennaGain.setFocus();  
			return false;
		}
		return true;
	}	
	
		
	
	/**
	 * Sets the UI data into Configuration
	 */
	public boolean getDataFromUIToConfiguration() {
		
		initBtnPrivChannelState = btnSelectCustomCountry.getSelection();
		
		if(btnSelectCustomCountry.getSelection() == false) {
			
			if(cmbCountryCodeHelper.isCountryCodeChanged() == true) {
				if(grpCountryCode.getVisible() == false) {
					return true;
				}
				int code	= cmbCountryCode.getCountryCode();
				updateRegDomainChanges(code);
				updateCountryCodeInformation();
			}
			return true;
		}
		if(btnSelectCustomCountry.getSelection() == true) {
			
			getCustomCountryCodeInformationFromUI();
			updateRegDomainChanges(Mesh.CUSTOM_COUNTRY_CODE);
			return true;
		}
		return true;
	}



	

	/**
	 * 
	 */
	private void getCustomCountryCodeInformationFromUI() {
		
		IInterfaceInfo	interfaceInfo 	= null;
		String			selectedIfName	= portModelFrame.getLastSelectedRadioName();
		interfaceInfo					= (IInterfaceInfo)portModelFrame.getRadiodataByName(selectedIfName);
		
		if(interfaceInfo == null) {
			return;
		}
		
		InterfaceCustomChannelConfig customConfig	= (InterfaceCustomChannelConfig)interfaceInfo.getCustomChannelConfiguration();
		
		if(customConfig == null) {
			return;
		}	
		
		try{
			
			customConfig.setChannelBandwidth(getChannelWidth());
			customConfig.setAntennaGain(Short.parseShort(txtMaxAntennaGain.getText().trim()));
			customConfig.setAntennaPower(Short.parseShort(txtMaxPower.getText().trim()));
			customConfig.setChannelInfoCount(tblChannels.getItemCount());
			
			if(cmbConformance.getSelectionIndex() < 0) {
				return;
			}
			customConfig.setCtl((short)cmbConformance.getSelectionIndex());
			
			int i = 0;
			for(i = 0; i < customConfig.getChannelInfoCount(); i++) {
			
				IChannelInfo custChInfo	= (IChannelInfo)customConfig.getChannelInfo(i);
				if(custChInfo == null) {
					continue;
				}
				TableItem item	= tblChannels.getItem(i);
				String	chNo	= item.getText(0).trim();
				String 	chFreq	= item.getText(1).trim();
				String	chDfs	= item.getText(3).trim();
				
				short channelNo		= Short.parseShort(chNo);
				long  channelFreq 	= Long.parseLong(chFreq);
				short channelDfs	= 0;
					
				if(chDfs.equals(YES)) {
					channelDfs	= 1;
				}
				custChInfo.setChannelNumber(channelNo);
				custChInfo.setFrequency(channelFreq);
				custChInfo.setPrivateChannelFlags(channelDfs);
				
			}
			int channelInfoCount	= customConfig.getChannelInfoCount();
			short[] dcaList = new short[channelInfoCount];
			
			for(i = 0; i < channelInfoCount; i++) {
				
	            IChannelInfo  ch = (IChannelInfo) customConfig.getChannelInfo(i);
				if(ch == null) { 
					continue;
				}
				dcaList[i]	= ch.getChannelNumber();
				
			}
			interfaceInfo.setDcaListCount((short)dcaList.length);
			setInterfaceDCAList(interfaceInfo,dcaList);
			
			/**
			 * For B or BG interfaces, turn them into G only
			 */
			
			short subType = interfaceInfo.getMediumSubType();
			
		    if(subType == Mesh.PHY_SUB_TYPE_802_11_B
		    || subType == Mesh.PHY_SUB_TYPE_802_11_B_G) {		        
		        interfaceInfo.setMediumSubType((short)Mesh.PHY_SUB_TYPE_802_11_G);		        
		    }
			
		}catch(Exception e) {
			e.printStackTrace();
			return;
		}
	}



	private void updateCountryCodeInformation(){
		
		short[] dcaList = new short[0];
	    
	    IInterfaceConfiguration	interfaceConfiguration	= iConfiguration.getInterfaceConfiguration();
		if(interfaceConfiguration == null) {
			return;
		}
		int ifCount	=	interfaceConfiguration.getInterfaceCount();
		
		for(int ifNo=0 ; ifNo < ifCount; ifNo++) {
			
			IInterfaceInfo interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(ifNo);
			if(interfaceInfo == null) {
				continue;
			}
			if(interfaceInfo.getMediumType() == Mesh.PHY_TYPE_802_3){ 
				continue;
			}
			Vector<ChannelListInfo> channelList 		= cmbCountryCode.getChannelList();
			String 					ifName 				= interfaceInfo.getName();
			short  					channelInfoCount	= (short)channelList.size();
			boolean					ifFound				= false;
			  
		    for(int i=0; i<channelInfoCount; i++) {
				
	            ChannelListInfo ch = (ChannelListInfo) channelList.get(i);
				if(ch == null) { 
					continue;
				}
				if(ifName.equalsIgnoreCase(ch.getInterfaceName()) == true) {
				
					short [] channels = ch.getChannels();
					interfaceInfo.setDcaListCount((short)(channels.length));
					setInterfaceDCAList(interfaceInfo,channels);
					ifFound = true;
					break;
				}
			}
		    if(ifFound == false) {
		    	setInterfaceDCAList(interfaceInfo,dcaList);
		    }
		}
	}

	/**
	 * @param interfaceInfo
	 * @param channels
	 */
	private void setInterfaceDCAList(IInterfaceInfo interfaceInfo, short[] channels) {
		
		short ifUsageType = interfaceInfo.getUsageType();
	    
		if((ifUsageType == Mesh.PHY_USAGE_TYPE_DS)) {
				interfaceInfo.setDcaListCount((short) 0);
				return;
		}
		
		for(int i = 0; i < channels.length && i < interfaceInfo.getDcaListCount(); i++ ) {
			interfaceInfo.setDcaChannel(i,channels[i]);
		}
	}
	/**
	 * @param code
	 * 
	 */
	private void updateRegDomainChanges(int code) {
	
		int reg_domain  = iConfiguration.getRegulatoryDomain();
		reg_domain 		&= 0xFF00;
		
		if(code == Mesh.CUSTOM_COUNTRY_CODE){
			
				reg_domain |= Mesh.REG_DOMN_CODE_CUSTOM;
				cmbCountryCode.setCountryCodeData(code);
				iConfiguration.setCountryCode(Mesh.CUSTOM_COUNTRY_CODE);
				iConfiguration.setRegulatoryDomain(reg_domain);
				chkForDfs();
				
			}else{
				
				CountryData cData   = cmbCountryCode.getCountryCodeData(""+code);
				iConfiguration.setCountryCode(code);
				cmbCountryCode.setCountryCodeData(code);
				updateCountryCodeInformation();
				
				int regDomainCode = cData.regDomnFld & 0X00FF;
				reg_domain |= regDomainCode;
				int dfsBit	= cData.regDomnFld & Mesh.DFS_BIT_SET;
				
				reg_domain &= 0XFEFF;// resetting the DFS bit
				
				reg_domain |= dfsBit;// setting the new DFS value
				
				iConfiguration.setRegulatoryDomain(reg_domain);
	 		}
	}



	/**
	 * 
	 */
	private void chkForDfs() {

		IInterfaceConfiguration	interfaceConfiguration	= iConfiguration.getInterfaceConfiguration();
		int 					ifCount					= interfaceConfiguration.getInterfaceCount();
		boolean 				dfsFlag 				= false;
		
		for(int j = 0; j < ifCount; j++) {
			
			IInterfaceInfo	interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(j);
			if(interfaceInfo == null) {
				return;
			}
			if(interfaceInfo.getMediumType() == Mesh.PHY_TYPE_802_3){
				continue;
			}
			InterfaceCustomChannelConfig customConfig	= (InterfaceCustomChannelConfig)interfaceInfo.getCustomChannelConfiguration();
			if(customConfig == null) {
				return;
			}	
			int channelInfoCount	= customConfig.getChannelInfoCount(); 
			
			for(int k = 0; k  < channelInfoCount; k++) {
				IChannelInfo channelInfo	= (IChannelInfo)customConfig.getChannelInfo(k);
				if((channelInfo.getPrivateChannelFlags()& Mesh.ENABLED)== 1){
					dfsFlag = true;
					break;
				}
			}
			if(dfsFlag == true){
				break;
			}
		}
		
		int regDomain	= iConfiguration.getRegulatoryDomain();
		if(dfsFlag == true) {
			regDomain |= Mesh.DFS_BIT_SET;
			iConfiguration.setRegulatoryDomain(regDomain);
		}else {
			regDomain &= ~Mesh.DFS_BIT_SET; 
			iConfiguration.setRegulatoryDomain(regDomain);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.ConfigPage#setGrpBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	public void setGrpBounds(Rectangle minimumGrpBounds) {
		grp.setBounds(10,165,minimumGrpBounds.width-10,minimumGrpBounds.height-(15+165));
	}
}
	

