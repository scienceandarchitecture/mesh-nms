package com.meshdynamics.nmsui.dialogs;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.mss.IMssExecutionListener;
import com.meshdynamics.mss.Msc;
import com.meshdynamics.mss.MssHelper;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

public class ExecuteMssDialog extends MeshDynamicsDialog implements IMssExecutionListener {
	
	private MeshNetworkUI					meshNetworkUI;
	private MssHelper						mssHelper;
	private IpAddress						hostIpAddress;
	private Hashtable<String, AccessPoint>	hashAvailableAp;
	private Vector<String>					allowedMacList;
	
	private Canvas					mainCnv;
	private Label					lblHostIpAddress;
	private Table					tblAccessPoints;
	private Table					tblExecutionStatus;
	private Button					btnStartStop;

	
	public ExecuteMssDialog(MeshNetworkUI networkUI, String fileName, Enumeration<String> macEnum) {

		this.meshNetworkUI 	= networkUI;
		allowedMacList		= new Vector<String>();
		
		if(macEnum != null) {
			while(macEnum.hasMoreElements()) {
				allowedMacList.add(macEnum.nextElement());
			}
		}
		
		mssHelper			= new MssHelper();
		mssHelper.mssHelperInit();
		mssHelper.setWorkingDirectory(MFile.getUpdatesPath());
		mssHelper.load(fileName);
		
		hostIpAddress 		= new IpAddress();
		try {
			hostIpAddress.setBytes(InetAddress.getLocalHost().getAddress());
		} catch (UnknownHostException e) {
			e.printStackTrace();
			hostIpAddress.setBytes(IpAddress.resetAddress.getBytes());
		}
		
		hashAvailableAp = new Hashtable<String, AccessPoint>();
		super.setTitle("Mesh Utility Script Execution");
	}
	
	@Override
	protected void createControls(Canvas mainCnv) {
		
		this.mainCnv = mainCnv;
		int relX = 15;
		int relY = 10;
		
		lblHostIpAddress = new Label(mainCnv, SWT.SIMPLE);
		lblHostIpAddress.setBounds(relX, relY, 400, 20);
		lblHostIpAddress.setText("$HOST_IP : " + hostIpAddress.toString());
		relY += 20;
		
		Label lbl = new Label(mainCnv, SWT.SIMPLE);
		lbl.setBounds(relX, relY, 220, 15);
		lbl.setText("Available Accesspoint's");
		relY += 15;
		
		tblAccessPoints = new Table(mainCnv, SWT.BORDER|SWT.SINGLE|SWT.FULL_SELECTION);
		tblAccessPoints.setHeaderVisible(true);
		tblAccessPoints.setLinesVisible(true);
		tblAccessPoints.setBounds(relX, relY, 580, 200);
		
		String[] headers = {"MacAddress", "IpAddress", "Status"};
		TableColumn column = new TableColumn(tblAccessPoints, SWT.SIMPLE);
		column.setText(headers[0]);
		column.setWidth(150);
		column = new TableColumn(tblAccessPoints, SWT.SIMPLE);
		column.setText(headers[1]);
		column.setWidth(150);
		column = new TableColumn(tblAccessPoints, SWT.SIMPLE);
		column.setText(headers[2]);
		column.setWidth(200);
		
		relY += 210;
		
		lbl = new Label(mainCnv, SWT.SIMPLE);
		lbl.setBounds(relX, relY, 380, 15);
		lbl.setText("Mesh Utility Script's");
		relY += 15;

		tblExecutionStatus = new Table(mainCnv, SWT.BORDER|SWT.SINGLE|SWT.FULL_SELECTION);
		tblExecutionStatus.setHeaderVisible(true);
		tblExecutionStatus.setLinesVisible(true);
		tblExecutionStatus.setBounds(relX, relY, 580, 200);
		
		String[] statusHeaders = {"Script", "Status"};
		column = new TableColumn(tblExecutionStatus, SWT.SIMPLE);
		column.setText(statusHeaders[0]);
		column.setWidth(400);
		column = new TableColumn(tblExecutionStatus, SWT.SIMPLE);
		column.setText(statusHeaders[1]);
		column.setWidth(170);
		
		relY += 210;
		
		btnStartStop = new Button(mainCnv, SWT.PUSH);
		btnStartStop.setBounds(relX, relY, 60, 25);
		btnStartStop.setText("Start");
		btnStartStop.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				handleStartStop();
				
			}
		});
		
		setAllControls(false);
		this.setCursor(Display.getDefault().getSystemCursor(SWT.CURSOR_APPSTARTING));
		Display.getDefault().asyncExec(new Runnable(){
			public void run() {
				setupData();
			}
		});
		
	}

	private void executeMsc(String mscId) {
		mssHelper.executeMsc(mscId , this);
	}
	
	protected void handleStartStop() {
		
		String btnText = btnStartStop.getText(); 
		if(btnText.equals("Start")) {
			final Msc msc = mssHelper.getMscEnumeration().nextElement();
			if(msc != null) {
				btnStartStop.setEnabled(false);
				new Thread(new Runnable(){
					public void run() {
						executeMsc(msc.getId());
					}
				}).start();
				
			}
		}
		
	}

	private void setAllControls(boolean enableDisable) {

		for(Control c : mainCnv.getChildren()) {
			c.setEnabled(enableDisable);
		}
		
	}
	
	private void setupData() {
		
		Enumeration<Msc> mscEnum = mssHelper.getMscEnumeration();
		if(mscEnum.hasMoreElements() == false) {
			setAllControls(false);
			return;
		}

		Msc msc	= mscEnum.nextElement();
		
		Enumeration <MacAddress> macEnum = msc.getMacAddressEnumeration();
		while(macEnum.hasMoreElements()) {
			MacAddress 	macAddress 		= macEnum.nextElement();
			String 		strMacAddress	= macAddress.toString();
			addIfAvailableForUpdate(strMacAddress);
		}
		
		int 		entryCount 		= msc.getScriptEntryCount();
		boolean		needTftpServer	= false;
		for(int i=0;i<entryCount;i++) {
			TableItem 	item 	= new TableItem(tblExecutionStatus, SWT.SIMPLE);
			String 		entry 	= msc.getScriptEntryAt(i); 
			item.setText("Command " + (i+1));
			if(entry.indexOf("tftp") >= 0)
				needTftpServer = true;
		}

		if(needTftpServer == true)
			runTftpd32();
		
		if(hashAvailableAp.size() <= 0) 
			setAllControls(false);
		else
			setAllControls(true);
		
		this.setCursor(Display.getDefault().getSystemCursor(SWT.CURSOR_ARROW));
	}

	private void runTftpd32() {
        String cmdLine = MFile.getSystemPath() +  File.separatorChar + "tftpd32.exe";
        
        try {
            Runtime.getRuntime().exec(cmdLine);
        } catch(IOException e) {
             e.printStackTrace();  
        }
	}

	private boolean addIfAvailableForUpdate(String strMacAddress) {
		
		TableItem item = getAccesspointItem(strMacAddress, true);
		item.setText(1, "Not found");
		item.setForeground(UIConstants.RED_COLOR);
		item.setGrayed(true);
		
		if(meshNetworkUI == null)
			return false;
		
		boolean found = false;
		for(String enumMacAddress : allowedMacList) {
			if(enumMacAddress.equals(strMacAddress) == true) {
				found = true;
				break;
			}
		}
		if(found == false)
			return false;
		
		MeshNetwork network = meshNetworkUI.getMeshNetwork();
		MacAddress.tempAddress.setBytes(strMacAddress);
		AccessPoint ap = network.getAccessPoint(MacAddress.tempAddress);
		if(ap == null)
			return false;
		
		IpAddress nodeIpAddress = ap.getIpAddress();
		try {
			InetAddress inetAddress = InetAddress.getByAddress(nodeIpAddress.getBytes());
			if(inetAddress.isReachable(10000) == false) {
				item.setText(1, "Unreachable");
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			item.setText(1, "Unreachable");
		}
		
		hashAvailableAp.put(strMacAddress, ap);
		item.setText(1, nodeIpAddress.toString());
		item.setText(2, "Waiting");
		item.setForeground(UIConstants.BLACK_COLOR);
		item.setGrayed(false);
		
		return true;
	}

	private TableItem getAccesspointItem(String string, boolean createIfNotFound) {
		
		for(TableItem item : tblAccessPoints.getItems()) {
			
			if(item.getText().trim().equals(string) == true) {
				return item;
			}
		}
		
		if(createIfNotFound == false)
			return null;
		
		TableItem newItem = new TableItem(tblAccessPoints, SWT.SIMPLE);
		newItem.setText(string);
		
		return newItem;
	}

	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x 		= 0;
		bounds.y 		= 0;
		bounds.width 	= 600;
		bounds.height 	= 500;
	}

	@Override
	protected boolean onOk() {
		mssHelper.mssHelperUninit();
		return true;
	}

	public static void main(String[] args) {
		ExecuteMssDialog dlg = new ExecuteMssDialog(null, "c:\\untitled.mss", null);
		dlg.show();
	}

	@Override
	public void executeClientEnd(final String mscId, final String strMacAddress, final boolean success) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				for(TableItem item : tblAccessPoints.getItems()) {
						if(strMacAddress.equalsIgnoreCase(item.getText(0)) == true) {
							if(success == true)
								item.setText(2, "Successful");
							else
								item.setText(2, "Failed");
						}
				}
			}
		});
	}

	@Override
	public void executeClientStart(final String mscId, final String strMacAddress) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				
				for(TableItem item : tblAccessPoints.getItems()) {
						if(strMacAddress.equalsIgnoreCase(item.getText(0)) == true)
							item.setText(2, "In Progress");
				}
				
				for(TableItem item : tblExecutionStatus.getItems()) {
					item.setText(1, "");
				}
			}
		});
	}

	@Override
	public void executionEnded(String mscId, String outcome) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				btnStartStop.setEnabled(true);
				enableOkButton(true);
				enableCancelButton(true);
			}
		});
	}

	@Override
	public void executionProgress(final String mscId, final String strMacAddress, final int mscEntryIndex, final String outcome) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				if(mscEntryIndex < 0) {
					for(TableItem item : tblAccessPoints.getItems()) {
						if(strMacAddress.equalsIgnoreCase(item.getText(0)) == true)
							item.setText(2, outcome);
					}
					
					for(TableItem item : tblExecutionStatus.getItems()) {
						item.setText(1, "");
					}
				} else {
					TableItem item = tblExecutionStatus.getItem(mscEntryIndex);
					item.setText(1, outcome);
				}
			}
		});
	}

	@Override
	public void executionStarted(String mscId) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				btnStartStop.setEnabled(false);
				enableOkButton(false);
				enableCancelButton(false);
			}
		});
	}

	@Override
	public byte[] getIpAddress(String strMacAddress) {
		
		String str = strMacAddress.toUpperCase();
		AccessPoint ap = hashAvailableAp.get(str);
		if(ap == null)
			return null;
		
		IpAddress ip = ap.getIpAddress(); 
		return ip.getBytes();
	}

	@Override
	protected boolean onCancel() {
		mssHelper.mssHelperUninit();
		return true;
	}

	@Override
	public byte[] getHostIpAddress() {
		return hostIpAddress.getBytes();
	}
}
