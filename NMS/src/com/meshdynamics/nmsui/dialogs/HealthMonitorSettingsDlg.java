/**
 * MeshDynamics 
 * -------------- 
 * File     : HealthMonitorSettingsDlg.java
 * Comments : 
 * Created  : Feb 08, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ---------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author     |
 *  --------------------------------------------------------------------------------
 * | 1   |May 1, 2007    | Changes due to MeshDynamicsDialog          |Abhishek    |
 * --------------------------------------------------------------------------------
 * | 0   | DEC 14, 2006  | created									  | Abhishek   |
 * --------------------------------------------------------------------------------*/
 
package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.mesh.HealthMonitorProperties;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;

/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class HealthMonitorSettingsDlg extends MeshDynamicsDialog{

	private Shell 					shell;
	private Label					lblYellowAlert;
	private Label					lblRedAlert;
	private Label					lblMins;
	private Group					grp;
	private	Text					txtYellowAlertTiming;
	private	Text					txtRedAlertTiming;
	
	private int						yellowAlertValue;
	private int						redAlertValue;
	private HealthMonitorProperties	healthMonitorProperties;
	private final int 				MAX_TICK_COUNT	= 480 * 60;
	
	
	public HealthMonitorSettingsDlg(HealthMonitorProperties	healthMonitorProperties){
		super();
		this.healthMonitorProperties = healthMonitorProperties;
	}
	
	private void displayValues() {
		txtYellowAlertTiming.setText(""+(healthMonitorProperties.getYellowAlertTime()/60));
		txtRedAlertTiming.setText(""+(healthMonitorProperties.getRedAlertTime()/60));
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	
	public void createControls(Canvas mainCnv){
		
		int rel_x = 5;
		int rel_y = 5;
		
		grp = new Group(mainCnv,SWT.NONE);
		grp.setBounds(rel_x,rel_y,240,90);
		
		rel_x = 15;
		rel_y = 25;
		
	    lblYellowAlert =  new Label(grp,SWT.NONE);
	    lblYellowAlert.setText("Yellow Alert Timeout");
	    lblYellowAlert.setBounds(rel_x,rel_y,100,20);
	    
	    rel_x = rel_x + 100;
	    
	    txtYellowAlertTiming = new Text(grp,SWT.BORDER);
	    txtYellowAlertTiming.setBounds(rel_x,rel_y,80,20);
	    txtYellowAlertTiming.setFocus();
	    
	    rel_x = rel_x + 85;
	    
	    lblMins    =  new Label(grp,SWT.NONE);
	    lblMins.setText("mins");
	    lblMins.setBounds(rel_x,rel_y,30,20);
	    
	    rel_x = 15;
	    rel_y = rel_y + 25;
	    
		lblRedAlert =  new Label(grp,SWT.NONE);
		lblRedAlert.setText("Red Alert Timeout");
		lblRedAlert.setBounds(rel_x,rel_y,100,20);
	    
	    rel_x = rel_x + 100;
	    
	    txtRedAlertTiming = new Text(grp,SWT.BORDER);
	    txtRedAlertTiming.setBounds(rel_x,rel_y,80,20);
	    
	    rel_x = rel_x + 85;
	    
	    lblMins    =  new Label(grp,SWT.NONE);
	    lblMins.setText("mins");
	    lblMins.setBounds(rel_x,rel_y,30,20);
	    
	    displayValues();
	}
	
	public void setValues(){
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	public boolean onOk() {
		if(validateValues() == false)
			return false;
		if(getValues()		== false)
			return false;
		
		//super.shell.dispose();
		return true;
	}

	 public boolean onExit(){
	        shell.dispose();
	        return true;     
	    }
	 
	
	public void show(){
		super.show(); 
	}
	
	/**
	 * @return
	 */
	private boolean getValues() {
		
		try {
			  yellowAlertValue   = Integer.parseInt(txtYellowAlertTiming.getText().trim());
			  yellowAlertValue   = yellowAlertValue * 60;
			}
		catch(Exception e)
		{
			MessageBox msg = new MessageBox(new Shell(), SWT.OK|SWT.ICON_ERROR);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Yellow Alert Timeout should be numeric.");
			msg.open();
			txtYellowAlertTiming.setFocus();  
			return false;
		}
		
		try {
			  redAlertValue   = Integer.parseInt(txtRedAlertTiming.getText().trim());
			  redAlertValue   = redAlertValue * 60;  
		    }
		catch(Exception e)
		{
			MessageBox msg = new MessageBox(new Shell(), SWT.OK|SWT.ICON_ERROR);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Red Alert Timeout should be numeric.");
			msg.open();
			txtRedAlertTiming.setFocus();  
			return false;
		}
		
		if(yellowAlertValue <= 0  || yellowAlertValue > MAX_TICK_COUNT ){
			
			MessageBox msg = new MessageBox(new Shell(), SWT.OK|SWT.ICON_ERROR);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Yellow Alert Timeout should be in between 1 to 480.");
			msg.open();
			txtYellowAlertTiming.setFocus();  
			return false;
		}
		
		if(redAlertValue <= 0  || redAlertValue > MAX_TICK_COUNT){
			
			MessageBox msg = new MessageBox(new Shell(), SWT.OK|SWT.ICON_ERROR);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Red Alert Timeout should be in between 1 to 480.");
			msg.open();
			txtRedAlertTiming.setFocus();  
			return false;
		}
			
		return true;
	}

	/**
	 * @return
	 */
	private boolean validateValues() {

		if(txtYellowAlertTiming.getText().trim().equals(""))
		{
			MessageBox msg = new MessageBox(new Shell(), SWT.OK|SWT.ICON_ERROR);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Yellow Alert Timeout is blank.");
			msg.open();
			txtYellowAlertTiming.setFocus();
			return false;
		}
		if(txtRedAlertTiming.getText().trim().equals(""))
		{
			MessageBox msg = new MessageBox(new Shell(), SWT.OK|SWT.ICON_ERROR);
			msg.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			msg.setMessage("Red Alert Timeout is blank.");
			msg.open();
			txtRedAlertTiming.setFocus();
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	public boolean onCancel() {
		return true;
	}

	/**
	 * @return Returns the redAlertValue.
	 */
	public int getRedAlertValue() {
		return redAlertValue;
	}
	/**
	 * @param redAlertValue The redAlertValue to set.
	 */
	public void setRedAlertValue(int redAlertValue) {
		this.redAlertValue = redAlertValue;
	}
	/**
	 * @return Returns the yellowAlertValue.
	 */
	public int getYellowAlertValue() {
		return yellowAlertValue;
	}
	/**
	 * @param yellowAlertValue The yellowAlertValue to set.
	 */
	public void setYellowAlertValue(int yellowAlertValue) {
		this.yellowAlertValue = yellowAlertValue;
	}
	
	public boolean onShellExit() {
		return true;
	}
	
	public static void main(String[] args) {
	//	HealthMonitorSettingsDlg hm	=	new HealthMonitorSettingsDlg();
	//	hm.show(); 
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#modifyMinimumBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width	= 240;
		bounds.height	= 100;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onApply()
	 */
	protected boolean onApply() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onImport()
	 */
	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onExport()
	 */
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}
}
