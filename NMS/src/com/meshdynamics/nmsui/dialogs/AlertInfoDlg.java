package com.meshdynamics.nmsui.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;

public class AlertInfoDlg extends MeshDynamicsDialog {
	
	private Text	message;
	private Text	description;
	private String	strMessage;
	private String 	strDescription;
	
	public AlertInfoDlg(String title, String message, String description) {
		super();
		this.strMessage 		= message;
		this.strDescription 	= description;
		super.setButtonDisplayMode(BUTTON_DISPLAY_MODE_OK);
		setTitle(title);
	}

	@Override
	protected void createControls(Canvas mainCnv) {
		
		int relY = 15; 
		
		Label lbl = new Label(mainCnv, SWT.NONE);
		lbl.setText("Message : ");
		lbl.setBounds(15, relY, 580, 15);
		relY += 15;
		
		message = new Text(mainCnv, SWT.BORDER|SWT.READ_ONLY);
		message.setBounds(15, relY, 580, 20);
		message.setText(strMessage);
		relY += 30;

		lbl = new Label(mainCnv, SWT.NONE);
		lbl.setText("Description : ");
		lbl.setBounds(15, relY, 580, 15);
		relY += 15;
		
		description = new Text(mainCnv, SWT.BORDER|SWT.READ_ONLY|SWT.H_SCROLL|SWT.V_SCROLL);
		description.setBounds(15, relY, 580, 330);
		description.setText(strDescription);
		
	}

	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.width 	= 600;
		bounds.height 	= 400;
	}

	@Override
	protected boolean onCancel() {
		return true;
	}

	@Override
	protected boolean onOk() {
		return true;
	}

}
