/**
 * MeshDynamics 
 * -------------- 
 * File     : SelectListTab.java
 * Comments : 
 * Created  : Sept 16, 2005
 * Author   : Sneha 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *----------------------------------------------------------------------------------
 * |  21 |Nov 19, 2006   | Misc changes:Changes due to StatusDisplayUI | Abhishek    |
 * ----------------------------------------------------------------------------------
 * |  20 |Apr 21, 2006	| Cleanup									  | Mithil 		 |
 * ----------------------------------------------------------------------------------
 * |  19 |Mar 31, 2006  |Nodes ! in viewer(stopped),in lst error fixed| Mithil 		|
 *  --------------------------------------------------------------------------------
 * |  21 |Mar 29, 2006  | Added skipped nodes dialog				  | Mithil 		|
 * ----------------------------------------------------------------------------------
 * |  20 |Mar 28, 2006  | Fixed crashes in Macro tab				  | Mithil 		|
 * ----------------------------------------------------------------------------------
 * |  19 |Mar 28, 2006  |Nodes not in viewer but in list error fixed  | Mithil 		|
 *  --------------------------------------------------------------------------------
 * |  18 |Mar 28, 2006  |File Browse for file list cancel error fixed | Mithil 		|
 *  --------------------------------------------------------------------------------
 * |  17 |Mar 16, 2006  |Added ACL import template					  | Mithil 		|
 *  --------------------------------------------------------------------------------
 * |  16 |Mar 03, 2006  | import vlan template changes				  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  15 |Mar 01, 2006  | File empty check added					  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  14 |Mar 01, 2006  | texts set focus when radio selected	  	  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  13 |Mar 01, 2006  | texts Disabled when radio not selected	  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  12 |Feb 27, 2006  | Invalid File check added					  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  11 |Jan 30, 2006  | Restructuring								  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  10 |Jan 25, 2006  | Run Macro Tabs removed					  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  9  |Jan 20, 2006  | Run Macro Changes							  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  8  |Jan 20, 2006  | Group select done							  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  7  |Jan 10, 2006  | Validate MAC Id not Entered			 	  | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  6  |Jan 09, 2006 | MsgBox Diaplayed if proper sel is not done   | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  5  |Dec 21, 2005 |  run macros dialog needs to be made modal in | Mithil 		 |
 * |	 			      relation to the open dialog box : 1061                   	 |
 * ----------------------------------------------------------------------------------
 * |  4  |Oct 11, 2005   |  UI changes		                          | Sneha        |
 * ----------------------------------------------------------------------------------
 * |  3  |Oct 05, 2005   |  Radio Buttons Text, Misc Bugs			  | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  2  |Sept 30, 2005  |  selected radio, buttons enabled/disabled  | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  1  |Sept 27, 2005  |  enable and disable widget func added      | Amit         |
 * -----------------------------------------------------------------------------------
 * |  0  |Sept 16, 2005  |  Created	                                  | Sneha        |
 * ----------------------------------------------------------------------------------
 */
package com.meshdynamics.nmsui.dialogs.macros;

import java.util.Enumeration;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.nmsui.mesh.views.AccessPointHandler;
import com.meshdynamics.util.MacAddress;


public class SelectListTab  extends Canvas{
	
	public static final int OPTION_ACTIVELIST 	= 1;
	public static final int OPTION_SINGLENODE 	= 3;
	 
	private 	Vector<String>	addressList;
	private 	Button  		optActiveList;
	private		Button  		optSingleNode;
	private		Text			txtMacAddr;
	private 	MacrosDlg 		parent;
	private 	int 			currentSelectedOption;
	
	/**
	 * @param arg0
	 * @param arg1
	 */
	public SelectListTab(MacrosDlg parent, Composite arg0, int arg1) {
		super(arg0, arg1);
		
		this.parent 			= parent;
		addressList	 			= new Vector<String>();
		currentSelectedOption	= OPTION_ACTIVELIST;
		createControls();
	}

	/**
	 * 
	 */
	private void createControls() {
	    
		int rel_y = 70;
		int rel_x = 30;
		
		optActiveList	=	new Button(this,SWT.RADIO);
		optActiveList.setBounds(rel_x,rel_y,160,20);
		optActiveList.setSelection(true);
		optActiveList.setText("Select Nodes From Active List");
		optActiveList.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				if(optActiveList.getSelection() == true) {
					addressList.clear();
					currentSelectedOption = OPTION_ACTIVELIST;
				}
				
			}

		});
		
		rel_y	+= 37;
		optSingleNode	=	new Button(this,SWT.RADIO);
		optSingleNode.setBounds(rel_x,rel_y,150,20);
		optSingleNode.setText("Operate on one node");
		optSingleNode.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				if(optSingleNode.getSelection() == true) {
					addressList.clear();
					txtMacAddr.setEnabled(true);
					txtMacAddr.setFocus();
					currentSelectedOption = OPTION_SINGLENODE;
				} else {
					txtMacAddr.setEnabled(false);
				}
			}

		});

		rel_y	+=33;
		rel_x	+=20;
		Label lab	=	new Label(this,SWT.NO);
		lab.setText("MAC ID");
		lab.setBounds(rel_x,rel_y,40,20);
		
		rel_x	+=60;
		rel_y	-=3;
		txtMacAddr =	new Text(this,SWT.BORDER);
		txtMacAddr.setBounds(rel_x,rel_y,200,20);
		txtMacAddr.setEnabled(false);
		
	}
	
	/**
	 * @param flag
	 */
	public void setEnableAll(boolean flag) {
		
		if(flag==false){
		 	optActiveList.setEnabled(flag);
		 	optActiveList.setSelection(flag);
		  	optSingleNode.setEnabled(flag);
		  	optSingleNode.setSelection(flag);		 				
		 	txtMacAddr.setEnabled(flag);						
		}
	}

	/**
	 * @param selection
	 */
	public void setSelection(int selection) {
		if(selection == OPTION_ACTIVELIST){
			setEnableAll(false);
			optActiveList.setEnabled(true);
			optActiveList.setSelection(true);
		} else if(selection == OPTION_SINGLENODE){
			setEnableAll(false);
			optSingleNode.setEnabled(true);
			optSingleNode.setSelection(true);		
		}
	}
	
	public boolean onNext() {
		
		addressList.clear();
		
		if(validateData() == false) {
			return false;
		}
		
		parent.clearAddressList();
		
		for(int i=0;i<addressList.size();i++) {
			parent.addAddressToList((String) addressList.get(i));
		}
		
		return true;
		
	}

	private boolean validateData() {

		if(currentSelectedOption == OPTION_ACTIVELIST) {
			return validateActiveList();
		} else if(currentSelectedOption == OPTION_SINGLENODE) {
			return validateSingleNode();
		}
		
		return false;
	}

	private boolean validateSingleNode() {
		
		String strMacAddress = txtMacAddr.getText().trim();
		if(strMacAddress.equals("") == true) {
    		MessageBox msgBox =  new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.ICON_INFORMATION);
    		String msg = "Enter mac address.";
    		msgBox.setMessage(msg);
    		msgBox.setText("Error");
    		msgBox.open();	    		
    		return false;
		}
		if(MeshValidations.isMacAddress(strMacAddress) == false) {
    		MessageBox msgBox =  new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.ICON_WARNING);
    		String msg = "Invalid mac address.";
    		msgBox.setMessage(msg);
    		msgBox.setText("Error");
    		msgBox.open();	    		
    		return false;
    	}
		
		MacAddress dsMacAddress = new MacAddress();
		dsMacAddress.setBytes(strMacAddress);
		MeshNetworkUI meshnetworkUI = parent.meshViewerUI.getSelectedMeshNetworkUI();
		 
		if(meshnetworkUI.hasAccessPoint(dsMacAddress.toString()) == false) {
    		MessageBox msgBox =  new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.ICON_WARNING);
    		String msg = "Accesspoint with mac address " + dsMacAddress.toString() + " not found.";
    		msgBox.setMessage(msg);
    		msgBox.setText("Error");
    		msgBox.open();	    		
    		return false;
		}
		
		if(meshnetworkUI.isApRunning(dsMacAddress.toString()) == false) {
    		MessageBox msgBox =  new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.ICON_WARNING);
    		String msg = "Accesspoint with mac address " + dsMacAddress.toString() + " not running.";
    		msgBox.setMessage(msg);
    		msgBox.setText("Error");
    		msgBox.open();	    		
    		return false;
		}
		
		addressList.add(strMacAddress);
		
		return true;
	}

	private boolean validateActiveList() {
		
		MeshNetworkUI meshnetworkUI = parent.meshViewerUI.getSelectedMeshNetworkUI();
		
		Enumeration<AccessPointHandler> apHandlerEnum = meshnetworkUI.getAccesspoints();
		
		while(apHandlerEnum.hasMoreElements()) {
			
			AccessPointHandler apHandler = apHandlerEnum.nextElement();
			
			if(apHandler.isRunning() == true) {
				
				MacAddress	macAddr	= apHandler.getDSMacAddr();
				
				if(meshnetworkUI.isGroupSelectionEnabled() == true){
					if(meshnetworkUI.isGroupSelected(macAddr.toString()) == true){
						addressList.add(macAddr.toString());
					}
				}else {
					addressList.add(macAddr.toString());
				}
			}
		}
		if(addressList.size() <= 0) {
			parent.showErrorMessage("No nodes selected. ",SWT.ICON_INFORMATION|SWT.OK);
			return false;
		}
		return true;
	}
	
}
