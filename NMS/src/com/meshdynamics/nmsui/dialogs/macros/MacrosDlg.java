/**
 * MeshDynamics 
 * -------------- 
 * File     : MacrosDlg.java
 * Comments : 
 * Created  : Sept 16, 2005
 * Author   : Amit 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * |  26 |Jul 23, 2007   | MoveNetwork list changes(FIPS 140-2)   	  |Abhishek      |
 * ----------------------------------------------------------------------------------
 * |  25 |Mar 16, 2007   | changes due to meshdynamicsdialog change	  |Abhijit       |
 * ----------------------------------------------------------------------------------
 * |  24 |Mar 06, 2007   | onApply for Vlan,ACL,Rf import commented	  |Abhishek      |
 * ----------------------------------------------------------------------------------
 * |  23 |Jan 18, 2007   | Changes for Fips 			  			  |Abhishek      |
 * ----------------------------------------------------------------------------------
 * |  22 |Apr 13, 2006   | Changes for Formware Update  			  | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  21 |Mar 30, 2006   | switch from panels disables finish button  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  20 |Mar 28, 2006   |Nodes not in viewer but in list error fixed | Mithil 		 |
 *  --------------------------------------------------------------------------------
 * |  19 |Mar 16, 2006   |Changes for ACL							  | Mithil 		 |
 *  --------------------------------------------------------------------------------
 * |  18 |Mar 03, 2006   | import vlan template changes				  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  17 |Feb 27, 2006   | Invalid File check added					  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  16  |Jan 27, 2006  |  File list not working				      | Mithil 	 	 |
 * ----------------------------------------------------------------------------------
 * |  15  |Jan 25, 2006  | ApplyClicked shifted to Macros Dialog      | Mithil 	 	 |
 * ----------------------------------------------------------------------------------
 * |  14  |Jan 25, 2006  | Run Macro Tabs removed					  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  13  |Jan 20, 2006  | Run Macro Changes						  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  12  |Jan 10, 2006  | Validate MAC Id not Entered			 	  | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  11  |Jan 10, 2006  | Next working for all options			 	  | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  10  |Jan 09, 2006  | MsgBox Diaplayed if proper sel is not done | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  9  |Jan 09, 2006   | Apply btn disabled until proper sel is done| Mithil       |
 * ----------------------------------------------------------------------------------
 * |  8  |Jan 06, 2006   | Unused variable parentShell removed	      | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  7  |Jan 05, 2006   | The Run Macro dialog should have things 	  | Mithil 		 |
 * |			 		   disabled when network is not running						 |
 * ----------------------------------------------------------------------------------
 * |  6  |Jan 5, 2006    |  Tab switching not handled properly        | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  5  |Jan 2, 2006    |  UI changes Ref 1066		                  | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  4  |Oct 11, 2005   |  UI changes		                          | Sneha        |
 * ----------------------------------------------------------------------------------
 * |  3  |Oct 05, 2005   |  Misc Bugs Fixed							  | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  2  |Sept 30, 2005  |  Setting of slection in runMacroDlg        | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  1  |Sept 29, 2005  |  Run Macros fns added                      | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  0  |Sept 16, 2005  |  Created	                                  | Amit         |
 * ----------------------------------------------------------------------------------
 */
package com.meshdynamics.nmsui.dialogs.macros;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;

import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.nmsui.MeshViewerUI;
import com.meshdynamics.nmsui.dialogs.base.IMessage;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;

public class MacrosDlg extends MeshDynamicsDialog implements IMessage{
	
	
	private SelectListTab				selListComp;
	private RunMacroTab					runMacComp;
	
	private Hashtable<String, String>	addressList;		
	public 	MeshViewerUI 				meshViewerUI;
	private boolean						enableRunMacros;
		
	private int							selectedMacro;
	private	String						updateFolder;
	private String						nodeFileName;
	private String						selectedNetworkName;
	private String						sipRegistryFileName;
	private String 						mssFileName;
	private String 						importScriptFileName;
	
	
	public MacrosDlg(MeshViewerUI meshViewerUI){
		super();
		this.meshViewerUI	= meshViewerUI;
		addressList			= new Hashtable<String, String>(); 
		enableRunMacros		= false;
		setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_WIZARD);
		setTitle("Run Macro");
		selectedMacro		= -1;
	} 
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#createControls(org.eclipse.swt.widgets.Canvas)
	 */
	protected void createControls(Canvas mainCnv) {
		
		selListComp	=	new SelectListTab(this,mainCnv,SWT.NO);
		selListComp.setBounds(5,5,360,280);
		
		ContextHelp.addContextHelpHandlerEx(selListComp,contextHelpEnum.DLGRUNMACROPAGE1);
		
		runMacComp	=	new RunMacroTab(this,mainCnv,SWT.NO);
		runMacComp.setBounds(5,5,360,280);
		runMacComp.setNetworkList(getNetworkList());
	
		runMacComp.enableOptions(enableRunMacros);
		ContextHelp.addContextHelpHandlerEx(runMacComp,contextHelpEnum.DLGRUNMACROPAGE2);
		
		enableApplyButton(false);
		enablePreviousButton(false);
		
		selListComp.setFocus();
	}
	
	private Vector<String> getNetworkList() {

		Enumeration<MeshNetworkUI> netEnum = meshViewerUI.getMeshNetworkUIs();
		MeshNetworkUI selMeshNetworkUI 		= meshViewerUI.getSelectedMeshNetworkUI();
		MeshNetwork   selectedMeshnetwork	= selMeshNetworkUI.getMeshNetwork();
		Vector<String> networkList = new Vector<String>();
		
		while(netEnum.hasMoreElements()){

			MeshNetworkUI networkUI = (MeshNetworkUI)netEnum.nextElement();
		    if(selMeshNetworkUI.getNwName().equalsIgnoreCase(networkUI.getNwName()) == true) {
		       continue;
		    }
		    MeshNetwork network	= networkUI.getMeshNetwork();
		    if(selectedMeshnetwork.getNetworkType() == MeshNetwork.NETWORK_FIPS_ENABLED) {
		    			// fips network
		    			networkList.add(networkUI.getNwName());
		    }else if(selectedMeshnetwork.getNetworkType() != MeshNetwork.NETWORK_FIPS_ENABLED &&
		    		network.getNetworkType()			  != MeshNetwork.NETWORK_FIPS_ENABLED)
		    			// non fips network 
		    			networkList.add(networkUI.getNwName());        
		 	}
		 return networkList;
	}
	
	protected void onNext(){
		
		if(selListComp.onNext() == false) {
			return;
		}
		selListComp.setVisible(false);
		runMacComp.setVisible(true);
		runMacComp.setFocus();
		enableNextButton(false);
		enablePreviousButton(true);
		enableApplyButton(true);
		runMacComp.enableOptions(true);
		
	}
	
	protected void onPrevious(){
		selListComp.setVisible(true);
		runMacComp.setVisible(false);
		enableNextButton(true);
		enablePreviousButton(false);
		enableApplyButton(false);
	}
		
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onCancel()
	 */
	public boolean onCancel() {
		selectedMacro	= RunMacroTab.MAC_TAB_CANCEL_PRESSED;
		return true;
	}

	protected boolean onApply(){
	    
		selectedMacro = runMacComp.getMacTabSelection();
		
		if(selectedMacro == RunMacroTab.MAC_TAB_UPD_NODE_FIRMWARE) {
			updateFolder		= runMacComp.getFirmwareDirName();
		} else if(selectedMacro == RunMacroTab.MAC_TAB_MOVEDEF_NODES_TO) {
			selectedNetworkName	= runMacComp.getSelectedNetworkName();
		} else if(selectedMacro == RunMacroTab.MAC_TAB_IMPORT_SIP_REGISTRY) {
			sipRegistryFileName = runMacComp.getSipRegistryFileName();
		} else if(selectedMacro == RunMacroTab.MAC_TAB_EXECUTE_MSS) {
			mssFileName 		= runMacComp.getMssFileName();
		} else if(selectedMacro == RunMacroTab.MAC_TAB_IMPORT_CONFIG_SCRIPT) {
			importScriptFileName = runMacComp.getImportScriptMacroFileName();
		}

		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.MeshDynamicsDialog#onOk()
	 */
	protected boolean onOk() {
		// TODO Auto-generated method stub
		return false;
	}
	
	protected boolean onExport() {
		// TODO Auto-generated method stub
		return false;
	}

	protected boolean onImport() {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * @param dirName
	 * @return
	 */
	private boolean isValidDirectory(String dirName) {
		
		if(dirName.equalsIgnoreCase("")) {
			showErrorMessage("Directory not selected.",SWT.OK | SWT.ICON_INFORMATION);
		}
		File f = new File(dirName);
		if(f.isDirectory() == false){
			showErrorMessage("Directory does not exist.",SWT.OK | SWT.ICON_INFORMATION);
			return false;
		}
		return true;
	}
	
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x 		= 0;
		bounds.y 		= 0;
		bounds.width 	= 360;
		bounds.height	= 220;
	}

	void clearAddressList() {
		addressList.clear();
	}
	
	void addAddressToList(String strMacAddress) {
		addressList.put(strMacAddress, strMacAddress);
	}
	
	public Enumeration<String> getAddressList(){
		return addressList.elements();
	}

	public int getAddressCount() {
		return addressList.size();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMessage#showMessage(java.lang.String, int)
	 */
	public int showMessage(String message, int style) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return
	 */
	public int getSelectedMacroAction() {
		return selectedMacro;
	}

	/**
	 * @return
	 */
	public String getUpdateFileName() {
		return nodeFileName;
	}

	/**
	 * @return
	 */
	public String getSelectedNetworkName() {
		return selectedNetworkName;
	}

	/**
	 * @return
	 */
	public String getFirmwareDirName() {
		if(isValidDirectory(updateFolder) == false) {
			return null;
		}
		return updateFolder;
	}

	/**
	 * @return
	 */
	byte getCurrentNetworkType() {
		MeshNetworkUI selMeshNetworkUI 		= meshViewerUI.getSelectedMeshNetworkUI();
		return selMeshNetworkUI.getNetworkType();
	}

	public String getSipRegistryFileName() {
		return sipRegistryFileName;
	}
	
	public String getMssFileName() {
		return mssFileName;
	}

	public String getImportScriptFileName() {
		return importScriptFileName;
	}
	
	String getCurrentNetworkName() {
		return meshViewerUI.getSelectedMeshNetworkUI().getNwName();
	}
}
