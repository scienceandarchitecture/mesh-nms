/**
 * MeshDynamics 
 * -------------- 
 * File     : MacrosDlg.java
 * Comments : 
 * Created  : Sept 16, 2005
 * Author   : Sneha 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                    | Author   |
 *  -------------------------------------------------------------------------------
 * |  55 |Jun 28, 2007   | rewrite                                     |Imran     |
 *  -------------------------------------------------------------------------------
 * |  54 |Apr 02, 2007   | selected flag changes                       |Abhishek   |
 *  -------------------------------------------------------------------------------
 * |  53 |Mar 06, 2007   | Import Vlan,ACL,Rf import commented	  	   |Abhishek  |
 * --------------------------------------------------------------------------------
 * |  52 |Jan 23, 2007   | Set FIPS140-2 added                         | Abhishek |
 *  --------------------------------------------------------------------------------
 * |  51 |Jan 12,2006    |  Move nodes method modified 				   | Imran 	  |
 * --------------------------------------------------------------------------------
 * |  51 |Dec 23, 2006   | Import RF template commented                | Abhishek |
 * --------------------------------------------------------------------------------
 * |  50 |Dec 18, 2006   | Import RF template added                    | Abhishek |
 * --------------------------------------------------------------------------------
 * |  49 |Nov 19, 2006   | Misc changes:Changes due to StatusDisplayUI | Abhishek       |
 * ----------------------------------------------------------------------------------
 * |  48 |Nov 17, 2006   |  CompareConfiguration for template removed | Imran        |
 * ----------------------------------------------------------------------------------
 * |  47 |Oct 18, 2006   | Reboot node changes for moving  node added | Imran       |
 * ----------------------------------------------------------------------------------
 * |  46 |Jun 21, 2006   | compare bet'n config's for vlan			  | Mithil 		 |
 * ----------------------------------------------------------------------------------
 * |  45 |May 20, 2006   | F/W Update rewritten						  | Bindu 		 |
 * ----------------------------------------------------------------------------------
 * |  44 |May 19, 2006   | template comparison						  | Mithil 		 |
 * ----------------------------------------------------------------------------------
 * |  43 |May 11, 2006   | compare Configuration Added				  | Mithil 		 |
 * ----------------------------------------------------------------------------------
 * |  42  |Apr 21, 2006	 | Cleanup									  | Mithil 		 |
 * ----------------------------------------------------------------------------------
 * |  41  |Apr 17, 2005  | Firmware Update array index problem		  | Mithil 		 |
 * --------------------------------------------------------------------------------
 * |  40  |Apr 13, 2006  | Changes for Firmware Update  			  | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  39  |Mar 30, 2006  | Errors display for Restore Factory	      | Mithil       |	 
 * |					   settings & Reboot if no nodes present n/w				 |	
 *  --------------------------------------------------------------------------------
 * |  38  |Mar 30, 2006  | switch from panels disables finish button  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  37  |Mar 29, 2006  | Checked for conf present in directory	  | Mithil 		 |
 * ----------------------------------------------------------------------------------
 * |  36  |Mar 28, 2006  | Fixed crashes in Macro tab				  | Mithil 		 |
 * ----------------------------------------------------------------------------------
 * |  35  |Mar 17, 2006  | Version in msg box generalised			  | Mithil 		 |
 * ----------------------------------------------------------------------------------
 * |  34  |Mar 16, 2006  |Finish button misc problems				  | Mithil 		 |
 *  --------------------------------------------------------------------------------
 * |  33  |Mar 16, 2006  |Finish button problems					  | Mithil 		 |
 *  --------------------------------------------------------------------------------
 * |  32  |Mar 16, 2006  |Added ACL import template					  | Mithil 		 |
 *  --------------------------------------------------------------------------------
 * |  31  |Mar 13, 2006  |Move nodes done for relays				  | Mithil 		 |
 *  --------------------------------------------------------------------------------
 * |  30  |Mar 08, 2006  |VLAN QOS Template Dialog Added			  | Mithil 		 |
 *  --------------------------------------------------------------------------------
 * |  29  |Mar 03, 2006  | import vlan template changes				  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  28  |Mar 01, 2006  | texts Disabled when radio not selected	  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  27  |Feb 28, 2006  | Send reset reboot called from run macro	  | Mithil 		 |
 *  -------------------------------------------------------------------------------
 * |  26  |Feb 20, 2006  | Commented move nodes if stacnt != 0		  | Mithil 	 	 |
 * ----------------------------------------------------------------------------------
 * |  25  |Feb 17, 2006  | Update Firmware settings dialog			  | Mithil 	 	 |
 * ----------------------------------------------------------------------------------
 * |  24  |Feb 16, 2006  | Change of labels							  | Mithil 	 	 |
 * ----------------------------------------------------------------------------------
 * |  23  |Feb 14, 2006  | Clear Macro Message after reboot			  | Mithil 	 	 |
 * ----------------------------------------------------------------------------------
 * |  22  |Jan 30, 2006  | Restructuring						      | Mithil 	 	 |
 * ----------------------------------------------------------------------------------
 * |  21  |Jan 27, 2006  | Move node to network Disabled		      | Mithil 	 	 |
 * ----------------------------------------------------------------------------------
 * |  20  |Jan 27, 2006  |  File list not working				      | Mithil 	 	 |
 * ----------------------------------------------------------------------------------
 * |  19  |Jan 25, 2006  | Not supported for version added			  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  18  |Jan 25, 2006  | Run Macro Tabs removed					  | Mithil       |
 *  --------------------------------------------------------------------------------
 * |  17  |Jan 20, 2006  | import setting file dialog made modal	  | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  16  |Jan 10, 2006  | import setting file dialog made modal	  | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  15  |Jan 09, 2006  | sel done in FileDialog if file sel before  | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  14  |Jan 09, 2006  | MsgBox Displayed if proper sel is not done | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  13 |Jan 09, 2006   | Bugs intoduced after last problem fixed	  | Mithil       |
 * ----------------------------------------------------------------------------------
 * |  12 |Jan 09, 2006   | Apply btn disabled until proper sel is done| Mithil       |
 * ----------------------------------------------------------------------------------
 * |  11 |Jan 05, 2006   | The Run Macro dialog should have things 	  | Mithil 		 |
 * |			 		   disabled when network is not running						 |
 * ----------------------------------------------------------------------------------
 * |  10 |Jan 02, 2006   | Message Box text uniform				      | Mithil 		 |
 * ----------------------------------------------------------------------------------
 * |  9  |Nov 07, 2005   |  Movenode condition checking done          | Amit         |
 * ----------------------------------------------------------------------------------
 * |  8  |Oct 11, 2005   |  UI changes		                          | Sneha        |
 * ----------------------------------------------------------------------------------
 * |  7  |Oct 07, 2005   |  Moving from/to default modified			  | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  6  |Oct 05, 2005   |  Misc bugs fixed(msgBox, resp pkt req)	  | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  5  |Oct 04, 2005   |  Radio Buttons Text, Macros logic		  | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  4  |Sept 30, 2005  |  Auth Key bug, buttons enabled/disabled    | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  3  |Sept 29, 2005  |  Go Buttons removed					      | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  2  |Sept 28, 2005  |  Option buttons handling done		      | Amit         |
 * ----------------------------------------------------------------------------------
 * |  1  |Sept 27, 2005  |  enable and disable widget func added      | Amit         |
 * ----------------------------------------------------------------------------------
 * |  0  |Sept 16, 2005  |  Created	                                  | Sneha        |
 * ----------------------------------------------------------------------------------
 */
package com.meshdynamics.nmsui.dialogs.macros;

import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.nmsui.util.MFile;

public class RunMacroTab extends Canvas{

	public static final int MAC_TAB_UPD_NODE_FIRMWARE			= 2;
	public static final int MAC_TAB_MOVEDEF_NODES_TO 			= 3;
	public static final int MAC_TAB_RESTORE_FACT_DEFS			= 4;
	public static final int MAC_TAB_REBOOT_NODES				= 5;
	public static final int MAC_TAB_CANCEL_PRESSED				= 6; 
	public static final int MAC_TAB_IMPORT_SIP_REGISTRY			= 7;
	public static final int MAC_TAB_EXECUTE_MSS					= 8;
	public static final int MAC_TAB_IMPORT_CONFIG_SCRIPT		= 9;
	public static final int MAC_TAB_MGMT_GW                     = 10;
	
	public Text				txtFirmwareDir;
	private Combo			cmbMove;
	private MacrosDlg 		parent;
	private Button			optUpdateNodeFirmware;
	private Button			optMoveDefault;
	private Button			optMgmt_gw;
	private Button			optRestoreFactory;
	private Button			optRebootNodes;	
	private	int 			selected;
	private Button 			btnFirmwareBrowse;
	private Button			optImportSipRegistry;
	private Text			txtSipRegistryDir;
	private Button			btnSipRegistryBrowse;
	private Button			optExecuteMss;
	private Button			btnBrowseMss;
	private Text			txtMssFile;
	private Button			optImportConfigScript;
	private Text			txtImportConfigScript;
	private Button			btnBrowseImportConfigScript;
	
	/**
	 * @param arg0
	 * @param arg1
	 */
	RunMacroTab(MacrosDlg parent,Composite arg0, int arg1) {
		super(arg0, arg1);
		this.parent	= parent;	
		selected	= -1;
		createControls();
	}

	/**
	 * 
	 */
	private void createControls() {
		
		int rel_y = 20;
		int rel_x = 10;
		
		optMoveDefault  =	new Button(this,SWT.RADIO);
		optMoveDefault.setBounds(rel_x ,rel_y,135,20);
		optMoveDefault.setText("Move Nodes To");
		optMoveDefault.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {

				if(optMoveDefault.getSelection() == true) {
					if(cmbMove.getItemCount() > 0){
						cmbMove.setEnabled(true);
						parent.enableApplyButton(true);
					}else {
						parent.enableApplyButton(false);
						showMoveNodesWnd();
					}
					 selected = RunMacroTab.MAC_TAB_MOVEDEF_NODES_TO;
				}else {
					cmbMove.setEnabled(false);
					parent.enableApplyButton(false);
				}
			}
		});
		
		rel_x	+= 140;
		
		cmbMove =	new Combo(this,SWT.BORDER|SWT.READ_ONLY);
		cmbMove.setBounds(rel_x,rel_y,140,20);
		cmbMove.setEnabled(false);
				
		rel_x	-= 140;
		rel_y	+= 30;
		optRestoreFactory   =	new Button(this,SWT.RADIO);
		optRestoreFactory.setBounds(rel_x,rel_y,135,20);
		optRestoreFactory.setText("Restore Default Settings");
		optRestoreFactory.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(optRestoreFactory.getSelection() == true) {
					parent.enableApplyButton(true);
					selected = RunMacroTab.MAC_TAB_RESTORE_FACT_DEFS ;
				}
			}
		});
		rel_y	+= 30;
		optMgmt_gw     = new Button(this,SWT.RADIO);
		optMgmt_gw.setBounds(rel_x,rel_y,135,20);
		optMgmt_gw.setText("Management Gateway");
		optMgmt_gw.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(optMgmt_gw.getSelection() == true) {
					parent.enableApplyButton(true);
					selected = RunMacroTab.MAC_TAB_MGMT_GW ;
				}
			}
		});
		rel_y 	+= 30;
		optRebootNodes    =	new Button(this,SWT.RADIO);
		optRebootNodes.setBounds(rel_x ,rel_y,125,20);
		optRebootNodes.setText("Reboot Nodes");
		optRebootNodes.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(optRebootNodes.getSelection() == true) {
					parent.enableApplyButton(true);
					selected = RunMacroTab.MAC_TAB_REBOOT_NODES;
				}				
			}
		});

		rel_y	+= 30;
		optUpdateNodeFirmware  =	new Button(this,SWT.RADIO);
		optUpdateNodeFirmware.setBounds(rel_x,rel_y,135,20);
		optUpdateNodeFirmware.setText("Update Node Firmware");
		optUpdateNodeFirmware.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(optUpdateNodeFirmware.getSelection() == true) {
					txtFirmwareDir.setText(""+MFile.getUpdatesPath());					
					txtFirmwareDir.setEnabled	(true);
					btnFirmwareBrowse.setEnabled(true);										
					selected = RunMacroTab.MAC_TAB_UPD_NODE_FIRMWARE;
				} else {
					txtFirmwareDir.setEnabled	(false);
					btnFirmwareBrowse.setEnabled(false);				
					txtFirmwareDir.setText("");
				}
			}
		});	

		rel_x	+= 140; 
		
		txtFirmwareDir	=	new Text(this,SWT.BORDER);
		txtFirmwareDir.setBounds(rel_x,rel_y,140,20);
		
		txtFirmwareDir.addModifyListener(new ModifyListener(){
			public void modifyText(ModifyEvent me){
				if(optUpdateNodeFirmware.getSelection() == true) {
					if(txtFirmwareDir.getText().equals("") || txtFirmwareDir.getText() == null){					
						parent.enableApplyButton(false);
					} else {
						parent.enableApplyButton(true);
					}
				}
			}
		});
		
		txtFirmwareDir.setEnabled(false);
		
		rel_x	+= 150;
		btnFirmwareBrowse	=	new Button(this,SWT.PUSH);
		btnFirmwareBrowse.setText("Browse");
		btnFirmwareBrowse.setBounds(rel_x,rel_y,50,20);
		btnFirmwareBrowse.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				DirectoryDialog dirDialog 	= new DirectoryDialog(new Shell());
				dirDialog.setFilterPath(MFile.getUpdatesPath());
				dirDialog.setText("Select Directory");
				dirDialog.setMessage("Select a directory containing changed configuration files.");
				String dirpath 				= dirDialog.open(); 
				if((dirpath == null) || (dirpath.equalsIgnoreCase("") == true)) {
					return;
				}
				txtFirmwareDir.setText(dirpath);
				
			}
		});
		
		btnFirmwareBrowse.setEnabled(false);
		
		rel_x 	-= 290;
		rel_y	+= 30;
		optImportSipRegistry  =	new Button(this,SWT.RADIO);
		optImportSipRegistry.setBounds(rel_x,rel_y,135,20);
		optImportSipRegistry.setText("Import PBV(tm) Registry");
		optImportSipRegistry.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(optImportSipRegistry.getSelection() == true) {
					txtSipRegistryDir.setText(MFile.getConfigPath() + "\\" + parent.getCurrentNetworkName() + ".sip");					
					txtSipRegistryDir.setEnabled(true);
					btnSipRegistryBrowse.setEnabled(true);										
					selected = RunMacroTab.MAC_TAB_IMPORT_SIP_REGISTRY;
				} else {
					txtSipRegistryDir.setEnabled(false);
					btnSipRegistryBrowse.setEnabled(false);				
					txtSipRegistryDir.setText("");
				}
			}
		});	

		rel_x	+= 140; 
		
		txtSipRegistryDir	=	new Text(this,SWT.BORDER);
		txtSipRegistryDir.setBounds(rel_x,rel_y,140,20);
		
		txtSipRegistryDir.addModifyListener(new ModifyListener(){
			public void modifyText(ModifyEvent me){
				if(optImportSipRegistry.getSelection() == true) {
					if(txtSipRegistryDir.getText().trim().equals("") || txtSipRegistryDir.getText() == null){					
						parent.enableApplyButton(false);
					} else {
						parent.enableApplyButton(true);
					}
				}
			}
		});
		
		txtSipRegistryDir.setEnabled(false);
		
		rel_x	+= 150;
		btnSipRegistryBrowse	= new Button(this,SWT.PUSH);
		btnSipRegistryBrowse.setText("Browse");
		btnSipRegistryBrowse.setBounds(rel_x,rel_y,50,20);
		btnSipRegistryBrowse.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				FileDialog fileDialog = new FileDialog(new Shell());
				fileDialog.setFilterPath(MFile.getConfigPath());
				fileDialog.setText("Select SIP Registry File");
				String filePath = fileDialog.open(); 
				if((filePath == null) || (filePath.equalsIgnoreCase("") == true)) {
					return;
				}
				txtSipRegistryDir.setText(filePath);
				
			}
		});
		
		btnSipRegistryBrowse.setEnabled(false);

		rel_x 	-= 290;
		rel_y	+= 30;
		optExecuteMss  =	new Button(this,SWT.RADIO);
		optExecuteMss.setBounds(rel_x,rel_y,135,20);
		optExecuteMss.setText("Execute Utility Macro");
		optExecuteMss.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(optExecuteMss.getSelection() == true) {
					txtMssFile.setText("");					
					txtMssFile.setEnabled(true);
					btnBrowseMss.setEnabled(true);										
					selected = RunMacroTab.MAC_TAB_EXECUTE_MSS;
				} else {
					txtMssFile.setEnabled(false);
					btnBrowseMss.setEnabled(false);				
					txtMssFile.setText("");
				}
			}
		});	

		rel_x	+= 140; 
		
		txtMssFile	=	new Text(this,SWT.BORDER);
		txtMssFile.setBounds(rel_x,rel_y,140,20);
		
		txtMssFile.addModifyListener(new ModifyListener(){
			public void modifyText(ModifyEvent me){
				if(optExecuteMss.getSelection() == true) {
					if(txtMssFile.getText().trim().equals("") || txtMssFile.getText() == null){					
						parent.enableApplyButton(false);
					} else {
						parent.enableApplyButton(true);
					}
				}
			}
		});
		
		txtMssFile.setEnabled(false);
		
		rel_x	+= 150;
		btnBrowseMss	= new Button(this,SWT.PUSH);
		btnBrowseMss.setText("Browse");
		btnBrowseMss.setBounds(rel_x,rel_y,50,20);
		btnBrowseMss.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				FileDialog fileDialog = new FileDialog(new Shell());
				fileDialog.setFilterExtensions(new String[]{"*.mss"});
				fileDialog.setFilterPath(MFile.getUpdatesPath());
				fileDialog.setText("Select Utility Macro File");
				String filePath = fileDialog.open(); 
				if((filePath == null) || (filePath.equalsIgnoreCase("") == true)) {
					return;
				}
				txtMssFile.setText(filePath);
			}
		});
		btnBrowseMss.setEnabled(false);

		rel_x 	-= 290;
		rel_y	+= 30;
		optImportConfigScript  =	new Button(this,SWT.RADIO);
		optImportConfigScript.setBounds(rel_x,rel_y,135,20);
		optImportConfigScript.setText("Run Config Import Script");
		optImportConfigScript.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				if(optImportConfigScript.getSelection() == true) {
					txtImportConfigScript.setEnabled(true);
					btnBrowseImportConfigScript.setEnabled(true);										
					selected = RunMacroTab.MAC_TAB_IMPORT_CONFIG_SCRIPT;
				} else {
					txtImportConfigScript.setEnabled(false);
					btnBrowseImportConfigScript.setEnabled(false);				
					txtImportConfigScript.setText("");
				}
			}
		});
		
		rel_x	+= 140; 
		
		txtImportConfigScript =	new Text(this,SWT.BORDER);
		txtImportConfigScript.setBounds(rel_x,rel_y,140,20);
		
		txtImportConfigScript.addModifyListener(new ModifyListener(){
			public void modifyText(ModifyEvent me){
				if(optImportConfigScript.getSelection() == true) {
					if(txtImportConfigScript.getText().trim().equals("") || txtImportConfigScript.getText() == null){					
						parent.enableApplyButton(false);
					} else {
						parent.enableApplyButton(true);
					}
				}
			}
		});
		
		txtImportConfigScript.setEnabled(false);
		
		rel_x	+= 150;
		btnBrowseImportConfigScript = new Button(this,SWT.PUSH);
		btnBrowseImportConfigScript.setText("Browse");
		btnBrowseImportConfigScript.setBounds(rel_x,rel_y,50,20);
		btnBrowseImportConfigScript.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				FileDialog fileDialog = new FileDialog(new Shell());
				fileDialog.setFilterExtensions(new String[]{"*.mns"});
				fileDialog.setFilterPath(MFile.getTemplatePath());
				fileDialog.setText("Select Configuration Import Script File");
				String filePath = fileDialog.open(); 
				if((filePath == null) || (filePath.equalsIgnoreCase("") == true)) {
					return;
				}
				txtImportConfigScript.setText(filePath);
			}
		});
		btnBrowseImportConfigScript.setEnabled(false);
		
	}

	void enableOptions(boolean state) {
		optUpdateNodeFirmware.setEnabled(state);
		optRebootNodes.setEnabled(state);
		optMoveDefault.setEnabled(state);
		optRestoreFactory.setEnabled(state);
		optExecuteMss.setEnabled(state);
	}
	
	private boolean showMoveNodesWnd() {
		if(cmbMove.getItemCount() <= 0) {
			
    		MessageBox msgBox =  new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.ICON_WARNING);
    		String msg;
    		if(parent.getCurrentNetworkType() == MeshNetwork.NETWORK_FIPS_ENABLED) {
    			msg = "No open FIPS 140-2 compliant networks.";
    		}else {
    			msg = "No open networks.";
    		}
    		msgBox.setMessage(msg);
    		msgBox.setText("Error");
    		msgBox.open();
    		
			return false;
		}
        int index 			= cmbMove.getSelectionIndex();
        String newMeshId 	= cmbMove.getItem(index);
        if(newMeshId == null || newMeshId.trim().equals("")) {
    		MessageBox msgBox =  new MessageBox(new Shell(SWT.CLOSE),SWT.CLOSE|SWT.ICON_WARNING);
    		String msg = "Please enter network name.";
    		msgBox.setMessage(msg);
    		msgBox.setText("Error");
    		msgBox.open();
    		return false;
        }
        
		return true;
	}
	
	void setNetworkList(Vector<String> networkList) {
	       
		int index;
		if(networkList.size() == 0) {
		    cmbMove.setEnabled(false);
		    parent.enableApplyButton(false);
		    return;
		}
		cmbMove.setFocus();
	    cmbMove.removeAll();
	    for(index = 0; index<networkList.size(); index++) {
	    	cmbMove.add((String)networkList.get(index));
	    }
	    cmbMove.select(0);
	 }
		
	int getMacTabSelection() {
		return selected;
	}
	
	String getSelectedNetworkName() {
		int index;
		index	= cmbMove.getSelectionIndex();
		if(index < 0) {
			return null;
		}
		return cmbMove.getItem(index);
	}

	/**
	 * @return
	 */
	String getFirmwareDirName() {
		return txtFirmwareDir.getText().trim();
	}
		
	String getSipRegistryFileName() {
		return txtSipRegistryDir.getText().trim();
	}
	
	String getMssFileName() {
		return txtMssFile.getText().trim();
	}
	
	String getImportScriptMacroFileName() {
		return txtImportConfigScript.getText().trim();
	}
}

