package com.meshdynamics.nmsui.dialogs.base;


import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Control;

public class ConfigPage implements IMeshConfigPage {

	private 	CTabItem		tabItem;
	private 	String			caption;
	protected 	Canvas			canvas;		
	protected 	Rectangle		minimumBounds;
	protected 	IMeshConfigBook parent;
	
	public ConfigPage(IMeshConfigBook parent) {
		
		minimumBounds 	= new Rectangle(0,0,400,600);
		this.parent		= parent;
		caption			= "Configuration";
	}
	
	public void createControls(CTabFolder parent) {
		
		tabItem = new CTabItem(parent,SWT.NONE); 
		tabItem.setText(caption);
					
		canvas = new Canvas(parent,SWT.NONE);
		canvas.setBounds(minimumBounds);
		
		tabItem.setControl(canvas);
		
		parent.setSelection(0);
	}

	public Rectangle getMinimumBounds() {
		return minimumBounds;
	}

	public void initalizeLocalData() {
		// TODO Auto-generated method stub

	}

	public void onCancel() {
		// TODO Auto-generated method stub

	}

	public boolean onOk() {
		if(validateLocalData() == false) {
			return false;
		}
		return getDataFromUIToConfiguration();
	}

	public void selectionChanged(boolean selected) {
	}

	public void setCaption(String caption) {
		this.caption = caption;
		
		if(tabItem != null)
			tabItem.setText(caption);
		
	}

	public boolean validateLocalData() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#getDataFromUIToConfiguration()
	 */
	public boolean getDataFromUIToConfiguration() {
		// TODO Auto-generated method stub
		return true;
	}
	
	protected void stayOnSameTab(){
		tabItem.getParent().setSelection(tabItem);	
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#setGrpBounds(org.eclipse.swt.graphics.Rectangle)
	 */
	public void setGrpBounds(Rectangle minimumGrpBounds) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.dialogs.IMeshConfigPage#isFipsFulfilled()
	 */
	public boolean isFipsFulfilled() {
		return true;
	}

	public void setUIAccess(boolean enabled) {
		
		if(this.canvas != null && enabled == false) {
			for(Control c : this.canvas.getChildren()) {
				c.setEnabled(false);
			}
		}
			
	}
	
}
