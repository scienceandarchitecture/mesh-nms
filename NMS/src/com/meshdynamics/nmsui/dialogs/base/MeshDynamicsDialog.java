/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshDynamicsDialog.java
 * Comments : Base Dialog for all dialogs
 * Created  : Feb 19, 2007
 * Author   : Abhijit Ayarekar 
 * 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History  
 * ---------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                         | Author  |
 *  --------------------------------------------------------------------------------
 * |  4  |Oct 2 ,2007  | displose listener added to shell                 |Imran    |
 *  --------------------------------------------------------------------------------
 * |  3  |Jul 30,2007  | setCursor method added			                  |Abhishek |
 *  --------------------------------------------------------------------------------
 * |  2  |Jun 1, 2007  | Save As button added			                  |Imran    |
 *  --------------------------------------------------------------------------------
 * |  1  |May 1, 2007  | CreateControlsWithApply is added                 |Abhishek|
 * --------------------------------------------------------------------------------
 * |  0  |Feb 19, 2007  | Created                                         | Abhijit |
 * ---------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.dialogs.base;



import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.nmsui.Branding;
import com.meshdynamics.nmsui.dialogs.UIConstants;


public abstract class MeshDynamicsDialog extends EnterEscapeDialog {

	protected final static int BUTTON_DISPLAY_MODE_OK_CANCEL 		= 0;
	protected final static int BUTTON_DISPLAY_MODE_OK_CANCEL_APPLY 	= 1;
	protected final static int BUTTON_DISPLAY_MODE_OK 				= 2;
	protected final static int BUTTON_DISPLAY_MODE_WIZARD 			= 3;	// show back,next and apply/finish
	protected final static int BUTTON_DISPLAY_MODE_NONE				= 4;
		
	private Shell 				shell;
	private Display 			display;
	private Label 				statusBar;
	
	private Button 				btnOk;
	private Button 				btnCancel;
	private Button				btnApply;
	private Button				btnPrevious;
	private Button				btnNext;
	private Button				btnSaveAs;
	
	private int					status;
	private boolean				isDirty;
	private boolean 			statusbarEnabled;
	private boolean				saveAsEnabled;
	private boolean 			closeEnabled;
	private int 				buttonDisplayMode;
	private boolean				ignoreDefaultReturnPress;
	
	private void initialize() {
		
		display						= Display.getDefault();
	    status						= SWT.NONE;
	    statusbarEnabled			= false;
	    saveAsEnabled				= false;
	    closeEnabled				= true;
	    ignoreDefaultReturnPress	= false;
	    
	    if(isModal()) {
	    	shell 		= new Shell(display,SWT.CLOSE|SWT.APPLICATION_MODAL);
	    } else {
			shell 		= new Shell(display,SWT.CLOSE);
	    }
	    
	}
	
	public MeshDynamicsDialog(){
		initialize();
	}
	
	
	public final void setTitle(String newTitle) {
		shell.setText(newTitle);
	}

	protected final void enableOkButton(boolean enable) {
		btnOk.setEnabled(enable);
	}
	
	protected final void enableCancelButton(boolean enable) {
		btnCancel.setEnabled(enable);
	}

	protected final void enablePreviousButton(boolean enable) {
		btnPrevious.setEnabled(enable);
	}
	
	protected final void enableNextButton(boolean enable) {
		btnNext.setEnabled(enable);
	}
	
	public final void enableApplyButton(boolean enable) {
		btnApply.setEnabled(enable);
	}
	
	protected final void enableSaveAsButton(boolean enable) {
		btnSaveAs.setEnabled(enable);
	}
	
	protected final void setOkButtonText(String newText) {
		btnOk.setText(newText);
	}
	
	protected final void setCancelButtonText(String newText) {
		btnCancel.setText(newText);
	}
	
	protected final String getCancelButtonText() {
		return btnCancel.getText();
	}
	
	protected final void setApplyButtonText(String newText) {
		btnApply.setText(newText);
	}
	
	protected final void setSaveAsButtonTaxt(String newText) {
		btnSaveAs.setText(newText);
	}
	
	protected void showStatusbar(boolean show){
		statusbarEnabled = show;
	}

	public int getStatus() {
	    return status;
	}
	
	protected void showSaveAsButton(boolean show){
		saveAsEnabled = show;
	}
	
	protected void enableCloseButton(boolean enable) {
		closeEnabled = enable;
	}
	
	protected void setDefaultButton(Button defaultButton) {
		shell.setDefaultButton(defaultButton);
	}
	
	protected void ignoreDefaultReturnKeyPress(boolean ignore) {
		this.ignoreDefaultReturnPress = ignore;
	}
	
	/**
	 * 
	 */
	public  void  show() {
		
		Rectangle shellBounds = new Rectangle(0,0,0,0);
		modifyMinimumBounds(shellBounds);
		shellBounds.width 	+= 20;
		
		if(this.buttonDisplayMode != BUTTON_DISPLAY_MODE_NONE)
			shellBounds.height 	+= 80;
		else
			shellBounds.height 	+= 30;
		
		if(statusbarEnabled == true) {
			shellBounds.height 	+= 25;
		}
		
		shell.setBounds(shellBounds);
		createButtons();
		createBasicControls();	
		if(display == null)
			display = Display.getCurrent();
		
		Monitor primary 	= display.getPrimaryMonitor();
	    Rectangle rect1 	= primary.getBounds();
	    Rectangle rect2		= shell.getBounds();
	    
	    int x = (rect1.width - rect2.width)/2;
		int y = (rect1.height - rect2.height)/2;
		shell.setLocation(x,y);
		
		shell.setImage(Branding.getIconImage());
		if(closeEnabled == true) {
			addKeyListenersToControls(shell.getChildren());
		}
		shell.addDisposeListener(new DisposeListener() {

			public void widgetDisposed(DisposeEvent arg0) {
				onShellDispose();
			}
	    	
	    });
		shell.open();
		shell.setMaximized(false);
		shell.addShellListener(new ShellAdapter(){
	        public void shellClosed(ShellEvent arg0) {
	        	arg0.doit = onShellExit(); 
	    }});
	            
		while(!shell.isDisposed()){
			if(!display.readAndDispatch()){
				display.sleep();
			}
		}	
	}
	
	/**
	 * @param controls
	 * 
	 */
	private void addKeyListenersToControls(Control[] childrenItems) {
		
		for(int i = 0; i < childrenItems.length; i++) {
			childrenItems[i].addKeyListener(this);
			if(childrenItems[i] instanceof Composite) {
				Control[] grandChildrenItems = ((Composite)childrenItems[i]).getChildren();
				addKeyListenersToControls(grandChildrenItems);
			}
		}
	}

	private void createButtons() {
	
		if(buttonDisplayMode == BUTTON_DISPLAY_MODE_WIZARD) {
			createWizardButtons();
		} else {
			createOkCancelApplyButtons();
		}
		
		if(statusbarEnabled == true) {
			createStatusbar();
		}
		
		if(saveAsEnabled == true) {
			createSaveAsButton();
		}

	}

	/**
	 * 
	 */
	private void createBasicControls() {
		
		int canvasHeight = (statusbarEnabled == true) ?
				shell.getBounds().height - 95 : shell.getBounds().height- 70;
		
		if(this.buttonDisplayMode == BUTTON_DISPLAY_MODE_NONE)
			canvasHeight = shell.getBounds().height;
		
		Canvas 	mainCnv = new Canvas(shell, SWT.NONE);
		mainCnv.setBounds(0,0,shell.getBounds().width,canvasHeight);
		mainCnv.addKeyListener(new KeyListener (){
			public void keyPressed(KeyEvent arg0) {
			
				if(arg0.keyCode == SWT.ESC){
					onCancelClick();  
				}
				if(arg0.keyCode == SWT.CR ){
					onSaveClick(); 
				}
			}

			public void keyReleased(KeyEvent arg0) {
			}
		} );

		createControls(mainCnv);	
	}

	private void createStatusbar() {
    
		Rectangle shellBounds = shell.getBounds();

		statusBar = new Label(shell,SWT.BORDER);	
		statusBar.setBounds(4, shellBounds.height-50, shellBounds.width - 13, 15);
		statusBar.setForeground(UIConstants.BLUE_COLOR);
		
	}
	
	/*
	private void createImportExportButtons() {
		
		Rectangle shellBounds 	= shell.getBounds();
		int 		yOffset		= (statusbarEnabled == true) ? 
									shellBounds.height - 85 : shellBounds.height - 60;
		int 		height		= 20;
		int 		width 		= 50;
		
		btnImport = new Button(shell,SWT.PUSH);
		btnImport.setText("Import"); //$NON-NLS-1$
		btnImport.setBounds(shellBounds.x + 20, yOffset,width,height);
		btnImport.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				onImport();				
			}
		});

		btnExport = new Button(shell,SWT.PUSH);
		btnExport.setText("Export"); //$NON-NLS-1$
		btnExport.setBounds(shellBounds.x + 80, yOffset,width,height);
		btnExport.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				onExport();				
			}
		});
	}
*/
	private void createOkCancelApplyButtons() {

		Rectangle 	shellBounds = shell.getBounds();
		int 		yOffset		= (statusbarEnabled == true) ? 
										shellBounds.height - 85 : shellBounds.height - 60;
		int 		height		= 20;
		int 		width 		= 50;

		if(buttonDisplayMode == BUTTON_DISPLAY_MODE_NONE) {
			return;
		}
		
		btnOk = new Button(shell,SWT.PUSH);
		btnOk.setText("OK"); //$NON-NLS-1$
		btnOk.setBounds(shellBounds.width - 190,yOffset,width,height);
		btnOk.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
					onSaveClick();
			}
		});
		
		if(buttonDisplayMode == BUTTON_DISPLAY_MODE_OK) {
			btnOk.setBounds(shellBounds.width - 70,yOffset,width,height);
			return;
		}
		
		btnCancel = new Button(shell,SWT.PUSH);
		btnCancel.setText("Cancel"); //$NON-NLS-1$
		btnCancel.setBounds(shellBounds.width - 130,yOffset,width,height);
		btnCancel.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
					onCancelClick();
			}
		});
		
		if(buttonDisplayMode == BUTTON_DISPLAY_MODE_OK_CANCEL) {
			btnOk.setBounds(shellBounds.width - 130,yOffset,width,height);
			btnCancel.setBounds(shellBounds.width - 70,yOffset,50,20);
			return;
		}
		
		
		btnApply = new Button(shell,SWT.PUSH);
		btnApply.setText("Apply"); //$NON-NLS-1$
		btnApply.setBounds(shellBounds.width - 70, yOffset,width,height);
		btnApply.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				onApplyClick();				
			}
		});
		
	}

	private void createWizardButtons() {
		Rectangle 	shellBounds = shell.getBounds();
		int 		yOffset		= (statusbarEnabled == true) ? 
										shellBounds.height - 85 : shellBounds.height - 60;
		int 		height		= 20;
		int 		width 		= 50;
		
		btnPrevious = new Button(shell,SWT.PUSH);
		btnPrevious.setText("Back"); //$NON-NLS-1$
		btnPrevious.setBounds(shellBounds.width - 250,yOffset,width,height);
		btnPrevious.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
					onPrevious();
			}
		});

		btnNext = new Button(shell,SWT.PUSH);
		btnNext.setText("Next"); //$NON-NLS-1$
		btnNext.setBounds(shellBounds.width - 190,yOffset,width,height);
		btnNext.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
					onNext();
			}
		});
		
		btnApply = new Button(shell,SWT.PUSH);
		btnApply.setText("Apply"); //$NON-NLS-1$
		btnApply.setBounds(shellBounds.width - 130, yOffset,width,height);
		btnApply.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				onApplyClick();				
				if(status == SWT.OK)
					shell.dispose();
			}
		});
		btnApply.setText("Finish"); //$NON-NLS-1$
		shell.setDefaultButton(btnApply);
		
		btnCancel = new Button(shell,SWT.PUSH);
		btnCancel.setText("Cancel"); //$NON-NLS-1$
		btnCancel.setBounds(shellBounds.width - 70,yOffset,width,height);
		btnCancel.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
					onCancelClick();
			}
		});
		
	}
	
	private void createSaveAsButton() {
		Rectangle shellBounds 	= shell.getBounds();
		int 		yOffset		= (statusbarEnabled == true) ? 
									shellBounds.height - 85 : shellBounds.height - 60;
		int 		height		= 20;
		int 		width 		= 50;
		
		btnSaveAs = new Button(shell,SWT.PUSH);
		btnSaveAs.setText("SaveAs"); //$NON-NLS-1$
		btnSaveAs.setBounds(shellBounds.x + 10, yOffset,width,height);
		btnSaveAs.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
					onSaveAsClick();
			}
		});
	}

	
	public void showErrorMessage(String message,int style) {
		//TODO create and show MDMessagebox
		 MessageBox msg = new MessageBox(new Shell(SWT.CLOSE),SWT.OK|style);
	     msg.setMessage(""+message); //$NON-NLS-1$
	     msg.setText("NetworkViewer" + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor); //$NON-NLS-1$ //$NON-NLS-2$
	     msg.open();
	}
    
    private void onSaveClick() {
		if(onOk()) {
			shell.dispose();
			status = SWT.OK;
		}
	}
	
    private boolean onShellExit() {
		if(closeEnabled == false) {
			return false;
		}
		status	= SWT.CANCEL;
		return onCancel();
	}

    private void onApplyClick() {
		if(onApply()) {
			status = SWT.OK;
		}
	}
    
    private void onCancelClick() {
		if(onCancel()) {
			shell.dispose();
			status = SWT.CANCEL;
		}
	}
	
    protected void onPrevious() {
    	
    }
    
    protected void onNext() {
    	
    }
    
    private void onSaveAsClick() {
    	if(onSaveAs()) {
			status = SWT.OK;
		}
	}
    protected void  onShellDispose(){
    	
    }
    
	protected void setIsDirty(boolean value){
		isDirty = value;
		if(statusbarEnabled == false)
			return;
	}
	
	protected boolean isShellDisposed() {
		return shell.isDisposed();
	}
	
	protected boolean isDirty() {
		return isDirty;
	}

	protected boolean isModal() {
		return true;
	}
	
	protected void showStatusText(String statusText) {
		statusBar.setText(statusText);
	}

	protected void setButtonDisplayMode(int buttonDisplayMode) {
		this.buttonDisplayMode = buttonDisplayMode;
	}

	public boolean isDisposed() {
		return shell.isDisposed();
	}

	protected void setCursor(Cursor cursor) {
		shell.setCursor(cursor);
	}
	
	protected  boolean  onSaveAs(){
		return true;
	}
	
	public void keyPressed(KeyEvent arg0) {
		
		if(arg0.keyCode == SWT.CR && ignoreDefaultReturnPress == false) {
			onSaveClick();
		}else if(arg0.keyCode == SWT.ESC) {
			onCancelClick();
		}
	}
	
	protected abstract void	  	modifyMinimumBounds(Rectangle bounds);
	protected abstract void 	createControls(Canvas mainCnv);
	protected abstract boolean 	onOk();
	protected abstract boolean 	onCancel();
	
	protected boolean	onApply() {
		return false;
	}
	
	protected  boolean 	onImport() {
		return false;
	}
	
	protected  boolean 	onExport() {
		return false;
	}

	
		
}