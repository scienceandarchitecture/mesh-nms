package com.meshdynamics.nmsui.dialogs.base;

import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Rectangle;


public interface IMeshConfigPage {

	public Rectangle	getMinimumBounds();
	
	public void 		setCaption(String caption);
	
	public void 		createControls(CTabFolder tabFolder);

	public void 		initalizeLocalData  ();

	public void 		selectionChanged(boolean selected);

	public boolean 		onOk();

	public void 		onCancel();
	
	public boolean		validateLocalData();
	
	public boolean		getDataFromUIToConfiguration();
	
	public void 		setGrpBounds(Rectangle minimumGrpBounds);

	public boolean      isFipsFulfilled();
	
	public void 		setUIAccess(boolean enabled);
}
