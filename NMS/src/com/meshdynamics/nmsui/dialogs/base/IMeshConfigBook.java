package com.meshdynamics.nmsui.dialogs.base;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;

public interface IMeshConfigBook extends IMessage {

	public static final int CONFIG_MODE_NODE		= 1;
	public static final int CONFIG_MODE_TEMPLATE	= 2;
	
	public IConfiguration 	getConfiguration();
	public int 				getConfigMode();

	public void 			dataModified();
	public boolean 			canDeleteVlan(int tag);
		
	public boolean			isRfConfigChanged();	
	
	public MeshNetwork		getMeshNetWork();
	public String			getIfEssIdByName(String ifName);
	
	public IVersionInfo		getVersionInfo();
	
	public void 			essIdChangedForInterface(String ifName);
	public boolean 			isCountryCodeChanged();
	
	public void 			notifyWPAPersonalKeyRegeneratedFor(String ifName);
	public void 			notifySecurityConfigurationChangedFor(String ifName);
	public void             notifyDCAListChanged(String ifName);
	public void             notifyDCAListRemove(String ifName);
	public IInterfaceInfo   getInterfaceInfoByName(String ifName);
}
