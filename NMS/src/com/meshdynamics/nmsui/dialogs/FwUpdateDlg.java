package com.meshdynamics.nmsui.dialogs;

import java.io.File;
import java.net.InetAddress;
import java.util.Enumeration;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IApFwUpdateListener;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.util.Event;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

public class FwUpdateDlg extends MeshDynamicsDialog implements IApFwUpdateListener {

    private Button			 	btnStart;
    private Table				tblAPs;
    private Label               label;
    private ProgressBar			pbNode;
    private ProgressBar			pbTotal;
    private String 				strUpdateFolder;
    private Label 				lblNodeProgress;
    private Label 				lblTotalProgress;
    private Button				btnRetry;
	private Vector<AccessPoint>	apVector;
	private MeshNetwork			meshNetwork;
	private Enumeration<String> enumMacStr;
	private Canvas				mainCnv;
	private Group 				grpProgress;
 	private Thread				updaterThread;
 	private boolean				stopUpdate;
 	
 	private AccessPoint			currentAp;
 	private TableItem			currentTableItem;
 	private Event				updateEvent;
 	
	public FwUpdateDlg(String updateFolder, MeshNetwork meshNetwork, Enumeration<String> enumMacStr) {
		this.strUpdateFolder	= updateFolder;
		this.meshNetwork 		= meshNetwork;
		this.enumMacStr			= enumMacStr;
		apVector 				= new Vector<AccessPoint>();
		updateEvent				= new Event();
		updateEvent.setEvent();
		showStatusbar(true);
	}
	
	@Override
	protected void createControls(Canvas mainCnv) {
		
		this.mainCnv = mainCnv;
		enableOkButton(false);
		setCancelButtonText("Close");
		
		Group grp = new Group(mainCnv,SWT.NONE);
		grp.setBounds(10,5,475,350);
		
		label = new Label(grp,SWT.BOLD);
		label.setText("Press Start to begin.");
		label.setBounds(15, 13, 210, 20);
		
		btnStart = new Button(grp,SWT.PUSH);
		btnStart.setBounds(15, 33, 75, 22);
		btnStart.setText("Start");
		btnStart.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent arg0) {
				handleStartUpdate(apVector);
			}
		});
		
		btnRetry = new Button(grp,SWT.PUSH);
		btnRetry.setBounds(95, 33, 75, 22);
		btnRetry.setText("Retry");
		btnRetry.addSelectionListener(new SelectionAdapter(){
            public void widgetSelected(SelectionEvent arg0) {
                handleRetry();
            }
		});
		
		tblAPs = new Table(grp,SWT.BORDER|SWT.V_SCROLL|SWT.SINGLE|SWT.FULL_SELECTION);
		tblAPs.setBounds(15,75,445,150);
		tblAPs.setLinesVisible(true);
		tblAPs.setHeaderVisible(true);		
		
		TableColumn tableColumn = new TableColumn(tblAPs,SWT.LEFT);
		tableColumn.setText("Node Address");
		tableColumn.setWidth(150);
		
		tableColumn = new TableColumn(tblAPs,SWT.LEFT);
		tableColumn.setText("IP Address");
		tableColumn.setWidth(120);
		
		tableColumn = new TableColumn(tblAPs,SWT.LEFT);
		tableColumn.setText("Status");
		tableColumn.setWidth(200);		
		
		grpProgress = new Group(grp,SWT.NONE);
		grpProgress.setBounds(15,235,445,100);
		grpProgress.setText("Progress");

		label = new Label(grpProgress,SWT.BOLD);
		label.setText("Node");
		label.setBounds(8, 32, 30, 20);
		
		pbNode = new ProgressBar(grpProgress, SWT.NONE);
		pbNode.setBounds(40, 30, 365, 20);

		lblNodeProgress = new Label(grpProgress,SWT.BOLD|SWT.RIGHT);
		lblNodeProgress.setText("0%");
		lblNodeProgress.setBounds(410, 32, 30, 20);

		label = new Label(grpProgress,SWT.BOLD);
		label.setText("Total");
		label.setBounds(8, 62, 30, 20);
		
		pbTotal = new ProgressBar(grpProgress, SWT.NONE);
		pbTotal.setBounds(40, 60, 365, 20);		
	
		lblTotalProgress = new Label(grpProgress,SWT.BOLD|SWT.RIGHT);
		lblTotalProgress.setText("0%");
		lblTotalProgress.setBounds(410, 62, 30, 20);
		
		setTitle("Over-the-Air Firmware Update [Verifying available units...]");
		setCursor(Display.getDefault().getSystemCursor(SWT.CURSOR_WAIT));
		setAllControls(false);
		Display.getDefault().asyncExec(new Runnable(){
			public void run() {
				setupData();
			}
		});
		
	}

	protected void handleRetry() {
	
		if(tblAPs.getSelectionCount() <= 0) {
			handleStartUpdate(apVector);
			return;
		}

		if(tblAPs.getSelectionCount() <= 0) {
			MessageBox msgBox = new MessageBox(new Shell(), SWT.OK);
			msgBox.setText("No accesspoint selected");
			msgBox.setMessage("Please select accesspoint for retrying.");
			msgBox.open();
			return;
		}
		
		Vector<AccessPoint> aps = new Vector<AccessPoint>();
		for(TableItem item : tblAPs.getSelection()) {
			AccessPoint ap = (AccessPoint)item.getData();
			if(ap == null)
				continue;
			
			aps.add(ap);
		}
		
		if(aps.size() > 0) {
			handleStartUpdate(aps);
			return;
		}

		MessageBox msgBox = new MessageBox(new Shell(), SWT.OK);
		msgBox.setText("No accesspoint available");
		msgBox.setMessage("Selection does not contain accesspoints available for update.");
		msgBox.open();
		return;
		
	}

	protected void handleStartUpdate(Vector<AccessPoint> aps) {
		btnStart.setEnabled(false);
		btnRetry.setEnabled(false);
		enableOkButton(false);
		setCancelButtonText("Cancel");
		notifyApFileUpdate(-1, 0);
		notifyTotalUpdate(-1, 0);
		startUpdate(aps, this);
	}

	protected void incrementTotalUpdateCount() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
	    		pbTotal.setSelection(pbTotal.getSelection() + 1);
	    		lblTotalProgress.setText((pbTotal.getSelection() * 100)/pbTotal.getMaximum() + "%");
			}
		});
	}
	
	protected void startUpdate(final Vector<AccessPoint> aps, final IApFwUpdateListener listener) {
		
		stopUpdate = false;
		updateEvent.resetEvent();
		
		updaterThread = new Thread(new Runnable() {
			public void run() {
				int ret;
				int apNo = 0;
				
				notifyTotalUpdate(-1, aps.size());
				for(AccessPoint ap :aps) {
					
					setCurrentSelectedAp(ap);
					notifyUpdateStatus(Mesh.FW_UPDATE_RETURN_SUCCESS_UPDATE_STATUS, "In Progress...");
					ret = ap.updateFirmware(getUpdateFileName(ap.getDSMacAddress()), listener);
					switch(ret) {
						case Mesh.FW_UPDATE_RETURN_SUCCESS:
							notifyUpdateStatus(ret, "Successful");
							ap.showMessage(ap,Mesh.MACRO_FIRMWARE_UPDATE, Mesh.STATUS_SUCCESSFUL, Mesh.STATUS_UPDATE_AFT_REBOOT);
							break;
						case Mesh.FW_UPDATE_RETURN_FAILED:
							notifyUpdateStatus(ret, "Failed");
							ap.showMessage(ap,Mesh.MACRO_FIRMWARE_UPDATE, Mesh.STATUS_FAILED, "");
							break;
						case Mesh.FW_UPDATE_RETURN_ERROR_FW_REQ_FAILED:
							notifyUpdateStatus(ret, "Failed");
							ap.showMessage(ap,Mesh.MACRO_FIRMWARE_UPDATE, Mesh.STATUS_FAILED, Mesh.STATUS_FAILED_FIRMWAREREQ);
							break;
						case Mesh.FW_UPDATE_RETURN_ERROR_FW_FILE_OPEN:
							notifyUpdateStatus(ret, "Failed");
							ap.showMessage(ap,Mesh.MACRO_FIRMWARE_UPDATE, Mesh.STATUS_FAILED, Mesh.STATUS_ERROR_FIRMWARE_OPEN);
							break;
						case Mesh.FW_UPDATE_RETURN_ERROR_CONNECTION:
							notifyUpdateStatus(ret, "Failed");
							ap.showMessage(ap,Mesh.MACRO_FIRMWARE_UPDATE, Mesh.STATUS_FAILED, Mesh.STATUS_FAILED_CONNECTION);
							break;
						case Mesh.FW_UPDATE_RETURN_SUCCESS_INIT_REMOTE_UPDATE:
							notifyUpdateStatus(ret, "Remote Update Initiated");
							ap.showMessage(ap,Mesh.MACRO_FIRMWARE_UPDATE, "", "Remote Update Initiated");
							break;
					}
					notifyTotalUpdate(apNo++, aps.size());
					
					if(stopUpdate == true) {
						break;
					}
				}
				
				notifyUpdatesDone();
				updateEvent.setEvent();
			}
		});
		updaterThread.start();
	}

	protected void setCurrentSelectedAp(AccessPoint ap) {
		currentAp 			= ap;
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				for(TableItem item : tblAPs.getItems()) {
					if(item.getText().equals(currentAp.getDSMacAddress()) == true) {
						currentTableItem = item;
						grpProgress.setText("Updating " + currentAp.getDSMacAddress());
						return;
					}
				}
			}
		});
	}

	private void setAllControls(boolean enableDisable) {
		for(Control c : mainCnv.getChildren()) {
			c.setEnabled(enableDisable);
		}
	}
	
    private void setupData() {

    	tblAPs.clearAll();
    	apVector.clear();
    	
		MacAddress	apMac = new MacAddress();
		while(enumMacStr.hasMoreElements()) {
			String strMacAddress = enumMacStr.nextElement(); 
			apMac.setBytes(strMacAddress);
			AccessPoint ap = meshNetwork.getAccessPoint(apMac);
			if(ap == null)
				continue;

			TableItem item = new TableItem(tblAPs,SWT.NONE);
			item.setText(0, ap.getDSMacAddress());
			
			boolean isReachable = true;
			String 	txt = "";
			if(ap.isLocal() == false) {
				txt = "(Remote) " + ap.getIpAddress().toString();
			} else {
				IpAddress nodeIpAddress = ap.getIpAddress();
				try {
					InetAddress inetAddress = InetAddress.getByAddress(nodeIpAddress.getBytes());
					isReachable = inetAddress.isReachable(10000);
				} catch (Exception e) {
					System.out.println(e.getMessage());
					isReachable = false;
				}
				
				if(isReachable == false) {
					item.setText(1, "Unreachable");
					item.setText(2, "IP Address not reachable.");
					item.setForeground(UIConstants.GRAY_COLOR);
					ap.showMessage(ap,Mesh.MACRO_FIRMWARE_UPDATE,Mesh.STATUS_FAILED, "IP Address Unreachable.");
					continue;
				}
				
				txt = nodeIpAddress.toString();
			}
			item.setText(1, txt);
			
			if(verifyUpdateFilePresent(strMacAddress) == false) {
				item.setText(2, "Firmware files not found.");
				item.setForeground(UIConstants.GRAY_COLOR);
				ap.showMessage(ap,Mesh.MACRO_FIRMWARE_UPDATE,Mesh.STATUS_FAILED, "Firmware file not found.");
				continue;
			}
			
			IVersionInfo versionInfo = ap.getVersionInfo();
			if(versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,
												IVersionInfo.MINOR_VERSION_4,
												IVersionInfo.VARIANT_VERSION_94) == false) {
				item.setText(2, "Firmware update not supported.");
				item.setForeground(UIConstants.GRAY_COLOR);
				ap.showMessage(ap,Mesh.MACRO_FIRMWARE_UPDATE,Mesh.STATUS_FAILED, "Firmware file not found.");
				continue;
			}

			item.setText(2, "Ready for Update.");
			item.setData(ap);

			apVector.add(ap);
		}
    	
		setTitle("Over-the-Air Firmware Update [" + apVector.size() + " unit(s)]");
		setAllControls(true);
		setCursor(Display.getDefault().getSystemCursor(SWT.CURSOR_ARROW));
		if(apVector.size() <= 0) {
			btnStart.setEnabled(false);
			btnRetry.setEnabled(false);
		}
	}
	
    private String getUpdateFileName(String strMacAddress) {
		String fileName = strMacAddress.replace(':', '_');
		fileName += ".zip";
		fileName = strUpdateFolder + "/firmware_" + fileName;
		return fileName;
    }
    
	private boolean verifyUpdateFilePresent(String strMacAddress) {
		
		String fileName = getUpdateFileName(strMacAddress);
		File f = new File(fileName);
		if(f.exists() == false)
			return false;
		
		return true;
	}

	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x 		= 100;
		bounds.y 		= 100;
		bounds.width 	= 480;
		bounds.height 	= 350;
	}

	@Override
	protected boolean onCancel() {
		if(updateEvent.WaitForSingleObject(1) == Event.NOTIFY)
			return true;
		
		stopUpdate = true;
		
		MessageBox msgBox = new MessageBox(new Shell(), SWT.OK);
		msgBox.setText("Cancelling further updates");
		msgBox.setMessage("Current update cannot be cancelled.Further updates will be cancelled after current update is finished.");
		msgBox.open();
		return false;
	}

	@Override
	protected boolean onOk() {
		return true;
	}

	@Override
	public void notifyUpdateStatus(final int ret, final String status) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				
				showStatusText(status);
				
				if(ret == Mesh.FW_UPDATE_RETURN_SUCCESS_UPDATE_STATUS)
					return;
				if(currentTableItem == null)
					return;
				if(currentTableItem.isDisposed() == true)
					return;
				currentTableItem.setText(2, status);
				
			}
		});
	}

	@Override
	public void notifyApFileUpdate(final int currentFileNo, final int totalFileCount) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				if(currentFileNo == -1) {
		    		pbNode.setMinimum(0);
		    		pbNode.setMaximum(totalFileCount);
		    		pbNode.setSelection(0);
				}
				
				pbNode.setSelection(currentFileNo+1);
				lblNodeProgress.setText((pbNode.getSelection() * 100)/pbNode.getMaximum() + "%");
				
			}
		});
	}

	private void notifyTotalUpdate(final int currentApNo, final int totalApCount) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				if(currentApNo == -1) {
		    		pbTotal.setMinimum(0);
		    		pbTotal.setMaximum(totalApCount);
		    		pbTotal.setSelection(0);
				}
				
				pbTotal.setSelection(currentApNo + 1);
				lblTotalProgress.setText((pbTotal.getSelection() * 100)/pbTotal.getMaximum() + "%");
			}
		});
	}
	
	private void notifyUpdatesDone() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				enableOkButton(true);
				btnRetry.setEnabled(true);
				btnStart.setEnabled(true);
				btnStart.setText("Restart All");
			}
		});
	}
	
}
