package com.meshdynamics.nmsui.dialogs;

import java.io.PrintStream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Text;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.nmsui.MeshViewerUI;
import com.meshdynamics.nmsui.dialogs.base.MeshDynamicsDialog;
import com.meshdynamics.util.MacAddress;

public class ScriptOutputDlg extends MeshDynamicsDialog {

	private Text 			txtStdOut;
	private MeshViewerUI 	meshViewerUI;
	private String 			importScriptFileName; 
	private String[] 		dsAddresses;
	private MeshNetwork 	meshNetwork;
	private boolean			canClose;
	
	public ScriptOutputDlg(MeshViewerUI meshViewerUI, String importScriptFileName, 
			String[] dsAddresses,MeshNetwork meshNetwork) {
		super();   
	    super.setTitle("Script output console");
	    super.setButtonDisplayMode(MeshDynamicsDialog.BUTTON_DISPLAY_MODE_NONE);
	    this.meshViewerUI 			= meshViewerUI;
	    this.importScriptFileName 	= importScriptFileName;
	    this.dsAddresses 			= dsAddresses;
	    this.meshNetwork 			= meshNetwork;
	    this.canClose				= true;
	}
	
	@Override
	protected void createControls(Canvas mainCnv) {
		txtStdOut = new Text(mainCnv, SWT.MULTI|SWT.BORDER|SWT.H_SCROLL|SWT.V_SCROLL|SWT.WRAP);
		txtStdOut.setBackground(UIConstants.BLACK_COLOR);
		txtStdOut.setForeground(UIConstants.GREEN_COLOR);
		txtStdOut.setBounds(0, 0, 615, 400);

		Thread t = new Thread(new Runnable(){
			@Override
			public void run() {
				canClose = false;
				executeScripts();
				canClose = true;
			}
		});
		t.start();
	}

	@Override
	protected void modifyMinimumBounds(Rectangle bounds) {
		bounds.x		= 0;
		bounds.y		= 0;
		bounds.width 	= 600;
		bounds.height 	= 400;
	}

	@Override
	protected boolean onCancel() {
		return canClose;
	}

	@Override
	protected boolean onOk() {
		return canClose;
	}

	private void executeScripts() {

		PrintStream 	originalSysOut = System.out;
		System.setOut(new PrintStream(System.out){
			@Override
			public void print(final String s) {
				MeshViewerUI.display.asyncExec(new Runnable(){
					@Override
					public void run() {
						if(txtStdOut.isDisposed() == false)
							txtStdOut.append(s + "\n");
					}
				});
			}
		});
		
		boolean[] ret = meshViewerUI.importConfigScript(importScriptFileName, 
														meshNetwork.getNwName(),	
														dsAddresses);
		MacAddress dsMac = new MacAddress();
		for(int i=0;i<ret.length;i++) {
			dsMac.setBytes(dsAddresses[i]);
			AccessPoint ap = meshNetwork.getAccessPoint(dsMac);
			if(ret[i] == true){
				meshViewerUI.showMessage(ap, "Configuration Import", "Successful", "" );
			    ap.isRebootRequired();
			    meshViewerUI.showMessage(ap,Mesh. MACRO_IMPORT_UPDATE,Mesh.STATUS_REBOOTREQD,"" );
			}
			else
				meshViewerUI.showMessage(ap, "Configuration Import", "Failed", "");	
		}
		
		System.setOut(originalSysOut);
	}

}
