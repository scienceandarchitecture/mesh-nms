/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Tile.java
 * Comments : Tile class represents a particular square image of world bitmap for a zoom 
 * Created  : Jan 10, 2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Jan 10, 2008 | Created                                         |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.maps;


import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;


/**
 * The Tile class represents a particular square image
 * piece of the world bitmap at a particular zoom level.
 */

public class Tile {
    
    public final static int PRIORITY_HIGH 	= 1;
    public final static int PRIORITY_LOW 	= 2;
    private Listener uniqueListener 		= null;
    private boolean loading = false;
    
    public int priority; 
    
    //Most Recently Accessed Tiles. These are strong references, to prevent reloading
    //of most recently used tiles.
    //private static final Map<URI, BufferedImage> recentlyAccessed = new HashMap<URI, BufferedImage>();
    //private static final TileCache cache = new TileCache();
    boolean isLocal;
    
    /**
     * If an error occurs while loading a tile, store the exception
     * here.
     */
    Throwable error;
    
    /**
     * The url of the image to load for this tile
     */
    private String url;
    
    /**
     * Indicates that loading has succeeded. A PropertyChangeEvent will be fired
     * when the loading is completed
     */
    
    private boolean loaded = false;
    /**
     * The zoom level this tile is for
     */
    private int zoom, x, y;
    public int dx,dy;
    
    /**
     * The image loaded for this Tile
     */
    //SoftReference image = new SoftReference(null);
    
    Image image;
    
    /**
     * Create a new Tile at the specified tile point and zoom level
     * @param location
     * @param zoom
     */
    public Tile(int x, int y, int zoom) {
        loaded = false;
        this.zoom = zoom;
        this.x = x;
        this.y = y;
        isLocal	= true;
    }
    
    /**
     * Create a new Tile that loads its data from the given URL. The URL must
     * resolve to an image
     */
    Tile(int x, int y, int zoom, String url, int priority, DefaultTileFactory dtf) {
        this.url = url;
        loaded = false;
        this.zoom = zoom;
        this.x = x;
        this.y = y;
        this.priority = priority;
        this.dtf = dtf;
        //startLoading();
        isLocal	= true;
    }
    
    /**
     *
     * Indicates if this tile's underlying image has been successfully loaded yet.
     * @returns true if the Tile has been loaded
     * @return
     */
    public synchronized boolean isLoaded() {
        return loaded;
    }
    
    /**
     * Toggles the loaded state, and fires the appropriate property change notification
     */
    synchronized void setLoaded(boolean loaded) {
        this.loaded = loaded;
        if(loaded == true) {
        	loading = false;
            notifyListener();
        }
    }
    
    public void notifyListener() {
    	if(uniqueListener == null)
    		return;
    	
        final Tile thisTile = this; 
        Display.getDefault().asyncExec(new Runnable() {

            public void run() {
                Event evt 	= new Event();
                evt.data	= thisTile;
                uniqueListener.handleEvent(evt);
            }});
    }
    
    /**
     * Returns the last error in a possible chain of errors that occured during
     * the loading of the tile
     */
    public Throwable getUnrecoverableError() {
        return error;
    }
    
    /**
     * Returns the Throwable tied to any error that may have ocurred while
     * loading the tile. This error may change several times if multiple
     * errors occur
     * @return
     */
    public Throwable getLoadingError() {
        return error;
    }
    
    /**
     * Returns the Image associated with this Tile. This is a read only property
     *          This may return null at any time, however if this returns null,
     *          a load operation will automatically be started for it.
     */
    public Image getImage() {
        Image img = image;
        if (img == null) {
            setLoaded(false);
            if(dtf != null)
            	dtf.startLoading(this);
        }
        
        return img;
    }
    
    /**
     * @return the location in the world at this zoom level that this tile should
     * be placed
     */
    /*
    public TilePoint getLocation() {
        return location;
    }*/
    
    /**
     * @return the zoom level that this tile belongs in
     */
    public int getZoom() {
        return zoom;
    }
    
    //////////////////JavaOne Hack///////////////////
    
    /**
     * Adds a single property change listener. If a listener has been previously
     * added then it will be replaced by the new one.
     * @param propertyName
     * @param listener
     */
    public void addUniquePropertyChangeListener(String propertyName, Listener listener) {
        if (uniqueListener != null && uniqueListener != listener) {
            uniqueListener = null;
        }
        if (uniqueListener != listener) {
            uniqueListener = listener;
        }
    }
   
    /**
     * Gets the loading priority of this tile.
     * @return
     */
    public int getPriority() {
        return priority;
    }
    
    /**
     * Gets the URL of this tile.
     * @return
     */
    public String getURL() {
        return url;
    }

    public void setURL(String newURL) {
    	this.url	= newURL;
    }
    
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    public synchronized void setLoading(boolean set) {
    	this.loading = set;
    }
    
    public synchronized boolean isLoading() {
    	return loading;
    }
    
    private DefaultTileFactory dtf;

    
}

