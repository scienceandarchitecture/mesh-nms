/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MDMap.java
 * Comments : Map control
 * Created  : Jan 10, 2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |Dec 3, 2007  | ZoomScale zooms in opposite direction           |Prachiti|
 * --------------------------------------------------------------------------------
 * |  0  |Dec 3, 2007  | Created                                         |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.maps;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

/*
 * Created on Dec 3, 2007
 *
 */

public class MDMap {
    
    public static final int MAXZOOM 			= 18; 
    public static final int MINZOOM 			= 0;
    
    private Composite			parent;
    private Point				center;
    private Rectangle			drawRect;
    private int 				zoom;
    private TileFactory 		factory;
    private TileFactoryInfo 	factoryInfo;
    private Image 				loadingImage;
	private TileLoadListener 	tileLoadListener;;
	private boolean				moveAllowed;
	
    public MDMap(Composite arg0, int arg1) {
         
        parent 				= arg0;
        center				= new Point(0,0);
        drawRect		 	= new Rectangle(0,0,0,0);
        tileLoadListener 	= new TileLoadListener(this);
        zoom				= 2;
        moveAllowed			= false;
        
        setLoadingImage();
    }
    
    public void setTileFactory(String filePath,boolean zipped) {
        
        factoryInfo	= new TileFactoryInfo(MINZOOM,MAXZOOM,MAXZOOM,
                256, true, true, 
                filePath,
                "x","y","z",zipped);
        
        factoryInfo.setDefaultZoomLevel(0);
        factory	 = new DefaultTileFactory(factoryInfo);
    }
   
    public void setCenterPosition(GeoPosition geoPosition) {
        setCenter(factory.geoToPixel(geoPosition, zoom));
        resize();
    }

    /**
     * A property indicating the center position of the map
     * @return the current center position
     */
    public GeoPosition getCenterPosition() {
        return factory.pixelToGeo(center, zoom);
    }
    
    private Rectangle getViewportBounds() {
        
    	//calculate the "visible" viewport area in pixels
        int viewportWidth 	= parent.getClientArea().width;
        int viewportHeight 	= parent.getClientArea().height;
        
        if(viewportWidth == 0 && viewportHeight == 0)
            return drawRect;
       
        double viewportX 	= (center.x - viewportWidth/2);
        double viewportY 	= (center.y - viewportHeight/2);
        
        if(center.x == 0 && center.y == 0) {
            viewportX      = viewportWidth/2;
            viewportY      = viewportHeight/2;
        }
        
        drawRect.x		= (int)viewportX;
        drawRect.y		= (int)viewportY;
        drawRect.width	= viewportWidth;
        drawRect.height	= viewportHeight;
        return drawRect;
        
        //return new Rectangle((int)viewportX, (int)viewportY, viewportWidth, viewportHeight);
    }
    
    public void setCenter(Point center) {
        
        int viewportHeight 	= parent.getClientArea().height;
        int viewportWidth 	= parent.getClientArea().width;
        
        Rectangle newVP = getViewportBounds();
        if(newVP.y < 0) {
            int centerY = viewportHeight/2;
            center = new Point(center.x,centerY);
        }
        if(newVP.x <0) {
            int centerX = viewportWidth/2;
            center 		= new Point(centerX, center.y);
        }
        
        Rectangle mapSize 	= factory.getMapSize(getZoom());
        int mapHeight 		= (int)mapSize.height*factory.getTileSize(getZoom());
        if(newVP.y + newVP.height > mapHeight) {
            int centerY = mapHeight - viewportHeight/2;
            center = new Point(center.x,centerY);
        }
        
        int mapWidth = (int)mapSize.width*factory.getTileSize(getZoom());
        if((newVP.x + newVP.width > mapWidth)) {
            int centerX = mapWidth - viewportWidth/2;
            center = new Point(centerX, center.y);
        }
        
        if(mapHeight < newVP.height) {
            int centerY = mapHeight/2;
            center = new Point(center.x,centerY);
        }
        
        if(mapWidth < newVP.width) {
            int centerX = mapWidth/2;
            center = new Point(centerX, center.y);
        }
        
        this.center = center;
        
    }
    
    public void resize() {
		drawRect = getViewportBounds();
		parent.redraw();
    }

    public int getZoom() {
        return zoom;
    }
    
    public void setZoom(int zoom) {
        
        if(zoom == this.zoom) {
            return;
        }
        
        TileFactoryInfo info = factory.getInfo();
       
        if(info != null &&
                (zoom < info.getMinimumZoomLevel() ||
                zoom > info.getMaximumZoomLevel())) {
            return;
        }

        factory.clearCache();
        
        GeoPosition oldCenter	= getCenterPosition();
        this.zoom = zoom;
        setCenterPosition(oldCenter);
        
    }

    public void drawMapTiles(final GC g) {
        
        int size 	= factory.getTileSize(zoom);
        
        int numWide = drawRect.width/size + 2;
        int numHigh = drawRect.height/size + 2 ;
        
        int tpx = (int)Math.floor(drawRect.x / size);
        int tpy = (int)Math.floor(drawRect.y / size);
        
        for(int x = 0;x <= numWide; x++) {
            for(int y = 0;y <= numHigh; y++) {
            	
                int itpx = x + tpx;
                int itpy = y + tpy;
                int tileSize	= factory.getTileSize(zoom);
                int ox = ((itpx * tileSize) - drawRect.x);
                int oy = ((itpy * tileSize) - drawRect.y);
                
                Tile tile = factory.getTile(itpx,itpy,zoom);
                tile.dx = ox; tile.dy = oy;
                if(tile.isLoaded() == false)
                	tile.addUniquePropertyChangeListener("loaded",tileLoadListener);
                
                Image image = tile.getImage();
                if(image == null)
                	g.drawImage(loadingImage, ox, oy);
                else 
                	g.drawImage(image, ox, oy);
               
            }
        }
    }
    
    public void setCenterOnMove (Point current,Point prev) {
        
        int x = center.x - (current.x - prev.x);
        int y = center.y - (current.y - prev.y);
        
        int maxHeight = (int)(factory.getMapSize(getZoom()).height*factory.getTileSize(zoom));
        if (y > maxHeight) {
            y = maxHeight;
        }
        
        prev = current;
        setCenter(new Point(x, y));
        resize();
    }
    
    public Point convertGeoPositionToPoint(GeoPosition pos) {
        //convert from geo to world bitmap
        Point pt = factory.geoToPixel(pos, getZoom());
        //convert from world bitmap to local 
        Rectangle bounds = getViewportBounds();
        return new Point(pt.x - bounds.x,pt.y - bounds.y);
    }
    
    public GeoPosition convertPointToGeoPosition(Point pt) {
        //convert from local to world bitmap
        Rectangle bounds = getViewportBounds();
        
        Point pt2 = new Point(pt.x + bounds.x, pt.y + bounds.y);
        // convert from world bitmap to geo
        GeoPosition pos = factory.pixelToGeo(pt2, getZoom());
        return pos;
    }
    
    private final class TileLoadListener implements Listener {
    	MDMap thisObj;
    	public TileLoadListener(MDMap thisObj) {
    		this.thisObj = thisObj;
		}
         public void handleEvent(Event evt) {
             Tile t = (Tile)evt.data;
             if(thisObj.isDisposed() == true)
             	return;
             
             thisObj.redraw(t.dx, t.dy, 256, 256, true);
         }
    }
    
    /**
	 * @param dx
	 * @param dy
	 * @param i
	 * @param j
	 * @param b
	 */
	public void redraw(int dx, int dy, int i, int j, boolean b) {
		parent.redraw();
		parent.update();
	}

	/**
	 * @return
	 */
	public boolean isDisposed() {
		return parent.isDisposed();
	}

	public void setLoadingImage() {
         
     	int width = 256, height = 256;
     	loadingImage 	= new Image(Display.getCurrent(), width, height);
		GC gc 			= new GC(loadingImage);
		gc.fillRectangle(0, 0, width, height);
		gc.drawLine(0, 0, width, height);
		gc.drawLine(0, height, width, 0);
		gc.drawText("Loading", 10, 10);
		gc.dispose();
		
     }
    
	/**
	 * @return Returns the moveAllowed.
	 */
	public boolean isMoveAllowed() {
		return moveAllowed;
	}
	
	/**
	 * @param moveAllowed The moveAllowed to set.
	 */
	public void setMoveAllowed(boolean moveAllowed) {
		this.moveAllowed = moveAllowed;
	}

	public void stopServices() {
		if(this.factory == null)
			return;
		
		factory.stopServices();
	}
}