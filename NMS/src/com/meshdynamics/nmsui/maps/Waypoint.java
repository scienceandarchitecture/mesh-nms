/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Waypoint.java
 * Comments : A Waypoint is a GeoPosition that can be drawn on a may using 
 * 			  a WaypointPainter. 
 * Created  : Jan 10, 2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Jan 10, 2008 | Created                                         |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.maps;


/**
 * A Waypoint is a GeoPosition that can be drawn on a may using 
 * a WaypointPainter.
 * 
 */
public class Waypoint {
    private GeoPosition position;

    /**
     * Creates a new instance of Waypoint at lat/long 0,0
     */
    public Waypoint() {
        this(new GeoPosition(0, 0));
    }
    
    /**
     * Creates a new instance of Waypoint at the specified
     * latitude and longitude
     * @param latitude new latitude
     * @param longitude new longitude
     */
    public Waypoint(double latitude, double longitude) {
        this(new GeoPosition(latitude,longitude));
    }
    
    /**
     * Creates a new instance of Waypoint at the specified
     * GeoPosition
     * @param coord a GeoPosition to initialize the new Waypoint
     */
    public Waypoint(GeoPosition coord) {
        this.position = coord;
    }
    
    /**
     * Get the current GeoPosition of this Waypoint
     * @return the current position
     */
    public GeoPosition getPosition() {
        return position;
    }

    /**
     * Set a new GeoPosition for this Waypoint
     * @param coordinate a new position
     */
    public void setPosition(GeoPosition coordinate) {
        this.position = coordinate;
        //firePropertyChange("position", old, getPosition());
    }
}
