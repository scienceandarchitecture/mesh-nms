/*
 * Created on Feb 11, 2008
 *
 */
package com.meshdynamics.nmsui.maps;


/**
 * @author prachiti
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MapZipManager {
    
	private static MapZipManager instance;
	
    static {
    	instance = null;
	    //System.load(MFile.getSystemPath() +	"/ZipMap.dll");        
	    System.loadLibrary("ZipMap");
	}
	
    private MapZipManager() {
    	
    }
    
    public static MapZipManager getInstance() {
    	
    	if(instance == null)
    		instance = new MapZipManager();
    	
    	return instance;
    }
    
    public native void 					setMapPath			(String MapPath);
    public native synchronized void 	addFileToMap		(String MapPath, String ImageFile);
	public native synchronized void 	removeFileFromMap	(String MapPath, String fileName);
	public native synchronized byte[]	getImageBytes		(String MapPath, String fileName);
}
