/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : DefaultTileFactory.java
 * Comments : Implementation of TileFactory
 * Created  : Jan 10, 2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Jan 10, 2008 | Created                                         |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.maps;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;

import com.meshdynamics.nmsui.MeshViewerProperties;
import com.meshdynamics.nmsui.util.MFile;

/**
 * A tile factory which configures itself using a TileFactoryInfo object and uses a Google Maps like mercator projection.
 * 
 */
public class DefaultTileFactory extends TileFactory {
    
    private static final int ZIPPED_FILE_SYS 	= 0;
    private static final int LOCAL_FILE_SYS 	= 1;
    private static final int WEB_FILE_SYS 		= 2;
    private static final int THREAD_POOL_SIZE	= 4;
    
    /**
     * Creates a new instance of DefaultTileFactory using the spcified TileFactoryInfo
     * @param info a TileFactoryInfo to configure this TileFactory
     */
    public DefaultTileFactory(TileFactoryInfo info) {
        this.info 			= info;
        this.tileMap		= new HashMap<String, Tile>();
        this.cache			= new TileCache();
        this.service		= null;
    }
    
    private TileFactoryInfo info;
    //TODO the tile map should be static ALWAYS, regardless of the number
    //of GoogleTileFactories because each tile is, really, a singleton.
    private HashMap<String, Tile> 	tileMap;
    private TileCache 				cache;
    private ExecutorService 		service;
    
    /**
     * Gets the size of an edge of a tile in pixels at the current zoom level. Tiles must be square.
     * @param zoom the current zoom level
     * @return the size of an edge of a tile in pixels
     */
    public int getTileSize(int zoom) {
        return getInfo().getTileSize(zoom);
        
    }
    
    /**
     * Get the size of the world bitmap at the current zoom level in <b>tiles</b>
     * @param zoom the current zoom level
     * @return size of the world bitmap in tiles
     */
    
    public Rectangle getMapSize(int zoom) {
        return GeoUtil.getMapSize(zoom, getInfo());
    }
    
    /**
     * Convert a GeoPosition to a Point2D pixel coordinate in the world bitmap
     * @param c a coordinate
     * @param zoomLevel the current zoom level
     * @return a pixel location in the world bitmap
     */
    
    public Point geoToPixel(GeoPosition c, int zoom) {
        return GeoUtil.getBitmapCoordinate(c, zoom, getInfo());
    }
    
    /**
     * Returns
     * @param pixelCoordinate
     * @return
     */
    //public TilePoint getTileCoordinate(Point2D pixelCoordinate) {
    //    return GeoUtil.getTileCoordinate(pixelCoordinate, getInfo());
    //}
    
    
    /**
     * Converts a pixel coordinate in the world bitmap to a GeoPosition
     * @param pixelCoordinate a point in the world bitmap at the current zoom level
     * @param zoom the current zoom level
     * @return the point in lat/long coordinates
     */
    
    public GeoPosition pixelToGeo(Point pix, int zoom) {
        return GeoUtil.getPosition(pix,zoom, getInfo());
        
    }
    
    /**
     * Returns the tile that is located at the given tilePoint for this zoom. For example,
     * if getMapSize() returns 10x20 for this zoom, and the tilePoint is (3,5), then the
     * appropriate tile will be located and returned.
     * @param tilePoint
     * @param zoom
     * @return
     */
    public Tile getTile(int x, int y, int zoom) {
        return getTile(x, y , zoom, true);
    }
    
    public void clearCache() {
        
        Iterator<Tile> i =  tileMap.values().iterator();
        while(i.hasNext()){
        	Tile tile = (Tile)i.next();
        	if(tile.image != null) {
	            tile.image.dispose();
	            tile.image = null;
        	}
        	tile.setLoaded(false);
        }
    	
    	tileMap.clear();
    	
        tileQueue.clear();
        cache.removeAll();
    }
    
    public String removeImage(int tpx, int tpy, int zoom) {
        
        URI uri;
        
        int tileX = tpx;
        int numTilesWide = (int)getMapSize(zoom).width;
        if (tileX < 0) {
            tileX = numTilesWide - (Math.abs(tileX)  % numTilesWide);
        }
        
        tileX = tileX % numTilesWide;
        int tileY = tpy;
        
        String url = getInfo().getTileUrl(tileX, tileY, zoom);
        
        try {
            uri = new URI(url);
            cache.removeFromImageCache(uri);
            tileMap.remove(url);
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return url;   
    }
    
    private Tile getTile(int tpx, int tpy, int zoom, boolean eagerLoad) {
        //wrap the tiles horizontally --> mod the X with the max width
        //and use that
        int tileX = tpx;//tilePoint.getX();
        int numTilesWide = (int)getMapSize(zoom).width;
        if (tileX < 0) {
            tileX = numTilesWide - (Math.abs(tileX)  % numTilesWide);
        }
        
        tileX = tileX % numTilesWide;
        int tileY = tpy;
        
        
        String url = getInfo().getTileUrl(tileX, tileY, zoom);//tilePoint);
        //System.out.println("loading: " + url);

        int pri = Tile.PRIORITY_HIGH;
        if (!eagerLoad) {
            pri = Tile.PRIORITY_LOW;
        }
        Tile tile = null;
        //System.out.println("testing for validity: " + tilePoint + " zoom = " + zoom);
        if (!tileMap.containsKey(url)) {
        	
            if (!GeoUtil.isValidTile(tileX, tileY, zoom, getInfo())) {
                tile = new Tile(tileX, tileY, zoom);
            }  else {
                tile = new Tile(tileX, tileY, zoom, url, pri, this);
                startLoading(tile);
            }
            tileMap.put(url,tile);
        }  else {
            tile = (Tile) tileMap.get(url);
            // if its in the map but is low and isn't loaded yet
            // but we are in high mode
            if (tile.getPriority()  == Tile.PRIORITY_HIGH && eagerLoad && !tile.isLoaded()) {
                //System.out.println("in high mode and want a low");
                //tile.promote();
                promote(tile);
            }
        }
        
        
        return tile;
    }
    
    /**
     * Get the TileFactoryInfo describing this TileFactory
     * @return a TileFactoryInfo
     */
    public TileFactoryInfo getInfo() {
        return info;
    }
    
    
    private static BlockingQueue tileQueue = new PriorityBlockingQueue(5,new java.util.Comparator() {
        public int compare(Object o1, Object o2) {
            if(((Tile)o1).getPriority() == Tile.PRIORITY_LOW && ((Tile)o2).getPriority() == Tile.PRIORITY_HIGH) {
                return 1;
            }
            if(((Tile)o1).getPriority() == Tile.PRIORITY_HIGH && ((Tile)o2).getPriority() == Tile.PRIORITY_LOW ) {
                return -1;
            }
            return 0;
            
        }

        public boolean equals(Object obj) {
            return obj == this;
        }
    });

     synchronized void startLoading(Tile tile) {
        
         
        if(tile.isLoading() == true)
            return;

        if(tileQueue.contains(tile) == true)
        	return;
        
        try {
	        URI 	uri;
	        Image 	img;
	        
	        uri 	= getURI(tile);
	        if(uri != null) {
		        img		= cache.get(uri);
		        if(img != null) {
		            tile.image 		= img;
		            tile.setLoaded(true);
		            return;
		        }
	        }
            tile.setLoading(true);
            if(getFromZipFileSystem(tile) != null) { 
            	tile.setLoading(false);
            	return;
            }
            
        } catch(Exception e) {
        	e.printStackTrace();
        }
        
        if(MeshViewerProperties.getMapTilesFromWeb != 1) {
            tile.image 	 	= null;
            tile.setLoaded(false);
            tile.setLoading(false);
            return;
        }
        	
        tile.setLoading(true);
        try {
            tileQueue.put(tile);
            getService().submit(new TileRunner());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Increase the priority of this tile so it will be loaded sooner.
     */
    public synchronized void promote(Tile tile) {
        if(tileQueue.contains(tile)) {
            try {
                tileQueue.remove(tile);
                tile.priority = Tile.PRIORITY_HIGH;
                tileQueue.put(tile);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    /**
     * An inner class which actually loads the tiles. Used by the thread queue. Subclasses
     * can override this if necessary.
     */
    private synchronized byte[] getFromZipFileSystem(Tile tile) throws Exception {
        
        Image 	img;
        URI 	uri 	= null;
        byte[] 	bimg	= null;

        tile.setLoading(true);
        String strUrl		= tile.getZoom()+"\\"+tile.getX()+"\\"+tile.getY()+".png";
    	try {
    		bimg = MapZipManager.getInstance().getImageBytes(MFile.getMapPath(), strUrl);
    		if(bimg != null) {
    			img  = new Image(Display.getCurrent(), new ByteArrayInputStream(bimg));
                uri = new URI(tile.getURL());
                cache.put(uri,bimg,img);
                tile.image = img;
                tile.setLoaded(true);
                return bimg;
    		}
    	} catch(Exception e) {
    		tile.image = null;
    		tile.setLoaded(false);
    		tile.setLoading(false);
    		System.out.println("Error occured while retreiving tile from zip file. " + e.getMessage());
    		cache.removeFromImageCache(uri);
    		return null;
    	}
    	
		tile.image = null;
		tile.setLoaded(false);
		tile.setLoading(false);
        return null;
    }
 
	private class TileRunner implements Runnable { 
        
        byte[]	bimage; 		 
        int trys = 1;
        
        public void run() {
        	Tile tile = null;
        	try { 
        		tile = (Tile) tileQueue.remove();
        	}catch (Exception e){
        		return;
        	}
        	while (!tile.isLoaded() && trys > 0) {
        		try {
                	tile.setLoading(true);
                    if(getFromWeb(tile) != null) {
                    	tile.setLoading(false);
                        break;
                    }
	            } catch (OutOfMemoryError memErr) {
	                cache.needMoreMemory();
	            } catch (Throwable e) {
	                tile.error = e;
	            }
	            trys--;
	        }
        	tile.setLoading(false);
	    }
    }

	private byte[]  getFromWeb(final Tile tile) {
        
        Image img 			= null;
        String baseURL 		= "http://tile.openstreetmap.org";
        String url			= baseURL +"/"+tile.getZoom()+"/"+tile.getX()+"/"+tile.getY()+".png";
        
        byte[] 	bimg = null;
        try {

	        URI 	uri 	= new URI(url);
	        String 	oldURL 	= tile.getURL();
	        tile.setURL(url);
	        bimg 		= cacheInputStream(tile,WEB_FILE_SYS);
	        tile.setURL(oldURL);
	        
	        if(bimg !=  null && bimg.length > 100) {
	            
	            img  = new Image(Display.getCurrent(),
	              	   new ByteArrayInputStream(bimg));
	            
	            File tileFile = saveTile(bimg,tile); 
	            if(tileFile != null) {
	            	
	            	MapZipManager.getInstance().addFileToMap(MFile.getMapPath(), tileFile.getName());
	            	tileFile.delete();
	            	
		            cache.put(uri,bimg,img);
		            tile.image 	 	= img;
		            tileMap.put(oldURL,tile);
		            tile.setLoaded(true);
	            } else 
	            	System.out.println("saveTile returned null");
	            	
	        } else {
	            tile.image 	 	= null;
	            tile.setLoaded(false);
	            tile.setLoading(false);
	        }
        }catch (Exception e) {
        	System.out.println("Error occured while loading " + url);
            e.printStackTrace();
            tile.image 	 	= null;
            tile.setLoaded(false);
            tile.setLoading(false);
        }
    return bimg;
        
    }
   
        
	private File saveTile(byte[] bImage, Tile tile) {
        String parentUrl    = MFile.getMapPath();
        String url 			= parentUrl + "/"+tile.getZoom()+"_"+tile.getX()+"_"+tile.getY()+".png";
        
        File file = new File(url);
        
        try {
        	
        	if(file.exists() == true)
        		file.delete();
        	
            file.createNewFile();
            FileOutputStream fileOut = new FileOutputStream(file);
            fileOut.write(bImage);
            fileOut.close();
            return file;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        file.delete();
        return null;
    }

    protected URI getURI(Tile tile) throws URISyntaxException {
        if(tile.getURL() == null) {
            return null;
        }
        return new URI(tile.getURL());
    }
 
    private byte[] cacheInputStream(Tile tile,int type) throws IOException {
        
        InputStream ins = null;
        
        switch(type) {
            case ZIPPED_FILE_SYS:
                String strUrl		= tile.getZoom()+"/"+tile.getX()+"/"+tile.getY()+".png";
    	        String baseUrl 		= this.info.baseURL;
    	        baseUrl 			=  baseUrl.replace("%20"," ");
    	        ZipFile srcZipFile 	= new ZipFile(baseUrl);
    			ZipEntry urlEntry 	= srcZipFile.getEntry(strUrl);
    			
    			if (urlEntry == null) {
    				return null;
    			}
    			ins = srcZipFile.getInputStream(urlEntry);
                break;
            case WEB_FILE_SYS:
                try {
                    URL url = getURI(tile).toURL();
                    ins = url.openStream();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
                break;
            case LOCAL_FILE_SYS:
                //todo
                strUrl		= tile.getZoom()+"_"+tile.getX()+"_"+tile.getY()+".png";
    	        baseUrl 	= MFile.getMapPath();
    	        
    	        try {
    	        	ins = new FileInputStream(new File(baseUrl +"/"+ strUrl));
    	        }catch(Exception e) {
    	            ins = null;
    	        }
                break;
        }
        
        if(ins == null)
		    return null;
		
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        while(true) {
            int n = ins.read(buf);
            if(n == -1) break;
            bout.write(buf,0,n);
        }
        return bout.toByteArray();
    }

    /**
     * Subclasses may override this method to provide their own executor services. This 
     * method will be called each time a tile needs to be loaded. Implementations should 
     * cache the ExecutorService when possible.
     * @return ExecutorService to load tiles with
     */
    protected synchronized ExecutorService getService() {
        if(service == null) {
            service = Executors.newFixedThreadPool(THREAD_POOL_SIZE, new ThreadFactory() {
                private int count = 0;
                
                public Thread newThread(Runnable r) {
                    Thread t = new Thread(r, "tile-pool-" + count++);
                    t.setPriority(Thread.MIN_PRIORITY);
                    t.setDaemon(true);
                    return t;
                }
            });
        }
        return service;
    }
    
    public void stopServices() {
    	
    	if(service == null)
    		return;
    	
    	if(service.isShutdown() == true) {
    		service = null;
    		return;
    	}
    	
    	service.shutdownNow();
    	service = null;
    		
    }
    
}
