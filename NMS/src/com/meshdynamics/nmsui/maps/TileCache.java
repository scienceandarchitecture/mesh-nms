/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : TileCache.java
 * Comments : Cache for Tiles 
 * Created  : Jan 10, 2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Jan 10, 2008 | Created                                         |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.maps;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

/**
 * An implementation only class for now. For internal use only.
 * 
 */
public class TileCache {
    private Map<URI, Image> imgmap = new HashMap<URI, Image>();
    private int imagesize = 0;
    private Map<URI, byte[]> bytemap = new HashMap<URI, byte[]>();
    private int bytesize = 0;
    
    public TileCache() { }
    

    /** Put a tile image into the cache. This puts both a buffered image and array of bytes that make
     * up the compressed image.
     * @param uri URI of image that is being stored in the cache
     * @param bimg bytes of the compressed image, ie: the image file that was loaded over the network
     * @param img image to store in the cache
     */
    public void put(URI uri, byte[] bimg, Image img) {
        if(bytesize > 1000*1000*10) { // clear if it goes beyond 10MB
            bytesize = 0;
            bytemap.clear();
        }
        
        bytemap.put(uri,bimg);
        bytesize += bimg.length;
        addToImageCache(uri, img);
    }
    
    /** Returns a buffered image for the requested URI from the cache. This method must return null if the image is not
     * in the cache.  If the image is unavailable but it's compressed version *is* available, then the compressed version
     * will be expanded and returned.
     * 
     * @param uri URI of the image previously put in the cache
     * @return the image matching the requested URI, or null if not available
     * @throws java.io.IOException 
     */
    public Image get(URI uri) throws IOException {
        if(imgmap.containsKey(uri)) {
           // System.out.println("getting from img cache: " + imagesize/1000+"k");
            return (Image) imgmap.get(uri);
        }
        if(bytemap.containsKey(uri)) {
            p("retrieving from bytes");
            Image img = new Image(Display.getCurrent(),new ByteArrayInputStream((byte[]) bytemap.get(uri)));
            addToImageCache(uri,img);
            return img;
        }
        return null;
    }
    
    
    /** Request that the cache free up some memory. How this happens or how much memory is freed is 
     * up to the TileCache implementation. Subclasses can implement their own strategy. The
     * default strategy is to clear out all buffered images but retain the compressed versions.
     */
    public void needMoreMemory() {
        Iterator<Image> i =  imgmap.values().iterator();
        while(i.hasNext()){
            Image image = (Image)i.next();
            image.dispose();
            image = null;
        }
        imgmap.clear();
    }

    public synchronized void removeAll () {
        
        Iterator<Image> i =  imgmap.values().iterator();
        while(i.hasNext()){
            Image image = (Image)i.next();
            image.dispose();
            image = null;
        }
        imagesize = 0;
        imgmap.clear();
        
        bytemap.clear();
        bytesize = 0;
    }
    
    public synchronized void  removeFromImageCache(final URI uri) {
        Image img = (Image)imgmap.get(uri);
        if(img != null) {
            img.dispose();
            img = null;
            imgmap.remove(uri);
        }
    }
    
    private synchronized void  addToImageCache(final URI uri, final Image img) {
        
        // this is a hack. we need a real time based cache
        if(imagesize > 1000*1000*10) { // clear if it goes beyond 10MB
            p("HACK! clearing the image cache ");
            imagesize = 0;
            imgmap.clear();
        }
        
        imgmap.put(uri,img);
        imagesize += img.getBounds().width*img.getBounds().height*4;
        
        p("added to cache: "+
                " uncompressed = " + imgmap.keySet().size() + " / " + imagesize/1000 + "k" +
                " compressed = " + bytemap.keySet().size() + " / " + bytesize/1000 + "k");
    }
    
    private void p(String string) {
       //System.out.println(string);
    }

}
