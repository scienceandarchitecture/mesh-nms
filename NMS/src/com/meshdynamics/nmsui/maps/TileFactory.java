/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : TileFactory.java
 * Comments : Interface for getting tiles for Map control
 * Created  : Jan 10, 2008
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Jan 10, 2008 | Created                                         |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.nmsui.maps;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

/**
 * A class that can produce tiles and convert coordinates to pixels
 * 
 */
public abstract class TileFactory {
    
    /** Creates a new instance of TileFactory */
    protected TileFactory() {
    }
    
    /**
     * The the size of tiles for this factory. Tiles must be square.
     * @return the size of the tiles in pixels. All tiles must be square. A return
     * value of 256, for example, means that each tile will be 256 pixels wide and tall
     */
    public abstract int getTileSize(int zoom);
        
    /**
     * Returns a Dimension containing the width and height of the map, 
     * in tiles at the
     * current zoom level.
     * So a Dimension that returns 10x20 would be 10 tiles wide and 20 tiles
     * tall. These values can be multipled by getTileSize() to determine the
     * pixel width/height for the map at the given zoom level
     * @return the size of the world bitmap in tiles
     * @param zoom the current zoom level
     */
    public abstract Rectangle getMapSize(int zoom);
    
    /**
     * 
     * Return the Tile at a given TilePoint and zoom level
     * @return the tile that is located at the given tilePoint for this zoom level. For
     *         example, if getMapSize() returns 10x20 for this zoom, and the
     *         tilePoint is (3,5), then the appropriate tile will be located
     *         and returned. This method must not return null. However, it
     * can return dummy tiles that contain no data if it wants. This is appropriate,
     * for example, for tiles which are outside of the bounds of the map and if the
     * factory doesn't implement wrapping.
     * @param tilePoint the tilePoint
     * @param zoom the current zoom level
     */
    public abstract Tile getTile(int x, int y, int zoom);
    
    
    /**
     * Convert a pixel in the world bitmap at the specified
     * zoom level into a GeoPosition
     * @param pixelCoordinate a Point2D representing a pixel in the world bitmap
     * @param zoom the zoom level of the world bitmap
     * @return the converted GeoPosition
     */
    public abstract GeoPosition pixelToGeo(Point pixelCoordinate, int zoom);
    
    /**
     * Convert a GeoPosition to a pixel position in the world bitmap
     * a the specified zoom level.
     * @param c a GeoPosition
     * @param zoom the zoom level to extract the pixel coordinate for
     * @return the pixel point
     */
    public abstract Point geoToPixel(GeoPosition c, int zoom);
    
    /**
     * Return the TileFactoryInfo containing map metrics information
     * 
     * @return the TileFactoryInfo for this TileFactory
     */
    public abstract TileFactoryInfo getInfo();

    public abstract void clearCache();
    
    public abstract String removeImage(int tpx, int tpy, int zoom);
    
    public abstract void stopServices();
        
    
}
