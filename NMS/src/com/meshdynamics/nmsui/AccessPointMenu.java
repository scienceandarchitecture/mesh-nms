
/***********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : AccessPointMenu.java
 * Comments : Menu class for AccessPoint . 
 * Created  : 26/10/2004
 * Author   : Amit Chavan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ---------------------------------------------------------------------------------
 * | No  |Date      	|  Comment                                        | Author |
 * ---------------------------------------------------------------------------------
 * | 14  |Jun 21,2007   |  menu items saved in menu object for reuse	  |Abhijit |
 *---------------------------------------------------------------------------------
 * | 14  |May 02,2007   |  Configure Advanced Settings added			  |Abhishek |
 *---------------------------------------------------------------------------------
 * | 13  |Feb 27,2007   |  removed unused code							  |Abhijit |
 *---------------------------------------------------------------------------------
 * | 12  |Oct 18, 2006 |  Node Status Web Page added                      | Imran |
 * ---------------------------------------------------------------------------------
 * | 11  |Jul 07, 2006 |Advanced Setting Menu added			              |Prachiti|
 * ------------------------------------------------------------------------------- 
 * |  10 |Feb 13, 2006 | Update and CTM Pop up Menus removed			  | Mithil |
 * --------------------------------------------------------------------------------
 * |  9  |Feb 13, 2006 | Reject Clients menu removed	 				  | Mithil |
 * --------------------------------------------------------------------------------
 * |  8  |Feb 13, 2006 | Log Menu removed					              | Mithil |
 * --------------------------------------------------------------------------------
 * |  7  | Feb 03,2006  | Menus Shuffled						  		  | Mithil |
 * --------------------------------------------------------------------------------
 * |  6  |Feb 03, 2006  | Generic Request packet menus added     		  | Abhijit|
 * --------------------------------------------------------------------------------
 * |  5  |Feb 02, 2006  | STA Information Packet   	 					  | Mithil |
 * --------------------------------------------------------------------------------
 * |  4  |Jan 17, 2006  |  Set Monitor Mode - Continuous Transmit mode	  | Mithil |
 * ----------------------------------------------------------------------------------
 * |  3  |Jan 04, 2006 |  System.out.println()'s removed			     | Mithil |
 * ----------------------------------------------------------------------------------
 * |  2  |Nov 25,2005   | Changes for set Monitor Mode              	  | Mithil |
 * ------------------------------------------------------------------------------- 
 * |  1  |Sep 19, 2005 	| Rearranged menus for version 3.0                | Abhijit|
 * ---------------------------------------------------------------------------------
 * |  0  |26/10/2004 	| Created                                         | Amit   |
 * ---------------------------------------------------------------------------------
 ***********************************************************************************/

package com.meshdynamics.nmsui;

import java.util.Enumeration;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Decorations;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;

import com.meshdynamics.nmsui.mesh.ExtendedMenuInfo;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.nmsui.mesh.views.AccessPointUI;
import com.meshdynamics.util.MacAddress;

public class AccessPointMenu {
	
	// Declarations for items in Accesspoint Popup menu..
	public static final int    	APPOPUP_ITEM_DELETE							= 1;
	public static final int    	APPOPUP_ITEM_REBOOT							= 2;	

	public static final int    	APPOPUP_ITEM_SETTINGS						= 3;
	public static final int    	APPOPUP_ITEM_SETTINGS_GENERAL				= 4;
	public static final int    	APPOPUP_ITEM_SETTINGS_ADVANCED				= 5;
	public static final int    	APPOPUP_ITEM_SETTINGS_CUSTOM				= 6;
	public static final int    	APPOPUP_ITEM_SETTINGS_RESTORE_DEFAULTS		= 7;
	public static final int    	APPOPUP_ITEM_SETTINGS_EXPORT_CONFIG_SCRIPT	= 8;
	public static final int    	APPOPUP_ITEM_SETTINGS_IMPORT_CONFIG_SCRIPT	= 9;
	
	public static final int    	APPOPUP_ITEM_TOOLS							= 10;
	public static final int    	APPOPUP_ITEM_TOOLS_PERFORMANCE_TEST			= 11;
	public static final int    	APPOPUP_ITEM_TOOLS_MESHCOMMAND				= 12;
	public static final int    	APPOPUP_ITEM_TOOLS_RF_SPACEINFO				= 13;
	public static final int    	APPOPUP_ITEM_TOOLS_NODE_STATUS_WEB_PAGE		= 14;
	public static final int    	APPOPUP_ITEM_TOOLS_UPDATE_GEOPOSITION		= 15;
	
	public static final int    	APPOPUP_ITEM_SET_OFFLINE_IMAGES				= 16;
	
	public static final int     APPOPUP_ITEM_SETTINGS_EXPORT_RF_CONFIG_SCRIPT   = 17;
	
	public static final int		EXTENDED_MENU								= 256;
	
	
	private MeshViewerUI			meshViewerUI;
	private AccessPointMenuListener	menuListener;
	private Menu					menu;	
	private MenuItem				itmDelete;
	private MenuItem				itmSettings;
	private MenuItem				itmGeneralSettings;
	private MenuItem				itmAdvancedSettings;
	private MenuItem				itmRestoreDefaults;
	private MenuItem				itmRebootNode;
	private MenuItem				itmTools;
	private MenuItem				itmRFInfo;
	private MenuItem				itmPerformanceTest;
	private MenuItem				itmMeshCommand;
	private MenuItem				itmWebPage;
	private MenuItem				itmLatLngFromMap;
	private MenuItem				itmExportConfigScript;
	private MenuItem				itmOfflineViewNodeImg;
	private MenuItem				itmImportConfigScript;
	
	private static Image 			imgDelete;
	private static Image			imgConfigNode;
	private static Image			imgAdvSetting;
	private static Image			imgRestoreDefault;
	private static Image			imgReboot;
	private static Image			imgRfSpace;
	private static Image			imgTools;
	private static Image			imgPerfTest;
	private static Image			imgMeshCmd;
	private static Image			imgWeb;
	private static Image			imgExportConfigScript;
	private static Image			imgSetLatLng;
	private static Image			imgOfflineViewNodeImg;
	private static Image			imgImportConfigScript;
	
	static {
		
		imgDelete		  		= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/delete.gif"));  		
		imgConfigNode     		= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/configure.gif"));
		imgAdvSetting     		= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/advsettings.jpg"));
		imgRestoreDefault 		= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/restore.jpg"));
		imgReboot		  		= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/reboot.gif"));
	    imgTools		  		= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/advtools.gif"));
	    imgRfSpace		  		= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/graph.gif"));
	    imgPerfTest		  		= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/perftest.gif"));
	    imgMeshCmd		  		= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/console.jpg"));	
	    imgWeb			  		= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/ie.gif"));
	    imgExportConfigScript 	= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/exportscript.gif"));
	    imgSetLatLng	  		= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/map.png"));	
	    imgOfflineViewNodeImg 	= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/OfflineMapPicture.gif"));
	    imgImportConfigScript 	= new Image(null, MeshViewerUI.class.getResourceAsStream("apmenu_icons/importscript.gif"));
	}
	
	private AccessPointMenu refreshMenuItems(int  state) {
		
		if(state == AccessPointUI.STATE_RUNNING) {
			itmDelete.setEnabled(false);
			itmSettings.setEnabled(true);
			itmRestoreDefaults.setEnabled(true);
			itmRebootNode.setEnabled(true);
			itmTools.setEnabled(true);
			itmAdvancedSettings.setEnabled(true);
			itmGeneralSettings.setEnabled(true);
			itmMeshCommand.setEnabled(true);
			itmPerformanceTest.setEnabled(true);
			itmRFInfo.setEnabled(true);
			showHideOfflineViewMenu();
		} else {
			itmDelete.setEnabled(true);
			itmSettings.setEnabled(false);
			itmRestoreDefaults.setEnabled(false);
			itmRebootNode.setEnabled(false);
			itmTools.setEnabled(false);
		}
		
		refreshExtendedMenus(state);
		
		return this;
	}
	
    private void refreshExtendedMenus(int apState) {
    	Enumeration<ExtendedMenuInfo> e 	= meshViewerUI.getExtendedMenuTable().elements();
    	while(e.hasMoreElements()) {
    		ExtendedMenuInfo menuInfo = e.nextElement();
    		if(menuInfo.submenu != null) {
    			
    			if(apState == AccessPointUI.STATE_RUNNING) 
    				menuInfo.menuItem.setEnabled(true);
    			else
    				menuInfo.menuItem.setEnabled(false);
    		}
    	}
		
	}

	private void showHideOfflineViewMenu() {

		if(meshViewerUI.getSelectedMeshNetworkUI().getCurrentViewType() !=
		    MeshNetworkUI.VIEW_OFFLINE_MAP) {
		    itmLatLngFromMap.setEnabled(false);
		    itmOfflineViewNodeImg.setEnabled(false);
		}else {
		    itmLatLngFromMap.setEnabled(true);
		    itmOfflineViewNodeImg.setEnabled(true);
		}
    }

    public AccessPointMenu(MeshViewerUI ui, Decorations mainMenu){

		meshViewerUI   		= ui;
		menu 				= new Menu(mainMenu,SWT.POP_UP); 
		menuListener 		= new AccessPointMenuListener(meshViewerUI);
		createmenuItems();
		createExtendedMenu();
	
	}
	
	private void createmenuItems() {

		itmSettings = new MenuItem(menu,SWT.CASCADE);
		itmSettings.setText("Settings");
		itmSettings.setImage(imgConfigNode);

		Menu submenu = new Menu(menu);
		itmSettings.setMenu(submenu);
		
		itmGeneralSettings = new MenuItem(submenu,SWT.PUSH);
		itmGeneralSettings.setText("Configuration");
		itmGeneralSettings.addSelectionListener(menuListener);
		itmGeneralSettings.setData(new Integer(APPOPUP_ITEM_SETTINGS_GENERAL));
		itmGeneralSettings.setImage(imgConfigNode);
		
		itmAdvancedSettings = new MenuItem(submenu,SWT.PUSH);
		itmAdvancedSettings.setText("Advanced");
		itmAdvancedSettings.addSelectionListener(menuListener);
		itmAdvancedSettings.setData(new Integer(APPOPUP_ITEM_SETTINGS_ADVANCED));
		itmAdvancedSettings.setImage(imgAdvSetting);

		itmRestoreDefaults = new MenuItem(submenu,SWT.PUSH);
		itmRestoreDefaults.setText("Restore Defaults"); 
		itmRestoreDefaults.addSelectionListener(menuListener);
		itmRestoreDefaults.setData(new Integer(APPOPUP_ITEM_SETTINGS_RESTORE_DEFAULTS));
		itmRestoreDefaults.setImage(imgRestoreDefault);

		itmExportConfigScript = new MenuItem(submenu,SWT.PUSH);
		itmExportConfigScript.setText("Export Configuration Script");
		itmExportConfigScript.addSelectionListener(menuListener);
		itmExportConfigScript.setData(new Integer(APPOPUP_ITEM_SETTINGS_EXPORT_CONFIG_SCRIPT));
		itmExportConfigScript.setImage(imgExportConfigScript);

		itmImportConfigScript = new MenuItem(submenu,SWT.PUSH);
		itmImportConfigScript.setText("Import Configuration Script");
		itmImportConfigScript.addSelectionListener(menuListener);
		itmImportConfigScript.setData(new Integer(APPOPUP_ITEM_SETTINGS_IMPORT_CONFIG_SCRIPT));
		itmImportConfigScript.setImage(imgImportConfigScript);
		
		try {
            if(ClassLoader.getSystemClassLoader().loadClass("com.meshdynamics.api.restrict.RFEditor") != null) {                
                itmExportConfigScript = new MenuItem(submenu,SWT.PUSH);
                itmExportConfigScript.setText("Export RF-Editor Script");
                itmExportConfigScript.addSelectionListener(menuListener);
                itmExportConfigScript.setData(new Integer(APPOPUP_ITEM_SETTINGS_EXPORT_RF_CONFIG_SCRIPT));
                itmExportConfigScript.setImage(imgExportConfigScript);                  
            }
        } catch (ClassNotFoundException e) {
            /** Silently ignore */
        }
			
		
		new MenuItem(menu,SWT.SEPARATOR);
		
		itmDelete = new MenuItem(menu,SWT.PUSH);
		itmDelete.setText("Delete"); 		
		itmDelete.addSelectionListener(menuListener);
		itmDelete.setData(new Integer(APPOPUP_ITEM_DELETE));
		itmDelete.setImage(imgDelete);

		itmRebootNode = new MenuItem(menu,SWT.PUSH);
		itmRebootNode.setText("Reboot Node");
		itmRebootNode.addSelectionListener(menuListener);
		itmRebootNode.setData(new Integer(APPOPUP_ITEM_REBOOT));
		itmRebootNode.setImage(imgReboot);

		new MenuItem(menu,SWT.SEPARATOR);

		itmTools = new MenuItem(menu,SWT.CASCADE);
		itmTools.setText("Tools");
		itmTools.addSelectionListener(menuListener);
		itmTools.setData(new Integer(APPOPUP_ITEM_TOOLS));
		itmTools.setImage(imgTools);

		submenu = new Menu(menu);
		itmTools.setMenu(submenu);
		
		itmRFInfo = new MenuItem(submenu,SWT.PUSH);
		itmRFInfo.setText("RF Space Information");
		itmRFInfo.addSelectionListener(menuListener);
		itmRFInfo.setData(new Integer(APPOPUP_ITEM_TOOLS_RF_SPACEINFO));
		itmRFInfo.setImage(imgRfSpace);
		
		itmPerformanceTest = new MenuItem(submenu,SWT.PUSH);
		itmPerformanceTest.setText("Performance Test");
		itmPerformanceTest.addSelectionListener(menuListener);
		itmPerformanceTest.setData(new Integer(APPOPUP_ITEM_TOOLS_PERFORMANCE_TEST));
		itmPerformanceTest.setImage(imgPerfTest);
		
		itmMeshCommand = new MenuItem(submenu,SWT.PUSH);
		itmMeshCommand.setText("Mesh Command");
		itmMeshCommand.addSelectionListener(menuListener);
		itmMeshCommand.setData(new Integer(APPOPUP_ITEM_TOOLS_MESHCOMMAND));
		itmMeshCommand.setImage(imgMeshCmd);
		
		itmWebPage = new MenuItem(submenu,SWT.PUSH);
		itmWebPage.setText("Node Status Web-Page");
		itmWebPage.addSelectionListener(menuListener);
		itmWebPage.setData(new Integer(APPOPUP_ITEM_TOOLS_NODE_STATUS_WEB_PAGE));
		itmWebPage.setImage(imgWeb);
		
		itmLatLngFromMap = new MenuItem(submenu,SWT.PUSH);
		itmLatLngFromMap.setText("Update GeoPosition");
		itmLatLngFromMap.addSelectionListener(menuListener);
		itmLatLngFromMap.setData(new Integer(APPOPUP_ITEM_TOOLS_UPDATE_GEOPOSITION));
		itmLatLngFromMap.setImage(imgSetLatLng);

		itmOfflineViewNodeImg = new MenuItem(submenu,SWT.PUSH);
		itmOfflineViewNodeImg.setText("Set Node Images");
		itmOfflineViewNodeImg.addSelectionListener(menuListener);
		itmOfflineViewNodeImg.setData(new Integer(APPOPUP_ITEM_SET_OFFLINE_IMAGES));
		itmOfflineViewNodeImg.setImage(imgOfflineViewNodeImg);
		
	}
	
	private void createExtendedMenu() {
	    
	    Enumeration<ExtendedMenuInfo> e 	= meshViewerUI.getExtendedMenuTable().elements();
	    MenuItem itm 	= null;
	    Menu parent		= null;
	    
	    while(e.hasMoreElements()){
	        ExtendedMenuInfo extendedInfo = (ExtendedMenuInfo)e.nextElement();
	        
	        if(extendedInfo.parentSection == null && extendedInfo.commandId == null) {
	            itm  					= new MenuItem(menu,SWT.CASCADE);
	            Menu submenu 			= new Menu(menu);
	            itm.setMenu(submenu);
	            extendedInfo.submenu	= submenu;
	        } else {
	            parent = getParentExtendedMenu(extendedInfo);
	            if(parent != null) {
	                itm   = new MenuItem(parent,SWT.CASCADE);
	            }
	        }
	        
	        if(itm != null) {
	    	    itm.setText(extendedInfo.text);
	    	    itm.addSelectionListener(menuListener);
	    	    itm.setData(new Integer(extendedInfo.menuID));
	    	    
	    	    if (extendedInfo.icon instanceof Image) {
	    	        itm.setImage((Image)extendedInfo.icon);
	            }else {
	                 //throw exception
	            }
	    	    extendedInfo.menuItem = itm;
	    	    itm.setData(itm.getText(),extendedInfo);
	        }
	    }
	}
		
	private Menu getParentExtendedMenu(ExtendedMenuInfo extMenuInfo) {
	    
	    if(extMenuInfo != null) {
	        ExtendedMenuInfo parentInfo =  (ExtendedMenuInfo)extMenuInfo.parentSection;
	        if(parentInfo != null)
	            return parentInfo.submenu;
	    }
	    return null;
	}
	
	
	public Menu getMenu() {
		return menu;
	}
	
	public void refresh(MacAddress dsMacAddress, int state) {
		menuListener.setDsMacAddress(dsMacAddress);
		
		MeshNetworkUI netUI = meshViewerUI.getSelectedMeshNetworkUI();
		if(netUI.getIsNetworkLocked() == false) {
			refreshMenuItems(state);
		}else {
			setViewOnlyOrEditableMode(false);
		}
		
	}

	/** These functions are used by Lock/Unlock Network change
	 * disable all the menu items in View Only mode 
	 **/
	private void setViewOnlyOrEditableMode(boolean enabled) {
		
		itmSettings.setEnabled(enabled);
		itmRebootNode.setEnabled(enabled);
		itmAdvancedSettings.setEnabled(enabled);
		itmGeneralSettings.setEnabled(enabled);
		itmRestoreDefaults.setEnabled(enabled);
		itmTools.setEnabled(enabled);
		itmMeshCommand.setEnabled(enabled);
		itmDelete.setEnabled(enabled);
		itmPerformanceTest.setEnabled(enabled);
		itmRFInfo.setEnabled(enabled);
    	Enumeration<ExtendedMenuInfo> e 	= meshViewerUI.getExtendedMenuTable().elements();
    	while(e.hasMoreElements()) {
    		ExtendedMenuInfo menuInfo = e.nextElement();
    		if(menuInfo.submenu != null) {
   				menuInfo.menuItem.setEnabled(enabled);
    		}
    	}
		
	}
}
