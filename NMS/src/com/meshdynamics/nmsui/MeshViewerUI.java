/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshViewerUI.java
 * Comments : Main UI window for MeshViewer
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History  
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * | 69  |Jan 29, 2008 | Edit Menu Removed								 |Prachiti|
 * --------------------------------------------------------------------------------
 * | 68  |Sep 06, 2007 | Restructuring									 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 67  |Aug 02,2007  |  apDoubleClickedFromStatusDisplay				 |Prachiti|
 *  --------------------------------------------------------------------------------
 * | 66  |May 24,2007  | ToolTip on tray item Shell 					 |Prachiti|
 *  --------------------------------------------------------------------------------
 * | 65  |May 24,2007  | group selection implementation					 |Abhijit |
 *  --------------------------------------------------------------------------------
 * | 64  |May 24,2007  | updateDLSaturation info modified				 |Imran   |
 *  --------------------------------------------------------------------------------
 * | 63  |Apr 25,2007  | tray icon added								 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 62  |Apr 13, 2007 | Meshviewer Properties changes					 |Prachiti|
 * --------------------------------------------------------------------------------
 * | 61  |Apr 12,2007  | resize function changed						 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 60  |Mar 30,2007  | runMacro menu item disabled if viewer stopped	 |Imran   |
 * --------------------------------------------------------------------------------
 * | 59  |Mar 26,2007  |constructor of meshnetworkui changed			 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 58  |Mar 21,2007  |Viewer starting logic changed					 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 57  |Mar 08,2007  |  removed Edit ACL, VLAN						 |Abhishek|
 *---------------------------------------------------------------------------------
 * | 56  |Mar 08,2007  | additions for rfspace dialog					 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 55  |Feb 27,2007  |  removed unused code							 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 54  |Feb 13, 2007 |selectionListener modified for tabFolder         |Imran   |
 * --------------------------------------------------------------------------------
 * | 53  |Feb 13, 2007 |mouseListener added to tabFolder                 |Abhishek|
 * --------------------------------------------------------------------------------
 * | 51  |Feb 13, 2007 |paintNetworkTab method added                     | Imran  |
 *  --------------------------------------------------------------------------------
 * | 51  |Feb 9, 2007  |notifyHealthAlert added                          | Imran  |
 * |	 |			   |												 |Abhishek|
 * --------------------------------------------------------------------------------
 * | 50  |Dec 28, 2006 |Call for saving ui properties added 			 |Abhijit |
  * --------------------------------------------------------------------------------
 * | 49  |Nov 27, 2006 |refresh button added				             |Abhishek|
  * --------------------------------------------------------------------------------
 * | 48  |Nov 10, 2006 |misc fix							             |Abhishek|
  * --------------------------------------------------------------------------------
 * | 47  |Oct 17, 2006 |misc fix							             |Abhishek|
  * --------------------------------------------------------------------------------
 * | 46  |Jul 05, 2006 |misc fix							             |Prachiti|
 * ------------------------------------------------------------------------------- 
 * | 45	  |June 30,2006| Node name added to Macro Action table			 |Prachiti|
 * ----------------------------------------------------------------------------------
 * | 44  |Jun 27, 2006 | Open Template removed & misc fixes 			 | Bindu  |
 * --------------------------------------------------------------------------------
 * | 44  |Apr 18, 2006 | Node Added in Macro Action if present in network| Mithil |
 * --------------------------------------------------------------------------------
 * | 43  |Mar 29, 2006 | Cleared Table on network switch			  	 | Mithil |
 * --------------------------------------------------------------------------------
 * | 42  |Mar 17, 2006 | Version in msg box generalised				     | Mithil |
 * --------------------------------------------------------------------------------
 * | 41  |Mar 16, 2006 |Changes for ACL							  		 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 40  |Mar 08, 2006 | VLAN QOS Template Dialog Added			  		 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 39  |Mar 01, 2006 | Macro menu name Changes			 		     | Mithil |
 * --------------------------------------------------------------------------------
 * | 38  |Feb 22, 2006 | Led Color Changes			 				    | Mithil |
 * --------------------------------------------------------------------------------
 * | 37  | Feb 15,2006 | STA Log setings menu added				  		 | Mithil |
 * --------------------------------------------------------------------------------
 * | 36  | Feb 13,2006 | Tools Menu added to Main Menu			  		 | Mithil |
 * --------------------------------------------------------------------------------
 * | 35  | Feb 03,2006 | Generic Request handling done thru flags  		 | Mithil |
 * --------------------------------------------------------------------------------
 * | 34   |Feb 03, 2006| Generic Request		   	 					 | abhijit|
 * --------------------------------------------------------------------------------
 * | 33  |Jan 27, 2006 | Crash occurs on hide log window				 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 32  |Jan 27, 2006 | Grp select after starting crashes				 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 31  |Jan 20, 2006 | Grp select shld'nt persists after turned off/on | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 30  |Jan 20, 2006 | Group select done								 | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 29  |Jan 17, 2006 |  Set Monitor Mode - Continuous Transmit mode	 | Mithil |
 * ----------------------------------------------------------------------------------
 * | 28  |Jan 13, 2006 |  Rmvd Edit View Settings Dialog from Edit Menu	 | Mithil |
 * ----------------------------------------------------------------------------------
 * | 27  |Jan 11, 2006 |  Removed System Log Dialog					     | Mithil |
 * ----------------------------------------------------------------------------------
 * | 26  |Jan 04, 2006 |  System.out.println()'s removed			     | Mithil |
 * ----------------------------------------------------------------------------------
 * | 25  |Jan 04, 2006 |  Toplogy View and Map View selection Toggling   | Mithil |
 * ----------------------------------------------------------------------------------
 * | 24  | Jan 04,2006 | Clear All Items from table after deleting AP	 | Mithil |
 * --------------------------------------------------------------------------------
 * | 23  | Jan 03,2006 | Clear All Items from table after stopping viewer| Mithil |
 * --------------------------------------------------------------------------------
 * | 22  | Dec 08,2005 | Menu for log setting added						 | Amit   |
 * --------------------------------------------------------------------------------
 * | 21  | Nov 08,2005 | Change for displaying hb's for selected network | Mithil |
 * --------------------------------------------------------------------------------
 * | 20  |Sept 30,2005 | Images for the start and stop menu added        | Amit   |
 * --------------------------------------------------------------------------------
 * | 19  |Sep 29, 2005 | getMeshNetworkUIs() implemented				 |Bindu   |  
 * -------------------------------------------------------------------------------- 
 * | 18  |Sep 25, 2005 | Misc menu changes         	 					 |Abhijit |  
 * -------------------------------------------------------------------------------- 
 * | 17  |Sep 22, 2005 | Changed LogDisplayUI for Version 3.0         	 |Prachiti|  
 * -------------------------------------------------------------------------------- 
 * | 16  |Sep 19, 2005 | Rearranged menus for Version 3.0              	 |Abhijit |  
 * -------------------------------------------------------------------------------- 
 * | 15  |Sep 08, 2005 | File->Save menmu bug fixed                    	 |Abhijit |  
 * --------------------------------------------------------------------------------
 * | 14  |Aug 29, 2005 | Network deletion fixed                          |Prachiti|  
 * --------------------------------------------------------------------------------
 * | 13  |Aug 25, 2005 | Keyboard menu bug fixed                         | Amit   |  
 * --------------------------------------------------------------------------------
 * | 12  |May 16, 2005 | Two network ap property problem fixed           | Anand  |
 * --------------------------------------------------------------------------------
 * | 11  |May 12, 2005 | Listener added on CTabFolder   		         | Amit   |
 * --------------------------------------------------------------------------------
 * | 10  |May 06, 2005 | Images added to graph Menu Items  		         | Anand  |
 * --------------------------------------------------------------------------------
 * |  9  |May 04, 2005 | Configuration Polling added       		         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  8  |May 02, 2005 | New look tab changes           		         | Anand  |
 * --------------------------------------------------------------------------------
 * |  7  |Apr 27, 2005 | New look tabs & ui changes      		         | Anand  |
 * --------------------------------------------------------------------------------
 * |  6  |Apr 25, 2005 | Accelerators & images added to menu items       | Amit   |
 * --------------------------------------------------------------------------------
 * |  5  |Mar 25, 2005 | Images moved to branding native dll.            | Anand  |
 * --------------------------------------------------------------------------------
 * |  4  |Mar 04, 2005 | Change in updatePropertyTabs		             | Bindu  |
 * --------------------------------------------------------------------------------
 * |  3  |Feb 11, 2005 | Change Detection-PropertyUI changes             | Anand  |
 * --------------------------------------------------------------------------------
 * |  2  |Feb 02, 2005 | Changes for Customized MeshViewer               | Anand  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 01, 2005 | Icons moved as resource                         | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 */

package com.meshdynamics.nmsui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabFolder2Adapter;
import org.eclipse.swt.custom.CTabFolderEvent;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.*;

import com.meshdynamics.aes.AES;
import com.meshdynamics.api.NMS;
import com.meshdynamics.api.NMSUI;
import com.meshdynamics.api.NMS.Network;
import com.meshdynamics.api.NMS.Node;
import com.meshdynamics.api.NMSUI.Alert;
import com.meshdynamics.api.NMSUI.ContextMenuSection;
import com.meshdynamics.api.NMSUI.MenuCommandHandler;
import com.meshdynamics.api.NMSUI.NodeText;
import com.meshdynamics.api.NMSUI.StatusTab;
import com.meshdynamics.api.NMSUI.StatusTabHeader;
import com.meshdynamics.api.NMSUI.StatusTabRow;
import com.meshdynamics.api.script.ScriptExecEnv;
import com.meshdynamics.hmacsha1.HMAC_SHA1;
import com.meshdynamics.meshviewer.IMeshViewerListener;
import com.meshdynamics.meshviewer.MeshViewer;
import com.meshdynamics.meshviewer.NMS_VERSION2;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.mesh.IMessageHandler;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;
import com.meshdynamics.mg.MGClient;
import com.meshdynamics.nmsui.MeshViewerProperties.MGProperties;
import com.meshdynamics.nmsui.dialogs.AboutDialog;
import com.meshdynamics.nmsui.dialogs.HealthMonitorStatusDlg;
import com.meshdynamics.nmsui.dialogs.network.OptionMessageBox;
import com.meshdynamics.nmsui.dialogs.rfspace.RFSpaceInfoDlg;
import com.meshdynamics.nmsui.help.ContextHelp;
import com.meshdynamics.nmsui.help.contextHelpEnum;
import com.meshdynamics.nmsui.http.IJsonMeshInfoProvider;
import com.meshdynamics.nmsui.http.MeshJsonServer;
import com.meshdynamics.nmsui.mesh.ExtendedMenuInfo;
import com.meshdynamics.nmsui.mesh.MeshNetworkMenu;
import com.meshdynamics.nmsui.mesh.MeshNetworkUI;
import com.meshdynamics.nmsui.statusdisplay.StatusDisplayController;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.Bytes;
import com.meshdynamics.util.DateTime;
import com.meshdynamics.util.MacAddress;


public class MeshViewerUI implements IMeshViewerListener, IMessageHandler, IJsonMeshInfoProvider {

    static final int 				PROPERTY_WINDOW 			= 0;
    static final int 				LOG_WINDOW 					= 1;
    static final int 				GRID_LINES 					= 2;
    static final int 				NEIGHBOUR_LINES 			= 3;
    static final int 				NEW_LOG_WINDOW 					= 4;
    
    private static int   			MIN_LOG_HEIGTH				= 250;
	private static int   			MIN_PROP_WIDTH				= 295;
	private static final int 		COOL_BAR_HEIGHT 			= 24;
	private static final int 		STATUS_BAR_HEIGHT 			= 20;
	
	private Canvas 		    					propComp;
	private Canvas 								compTabFolder; 
	private Sash 								vSash,hSash;
	private CoolBar  							coolBar;
	private CTabFolder 							tabFolder;
	private Hashtable<String, MeshNetworkUI>	meshNetworkUIs;
	private Canvas 								logCanvas;
	private MenuItem							startViewer,stopViewer;
	private MenuItem							startMG,stopMG,MGMenu;
	private ToolItem							startToolItem, stopToolItem;
	private ToolItem							mgStartToolItem, mgStopToolItem;
	private ToolItem							groupSelOn, explorer, toggleViews;
	private MenuItem							propItem, statusItem,toolsMenu,
												viewItem,staActivity,mgSettings,meshCommand,servSettings,
												fipsWizardItem,gpsWizardItem, iperfSrcDestTool;
	private MenuItem							viewDetailItem, viewOnlineMapItem,viewOfflineMapItem;
	private MenuItem							wizardsItem;
	private ToolItem							propToolItem, logToolItem;
//	/private ToolItem							healthMonitorItem;
	private MenuItem							groupSelectionItem;
	private MenuItem 							runMacro,searchItem; 
	private StatusDisplayController				statusDisplayUI;
	private APPropertyUI    	 				apPropertyUI;
	private AccessPoint							currentSelectedAp;
    private MeshViewer 							meshViewer;
    public  static Display 						display; 
    private boolean 							showGridLines;
    private boolean 							showNeighbourLines;
    private Image								portImage;
    private	RFSpaceInfoDlg 	    				rfSpaceInfoDlg;
    private MeshNetworkUI						selectedNetworkUI;
    private boolean								viewerRunning;
    private boolean								mgRunning;
    private MeshNetworkMenu 					popupMenu;
    private boolean								groupSelectionEnabled;
    private PerspectiveSettings 				perspectiveSettings;
    private Vector<ExtendedMenuInfo>			extendedMenuTable;
    private Vector<IJsonAp>						jsonApList;
    private AccessPointMenu 					accessPointMenu;
    private Menu 								mnuMain;
    private ContextHelp							contextHelp;
    private AlertManager						alertManager;
    private NMS									nmsInstance;
    private Shell								shell;
    private Label								statusBar;
	//private TrayItem 							trayItem;
	//private String 								trayToolTip;
    private MeshJsonServer						jsonServer;
    private NMSUIImplementation					extensionHandler;
    private boolean								showUI;
    private boolean								serviceMode;
    private SplashScreen						splash;
    private MenuItem							scriptConsoleItem;
    private Shell                               logTabsWindow;
    private boolean                             logTabsInNewWindow;

    public MeshViewerUI(boolean serviceMode) {

    	display 			= Display.getDefault();
    	this.serviceMode	= serviceMode;
    	
    	if(checkFolders() == false) {
    		return;
    	}
    	
    	meshViewer 	= MeshViewer.getInstance();
    	nmsInstance	= NMS.getInstance();
    	readMeshViewerSettings();
    	
		currentSelectedAp		= null;
		showGridLines   		= true;
		showNeighbourLines 		= true;
		rfSpaceInfoDlg 			= null;
		viewerRunning 			= false;
		mgRunning				= false;
		groupSelectionEnabled	= false;
		showUI					= false;
		splash					= null;
		
		meshNetworkUIs			= new Hashtable<String, MeshNetworkUI>();
		extensionHandler		= new NMSUIImplementation(this);
		alertManager			= new AlertManager(this);
		
		if(serviceMode == true)
			processProperties();
    }
    
    private void showLoadingStatus(String status) {
    	if(splash != null)
    		splash.setText(status);
    	else
    		System.out.println(status);
    }
    
    private void initUI() throws InterruptedException {
    	
    	perspectiveSettings 	= new PerspectiveSettings(this);
    	showLoadingStatus("Starting context help server");
    	contextHelp 			= new ContextHelp(this);
    	Thread.sleep(1000);    	
    	System.setErr(new ErrPrintStream(this, System.err));
		jsonApList				= new Vector<IJsonAp>();
		showLoadingStatus("Starting script extension handler");
		Thread.sleep(1000);
		extendedMenuTable 		= new Vector<ExtendedMenuInfo>();
		extensionHandler.loadExtensions();
		showLoadingStatus("Starting web view server");
		Thread.sleep(1000);
		jsonServer 				= new MeshJsonServer(this);
		jsonServer.start();
		showHidePropertyUI(perspectiveSettings.showProperty);
		showHideLogUI(perspectiveSettings.showStatusDisplay);
		loadUIProperties();
    }
    
    private void uninitUI() {
    	
    	alertManager.stopServices();
    	jsonServer.stopServer();
    	contextHelp.stopServer();
    	jsonServer 			= null;
    	extensionHandler.unloadExtensions();
    	jsonApList			= null;
    	contextHelp			= null;
    	meshNetworkUIs		= null;
    	
		//trayItem.setVisible(false);
		shell.dispose();
    	
    }
    
	private static void readMeshViewerSettings() {
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(MFile.getConfigPath() + "/meshviewer.properties"));
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			return;
		}
		String input = null;
		 try {
			while (( input = br.readLine()) != null){
				input = input.trim();
				if(input.equals(""))
					continue;
				if(input.charAt(0) == '#')
					continue;
				
				String values[] = input.split("=");
				if(values.length != 2)
					continue;
				
				if(values[0].equals("content.path")) {
				    MeshViewerProperties.contentPath = values[1].trim();
				}else if(values[0].equals("start.auto")){
					MeshViewerProperties.startViewer = Integer.parseInt(values[1]);
				}else if(values[0].equals("mobility.advanced")) {
					MeshViewerProperties.mobilityAdvanced = Integer.parseInt(values[1].trim());
				}else if(values[0].equals("meshcommand.advanced")) {
					MeshViewerProperties.meshCommandAdvanced = Integer.parseInt(values[1].trim());
				}else if(values[0].equals("rfeditor.enabled")){
					MeshViewerProperties.rfEditorEnabled = Integer.parseInt(values[1].trim());
				}else if(values[0].equals("OfflineMapView.GetTilesFromInternet")){
					MeshViewerProperties.getMapTilesFromWeb = Integer.parseInt(values[1].trim());
				}else if(values[0].equals("networkopen.auto")){
					int value = Integer.parseInt(values[1].trim());
					MeshViewerProperties.autoOpenNetwork = (value == 1) ? true : false;
				}else if(values[0].equals("startmg.auto")){
					MeshViewerProperties.startMGClient = Integer.parseInt(values[1]);
				}
			}
			
			br.close();
			
		} catch (Exception e1) {
			System.out.println(e1.getMessage());
		} 
	}
    
    private boolean checkFolders() {
		
    	showLoadingStatus("Checking for required folders...");
		String[] requiredFolders = {
			MFile.getSystemPath(),
			MFile.getConfigPath(),			
			MFile.getWebPath()
		};
		
		for(String fileName : requiredFolders) {
			File f = new File(fileName);
			if(f.exists() == false || f.isDirectory() == false) {
				showLoadingStatus("Required folder " + fileName + " is missing.Please reinstall...");
				return false;
			}
		}
		
		showLoadingStatus("Checking for support folders...");
		
		String[] supportFolders = {	
				MFile.getAlertScriptPath(), 
				MFile.getExtensionsPath(),
				MFile.getIconsPath(),
				MFile.getLogPath(),
				MFile.getMapPath(),
				MFile.getNetworkPath(),
				MFile.getNodeFilePath(),
				MFile.getTemplatePath(),
				MFile.getUpdatesPath()
		};

		for(String fileName : supportFolders) {
			File f = new File(fileName);
			if(f.exists() == false)
				f.mkdir();
			if(f.isDirectory() == false) {
				f.delete();
				f.mkdir();
			}
		}
		
		showLoadingStatus("Folders structure OK...");
		return true;
	}
/*    
	private void trayToolTipNotifyListener(String statusText) {
		
		if(showUI == false)
			return;
		
    	if(trayItem.isDisposed() == true) {
    		return;
    	}
    	
    	if(selectedNetworkUI == null)
    		return;
    	
    	trayToolTip =  "Network Viewer "+ NMS_VERSION2.buildmajor+"."+NMS_VERSION2.buildminor+"."+NMS_VERSION2.buildid;
    	trayToolTip += " "+ SWT.CR + SWT.LF;
    	trayToolTip += "Network :" + selectedNetworkUI.getNwName();
    	trayToolTip += " "+ SWT.CR + SWT.LF;
    	trayToolTip += statusText; 
		trayItem.setToolTipText(trayToolTip);
	 
    }

    private void createTrayIcon() {
    	
    	if(shell == null || display == null)
    		return;
    	
    	Image 			image 	= Branding.getIconImage();
    	final Tray 		tray 	= display.getSystemTray ();
		trayItem 				= new TrayItem (tray, SWT.NONE);
		
		trayItem.setToolTipText(trayToolTip);
		trayItem.setImage (image);
		
		final Menu 	menu 	= new Menu (shell, SWT.POP_UP);
		MenuItem 	mi 		= new MenuItem (menu, SWT.PUSH);
		
		mi.setText ("Show");
		mi.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event event) {
				shell.setVisible(true);
				shell.setMaximized(true);
			}

		});
		
		mi = new MenuItem (menu, SWT.PUSH);
		mi.setText ("Exit");
		
		mi.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event event) {
				close();
			}
		});

		trayItem.addListener (SWT.MenuDetect, new Listener () {
			public void handleEvent (Event event) {
				menu.setVisible (true);
			}
		});
		
		trayItem.addListener(SWT.Selection,new  Listener(){
			public void handleEvent(Event arg0) {
				shell.setMaximized(true);
			}
		});
		
    }
*/
    
	private void saveUIProperties() {
		if(showUI == false)
			return;
		
    	perspectiveSettings.logHeight = shell.getClientArea().height + coolBar.getBounds().height - hSash.getBounds().y;
    	perspectiveSettings.propWidth = propComp.getBounds().width;
    	MGProperties.writeSettings();
		apPropertyUI.saveUIProperties(perspectiveSettings.getPropertyTableInfo());
		statusDisplayUI.saveUIProperties(perspectiveSettings.getLogTableInfo());
		perspectiveSettings.writeSettings();
	}

	private void loadUIProperties() {
		if(showUI == false)
			return;
		
    	if(perspectiveSettings.readSettings() == true) {
    		MIN_LOG_HEIGTH 	= perspectiveSettings.logHeight;
    		MIN_PROP_WIDTH 	= perspectiveSettings.propWidth;
    	}
		
    	MGProperties.readSettings();
		apPropertyUI.loadUIProperties(perspectiveSettings.getPropertyTableInfo());
		statusDisplayUI.loadUIProperties(perspectiveSettings.getLogTableInfo());
	}

	private void processProperties() {

		if(MeshViewerProperties.startViewer == 1) {
			showLoadingStatus("Starting IMCP engine");
		    meshViewer.startViewer();
		} else
			showLoadingStatus("IMCP engine startup skipped.");
		
		if(openMeshNetwork(Mesh.DEFAULT_MESH_ID) == false) {
			createMeshNetwork(Mesh.DEFAULT_MESH_ID,Mesh.DEFAULT_MESH_ENC_KEY, MeshNetwork.NETWORK_REGULAR);
		}
        if(MeshViewerProperties.autoOpenNetwork == true) {
        	autoOpenNetworks();
        }
		if(MeshViewerProperties.startMGClient == 1) {
			showLoadingStatus("Starting MG Client");
		    startMGClient();
		} else
			showLoadingStatus("MG client startup skipped.");
		
		
	}
	
	public static int msgBox(String message, int flags) {
		MessageBox messageBox = new MessageBox(new Shell(), flags);
		messageBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
		messageBox.setMessage(message);			
		return messageBox.open();
	}
	
	/**
	 * @param splash 
	 * @throws InterruptedException 
	 * 
	 */
	public void open(SplashScreen splash) throws InterruptedException {

		this.splash = splash;
		if(splash != null)
			splash.setMaxProgressSteps(14);
		
    	if(checkFolders() == false) {
    		showLoadingStatus("Cannot open ui.Please check installation folder");
    		return;
    	}
		
		if(meshViewer == null || nmsInstance == null) {
			showLoadingStatus("Cannot get nms instance.Please restart all NMS services");
			return;
		}

		showUI 		= true;
    	shell 		= new Shell(display, SWT.MENU|SWT.MAX|SWT.MIN|SWT.RESIZE);
		shell.addShellListener(new ShellAdapter(){
			@Override
			public void shellClosed(ShellEvent shellevent) {
				close();
			}
		});

		showLoadingStatus("Initializing main screen");
    	shell.setImage(Branding.getIconImage());
    	shell.setText(Branding.getBrandTitle() + " Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
    	showLoadingStatus("Creating menus");
		createMenus();
		showLoadingStatus("Creating controls");
		createControls();
		//createTrayIcon();
		shell.setMenuBar(mnuMain);
		showLoadingStatus("Initializing ui components");
		initUI();
		meshViewer.setMeshViewerListener(this);
		meshViewer.addSipEventListener(statusDisplayUI);
		showLoadingStatus("Processing startup properties");
		processProperties();
		showLoadingStatus("Processing startup properties done");
		Thread.sleep(1000);
	
		shell.pack();
		shell.setMaximized(true);
		if(splash != null)
			splash.close();
		this.splash	= null;
		
		shell.open();
		while(shell.isDisposed() == false) {
			try {
				if(display.isDisposed() == true) {
					return;
				}
				if(!display.readAndDispatch()) 
					display.sleep();
			} catch(Exception e) {
			    e.printStackTrace();
				System.out.println(e.getMessage());
			}
		}
        
	}

	private void autoOpenNetworks() {
	
		File 		file 	= new File(MFile.getNetworkPath());
		String[] 	files 	= file.list();
		
		if(files.length <= 0) {
			return;
		}
		
		for(int i=0;i<files.length;i++) {
			
			if(files[i].endsWith(".net") == false) {
				continue;
			}
			
			String networkName 	= files[i].substring(0,files[i].length()-4);
			
			if(meshViewer.getMeshNetworkByName(networkName) != null) {
				continue;
			}
		
			openMeshNetwork(networkName);
		}
		
	}
	
	/**
	 * 
	 */
	private void createCoolBar(MenuListener menuListener) {
		coolBar = new CoolBar (shell, SWT.FLAT);
		coolBar.setBounds(0, 0, shell.getClientArea().width, COOL_BAR_HEIGHT);
		addCoolBarItems(menuListener);
		
	}
	
	/**
	 * @param coolBar
	 */
	private void addCoolBarItems(MenuListener menuListener) {

		ToolBar toolBar = new ToolBar(coolBar,SWT.FLAT);
		toolBar.setBounds(0,5,400,COOL_BAR_HEIGHT);	
		
        CoolItem coolItem = new CoolItem(coolBar,SWT.PUSH);
        coolItem.setControl(toolBar);

        ToolItem toolItem = new ToolItem(toolBar,SWT.IMAGE_BMP);
        toolItem.setImage(new Image(null,  
                MeshViewerUI.class.getResourceAsStream("icons/OpenNetwork.gif"))); 
        toolItem.setToolTipText("Open Network");
        toolItem.setData(new Integer(MenuListener.FILE_ITEM_OPEN_NETWORK));
        toolItem.addSelectionListener(menuListener);
        
        toolItem = new ToolItem(toolBar,SWT.IMAGE_BMP);
        toolItem.setImage(new Image(null,  
                MeshViewerUI.class.getResourceAsStream("icons/SaveNetwork.gif"))); 
        toolItem.setToolTipText("Save Network");
        toolItem.setData(new Integer(MenuListener.FILE_ITEM_SAVE_NETWORK));
        toolItem.addSelectionListener(menuListener);
        
        Point point = toolBar.computeSize(SWT.DEFAULT,SWT.DEFAULT);
	    point 		= coolItem.computeSize(point.x,point.y);
	    coolItem.setSize(point);

	    new ToolItem(toolBar,SWT.SEPARATOR);
        
        startToolItem = new ToolItem(toolBar,SWT.IMAGE_BMP);	    
        startToolItem.setImage(new Image(null,  
                MeshViewerUI.class.getResourceAsStream("icons/StartViewer.gif"))); 
        startToolItem.setToolTipText("Start Viewer");
        startToolItem.setData(new Integer(MenuListener.RUN_ITEM_START_VIEWER));
        startToolItem.addSelectionListener(menuListener);
        
        stopToolItem = new ToolItem(toolBar,SWT.IMAGE_BMP);
        stopToolItem.setImage(new Image(null,  
                MeshViewerUI.class.getResourceAsStream("icons/StopViewer.gif"))); 
        stopToolItem.setToolTipText("Stop Viewer");
        stopToolItem.setData(new Integer(MenuListener.RUN_ITEM_STOP_VIEWER));
        stopToolItem.setEnabled(false);
        stopToolItem.addSelectionListener(menuListener);
        
        new ToolItem(toolBar,SWT.SEPARATOR);
        
        toolItem = new ToolItem(toolBar,SWT.IMAGE_BMP|SWT.CHECK);
        
        toolItem.setImage(new Image(null,  
                MeshViewerUI.class.getResourceAsStream("icons/PropertiesTab.gif"))); 
        toolItem.setToolTipText("Show Properties");
        toolItem.setData(new Integer(MenuListener.VIEW_ITEM_SHOW_PROPERTY_VIEW));
        toolItem.setSelection(true);
        toolItem.addSelectionListener(menuListener);
        propToolItem = toolItem;
        
        toolItem = new ToolItem(toolBar,SWT.IMAGE_BMP|SWT.CHECK);
        toolItem.setImage(new Image(null,  
                MeshViewerUI.class.getResourceAsStream("icons/StatusTab.gif"))); 
        toolItem.setToolTipText("Show Status");
        toolItem.setData(new Integer(MenuListener.VIEW_ITEM_SHOW_MESSAGES_VIEW));
        toolItem.setSelection(true);
        toolItem.addSelectionListener(menuListener);

        new ToolItem(toolBar,SWT.SEPARATOR);
        
        groupSelOn = new ToolItem(toolBar,SWT.IMAGE_BMP|SWT.CHECK);
        groupSelOn.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/GroupSelection.gif")));
        groupSelOn.setToolTipText("Group Select");
        groupSelOn.setData(new Integer(MenuListener.RUN_ITEM_GROUP_SELECTION));
        groupSelOn.addSelectionListener(menuListener);
        
        new ToolItem(toolBar,SWT.SEPARATOR);

        logToolItem = toolItem;
                        
        mgStartToolItem = new ToolItem(toolBar,SWT.IMAGE_BMP);	    
        mgStartToolItem.setImage(new Image(null,  
                MeshViewerUI.class.getResourceAsStream("icons/StartMG.gif"))); 
        mgStartToolItem.setToolTipText("Start Mesh Gateway Client");
        mgStartToolItem.setData(new Integer(MenuListener.MG_ITEM_START));
        mgStartToolItem.addSelectionListener(menuListener);
        
        mgStopToolItem = new ToolItem(toolBar,SWT.IMAGE_BMP);
        mgStopToolItem.setImage(new Image(null,  
                MeshViewerUI.class.getResourceAsStream("icons/StopMG.gif"))); 
        mgStopToolItem.setToolTipText("Stop Mesh Gateway Client");
        mgStopToolItem.setData(new Integer(MenuListener.MG_ITEM_STOP));
        mgStopToolItem.setEnabled(false);
        mgStopToolItem.addSelectionListener(menuListener);
        mgStopToolItem.setEnabled(false);
        
        new ToolItem(toolBar,SWT.SEPARATOR);  
        
      /*        
        healthMonitorItem  = new ToolItem(toolBar,SWT.IMAGE_BMP|SWT.PUSH);
        healthMonitorItem.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/ShowHealthStatus.gif")));
        healthMonitorItem.setToolTipText("Health Status for Selected Network");
        healthMonitorItem.setData(new Integer(MenuListener.VIEWPOPUP_ITEM_HEALTH_MONITOR_STATUS));
        healthMonitorItem.addSelectionListener(menuListener);
        
        new ToolItem(toolBar,SWT.SEPARATOR);
 */       
        toolItem = new ToolItem(toolBar,SWT.IMAGE_BMP);
        
        toolItem.setImage(new Image(null,  
                MeshViewerUI.class.getResourceAsStream("icons/Search.gif"))); 
        toolItem.setToolTipText("Search Mesh Node");
        toolItem.setData(new Integer(MenuListener.SEARCH_ITEM_BY_ANY));
        toolItem.addSelectionListener(menuListener);
       
        new ToolItem(toolBar,SWT.SEPARATOR);
        
        explorer = new ToolItem(toolBar,SWT.IMAGE_BMP);
        
        explorer.setImage(new Image(null,  
        		MeshViewerUI.class.getResourceAsStream("icons/HomeFolder.gif"))); 
        explorer.setToolTipText("Updates Folder");
        explorer.setData(new Integer(MenuListener.WINDOWS_EXPLORER));
        explorer.addSelectionListener(menuListener);
       
        toggleViews = new ToolItem(toolBar,SWT.IMAGE_BMP);
        
        toggleViews.setImage(new Image(null,  
        		MeshViewerUI.class.getResourceAsStream("icons/Views.gif"))); 
        toggleViews.setToolTipText("Toggle Views");
        toggleViews.setData(new Integer(MenuListener.VIEW_ITEM_TOGGLE_VIEWS));
        toggleViews.addSelectionListener(menuListener);
	
	}
		

	/**
	 * 
	 */
	private void createControls() {
	
		if(shell == null || showUI == false)
			return;
		
		shell.addControlListener(new ControlAdapter(){
            public void controlResized(ControlEvent arg0) {
                coolBar.setBounds(0,0,shell.getClientArea().width, COOL_BAR_HEIGHT);
                statusBar.setBounds(0,shell.getClientArea().height-STATUS_BAR_HEIGHT,shell.getClientArea().width, STATUS_BAR_HEIGHT);
                hSash.setBounds(0,shell.getClientArea().height-MIN_LOG_HEIGTH+coolBar.getBounds().height,shell.getClientArea().width,3);
                sashResized(false);
            }
        });
	    
		hSash = new Sash(shell, SWT.BORDER|SWT.HORIZONTAL|SWT.FLAT);
		hSash.setBounds(0,shell.getClientArea().height-MIN_LOG_HEIGTH+coolBar.getBounds().height,shell.getClientArea().width,3);
		hSash.addSelectionListener(new SelectionAdapter(){

			public void widgetSelected(SelectionEvent arg0) {
				
				if(arg0.detail == SWT.NONE) {
				    hSash.setBounds(arg0.x, arg0.y, arg0.width, 3);
				    sashResized(false);
				}
		 				
			}	    	
	    });
			    	    
		vSash = new Sash(shell,SWT.BORDER|SWT.VERTICAL|SWT.FLAT);
		vSash.setBounds(MIN_PROP_WIDTH,COOL_BAR_HEIGHT,3,shell.getClientArea().height-MIN_LOG_HEIGTH);
		
		vSash.addSelectionListener(new SelectionAdapter(){
	    	public void widgetSelected(SelectionEvent arg0) {
	    		if (arg0.detail == SWT.NONE) {
	    		    vSash.setBounds(arg0.x, arg0.y, 3, arg0.height);
	    		    sashResized(true);
	    		}
			}
		});
	    
		propComp = new Canvas(shell, SWT.SIMPLE);

		compTabFolder = new Canvas(shell, SWT.SIMPLE);
		compTabFolder.addControlListener(new ControlAdapter(){
            public void controlResized(ControlEvent arg0) {
        		tabFolder.setBounds(1,1, compTabFolder.getBounds().width-2, compTabFolder.getBounds().height-2);
            }}
	    );
				
	    tabFolder = new CTabFolder(compTabFolder,SWT.BORDER|SWT.CLOSE);
	    tabFolder.setTabHeight(24);
	    tabFolder.setSimple(false);
	    tabFolder.setMinimumCharacters(8);
	    tabFolder.setSelectionForeground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		final Display display = Display.getCurrent();
		tabFolder.setSelectionBackground(new Color[]{display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND), 
                display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND),
                display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT), 
                display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT)},
       new int[] {40, 70, 100}, true);
	    
		tabFolder.addMouseListener(new MouseAdapter(){

			public void mouseUp(MouseEvent arg0) {
				
				if(arg0.button == 3) {
					CTabItem 		item 		= tabFolder.getSelection();
	            	MeshNetworkUI 	networkUI 	= (MeshNetworkUI) item.getControl();
	            	
	            	popupMenu.setNetworkUI(networkUI);
	            	tabFolder.setMenu(popupMenu.getMenu());
            		tabFolder.getMenu();
				}
			}
		});

		tabFolder.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent arg0) {
            	CTabItem 		item 		= tabFolder.getSelection();
            	MeshNetworkUI 	networkUI 	= (MeshNetworkUI) item.getControl();
            	setLockThisNetwork(networkUI);
            	tabFolderSelectionHandler((CTabItem) arg0.item);
            }
        });	    
	   
	    tabFolder.addCTabFolder2Listener(new CTabFolder2Adapter(){
			public void close(CTabFolderEvent arg0) {
				CTabItem item 			=	(CTabItem)arg0.item;
				arg0.doit 				= 	handleNetworkClose(item.getText());
			}
			
		});
	    
	    ContextHelp.addContextHelpHandler(shell,contextHelpEnum.MAINWINDOW);
	    logCanvas = new Canvas(shell,SWT.SIMPLE);
        //  Initially the logCanvas is housed in the MeshViewerUI and not separate window
        logTabsInNewWindow = false;

	    statusBar = new Label(shell, SWT.FLAT|SWT.BORDER|SWT.SIMPLE);
	    
	    sashResized(false);
	     
	    statusDisplayUI  	=  new StatusDisplayController(logCanvas,this);
	    apPropertyUI     	=  new APPropertyUI(propComp,this);

	    ContextHelp.addContextHelpHandler(propComp,contextHelpEnum.PROPERTIESWINDOW);
	}
	
	protected boolean handleNetworkClose(String networkName) {
		
		if(networkName.equalsIgnoreCase(Mesh.DEFAULT_MESH_ID) == true){
			MessageBox messageBox = new MessageBox(new Shell(),SWT.OK|SWT.ICON_INFORMATION);
			messageBox.setText("Network Viewer " + NMS_VERSION2.buildmajor + "." + NMS_VERSION2.buildminor);
			messageBox.setMessage("Default network cannot be removed.");			
			messageBox.open();
			return false;
		}

		OptionMessageBox msgBox = new OptionMessageBox("Do you want to Delete or Save and Close network",
														"Save and Close Network",
														"Delete Network");
		msgBox.show();
		
		if(msgBox.getBtnSelected() == -1){
			return false;
		} else if(msgBox.getBtnSelected() == 1){
			saveMeshNetwork(networkName);
			closeMeshNetwork(networkName);
		}else if(msgBox.getBtnSelected() == 2){
			deleteMeshNetwork(networkName);
		}
		return true;
	}

	private void tabFolderSelectionHandler(CTabItem selectedItem) {

    	/*
		 * Following code has to be enabled if health monitor is 
		 * required to be shown in UI
		 */
/*    	
 		MeshNetworkUI	networkUI		= (MeshNetworkUI) selectedItem.getControl();
    	final int 		healthStatus 	= networkUI.getCurrentHealthStatus();
    	
    	display.asyncExec(new Runnable() {
    		public void run(){
    			paintNetworkTab(healthStatus);
       		}
    	});
*/
    	
    	selectedNetworkUI 	= (MeshNetworkUI)selectedItem.getControl();
        statusDisplayUI.networkSelected(selectedNetworkUI.getNwName());
        
        groupSelectionItem.setSelection(selectedNetworkUI.isGroupSelectionEnabled());
        groupSelOn.setSelection(selectedNetworkUI.isGroupSelectionEnabled());
        
        AccessPoint			ap			= selectedNetworkUI.getSelectedAccessPoint();
        
        apSelectionChanged(ap);

        setViewSelection(selectedNetworkUI.getCurrentViewType());
        selectedNetworkUI.notifyStatusBar(selectedNetworkUI.getStatusText());
        extensionHandler.notifyNetworkSelected(selectedNetworkUI.getNwName());
	}


	/**
	 * 
	 */
	protected void showHealthMonitorStaus() {
		
		MeshNetwork currentNetwork  =	selectedNetworkUI.getMeshNetwork();  
		String meshId   			=   currentNetwork.getNwName();
		
		HealthMonitorStatusDlg hmStatusDlg = new HealthMonitorStatusDlg(currentNetwork);
		hmStatusDlg.setTitle("Health Status for " + meshId + " Network");
		hmStatusDlg.show();  
	}


	private void sashResized(boolean vertical) {
		
	    Rectangle rect 			= new Rectangle(0,0,0,0);	    
	    Rectangle vSashbnd 		= vSash.getBounds();
	    Rectangle hSashbnd 		= hSash.getBounds();
	    Rectangle coolbarBnd 	= coolBar.getBounds();
	    Rectangle shellBounds	= shell.getClientArea();
	    
	    if(!vertical){
	        if(hSash.getVisible())
	            vSashbnd.height = hSashbnd.y-coolbarBnd.height;
	        else
	            vSashbnd.height = shellBounds.height - statusBar.getBounds().height - coolbarBnd.height;
	            
            vSash.setBounds(vSashbnd);
			logCanvas.setBounds(0, hSashbnd.y+hSashbnd.height, shellBounds.width, shellBounds.height - (hSashbnd.y+hSashbnd.height+statusBar.getBounds().height));
		}	    
		propComp.setBounds(0, coolbarBnd.y+coolbarBnd.height, vSashbnd.x-1,vSashbnd.height);
		
		if(hSashbnd.y < (coolbarBnd.height + coolbarBnd.y)) {
			hSash.setBounds(0,coolbarBnd.height+coolbarBnd.y,shellBounds.width,3);
		}
		
		rect.x 			= 0;
		rect.y 			= coolbarBnd.y + coolbarBnd.height + hSashbnd.height;
	    rect.width 		= shellBounds.width;
		rect.height 	= shellBounds.height - statusBar.getBounds().height - coolbarBnd.height - hSashbnd.height;
		if(propComp.getVisible()) {
		    rect.x 		= vSashbnd.x+vSashbnd.width;
			rect.width 	= shellBounds.width-(rect.x)-1;
		}
		
		if(hSash.getVisible()) {
		    rect.height = vSashbnd.height-1;
		}
		
		compTabFolder.setBounds(rect);
	}
	
	protected void showHidePropertyUI(boolean flag){
		if(showUI == false)
			return;
		
	    propComp.setVisible(flag);
	    vSash.setVisible(flag);
	    propToolItem.setSelection(flag);
	    propItem.setSelection(flag);
	    sashResized(false);
		perspectiveSettings.showProperty = flag;
	}
	
	/**
	 * @param selection
	 */
	protected void showHideLogUI(boolean flag) {
		if(showUI == false)
			return;
	    logCanvas.setVisible(flag);
        hSash.setVisible(flag);
        if((null != logTabsWindow) && logTabsInNewWindow) {
            hSash.setVisible(false);
            logTabsWindow.setVisible(flag);
        }
	    logToolItem.setSelection(flag);
	    statusItem.setSelection(flag);
	    sashResized(false);
        //call logTabsWindow.layout() after sashResized as it resizes the tabcontrol
        if((null != logTabsWindow) && logTabsInNewWindow) {
            logTabsWindow.layout();
        }
		perspectiveSettings.showStatusDisplay = flag;
	}
	
	protected ExtendedMenuInfo createExtendedMenu(ContextMenuSection parentSection, String commandId, String text, Object icon, MenuCommandHandler handler) {

		if(showUI == false)
			return null;
		
	    ExtendedMenuInfo extendedInfo 	= new ExtendedMenuInfo();
        extendedInfo.parentSection 		= parentSection;
        extendedInfo.commandId	 		= commandId;
        extendedInfo.text				= text;
        extendedInfo.icon				= icon;
        extendedInfo.handler			= handler;	
	    extendedMenuTable.add(extendedInfo);
	    extendedInfo.menuID				=  AccessPointMenu.EXTENDED_MENU + extendedMenuTable.size();
	    
	    return extendedInfo;
	}
	
	protected void destroyExtendedMenu(ContextMenuSection extendedMenuInfo) {
		if(showUI == false)
			return;
		
	    extendedMenuTable.remove(extendedMenuInfo);
	}
	
	private Menu createMenus() {

		if(showUI == false || shell == null)
			return null;
		
		MenuListener menuListener 	= new MenuListener(meshViewer, this);
		popupMenu					= new MeshNetworkMenu(shell, menuListener);
		mnuMain						= new Menu(shell, SWT.BORDER|SWT.FLAT|SWT.BAR);
		
		createFileMenu(mnuMain,menuListener);
		createRunMenu(mnuMain,menuListener);
		createSearchMenu(mnuMain,menuListener);
		createViewMenu(mnuMain,menuListener);
		createToolsMenu(mnuMain,menuListener);
		createMGMenu(mnuMain,menuListener);
		createHelpMenu(mnuMain,menuListener);

		createCoolBar(menuListener);
		return mnuMain;
	}

	private void createSearchMenu(Menu main_menu,MenuListener menuListener) {

		Menu menu = new Menu(main_menu);
		
		MenuItem searchMenu = new MenuItem(main_menu,SWT.CASCADE);
		searchMenu.setText("Search");
		searchMenu.setMenu(menu);

		searchItem = new MenuItem(menu,SWT.PUSH);
		searchItem.setText("Search        F3");
		searchItem.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/Search.gif")));
		searchItem.setData(new Integer(MenuListener.SEARCH_ITEM_BY_ANY));
		searchItem.addSelectionListener(menuListener);
		searchItem.setAccelerator(SWT.F3);
		
	}
	
	private void createMGMenu(Menu main_menu,MenuListener menuListener) {

		Menu menu = new Menu(main_menu);
		
		MGMenu = new MenuItem(main_menu,SWT.CASCADE);
		MGMenu.setText("Remote Management");
		MGMenu.setMenu(menu);
	    
		startMG = new MenuItem(menu,SWT.PUSH);
		startMG.setText("Start Client");
		startMG.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/StartMG.gif")));
		startMG.setData(new Integer(MenuListener.MG_ITEM_START));
		startMG.addSelectionListener(menuListener);
	   
		stopMG = new MenuItem(menu,SWT.PUSH);
		stopMG.setText("Stop Client");
		stopMG.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/StopMG.gif")));
		stopMG.setData(new Integer(MenuListener.MG_ITEM_STOP));
		stopMG.addSelectionListener(menuListener);
		stopMG.setEnabled(false);
		
		new MenuItem(menu,SWT.SEPARATOR);
		mgSettings = new MenuItem(menu,SWT.NONE);
		mgSettings.setText("Properties");
		mgSettings.setData(new Integer(MenuListener.MG_ITEM_SETTINGS));
		mgSettings.addSelectionListener(menuListener);	
		mgSettings.setSelection(true);
		mgSettings.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/SetupMG.gif")));
		
		new MenuItem(menu,SWT.SEPARATOR);
		servSettings = new MenuItem(menu,SWT.NONE);
		servSettings.setText("ServerDetails");
		servSettings.setData(new Integer(MenuListener.SERVER_SETTINGS));
		servSettings.addSelectionListener(menuListener);	
		servSettings.setSelection(true);
		servSettings.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/SetupMG.gif")));		
	}
	
	/**
	 * @param main_menu
	 */
	private void createToolsMenu(Menu main_menu,MenuListener menuListener) {
	
		Menu menu = new Menu(main_menu);
		
		toolsMenu = new MenuItem(main_menu,SWT.CASCADE);
		toolsMenu.setText("Tools");
		toolsMenu.setMenu(menu);
		
		staActivity = new MenuItem(menu,SWT.NONE);
		staActivity.setText("Show Client Activity");
		staActivity.setData(new Integer(MenuListener.TOOLS_ITEM_STA_ACTIVITY));
		staActivity.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/ClientActivity.jpg")));
		staActivity.addSelectionListener(menuListener);	
		
		iperfSrcDestTool	= new MenuItem(menu,SWT.NONE);
		iperfSrcDestTool.setText("Internode Performance Test");
		iperfSrcDestTool.setData(new Integer(MenuListener.TOOLS_ITEM_IPERF_SRCDEST_TOOL));
		iperfSrcDestTool.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/PerformanceTest.gif")));
		iperfSrcDestTool.addSelectionListener(menuListener);	
		iperfSrcDestTool.setEnabled(false);
		
		meshCommand  = new MenuItem(menu,SWT.NONE);
		meshCommand.setText("Mesh Command");
		meshCommand.setData(new Integer(MenuListener.TOOLS_ITEM_MESHCOMMAND));
		meshCommand.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/MeshCommand.jpg")));
		meshCommand.addSelectionListener(menuListener);	

		wizardsItem = new MenuItem(menu,SWT.CASCADE);
		wizardsItem.setText("Wizards");
		wizardsItem.setData(new Integer(MenuListener.TOOLS_ITEM_WIZARDS));
		wizardsItem.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/Wizards.gif")));
		wizardsItem.addSelectionListener(menuListener);	

		Menu submenu = new Menu(menu);
		wizardsItem.setMenu(submenu);

		fipsWizardItem	= new MenuItem(submenu,SWT.NONE);
		fipsWizardItem.setText("FIPS 140-2 Wizard");
		fipsWizardItem.setData(new Integer(MenuListener.TOOLS_ITEM_WIZARDS_FIPS));
		fipsWizardItem.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/FipsWizard.gif")));
		fipsWizardItem.addSelectionListener(menuListener);	

		gpsWizardItem	= new MenuItem(submenu,SWT.NONE);
		gpsWizardItem.setText("GPS Wizard");
		gpsWizardItem.setData(new Integer(MenuListener.TOOLS_ITEM_WIZARDS_GPS));
		gpsWizardItem.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/GpsWizard.png")));
		gpsWizardItem.addSelectionListener(menuListener);	

		scriptConsoleItem	= new MenuItem(menu,SWT.NONE);
		scriptConsoleItem.setText("Script Console");
		scriptConsoleItem.setData(new Integer(MenuListener.TOOLS_ITEM_SCRIPT_CONSOLE));
		scriptConsoleItem.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/ScriptConsole.png")));
		scriptConsoleItem.addSelectionListener(menuListener);	
		
	}

	/**
	 * @param main_menu
	 */
	private void createRunMenu(Menu main_menu,MenuListener menuListener) {
		
		Menu menu = new Menu(main_menu);
		
		MenuItem editMenu = new MenuItem(main_menu,SWT.CASCADE);
		editMenu.setText("Run");
		editMenu.setMenu(menu);
		
		startViewer = new MenuItem(menu,SWT.PUSH);
		startViewer.setText("Start Viewer");
		startViewer.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/StartViewer.gif")));
		startViewer.setData(new Integer(MenuListener.RUN_ITEM_START_VIEWER));
		startViewer.addSelectionListener(menuListener);
	   
		stopViewer = new MenuItem(menu,SWT.PUSH);
		stopViewer.setText("Stop Viewer");
		stopViewer.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/StopViewer.gif")));
		stopViewer.setData(new Integer(MenuListener.RUN_ITEM_STOP_VIEWER));
		stopViewer.addSelectionListener(menuListener);
		stopViewer.setEnabled(false);
		
		new MenuItem(menu,SWT.SEPARATOR);
		
		groupSelectionItem = new MenuItem(menu,SWT.CHECK);
		groupSelectionItem.setText("Group Selection");
		groupSelectionItem.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/GroupSelection.gif")));
		groupSelectionItem.setData(new Integer(MenuListener.RUN_ITEM_GROUP_SELECTION));
		groupSelectionItem.addSelectionListener(menuListener);
		
		new MenuItem(menu,SWT.SEPARATOR);
		
		runMacro = new MenuItem(menu,SWT.PUSH);
		runMacro.setText("Run Macro");
		runMacro.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/RunMacros.gif")));
		runMacro.setData(new Integer(MenuListener.RUN_ITEM_RUN_MACROS));
		runMacro.addSelectionListener(menuListener);
		
	}


	/**
	 * @param main_menu
	 * @param tfw
	 */
	
	private void createHelpMenu(Menu main_menu,MenuListener menuListener) {
		
		Menu menu = new Menu(main_menu);
		MenuItem helpMenu = new MenuItem(main_menu,SWT.CASCADE);
		helpMenu.setText("Help");
		helpMenu.setMenu(menu);
		
		MenuItem itm = new MenuItem(menu,SWT.PUSH);
		itm.setText("Contents");
		itm.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/Help.jpg")));
		itm.addSelectionListener(menuListener);		
		itm.setData(new Integer(MenuListener.HELP_ITEM_CONTENT));
		
		itm = new MenuItem(menu,SWT.PUSH);
		itm.setText("About");
		if(Branding.isMeshDyanmics() == false){
			
			portImage	= Branding.getIconImage();	
			itm.setImage(portImage);
			
	   }else{
			itm.setImage(Branding.getIconImage());
	    }
		itm.addSelectionListener(menuListener);		
		itm.setData(new Integer(MenuListener.HELP_ITEM_ABOUT));
	}

	
	/**
	 * @param main_menu
	 */
	
	private void createViewMenu(Menu main_menu,MenuListener menuListener) {
		
		Menu menu = new Menu(main_menu);
		
		MenuItem actionsMenu = new MenuItem(main_menu,SWT.CASCADE);
		actionsMenu.setText("View");
		actionsMenu.setMenu(menu);
		
		MenuItem viewsMenu = new MenuItem(menu,SWT.CASCADE);
		viewsMenu.setText("Views");
		Menu submenu = new Menu(menu);
		viewsMenu.setMenu(submenu);
		viewsMenu.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/Views.gif")));
		

		viewDetailItem = new MenuItem(submenu,SWT.CHECK);
		viewDetailItem.setText("Show Topology");
		viewDetailItem.addSelectionListener(menuListener);
		viewDetailItem.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/TopologyView.gif")));
		viewDetailItem.setData(new Integer(MenuListener.VIEW_ITEM_SHOW_TOPOLOGY_VIEW));
		viewDetailItem.setSelection(true);
			
		viewOnlineMapItem = new MenuItem(submenu,SWT.CHECK);
		viewOnlineMapItem.setText("Show Online Map");
		viewOnlineMapItem.addSelectionListener(menuListener);
		viewOnlineMapItem.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/OnlineView.gif")));
		viewOnlineMapItem.setData(new Integer(MenuListener.VIEW_ITEM_SHOW_ONLINE_MAP_VIEW));
		viewOnlineMapItem.setSelection(false);

		viewOfflineMapItem = new MenuItem(submenu,SWT.CHECK);
		viewOfflineMapItem.setText("Show Offline Map");
		viewOfflineMapItem.addSelectionListener(menuListener);
		viewOfflineMapItem.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/OfflineView.gif")));
		viewOfflineMapItem.setData(new Integer(MenuListener.VIEW_ITEM_SHOW_OFFLINE_MAP_VIEW));
		viewOfflineMapItem.setSelection(false);
		
		new MenuItem(menu,SWT.SEPARATOR);
		
		propItem = new MenuItem(menu,SWT.CHECK);
		propItem.setText("Show Properties");
		propItem.addSelectionListener(menuListener);
		propItem.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/PropertiesTab.gif")));
		propItem.setData(new Integer(MenuListener.VIEW_ITEM_SHOW_PROPERTY_VIEW));
		propItem.setSelection(true);
		
		statusItem = new MenuItem(menu,SWT.CHECK);
		statusItem.setText("Show Status");
		statusItem.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/StatusTab.gif")));
		statusItem.setData(new Integer(MenuListener.VIEW_ITEM_SHOW_MESSAGES_VIEW));
		statusItem.addSelectionListener(menuListener);	
		statusItem.setSelection(true);
		
		new MenuItem(menu,SWT.SEPARATOR);
		
		viewItem = new MenuItem(menu,SWT.PUSH);
		viewItem.setText("View Settings");
		viewItem.addSelectionListener(menuListener);
		viewItem.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/ViewSettings.jpg")));
		viewItem.setData(new Integer(MenuListener.EDIT_ITEM_EDIT_VIEW_SETTINGS));
		viewItem.setSelection(false);
	}
	
	
	
	/**
	 * 
	 */
	
	private void createFileMenu(Menu main_menu,MenuListener menuListener) {
		
		Menu menu = new Menu(main_menu);
		MenuItem fileMenu = new MenuItem(main_menu, SWT.CASCADE);
		fileMenu.setText("File");
		fileMenu.setMenu(menu);
		
		MenuItem menuItem = new MenuItem(menu,SWT.PUSH);
		menuItem.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/NewNetwork.gif")));
		menuItem.setText("New Network");
		menuItem.setData(new Integer(MenuListener.FILE_ITEM_NEW_NETWORK));
		menuItem.setAccelerator(SWT.CONTROL | 'N');
		menuItem.addSelectionListener(menuListener);
	
		
		menuItem = new MenuItem(menu,SWT.PUSH);
		menuItem.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/OpenNetwork.gif")));
		menuItem.setText("Open Network");
		menuItem.setData(new Integer(MenuListener.FILE_ITEM_OPEN_NETWORK));
		menuItem.setAccelerator(SWT.CONTROL | 'O');
		menuItem.addSelectionListener(menuListener);

		menuItem = new MenuItem(menu,SWT.PUSH);
		menuItem.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/SaveNetwork.gif")));
		menuItem.setText("Save Network");
		menuItem.setData(new Integer(MenuListener.FILE_ITEM_SAVE_NETWORK));
		menuItem.setAccelerator(SWT.CONTROL | 'S');
		menuItem.addSelectionListener(menuListener);
		
		new MenuItem(menu,SWT.SEPARATOR);
		
		menuItem = new MenuItem(menu,SWT.PUSH);
		menuItem.setText("Exit        Alt+F4");
		menuItem.setAccelerator(SWT.ALT|SWT.F4);		
		menuItem.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/Exit.gif")));
		menuItem.setData(new Integer(MenuListener.FILE_ITEM_EXIT));
		menuItem.addSelectionListener(menuListener);
		
	}
	
    /**
     * @param nwkName
     */
    private MeshNetworkUI addNetworkTab(String networkName) {
    	
    	MeshNetwork meshNetwork = meshViewer.getMeshNetworkByName(networkName);
    	if(meshNetwork == null) {
    		return null;
    	}
    	meshNetwork.addNetworkListener(alertManager);

		if(showUI == false)
			return null;
    	
    	String networkId 	= meshNetwork.getNwName(); 
    	selectedNetworkUI	= new MeshNetworkUI(meshNetwork, tabFolder, SWT.DRAG,this);
    	
    	CTabItem mnTabItem 	= new CTabItem(tabFolder,SWT.LEFT);
    	
    	selectedNetworkUI.setBounds(tabFolder.getBounds());
    	selectedNetworkUI.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_LIST_BACKGROUND));

		groupSelOn.setSelection(false);
		groupSelectionItem.setSelection(false);
    	
		mnTabItem.setImage(new Image(null, MeshViewerUI.class.getResourceAsStream("icons/Network.gif")));
		mnTabItem.setText(networkId);
		mnTabItem.setControl(selectedNetworkUI);
		meshNetworkUIs.put(selectedNetworkUI.getNwName(),selectedNetworkUI);
		tabFolder.setSelection(mnTabItem);
		tabFolderSelectionHandler(mnTabItem);
		
		notifyHealthAlert(selectedNetworkUI.getCurrentHealthStatus(), networkId);
		selectedNetworkUI.notifyStatusBar(selectedNetworkUI.getStatusText());
		statusDisplayUI.networkCreated(networkId);
		
		selectedNetworkUI.addNetworkStatusListener(statusDisplayUI);
		extensionHandler.notifyNetworkOpened(networkId);
		
		return selectedNetworkUI;
    }
    
    /**
     * 
     */
    public void viewerStarted() {
    	viewerRunning = true;
    	
    	if(showUI == false)
    		return;
    	
    	startViewer.setEnabled(false);
    	stopViewer.setEnabled(true);
    	startToolItem.setEnabled(false);
    	stopToolItem.setEnabled(true);
    	runMacro.setEnabled(true);
    	groupSelOn.setEnabled(true);
    	groupSelectionItem.setEnabled(true);
    	extensionHandler.notifyViewerStarted();
    	startMG.setEnabled(true);
    	mgStartToolItem.setEnabled(true);
    }


    /**
     * 
     */
    public void viewerStopped() {
    	viewerRunning = false;

    	if(showUI == false)
    		return;
    	
    	startViewer.setEnabled(true);
    	stopViewer.setEnabled(false);
    	startToolItem.setEnabled(true);
    	stopToolItem.setEnabled(false);
    	startMG.setEnabled(true);
    	stopMG.setEnabled(false);
    	mgStartToolItem.setEnabled(true);
    	mgStopToolItem.setEnabled(false);
    	runMacro.setEnabled(false);
    	groupSelOn.setEnabled(false);
    	groupSelectionItem.setEnabled(false);
    	setGroupSelection(false);
    	extensionHandler.notifyViewerStopped();
    	
    	startMG.setEnabled(false);
    }

    protected void setGroupSelection(boolean groupSelectionEnabled){
    	groupSelectionItem.setSelection(groupSelectionEnabled);
    	groupSelOn.setSelection(groupSelectionEnabled);
    	this.groupSelectionEnabled	= groupSelectionEnabled;
    	selectedNetworkUI.setGroupSelectionEnabled(groupSelectionEnabled);
    	statusDisplayUI.groupSelectionChanged(groupSelectionEnabled);
    	if(groupSelectionEnabled == false)
    		iperfSrcDestTool.setEnabled(false);
    }
    
    /**
     * 
     */
    protected void showAboutDialog() {
		AboutDialog abtDlg = new AboutDialog();
		abtDlg.show();
    }

    /**
     * @return MeshNetworkUI
     */
    public MeshNetworkUI getSelectedMeshNetworkUI() {
    	if(showUI == false)
    		return null;
    	
    	if(tabFolder.isDisposed())
    		return null;
    	
		return selectedNetworkUI;
    }

    public void apSelectionChanged(AccessPoint newAP) {

    	if(showUI == false)
    		return;
    	
        if(currentSelectedAp != newAP) {
        	if(currentSelectedAp != null) {
        		currentSelectedAp.removePropertyListener(apPropertyUI);
        	}
        	currentSelectedAp = newAP;
        	apPropertyUI.apSelectionChanged(newAP);
        }

        if(newAP == null) {
        	return;
        }
        
        newAP.addPropertyListener(apPropertyUI);
        apPropertyUI.apPropertyChanged(IAPPropListener.MASK_ALL, IAPPropListener.MASK_ALL, newAP);
        statusDisplayUI.selectAccessPoint(newAP.getDSMacAddress());
        extensionHandler.notifyNodeSelected(newAP.getNetworkId(), newAP.getDSMacAddress());
    }

 	/**
     * @return Returns the showGridLines.
     */
    protected boolean isPropVisible(int prop) {
        
    	if(showUI == false)
    		return false;
    	
        switch(prop) {
        case PROPERTY_WINDOW :
            return propComp.isVisible();
        case LOG_WINDOW :
            return logCanvas.isVisible();
        case GRID_LINES :
            return showGridLines;
        case NEIGHBOUR_LINES :
            return showNeighbourLines;
        case NEW_LOG_WINDOW :
            return logTabsWindow.isVisible();
        }
        return false;
    }

    /**
     * 
     */
    public boolean isMeshViewerStarted() {
        return viewerRunning;
    }

    protected void setViewSelection(int viewType){
		
    	if(showUI == false)
    		return;
    	
		switch(viewType) {
			case MeshNetworkUI.VIEW_DETAIL:
				viewDetailItem.setSelection(true);
				viewOnlineMapItem.setSelection(false);
				viewOfflineMapItem.setSelection(false);
				break;
			case MeshNetworkUI.VIEW_ONLINE_MAP:
				viewDetailItem.setSelection(false);
				viewOnlineMapItem.setSelection(true);
				viewOfflineMapItem.setSelection(false);
				break;
			case MeshNetworkUI.VIEW_OFFLINE_MAP:
				viewDetailItem.setSelection(false);
				viewOnlineMapItem.setSelection(false);
				viewOfflineMapItem.setSelection(true);
				break;
		}
		
	}
	
	/**
	 * @return Returns the meshNetworkUIs.
	 */
	public final Enumeration<MeshNetworkUI> getMeshNetworkUIs() {
    	if(showUI == false)
    		return null;
		
		return meshNetworkUIs.elements();
	}
	
	/**
	 * @param i
	 * @param meshId
	 */
    public void notifyHealthAlert(final int healthStatus, final String meshId) {
    }

    
    /*
	 * Following code has to be enabled if health monitor is 
	 * required to be shown in UI
	 */
/*
	public void notifyHealthAlert(final int healthStatus, final String meshId) {
				 
		//match meshid
		//mcheck curent meshid
		//if paintAllowed == false the return
		
		display.asyncExec(new Runnable() {
			public void run(){
				
				if(tabFolder.isDisposed() == true)
					return;
				
				boolean paintAllowed 	= true;
				
				for(int i=0;i<tabFolder.getItemCount();i++){
					CTabItem 			tabItem			=	tabFolder.getItem(i);
					String   			tabMeshId 		=  tabItem.getText();
								
				    if(tabMeshId.equalsIgnoreCase(meshId)){
				    	
				    	if(i != tabFolder.getSelectionIndex()) {
				    		paintAllowed = false;
				    	}
				    	break;
				    }
				}
		
				if(paintAllowed == false)
					return;
				
				paintNetworkTab(healthStatus);
				
			}
		});
		
	}
	
	private void paintNetworkTab(int healthStatus){
		
		if(tabFolder.isDisposed() == true) {
			return;
		}
		
		if(healthStatus == IHealthMonitor.HEALTH_STATUS_DISABLED){
			
			tabFolder.setSelectionBackground(new Color[]{display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND), 
	                display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND),
	                display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT), 
	                display.getSystemColor(SWT.COLOR_TITLE_BACKGROUND_GRADIENT)},
	       new int[] {40, 70, 100}, true);
			healthMonitorItem.setEnabled(false);
    	} else if(healthStatus == IHealthMonitor.HEALTH_STATUS_GREEN){
			
    		tabFolder.setSelectionBackground(new Color[]{display.getSystemColor(SWT.COLOR_GREEN), 
                display.getSystemColor(SWT.COLOR_GREEN),
                display.getSystemColor(SWT.COLOR_DARK_GREEN), 
                display.getSystemColor(SWT.COLOR_GREEN)},
				new int[] {40, 70, 100}, true);
    		healthMonitorItem.setEnabled(true);
    	} else if(healthStatus == IHealthMonitor.HEALTH_STATUS_YELLOW){
		
			tabFolder.setSelectionBackground(new Color[]{display.getSystemColor(SWT.COLOR_YELLOW), 
                display.getSystemColor(SWT.COLOR_YELLOW),
                display.getSystemColor(SWT.COLOR_DARK_YELLOW), 
                display.getSystemColor(SWT.COLOR_YELLOW)},
				new int[] {40, 70, 100}, true);
			healthMonitorItem.setEnabled(true);
		} else if(healthStatus == IHealthMonitor.HEALTH_STATUS_RED){
			
			tabFolder.setSelectionBackground(new Color[]{display.getSystemColor(SWT.COLOR_RED), 
                display.getSystemColor(SWT.COLOR_RED),
                display.getSystemColor(SWT.COLOR_DARK_RED), 
                display.getSystemColor(SWT.COLOR_RED)},
				new int[] {40, 70, 100}, true);
			healthMonitorItem.setEnabled(true);
		}	
		
	}
*/
    
    protected void showDLSaturationInfo(MeshNetwork meshNetwork, IConfiguration config) {
		rfSpaceInfoDlg = new RFSpaceInfoDlg(meshNetwork,config,meshViewer.getPacketSender());
		rfSpaceInfoDlg.show();
		rfSpaceInfoDlg = null;
	}
	
	public void updateDLSaturationInfo(final AccessPoint ap, final String ifName) {
		
    	if(showUI == false)
    		return;
		
		/*
		 * This will always be interthread call 
		 * mostly called when saturationinfo packet is received 
		 */
		if(rfSpaceInfoDlg == null) {
			return;
		}
		if(rfSpaceInfoDlg.isDisposed() == true) {
			return;
		}
		
		display.syncExec (new Runnable () {
	        public void run () {	        	
	        	rfSpaceInfoDlg.refreshInformation(ifName);
	        }
	     });
	}


	protected PerspectiveSettings getPerspectiveSettings() {
		return perspectiveSettings;
	}


	public void accesspointAdded(AccessPoint accessPoint) {
		
    	if(showUI == false)
    		return;
		
		if(accessPoint == null) {
			return;
		}
		
		accessPoint.addMessageListener(statusDisplayUI);
		accessPoint.addPropertyListener(statusDisplayUI);
		statusDisplayUI.apPropertyChanged(IAPPropListener.MASK_HEARTBEAT, IAPPropListener.MASK_ALL, accessPoint);
		extensionHandler.notifyNodeAdded(accessPoint.getNetworkId(), accessPoint.getDSMacAddress());

		MeshNetworkUI networkUI = meshNetworkUIs.get(accessPoint.getNetworkId());
		if(networkUI == null)
			return;
		
		IJsonAp jsonAp = networkUI.getJsonAp(accessPoint.getDSMacAddress());
		if(jsonAp == null)
			return;
	
		if(jsonApList.contains(jsonAp) == false)
			jsonApList.add(jsonAp);
	}
	
	public void accesspointRemoved(AccessPoint accessPoint) {

    	if(showUI == false)
    		return;
		
		if(accessPoint == null) {
			return;
		}
		
		statusDisplayUI.apPropertyChanged(IAPPropListener.MASK_AP_STATE, IAPPropListener.AP_STATE_DISPOSED, accessPoint);		
		accessPoint.removeMessageListener(statusDisplayUI);
		accessPoint.removePropertyListener(statusDisplayUI);
		extensionHandler.notifyNodeDeleted(accessPoint.getNetworkId(), accessPoint.getDSMacAddress());
		
		MeshNetworkUI networkUI = meshNetworkUIs.get(accessPoint.getNetworkId());
		if(networkUI == null)
			return;
		
		IJsonAp jsonAp = networkUI.getJsonAp(accessPoint.getDSMacAddress());
		if(jsonAp == null)
			return;
	
		jsonApList.remove(jsonAp);
		
	}


	public void showMessage(IMessageSource msgSource, String messageHeader, String message, String progress) {
    	if(showUI == false)
    		return;
		
		statusDisplayUI.showMessage(msgSource, messageHeader, message, progress);
	}


	protected boolean isMGRunning() {
		return mgRunning;
	}


	protected boolean isGroupSelectionEnabled() {
		return groupSelectionEnabled;
	}

	/**
	 * @param strMac
	 */
	public void apDoubleClickedFromStatusDisplay(String strMac) {
    	if(showUI == false)
    		return;

		selectedNetworkUI.apSelectionChanged(null, strMac);
	}

	/**
	 * @param strMac
	 */
	public void apGroupSelectedFromStatusDisplay(String strMac, boolean selected) {
    	if(showUI == false)
    		return;

    	selectedNetworkUI.apGroupSelectionChanged(null, strMac, selected);
	}
	
	/**
	 * @param shell2
	 * @param mac
	 */
	public void apRightClicked(Table tblHeartBeat, MacAddress mac) {
	}

	/**
	 * 
	 */
	public void close() {
		
		extensionHandler.notifyViewerStopped();
		meshViewer.setMeshViewerListener(null);
		meshViewer.removeSipEventListener(statusDisplayUI);
		
		Enumeration<MeshNetworkUI> nwEnum = meshNetworkUIs.elements();
		while(nwEnum.hasMoreElements()) {
			MeshNetworkUI networkUI = (MeshNetworkUI) nwEnum.nextElement();
			saveMeshNetwork(networkUI.getNwName());
		}
		
		saveUIProperties();
	
		if(serviceMode == false) {
			meshViewer.exitMeshViewer();
			NMS.unInitializeInstance();
			meshViewer 	= null;
			nmsInstance = null;
		}
		uninitUI();
		
		showUI = false;
	}

	/**
	 * @param networkName
	 * @param networkKey
	 * @param newtworkType
	 * @return
	 */
	protected boolean createMeshNetwork(String networkName, String networkKey, byte networkType) {
		
		if(meshNetworkUIs.contains(networkName) == true) {
			return true;
		}

		int nmsNwType = (networkType == MeshNetwork.NETWORK_FIPS_ENABLED) ?
							NMS.NETWORK_TYPE_FIPS_140_2 : NMS.NETWORK_TYPE_REGULAR;
		
		if(nmsInstance.openNetwork(networkName, networkKey, nmsNwType) == null) {
			return false;
		}
		addNetworkTab(networkName);
		return true;
	}

	/**
	 * @param networkFilename
	 * @return
	 */
	protected boolean openMeshNetwork(String networkName) {
		
		MeshNetworkUI networkUI = (MeshNetworkUI) meshNetworkUIs.get(networkName);
		if(networkUI != null) {
			return true;
		}
		
		String networkFilename = MFile.getNetworkPath() + "\\" + networkName + ".net";
		FileInputStream in;
		try {
			
			in = new FileInputStream(networkFilename);
			if(loadNetworkCreationInfo(in, null) == false) {
				in.close();
				return false;
			}
			
			networkUI = addNetworkTab(networkName);
			if(networkUI != null) {
				networkUI.load(in);
				setViewSelection(selectedNetworkUI.getCurrentViewType());
			}
			
			in.close();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}

	/**
	 * @param networkFilename
	 * @return
	 */
	protected boolean openFipsNetwork(String networkName, String networkKey) {
		
		MeshNetworkUI networkUI = (MeshNetworkUI) meshNetworkUIs.get(networkName);
		if(networkUI != null) {
			return true;
		}
		
		String networkFilename = MFile.getNetworkPath() + "\\" + networkName + ".net";
		FileInputStream in;
		try {
			
			in = new FileInputStream(networkFilename);
			if(loadNetworkCreationInfo(in, networkKey) == false) {
				in.close();
				return false;
			}
			
			networkUI = addNetworkTab(networkName);
			if(networkUI != null) {
				networkUI.load(in);
			}
			
			in.close();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		
		return true;
	}
	
	/**
	 * @param networkName
	 * @return
	 */
	private boolean loadNetworkCreationInfo(FileInputStream in, String fipsNetworkKey) {
		
    	byte[] fourBytes 	= new byte[4];
    	byte[]	oneByte		= new byte[1];
    	
		try{
    		in.read(fourBytes);
    		
    		byte[] mac = new byte[6];
    		mac[0] = fourBytes[0];
    		mac[1] = fourBytes[3];
    		mac[2] = fourBytes[2];
    		mac[3] = fourBytes[1];
    		mac[4] = fourBytes[3];
    		mac[5] = fourBytes[1];
    		
    		byte[] buffer = new byte[Bytes.bytesToInt(fourBytes)];
    		in.read(buffer);
    		String meshNetworkId = new String(buffer);
    		
    		in.read(fourBytes);
    		buffer = new byte[Bytes.bytesToInt(fourBytes)];
    		in.read(buffer);
    		
    		in.read(oneByte);
    		byte networkType	 = oneByte[0];

    		String networkEncKey 	= null;
    		if(networkType == MeshNetwork.NETWORK_REGULAR) {
	    		AES aes 		= new AES();
	    		buffer 			= aes.aesdecodeKey(buffer,buffer.length,mac);
	    		networkEncKey 	= new String(buffer);
	    		nmsInstance.openNetwork(meshNetworkId, networkEncKey, NMS.NETWORK_TYPE_REGULAR);
    		} else if(networkType == MeshNetwork.NETWORK_FIPS_ENABLED) {
    	    	if(fipsNetworkKey == null) 
    	    		return false;
    	    	buffer			= new byte[HMAC_SHA1.DIGEST_LENGTH];
    			networkEncKey 	= new String(fipsNetworkKey);
    			nmsInstance.openNetwork(meshNetworkId, networkEncKey, NMS.NETWORK_TYPE_FIPS_140_2);
    			//fips enabled network key will be entered by user 
    			//hence we donot process the loaded byte array 
    		}
    		
    		
    		//meshViewer.createMeshNetwork(meshNetworkId,networkEncKey,networkType);
    		
    	}catch(Exception e){
    		System.out.println(e.getMessage());
    		return false;
    	}
    	
    	return true;
	}

	protected boolean saveMeshNetwork(String networkName) {
		
		MeshNetworkUI networkUI = (MeshNetworkUI) meshNetworkUIs.get(networkName);
		if(networkUI == null) {
			return false;
		}
		
		String networkFilename = MFile.getNetworkPath() + "\\" + networkName + ".net";
		FileOutputStream out;
		
		try {
			out = new FileOutputStream(networkFilename);
			saveNetworkCreationInfo(networkUI,out);
			networkUI.save(out);
			out.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return true;
	}

	private void saveNetworkCreationInfo(MeshNetworkUI networkUI, FileOutputStream out) {
		
    	try{
    		
    		byte[] fourBytes 	= new byte[4];
    		
    		String networkId = networkUI.getNwName();
    		
    		Bytes.intToBytes(networkId.length(),fourBytes);
    		out.write(fourBytes);
    		byte[] buffer =  networkId.getBytes();
    		out.write(buffer);
    	    
    		byte[] mac = new byte[6];
    		mac[0] = fourBytes[0];
    		mac[1] = fourBytes[3];
    		mac[2] = fourBytes[2];
    		mac[3] = fourBytes[1];
    		mac[4] = fourBytes[3];
    		mac[5] = fourBytes[1];
    		String key = networkUI.getNetworkKey();
    		
    		if(networkUI.getNetworkType() == MeshNetwork.NETWORK_REGULAR) {
	    		AES aes = new AES();
	    		buffer = aes.aesencodeKey(key.getBytes(), key.length(), mac);
    		} else if(networkUI.getNetworkType() == MeshNetwork.NETWORK_FIPS_ENABLED) {
    			buffer = new byte[HMAC_SHA1.DIGEST_LENGTH];
    			HMAC_SHA1.getInstance().generateHMAC(key.getBytes(), mac, buffer);
    		}
    		
    		Bytes.intToBytes(buffer.length,fourBytes);
    		out.write(fourBytes);	// writes network key to file
    		    		
    		out.write(buffer);
    		
    		out.write(networkUI.getNetworkType()); // writes network type to file
    		
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	private boolean closeMeshNetwork(String networkName) {
		if(networkName.equalsIgnoreCase(Mesh.DEFAULT_MESH_ID) == true){
			return false;
		}

	    MeshNetworkUI meshNetworkUI = (MeshNetworkUI)meshNetworkUIs.remove(networkName);
    	statusDisplayUI.networkDeleted(networkName);
	    extensionHandler.notifyNetworkClosed(networkName);
	    
	    meshNetworkUI.removeNetworkStatusListener(statusDisplayUI);
	    
	    MeshNetwork meshNetwork = meshNetworkUI.getMeshNetwork();
	    meshNetwork.removeNetworkListener(alertManager);
	    
	    nmsInstance.closeNetwork(nmsInstance.getNetworkByName(networkName));
	    
	    return true;
		
	}
	
	private boolean deleteMeshNetwork(String networkName) {

		if(networkName.equalsIgnoreCase(Mesh.DEFAULT_MESH_ID) == true){
			return false;
		}
		
		closeMeshNetwork(networkName);
		
		String networkFileName = MFile.getNetworkPath() + "\\" + networkName + ".net";
		File netfile = new File(networkFileName);
		if(netfile.exists() == true) {
			if(netfile.delete() == false) {
				return true;
			}
		}
		
		return true;
	}

	public void updateStatusMessage(String message) {
		if(showUI == false)
			return;
		
		statusBar.setText(message);
		//trayToolTipNotifyListener(message);
		if(groupSelectionEnabled == false)
			return;
		
		int selectionCount = selectedNetworkUI.getGroupSelectedApCount();
		iperfSrcDestTool.setEnabled((selectionCount == 2) ? true : false);
	}

	/**
	 * @param sectionId
	 * @param sectionText
	 * @return
	 * 0 on success
	 */
	protected int createExtPropertySection(String sectionId, String sectionText) {
		if(showUI == false)
			return 1;
		
		return apPropertyUI.createExtPropertySection(sectionId, sectionText);
	}

	/**
	 * @param sectionId
	 */
	protected void destroyExtPropertySection(String sectionId) {
		if(showUI == false)
			return;
		
		apPropertyUI.destroyExtPropertySection(sectionId);
	}

	/**
	 * @param sectionId
	 */
	protected void updateExtPropertySection(String sectionId, String nodeId) {
		if(showUI == false)
			return;
		
		apPropertyUI.updateExtPropertySection(sectionId, nodeId);
	}
	
	/**
	 * @param sectionId
	 * @param propertyId
	 * @param propertyText
	 */
	protected boolean addExtProperty(String sectionId, String propertyId, String propertyText) {
		if(showUI == false)
			return false;
		
		return apPropertyUI.addExtProperty(sectionId, propertyId, propertyText);
	}

	/**
	 * @param sectionId
	 * @param propertyId
	 */
	protected void removeExtProperty(String sectionId, String propertyId) {
		if(showUI == false)
			return;
		
		apPropertyUI.removeExtProperty(sectionId, propertyId);		
	}

	/**
	 * @param nodeId
	 * @param text
	 */
	protected String getExtPropertyValue(String sectionId, String nodeId, String propertyText) {

		if(extensionHandler == null) {
			return "Not Available";
		}
		
		return extensionHandler.getPropertyValue(sectionId, nodeId, propertyText);
		
	}

	/**
	 * @return
	 */
	protected StatusDisplayController getStatusDisplayUI() {
		return statusDisplayUI;
	}

    protected Vector<ExtendedMenuInfo> getExtendedMenuTable() {
        return extendedMenuTable;
    }

	/* (non-Javadoc)
	 * @see com.meshdynamics.viewport.http.IJsonMeshInfoProvider#getJsonApList()
	 */
	public Vector<IJsonAp> getJsonApList() {
		return jsonApList;
	}

	/**
	 * @return Returns the accessPointMenu.
	 */
	public AccessPointMenu getAccessPointMenu() {
		if(accessPointMenu == null) {
			accessPointMenu = new AccessPointMenu(this, shell);
		}
		
		if(accessPointMenu.getMenu().isDisposed() == true) {
			accessPointMenu = new AccessPointMenu(this, shell);
		}
		
		return accessPointMenu;
	}

	private void handleJsonDoubleClick(String[] eventInfo) {
		
		String tokens[] = eventInfo[1].split("/");
		if(tokens[0].equalsIgnoreCase(selectedNetworkUI.getNwName()) == false) {
			return;
		}
		selectedNetworkUI.apSelectionChanged(null, tokens[1]);
	}
	
	private void handleJsonRightClick(final String[] eventInfo) {
		
		String tokens[] = eventInfo[1].split("/");
		if(tokens[0].equalsIgnoreCase(selectedNetworkUI.getNwName()) == false) {
			return;
		}
		
		int x	= Integer.parseInt(eventInfo[2]);
		int y 	= Integer.parseInt(eventInfo[3]);
		Point location 		= display.map(selectedNetworkUI, null, x, y);
		
		MacAddress.tempAddress.setBytes(tokens[1]);
		AccessPointMenu menu = getAccessPointMenu();
		if(menu == null)
			return;
		
		menu.refresh(MacAddress.tempAddress, 2);
		menu.getMenu().setLocation(location.x+10, location.y+10);
		menu.getMenu().setVisible(true);

	}

	public void handleJsonMouseClick(final int type, final int button, final String[] eventInfo) {
		
		if(display == null || display.isDisposed() == true)
			return;
		
		display.asyncExec(new Runnable(){

			@Override
			public void run() {
				if(type == JSON_MOUSE_SINGLE_CLICK && button == JSON_MOUSE_BUTTON_RIGHT) {
					handleJsonRightClick(eventInfo);
				} else if(type == JSON_MOUSE_DOUBLE_CLICK && button == JSON_MOUSE_BUTTON_LEFT) {
					handleJsonDoubleClick(eventInfo);
				}
			}
		});
		
	}
	
	protected void setLockThisNetwork(MeshNetworkUI networkUI) {
		
		boolean islocked = networkUI.getIsNetworkLocked();
		
		groupSelectionItem.setEnabled(!islocked);
		runMacro.setEnabled(!islocked);
		groupSelOn.setEnabled(!islocked);
		viewItem.setEnabled(!islocked);
		toolsMenu.setEnabled(!islocked);
	}
	
	protected void showAlert(Alert objAlert) {
		if(statusDisplayUI != null)
			statusDisplayUI.showAlert(objAlert);
		else {
			try {
				FileOutputStream out 	= new FileOutputStream(MFile.getConfigPath() + "\\Alerts.txt", true);
				PrintWriter		 p		= new PrintWriter(out);
				
				String line = "";
				switch (objAlert.level) {
				case Alert.ALERT_LEVEL_LOW:
					line += "LOW#";
					break;
				case Alert.ALERT_LEVEL_MEDIUM:
					line += "MEDIUM#";
					break;
				case Alert.ALERT_LEVEL_HIGH:
					line += "HIGH#";
					break;
				}
				line += (DateTime.getDateTime() + "#");
				line += (objAlert.message + "#");
				line += objAlert.description;
				
				p.println(line);
				p.close();
				out.close();
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
		}
	}

	void clientActivityChanged(String name, boolean enable) {
		statusDisplayUI.clientActivityChanged(name, enable);
	}

	protected NMSUI getNMSUI() {
		return extensionHandler;
	}

	protected StatusTab createStatusTab(String tabName, StatusTabHeader columns) {
		return statusDisplayUI.createStatusTab(tabName, columns);
	}

	protected void destroyStatusTab(StatusTab statusTab) {
		statusDisplayUI.destroySTatusTab(statusTab);
	}

	protected StatusTabRow addStatusTabRow(StatusTab statusTab) {
		return statusDisplayUI.addStatusTabRow(statusTab);
	}

	protected void removeStatusTabRow(StatusTabRow statusTabRow) {
		statusDisplayUI.removeStatusTabRow(statusTabRow);
	}

	protected void updateStatusTabRow(StatusTabRow statusTabRow) {
		statusDisplayUI.updateStatusTabRow(statusTabRow);
	}

	protected NodeText createNodeText(String nodeId, String nodeTextId) {
		Enumeration<MeshNetworkUI> enumNetworkUI = meshNetworkUIs.elements();
		while(enumNetworkUI.hasMoreElements()) {
			MeshNetworkUI networkUI = enumNetworkUI.nextElement();
			NodeText nodeText = networkUI.createNodeText(nodeId, nodeTextId);
			if(nodeText != null)
				return nodeText;
		}
		return null;
	}

	protected void destroyNodeText(NodeText nodeText) {
		Enumeration<MeshNetworkUI> enumNetworkUI = meshNetworkUIs.elements();
		while(enumNetworkUI.hasMoreElements()) {
			enumNetworkUI.nextElement().destroyNodeText(nodeText);
		}
	}

	@Override
	public void meshGatewayStarted() {

		mgRunning	= true;
		
		if(showUI == false)
			return;
		
		display.asyncExec(new Runnable(){
			@Override
			public void run() {
		    	startMG.setEnabled(false);
		    	stopMG.setEnabled(true);
		    	mgStartToolItem.setEnabled(false);
		    	mgStopToolItem.setEnabled(true);
			}
		});
		
	}

	@Override
	public void meshGatewayStopped() {
		mgRunning	= false;
		
		if(showUI == false)
			return;
		
		startMG.setEnabled(true);
		stopMG.setEnabled(false);
		mgStartToolItem.setEnabled(true);
		mgStopToolItem.setEnabled(false);
		if(meshViewer.isRunning() == false) {
			startMG.setEnabled(false);
			mgStartToolItem.setEnabled(false);
		}
	}

	void startMGClient() {
		
		mgRunning	= true;
		int mode = MGClient.MODE_FORWARDER;
		
		if(MGProperties.mode == MGProperties.MODE_REMOTE_MANAGER)
			mode = MGClient.MODE_REMOTE_MANAGER;
		
		meshViewer.startMGClient(mode, MGProperties.server, MGProperties.port, MGProperties.useSSL,
									MGProperties.userName, MGProperties.password, MGProperties.ignoreLocalPackets);	
		
		if(showUI == false)
			return;
		
    	startMG.setEnabled(false);
    	stopMG.setEnabled(true);
    	mgStartToolItem.setEnabled(false);
    	mgStopToolItem.setEnabled(true);
		
	}

	void stopMGClient() {
		
		meshViewer.stopMGClient();
		
		if(showUI == false)
			return;
		
		stopMG.setEnabled(false);
		mgStopToolItem.setEnabled(false);
		
	}

	String exportConfigScript(String nwName, String dsMacAddress) {
		/*
		 * currently we assume ruby to be default export script language
		 */
		Network nw = nmsInstance.getNetworkByName(nwName);
		if(nw == null) {
			System.err.println("<LOW> " + nwName + " Network not found.Script Export Failed.");
			return "";
		}
		Node node = nw.getNodeByMacAddress(dsMacAddress);
		if(node == null) {
			System.err.println("<LOW> " + nwName + ":" + dsMacAddress + " not found.Script Export Failed.");
			return "";
		}
		return node.generateConfigMacro(null);
	}
	
    String exportRFConfigScript(String nwName, String dsMacAddress) {
        
        ScriptExecEnv   scriptEnv;
        String          script;

        Network nw = nmsInstance.getNetworkByName(nwName);
        
        if(nw == null) {
            System.err.println("<LOW> " + nwName + " Network not found.Script Export Failed.");
            return "";
        }
        
        Node node = nw.getNodeByMacAddress(dsMacAddress);
        if(node == null) {
            System.err.println("<LOW> " + nwName + ":" + dsMacAddress + " not found.Script Export Failed.");
            return "";
        }
        
        script = new String( "function generateScript(node) {"
                + "  return rfEditor.generateConfigMacro(node);"
                + "}");                                        
        
        try {
            scriptEnv = new ScriptExecEnv("js");
            scriptEnv.execute(script);
            return (String)scriptEnv.invokeFunction("generateScript", node);
        } catch (Exception e) {
            System.err.println("<LOW> " + nwName + ":" + dsMacAddress + " RF Config script export failed.\n" + e.getMessage());            
        }
        
        return null;
    }	
	
	public boolean[] importConfigScript(String scriptFilePath, String nwName, String[] dsMacAddresses) {
		/*
		 * currently we assume ruby to be default export script language
		 */
		
		boolean[] ret = new boolean[dsMacAddresses.length];
		try {
			
			Network nw 		= nmsInstance.getNetworkByName(nwName);
			if(nw == null)
				return ret;
			
			ScriptExecEnv 	scriptEnv 		= new ScriptExecEnv("jruby");
			FileReader 		scriptReader	= new FileReader(scriptFilePath);
			scriptEnv.execute(scriptReader);
			
			scriptEnv.invokeFunction("nmsConfigMacroInit", nmsInstance, nw);
			for(int i=0;i<dsMacAddresses.length;i++) {
				
				Node 	node	= nw.getNodeByMacAddress(dsMacAddresses[i]);
				if(node == null) {
					ret[i] = false;
					continue;
				}
				
				try {
					scriptEnv.invokeFunction("nmsConfigMacroExecute", nmsInstance, nw, node);
					ret[i] = true;
				} catch(Exception e) {
					e.printStackTrace();
					ret[i] = false;
				}
				
			}
			scriptEnv.invokeFunction("nmsConfigMacroUninit", nmsInstance, nw);
			
		} catch (Exception ee) {
			ee.printStackTrace();
		}
		
		return ret;
	}

	void setNodeElementProperty(String networkName, String nodeId, String property, String value) {
		
		if(showUI == false)
			return;
		
		Enumeration<MeshNetworkUI> enumNetworkUI = meshNetworkUIs.elements();
		while(enumNetworkUI.hasMoreElements()) {
			MeshNetworkUI networkUI = enumNetworkUI.nextElement();
			if(networkUI.getNwName().equalsIgnoreCase(networkName) == false)
				continue;
			
			networkUI.setNodeElementProperty(nodeId, property, value);
			return;
		}

	}

	Enumeration<String> getGroupSelection() {
		return selectedNetworkUI.getGroupSelection();
	}

	public static void main(String[] args) {
		new MeshViewerUI(true);
	}

    public void toggleStatusLogParent() {
        Shell parent;
        if(logTabsInNewWindow) {
            parent = shell;
        } else {
            if(null == logTabsWindow){
                createLogStatusWindow();
            }
            parent = logTabsWindow;
        }
        this.logCanvas.setParent(parent);

        updateStatusBarParentState(!logTabsInNewWindow);

        //Reset the layout
        sashResized(false);
        parent.layout();

        logTabsInNewWindow = !logTabsInNewWindow;
    }

    private void updateStatusBarParentState(boolean bNewWindow) {
        logTabsWindow.setVisible(bNewWindow);
        hSash.setVisible(!bNewWindow);
        statusDisplayUI.toggleStatusControlParent(bNewWindow);

    }

    private void createLogStatusWindow()
    {
        //New window to display the log tabs
        logTabsWindow =   new Shell(display, SWT.MENU|SWT.MAX|SWT.MIN|SWT.RESIZE);
        logTabsWindow.setLayout(new FillLayout());
        logTabsWindow.setImage(new Image(null,MeshViewerUI.class.getResourceAsStream("icons/StatusTab.gif")));
        logTabsWindow.setText("Network Status");

        logTabsWindow.addListener(SWT.Close, new Listener() {
            public void handleEvent(Event e) {
                logTabsWindow.setVisible(false);
                e.doit =false;
                toggleStatusLogParent();
            }
        });
    }
}
