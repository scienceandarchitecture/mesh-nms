/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MFile.java
 * Comments : 
 * Created  : Oct 21, 2005
 * Author   : Mithil Wane
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date        |  Comment                                        | Author |
 * ------------------------------------------------------------------------------
 * |  1  |Apr 13, 2006| Changes for Firmware Update       			    | Bindu  |
 * ------------------------------------------------------------------------------
 * |  0  |Oct 21, 2005| Created                                         | Mithil |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.nmsui.util;

import java.io.File;

public class MFile extends File{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2914997812433715174L;
	
	static File f ;
	
	static {
		f = new File(System.getProperty("com.meshdynamics.meshviewer.path",""));
		}
	
	public static String nonDefaultPath = new String(); 
	private static boolean useNonDefaultPath = false;
	/**
	 * @param arg0
	 */
	public MFile(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	public static String getFileAbsolutePath(){
		return f.getAbsolutePath(); 
	}	
	
	public static String getConfigPath(){
		return f.getAbsolutePath() + "/Config";
	}
	
	public static String getSystemPath(){
		return f.getAbsolutePath() + "/System";
	}
	
	public static String getTemplatePath() {
		return f.getAbsolutePath() + "/Templates";
	}
	
	public static String getLogPath() {
		return f.getAbsolutePath() + "/SystemLogFiles";
	}
	
	public static String getNetworkPath() {
		return f.getAbsolutePath() + "/Networks";
	}
	
	public static String getMapPath() {
		return f.getAbsolutePath() + "\\Map";
	}

	public static String getMapBackupPath() {
		return f.getAbsolutePath() + "/Map/backup";
		}
	
	public static String getNodeFilePath() {
		if(isUseNonDefaultPath())
			return nonDefaultPath;
		else
			return f.getAbsolutePath() + "/NodeFiles";	
	}
	
	public static String getUpdatesPath() {
		return f.getAbsolutePath() + "\\Updates";	
	}
	/*public Object getPluginPath() {
		File f = new File(super.getAbsolutePath() + "/Plugins");
		return f.getAbsolutePath();
	}*/
	
	public static String getWebPath() {
		return f.getAbsolutePath() + "/Web";
	}

	public static String getAlertScriptPath() {
		return f.getAbsolutePath() + "/AlertScripts";
	}
	
	public static String getExtensionsPath() {
		return f.getAbsolutePath() + "/Extensions";
	}
	
	public static String getIconsPath() {
		return f.getAbsolutePath() + "\\Library\\Icons";
	}
	
	public static void main(String args[]){
		
		//MFile f = new MFile("");
		System.out.println(MFile.getConfigPath());
		
	}
	
	/**
	 * @return Returns the useNonDefaultPath.
	 */
	public static boolean isUseNonDefaultPath() {
		return useNonDefaultPath;
	}
	/**
	 * @param useNonDefaultPath The useNonDefaultPath to set.
	 */
	public static void setUseNonDefaultPath(boolean useNonDefaultPath) {
		MFile.useNonDefaultPath = useNonDefaultPath;
	}
}
