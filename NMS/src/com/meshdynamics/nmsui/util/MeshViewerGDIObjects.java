/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshViewerFont.java
 * Comments : Font settings
 * Created  : Mar 26,2007
 * Author   : Abhishek Patankar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Mar 26,2007  | Created                                         |Abhishek|
 * --------------------------------------------------------------------------------*/

package com.meshdynamics.nmsui.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;

public class MeshViewerGDIObjects {
	
	public static FontData 	fontdata;
	public static Font 		boldVerdanaFont8;
	public static Font 		courierFont8;
	public static Font 		boldVerdanaFont12; 
	public static Cursor 	arrCursor;
	
	static {
			
			fontdata = new FontData();
			fontdata.setHeight(8);
			fontdata.setName("Verdana");
			fontdata.setStyle(SWT.BOLD);
			boldVerdanaFont8 = new Font(null,fontdata);
			
			
			fontdata = new FontData();
			fontdata.setHeight(12);
			fontdata.setStyle(SWT.BOLD) ;
			fontdata.setName("Verdana");
			boldVerdanaFont12 = new Font(null,fontdata);
			
			fontdata = new FontData();
			fontdata.setHeight(8);
			fontdata.setName("Courier New");
			courierFont8 = new Font(null,fontdata);
			
			arrCursor = new Cursor(Display.getDefault(), SWT.CURSOR_ARROW);
		 }

	public static void setFontData(int height, int style, String fontName){
		
		fontdata.setHeight(height);
		fontdata.setStyle(SWT.BOLD);
		fontdata.setName("Verdana");
		
	}
 
}

