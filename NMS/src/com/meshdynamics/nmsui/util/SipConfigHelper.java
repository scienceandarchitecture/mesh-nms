package com.meshdynamics.nmsui.util;

import com.meshdynamics.meshviewer.configuration.I80211eCategoryConfiguration;
import com.meshdynamics.meshviewer.configuration.IACLConfiguration;
import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IEffistreamConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;
import com.meshdynamics.meshviewer.configuration.INetworkConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.SipConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.MacAddress;

public class SipConfigHelper implements IConfiguration {
	
	SipConfiguration sipConfig;
	
	public SipConfigHelper() {
		sipConfig = new SipConfiguration();
	}
	
	@Override
	public I80211eCategoryConfiguration get80211eCategoryConfiguration() {
		return null;
	}

	@Override
	public IACLConfiguration getACLConfiguration() {
		return null;
	}

	@Override
	public IEffistreamConfiguration getEffistreamConfiguration() {
		return null;
	}

	@Override
	public IInterfaceConfiguration getInterfaceConfiguration() {
		return null;
	}

	@Override
	public IMeshConfiguration getMeshConfiguration() {
		return null;
	}

	@Override
	public INetworkConfiguration getNetworkConfiguration() {
		return null;
	}

	@Override
	public ISipConfiguration getSipConfiguration() {
		return sipConfig;
	}

	@Override
	public IVlanConfiguration getVlanConfiguration() {
		return null;
	}

	@Override
	public void setMacAddress(MacAddress macAddress) {
	}

	@Override
	public int getCountryCode() {
		return 0;
	}

	@Override
	public MacAddress getDSMacAddress() {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public short getFirmwareMajorVersion() {
		return 0;
	}

	@Override
	public short getFirmwareMinorVersion() {
		return 0;
	}

	@Override
	public short getFirmwareVariantVersion() {
		return 0;
	}

	@Override
	public String getFirmwareVersion() {
		return null;
	}

	@Override
	public String getHardwareModel() {
		return null;
	}

	@Override
	public String getLatitude() {
		return null;
	}

	@Override
	public String getLongitude() {
		return null;
	}

	@Override
	public String getNodeName() {
		return null;
	}

	@Override
	public String getPreferedParent() {
		return null;
	}

	@Override
	public int getRegulatoryDomain() {
		return 0;
	}

	@Override
	public boolean isCountryCodeChanged() {
		return false;
	}

	@Override
	public boolean isFipsEnabled() {
		return false;
	}

	@Override
	public void setCountryCode(int countryCode) {
	}

	@Override
	public void setCountryCodeChanged(boolean changed) {
	}

	@Override
	public void setDescription(String desc) {
	}

	@Override
	public void setFipsEnabled() {
	}

	@Override
	public void setFirmwareMajorVersion(short firmwareMajorVersion) {
	}

	@Override
	public void setFirmwareMinorVersion(short firmwareMinorVersion) {
	}

	@Override
	public void setFirmwareVariantVersion(short firmwareVariantVersion) {
	}

	@Override
	public void setHardwareModel(String hardwareModel) {
	}

	@Override
	public void setLatitude(String lat) {
	}

	@Override
	public void setLongitude(String longt) {
	}

	@Override
	public void setNodeName(String name) {
	}

	@Override
	public void setPreferedParent(String prefParent) {
	}

	@Override
	public void setRegulatoryDomain(int regDomain) {
	}

	@Override
	public void clearData() {
		sipConfig.clearData();
	}

	@Override
	public boolean load(ConfigurationReader configReader) {
		return sipConfig.load(configReader);
	}

	@Override
	public boolean save(ConfigurationWriter configWriter) {
 		return sipConfig.save(configWriter);
	}

	@Override
	public boolean copyTo(ICopyable destination) {
		return sipConfig.copyTo(destination);
	}

	@Override
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		return sipConfig.compare(destination, statusHandler);
	}

}
