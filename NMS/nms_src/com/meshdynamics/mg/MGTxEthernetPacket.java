/*
 * Created Date: Aug 6, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

class MGTxEthernetPacket extends MGPacket {
    
    public MGTxEthernetPacket(long authToken, byte[] packetData) {
        
        super(MGPacket.TYPE_SEND_ETHERNET);
        
        int     offset;                
        
        /* AuthToken, PacketLength, Packet Data */
        
        this.packetData = new byte[4 + 2 + packetData.length];
        
        offset  = 0;        
        offset  = MGByteUtil.write32BE(this.packetData, offset, authToken);
        offset  = MGByteUtil.write16BE(this.packetData, offset, packetData.length);
        offset  = MGByteUtil.writeBlock(this.packetData, offset, packetData);        
        
    }
       
    @Override
    protected byte[] getDataBytes() {
        return packetData;
    }
    
    
    private byte[]  packetData;

}
