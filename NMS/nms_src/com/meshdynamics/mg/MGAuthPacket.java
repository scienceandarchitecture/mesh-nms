/*
 * Created Date: Aug 5, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

class MGAuthPacket extends MGPacket {

    public MGAuthPacket(String userName, String password) {      
        
        super(MGPacket.TYPE_AUTHENTICATE);
        
        this.userName   = userName;
        this.password   = password;
        
    }

    protected byte[] getDataBytes() {
        
        byte[]  dataBytes;
        int     offset;
        
        dataBytes = new byte[1 + userName.length() + 1 + password.length()];
        
        offset    = MGByteUtil.writeByte(dataBytes, 0, userName.length());
        offset    = MGByteUtil.writeBlock(dataBytes, offset, userName.getBytes());
        offset    = MGByteUtil.writeByte(dataBytes, offset, password.length());
        offset    = MGByteUtil.writeBlock(dataBytes, offset, password.getBytes());
        
        return dataBytes;
    }
    
    protected String    userName;
    protected String    password;

}
