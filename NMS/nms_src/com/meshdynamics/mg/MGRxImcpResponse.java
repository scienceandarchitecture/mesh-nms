/*
 * Created Date: Aug 6, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

import java.util.Vector;

class MGRxImcpResponse extends MGResponse {
    
    public int getPacketCount() {
        return packets.size();
    }
    
    public byte[] getPacket(int index) {
        return packets.get(index);
    }
    
    protected static void processBytes(byte[] bytes, MGRxImcpResponse response) {
        
        MGByteUtil.Reader   reader;
        long                count;
        byte[]              packet;
        int                 length;
        
        reader      = new MGByteUtil.Reader(bytes, 0);
        
        count       = reader.read32BE();
        
        for(int i = 0; i < count; i++) {

            length  = reader.read16BE();
            
            if(length < 0)
                break;
           
            packet  = reader.readBlock(length);
            
            response.packets.add(packet);
            
        }        
        
    }
    
    protected static MGRxImcpResponse fromBytes(byte[] bytes) {
        
        MGRxImcpResponse    response;

        response    = new MGRxImcpResponse();
        
        processBytes(bytes, response);
        
        return response;
        
    }
    
    protected MGRxImcpResponse() {
        
        super(MGResponse.TYPE_RECV_IMCP);
        
        packets = new Vector<byte[]>();
        
    }
    
    protected Vector<byte[]>    packets;
}
