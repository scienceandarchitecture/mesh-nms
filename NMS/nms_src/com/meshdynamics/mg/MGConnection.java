/*
 * Created Date: Aug 5, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

class MGConnection {

    public MGConnection(String server, int port) {
        
        try {
            address = InetAddress.getByName(server);
        } catch (Exception e) {
            address = null;
        }
        
        this.port       = port;
        socket          = null;
        outputStream    = null;
        inputStream     = null;
    }
    
    public boolean isConnected() {
        
        if(socket != null)
            return socket.isConnected();
        
        return false;
        
    }
    
    public void connect() throws Exception {
        
        if(isConnected()) {
            disconnect();
        }
        
        socket          = new Socket(address,port);
        outputStream    = socket.getOutputStream();
        inputStream     = socket.getInputStream();       
        
    }    
    
    public int read(byte[] buffer) throws Exception {
    	int offset = 0;
    	while(offset < buffer.length) {
    		int ret = inputStream.read(buffer, offset, (buffer.length-offset));
    		if(ret < 0)
    			break;
    		
    		offset += ret;
    	}
    	
        return offset; 
    }    
    
    public int read(char[] buffer) throws Exception {
        
        byte[]  b;
        String  str;
        int     r;
        
        b = new byte[buffer.length];        
        r = inputStream.read(b);
        
        if(r > 0) {
            
          str = new String(b,0,r,"8859_1");
          
          for(int i = 0; i < r; i++) {
              
            buffer[i] = str.charAt(i) ;
            
          }
          
        }
        
        return r;
    }    
    
    public String readln() throws Exception {
        
        String  str;
        byte[]  b;
        int     c;
        
        str = new String("");
        b   = new byte[1];
        
        do {
            
          c = inputStream.read(b);
          
          if(c <= 0)
            break;
          
          if(b[0] == '\r') {
              
            c = inputStream.read(b);
            
            if(c <= 0)
              break;

            if(b[0] == '\n') {
              break;
            } else {
              break; // also break if just \r is encountered. some servers send erroneous headers containing \r's only
            }
            
          } else if(b[0] == '\n') {
              break; // break if just a \n is encountered. some servers send erroneous headers containing \n's only
          } else {
            str += new String(b);
          }
          
        }while(true);
        
        return str;
    }    
    
    public int availableRead() throws Exception {        
        return inputStream.available();
    }
    
    public void write(byte[] buffer) throws Exception {
        outputStream.write(buffer);
    }    
    
    public void write(byte[] buffer, int offset, int len) throws Exception {
        outputStream.write(buffer,offset,len);
    }    
    
    public void disconnect() throws Exception {
        
        if(inputStream != null)
            inputStream.close();
        
        if(outputStream != null)
            outputStream.close();
        
        if(socket != null)
            socket.close();
        
        socket          = null;
        inputStream     = null;
        outputStream    = null;
    }    
    
    protected int           port;
    protected Socket        socket;
    protected InetAddress   address;
    protected OutputStream  outputStream;
    protected InputStream   inputStream;    
}
