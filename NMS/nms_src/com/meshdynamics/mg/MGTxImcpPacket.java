/*
 * Created Date: Aug 6, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

class MGTxImcpPacket extends MGPacket {
    
    public static final int PACKET_TYPE_UNKNOWN     = 0;
    public static final int PACKET_TYPE_FROM_NODE   = 1;
    public static final int PACKET_TYPE_FROM_VIEWER = 2;

    public MGTxImcpPacket(long authToken, int packetType, byte[] dsMacAddress, byte[] packetData) {
        
        super(MGPacket.TYPE_SEND_IMCP);
        
        int     offset;
        byte[]  dsMacAddrBuffer;
        
        /* AuthToken, PacketType, DS MAC, PacketLength, Packet Data */
        
        dsMacAddrBuffer = new byte[6];
        this.packetData = new byte[4 + 1 + 6 + 2 + packetData.length];
        
        System.arraycopy(dsMacAddress, 0, dsMacAddrBuffer, 0, 6);
        
        offset  = 0;        
        offset  = MGByteUtil.write32BE(this.packetData, offset, authToken);
        offset  = MGByteUtil.writeByte(this.packetData, offset, packetType);
        offset  = MGByteUtil.writeBlock(this.packetData, offset, dsMacAddrBuffer);
        offset  = MGByteUtil.write16BE(this.packetData, offset, packetData.length);
        offset  = MGByteUtil.writeBlock(this.packetData, offset, packetData);
                
    }
    
    @Override
    protected byte[] getDataBytes() {
        return packetData;
    }

    private byte[]  packetData;
}
