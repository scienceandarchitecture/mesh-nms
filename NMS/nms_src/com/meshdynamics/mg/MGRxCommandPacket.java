/*
 * Created Date: Aug 7, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

class MGRxCommandPacket extends MGRxImcpPacket {

    public MGRxCommandPacket(long authToken) {
        
        super(authToken);
        
        type = TYPE_RECV_COMMAND;
    }    
}
