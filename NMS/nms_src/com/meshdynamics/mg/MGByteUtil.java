/*
 * Created Date: Aug 5, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

final class MGByteUtil {
    
   public static class Reader {
       
       public Reader(byte[] bytes, int offset) {
           this.bytes   = bytes;
           this.offset  = offset;
       }
       
       public byte[] readBlock(int length) {
           
           byte[] block;
           
           block = new byte[length];
           
           System.arraycopy(bytes,offset, block, 0, block.length);
           
           offset += block.length;
           
           return block;
      }       
       
       protected int readByte() {           
           return (int)bytes[offset++];
       }
       
       public int read16BE() {
           
           int ret;
           
           ret  = MGByteUtil.read16BE(bytes, offset);
           
           offset   += 2;
           
           return ret;           
       }       
       
       public long read32BE() {           
           
           long ret;
           
           ret  = MGByteUtil.read32BE(bytes, offset);
           
           offset   += 4;
           
           return ret;
       }
                
       
       private byte[]   bytes;
       private int      offset;
       
   }
   
   public static int read16BE(byte[] bytes, int offset) {
       
       int    value; 
       short   s;
       
       value = 0;
       
       for(int i = 0; i < 2; i++) {
           
           s        = bytes[offset + 1 - i];
           s        &= 0xFF;
           
           value    += (s << (8 * i));
           
       }
       
       value   &= 0xFFFF;       
       
       return value;
   }     
   
   public static long read32BE(byte[] bytes, int offset) {
       
       long    value; 
       short   s;
       
       value = 0;
       
       for(int i = 0; i < 4; i++) {
           
           s        = bytes[offset + 3 - i];
           s        &= 0xFF;
           
           value    += (s << (8 * i));
           
       }
       
       value   &= 0xFFFFFFFF;       
       
       return value;
       
   }
    
   public static int writeBlock(byte[] destination, int offset, byte[] block) {
        
        System.arraycopy(block,0, destination, offset, block.length);
        
        return offset + block.length;
   }
    
   public static int writeByte(byte[] destination, int offset, int value) {
        
        destination[offset] = (byte)value;
        
        return offset + 1;
    }
    
   public static int write16BE(byte[] destination, int offset, int value) {
        
        value &= 0xFFFF;
        
        destination[offset]     = (byte)((value >> 8) & 0xFF);
        destination[offset + 1] = (byte)(value & 0xFF);
        
        return offset + 2;
    }    
      
   public static int write32BE(byte[] destination, int offset, long value) {
        
        value &= 0xFFFFFFFF;
        
        destination[offset]     = (byte)((value >> 24) & 0xFF);
        destination[offset + 1] = (byte)((value >> 16) & 0xFF);
        destination[offset + 2] = (byte)((value >> 8) & 0xFF);
        destination[offset + 3] = (byte)(value & 0xFF);
        
        return offset + 4;
    }    
    
    private MGByteUtil() {
        
    }

}
