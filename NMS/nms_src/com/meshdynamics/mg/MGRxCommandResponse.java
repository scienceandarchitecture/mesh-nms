/*
 * Created Date: Aug 7, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

import java.util.Vector;

class MGRxCommandResponse extends MGResponse {
    
    public static class CommandInfo {
        public int      id;
        public int      type;        
        public byte[]   dsMacAddress;
        public byte[]   data;
        public long     srcAuthToken;
    }
        
    
    public int getCommandCount() {
        return commands.size();
    }
    
    public CommandInfo getCommand(int index) {
        return commands.get(index);
    }    
       
    protected MGRxCommandResponse() {
        
        super(TYPE_RECV_COMMAND);
        
        commands = new Vector<CommandInfo>();
    }
    
    protected static MGRxCommandResponse fromBytes(byte[] bytes) {
        
        MGRxCommandResponse response;
        MGByteUtil.Reader   reader;
        int                 count;
        long                length;
        CommandInfo         commandInfo;
        
        response = new MGRxCommandResponse();
        
        reader   = new MGByteUtil.Reader(bytes, 0);
                
        count    = reader.readByte();        
        
        for(int i = 0; i < count; i++) {
            
            commandInfo = new CommandInfo();
            
            commandInfo.id              = reader.readByte();
            commandInfo.dsMacAddress    = reader.readBlock(6);
            commandInfo.type            = reader.readByte();
            commandInfo.srcAuthToken    = reader.read32BE();
            length                      = reader.read32BE();
            commandInfo.data            = reader.readBlock((int)length);            
            
            response.commands.add(commandInfo);
            
        }         
                
        return response;
    
    }
        
    protected Vector<CommandInfo>    commands;

}
