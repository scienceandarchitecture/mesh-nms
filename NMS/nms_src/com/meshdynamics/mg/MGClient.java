/*
 * Created Date: Aug 5, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

import java.util.LinkedList;
import java.util.Vector;

import com.meshdynamics.util.SyncEvent;

public class MGClient {
    
    public static final int MODE_FORWARDER          = 1;
    public static final int MODE_REMOTE_MANAGER     = 2;
    public static final int MODE_ETHERNET_BRIDGE    = 3;
    
    public static interface EthernetListener {
        
        public void ethernetReceive(byte[] packet);
        
    }
    
    public static interface ImcpListener {
        
        public void imcpReceive(byte[] packet);
        
        public void commandReceive(int id, byte[] dsMacAddress, int type, byte[] data, long srcAuthToken);
        
        public void commandResponseReceive(int id, byte[] dsMacAddress, byte[] data);
    }
          
    public MGClient(int     mode,
                    String  server,
                    int     port,
                    boolean useSSL,
                    String  userName,
                    String  password) {
        
        this.mode                       = mode;
        this.server                     = server;
        this.port                       = port;
        this.useSSL                     = useSSL;
        this.authToken                  = 0;
        this.authTokenValid             = false;
        this.serverErrorMessagePrinted  = false;
        this.sendExceptionPrinted       = false;
        this.keepAliveTimeout           = 0;
        this.userName                   = userName;
        this.password                   = password;
        
        try {
            this.senderEvent    = SyncEvent.create(SyncEvent.STATE_NOT_SIGNALLED, true, null);
        } catch(Exception e) {
            
        }
                
        this.sendPacketList     = new LinkedList[2];
        for(int i=0;i<2;i++) {
        	this.sendPacketList[i] = new LinkedList<MGPacket>();
        }
        
        this.quit               = false;        
                       
        this.ethernetListeners  = new Vector<EthernetListener>();
        this.imcpListeners      = new Vector<ImcpListener>();
                
    }
    
    public void start() {
        
        try {            
            authenticate();            
        } catch(Exception e) {       
            
            
            System.err.println(e);
            
            authTokenValid      = false;
            authToken           = 0;            
            keepAliveTimeout    = 0;                        
        }   
        
        new Thread( new Runnable() {
            public void run() {     
                senderThread();
            }            
        }).start();        
        
        switch(mode) {
        case MODE_ETHERNET_BRIDGE:
            new Thread( new Runnable() {
                public void run() {     
                    ethernetReceiverThread();
                }            
            }).start();            
            break;
        case MODE_FORWARDER:
            
            new Thread( new Runnable() {
                public void run() {     
                    imcpReceiverThread();
                }            
            }).start();              
            
            new Thread( new Runnable() {
                public void run() {     
                    commandReceiverThread();
                }            
            }).start();             
            
            break;
        case MODE_REMOTE_MANAGER:
            
            new Thread( new Runnable() {
                public void run() {     
                    imcpReceiverThread();
                }            
            }).start();              
            
            new Thread( new Runnable() {
                public void run() {     
                    commandResponseReceiverThread();
                }            
            }).start();              
            
            break;
        }
                        
    }
    
    public void stop() {
        
    	for(LinkedList<MGPacket> p : this.sendPacketList) {
            synchronized(p) {
                p.clear();
            }
    	}
    	
        
        quit    = true;
        senderEvent.signalEvent();        
        
    }    
    
    public boolean addImcpListener(ImcpListener listener) {
        
        if(mode != MODE_FORWARDER && mode != MODE_REMOTE_MANAGER)
            return false;
        
        imcpListeners.add(listener);
        
        return true;        
    }
    
    public boolean addEthernetListener(EthernetListener listener) {    
        
        if(mode != MODE_ETHERNET_BRIDGE)
            return false;
        
        ethernetListeners.add(listener);
        
        return true;
    }
    
    public boolean sendEthernetPacket(byte[] packetData) {
        
        MGTxEthernetPacket packet;
        
        if(mode != MODE_ETHERNET_BRIDGE)
            return false;
        
        if(!authTokenValid)
            return false;
        
        packet = new MGTxEthernetPacket(authToken, packetData);
        
        synchronized(sendPacketList[0]) {
        	sendPacketList[0].addLast(packet);
        }        
        
        senderEvent.signalEvent();
                
        return true;
        
    }
    
    public boolean sendImcpPacket(byte[] dsMacAddress, byte[] packetData) {
        
        MGTxImcpPacket  packet;
        
        if(mode != MODE_FORWARDER && mode != MODE_REMOTE_MANAGER)
            return false;        
        
        if(!authTokenValid)
            return false;        
        
        packet  = new MGTxImcpPacket(authToken,
                                    (mode == MODE_FORWARDER) ?  MGTxImcpPacket.PACKET_TYPE_FROM_NODE : MGTxImcpPacket.PACKET_TYPE_FROM_VIEWER,
                                    dsMacAddress,
                                    packetData);
        
        synchronized(sendPacketList[1]) {
            sendPacketList[1].add(packet);
        }        
        
        senderEvent.signalEvent();
                
        return true;
    }
    
    public boolean sendCommand(int commandId, byte[] dsMacAddress, int type, byte[] commandData) {
        
        MGTxCommandPacket   packet;
        
        if(mode != MODE_REMOTE_MANAGER)
            return false;
        
        if(!authTokenValid)
            return false;
        
        packet = new MGTxCommandPacket(authToken, commandId, dsMacAddress, type, commandData);
        
        synchronized(sendPacketList[0]) {
            sendPacketList[0].addLast(packet);
        }        
        
        senderEvent.signalEvent();
        
        return true;        
        
    }
    
    public boolean sendCommandResponse(int commandId, byte[] dsMacAddress, long srcAuthToken, byte[] responseData) {

        MGTxCommandResponsePacket   packet;
        
        if(mode != MODE_FORWARDER)
            return false;
        
        if(!authTokenValid)
            return false;        
        
        packet = new MGTxCommandResponsePacket(authToken, commandId, dsMacAddress, srcAuthToken, responseData);
        
        synchronized(sendPacketList[0]) {
            sendPacketList[0].addLast(packet);
        }        
        
        senderEvent.signalEvent();
                
        return true;         
        
    }
    
    protected void printConnectionException(Exception e, String context) {

        if(!sendExceptionPrinted) {                  
            
            System.err.println("<MEDIUM>MGClient : " + e + " " + context);
                        
            sendExceptionPrinted = true;
            
        }                
        
    }
    
    protected void printError(MGErrorResponse response, String context) {
        
        if(response == null) {
            
            System.err.printf("<MEDIUM>MGClient : No response received fom server %s:%d %s.\n",
                              server,
                              port,
                              context);
            
            return;
            
        }
        
        if(response.getCode() == MGErrorResponse.GENERAL_ERROR)
            return;
        
        if(!serverErrorMessagePrinted) {
            
            System.err.printf("<HIGH>MGClient : Server %s:%d responded with code %d (%s) %s\n",
                    server,
                    port,
                    response.getCode(),
                    response.getResponseLine(),
                    context);       
            
            serverErrorMessagePrinted   = true;
            
        }
        
    }
                                          
    
    protected void authenticate() {
        
        MGHttpConnection    connection;
        MGAuthPacket        packet;
        MGResponse          response;
        String              context;
        
        context         = "when trying to authenciate.";
        
        authTokenValid  = false;
        authToken       = 0;        
        connection      = new MGHttpConnection(server, port, useSSL);        
        packet          = new MGAuthPacket(userName,password);
                
        try {            
            response    = connection.sendPacket(packet);
        } catch(Exception e) {            
            printConnectionException(e, context);
            connection.close();
            return;
        }
        
        if(response instanceof MGAuthResponse) {
            
            authToken               = ((MGAuthResponse)response).getAuthToken();
            keepAliveTimeout        = ((MGAuthResponse)response).getKeepAliveTimeout();
            authTokenValid          = true;
            sendExceptionPrinted    = false;
            
            System.out.printf("MGClient : Authenticated with server %s:%d, session identifier : %08X\n",
                              server,
                              port,
                              authToken);
            
        } else if(response != null && (response instanceof MGErrorResponse)) {
            
            printError((MGErrorResponse)response,context);
            
        } else {
            
            printError((MGErrorResponse)response,context);          
        }
        
        connection.close();        
        
    }
        
    protected void ethernetReceiverThread() {   
        
        MGHttpConnection        connection;
        MGRxEthernetResponse    ethResponse;
        MGRxEthernetPacket      packet;
        MGResponse              response;
        String                  context;
        
        System.out.printf("MGClient : Ethernet receiver thread started for server %s:%d\n", server, port);
        
        context    = " in Ethernet receiver.";
        connection = new MGHttpConnection(server, port, useSSL);       
        
        packet     = new MGRxEthernetPacket(authToken);        
        
        while(true) {
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                
            }
            
            if(quit)
                break;
            
            if(!authTokenValid) {
                authenticate();     
                if(authTokenValid) {
                    packet = new MGRxEthernetPacket(authToken);
                } else {
                    continue;
                }
            }
                           
                                        
            try {
                response = connection.sendPacket(packet);
            } catch (Exception e) {                    
                printConnectionException(e, context);                    
                connection.close();
                continue;
            }                                
                
            if(response instanceof MGErrorResponse) {
                
                printError((MGErrorResponse)response, context);
        
                if(((MGErrorResponse)response).getCode() != MGErrorResponse.GENERAL_ERROR)
                    authTokenValid = false;
                                
            } else if(response == null) {
                
                printError(null, context);
                
                authTokenValid  = false;
                
            } else {
                
                ethResponse = (MGRxEthernetResponse)response;
                
                for(int i = 0; i < ethernetListeners.size(); i++) {                
                    for(int j = 0; j < ethResponse.getPacketCount(); j++) {                    
                        ethernetListeners.get(i).ethernetReceive(ethResponse.getPacket(j));                    
                    }                
                }   
                
                sendExceptionPrinted        = false;
                serverErrorMessagePrinted   = false;
            }
            
        }
                
        connection.close();
        
        System.out.printf("MGClient : Ethernet receiver stopped started for server %s:%d\n", server, port);
        
    }
    
    protected void commandResponseReceiverThread() {
        
        MGHttpConnection                            connection;
        MGRxCommandResponsePacket                   packet;
        MGResponse                                  response;
        MGRxCommandResponseResponse                 cmdResponse;
        MGRxCommandResponseResponse.ResponseInfo    responseInfo;

        System.out.printf("MGClient : Command response receiver thread started for server %s:%d\n", server, port);
        
        connection = new MGHttpConnection(server, port, useSSL);        
        
        while(true) {
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                
            }
            
            if(quit)
                break;
            
            if(!authTokenValid) {
                continue;                
            }               
            
            packet = new MGRxCommandResponsePacket(authToken);            
            
            try {
                response = connection.sendPacket(packet);
            } catch (Exception e) {
                connection.close();
                continue;
            }        
            
            if(response instanceof MGErrorResponse || response == null) {                
                connection.close();
                continue;                       
                                
            }         
            
            cmdResponse = (MGRxCommandResponseResponse)response;
            
            for(int i = 0; i < imcpListeners.size(); i++) {   
                
                for(int j = 0; j < cmdResponse.getResponseCount(); j++) { 
                    
                    responseInfo = cmdResponse.getResponse(j);
                    
                    imcpListeners.get(i).commandResponseReceive(responseInfo.id,
                                                                responseInfo.dsMacAddress,
                                                                responseInfo.data);  
                }                
            }
            
        }
                
        connection.close();
        
        System.out.printf("MGClient : Command response receiver stopped for server %s:%d\n", server, port);        
        
    }
    
    protected void commandReceiverThread() {
        
        MGHttpConnection                connection;
        MGRxCommandPacket               packet;
        MGResponse                      response;
        MGRxCommandResponse             cmdResponse;
        MGRxCommandResponse.CommandInfo commandInfo;

        System.out.printf("MGClient : Command receiver thread started for server %s:%d\n", server, port);
        
        connection = new MGHttpConnection(server, port, useSSL);        
        
        while(true) {
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                
            }
            
            if(quit)
                break;

            if(!authTokenValid) {
                continue;                
            }            
            
            packet = new MGRxCommandPacket(authToken);                        
            
            try {
                response = connection.sendPacket(packet);
            } catch (Exception e) {                
                connection.close();
                continue;
            }           
            
            if(response instanceof MGErrorResponse || response == null) {                
                connection.close();
                continue;                       
                                
            }
            
            cmdResponse = (MGRxCommandResponse)response;
            
            for(int i = 0; i < imcpListeners.size(); i++) {   
                
                for(int j = 0; j < cmdResponse.getCommandCount(); j++) { 
                    
                    commandInfo = cmdResponse.getCommand(j);
                    
                    imcpListeners.get(i).commandReceive(commandInfo.id,
                                                        commandInfo.dsMacAddress,
                                                        commandInfo.type,
                                                        commandInfo.data,
                                                        commandInfo.srcAuthToken);                    
                }                
            }

            connection.close();
        }
        
        System.out.printf("MGClient : Command receiver stopped for server %s:%d\n", server, port);        
        
    }
    
    protected void imcpReceiverThread() {
        
        MGHttpConnection        connection;
        MGRxImcpPacket          packet;
        MGRxImcpResponse        imcpResponse;
        MGResponse              response;
        String                  context;
        
        System.out.printf("MGClient : IMCP receiver thread started for server %s:%d\n", server, port);
        
        context    = " in IMCP receiver.";        
        connection = new MGHttpConnection(server, port, useSSL);
        
        packet     = new MGRxImcpPacket(authToken);
        
        while(true) {
                        
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                
            }
            
            if(quit)
                break;
            
            if(!authTokenValid) {
                authenticate();     
                if(authTokenValid) {
                    packet                      = new MGRxImcpPacket(authToken);
                    sendExceptionPrinted        = false;      
                    serverErrorMessagePrinted   = false;                    
                } else {
                    continue;
                }
            }
            
            try {
                response = connection.sendPacket(packet);
            } catch (Exception e) {
                printConnectionException(e, context);                    
                connection.close();
                continue;
            }            
            
            if(response instanceof MGErrorResponse) {
                
                printError((MGErrorResponse)response, context);
                
                if(((MGErrorResponse)response).getCode() != MGErrorResponse.GENERAL_ERROR)
                    authTokenValid = false;
                                
            } else if(response == null) {
                
                printError(null, context);
                
                authTokenValid  = false;
                
            } else {
                
                imcpResponse =  (MGRxImcpResponse)response;
                
                for(int i = 0; i < imcpListeners.size(); i++) {                
                    for(int j = 0; j < imcpResponse.getPacketCount(); j++) {                    
                        imcpListeners.get(i).imcpReceive(imcpResponse.getPacket(j));                    
                    }                
                }          
                
                sendExceptionPrinted        = false;      
                serverErrorMessagePrinted   = false;
                
            }           
        
            connection.close();
        }
        
        System.out.printf("MGClient : IMCP receiver stopped for server %s:%d\n", server, port);        
        
    }
    
    protected void senderThread() {
        
        MGHttpConnection    connection;
        MGPacket            packet;
        
        System.out.printf("MGClient : Transmission thread started for server %s:%d\n", server, port);        
        
        connection = new MGHttpConnection(server, port, useSSL);
        
        while(true) {
        	
            if(quit)
                break;        	
            
            senderEvent.resetEvent();
            senderEvent.waitOn();                

            if(quit)
                break;
            
            while(true) {
                
            	int nonEmptyQueueCount = sendPacketList.length;
            	
            	for(int i=0;i<sendPacketList.length;i++) {
            		
            		LinkedList<MGPacket>q = sendPacketList[i];
            		
                    synchronized(q) {
                        try {
                        	if(q.peek() == null) {
                        		nonEmptyQueueCount--;
                        		continue;
                        	}
                            packet   = q.removeFirst();
                        } catch(Exception e) {
                        	e.printStackTrace();
                            packet = null;
                        }
                        
                    }                

                    if(packet == null) {
                        continue;
                    }
                    
                    boolean packetSent = false;
                    try {
                    	
                        MGResponse response = connection.sendPacket(packet);
                        connection.close();
                        if(response != null && response.type == packet.type) {
                        	packetSent = true;
                        }
                        	
                    } catch (Exception e) {
                    	e.printStackTrace();
                    }

                    if(packetSent == false) {
    	            	synchronized(q) {
    	            		q.addFirst(packet);
    	            	}
    	            	System.out.println("Packet not sent.Requeuing");
                    }
                    
            	} //end for loop
                
            	if(nonEmptyQueueCount <= 0) {
            		break;
            	}
            	
            } //end while loop            
                       
        }
                
        connection.close();
        
        System.out.printf("MGClient : Transmission thread stopped for server %s:%d\n", server, port);
    }
    
    private Vector<ImcpListener>        imcpListeners;
    private Vector<EthernetListener>    ethernetListeners;
    private boolean                     quit;
    private LinkedList<MGPacket>[]		sendPacketList;
    private SyncEvent                   senderEvent;
    public  boolean                     authTokenValid;              
    private long                        authToken;
    private boolean                     sendExceptionPrinted;
    private boolean                     serverErrorMessagePrinted;
    private int                         keepAliveTimeout;
    private int                         mode;
    private String                      server;
    private int                         port;
    private boolean                     useSSL;
    private String                      userName;
    private String                      password;
}

