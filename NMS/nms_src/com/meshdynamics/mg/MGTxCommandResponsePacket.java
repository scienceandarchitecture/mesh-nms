/*
 * Created Date: Aug 7, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

class MGTxCommandResponsePacket extends MGPacket {
    
    public MGTxCommandResponsePacket(long   authToken,
                                     int    commandId,
                                     byte[] dsMacAddress,
                                     long   srcAuthToken,
                                     byte[] responseData) {
        
        super(TYPE_SEND_CMD_RESP);
        
        int     offset;
        byte[]  dsMacAddrBuffer;        
         
        dsMacAddrBuffer = new byte[6];
        
        this.packetData = new byte[  4  // AuthToken
                                   + 1  // Command ID
                                   + 6  // MAC address
                                   + 4  // Source Auth token
                                   + 4  // Response data length
                                   + responseData.length
                                   ];
        
        System.arraycopy(dsMacAddress, 0, dsMacAddrBuffer, 0, 6);
         
        offset  = 0;        
        offset  = MGByteUtil.write32BE(this.packetData, offset, authToken);
        offset  = MGByteUtil.writeByte(this.packetData, offset, commandId);        
        offset  = MGByteUtil.writeBlock(this.packetData, offset, dsMacAddrBuffer);
        offset  = MGByteUtil.write32BE(this.packetData, offset, srcAuthToken);       
        offset  = MGByteUtil.write32BE(this.packetData, offset, responseData.length);
        offset  = MGByteUtil.writeBlock(this.packetData, offset, responseData);               
        
    }

    
    @Override
    protected byte[] getDataBytes() {
        return packetData;
    }

    private byte[]  packetData;      
}


