/*
 * Created Date: Aug 5, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

class MGAuthResponse extends MGResponse {
    
    protected static MGAuthResponse fromBytes(byte[] bytes) {
        
        MGAuthResponse      response;
        MGByteUtil.Reader   reader;
        
        response    = new MGAuthResponse();
        reader      = new MGByteUtil.Reader(bytes, 0);
        
        response.authToken          = reader.read32BE();
        response.keepAliveTimeout   = reader.readByte();        
        
        return response;
    }
    
    protected MGAuthResponse() {
        super(MGResponse.TYPE_AUTHENTICATE);
    }
    
    public long getAuthToken() {
        return authToken;
    }
    
    public int getKeepAliveTimeout() {
        return keepAliveTimeout;
    }
    
    protected long   authToken;
    protected int    keepAliveTimeout;
}
