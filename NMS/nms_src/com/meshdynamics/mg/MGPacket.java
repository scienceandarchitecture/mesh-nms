/*
 * Created Date: Aug 5, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

abstract class MGPacket {
    
    public byte[] toBytes() {
        
        byte[]  dataBytes;
        byte[]  packetBytes;
        int     offset;
        
        dataBytes   = getDataBytes();
        packetBytes = new byte[dataBytes.length + HEADER_LENGTH];
        
        offset      = MGByteUtil.writeByte(packetBytes,0,majorVersion);
        offset      = MGByteUtil.writeByte(packetBytes,offset,minorVersion);
        offset      = MGByteUtil.write16BE(packetBytes,offset,type);
        offset      = MGByteUtil.write32BE(packetBytes,offset, dataBytes.length);
        offset      = MGByteUtil.writeBlock(packetBytes,offset,dataBytes);
                              
        return packetBytes;
        
    }
    
    public int getType() {
        return type;
    }    
    
 
    
    protected abstract byte[] getDataBytes();
    
    protected MGPacket(int type) {
        
        this.type          = type;
        this.majorVersion  = 1;
        this.minorVersion  = 0;
        
    }   
        
    protected int   type;
    protected int   majorVersion;
    protected int   minorVersion;
    
    protected static final int HEADER_LENGTH    = 8;
    
    public static final int TYPE_AUTHENTICATE   = 1;
    public static final int TYPE_SEND_IMCP      = 2;
    public static final int TYPE_RECV_IMCP      = 3;
    public static final int TYPE_SEND_COMMAND   = 4;
    public static final int TYPE_RECV_COMMAND   = 5;
    public static final int TYPE_SEND_CMD_RESP  = 6;
    public static final int TYPE_RECV_CMD_RESP  = 7;
    public static final int TYPE_SEND_ETHERNET  = 8;
    public static final int TYPE_RECV_ETHERNET  = 9;    

}
