/*
 * Created Date: Aug 5, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

import java.util.StringTokenizer;

class MGHttpConnection {
    
    public MGHttpConnection(String server, int port, boolean useSSL) {
        
        this.server = new String(server);
        
        if(useSSL) {
            connection = new MGSecureConnection(server, port);
        } else {
            connection = new MGConnection(server, port);
        }        
        
    }
    
    private int getResponseCode(String responseLine) {
        
        StringTokenizer tokenizer;
        int             code;
        
        tokenizer = new StringTokenizer(responseLine, " ");
        
        try {
            tokenizer.nextToken();  /* The HTTP version string e.g. HTTP/1.1 */        
            code = Integer.parseInt(tokenizer.nextToken());
        } catch(Exception e) {
            code = 500;
        }
        
        return code;
        
    }
    
    public void close() {
        try {
            connection.disconnect();
        } catch (Exception e) {
            
        }
    }
       
    public MGResponse sendPacket(MGPacket packet) throws Exception {
        
        byte[]  packetBytes;
        String  reqestHeader;
        String  responseLine;
        int     responseCode;
        byte[]  dataBytes;
        long    dataLength;
        
        /**
         * Connect if we are not already connected, or if the server closed our connection.
         */
        
        if(!connection.isConnected()) {
            connection.connect();
        }
        
        /**
         * Create and send the request header
         */
        
        packetBytes = packet.toBytes();
        
        reqestHeader = String.format(REQUEST_HEADER,server,packetBytes.length);
        
        connection.write(reqestHeader.getBytes());
        
        /**
         * Send Data
         */
        
        connection.write(packetBytes);
        /**
         * Read the main response line and parse Response code
         */
        
        responseLine    = connection.readln();       
        responseCode    = getResponseCode(responseLine);
        
        if(responseCode != 200 && responseCode != 204) {                       
            return new MGErrorResponse(responseCode, responseLine);            
        }
        
        /**
         * Skip all response headers
         */
        
        while(true) {
            
            responseLine = connection.readln();
            
            if(responseLine.equals("\r\n") 
            || responseLine.isEmpty())
                break;
        }
        
        if(responseCode != 200) {
            return null;
        }
        
        /**
         * Now read the data length
         */
        
        dataLength  = 0;
        dataBytes   = new byte[4];
        
        connection.read(dataBytes);        
        dataLength  = MGByteUtil.read32BE(dataBytes,0);
                
        /**
         * Read the data
         */
        
        if(dataLength > 0) {                    
            dataBytes 	= new byte[(int)dataLength];
           	connection.read(dataBytes);
        } else {
            dataBytes = null;
        }
        
        return MGResponse.fromBytes(packet.getType(), dataBytes);
              
    }
    
    protected static final String REQUEST_HEADER = "POST /md-mg/operation HTTP/1.1\r\n"
                                                 + "Host: %s\r\n"        
                                                 + "Connection: Keep-Alive\r\n"
                                                 + "User-Agent: Meshdynamics NMS\r\n"
                                                 + "Content-Type: application/x-binary\r\n"
                                                 + "Content-Length: %d\r\n"
                                                 + "\r\n";
        
    protected MGConnection  connection;
    protected String        server;
    
}
