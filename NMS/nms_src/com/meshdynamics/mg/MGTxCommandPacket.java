/*
 * Created Date: Aug 7, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

class MGTxCommandPacket extends MGPacket {
    
    public MGTxCommandPacket(long   authToken,
                             int    commandId,
                             byte[] dsMacAddress,
                             int    type,
                             byte[] commandData) {
     
       super(MGPacket.TYPE_SEND_COMMAND);
        
       int     offset;
       byte[]  dsMacAddrBuffer;        
        
       dsMacAddrBuffer = new byte[6];
       
       this.packetData = new byte[  4  // AuthToken
                                  + 1  // Command ID
                                  + 6  // MAC address
                                  + 1  // Command Type
                                  + 4  // Command data length
                                  + commandData.length
                                  ];
       
       System.arraycopy(dsMacAddress, 0, dsMacAddrBuffer, 0, 6);
        
       offset  = 0;        
       offset  = MGByteUtil.write32BE(this.packetData, offset, authToken);
       offset  = MGByteUtil.writeByte(this.packetData, offset, commandId);        
       offset  = MGByteUtil.writeBlock(this.packetData, offset, dsMacAddrBuffer);
       offset  = MGByteUtil.writeByte(this.packetData, offset, type);
       offset  = MGByteUtil.write32BE(this.packetData, offset, commandData.length);
       offset  = MGByteUtil.writeBlock(this.packetData, offset, commandData);                
       
    }

    @Override
    protected byte[] getDataBytes() {
        return packetData;
    }

    private byte[]  packetData;    
}
