/*
 * Created Date: Aug 5, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;


import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

class MGSecureConnection extends MGConnection {
   
    public MGSecureConnection(String server, int port) {
        super(server, port);
    }
    
    public void connect() throws Exception {
        
        SSLSocketFactory    sslFactory;
        
        sslFactory  = (SSLSocketFactory) SSLSocketFactory.getDefault();
        
        socket      = sslFactory.createSocket(address,port);
        
        ((SSLSocket)socket).startHandshake();
        
        inputStream     = socket.getInputStream();
        outputStream    = socket.getOutputStream();            
        
    }    

}

