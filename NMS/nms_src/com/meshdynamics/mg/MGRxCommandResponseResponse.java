/*
 * Created Date: Aug 7, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

import java.util.Vector;

class MGRxCommandResponseResponse extends MGResponse {
    
    public static class ResponseInfo {
        public int      id;    
        public byte[]   dsMacAddress;
        public byte[]   data;        
    }
    
    public int getResponseCount() {
        return responses.size();
    }
    
    public ResponseInfo getResponse(int index) {
        return responses.get(index);
    }     
    
    protected MGRxCommandResponseResponse() {
        
        super(TYPE_RECV_CMD_RESP);
        
        responses = new Vector<ResponseInfo>();
    }
    
    static protected MGRxCommandResponseResponse fromBytes(byte[] bytes) {
        
        MGRxCommandResponseResponse response;
        MGByteUtil.Reader           reader;
        int                         count;
        long                        length;
        ResponseInfo                responseInfo;
        
        response = new MGRxCommandResponseResponse();
        
        reader   = new MGByteUtil.Reader(bytes, 0);
        
        count    = reader.readByte();        
                
        for(int i = 0; i < count; i++) {
            
            responseInfo = new ResponseInfo();
            
            
            responseInfo.id             = reader.readByte();
            responseInfo.dsMacAddress   = reader.readBlock(6);
            length                      = reader.read32BE();
            responseInfo.data           = reader.readBlock((int)length);            
            
            response.responses.add(responseInfo);
            
        }         
        
        return response;
    }
                
    protected Vector<ResponseInfo>    responses;
    
}
