/*
 * Created Date: Aug 6, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

class MGRxImcpPacket extends MGPacket {
    
    public MGRxImcpPacket(long authToken) {
        
        super(MGPacket.TYPE_RECV_IMCP);
        
        this.packetData = new byte[4];
        
        MGByteUtil.write32BE(this.packetData, 0, authToken);
        
    }

    @Override
    protected byte[] getDataBytes() {
        return packetData;
    }

    private byte[]  packetData;
}
