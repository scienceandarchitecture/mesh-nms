/*
 * Created Date: Aug 7, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

class MGRxCommandResponsePacket extends MGRxImcpPacket {

    public MGRxCommandResponsePacket(long authToken) {
        
        super(authToken);
        
        type = TYPE_RECV_CMD_RESP;
    }    

}
