/*
 * Created Date: Aug 12, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

public class MGErrorResponse extends MGResponse {

    public MGErrorResponse(int code, String responseLine) {
        super(TYPE_ERROR);
        
        this.code = code;
        this.responseLine = responseLine;
    }
    
    public int getCode() {
        return code;
    }
    
    public String getResponseLine() {
        return responseLine;
    }
    
    private int code;
    private String responseLine;
    
    public static final int GENERAL_ERROR = 500;

}
