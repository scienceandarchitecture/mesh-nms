/*
 * Created Date: Aug 6, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

class MGRxEthernetResponse extends MGRxImcpResponse {
    
    protected static MGRxEthernetResponse fromBytes(byte[] bytes) {
        
        MGRxEthernetResponse    response;

        response    = new MGRxEthernetResponse();
        
        processBytes(bytes, response);
        
        return response;
        
    }    
    
    protected MGRxEthernetResponse() {
        this.type = TYPE_RECV_ETHERNET; 

    }

}
