/*
 * Created Date: Aug 6, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

class MGRxEthernetPacket extends MGRxImcpPacket {
    
    public MGRxEthernetPacket(long authToken) {
        
        super(authToken);
        
        type = TYPE_RECV_ETHERNET;
    }

}
