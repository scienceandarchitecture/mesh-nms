/*
 * Created Date: Aug 5, 2008
 * Author : Sri
 */
package com.meshdynamics.mg;

abstract class MGResponse {
    
    public int getType() {
        return type;
    }
    
    public static MGResponse fromBytes(int type, byte[] buffer) {
        
        switch(type) {
        case TYPE_AUTHENTICATE:
            return MGAuthResponse.fromBytes(buffer);    
        case TYPE_SEND_IMCP:
        case TYPE_SEND_ETHERNET:
        case TYPE_SEND_CMD_RESP:
        case TYPE_SEND_COMMAND:
            return new MGNullResponse(type);   
        case TYPE_RECV_IMCP:
            return MGRxImcpResponse.fromBytes(buffer);
        case TYPE_RECV_ETHERNET:
            return MGRxEthernetResponse.fromBytes(buffer);
        case TYPE_RECV_COMMAND:
            return MGRxCommandResponse.fromBytes(buffer);
        case TYPE_RECV_CMD_RESP:
            return MGRxCommandResponseResponse.fromBytes(buffer);
        }
        
        return null;
        
    }
         
    protected MGResponse(int type) {
        this.type   = type;
    }
    
    protected int type;
    
    public static final int TYPE_ERROR          = 0;
    public static final int TYPE_AUTHENTICATE   = 1;
    public static final int TYPE_SEND_IMCP      = 2;
    public static final int TYPE_RECV_IMCP      = 3;
    public static final int TYPE_SEND_COMMAND   = 4;
    public static final int TYPE_RECV_COMMAND   = 5;
    public static final int TYPE_SEND_CMD_RESP  = 6;
    public static final int TYPE_RECV_CMD_RESP  = 7;
    public static final int TYPE_SEND_ETHERNET  = 8;
    public static final int TYPE_RECV_ETHERNET  = 9; 
    

}

