/*
 * Created on Mar 13, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.sip;

import com.meshdynamics.meshviewer.sip.ISipEventListener.ICallDlgStatusInfo;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SipDialogStatusPacket extends SipPacket implements ICallDlgStatusInfo {

	public static final int PENDING_STATUS_FOR_NONE			= 0;
	public static final int PENDING_STATUS_FOR_CALLER		= 1;
	public static final int PENDING_STATUS_FOR_CALLEE		= 2;
	public static final int PENDING_STATUS_FOR_CLEANUP		= 3;
	
	private long 		dlgId;
	private short 		pendingStatus;
	private MacAddress 	callerMacAddress;
	private int			callerRport;
	private IpAddress 	callerIpAddress;
	private MacAddress 	calleeMacAddress;
	private int 		calleeRport;
	private IpAddress 	calleeIpAddress;
	private short 		callerCallState;
	private short 		calleeCallState;
	
	/**
	 * 
	 */
	public SipDialogStatusPacket(String meshId, MacAddress senderMacAddress) {
		super(meshId, senderMacAddress);
		this.packetType	= SIP_PACKET_TYPE_DIALOG_STATUS;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.sip.SipPacket#readPacket(byte[])
	 */
	public int readPacket(byte[] buffer) {
		long 			tempLng;
		int 			tempInt;

		BufferReader packetReader 	= new BufferReader(buffer, buffer.length);
		
		/*
		 * skip one byte packet type
		 */
		packetReader.readByte();
		
		tempLng						= packetReader.readInt();
		dlgId	 					= tempLng & 0xffffffff;
		pendingStatus		 		= packetReader.readByte();
		callerMacAddress 			= packetReader.readMacAddress();
		tempInt						= packetReader.readShort();
		callerRport					= tempInt & 0xffff;
		callerIpAddress				= packetReader.readIPAddress();
		calleeMacAddress 			= packetReader.readMacAddress();
		tempInt						= packetReader.readShort();
		calleeRport					= tempInt & 0xffff;
		calleeIpAddress				= packetReader.readIPAddress();
		callerCallState				= packetReader.readByte();
		calleeCallState				= packetReader.readByte();
		
		return 0;
	}

	/**
	 * @return Returns the calleeCallState.
	 */
	public short getCalleeCallState() {
		return calleeCallState;
	}
	/**
	 * @return Returns the calleeIpAddress.
	 */
	public IpAddress getCalleeIpAddress() {
		return calleeIpAddress;
	}
	/**
	 * @return Returns the calleeMacAddress.
	 */
	public MacAddress getCalleeMacAddress() {
		return calleeMacAddress;
	}
	/**
	 * @return Returns the calleeRport.
	 */
	public int getCalleeRport() {
		return calleeRport;
	}
	/**
	 * @return Returns the callerCallState.
	 */
	public short getCallerCallState() {
		return callerCallState;
	}
	/**
	 * @return Returns the callerIpAddress.
	 */
	public IpAddress getCallerIpAddress() {
		return callerIpAddress;
	}
	/**
	 * @return Returns the callerMacAddress.
	 */
	public MacAddress getCallerMacAddress() {
		return callerMacAddress;
	}
	/**
	 * @return Returns the callerRport.
	 */
	public int getCallerRport() {
		return callerRport;
	}
	/**
	 * @return Returns the dlgId.
	 */
	public long getDlgId() {
		return dlgId;
	}
	/**
	 * @return Returns the pendingStatus.
	 */
	public short getPendingStatus() {
		return pendingStatus;
	}
}
