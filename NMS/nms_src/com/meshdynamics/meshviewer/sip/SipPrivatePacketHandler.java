package com.meshdynamics.meshviewer.sip;

import java.util.Vector;

import com.meshdynamics.meshviewer.imcppackets.SipPrivatePacket;
import com.meshdynamics.meshviewer.sip.ISipEventListener.ISipEventInfo;

public class SipPrivatePacketHandler {
	
	private Vector<ISipEventListener> listeners;
	
	public SipPrivatePacketHandler() {
		listeners = new Vector<ISipEventListener>();
	}
	
	public boolean processPacket(SipPrivatePacket sipPacket) {
		
		if(sipPacket.data == null)
			return false;
		
		byte packetType = sipPacket.data[0];
	
		switch(packetType) {
			case SipPacket.SIP_PACKET_TYPE_DIALOG_STATUS:
				processDlgStatus(sipPacket);
				break;
			case SipPacket.SIP_PACKET_TYPE_NEW_DIALOG:
				processNewCallDlg(sipPacket);
				break;
			case SipPacket.SIP_PACKET_TYPE_STA_REGISTRATION:
				processStaRegistration(sipPacket);
				break;
		}
		
		return true;
	}

	private void processDlgStatus(SipPrivatePacket sipPacket) {
		SipDialogStatusPacket callStatusPacket = new SipDialogStatusPacket(sipPacket.getMeshId(), sipPacket.getDsMacAddress());
		callStatusPacket.readPacket(sipPacket.data);
		notifyListeners(ISipEventListener.SIP_EVENT_CALL_DLG_STATUS, callStatusPacket);
	}

	private void processNewCallDlg(SipPrivatePacket sipPacket) {
		SipCallDialogPacket callDlgPacket = new SipCallDialogPacket(sipPacket.getMeshId(), sipPacket.getDsMacAddress());
		callDlgPacket.readPacket(sipPacket.data);
		notifyListeners(ISipEventListener.SIP_EVENT_NEW_CALL_DLG, callDlgPacket);
	}

	private void processStaRegistration(SipPrivatePacket sipPacket) {
		SipStaRegistrationPacket staRegPacket = new SipStaRegistrationPacket(sipPacket.getMeshId(), sipPacket.getDsMacAddress());
		staRegPacket.readPacket(sipPacket.data);
		notifyListeners(ISipEventListener.SIP_EVENT_STA_REGISTRATION, staRegPacket);
	}

	private void notifyListeners(int event, ISipEventInfo eventInfo) {
		for(ISipEventListener l : listeners) {
			l.sipEvent(event, eventInfo);
		}
	}
	
	public void addListener(ISipEventListener listener) {
		if(listeners.contains(listener) == true)
			return;
		
		listeners.add(listener);
	}
	
	public void removeListener(ISipEventListener listener) {
		listeners.remove(listener);
	}
}
