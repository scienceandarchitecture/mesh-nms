/*
 * Created on Mar 13, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.sip;

import com.meshdynamics.meshviewer.sip.ISipEventListener.IStaRegistrationInfo;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SipStaRegistrationPacket extends SipPacket implements IStaRegistrationInfo {

	private MacAddress 	macAddress;
	private IpAddress	ipAddress;
	private int			rport;
	private short		registered;
	
	/**
	 * 
	 */
	public SipStaRegistrationPacket(String meshId, MacAddress senderMacAddress) {
		super(meshId, senderMacAddress);
		this.packetType = SIP_PACKET_TYPE_STA_REGISTRATION;
		macAddress		= null;
		ipAddress		= null;
		registered		= 0;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.sip.SipPacket#readPacket(byte[])
	 */
	public int readPacket(byte[] buffer) {
		BufferReader packetReader = new BufferReader(buffer, buffer.length);
		int			temp;
		
		/*
		 * skip one byte packet type
		 */
		packetReader.readByte();
		
		macAddress 	= packetReader.readMacAddress();
		registered	= packetReader.readByte();
		ipAddress	= packetReader.readIPAddress();
		temp 		= packetReader.readShort();
		rport		= temp & 0xffff;
		return 0;
	}

	
	/**
	 * @return Returns the ipAddress.
	 */
	public IpAddress getIpAddress() {
		return ipAddress;
	}
	/**
	 * @return Returns the macAddress.
	 */
	public MacAddress getMacAddress() {
		return macAddress;
	}
	/**
	 * @return Returns the rport.
	 */
	public int getRport() {
		return rport;
	}

	public short getRegistered() {
		return registered;
	}

}
