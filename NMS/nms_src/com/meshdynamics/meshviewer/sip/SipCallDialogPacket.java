/*
 * Created on Mar 13, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.sip;

import com.meshdynamics.meshviewer.sip.ISipEventListener.INewCallDlgInfo;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.MacAddress;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SipCallDialogPacket extends SipPacket implements INewCallDlgInfo {
	
	private long 		dlgId;
	private MacAddress 	callerMacAddress;
	private MacAddress 	calleeMacAddress;
	
	/**
	 * 
	 */
	public SipCallDialogPacket(String meshId, MacAddress senderMacAddress) {
		super(meshId, senderMacAddress);
		this.packetType			= SIP_PACKET_TYPE_NEW_DIALOG;
		this.calleeMacAddress	= null;
		this.callerMacAddress	= null;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.sip.SipPacket#readPacket(byte[])
	 */
	public int readPacket(byte[] buffer) {
		BufferReader packetReader = new BufferReader(buffer, buffer.length);
		
		/*
		 * skip one byte packet type
		 */
		packetReader.readByte();
		
		long tempLng 		= packetReader.readInt();
		dlgId 				= tempLng & 0xffffffff;
		callerMacAddress	= packetReader.readMacAddress();
		calleeMacAddress 	= packetReader.readMacAddress();
		return 0;
	}

	/**
	 * @return Returns the calleeMacAddress.
	 */
	public MacAddress getCalleeMacAddress() {
		return calleeMacAddress;
	}
	/**
	 * @return Returns the callerMacAddress.
	 */
	public MacAddress getCallerMacAddress() {
		return callerMacAddress;
	}
	/**
	 * @return Returns the dlgId.
	 */
	public long getDlgId() {
		return dlgId;
	}
}
