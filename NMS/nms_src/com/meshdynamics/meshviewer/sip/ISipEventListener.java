package com.meshdynamics.meshviewer.sip;

import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

public interface ISipEventListener {

	public static final int SIP_EVENT_STA_REGISTRATION 	= 1;
	public static final int SIP_EVENT_NEW_CALL_DLG 		= 2;
	public static final int SIP_EVENT_CALL_DLG_STATUS 	= 3;
	
	public interface ISipEventInfo {
		public String 		getMeshId();
		public MacAddress 	getSenderMacAddress();
	}
	
	public interface IStaRegistrationInfo extends ISipEventInfo {
		public MacAddress 	getMacAddress();
		public IpAddress 	getIpAddress();
		public short 		getRegistered();
		public int 			getRport();
	}
	
	public interface INewCallDlgInfo extends ISipEventInfo {
		public long 		getDlgId();
		public MacAddress 	getCalleeMacAddress();
		public MacAddress 	getCallerMacAddress();
	}

	public interface ICallDlgStatusInfo extends ISipEventInfo {
		
		public static final int PENDING_STATUS_FOR_NONE			= 0;
		public static final int PENDING_STATUS_FOR_CALLER		= 1;
		public static final int PENDING_STATUS_FOR_CALLEE		= 2;
		public static final int PENDING_STATUS_FOR_CLEANUP		= 3;
		
		public long 		getDlgId();
		public short 		getPendingStatus();
		
		public MacAddress 	getCallerMacAddress();
		public IpAddress 	getCallerIpAddress();
		public int 			getCallerRport();
		public short 		getCallerCallState();
		
		public MacAddress 	getCalleeMacAddress();
		public IpAddress 	getCalleeIpAddress();
		public int 			getCalleeRport();
		public short		getCalleeCallState();
	}
	
	/*
	 * 	dataSource will be as follows
	 *  event = SIP_EVENT_STA_REGISTRATION, eventInfo = IStaRegistrationInfo,
	 *  event = SIP_EVENT_NEW_CALL_DLG, eventInfo = INewCallDlgInfo,
	 *  event = SIP_EVENT_CALL_DLG_STATUS, eventInfo = ICallDlgStatusInfo, 
	 */
	public void sipEvent(int event, ISipEventInfo eventInfo);
	
}
