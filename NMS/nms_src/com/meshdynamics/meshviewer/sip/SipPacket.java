/*
 * Created on Mar 13, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.sip;

import com.meshdynamics.meshviewer.sip.ISipEventListener.ISipEventInfo;
import com.meshdynamics.util.MacAddress;


/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public abstract class SipPacket implements ISipEventInfo {
	
	public static final short	SIP_PACKET_TYPE_STA_REGISTRATION	= 1;
	public static final short	SIP_PACKET_TYPE_NEW_DIALOG			= 2;
	public static final short	SIP_PACKET_TYPE_DIALOG_STATUS		= 3;
	
	protected short			packetType;
	protected MacAddress	senderAddress;
	protected String 		meshId;
	
	/**
	 * 
	 */
	public SipPacket(String meshId, MacAddress senderMacAddress) {
		super();
		this.meshId			= meshId;			
		this.senderAddress = senderMacAddress;
	}

	
	/**
	 * @return Returns the packetType.
	 */
	public short getPacketType() {
		return packetType;
	}
	
	public MacAddress getSenderMacAddress() {
		return senderAddress;
	}
	
	abstract public int readPacket(byte[] buffer);

	public String getMeshId() {
		return meshId;
	}
}
