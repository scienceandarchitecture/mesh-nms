/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshViewer.java
 * Comments : 
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  35 |Sep 06, 2007 | Restructuring									 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  34 |Jul 23, 2007 | createMeshNetwork modified for addind a network |Imran   |
 * --------------------------------------------------------------------------------
 * |  33 |Apr 13, 2007 | MeshviewerProperties changes					 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  32 |Apr 12,2007  | auto open network logic fixed					 |Abhijit |
 * --------------------------------------------------------------------------------	 
 * |  31 |Mar 30,2007  | setTopologySelection added						 |Abhishek|
 * --------------------------------------------------------------------------------	 
 * |  30 |Mar 26,2007  | Display messages modified				 	 	 |Bindu   |
 * --------------------------------------------------------------------------------
 * |  29 |Mar 21,2007  | Viewer start logic modified				 	 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  28 |Mar 16,2007  |  onClose call repositioned removeMeshNetwork 	 | Imran  |
 * --------------------------------------------------------------------------------
 * |  27 |Feb 27,2007  |  removed unused code							 |Abhijit |
 *---------------------------------------------------------------------------------
 * |  26 |Feb 13,2007  |  HealthMonitor changes                          |Abhijit |
 *---------------------------------------------------------------------------------
 * |  25 | Nov 27,2006 | Autostart for IMCP/SMGP added/misc save changes | Bindu  |
 * --------------------------------------------------------------------------------
 * |  24 |Oct 17, 2006 |  Network closing dialog title changed           | Imran  |
 * --------------------------------------------------------------------------------
 * |  23 |Aug 09, 2006 | SMG Connection changed to TCP					 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  22 |Jun 30, 2006 | Node name added to Macro Action table			 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  21 |Jun 15, 2006 | RF Space Packet logger added   			     | Bindu  |
 * --------------------------------------------------------------------------------
 * |  20 |Jun 12, 2006 | All config req sent when viewer re-started      | Bindu  |
 * --------------------------------------------------------------------------------
 * |  19 |Apr 18, 2006 | Node Added in Macro Action if present in network| Mithil |
 * --------------------------------------------------------------------------------
 * |  18 |Apr 13, 2006 | Added Updates folder						     | Bindu  |
 * --------------------------------------------------------------------------------
 * |  17 |Mar 27, 2006 | Save and Close and file closing			     | Mithil |
 * --------------------------------------------------------------------------------
 * |  16 |Mar 17, 2006 | Version in msg box generalised				     | Mithil |
 * --------------------------------------------------------------------------------
 * |  15 |Mar 01, 2006 | when closed with map view and opened map view   | Mithil | 
 * |					shown but topology cheked		 						  |
 *  -------------------------------------------------------------------------------
 * |  14 |Feb 23, 2006 | Folder existence checked before starting		 | Mithil |
 *  -------------------------------------------------------------------------------
 * |  13 |Jan 30, 2006 | Restructuring									 | Mithil |
 *  -------------------------------------------------------------------------------
 * |  12 |Jan 11, 2006 | HW Info sent after mesh is started and stopped  | Mithil |
 * --------------------------------------------------------------------------------
 * |  11 |Jan 05, 2006 | The Run Macro dialog should have things 	     | Mithil |
 * |			 		   disabled when network is not running					  |
 * --------------------------------------------------------------------------------
 * |  10 | Jan 04,2006 | Donot stop viewer after save dialog 			 | Mithil |
 * --------------------------------------------------------------------------------
 * |  9  | Oct 24,2005 | Cleanup removed unused variables				 | Mithil |
 * --------------------------------------------------------------------------------
 * |  8  | Oct 20,2005 | Save,open and remove network done				 | Amit   |
 * --------------------------------------------------------------------------------
 * |  7  |Oct 20,2005  | Messagebox changes                              | Sneha  |
 * --------------------------------------------------------------------------------
 * |  6  |Oct 18,2005  |  exitMeshViewer method updated for menu exit    | Sneha  |
 * --------------------------------------------------------------------------------
 * |  5  |Oct 5,2005   | SaveMesh network done                           | Amit   |
 * -------------------------------------------------------------------------------- 
 * |  4  |Sep 25, 2005 | LedSetup load/save removed					     | Abhijit|
 * --------------------------------------------------------------------------------
 * |  3  |Sep 2, 2005  | Network load/save ordering changed			     | Abhijit|
 * --------------------------------------------------------------------------------
 * |  2  |Aug 25, 2005 | Default network deletion restricted			 | Abhijit|
 * --------------------------------------------------------------------------------
 * |  1  |May 05, 2005 | checkConfigUpdates() added for Polling			 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer;


import java.util.Enumeration;

import com.meshdynamics.meshviewer.mesh.MeshController;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.PacketSender;
import com.meshdynamics.meshviewer.sip.ISipEventListener;

public class MeshViewer {
	
	public static final int			TYPE_NONE 		= 0;
	public static final int			TYPE_DATABASE	= 1;
	public static final int			TYPE_CSV 		= 2;
	
    private MeshController		 	meshController;
    private IMeshViewerListener		meshViewerListener;
    private boolean 				isRunning;
    private static MeshViewer       singleton = null;
    
    public static MeshViewer getInstance() {
        
        if(singleton == null) {
            singleton = new MeshViewer();
        }
        
        return singleton;
    }
    
    private MeshViewer() {
        
        meshController 			= new MeshController(this);
        meshController.initialize();
        
    }

    public void startViewer() {
    	if(isRunning == false) {
    		if(meshController.startViewer()) {
    	        isRunning = true;
    		}
    	}
    	
		if(isRunning == true) {
			if(meshViewerListener != null)
				meshViewerListener.viewerStarted();
		}
    }

    public void stopViewer() {
    	if(isRunning) {
			if(meshController.stopViewer()) {
				isRunning = false;
				stopMGClient();
			}
    	}

    	if(isRunning == false) {
			if(meshViewerListener != null)
				meshViewerListener.viewerStopped();	
    	}
    }

    public int startMGClient(final int       mode,
    						 final String    server,
    						 final int       port,
    						 final boolean   useSSL,
    						 final String    userName,
                             final String    password,
                             final boolean   ignoreLocalPackets) {

    	if(isRunning == false)
    		return 1;

    	if(meshController.isMgClientRunning() == true) {
	        if(meshViewerListener != null)
	        	meshViewerListener.meshGatewayStarted();
    		return 2;
    	}
    	
    	new Thread(new Runnable() {

			@Override
			public void run() {
		        meshController.startMGClient(mode, server, port, useSSL, userName, password, ignoreLocalPackets);
		        if(meshViewerListener != null)
		        	meshViewerListener.meshGatewayStarted();
			}
		}).start();
    	
        
        return 0;
    }


    public int stopMGClient() {
        int ret = meshController.stopMGClient();
        if(ret != 0)
        	return ret;
        
        if(meshViewerListener != null)
        	meshViewerListener.meshGatewayStopped();
        
        return ret;
    }

    /**
     * 
     */
    public boolean exitMeshViewer() {
    	/**
    	 * 1) MeshVewer stop if running 
    	 * 2) MeshVIewer MeshNetwork dirty checking
    	 * 3) show do u want to save msg if dirty
    	 * 4) save networks
    	 * 5) meshProperties.write (data.dat) for mesh viewer properties
    	 * 1) meshViewerUI.dispose if 
    	 */
        
    	stopViewer();
        meshController.unInitialize();
		
        return true;
    }

	/**
	 * @return Returns the isRunning.
	 */
	public boolean isRunning() {
		return isRunning;
	}
	
	public PacketSender getPacketSender() {
		return meshController.getPacketSender();
	}
	
	public Enumeration<MeshNetwork> getMeshNetworks() {
		return meshController.getMeshNetworks();
	}
	
	public MeshNetwork getMeshNetworkByName(String networkName) {
		return meshController.getMeshNetwork(networkName);
	}

	public String getMeshEncKey(String dstNetworkName) {
		
		Enumeration<MeshNetwork> enumMeshNet	= getMeshNetworks();
		while(enumMeshNet.hasMoreElements()) {
			MeshNetwork meshnet	= (MeshNetwork)enumMeshNet.nextElement();
			if(meshnet == null) {
				continue;
			}
			if(meshnet.getNwName().equals(dstNetworkName) == true) {
				return meshnet.getKey();
			}
		}
		return null;
	}
	
	/**
	 * @param dstMeshEncKey
	 */
	public short getMeshNetworkType(String dstNetworkName) {

		Enumeration<MeshNetwork> enumMeshNet	= getMeshNetworks();
		while(enumMeshNet.hasMoreElements()) {
			MeshNetwork meshnet	= (MeshNetwork)enumMeshNet.nextElement();
			if(meshnet == null) {
				continue;
			}
			if(meshnet.getNwName().equals(dstNetworkName) == true) {
				return meshnet.getNetworkType();
			}
		}
		return MeshNetwork.NETWORK_REGULAR;
	}

	/**
	 * @param network
	 * @return
	 */
	public boolean closeMeshNetwork(String network) {
		return meshController.closeMeshNetwork(network);
	}
	
	/**
	 * @param networkName
	 * @param networkKey
	 * @param newtworkType
	 */
	public MeshNetwork createMeshNetwork(String networkName, String networkKey, byte networkType) {
		return meshController.createMeshNetwork(networkName,networkKey, networkType);
	}

	public void setMeshViewerListener(IMeshViewerListener listener) {
		this.meshViewerListener = listener; 
	}

	public void addSipEventListener(ISipEventListener listener) {
		meshController.addSipEventListener(listener);
	}

	public void removeSipEventListener(ISipEventListener listener) {
		meshController.removeSipEventListener(listener);
		
	}

}
