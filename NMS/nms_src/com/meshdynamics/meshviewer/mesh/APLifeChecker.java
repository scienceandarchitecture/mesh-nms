/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : APLifeChecker.java
 * Comments : 
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -------------------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                       		 | Author 		|
 *  ---------------------------------------------------------------------------------------------
 * |  1  |Feb 12, 2007 | notifications about AP status given to health monitor 	 |Imran/Abhishek |
 * ----------------------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         		 | Anand  		|
 * ----------------------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh;

import java.util.Enumeration;

public class APLifeChecker implements Runnable{
	boolean dead;
	MeshController meshController;
	
	public APLifeChecker(MeshController meshController) {
		super();
		dead = true;
		this.meshController = meshController;
	}

	public void run() {

		int	sleepInterval 	= 5 * 1000;
		dead = false;
		while (!dead) {

			try {
				Thread.sleep(sleepInterval);
			} catch (InterruptedException e) {
				if(dead == true) {
					System.out.println("AP Lifechecker stopped.");
					break;
				}
			}

			Enumeration<MeshNetwork> eNetworks = meshController.getMeshNetworks();

			while(eNetworks.hasMoreElements()) {
			    final MeshNetwork meshNetwork = (MeshNetwork)eNetworks.nextElement();
			    meshNetwork.checkApLife();
				if(dead) {
				    break;
				}
			}
			Runtime.getRuntime().gc();
		}
	}

    /**
     * 
     */
    public void stop() {
        dead = true;
    }

}
