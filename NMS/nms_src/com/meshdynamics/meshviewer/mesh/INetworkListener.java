package com.meshdynamics.meshviewer.mesh;

import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;


public interface INetworkListener {

	public void notifyHealthAlert		(final int healthStatus, final String meshId);
	public void accessPointDeleted		(final AccessPoint accessPoint);
    public void accessPointAdded		(final AccessPoint accessPoint);
    public void meshViewerStopped		();
    public void meshViewerStarted		();
    public void saturationInfoReceived	(final AccessPoint ap, String ifName);
    
}
