/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : HeartbeatInfo.java
 * Comments : 
 * Created  : Oct 10, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  6  |Oct 10, 2006 | getMeshType function added			             |Abhijit |
 * --------------------------------------------------------------------------------
 * |  5  |Oct 06, 2006 | signal added to line				             |Prachiti|
 * --------------------------------------------------------------------------------
 * |  4  |May 16, 2006 | getBitRateFromKnownAP				             |Prachiti|
 * * ------------------------------------------------------------------------------
 * |  3  |Sep 21, 2005 | GetDSMacadress added		 					 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  2  |Sep 08, 2005 | getStaEntries return type changed to StaEntry[] | Abhijit|
 * --------------------------------------------------------------------------------
 * |  1  |Mar 09, 2005 | Board Temp added to Heartbeat                   | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 10, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh;

import com.meshdynamics.meshviewer.configuration.KnownAp;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.util.StaEntry;
import com.meshdynamics.util.MacAddress;

public interface IHeartbeatInfo {
	
	public 	static final int MESH_TYPE_INFRA		= 0;
	public 	static final int MESH_TYPE_HYBRID		= 1;

	/**
     * @return Returns the apCount.
     */
    public short getApCount();

    /**
     * @return Returns the cumulativeRSSI.
     */
    public long getCumulativeRSSI();

    /**
     * @return Returns the cumulativeTollCost.
     */
    public long getCumulativeTollCost();

    /**
     * @return Returns the healthIndex.
     */
    public short getHealthIndex();

    /**
     * @return Returns the heartbeatInterval.
     */
    public short getHeartbeatInterval();

    /**
     * @return Returns the hopCount.
     */
    public short getHopCount();

    /**
     * @return Returns the knownAPs.
     */
    public KnownAp[] getKnownAPs();

    /**
     * @return Returns the parentBssid.
     */
    public MacAddress getParentBssid();

    /**
     * @return Returns the rootBssid.
     */
    public MacAddress getRootBssid();

    /**
     * @return Returns the sequenceNumber.
     */
    public long getSequenceNumber();

    /**
     * @return Returns the staCount.
     */
    public short getStaCount();

    /**
     * @return Returns the staEntries.
     */
    public StaEntry[] getStaEntries();

    /**
     * @return Returns the txRate.
     */
    public long getBitRate();

    /**
     * @return
     */
    public long getSignalfromParent();

    /**
     * @return
     */
    public long getByteSent();

    /**
     * @return
     */
    public long getPacketSent();

    /**
     * @return String
     */    
    public String getMeshId();
    
	public AccessPoint getParentAP();
	
	public double getBoardTempareture();
	
	public int getMeshType();
	
	public long getRxBytes();
	
	public long getRxPackets();
	
	public long getLongitude();
    public long getLatitude();
    public int 	getAltitude();
    public int 	getSpeed();
    
}
