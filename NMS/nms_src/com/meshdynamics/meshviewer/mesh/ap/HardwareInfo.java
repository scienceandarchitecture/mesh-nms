/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : HardwareInfo.java
 * Comments : 
 * Created  : Oct 10, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |Apr 27, 2005 | Hardware Model info added                       | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 10, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh.ap;

import java.io.InputStream;
import java.io.OutputStream;

import com.meshdynamics.util.MacAddress;

public interface HardwareInfo {
	public class InterfaceData{
		
		private String 		name;
		private MacAddress 	macAddr;
		private int			ifType;
		private int			ifSubType;
		private int 		channel;

		
		/**
         * @return Returns the channel.
         */
        public int getChannel() {
            return channel;
        }
        /**
         * @param channel The channel to set.
         */
        public void setChannel(int channel) {
            this.channel = channel;
        }
        
        /**
         * @return Returns the ifType.
         */
        public int getIfType() {
            return ifType;
        }
        /**
         * @param ifType The ifType to set.
         */
        public void setIfType(int ifType) {
            this.ifType = ifType;
        }
			
		/**
		 * @return Returns the name.
		 */
		public String getName() {
			return name;
		}
		/**
		 * @param name The name to set.
		 */
		public void setName(String name) {
			this.name = name;
		}
		/**
		 * @return Returns the wmMacAddr.
		 */
		public MacAddress getMacAddr() {
			return macAddr;
		}
		/**
		 * @param wmMacAddr The wmMacAddr to set.
		 */
		public void setWmMacAddr(MacAddress macAddr) {
			this.macAddr = macAddr;
		}
		/**
		 * @return Returns the ifSubType.
		 */
		public int getIfSubType() {
			return ifSubType;
		}
		/**
		 * @param ifSubType The ifSubType to set.
		 */
		public void setIfSubType(int ifSubType) {
			this.ifSubType = ifSubType;
		}
	}

	public String getVersionStr();
    public String getApName();
	public void setApName(String name);
	public void setMeshId(String meshId);
	public InterfaceData getDsInterfaceInfo();
	
	public short getWmCount();
	public InterfaceData[] getWmInterfaceInfo();  
	
	public String getHardwareModel();
	 public void read(InputStream in);
	 public void write(OutputStream os);
}
