/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApRuntimeConfigurationManager.java
 * Comments : 
 * Created  : May 18, 2007
 * Author   : Abhshek
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |Jun 19, 2007  | RuntimeConfigStatus added                       |Abhishek|
 * --------------------------------------------------------------------------------
 * |  0  |May 18, 2007  | Created                                         |Abhishek|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh.ap;

import java.util.Enumeration;

import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.configuration.IStaConfiguration;
import com.meshdynamics.meshviewer.configuration.KnownAp;
import com.meshdynamics.meshviewer.configuration.runtimeconfiguration.IRuntimeConfigStatus;
import com.meshdynamics.meshviewer.configuration.runtimeconfiguration.RuntimeConfigStatus;
import com.meshdynamics.meshviewer.configuration.runtimeconfiguration.RuntimeConfiguration;
import com.meshdynamics.meshviewer.imcppackets.HeartBeat2Packet;
import com.meshdynamics.meshviewer.imcppackets.HeartbeatPacket;
import com.meshdynamics.meshviewer.imcppackets.StaAssocPacket;
import com.meshdynamics.meshviewer.imcppackets.StaDisassocPacket;
import com.meshdynamics.meshviewer.imcppackets.StaInfoPacket;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.util.StaSignalEntry;
import com.meshdynamics.util.MacAddress;

public class ApRuntimeConfigurationManager {

	private RuntimeConfiguration 	configuration;
	private ApDataController 		dataManager;
	private IRuntimeConfigListener 	notifier;
	private boolean 				loggingEnabled;
	private boolean 				clientActivityEnabled;
	private RuntimeConfigStatus 	runtimeStatus;
	private MeshNetwork 			meshNetwork; 
	
	ApRuntimeConfigurationManager(ApDataController dataManager,
			IRuntimeConfigListener notifier,MeshNetwork meshNetwork) {

		configuration = new RuntimeConfiguration(this, dataManager
				.getVersionInfo());
		this.dataManager 	  = dataManager;
		this.notifier 		  = notifier;
		this.meshNetwork	  = meshNetwork;		
		loggingEnabled 		  = false;
		clientActivityEnabled = false;
		runtimeStatus		  = new RuntimeConfigStatus(dataManager);	 
	}

	public void setKnownAPNodeName(KnownAp kap) {
		
		if(kap == null) return;
		
		AccessPoint ap = meshNetwork.getAccessPointFromBssid(kap.macAddress);
		
		if(ap == null) return;
			
		IConfiguration conf = ap.getConfiguration();
		
		if(conf == null) return;
		
		String name = conf.getNodeName();
		
		if(name == null) return;
		
		kap.nodeName = name;
		
	}
	
	public void setMeshClientNodeName(IStaConfiguration staInfo) {
		
		if(staInfo == null) return;
		
		MacAddress staMac =  staInfo.getStaMacAddress();
		
		if(staMac == null) return;
		
		AccessPoint ap = meshNetwork.getAccessPointFromBssid(staMac);
		
		if(ap == null) return;
		
		IConfiguration conf = ap.getConfiguration();
		
		if(conf == null) return;
		
		String name = conf.getNodeName();
		
		if(name == null) return;
		
		staInfo.setName(name);
		
	}
	
	
	private boolean isRepeatedHeartbeat(long oldSequenceNo, long newSequenceNo) {

		if (oldSequenceNo >= newSequenceNo) {
			if (oldSequenceNo - newSequenceNo < 10) {
				return true;
			}
		}
		return false;
	}

	void setHeartBeat2Info(HeartBeat2Packet newHeartBeat2Info) {

		if (newHeartBeat2Info == null) {
			return;
		}
		if(configuration.isRebootRequired() == false){
			//Resetting the flag in runtime Config status
			runtimeStatus.setRfUpdateSent(false);
			runtimeStatus.setCountryCodeChanged(false);
		}
		if (isRepeatedHeartbeat(configuration.getHeartbeat2SequenceNumber(),
				newHeartBeat2Info.getSequenceNumber())) {
			return;
		}

		if (configuration.getConfigSeqNumber() < newHeartBeat2Info
				.getConfigSeqNumber()) {
			//probably board configuration has been updated
			dataManager.requestMissingConfiguration(true);
		}

		if (isStaActivityEnabled() == true) {
			StaSignalEntry[] staEntries = newHeartBeat2Info
					.getStaSignalEntries();
			if (staEntries != null) {
				for (int i = 0; i < newHeartBeat2Info.getStaCount(); i++) {
					dataManager.requestStaInfo(staEntries[i].macAddress);
				}
			}
		}
		configuration.setHeartBeat2Info(newHeartBeat2Info);
	}

	void setHeartBeatInfo(HeartbeatPacket newHeartBeatInfo) {
		
		if (configuration.is1stHeartbeat() == true) {
			//this is first heartbeat
			configuration.setHeartBeatInfo(newHeartBeatInfo);
			notifier.notify(IRuntimeConfigListener.FIRST_HEARTBEAT);
		} else {
			if (isRepeatedHeartbeat(
					configuration.getHeartbeat1SequenceNumber(),
					newHeartBeatInfo.getSequenceNumber())) {
				return;
			}

			notifier.notify(IRuntimeConfigListener.HEARTBEAT);
			
			if (configuration.getParentBssid().equals(
					newHeartBeatInfo.getParentBssid()) == false) {
				//parent shifted
				notifier.notify(IRuntimeConfigListener.PARENT_CHANGED);
			} else {
				notifier.notify(IRuntimeConfigListener.REFRESH_CONNECTION);
			}
		}
		
		configuration.setHeartBeatInfo(newHeartBeatInfo);
		dataManager.requestMissingConfiguration(false);
	}

	boolean isCapable(int packetType) {
		return configuration.isCapable(packetType);
	}

	boolean isCmdCapable(int comType) {
		return configuration.isCmdCapable(comType);
	}

	boolean isConfigCapable(int comType) {
		return configuration.isConfigCapable(comType);
	}

	boolean hasClient(MacAddress staAddress) {
		return configuration.hasClient(staAddress);
	}

	int getVoltage() {
		return configuration.getVoltage();
	}

	int getBoardFreeMemory() {
		return configuration.getBoardFreeMemory();
	}
	
	int getBoardCPUUsage() {
		return configuration.getBoardCPUUsage();
	}

	MacAddress getParentBSSID() {
		return configuration.getParentBssid();
	}

	boolean isStaActivityEnabled() {
		return clientActivityEnabled;
	}

	boolean isLoggingEnabled() {
		return loggingEnabled;
	}

	boolean isRebootRequired() {
		return configuration.isRebootRequired();
	}

	boolean isRadarDetected() {
		return configuration.isRadarDetected();
	}


	boolean isFIPSDetected() {
		return configuration.isFIPSDetected();
	}

	void setLoggingEnabled(boolean enable) {
		this.loggingEnabled = enable;
	}

	void setStaClientActivityEnabled(boolean enable) {
		this.clientActivityEnabled = enable;
	}

	void setStaAssocNotification(StaAssocPacket staAssocInfo) {
		configuration.setStaAssocInfo(staAssocInfo);
	}

	void setStaDissocNotification(StaDisassocPacket staDissocInfo) {
		configuration.setDissocInfo(staDissocInfo);
	}

	void setStaInfoPacket(StaInfoPacket staInfo) {
		configuration.setStaInfo(staInfo);
	}

	long getlastHeartbeatTime() {
		return configuration.getLastHeartbeatTime();
	}

	Enumeration<IStaConfiguration> getStaList() {
		return configuration.getStaList();

	}

	IStaConfiguration getStaInfo(MacAddress staAddress) {
		return configuration.getStaInformation(staAddress);
	}

	public MacAddress getParentDSMacAddress(MacAddress parentWMMacAdddress) {
		return dataManager.getApDsMacAddress(parentWMMacAdddress);
	}

	void clearData() {
		configuration.clearData();
	}
	
	void clearStatus(){
		runtimeStatus.clear();
	}
	
	public IRuntimeConfiguration getRuntimeConfiguration() {
		return configuration;
	}

	public boolean isStaIMCP(MacAddress address) {
		return (dataManager.getAccessPoint(address) == null) ? false : true;
	}
	
	IRuntimeConfigStatus getRuntimeConfigStatus() {
		return runtimeStatus;
	}

	void setRebootRequired(boolean enable) {
		configuration.setRebootRequired(enable);
	}

	void setRfUpdateSent(boolean b) {

        /**
         *  Notify runtime manager about RF update
         *  we have to set reboot required flag implicitly.
         *  and to set rfupdatesent flag in runtime config. 
         */
		runtimeStatus.setRfUpdateSent(true);
		setRebootRequired(true);
	}

	void setCountryCodeChanged(boolean enable) {
		runtimeStatus.setCountryCodeChanged(enable);
		setRebootRequired(true);
	}
}