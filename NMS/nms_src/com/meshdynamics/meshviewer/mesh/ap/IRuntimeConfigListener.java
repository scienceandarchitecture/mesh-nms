/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : IRuntimeConfigListener.java
 * Comments : 
 * Created  : May 8, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |May 18, 2007  | Created                                         |Abhishek|
 * --------------------------------------------------------------------------------
 **********************************************************************************/
package com.meshdynamics.meshviewer.mesh.ap;

interface IRuntimeConfigListener {

	public static final	short PARENT_CHANGED			= 0;
	public static final	short REFRESH_CONNECTION		= 1;
	public static final	short FIRST_HEARTBEAT			= 2;
	public static final	short HEARTBEAT					= 3;
	
	public void notify(int notification);
	
	
}
