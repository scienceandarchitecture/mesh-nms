/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApUpdateManager.java
 * Comments : 
 * Created  : Apr 11, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *   --------------------------------------------------------------------------------
 * | 11  |Oct 30,2007  | fillApConfigInfo modified                     	 |Abhishek |
 *  --------------------------------------------------------------------------------
 * | 10  |Aug 14,2007  | Status Reboot Reqd added on response ercvd 	|Abhishek |
 *  --------------------------------------------------------------------------------
 * |  9  |Jul 24,2007  | UpdateConfiguration is added           	 	 |Abhishek |
 *  --------------------------------------------------------------------------------
 * |  8  |Jun 12, 2007 | config responses added							 | Abhishek|
 * --------------------------------------------------------------------------------
 * |  7  |Jun 11, 2007 | updateAllConfiguration method implemented       | Imran  |
 *   --------------------------------------------------------------------------------
 * |  6  |Jun 07, 2007 | meshId set to RebootRequestpacket 				 | Imran  |
 *  --------------------------------------------------------------------------------
 * |  5  |May 30, 2007 | updateChangedConfig modified for reboot request | Imran  |
 *  --------------------------------------------------------------------------------
 * |  4  |May 09, 2007 | vlanCount incremanted in packet for default vlan| Imran  |
 *  --------------------------------------------------------------------------------
 * |  3  |May 07, 2007 | updateChangedConfiguration modified for reboot  | Imran  |
 * |	 |			   | request										 |        |
 * --------------------------------------------------------------------------------
 * |  2  |May 02, 2007 | sysouts added									 | Abhishek|
 * --------------------------------------------------------------------------------
 * |  1  |Apr 27, 2007 | fillpktinfo for all packets implemented         | Abhishek|
 * --------------------------------------------------------------------------------
 * |  0  |Apr 11, 2007 | Created                                         | Abhijit |
 * --------------------------------------------------------------------------------
 **********************************************************************************/


package com.meshdynamics.meshviewer.mesh.ap;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;

import com.meshdynamics.meshviewer.configuration.I80211eCategoryConfiguration;
import com.meshdynamics.meshviewer.configuration.I80211eCategoryInfo;
import com.meshdynamics.meshviewer.configuration.IACLConfiguration;
import com.meshdynamics.meshviewer.configuration.IACLInfo;
import com.meshdynamics.meshviewer.configuration.IChannelInfo;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IEffistreamAction;
import com.meshdynamics.meshviewer.configuration.IEffistreamConfiguration;
import com.meshdynamics.meshviewer.configuration.IEffistreamRule;
import com.meshdynamics.meshviewer.configuration.IEffistreamRuleCriteria;
import com.meshdynamics.meshviewer.configuration.IInterfaceChannelConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;
import com.meshdynamics.meshviewer.configuration.INetworkConfiguration;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipStaInfo;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.meshviewer.configuration.IWEPConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.Configuration;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceAdvanceConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceCustomChannelConfig;
import com.meshdynamics.meshviewer.configuration.impl.PSKConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.RadiusConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationTextReader;
import com.meshdynamics.meshviewer.imcppackets.ACLInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApAckTimeoutInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApAckTimeoutRequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.ApConfigInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApConfigRequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.ApHwInfoRequestPacket;
import com.meshdynamics.meshviewer.imcppackets.ApIfChannelInfoRequestPacket;
import com.meshdynamics.meshviewer.imcppackets.ApIpConfigInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApRegDomainInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApRegDomainRequestPacket;
import com.meshdynamics.meshviewer.imcppackets.ApResetPacket;
import com.meshdynamics.meshviewer.imcppackets.ApRestoreDefaultsRequestPacket;
import com.meshdynamics.meshviewer.imcppackets.ConfigRequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.Dot11eCategoryPacket;
import com.meshdynamics.meshviewer.imcppackets.Dot11eIfInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.EffistreamConfigPacket;
import com.meshdynamics.meshviewer.imcppackets.EffistreamConfigPacket.ActionInfo;
import com.meshdynamics.meshviewer.imcppackets.EffistreamConfigPacket.RuleInfo;
import com.meshdynamics.meshviewer.imcppackets.EffistreamRequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.Firmware_SW_UPDATE_REQUEST;
import com.meshdynamics.meshviewer.imcppackets.Firmware_SW_UPDATE_RESPONSE;
import com.meshdynamics.meshviewer.imcppackets.GenericCommandRequestResponse;
import com.meshdynamics.meshviewer.imcppackets.IMCPPacket;
import com.meshdynamics.meshviewer.imcppackets.IfHiddenESSIDInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ImcpKeyUpdatePacket;
import com.meshdynamics.meshviewer.imcppackets.ImcpKeyUpdateResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.MacAddrInfoRequestPacket;
import com.meshdynamics.meshviewer.imcppackets.MgmtgwConfigRequestPacket;
import com.meshdynamics.meshviewer.imcppackets.MgmtgwResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.imcppackets.RFCustomChannelInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.RFCustomChannelInfoPacket.RFChannelInfo;
import com.meshdynamics.meshviewer.imcppackets.RFCustomChannelInfoRequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.RebootRequestPacket;
import com.meshdynamics.meshviewer.imcppackets.RequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.SipConfigPacket;
import com.meshdynamics.meshviewer.imcppackets.StaInfoRequestPacket;
import com.meshdynamics.meshviewer.imcppackets.VLanConfigInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.helpers.ACLInfoDetails;
import com.meshdynamics.meshviewer.imcppackets.helpers.Dot11eCategoryDetails;
import com.meshdynamics.meshviewer.imcppackets.helpers.Dot11eIfInfoDetails;
import com.meshdynamics.meshviewer.imcppackets.helpers.EffistreamHelper;
import com.meshdynamics.meshviewer.imcppackets.helpers.IMCPInfoIDS;
import com.meshdynamics.meshviewer.imcppackets.helpers.InterfaceAdvanceInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.InterfaceConfigInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.RADSecurityInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.RSNPSKSecurityInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.SecurityInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.SipStaInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.VLanConfigInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.WEPSecurityInfo;
import com.meshdynamics.meshviewer.mesh.IMessageHandler;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.PacketSender;
import com.meshdynamics.nmsui.util.MFile;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;


public class ApUpdateManager  {

	private IConfiguration			configuration;
	private IMessageHandler			notificationHandler;
	private ConfigurationStatus		updateStatus;
	private ApDataController 		dataManager;
	
	private static final String AP_CONF_UPDATE		  			= "AP Configuration Update";
	private static final String IP_CONF_UPDATE					= "IP Configuration Update";
	private static final String VLAN_CONF_UPDATE			 	= "VLAN Configuration Update";
	private static final String	COUNTRYCODE_CONF_UPDATE			= "Country Code Configuration Update";
	private static final String	ACKTIMEOUT_CONF_UPDATE			= "Ack Timeout Configuration Update";
	private static final String	HIDDENESSID_CONF_UPDATE			= "Hidden ESSID Configuration Update";
	private static final String	_80211ECAT_CONF_UPDATE			= "802.11e Category Configuration Update";
	private static final String	_80211EIF_CONF_UPDATE			= "802.11e Interface Configuration Update";
	private static final String	ACL_CONF_UPDATE					= "ACL Configuration Update";
	private static final String	RF_CUSTOM_CHANNEL_CONF_UPDATE   = "RF Custom Channel Configuration Update";
	private static final String	EFFISTREAM_CONF_UPDATE   		= "Effistream Configuration Update";
	private static final String	IMCP_KEY_UPDATE   				= "IMCP Key Update";
	private static final String	SIP_CONF_UPDATE   				= "SIP Configuration Update";
	private static final String FIRMWARE_UPDATE                 = "FIRMWARE UPDATE";
	
	
	private static final String SUCCESSFUL		  				= "Successful";
	private static final String FAILURE                         = "Failure";
	private static int          successCount                    = 0;
	private static int          failCount                       = 0;
   
	ApUpdateManager(ApDataController dataManager,final IConfiguration configuration,IMessageHandler notificationHandler) {
		this.dataManager			= dataManager;
		this.configuration			= configuration;
		this.notificationHandler	= notificationHandler;
		this.updateStatus			= new ConfigurationStatus();
	}

	void mgmtgwResponseReceived(MgmtgwResponsePacket packet){
		int status=packet.getStatus();
		if(status == MgmtgwResponsePacket.mgmt_gw_enable){
		      notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_MGMT_GW,SUCCESSFUL,"");
		      isRebootRequired();
		      notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_MGMT_GW,Mesh.STATUS_REBOOTREQD,"" );
		      }else{
		    	  notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_MGMT_GW,FAILURE,"");
		      }
	}
	void ackTimeoutResponseReceived(ApAckTimeoutRequestResponsePacket packet) {
		
		if(updateStatus.isPacketChanged(PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET) == true) {
			System.out.println(successCount+" "+PacketFactory.AP_CONFIGURATION_INFO);
			updateStatus.clearPacketStatus(PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET);
			int totalUpdateCount		= updateStatus.getTotalStatusCount();		
			int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
			successCount++;
			notificationHandler.showMessage(dataManager.getMessageSource(), ACKTIMEOUT_CONF_UPDATE, 
					 ""+successCount+" "+SUCCESSFUL+
					  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
			if(totalUpdateCount ==completedUpdateCount){
				successCount=0;
				failCount=0;
			}
		}
	}

	void apConfigResponseReceived(ApConfigRequestResponsePacket packet) {
		
		if(updateStatus.isPacketChanged(PacketFactory.AP_CONFIGURATION_INFO) == true) {
			updateStatus.clearPacketStatus(PacketFactory.AP_CONFIGURATION_INFO);
			int totalUpdateCount		= updateStatus.getTotalStatusCount();		
			int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
			successCount++;
			notificationHandler.showMessage(dataManager.getMessageSource(), AP_CONF_UPDATE, 
											SUCCESSFUL,""+completedUpdateCount+ " of "+totalUpdateCount);
			if(totalUpdateCount == completedUpdateCount){
				successCount=0;
				failCount=0;
			}
			
		}
	}

	void regDomainResponseReceived(ApRegDomainRequestPacket packet) {
				
		if(updateStatus.isPacketChanged(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET) == true) {
			
				updateStatus.clearPacketStatus(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET);
				int totalUpdateCount		= updateStatus.getTotalStatusCount();		
				int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
				if(updateStatus.isRebootRequired() == true)
				isRebootRequired();
				successCount++;
				notificationHandler.showMessage(dataManager.getMessageSource(),COUNTRYCODE_CONF_UPDATE,
						 ""+successCount+" "+SUCCESSFUL+
						  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
				notificationHandler.showMessage(dataManager.getMessageSource(),COUNTRYCODE_CONF_UPDATE,
						Mesh.STATUS_REBOOTREQD,""+completedUpdateCount+ " of "+totalUpdateCount);
				if(totalUpdateCount ==completedUpdateCount){
					successCount=0;
					failCount=0;
				}
			}
	}

	synchronized void configResponseReceived(ConfigRequestResponsePacket packet) {
	
		/***
		 * Type =>
		 *  AP CONFIG
		 *  IP CONFIG
		 *  VLAN CONFIG
		 *  DOT11E CATEGORY INFO
		 *  DOT11E CONFIG INFO
		 *  INTERFACE HIDDEN ESSID PACKET
		 *  ACL CONFIG INFO  
		 */
		short cfgPacketType 	= packet.getConfigPktType();
		short cfgPacketType2 	= packet.getConfigPktType2();
		short errorCode         = packet.getErrorCode();
				
		if(updateStatus.isPacketChanged(PacketFactory.AP_CONFIGURATION_INFO) == true &&
			(cfgPacketType & ConfigRequestResponsePacket.AP_CONFIG_REQ_RES) != 0) {
			    updateStatus.clearPacketStatus(PacketFactory.AP_CONFIGURATION_INFO);
			   	int totalUpdateCount		= updateStatus.getTotalStatusCount();		
				int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
				if(errorCode == 0){
				successCount++;
				notificationHandler.showMessage(dataManager.getMessageSource(),AP_CONF_UPDATE, 
						""+successCount+" "+SUCCESSFUL+
						  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
				/**
				 * Reboot required might get set due to security change, manual channel change,protocol change or HT & VHT changes 
				 */
				if(updateStatus.isRebootRequired() == true) {
				    isRebootRequired();
					notificationHandler.showMessage(dataManager.getMessageSource(),AP_CONF_UPDATE, 
							Mesh.STATUS_REBOOTREQD,""+completedUpdateCount+ " of "+totalUpdateCount);
				}
				}else{
				  failCount++;
				  String errorMsg="";
				  updateStatus.isResponseStatus(true);
				  errorMsg=packet.getIfName()+"-"+packet.getParamName()+"-"+packet.getValueName();
				  notificationHandler.showMessage(dataManager.getMessageSource(),AP_CONF_UPDATE,""+successCount+" "+SUCCESSFUL+
						  " "+failCount+" "+FAILURE+" ("+errorMsg+")",""+completedUpdateCount+ " of "+totalUpdateCount);
				}
				if(totalUpdateCount == completedUpdateCount){
					successCount=0;
					failCount=0;
				}
		}
		if(updateStatus.isPacketChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO) == true &&
			(cfgPacketType & ConfigRequestResponsePacket.VLAN_CONFIG_REQ_RES) != 0) {
			
				updateStatus.clearPacketStatus(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
				int totalUpdateCount		= updateStatus.getTotalStatusCount();		
				int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
				if(errorCode == 0){
					successCount++;
					if(updateStatus.isRebootRequired() == true)
					  isRebootRequired();
					
				notificationHandler.showMessage(dataManager.getMessageSource(),VLAN_CONF_UPDATE, 
						""+successCount+" "+SUCCESSFUL+
						  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
				notificationHandler.showMessage(dataManager.getMessageSource(),VLAN_CONF_UPDATE, 
						Mesh.STATUS_REBOOTREQD,""+completedUpdateCount+ " of "+totalUpdateCount);
				}else{
					failCount++;
					 String errorMsg="";
					 errorMsg=packet.getIfName()+"-"+packet.getParamName()+"-"+packet.getValueName();
					 notificationHandler.showMessage(dataManager.getMessageSource(),AP_CONF_UPDATE,""+successCount+" "+SUCCESSFUL+
							  " "+failCount+" "+FAILURE+" ("+errorMsg+")",""+completedUpdateCount+ " of "+totalUpdateCount);
				}
				if(totalUpdateCount ==completedUpdateCount){
					successCount=0;
					failCount=0;
				}			}
		
		if(updateStatus.isPacketChanged(PacketFactory.ACL_INFO_PACKET) == true &&
			(cfgPacketType & ConfigRequestResponsePacket.ACL_CONFIG_REQ_RES) != 0) {
			
				updateStatus.clearPacketStatus(PacketFactory.ACL_INFO_PACKET);
				int totalUpdateCount		= updateStatus.getTotalStatusCount();		
				int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
				if(errorCode ==0){
					    successCount++;
				        notificationHandler.showMessage(dataManager.getMessageSource(),ACL_CONF_UPDATE, 
				        		""+successCount+" "+SUCCESSFUL+
								  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
				}else{
					failCount++;
					 String errorMsg="";
					  errorMsg=packet.getIfName()+"-"+packet.getParamName()+"-"+packet.getValueName();
					  notificationHandler.showMessage(dataManager.getMessageSource(),AP_CONF_UPDATE,""+successCount+" "+SUCCESSFUL+
							  " "+failCount+" "+FAILURE+" ("+errorMsg+")",""+completedUpdateCount+ " of "+totalUpdateCount);
				}
				if(totalUpdateCount ==completedUpdateCount){
					successCount=0;
					failCount=0;
				}
			}
		
		if(updateStatus.isPacketChanged(PacketFactory.AP_IP_CONFIGURATION_INFO) == true &&
			(cfgPacketType & ConfigRequestResponsePacket.IP_CONFIG_REQ_RES) != 0) {
			
				updateStatus.clearPacketStatus(PacketFactory.AP_IP_CONFIGURATION_INFO);
				int totalUpdateCount		= updateStatus.getTotalStatusCount();		
				int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
				if(errorCode ==0){
					successCount++;
				notificationHandler.showMessage(dataManager.getMessageSource(), IP_CONF_UPDATE, 
						""+successCount+" "+SUCCESSFUL+
						  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
				}else{
					   failCount++;
					   String errorMsg="";
					   errorMsg=packet.getIfName()+"-"+packet.getParamName()+"-"+packet.getValueName();
					   notificationHandler.showMessage(dataManager.getMessageSource(),AP_CONF_UPDATE,""+successCount+" "+SUCCESSFUL+
								  " "+failCount+" "+FAILURE+" ("+errorMsg+")",""+completedUpdateCount+ " of "+totalUpdateCount);
				}
				if(totalUpdateCount ==completedUpdateCount){
					successCount=0;
					failCount=0;
				}
			}
	
		if(updateStatus.isPacketChanged(PacketFactory.DOT11E_CATEGORY_INFO_PACKET) == true &&
			(cfgPacketType & ConfigRequestResponsePacket.DOT11E_CATEGORY_CONFIG_REQ_RES) != 0) {
			
				updateStatus.clearPacketStatus(PacketFactory.DOT11E_CATEGORY_INFO_PACKET);
				int totalUpdateCount		= updateStatus.getTotalStatusCount();		
				int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
				if(errorCode == 0){
					successCount++;
				notificationHandler.showMessage(dataManager.getMessageSource(),_80211ECAT_CONF_UPDATE, 
						 ""+successCount+" "+SUCCESSFUL+
						  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
				}else{
					failCount++;
					 String errorMsg="";
					  errorMsg=packet.getIfName()+"-"+packet.getParamName()+"-"+packet.getValueName();
					  notificationHandler.showMessage(dataManager.getMessageSource(),AP_CONF_UPDATE,""+successCount+" "+SUCCESSFUL+
							  " "+failCount+" "+FAILURE+" ("+errorMsg+")",""+completedUpdateCount+ " of "+totalUpdateCount);
				}
				if(totalUpdateCount == completedUpdateCount){
					successCount=0;
					failCount=0;
				}
			}
		
		if(updateStatus.isPacketChanged(PacketFactory.DOT11E_IFCONFIG_INFO_PACKET) == true &&
			(cfgPacketType & ConfigRequestResponsePacket.DOT11E_CONFIG_REQ_RES) != 0) {
			
				updateStatus.clearPacketStatus(PacketFactory.DOT11E_IFCONFIG_INFO_PACKET);
				int totalUpdateCount		= updateStatus.getTotalStatusCount();		
				int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
				if(errorCode ==0){
				successCount++;
				notificationHandler.showMessage(dataManager.getMessageSource(),_80211EIF_CONF_UPDATE, 
						""+successCount+" "+SUCCESSFUL+
						  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
				}else{
					failCount++;
					 String errorMsg="";
					  errorMsg=packet.getIfName()+"-"+packet.getParamName()+"-"+packet.getValueName();
					  notificationHandler.showMessage(dataManager.getMessageSource(),AP_CONF_UPDATE,""+successCount+" "+SUCCESSFUL+
							  " "+failCount+" "+FAILURE+" ("+errorMsg+")",""+completedUpdateCount+ " of "+totalUpdateCount);
				}
				if(totalUpdateCount ==completedUpdateCount){
					successCount=0;
					failCount=0;
				}
			}
		
		if(updateStatus.isPacketChanged(PacketFactory.IF_HIDDEN_ESSID_PACKET) == true &&
			(cfgPacketType & ConfigRequestResponsePacket.IF_HIDDEN_ESSID_REQ_RES) != 0) {
			
				updateStatus.clearPacketStatus(PacketFactory.IF_HIDDEN_ESSID_PACKET);
				int totalUpdateCount		= updateStatus.getTotalStatusCount();		
				int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
				if(errorCode == 0){
				successCount++;
				notificationHandler.showMessage(dataManager.getMessageSource(),HIDDENESSID_CONF_UPDATE, 
						""+successCount+" "+SUCCESSFUL+
						  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
				}else{
					failCount++;
					 String errorMsg="";
					 errorMsg=packet.getIfName()+"-"+packet.getParamName()+"-"+packet.getValueName();
					 notificationHandler.showMessage(dataManager.getMessageSource(),AP_CONF_UPDATE,""+successCount+" "+SUCCESSFUL+
							  " "+failCount+" "+FAILURE+" ("+errorMsg+")",""+completedUpdateCount+ " of "+totalUpdateCount);
				}
				if(totalUpdateCount ==completedUpdateCount){
					successCount=0;
					failCount=0;
				}
		}
		
		if(updateStatus.isPacketChanged(PacketFactory.SIP_CONFIG_PACKET) == true &&
			(cfgPacketType2 & ConfigRequestResponsePacket.SIP_CONFIG_REQ_RES) != 0) {
				
				updateStatus.clearPacketStatus(PacketFactory.SIP_CONFIG_PACKET);
				int totalUpdateCount		= updateStatus.getTotalStatusCount();		
				int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
				if(errorCode ==0){
				successCount++;
				notificationHandler.showMessage(dataManager.getMessageSource(), SIP_CONF_UPDATE, 
						""+successCount+" "+SUCCESSFUL+
						  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
				}else{
					failCount++;
					 String errorMsg="";
					 errorMsg=packet.getIfName()+"-"+packet.getParamName()+"-"+packet.getValueName();
					 notificationHandler.showMessage(dataManager.getMessageSource(),AP_CONF_UPDATE,""+successCount+" "+SUCCESSFUL+
							  " "+failCount+" "+FAILURE+" ("+errorMsg+")",""+completedUpdateCount+ " of "+totalUpdateCount);

				}
				if(totalUpdateCount ==completedUpdateCount){
					successCount=0;
					failCount=0;
				}
			}
		
	}

	void effistreamResponseReceived(EffistreamRequestResponsePacket packet) {
		
		if(updateStatus.isPacketChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET) == true) {
			
				updateStatus.clearPacketStatus(PacketFactory.EFFISTREAM_CONFIG_PACKET);
				int totalUpdateCount		= updateStatus.getTotalStatusCount();		
				int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
				if(updateStatus.isRebootRequired() == true)
				 isRebootRequired();
				successCount++;
				notificationHandler.showMessage(dataManager.getMessageSource(),EFFISTREAM_CONF_UPDATE, 
						 ""+successCount+" "+SUCCESSFUL+
						  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
				notificationHandler.showMessage(dataManager.getMessageSource(),EFFISTREAM_CONF_UPDATE, 
						Mesh.STATUS_REBOOTREQD,""+completedUpdateCount+ " of "+totalUpdateCount);
				if(totalUpdateCount ==completedUpdateCount){
					successCount=0;
					failCount=0;
				}

			}
	}

	void genericCommandResponseReceived(GenericCommandRequestResponse response) {
				
	}

	void imcpKeyUpdateResponseReceived(ImcpKeyUpdateResponsePacket packet) {
		
		if(updateStatus.isPacketChanged(PacketFactory.IMCP_KEY_UPDATE) == true) {
			
			updateStatus.clearPacketStatus(PacketFactory.IMCP_KEY_UPDATE);
			int totalUpdateCount		= updateStatus.getTotalStatusCount();
		 	int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
		 	
		 	if(updateStatus.isRebootRequired() == true)
			   isRebootRequired();
		 	
		 	successCount++;
			notificationHandler.showMessage(dataManager.getMessageSource(),IMCP_KEY_UPDATE, 
					 ""+successCount+" "+SUCCESSFUL+
					  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
			notificationHandler.showMessage(dataManager.getMessageSource(),IMCP_KEY_UPDATE,
					Mesh.STATUS_REBOOTREQD,""+completedUpdateCount+ " of "+totalUpdateCount);
			if(totalUpdateCount ==completedUpdateCount){
				successCount=0;
				failCount=0;
			}
		}	
	}

	void customChannelResponseReceived(RFCustomChannelInfoRequestResponsePacket packet) {
		
		if(updateStatus.isPacketChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET) == true) {
			
				updateStatus.clearPacketStatus(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
				int totalUpdateCount		= updateStatus.getTotalStatusCount();		
				int completedUpdateCount 	= totalUpdateCount - updateStatus.getPendingStatusCount();
				if(updateStatus.isRebootRequired() == true)
				 isRebootRequired();
				successCount++;
				notificationHandler.showMessage(dataManager.getMessageSource(),RF_CUSTOM_CHANNEL_CONF_UPDATE, 
						 ""+successCount+" "+SUCCESSFUL+
						  " "+failCount+" "+FAILURE,""+completedUpdateCount+ " of "+totalUpdateCount);
				notificationHandler.showMessage(dataManager.getMessageSource(),RF_CUSTOM_CHANNEL_CONF_UPDATE, 
						Mesh.STATUS_REBOOTREQD,""+completedUpdateCount+ " of "+totalUpdateCount);
				if(totalUpdateCount ==completedUpdateCount){
					successCount=0;
					failCount=0;
				}
			}
	}

	 public void fillRequestResponseData(RequestResponsePacket packet) {
		PacketSender packetSender = dataManager.getPacketSender();
		packet.setRequestResponseID(packetSender.getRequestId());
		packet.setRequestResponseType(RequestResponsePacket.TYPE_REQUEST);
	}

	private boolean fillApConfigInfo(ApConfigInfoPacket apConfigPacket) {
		
		fillRequestResponseData(apConfigPacket);
		/**
		 * according to  new packet strucutre 
		 */
		apConfigPacket.setNODE_INFO_ID(IMCPInfoIDS.IMCP_NODE_INFO_ID);
		apConfigPacket.setNODE_INFO_LENGTH(apConfigPacket.getPacketLength());
		
		apConfigPacket.setApName(configuration.getNodeName());
		apConfigPacket.setDescription(configuration.getDescription());
		apConfigPacket.setLatitude(configuration.getLatitude());
		apConfigPacket.setLongitude(configuration.getLongitude());
		
		MacAddress	mac	=	new MacAddress();
		mac.setBytes(configuration.getPreferedParent());
		apConfigPacket.setPreferredParent(mac);
		
		IMeshConfiguration		meshConfiguration		= configuration.getMeshConfiguration();
		IInterfaceConfiguration interfaceConfiguration  = configuration.getInterfaceConfiguration();
		
		if(meshConfiguration == null || interfaceConfiguration == null) {
			return false;
		}
		apConfigPacket.setBeaconInterval((int)meshConfiguration.getBeaconInterval());
		apConfigPacket.setCrThreshold(meshConfiguration.getChangeResistanceThreshold());
		apConfigPacket.setFragThreshold(meshConfiguration.getFragThreshold());
		apConfigPacket.setMaxAllowableHops(meshConfiguration.getMaxAllowableHops());
		apConfigPacket.setHeartbeatInterval(meshConfiguration.getHeartbeatInterval());
		apConfigPacket.setHeartbeatMissCount(meshConfiguration.getHeartbeatMissCount());
		apConfigPacket.setHopCost(meshConfiguration.getHopCost());
		apConfigPacket.setDsTxPower(meshConfiguration.getDSTxPower());
		apConfigPacket.setDsTxRate(meshConfiguration.getDSTxRate());
		apConfigPacket.setDynamicChannelAlloc(meshConfiguration.getDynamicChannelAllocation());
		apConfigPacket.setEssid(meshConfiguration.getEssid());
		apConfigPacket.setETSI(meshConfiguration.getETSICertifiedOperation());
		apConfigPacket.setFCC(meshConfiguration.getFCCCertifiedOperation());
		apConfigPacket.setSignalMap(meshConfiguration.getSignalMap());
		apConfigPacket.setLasi(meshConfiguration.getLocationAwarenessScanInterval());
		apConfigPacket.setRtsThreshold(meshConfiguration.getRTSThreshold());
		apConfigPacket.setStayAwakeCount(meshConfiguration.getStayAwakeCount());
		apConfigPacket.setBridgeAgeingTime(meshConfiguration.getBridgeAgeingTime());
       
		int	  i		=	0;	
		short count	=	(short)interfaceConfiguration.getInterfaceCount();
		apConfigPacket.setInterfaceCount(count);
		
		for(i = 0; i < count; i++) {
			
			InterfaceConfigInfo	ifConfPktInfo	= apConfigPacket.getIfRefByIndex(i);
			IInterfaceInfo		interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(i);
			if(ifConfPktInfo == null || interfaceInfo == null) {
				return false;
			}
			if (interfaceInfo.getName().equalsIgnoreCase("eth0") || interfaceInfo.getName().equalsIgnoreCase("eth1")) {
				ifConfPktInfo.setInterfaceID(IMCPInfoIDS.IMCP_IF_802_3_INFO_ID);
				ifConfPktInfo.setInterfaceName(interfaceInfo.getName());
				ifConfPktInfo.setInterfaceLEN(ifConfPktInfo.getPacketLength());
				ifConfPktInfo.setMediumSubType(interfaceInfo.getMediumSubType());
				ifConfPktInfo.setMediumType(interfaceInfo.getMediumType());
				ifConfPktInfo.setTxRate(interfaceInfo.getTxRate());
				ifConfPktInfo.setUsageType(interfaceInfo.getUsageType());
				ifConfPktInfo.setAppCtrl(interfaceInfo.getAppCtrl());
				ifConfPktInfo.setServiceType(interfaceInfo.getServiceType());
			}
			else  {
			ifConfPktInfo.setInterfaceID(IMCPInfoIDS.IMCP_IF_802_11_INFO_ID);
			ifConfPktInfo.setInterfaceLEN(ifConfPktInfo.getPacketLength());
			ifConfPktInfo.setInterfaceName(interfaceInfo.getName());
			ifConfPktInfo.setBeaconInt(interfaceInfo.getBeaconInterval());
			ifConfPktInfo.setChannel(interfaceInfo.getChannel());
			ifConfPktInfo.setChannelBondingType(interfaceInfo.getChannelBondingType());
			
			
			int   dcaListSize	= interfaceInfo.getDcaListCount();
			ifConfPktInfo.setDcaListCount(interfaceInfo.getDcaListCount());
			for(int j = 0; j < dcaListSize; j++) {
				ifConfPktInfo.setDcaListChannel(j, interfaceInfo.getDcaChannel(j));	
			}
						
			ifConfPktInfo.setDynamicChannelAlloc(interfaceInfo.getDynamicChannelAlloc());
			ifConfPktInfo.setMediumSubType(interfaceInfo.getMediumSubType());			
			ifConfPktInfo.setMediumType(interfaceInfo.getMediumType());
			ifConfPktInfo.setEssid(interfaceInfo.getEssid());
			ifConfPktInfo.setFragThreshold(interfaceInfo.getFragThreshold());
			ifConfPktInfo.setRtsThreshold(interfaceInfo.getRtsThreshold());
			ifConfPktInfo.setServiceType(interfaceInfo.getServiceType());
			ifConfPktInfo.setTxPower(interfaceInfo.getTxPower());
			ifConfPktInfo.setTxRate(interfaceInfo.getTxRate());
			ifConfPktInfo.setUsageType(interfaceInfo.getUsageType());
			
			ISecurityConfiguration	securityConfiguration	= (ISecurityConfiguration)interfaceInfo.getSecurityConfiguration();
			SecurityInfo 			secInfo					= ifConfPktInfo.getSecurityInfo();
			int 					securityType			= securityConfiguration.getEnabledSecurity();
			
			if(secInfo == null || securityConfiguration == null) {
				return false;
			}
			setSecurity(securityType, secInfo, securityConfiguration);
			
			InterfaceAdvanceConfiguration IFadvanceInfo=(InterfaceAdvanceConfiguration) interfaceInfo.getInterfaceAdvanceConfiguration();
			InterfaceAdvanceInfo advanceInfo=ifConfPktInfo.getInterfaceAdvanceInfo();
			
			if(advanceInfo == null || IFadvanceInfo == null)   
		     continue;
			
			if(interfaceInfo.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_24GHz_N || 
				interfaceInfo.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_5GHz_N|| 
				interfaceInfo.getMediumSubType()== Mesh.PHY_SUB_TYPE_802_11_BGN ||
				   interfaceInfo.getMediumSubType() ==Mesh.PHY_SUB_TYPE_802_11_AN){
				   setInterfaceAdvanceInfo(IFadvanceInfo,advanceInfo);
		           advanceInfo.setProtocal_Info_len(InterfaceAdvanceInfo.INTERFACE_ADVANCEINFO_LENGTH_FOR_N);
		           advanceInfo.setProtocal_Info_ID(IMCPInfoIDS.IMCP_IF_802_11_EXT_11N_INFO_ID);
		           advanceInfo.setProtocolID(InterfaceAdvanceInfo.protocalid_N);
		           ifConfPktInfo.setInterfaceLEN(ifConfPktInfo.getPacketLength() | 1<<15);// for extension
		          }else if(interfaceInfo.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_AC){
				    setInterfaceAdvanceInfo(IFadvanceInfo,advanceInfo);
			        advanceInfo.setProtocal_Info_len(InterfaceAdvanceInfo.INTERFACE_ADVANCEINFO_LENGTH_FOR_AC);
			        advanceInfo.setProtocal_Info_ID(IMCPInfoIDS.IMCP_IF_802_11_EXT_11N_11AC_INFO_ID);
			        advanceInfo.setProtocolID(InterfaceAdvanceInfo.protocalid_AC);
			        ifConfPktInfo.setInterfaceLEN(ifConfPktInfo.getPacketLength() | 1<<15);// for extension
			}else if(interfaceInfo.getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_ANAC){
				  setInterfaceAdvanceInfo(IFadvanceInfo,advanceInfo);
		          advanceInfo.setProtocal_Info_len(InterfaceAdvanceInfo.INTERFACE_ADVANCEINFO_LENGTH_FOR_N_AC);
		          advanceInfo.setProtocolID(InterfaceAdvanceInfo.protocalid_N_AC);
		          advanceInfo.setProtocal_Info_ID(IMCPInfoIDS.IMCP_IF_802_11_EXT_11N_11AC_INFO_ID);
		          ifConfPktInfo.setInterfaceLEN(ifConfPktInfo.getPacketLength() | 1<<15);// for extension
		        }
			  
		} 
	}
		return true;
	}
	
	
	public void setInterfaceAdvanceInfo(
			InterfaceAdvanceConfiguration ifadvanceInfo, InterfaceAdvanceInfo advanceInfo) {
		advanceInfo.setChannelBandwidth(ifadvanceInfo.getChannelBandwidth());
		advanceInfo.setSecondaryChannelPosition(ifadvanceInfo.getSecondaryChannelPosition());
		advanceInfo.setFrameAggregation(ifadvanceInfo.getFrameAggregation());
		advanceInfo.setGuardInterval_20(ifadvanceInfo.getGuardInterval_20());
		advanceInfo.setGuardInterval_40(ifadvanceInfo.getGuardInterval_40());
		advanceInfo.setGuardInterval_80(ifadvanceInfo.getGuardInterval_80());
		advanceInfo.setLdpc(ifadvanceInfo.getLDPC());
		advanceInfo.setTxSTBC(ifadvanceInfo.getTxSTBC());
		advanceInfo.setRxSTBC(ifadvanceInfo.getRxSTBC());
		advanceInfo.setCoexistence(ifadvanceInfo.getCoexistence());
		advanceInfo.setGfMode(ifadvanceInfo.getGFMode());
		advanceInfo.setMaxAMPDU(ifadvanceInfo.getMaxAMPDU());
		advanceInfo.setMaxAMSDU(ifadvanceInfo.getMaxAMSDU());
		advanceInfo.setMaxMPDU(ifadvanceInfo.getMaxMPDU());
		advanceInfo.setMaxAMPDUEnabled(ifadvanceInfo.getMaxAMPDUEnabled());
		advanceInfo.setProtocolID(ifadvanceInfo.getProtocolID());
		advanceInfo.setMaxMPDU(ifadvanceInfo.getMaxMPDU());
	}

	private boolean fillIpConfigInfo(ApIpConfigInfoPacket ipConfigPacket) {
		
		fillRequestResponseData(ipConfigPacket);
		
		INetworkConfiguration	networkConfiguration	=	configuration.getNetworkConfiguration();
		
		if(networkConfiguration	== null) {
			return false;
		}
		/**
		 * according to new IMCP structure 
		 */
		ipConfigPacket.setHostName(networkConfiguration.getHostName());
		ipConfigPacket.setGateway(networkConfiguration.getGateway());
		ipConfigPacket.setIpAddress(networkConfiguration.getIpAddress());
		ipConfigPacket.setSubnetMask(networkConfiguration.getSubnetMask());
		
		ipConfigPacket.setAP_IP_CONFIG_ID(IMCPInfoIDS.IMCP_IP_CONFIG_INFO_ID);
		ipConfigPacket.setAP_IP_CONFIG_LENGTH(ipConfigPacket.getPacketLength());		
		
		return true;
	}
	
	private boolean fillVlanConfigInfo(VLanConfigInfoPacket vlanConfigPacket) {
		
		fillRequestResponseData(vlanConfigPacket);
		
		IVlanConfiguration	vlanConfiguration	= configuration.getVlanConfiguration();
		
		if(vlanConfiguration == null) {
			return false;
		}
		
		/**
		 * according to new IMCP structure 
		 */
		vlanConfigPacket.setVLAN_CONFIG_ID(IMCPInfoIDS.IMCP_VLAN_CONFIG_INFO_ID);
		vlanConfigPacket.setVLAN_CONFIG_LENGTH(vlanConfigPacket.getPacketLength());
		
		vlanConfigPacket.setDefaultTag(vlanConfiguration.getDefaultTag());
		vlanConfigPacket.setDefaultDot11e(vlanConfiguration.getDefault80211eCategoryIndex());
		vlanConfigPacket.setDefaultDot11eEnabled(vlanConfiguration.getDefault80211eEnabled());
		vlanConfigPacket.setDefaultDot1p(vlanConfiguration.getDefault8021pPriority());
		
		
		int	  i			= 0;
		short vlanCount	= (short)vlanConfiguration.getVlanCount();
		vlanConfigPacket.setVlanCount((short) (vlanCount)); 
														
		
		
		for(i = 0; i < vlanCount; i++) {
			
			VLanConfigInfo	vlanPktInfo		= vlanConfigPacket.getVLanConfig(i);
			IVlanInfo		vlanConfInfo	= (IVlanInfo)vlanConfiguration.getVlanInfoByIndex(i);
			
			if(vlanConfInfo	== null || vlanPktInfo == null) {
				continue;
			}
			
			vlanPktInfo.setName(vlanConfInfo.getName());
			vlanPktInfo.setEssid(vlanConfInfo.getEssid());
			vlanPktInfo.setIpAddress(vlanConfInfo.getIpAddress());
			vlanPktInfo.setDot11e(vlanConfInfo.get80211eCategoryIndex());
			vlanPktInfo.setDot11eEnabled(vlanConfInfo.get80211eEnabled());
			vlanPktInfo.setDot1p(vlanConfInfo.get8021pPriority());
			vlanPktInfo.setTag(vlanConfInfo.getTag());
			vlanPktInfo.setBeaconInt(vlanConfInfo.getBeaconInterval());
			vlanPktInfo.setFragThreshold(vlanConfInfo.getfragThreshold());
			vlanPktInfo.setRtsThreshold(vlanConfInfo.getRtsThreshold());
			vlanPktInfo.setServiceType(vlanConfInfo.getServiceType());
			vlanPktInfo.setTxPower(vlanConfInfo.getTxPower());
			vlanPktInfo.setTxRate(vlanConfInfo.getTxRate());
			
			ISecurityConfiguration	securityConfiguration	= (ISecurityConfiguration)vlanConfInfo.getSecurityConfiguration();
			SecurityInfo			securityPktInfo			=  vlanPktInfo.getSecurityInfo();
			
			if(securityConfiguration == null || securityPktInfo == null) {
				return false;
			}
			
			int securityType	= securityConfiguration.getEnabledSecurity();
			setSecurity(securityType, securityPktInfo, securityConfiguration);
		}
		return true;
	}

	/**
	 * @param securityType
	 * @param securityPktInfo
	 * @param securityConfiguration
	 */
	public boolean setSecurity(int securityType, SecurityInfo securityPktInfo, ISecurityConfiguration securityConfiguration) {
		
		if(securityType == ISecurityConfiguration.SECURITY_TYPE_NONE) {
			securityPktInfo.setNoneEnabled	((short)Mesh.ENABLED);
			securityPktInfo.setWEPEnabled	((short)Mesh.DISABLED);
			securityPktInfo.setRSNPSKEnabled((short)Mesh.DISABLED);
			securityPktInfo.setRADEnabled	((short)Mesh.DISABLED);
		}
		if (securityType == ISecurityConfiguration.SECURITY_TYPE_WEP) {
			
			securityPktInfo.setWEPEnabled	((short)Mesh.ENABLED);
			securityPktInfo.setNoneEnabled	((short)Mesh.DISABLED);
			securityPktInfo.setRSNPSKEnabled((short)Mesh.DISABLED);
			securityPktInfo.setRADEnabled	((short)Mesh.DISABLED);
			
			WEPSecurityInfo		wepPktInfo	= securityPktInfo.getWEPInfo();
			IWEPConfiguration	wepConfInfo	= (IWEPConfiguration)securityConfiguration.getWEPConfiguration();
			
			if(wepConfInfo == null || wepPktInfo == null) {
				return false;
			}
			wepPktInfo.setWEPKey0(wepConfInfo.getKey(0));
			wepPktInfo.setWEPKey1(wepConfInfo.getKey(1));
			wepPktInfo.setWEPKey2(wepConfInfo.getKey(2));
			wepPktInfo.setWEPKey3(wepConfInfo.getKey(3));
			wepPktInfo.setWEPKeyIndex(wepConfInfo.getKeyIndex());
			wepPktInfo.setWEPPassPhrase(wepConfInfo.getPassPhrase());
			wepPktInfo.setWEPStrength(wepConfInfo.getKeyStrength());
			
		}
		if (securityType == ISecurityConfiguration.SECURITY_TYPE_WPA_PERSONAL) {
			
			securityPktInfo.setRSNPSKEnabled((short)Mesh.ENABLED);
			securityPktInfo.setNoneEnabled	((short)Mesh.DISABLED);
			securityPktInfo.setWEPEnabled	((short)Mesh.DISABLED);
			securityPktInfo.setRADEnabled	((short)Mesh.DISABLED);
			
			PSKConfiguration	pskConfig	= (PSKConfiguration) securityConfiguration.getPSKConfiguration();
			RSNPSKSecurityInfo  pskPktInfo	= securityPktInfo.getRSNPSKInfo();
			
			if(pskConfig == null || pskPktInfo == null) {
				return false;
			}
			
			pskPktInfo.setPSKCipherCCMP(pskConfig.getPSKCipherCCMP());
			pskPktInfo.setPSKCipherTKIP(pskConfig.getPSKCipherTKIP());
			pskPktInfo.setPSKGroupKeyRenewal(pskConfig.getPSKGroupKeyRenewal());
			pskPktInfo.setPSKKey(pskConfig.getPSKKey());
			pskPktInfo.setPSKKeyLength(pskConfig.getPSKKeyLength());
			pskPktInfo.setPSKMode(pskConfig.getPSKMode());
			pskPktInfo.setPSKPassPhrase(pskConfig.getPSKPassPhrase());
			
		}
		if (securityType == ISecurityConfiguration.SECURITY_TYPE_WPA_ENTERPRISE) {
			
			securityPktInfo.setRADEnabled	((short)Mesh.ENABLED);
			securityPktInfo.setNoneEnabled	((short)Mesh.DISABLED);
			securityPktInfo.setWEPEnabled	((short)Mesh.DISABLED);
			securityPktInfo.setRSNPSKEnabled((short)Mesh.DISABLED);
						
			RadiusConfiguration	radConfig	= (RadiusConfiguration) securityConfiguration.getRadiusConfiguration();
			RADSecurityInfo  	radPktInfo	= securityPktInfo.getRADInfo();
			
			if(radConfig == null || radPktInfo == null) {
				return false;
			}
			
			radPktInfo.setRADCipherCCMP(radConfig.getCipherCCMP());
			radPktInfo.setRADCipherTKIP(radConfig.getCipherTKIP());
			
			radPktInfo.setRADDecidesVLAN(radConfig.getWPAEnterpriseDecidesVlan());	
			radPktInfo.setRADGroupKeyRenewal(radConfig.getGroupKeyRenewal());
			
			IpAddress	ip	= new IpAddress();
			ip.setBytes(radConfig.getServerIPAddress().getBytes());
			radPktInfo.setRADIPAddress(ip);
			
			radPktInfo.setRADKey(radConfig.getKey());
			radPktInfo.setRADMode(radConfig.getMode());
			radPktInfo.setRADPort(radConfig.getPort());
		}
		return true;
	}


	private boolean fillRegDomainInfo(ApRegDomainInfoPacket regDomainPacket) {
		
		fillRequestResponseData(regDomainPacket);
		regDomainPacket.setAP_REGDOMAIN_INFO_ID(IMCPInfoIDS.IMCP_REG_DOMAIN_INFO_ID);
		regDomainPacket.setAP_REGDOMAIN_INFO_LENGTH(regDomainPacket.getPacketLength());
		regDomainPacket.setCountryCode(configuration.getCountryCode());
		regDomainPacket.setRegulatoryDomain(configuration.getRegulatoryDomain());
		return true;
	}

	private boolean fillAckTimeoutInfo(ApAckTimeoutInfoPacket ackTimeoutPacket) {
		
		fillRequestResponseData(ackTimeoutPacket);
		
		IInterfaceConfiguration interfaceConfiguration  = configuration.getInterfaceConfiguration();
		
		if(interfaceConfiguration == null) {
			return false;
		}
		
		int	  i	= 0;	
		int	  j	= 0;	
		
		if(getWirelessIfCount() < 0) {
			return false;
		}
		/**
		 * according to new IMCP structure 
		 */
		ackTimeoutPacket.setAP_ACK_TIMEOUT_INFO_ID(IMCPInfoIDS.IMCP_ACK_TIMEOUT_INFO_ID);
		ackTimeoutPacket.setAP_ACK_TIMEOUT_INFO_LENGTH(ackTimeoutPacket.getPacketLength());
		
		ackTimeoutPacket.setICnt(getWirelessIfCount());
		int count = interfaceConfiguration.getInterfaceCount();
		
		for(i = 0; i < count; i++) {
			
			IInterfaceInfo		interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(i);
			
			if(interfaceInfo == null) {
				return false;
			}
			
			if(interfaceInfo.getMediumType() != Mesh.PHY_TYPE_802_11 && interfaceInfo.getMediumType() != Mesh.PHY_TYPE_802_11_VIRTUAL) {
        		continue;
			}
			
			ackTimeoutPacket.setIfName(j,interfaceInfo.getName());
			ackTimeoutPacket.setAckTimeout(j,interfaceInfo.getAckTimeout());
			j++;
		}	
		
		return true;
	}
	
	/**
	 * @return
	 */
	private short getWirelessIfCount() {
		
		IInterfaceConfiguration interfaceConfiguration  = configuration.getInterfaceConfiguration();
		
		if(interfaceConfiguration == null) {
			return -1;
		}
			
		short count				= (short)interfaceConfiguration.getInterfaceCount();
		short wirelessIfCount	= 0;	
		int   i 				= 0;
		
		for(i = 0; i < count; i++) {
		
			IInterfaceInfo		interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(i);
			
			if(interfaceInfo == null) {
				continue;
			}
			if(interfaceInfo.getMediumType() != Mesh.PHY_TYPE_802_11 && interfaceInfo.getMediumType() != Mesh.PHY_TYPE_802_11_VIRTUAL) {
        		continue;
			}
			wirelessIfCount++;
		}
		return wirelessIfCount;
	}

	private boolean fillHiddenEssidInfo(IfHiddenESSIDInfoPacket hiddenEssidPacket) {
		
		fillRequestResponseData(hiddenEssidPacket);
		IInterfaceConfiguration interfaceConfiguration  = configuration.getInterfaceConfiguration();
		
		if(interfaceConfiguration == null) {
			return false;
		}
		
		int	  i		= 0;	
		int	  j		= 0;	
		
		if(getWirelessIfCount() < 0) {
			return false;
		}
		hiddenEssidPacket.setIFHIDDENESSID_INFO_ID(20);
		hiddenEssidPacket.setIFHIDDENESSID_INFO_LENGTH(hiddenEssidPacket.getPacketLength());
		hiddenEssidPacket.setICnt(getWirelessIfCount());
		int count = interfaceConfiguration.getInterfaceCount();
		
		for(i = 0; i < count; i++) {
			
			IInterfaceInfo		interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(i);
			
			if(interfaceInfo == null) {
				return false;
			}
			
			if(interfaceInfo.getMediumType() != Mesh.PHY_TYPE_802_11 && interfaceInfo.getMediumType() != Mesh.PHY_TYPE_802_11_VIRTUAL) {
        		continue;
			}
			
			hiddenEssidPacket.setIfName(j,interfaceInfo.getName());
			hiddenEssidPacket.setHiddenESSID(j,interfaceInfo.getEssidHidden());
			j++;
		}	
		return true;
	}
	
	private boolean fillDot11eCategoryInfo(Dot11eCategoryPacket dot11eCategoryPacket) {
		
		fillRequestResponseData(dot11eCategoryPacket);
		
		I80211eCategoryConfiguration	dot11eConfiguration	= (I80211eCategoryConfiguration)configuration.get80211eCategoryConfiguration();
		
		if(dot11eConfiguration == null) {
			return false;
		}
		
		int   i				= 0;
		short dot11Count	= (short)dot11eConfiguration.getCount();
		dot11eCategoryPacket.setCategoryCount(dot11Count);
		/**
		 * according to new IMCP structure 
		 */
		dot11eCategoryPacket.setDOT11E_CATEGORY_INFO_ID(IMCPInfoIDS.IMCP_DOT11E_CATEGORY_INFO_ID);
		dot11eCategoryPacket.setDOT11E_CATEGORY_INFO_LENGTH(dot11eCategoryPacket.getPacketLength());
		for(i = 0; i < dot11Count; i++) {
			
			I80211eCategoryInfo		dot11eInfo		= (I80211eCategoryInfo)dot11eConfiguration.getCategoryInfo(i);
			Dot11eCategoryDetails   dot11PktInfo	= dot11eCategoryPacket.getdDot11eCategoryRefByIndex(i);
			
			if(dot11eInfo == null || dot11PktInfo == null) {
				return false;
			}
			
			dot11PktInfo.aCWmax				= dot11eInfo.getACWMax();
			dot11PktInfo.aCWmin				= dot11eInfo.getACWMin();
			dot11PktInfo.aifsn				= dot11eInfo.getAIFSN();
			dot11PktInfo.burstTime			= dot11eInfo.getBurstTime();
			dot11PktInfo.categoryName		= dot11eInfo.getName();
			dot11PktInfo.disableBackoff		= dot11eInfo.getDisableBackoff();
		}
		
		return true;
	}
	
	public boolean fillDot11eConfigInfo(Dot11eIfInfoPacket dot11eConfigInfoPacket) {
		
		fillRequestResponseData(dot11eConfigInfoPacket);
		
		IInterfaceConfiguration interfaceConfiguration	= (IInterfaceConfiguration)configuration.getInterfaceConfiguration();
		
		if(interfaceConfiguration == null) {
			return false;
		}
		
		int   i		= 0;
		int   j		= 0;
		/**
		 * according to new IMCP structure 
		 */
		dot11eConfigInfoPacket.setDOT11E_INFO_ID(IMCPInfoIDS.IMCP_DOT11E_CONFIG_INFO_ID);
		dot11eConfigInfoPacket.setDOT11E_INFO_LENGTH(dot11eConfigInfoPacket.getPacketLength());
		
		dot11eConfigInfoPacket.setInterfaceCount(getWirelessIfCount());
		short count	= (short)interfaceConfiguration.getInterfaceCount();
		
		for(i = 0; i < count; i++) {
			
			IInterfaceInfo		interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByIndex(i);
			
			if(interfaceInfo == null) {
				return false;
			}
			if(interfaceInfo.getMediumType() != Mesh.PHY_TYPE_802_11 && interfaceInfo.getMediumType() != Mesh.PHY_TYPE_802_11_VIRTUAL) {
        		continue;
			}
			
			Dot11eIfInfoDetails	dot11IfPktInfo	= dot11eConfigInfoPacket.getIfRefByIndex(j);
			if(dot11IfPktInfo == null) {
				return false;
			}
			dot11IfPktInfo.setIfDot11eCategory(interfaceInfo.getDot11eCategory());
			dot11IfPktInfo.setIfDot11eEnabled(interfaceInfo.getDot11eEnabled());
			dot11IfPktInfo.setIfEssid(interfaceInfo.getEssid());
			dot11IfPktInfo.setInterfaceName(interfaceInfo.getName());
			j++;
		}
		return true;
	}
	
	private boolean fillAclInfo(ACLInfoPacket aclInfoPacket) {
		
		fillRequestResponseData(aclInfoPacket);
		
		IACLConfiguration	aclConfiguration	= (IACLConfiguration)configuration.getACLConfiguration();
		
		if(aclConfiguration == null) {
			return false;
		}
		/**
		 * according to new IMCP structure added two new fields 
		 */
		aclInfoPacket.setACL_INFO_ID(IMCPInfoIDS.IMCP_ACL_CONFIG_INFO_ID);
		aclInfoPacket.setACL_INFO_LENGTH(aclInfoPacket.getPacketLength());
		int i			= 0;
		short aclCount	= (short)aclConfiguration.getCount();
		aclInfoPacket.setACLCount(aclCount);
		
		for(i = 0; i < aclCount; i++) {
			
			ACLInfoDetails	aclPktInfo	= aclInfoPacket.getRuleRefByIndex(i);
			IACLInfo		aclConfInfo	= (IACLInfo)aclConfiguration.getACLInfo(i);
			
			if(aclConfInfo == null || aclPktInfo == null) {
				return false;
			}
			aclPktInfo.setAllow(aclConfInfo.getAllow());
			aclPktInfo.setCategory11e(aclConfInfo.get80211eCategoryIndex());
			aclPktInfo.setEnable11e(aclConfInfo.getEnable80211eCategory());
			aclPktInfo.setStaMac(aclConfInfo.getStaAddress());
			aclPktInfo.setVLANTag(aclConfInfo.getVLANTag());
			
		}
		return true;
	}
	
	private boolean fillRfCustomChannelInfo(RFCustomChannelInfoPacket rfConfigInfoPacket) {
		
		fillRequestResponseData(rfConfigInfoPacket);
		/**
		 * according to new IMCp structure 
		 */
		rfConfigInfoPacket.setRF_CUSTOMCHANNEL_INFO_ID(12);
		rfConfigInfoPacket.setRF_CUSTOMCHANNEL_INFO_LENGTH(rfConfigInfoPacket.getPacketLength());
		
		IInterfaceConfiguration interfaceConfiguration	= (IInterfaceConfiguration)configuration.getInterfaceConfiguration();
		
		if(interfaceConfiguration == null) {
			return false;
		}
		
		int    			i				= 0;
		String 			ifName			= rfConfigInfoPacket.getIfName();
		IInterfaceInfo	interfaceInfo 	= null;
		
		interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.getInterfaceByName(ifName);
		if(interfaceInfo == null) {
			return false;
		}
		
		InterfaceCustomChannelConfig	customConfig	= (InterfaceCustomChannelConfig)interfaceInfo.getCustomChannelConfiguration();
		
		rfConfigInfoPacket.setAntennaGain(customConfig.getAntennaGain());
		rfConfigInfoPacket.setAntennaPower(customConfig.getAntennaPower());
		rfConfigInfoPacket.setChannelBandwith(customConfig.getChannelBandwidth());
		rfConfigInfoPacket.setCtl(customConfig.getCtl());
		
		int chCount	= customConfig.getChannelInfoCount();
		rfConfigInfoPacket.setChannelCount(chCount);
		
		for(i = 0; i < chCount; i++) {
			
			RFChannelInfo	rfPktChInfo	= rfConfigInfoPacket.getRFChannelInfoByIndex(i);
			IChannelInfo	chInfo		= (IChannelInfo)customConfig.getChannelInfo(i);
			
			if(rfPktChInfo == null || chInfo == null) {
				return false;
			}
			rfPktChInfo.setChannel(chInfo.getChannelNumber());
			rfPktChInfo.setFrequency((int)chInfo.getFrequency());
			rfPktChInfo.setPrivateChannelFlag(chInfo.getPrivateChannelFlags());
		}
		return true;
	}
	
	
	private boolean fillEffistreamInfo(EffistreamConfigPacket effistreamConfigPacket) {
		
		if(effistreamConfigPacket == null) {
			return false;
		}
		fillRequestResponseData(effistreamConfigPacket);
		/**
		 * according to new IMCP structure 
		 */
		effistreamConfigPacket.setEFFISTREAM_CONFIG_ID(IMCPInfoIDS.IMCP_EFFISTREAM_INFO_ID);
		effistreamConfigPacket.setEFFISTREAM_CONFIG_LENGTH(effistreamConfigPacket.getPacketLength());
		
		IEffistreamConfiguration effistreamConfiguration	= configuration.getEffistreamConfiguration();
		if(effistreamConfiguration	== null) {
			return false;
		}
		IEffistreamRule			 srcRulesRoot  				= (IEffistreamRule)effistreamConfiguration.getRulesRoot();
		EffistreamConfigPacket.RuleInfo	 dstRulesRoot		= effistreamConfigPacket.getRulesRoot();
		
		dstRulesRoot.clearRuleData();
		
		fillEffistreamRuleToPkt(srcRulesRoot, dstRulesRoot);
		
		return true;
	}
	

	/**
	 * @param parentRule
	 * @param configRuleRoot
	 */
	public void fillEffistreamRuleToPkt(IEffistreamRule srcRule,RuleInfo dstRule) {
		String	ruleIdentifier;
		if(srcRule == null || dstRule == null) {
			return;
		}
		
		IEffistreamRuleCriteria	value		= (IEffistreamRuleCriteria)srcRule.getCriteriaValue();
		EffistreamHelper.copyToValueFromId(dstRule, value, srcRule.getCriteriaId());
		
		dstRule.childCount	= srcRule.getChildCount();
		
		Enumeration<String>	enumSrcChildren	= srcRule.getChildrenIdentifiers();
		
		while(enumSrcChildren.hasMoreElements()) {
			ruleIdentifier 	= (String)enumSrcChildren.nextElement();
			IEffistreamRule	srcEffiRule		= (IEffistreamRule)srcRule.getChild(ruleIdentifier);
			if(srcEffiRule == null) {
				continue;
			}
			EffistreamConfigPacket.RuleInfo	dstEffiRule	= dstRule.addRule();
			dstEffiRule.criteriaId	= srcEffiRule.getCriteriaId();
			
			fillEffistreamRuleToPkt(srcEffiRule, dstEffiRule);
		}
	
		if(srcRule.getChildCount() == 0) {
			if(srcRule.getAction() == null) {
				return;
			}
			EffistreamConfigPacket.ActionInfo	dstAction	= dstRule.setAction();
			IEffistreamAction					srcAction	= (IEffistreamAction)srcRule.getAction();
			fillActionToPkt(srcAction, dstAction);			  
		}			
	}
	

	
	/**
	 * @param srcAction
	 * @param dstAction
	 */
	private void fillActionToPkt(IEffistreamAction srcAction, ActionInfo dstAction) {
		
		if(srcAction == null || dstAction == null) {
			return;
		}
		dstAction.dot11eCategory	= srcAction.getDot11eCategory();
		dstAction.dropPacket		= srcAction.getDropPacket();
		dstAction.noAck				= srcAction.getNoAck();
		dstAction.bitRate			= srcAction.getBitRate();
		dstAction.queuedRetry		= srcAction.getQueuedRetry();
	}

	private boolean fillSipInfo(SipConfigPacket sipInfoPacket) {
		
		fillRequestResponseData(sipInfoPacket);
		
		ISipConfiguration	sipConfiguration	= (ISipConfiguration)configuration.getSipConfiguration();
		
		if(sipConfiguration == null) {
			return false;
		}
		/**
		 * according to new IMCP structure of sipInfoPacket 
		 */
		sipInfoPacket.setSIP_CONFIG_ID(IMCPInfoIDS.IMCP_SIP_CONFIG_INFO_ID);
		sipInfoPacket.setSIP_CONFIG_LENGTH(sipInfoPacket.getPacketLength());
		
		sipInfoPacket.setEnabled(sipConfiguration.getEnabled());
		sipInfoPacket.setAdhocOnly(sipConfiguration.getEnabledInP3M());
		sipInfoPacket.setPort(sipConfiguration.getPort());
		sipInfoPacket.setServerIpAddress(sipConfiguration.getServerIpAddress());
		
		int i			= 0;
		short staCount	= sipConfiguration.getStaInfoCount();
		sipInfoPacket.setStaCount(staCount);
		
		for(i = 0; i < staCount; i++) {
			
			SipStaInfo 	dstStaInfo 	= sipInfoPacket.getStaInfo(i);
			ISipStaInfo	srcStaInfo	= sipConfiguration.getStaInfoByIndex(i);
			
			if(srcStaInfo == null || dstStaInfo == null) {
				return false;
			}
			
			dstStaInfo.setExtn(srcStaInfo.getExtension());
			dstStaInfo.setMacAddress(srcStaInfo.getMacAddress().getBytes());
		
		}
		return true;
	}

	public boolean fillpacketdata(IMCPPacket packet) {

		/* cast this packet to appropriate packet type and
		 *  fill data from configuration into the packet 
		 *  
		 */
		boolean retVal = true;
		
		switch(packet.getPacketType()) {
		case PacketFactory.AP_CONFIGURATION_INFO:
			retVal = fillApConfigInfo((ApConfigInfoPacket) packet);
			break;
		case PacketFactory.AP_IP_CONFIGURATION_INFO:
			retVal = fillIpConfigInfo((ApIpConfigInfoPacket) packet);
			break;
		case PacketFactory.AP_VLAN_CONFIGURATION_INFO:
			retVal = fillVlanConfigInfo((VLanConfigInfoPacket) packet);
			break;
		case PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET:
			retVal = fillRegDomainInfo((ApRegDomainInfoPacket) packet);
			break;
		case PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET:
			retVal = fillAckTimeoutInfo((ApAckTimeoutInfoPacket) packet);
			break;
		case PacketFactory.IF_HIDDEN_ESSID_PACKET:
			retVal = fillHiddenEssidInfo((IfHiddenESSIDInfoPacket) packet);
			break;
		case PacketFactory.DOT11E_CATEGORY_INFO_PACKET:
			retVal = fillDot11eCategoryInfo((Dot11eCategoryPacket) packet);
			break;
		case PacketFactory.DOT11E_IFCONFIG_INFO_PACKET:
			retVal = fillDot11eConfigInfo((Dot11eIfInfoPacket) packet);
			break;
		case PacketFactory.ACL_INFO_PACKET:
			retVal = fillAclInfo((ACLInfoPacket) packet);
			break;
		case PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET:
			//we donot fill data here since we donot have interface name
			break;
		case PacketFactory.EFFISTREAM_CONFIG_PACKET:
			retVal = fillEffistreamInfo((EffistreamConfigPacket) packet);
			break;
		case PacketFactory.AP_REG_DOMAIN_CCODE_REQUEST_PACKET:
			fillRequestResponseData((RequestResponsePacket) packet);
			break;
		case PacketFactory.AP_ACK_TIMEOUT_REQUEST_PACKET:
			fillRequestResponseData((RequestResponsePacket) packet);	
			break;
		case PacketFactory.EFFISTREAM_CONFIG_REQUEST_RESPONSE_PACKET:
			fillRequestResponseData((RequestResponsePacket) packet);
			break;
		case PacketFactory.RF_CUSTOM_CHANNEL_REQUEST_RESPONSE_PACKET:
			fillRequestResponseData((RequestResponsePacket) packet);
			break;
		case PacketFactory.IMCP_KEY_UPDATE:
			fillRequestResponseData((RequestResponsePacket) packet);
			break;
		case PacketFactory.SIP_CONFIG_PACKET:
			retVal = fillSipInfo((SipConfigPacket) packet);
			break;
		}
		
		if(retVal == false) {
			return retVal;
		}
		
		packet.setDsMacAddress(configuration.getDSMacAddress());
		packet.setMeshId(dataManager.getMeshNetworkId());
		
		return retVal;
	}
	
	private IMCPPacket getIMCPPacket(int imcpPacketType) {
		IMCPPacket packet = PacketFactory.getNewIMCPInstance(imcpPacketType);
		
		if(packet == null)
			return null;
		
		if(fillpacketdata(packet) == false)
			return null;
		
		return packet;
	}
	
	private void sendPacket(IMCPPacket packet) {
		String					meshNetworkKey			= dataManager.getMeshNetworkKey();
		PacketSender			packetSender 			= dataManager.getPacketSender();
		if(packet != null) {
			packetSender.sendPacket(packet,meshNetworkKey);
		}
	}
	
	boolean updateChangedConfiguration() {
	    
	    boolean rfCustomChannelsSent;
	    
	    rfCustomChannelsSent = false;
		
        ICapabilityProvider	capabilityProvider	= dataManager.getCapabilityProvider();
        int 				updateCounter		= 0;
        
    	/**
    	 * Always ensure that the custom RF channels are sent before the Country code packet is sent.
    	 */
    	
        if(updateStatus.isPacketChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET) == true
        && capabilityProvider.isCapable(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET) == true) {
            
            IInterfaceConfiguration ifConfig = configuration.getInterfaceConfiguration();
            
            rfCustomChannelsSent = true;
            
            for(int i = 0; i < ifConfig.getInterfaceCount(); i++) {
                
                IInterfaceInfo ifInfo = (IInterfaceInfo)ifConfig.getInterfaceByIndex(i);
                
                if(ifInfo == null) {
                    continue;
                }
                
                int mediumType = ifInfo.getMediumType();
                if(mediumType != Mesh.PHY_TYPE_802_11)
                    continue;
                
                RFCustomChannelInfoPacket customChannelInfoPacket = 
                    (RFCustomChannelInfoPacket) getIMCPPacket(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
                
                if(customChannelInfoPacket == null) {
                    System.err.printf("ApUpdateManager : getIMCPPacket returned 'null' while creating custom channel info packet for interface %s\n",ifInfo.getName());
                    rfCustomChannelsSent = false;
                    continue;
                }

                customChannelInfoPacket.setIfName(ifInfo.getName());
                if(fillRfCustomChannelInfo(customChannelInfoPacket) == false) {
                    rfCustomChannelsSent = false;
                    System.err.printf("ApUpdateManager : fillRfCustomChannelInfo returned 'false' while creating custom channel info packet for interface %s\n",ifInfo.getName());
                    continue;
                }
                
                sendPacket(customChannelInfoPacket);
                updateCounter++;          
                
                try {
                    Thread.sleep(250);
                } catch(Exception e) {
                    
                }
            }
            
            ApRuntimeConfigurationManager runtimeManager    = dataManager.getRuntimeManager();
            runtimeManager.setRfUpdateSent(true);
       } else {
           rfCustomChannelsSent = true;
       }
    	
       if(updateStatus.isPacketChanged(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET) == true) {
           
           boolean validated;
           
           validated = true;
           
           /**
            * Do not change country code if the new code is Mesh.CUSTOM_COUNTRY_CODE
            * and 1 or more radio interfaces do not have any private channels
            * configured.
            */
           
           if(configuration.getCountryCode() == Mesh.CUSTOM_COUNTRY_CODE) {
                              
               if(rfCustomChannelsSent) {
                   
                   IInterfaceChannelConfiguration  ifChannelConfig;
                   IInterfaceInfo                  ifInfo;
                   IInterfaceConfiguration         ifConfig;                   
                   
                   ifConfig = configuration.getInterfaceConfiguration();
                   
                   for(int i = 0; i < ifConfig.getInterfaceCount(); i++) {
                       
                       ifInfo          = ifConfig.getInterfaceByIndex(i);
                       
                       int mediumType = ifInfo.getMediumType();
                       
                       if(mediumType != Mesh.PHY_TYPE_802_11)
                           continue;
                       
                       ifChannelConfig = ifInfo.getCustomChannelConfiguration();
                       
                       /**
                        * No channels configured
                        */
                       
                       if(ifChannelConfig.getChannelInfoCount() <= 0) {
                           System.err.printf("ApUpdateManager : getChannelInfoCount returned 0 for interface %s while sending custom country code.\n",ifInfo.getName());
                           validated = false;
                           break;
                       }                                      
                   }
               } else {
                   validated = false;
               }
             
           }
           
           if(validated) {    	 
               sendPacket(getIMCPPacket(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET));
               updateCounter++;
               ApRuntimeConfigurationManager runtimeManager = dataManager.getRuntimeManager();
               runtimeManager.setCountryCodeChanged(true);
           } else {
               System.err.println("<MEDIUM>The custom channel list for atleast one of the radio interfaces, could not be sent or was empty.\nThe country code shall not be changed.");
           }
       }
       
    	if(updateStatus.isPacketChanged(PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET) == true) {
    		sendPacket(getIMCPPacket(PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET));
    		updateCounter++;
    	}
    	if(updateStatus.isPacketChanged(PacketFactory.IF_HIDDEN_ESSID_PACKET) == true) {
    		sendPacket(getIMCPPacket(PacketFactory.IF_HIDDEN_ESSID_PACKET));
    		updateCounter++;
    	}
    	if(updateStatus.isPacketChanged(PacketFactory.DOT11E_CATEGORY_INFO_PACKET) == true &&
    		capabilityProvider.isCapable(PacketFactory.DOT11E_CATEGORY_INFO_PACKET) == true) {
    		sendPacket(getIMCPPacket(PacketFactory.DOT11E_CATEGORY_INFO_PACKET));
    		updateCounter++;
    	}
    	if(updateStatus.isPacketChanged(PacketFactory.DOT11E_IFCONFIG_INFO_PACKET) == true &&
       		capabilityProvider.isCapable(PacketFactory.DOT11E_IFCONFIG_INFO_PACKET) == true) {
       		sendPacket(getIMCPPacket(PacketFactory.DOT11E_IFCONFIG_INFO_PACKET));
       		updateCounter++;
       	}
    	if(updateStatus.isPacketChanged(PacketFactory.ACL_INFO_PACKET) == true &&
       		capabilityProvider.isCapable(PacketFactory.ACL_INFO_PACKET) == true) {
       		sendPacket(getIMCPPacket(PacketFactory.ACL_INFO_PACKET));
       		updateCounter++;
       	}
    	

    	if(updateStatus.isPacketChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET) == true &&
       		capabilityProvider.isCapable(PacketFactory.EFFISTREAM_CONFIG_PACKET) == true) {
      		sendPacket(getIMCPPacket(PacketFactory.EFFISTREAM_CONFIG_PACKET));
      		updateCounter++;
        }
    	if(updateStatus.isPacketChanged(PacketFactory.SIP_CONFIG_PACKET) == true &&
       		capabilityProvider.isCapable(PacketFactory.SIP_CONFIG_PACKET) == true) {
       		sendPacket(getIMCPPacket(PacketFactory.SIP_CONFIG_PACKET));
       		updateCounter++;
    	}
    	
    	if(updateStatus.isPacketChanged(PacketFactory.AP_CONFIGURATION_INFO) == true) {
    		sendPacket(getIMCPPacket(PacketFactory.AP_CONFIGURATION_INFO));
    		updateCounter++;
    	}
    	if(updateStatus.isPacketChanged(PacketFactory.AP_IP_CONFIGURATION_INFO) == true) {
    		sendPacket(getIMCPPacket(PacketFactory.AP_IP_CONFIGURATION_INFO));
    		updateCounter++;
    	}
    	if(updateStatus.isPacketChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO) == true) {
    		sendPacket(getIMCPPacket(PacketFactory.AP_VLAN_CONFIGURATION_INFO));
    		updateCounter++;
    	}
    	
/*    	if(updateStatus.isRebootRequired() == true) {
    		RebootRequestPacket	packet	= (RebootRequestPacket)PacketFactory.getNewIMCPInstance(PacketFactory.REBOOT_REQUEST_PACKET);
    		packet.setNodeId(IMCPInfoIDS.IMCP_RESET_INFO_ID);
    		packet.setNodeLength(packet.getPacketLength());
    		packet.setDsMacAddress	(configuration.getDSMacAddress());
    		packet.setMeshId		(dataManager.getMeshNetworkId());
    		sendPacket(packet);
    	}*/
    	if(updateCounter > 0) {
    		notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_UPDATE_NODE_SETTINGS,Mesh.STATUS_CONFIG_SENT,"");
    	}else {
    		notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_UPDATE_NODE_SETTINGS,Mesh.STATUS_CONFIG_NOT_SENT,"");
    	}
        return true;
	}

	boolean updateConfiguration(int imcpPacketType) {
		
		
/*		
		updateStatus.clearStatus();
		switch(imcpPacketType) {
		
			case PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET:
				sendPacket(getIMCPPacket(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET));
				updateStatus.packetChanged(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET);
	    		break;
	    		
	    	case PacketFactory.AP_CONFIGURATION_INFO: 
				sendPacket(getIMCPPacket(PacketFactory.AP_CONFIGURATION_INFO));
		    	updateStatus.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		    	break;
		    	
		    case PacketFactory.AP_VLAN_CONFIGURATION_INFO:
		    	sendPacket(getIMCPPacket(PacketFactory.AP_VLAN_CONFIGURATION_INFO));
		    	updateStatus.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
		    	break;
		    	
		}
*/		
		IMCPPacket packet = getIMCPPacket(imcpPacketType);
		if(packet == null)
			return false;
		
		updateStatus.clearStatus();
    	sendPacket(packet);
    	updateStatus.packetChanged(imcpPacketType);
    	notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_UPDATE_NODE_SETTINGS,Mesh.STATUS_CONFIG_SENT,"");
    	
		return true;
	}
	
	private void sendGeneralConfigRequest(IConfigStatusHandler packetStatusProvider, 
											ICapabilityProvider capabilityProvider, boolean forceRequestAllConfig) {

		short packetType 	= 0;
		short packetType2	= 0;

	    if (forceRequestAllConfig == false){
	        if(packetStatusProvider.isPacketReceived(PacketFactory.AP_CONFIGURATION_INFO) == false) 
	        	packetType += ConfigRequestResponsePacket.AP_CONFIG_REQ_RES;
	        if(packetStatusProvider.isPacketReceived(PacketFactory.AP_IP_CONFIGURATION_INFO) == false)
	        	packetType += ConfigRequestResponsePacket.IP_CONFIG_REQ_RES;
	        if(packetStatusProvider.isPacketReceived(PacketFactory.AP_VLAN_CONFIGURATION_INFO) == false)
	        	packetType += ConfigRequestResponsePacket.VLAN_CONFIG_REQ_RES;
            
            /*
             *  Check Capabilities for each packet
             * */	
	        if(packetStatusProvider.isPacketReceived(PacketFactory.IF_HIDDEN_ESSID_PACKET) == false &&
	        capabilityProvider.isConfigCapable(ICapabilityProvider.IF_HIDDEN_ESSID_CAPABILITY)){
                packetType += ConfigRequestResponsePacket.IF_HIDDEN_ESSID_REQ_RES;
            }
	        if(packetStatusProvider.isPacketReceived(PacketFactory.DOT11E_CATEGORY_INFO_PACKET) == false &&
            	capabilityProvider.isConfigCapable(ICapabilityProvider.DOT11E_CATEGORY_CONFIG_CAPABILITY)){
            	packetType += ConfigRequestResponsePacket.DOT11E_CATEGORY_CONFIG_REQ_RES;
            }
	        if(packetStatusProvider.isPacketReceived(PacketFactory.DOT11E_IFCONFIG_INFO_PACKET) == false && 
            	capabilityProvider.isConfigCapable(ICapabilityProvider.DOT11E_CONFIG_CAPABILITY)){
            	packetType += ConfigRequestResponsePacket.DOT11E_CONFIG_REQ_RES;
            }
	        if(packetStatusProvider.isPacketReceived(PacketFactory.ACL_INFO_PACKET) == false && 
                capabilityProvider.isConfigCapable(ICapabilityProvider.ACL_CONFIG_CAPABILITY)){
                packetType += ConfigRequestResponsePacket.ACL_CONFIG_REQ_RES;
            }
	        if(packetStatusProvider.isPacketReceived(PacketFactory.SIP_CONFIG_PACKET) == false &&
    	        capabilityProvider.isConfigCapable(ICapabilityProvider.SIP_CONFIG_CAPABILITY)){
	        	packetType 	+= ConfigRequestResponsePacket.EXTENSION_TO_BYTE_2;
                packetType2 += ConfigRequestResponsePacket.SIP_CONFIG_REQ_RES;
            }
	        
	    }else{
	        
	    	packetType += ConfigRequestResponsePacket.AP_CONFIG_REQ_RES;
	    	packetType += ConfigRequestResponsePacket.IP_CONFIG_REQ_RES;
	    	packetType += ConfigRequestResponsePacket.VLAN_CONFIG_REQ_RES;
            
            if(capabilityProvider.isConfigCapable(ICapabilityProvider.IF_HIDDEN_ESSID_CAPABILITY)){
                packetType += ConfigRequestResponsePacket.IF_HIDDEN_ESSID_REQ_RES;
            }
            if(capabilityProvider.isConfigCapable(ICapabilityProvider.DOT11E_CATEGORY_CONFIG_CAPABILITY)){
            	packetType += ConfigRequestResponsePacket.DOT11E_CATEGORY_CONFIG_REQ_RES;
            }
            if(capabilityProvider.isConfigCapable(ICapabilityProvider.DOT11E_CONFIG_CAPABILITY)){
            	packetType += ConfigRequestResponsePacket.DOT11E_CONFIG_REQ_RES;
            }
            if(capabilityProvider.isConfigCapable(ICapabilityProvider.ACL_CONFIG_CAPABILITY)){
            	packetType += ConfigRequestResponsePacket.ACL_CONFIG_REQ_RES;
            }
            if(capabilityProvider.isConfigCapable(ICapabilityProvider.SIP_CONFIG_CAPABILITY)){
	        	packetType 	+= ConfigRequestResponsePacket.EXTENSION_TO_BYTE_2;
                packetType2	+= ConfigRequestResponsePacket.SIP_CONFIG_REQ_RES;
            }

	    }
	    
	    if(packetType > 0 || packetType2 > 0){
		    ConfigRequestResponsePacket packet = (ConfigRequestResponsePacket) getIMCPPacket(PacketFactory.CONFIG_REQ_RES_PACKET);
		    packet.setConfigPktType(packetType);
		    packet.setConfigPktType2(packetType2);
		    packet.setCONFIG_REQ_RES_ID(IMCPInfoIDS.IMCP_IP_CONFIG_INFO_REQ_RES_INFO_ID);
		    packet.setCONFIG_REQ_RES_LENGTH(packet.getPacketLength());
		    sendPacket(packet);
	    }
		
	}
	
	void requestMissingConfiguration(boolean forceRequestAllConfig) {

		boolean 				sendAllowed				= false;
        IConfigStatusHandler 	packetStatusProvider 	= dataManager.getConfigStatusHandler();
        ICapabilityProvider		capabilityProvider		= dataManager.getCapabilityProvider();
		       
		sendGeneralConfigRequest(packetStatusProvider, capabilityProvider, forceRequestAllConfig);
		requestSupportedChannelInfo(forceRequestAllConfig, packetStatusProvider);	
		
		sendAllowed = (forceRequestAllConfig == true) ? true : 
						(packetStatusProvider.isPacketReceived(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET) == false);
        if(sendAllowed == true && capabilityProvider.isCapable(PacketFactory.AP_REG_DOMAIN_CCODE_REQUEST_PACKET) == true) {
        	sendPacket(getIMCPPacket(PacketFactory.AP_REG_DOMAIN_CCODE_REQUEST_PACKET));
        }
        
		sendAllowed = (forceRequestAllConfig == true) ? true : 
						(packetStatusProvider.isPacketReceived(PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET) == false);
		if(sendAllowed == true && capabilityProvider.isCapable(PacketFactory.AP_ACK_TIMEOUT_REQUEST_PACKET) == true) {
			sendPacket(getIMCPPacket(PacketFactory.AP_ACK_TIMEOUT_REQUEST_PACKET));
        }
       
		sendAllowed = (forceRequestAllConfig == true) ? true : 
						(packetStatusProvider.isPacketReceived(PacketFactory.EFFISTREAM_CONFIG_PACKET) == false);
		if(sendAllowed == true && capabilityProvider.isCapable(PacketFactory.EFFISTREAM_CONFIG_REQUEST_RESPONSE_PACKET) == true) {
			sendPacket(getIMCPPacket(PacketFactory.EFFISTREAM_CONFIG_REQUEST_RESPONSE_PACKET));
        } 

    	if(configuration.getCountryCode() != Mesh.CUSTOM_COUNTRY_CODE)
    		return;
		
		sendAllowed = (forceRequestAllConfig == true) ? true : 
						(packetStatusProvider.isPacketReceived(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET) == false);
		if(sendAllowed == true && capabilityProvider.isCapable(PacketFactory.RF_CUSTOM_CHANNEL_REQUEST_RESPONSE_PACKET) == true) {
			
			RFCustomChannelInfoRequestResponsePacket customChannelRequest = 
				(RFCustomChannelInfoRequestResponsePacket) getIMCPPacket(PacketFactory.RF_CUSTOM_CHANNEL_REQUEST_RESPONSE_PACKET);
			
			IInterfaceConfiguration ifConfig = configuration.getInterfaceConfiguration();
			
            for(int i=0;i<ifConfig.getInterfaceCount();i++) {
            	
            	IInterfaceInfo ifInfo = (IInterfaceInfo)ifConfig.getInterfaceByIndex(i);
            	if(ifInfo == null) {
            		continue;
            	}
            	int mediumType = ifInfo.getMediumType();
            	if(mediumType != Mesh.PHY_TYPE_802_11)
            		continue;
            	/**
            	 * according to new IMCP strucutre 
            	 */
            	customChannelRequest.setRF_CUSTOMCHANNEL_REQ_RES_ID(IMCPInfoIDS.IMCP_PRIVATE_CHANNEL_INFO_REQ_RES_INFO_ID);
            	customChannelRequest.setRF_CUSTOMCHANNEL_REQ_RES_LENGTH(customChannelRequest.getPacketLength());
            	
            	customChannelRequest.setIfName(ifInfo.getName());
            	sendPacket(customChannelRequest);
            }
        }
	
	}

	void requestConfiguration(int imcpPacketType) {
		IConfigStatusHandler requestStatusHandler = dataManager.getRequestStatusHandler();
		/* send packet and update the request status */
		requestStatusHandler.packetRequested(imcpPacketType);
	}
	
	final IConfigStatusHandler getUpdateStatusHandler() {
		return updateStatus;
	}

	void requestStaInfo(MacAddress staAddress) {
		
		updateStatus.packetRequested(PacketFactory.STA_INFO_REQUEST_PACKET);
    	StaInfoRequestPacket staRequestPacket =  
    		(StaInfoRequestPacket) getIMCPPacket(PacketFactory.STA_INFO_REQUEST_PACKET);
    	
    	if(staRequestPacket == null) {
    		return;
    	}
    	/**
    	 * according to new IMCP structure 
    	 */
    	staRequestPacket.setStaInfo_id(12);
    	staRequestPacket.setStaInfo_length(staRequestPacket.getPacketLength());
    	
    	staRequestPacket.setStaMacAddress(staAddress);
    	sendPacket(staRequestPacket);
	}

	boolean sendIMCPKeyUpdate(String newNetworkId, String newNetworkKey, String macroMessage) {

		updateStatus.packetRequested(PacketFactory.IMCP_KEY_UPDATE);
		ImcpKeyUpdatePacket keyUpdate 	= (ImcpKeyUpdatePacket) getIMCPPacket(PacketFactory.IMCP_KEY_UPDATE);
		if(keyUpdate == null) {
			return false;
		}
				
    	keyUpdate.setNewMeshId(newNetworkId);
		keyUpdate.setNewMeshIMCPKey(newNetworkKey);
		/**
		 * according to new IMCP structure 
		 */
		keyUpdate.setIMCPKEY_UPDATE_ID(IMCPInfoIDS.IMCP_MESH_IMCP_KEY_UPDATE_INFO_ID);
		keyUpdate.setIMCPKEY_UPDATE_LENGTH(keyUpdate.getPacketLength());
		sendPacket(keyUpdate);
		notificationHandler.showMessage(dataManager.getMessageSource(),macroMessage,Mesh.STATUS_CONFIG_SENT,"");
		return true;
	}

	void updateConfigurationFromFile(String fileName) {
		
		Configuration	fileConfiguration	= new Configuration();
		try{
			File 				configFile  = new File(fileName);
			FileInputStream		fileStream  = new FileInputStream(configFile);
			ConfigurationTextReader configReader = new ConfigurationTextReader(fileStream);   
			fileConfiguration.load(configReader);
		}catch(Exception e) {
			e.printStackTrace();
		}
		clearStatus();
		if(fileConfiguration.getHardwareModel().equalsIgnoreCase(configuration.getHardwareModel()) == false) {
			notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_UPDATE_NODE_SETTINGS, Mesh.STATUS_FAILED_INCORRECT_FILE,"");
			return;
		}
		
		/* since file configuration dose not contain mac address,firmware version info for the specific
		board, board mac, firmware info are explicitly set before copying*/
	
		fileConfiguration.setMacAddress(configuration.getDSMacAddress());
		
		fileConfiguration.setFirmwareMajorVersion(configuration.getFirmwareMajorVersion());
		fileConfiguration.setFirmwareMinorVersion(configuration.getFirmwareMinorVersion());
		fileConfiguration.setFirmwareVariantVersion(configuration.getFirmwareVariantVersion());
		
		fileConfiguration.compare(configuration,updateStatus);
		fileConfiguration.copyTo(configuration);
		
		updateChangedConfiguration();
	}

	void sendRebootRequest() {
       	ApResetPacket packet = (ApResetPacket) getIMCPPacket(PacketFactory.AP_RESET_REQUEST);
       	if(packet == null) {
       		return;
       	}
       	/**
       	 * according to new IMCP structure 
       	 */
       	packet.setInfo_id(IMCPInfoIDS.IMCP_RESET_INFO_ID);
       	packet.setInfo_length(packet.getPacketLength());
       	
       	packet.setResetType(ApResetPacket.RESET_TYPE_REBOOT_SYSTEM);
      	sendPacket(packet);
      	notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_REBOOT_NODES,Mesh.STATUS_REBOOTING,"");
	}

	void clearData() {
		configuration.clearData();
	}
	
	void clearStatus(){
		updateStatus.clearStatus();
	}

	void restoreDefaultSettings() {
		ApRestoreDefaultsRequestPacket apRestoreDefault = 
			(ApRestoreDefaultsRequestPacket) getIMCPPacket(PacketFactory.AP_RESTORE_DEFAULTS_REQUEST);
		if(apRestoreDefault != null) {
			apRestoreDefault.setApRestoreInfo_id(IMCPInfoIDS.IMCP_RESTORE_DEFAULTS_REQ_INFO_ID);
			apRestoreDefault.setApRestoreInfo_length(apRestoreDefault.getPacketLength());
			sendPacket(apRestoreDefault);
		}
	}

	private void requestSupportedChannelInfo(boolean forceRequestAllConfig, IConfigStatusHandler packetStatusProvider) {
		
		boolean sendAllowed = false;
		
		ApIfChannelInfoRequestPacket 	channelInfoReq 	= 
			(ApIfChannelInfoRequestPacket) getIMCPPacket(PacketFactory.AP_IF_CHANNEL_INFO_REQUEST_PACKET);

		if(channelInfoReq == null) {
			return;
		}
		
		IInterfaceConfiguration ifConfig = configuration.getInterfaceConfiguration();
		for(int i=0;i<ifConfig.getInterfaceCount();i++) {

			IInterfaceInfo ifInfo = (IInterfaceInfo)ifConfig.getInterfaceByIndex(i);
				
			if(ifInfo == null) {
				continue;
			}
			if(ifInfo.getMediumType() == Mesh.PHY_TYPE_802_3) {
				continue;
			}
			
			/*if(ifInfo.getMediumType() != Mesh.PHY_TYPE_802_11_VIRTUAL) {
				 continue;
			}*/
			if(ifInfo.getOperatingChannel() <= 0)	//most probably this interface is not active
				continue;
			
			sendAllowed = (forceRequestAllConfig == true) ? true : 
				(packetStatusProvider.isIfPacketReceived(PacketFactory.AP_IF_CHANNEL_INFO_PACKET, ifInfo.getName()) == false);

			if(sendAllowed == false)
				continue;
			channelInfoReq.setInfo_id(IMCPInfoIDS.IMCP_CHANNEL_CHANGE_INFO_ID);
			channelInfoReq.setInfo_length(channelInfoReq.getPacketLength());
			channelInfoReq.setInterfaceName(ifInfo.getName());
			sendPacket(channelInfoReq);				
		}
		
	}

	boolean updateAllConfiguration() {
		
		ICapabilityProvider		capabilityProvider		= dataManager.getCapabilityProvider();
	    
		updateStatus.clearStatus();
		
    	sendPacket(getIMCPPacket(PacketFactory.AP_CONFIGURATION_INFO));
    	updateStatus.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
    	
    	sendPacket(getIMCPPacket(PacketFactory.AP_IP_CONFIGURATION_INFO));
    	updateStatus.packetChanged(PacketFactory.AP_IP_CONFIGURATION_INFO);
    	
    	sendPacket(getIMCPPacket(PacketFactory.AP_VLAN_CONFIGURATION_INFO));
    	updateStatus.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
    	
    	sendPacket(getIMCPPacket(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET));
    	updateStatus.packetChanged(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET);
    	
    	sendPacket(getIMCPPacket(PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET));
    	updateStatus.packetChanged(PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET);
    	
    	sendPacket(getIMCPPacket(PacketFactory.IF_HIDDEN_ESSID_PACKET));
    	updateStatus.packetChanged(PacketFactory.IF_HIDDEN_ESSID_PACKET);
    	
    	if(capabilityProvider.isCapable(PacketFactory.DOT11E_CATEGORY_INFO_PACKET) == true) {
    		sendPacket(getIMCPPacket(PacketFactory.DOT11E_CATEGORY_INFO_PACKET));
    		updateStatus.packetChanged(PacketFactory.DOT11E_CATEGORY_INFO_PACKET);
    		
    	}
    	if(capabilityProvider.isCapable(PacketFactory.DOT11E_IFCONFIG_INFO_PACKET) == true) {
       		sendPacket(getIMCPPacket(PacketFactory.DOT11E_IFCONFIG_INFO_PACKET));
       		updateStatus.packetChanged(PacketFactory.DOT11E_IFCONFIG_INFO_PACKET);
       		
       	}
    	if(capabilityProvider.isCapable(PacketFactory.ACL_INFO_PACKET) == true) {
       		sendPacket(getIMCPPacket(PacketFactory.ACL_INFO_PACKET));
       		updateStatus.packetChanged(PacketFactory.ACL_INFO_PACKET);
       		
       	}
    	if(capabilityProvider.isCapable(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET) == true) {
    		IInterfaceConfiguration ifConfig = configuration.getInterfaceConfiguration();
            for(int i=0;i<ifConfig.getInterfaceCount();i++) {
            	
            	IInterfaceInfo ifInfo = (IInterfaceInfo)ifConfig.getInterfaceByIndex(i);
            	if(ifInfo == null) {
            		continue;
            	}
            	
            	int mediumType = ifInfo.getMediumType();
            	if(mediumType != Mesh.PHY_TYPE_802_11)
            		continue;
            	
            	RFCustomChannelInfoPacket customChannelInfoPacket = 
            		(RFCustomChannelInfoPacket) getIMCPPacket(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
            	
            	if(customChannelInfoPacket == null)
            		continue;

            	customChannelInfoPacket.setIfName(ifInfo.getName());
            	if(fillRfCustomChannelInfo(customChannelInfoPacket) == false) {
            		continue;
            	}
            	sendPacket(customChannelInfoPacket);
            }
            
            updateStatus.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
       	}
    	
    	if(capabilityProvider.isCapable(PacketFactory.EFFISTREAM_CONFIG_PACKET) == true) {
      		sendPacket(getIMCPPacket(PacketFactory.EFFISTREAM_CONFIG_PACKET));
      		updateStatus.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
        }

    	if(capabilityProvider.isCapable(PacketFactory.SIP_CONFIG_PACKET) == true) {
      		sendPacket(getIMCPPacket(PacketFactory.SIP_CONFIG_PACKET));
      		updateStatus.packetChanged(PacketFactory.SIP_CONFIG_PACKET);
        }
    	
    	/*if(updateStatus.isRebootRequired() == true) {
    		RebootRequestPacket	packet	= (RebootRequestPacket)PacketFactory.getNewIMCPInstance(PacketFactory.REBOOT_REQUEST_PACKET);
    		packet.setNodeId(IMCPInfoIDS.IMCP_RESET_INFO_ID);
    		packet.setNodeLength(packet.getPacketLength());
    		packet.setDsMacAddress	(configuration.getDSMacAddress());
    		packet.setMeshId		(dataManager.getMeshNetworkId());
    		sendPacket(packet);
    	}*/
    	
    	notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_UPDATE_NODE_SETTINGS,Mesh.STATUS_CONFIG_SENT_ALL,"");
    	
        return true;
		
	}

	void requestHardwareInfo() {
	    ApHwInfoRequestPacket hwInfoPacket = (ApHwInfoRequestPacket) getIMCPPacket(PacketFactory.AP_HW_INFO_REQUEST);
	    // according to new IMCP structure 
	    hwInfoPacket.setInfo_id(IMCPInfoIDS.IMCP_AP_HARDWARE_INFO_ID);
	    hwInfoPacket.setInfo_length(hwInfoPacket.getPacketLength());
	    sendPacket(hwInfoPacket);
	}	
	/***
	 * 
	 */
	public void isRebootRequired(){
	        RebootRequestPacket	packet	= (RebootRequestPacket)PacketFactory.getNewIMCPInstance(PacketFactory.REBOOT_REQUEST_PACKET);
    		packet.setNodeId(IMCPInfoIDS.IMCP_RESET_INFO_ID);
    		packet.setNodeLength(packet.getPacketLength());
    		packet.setDsMacAddress	(configuration.getDSMacAddress());
    		packet.setMeshId		(dataManager.getMeshNetworkId());
    		sendPacket(packet);		
	}
	
	void macAddr_request(){
		MacAddrInfoRequestPacket packet = (MacAddrInfoRequestPacket) getIMCPPacket(PacketFactory.IMCP_MAC_ADDR_INFO_REQUEST);
		packet.setMacAddressInfoID(IMCPInfoIDS.IMCP_RESET_INFO_ID);
		packet.setMacAddressInfoLength(packet.getPacketLength());
		packet.setDsMacAddress	(configuration.getDSMacAddress());
		packet.setMeshId		(dataManager.getMeshNetworkId());
		sendPacket(packet);		
	}
	
	public void haltNode(){
		ApResetPacket packet = (ApResetPacket) getIMCPPacket(PacketFactory.AP_RESET_REQUEST);
       	if(packet == null) {
       		return;
       	}
       	packet.setInfo_id(IMCPInfoIDS.IMCP_RESET_INFO_ID);
       	packet.setInfo_length(packet.getPacketLength());
       	packet.setResetType(ApResetPacket.RESET_TYPE_HALT_MESH);
        sendPacket(packet);
      	notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_NODE_RIGISTER,Mesh.STATUS_NODE_NOT_REGISTER,"");
	}
	
	
public boolean 	remoteNMS(String gatewayAddress,boolean enable){
	MgmtgwConfigRequestPacket gatewayRequest=(MgmtgwConfigRequestPacket) getIMCPPacket(PacketFactory.MGMT_GW_CONFIGREQUEST);
	
	if(enable)
		gatewayRequest.setMgmt_gw_enable((short) 1);
	else
		gatewayRequest.setMgmt_gw_enable((short) 0);
	gatewayRequest.setRemoteAddress(gatewayAddress);
	gatewayRequest.setCert(convertFileIntoByteArray(MFile.getConfigPath()+"/cert.txt"));
	gatewayRequest.setKey(convertFileIntoByteArray(MFile.getConfigPath()+"/key.txt"));	
	sendPacket(gatewayRequest);
	return true;
}

private byte[] convertFileIntoByteArray(String filePath){
	Path path = Paths.get(filePath);
    byte[] data = null;
	try {
		data = Files.readAllBytes(path);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    return data;
} 
/***
 * Firmware New Approche 
 */

public int firmwareUpgrading(){
	Firmware_SW_UPDATE_REQUEST packet = (Firmware_SW_UPDATE_REQUEST) getIMCPPacket(PacketFactory.IMCP_SW_UPDATE_REQUEST);
	packet.setAction(Firmware_SW_UPDATE_REQUEST.GET_KEY);
	packet.setFirmware_update_Id(IMCPInfoIDS.IMCP_FIRMWARE_REQUEST_ID);
	packet.setFirmware_update_length(packet.getPacketLength());
	sendPacket(packet);	
	try {
		copyFile(packet.getDsMacAddress().toString());
	} catch (Exception e) {
	
	}
	
	return Firmware_SW_UPDATE_REQUEST.GET_KEY;
}

public void updateFirmware(Firmware_SW_UPDATE_RESPONSE packet) {
	if(packet.getAction() == Firmware_SW_UPDATE_REQUEST.SET_KEY){
	    String key=packet.getKey();
	    sshKey(key);
	    Firmware_SW_UPDATE_REQUEST requestPacket = (Firmware_SW_UPDATE_REQUEST) getIMCPPacket(PacketFactory.IMCP_SW_UPDATE_REQUEST);
	    requestPacket.setAction( Firmware_SW_UPDATE_REQUEST.UPGRADE_INIT);
	    requestPacket.setUserName(getDirName());
	    requestPacket.setFilePath(getUpdateFileName(packet.getDsMacAddress().toString()));
	    requestPacket.setFirmware_update_Id(IMCPInfoIDS.IMCP_FIRMWARE_REQUEST_ID);
	    requestPacket.setFirmware_update_length(requestPacket.getPacketLength());	
	    /*if(System.getProperty("os.name").equals("Linux"))
	       requestPacket.setOSType((short) 11);
	    else
	    	requestPacket.setOSType((short) 10);*/
	    
	     sendPacket(requestPacket);		
	     notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_FIRMWARE,Mesh.STATUS_FIRMWARE_UPGRADING,"");
	}else if(packet.getAction() == Firmware_SW_UPDATE_REQUEST.UPGRADE_STARTING){
		 System.out.println("action starting : ===================="+packet.getAction());
		 notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_FIRMWARE,Mesh.STATUS_FIRMWARE_STARTING,"");
	}else if(packet.getAction() == Firmware_SW_UPDATE_REQUEST.IMAGE_COPY_FAILED){
		 System.out.println("action image copy failed : ================="+packet.getAction());
		 notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_FIRMWARE,Mesh.STATUS_FIRMWARE_IMAGE_COPY_FAILED,"");
	}else if(packet.getAction() == Firmware_SW_UPDATE_REQUEST.UPGRADE_FAILED){
		 System.out.println("action upgrading failed : ==================="+packet.getAction());
		 notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_FIRMWARE,Mesh.STATUS_FIRMWARE_UPGRADE_FAILED,"");
	}else if(packet.getAction() == Firmware_SW_UPDATE_REQUEST.UNKNOWN_ACTION){
		 System.out.println("UNKNOWNACTION :=================== "+packet.getAction());
		 notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_FIRMWARE,Mesh.STATUS_FIRMWARE_UPGRADE_FAILED,"");
	}else if(packet.getAction() == Firmware_SW_UPDATE_REQUEST.UPGRADE_TIMEOUT){
		 System.out.println("action upgrading Timeout : ===================="+packet.getAction());
		 notificationHandler.showMessage(dataManager.getMessageSource(),Mesh.MACRO_FIRMWARE,Mesh.STATUS_FIRMWARE_UPGRADE_FAILED,"");
	}
	
	}


public void sshKey(String key){
	String dirName=getDirName();		
	File authorized_keys=new File("C:\\cygwin64\\home\\"+dirName+"\\.ssh\\authorized_keys");
	if(!authorized_keys.exists()){
	     try {
	    	 authorized_keys.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}	    
	}
	try{
		 PrintWriter printWriter = new PrintWriter(new FileOutputStream(authorized_keys,true));
		 printWriter.print(key);
		 printWriter.println();
		 printWriter.flush();
		 printWriter.close();
	}
	catch(IOException ioe)
	{
	    System.err.println("IOException: " + ioe.getMessage());
	}
}
private String getUpdateFileName(String strMacAddress) {
	/*String fileName = strMacAddress.replace(':', '_');
	fileName += ".bin";
	if(deviceType.startsWith("CNS"))
	fileName = MFile.getUpdatesPath() + "/cns3xxx_md_3.0.1_"+fileName;
	else
		fileName = MFile.getUpdatesPath() + "/imx_md_3.0.1_"+fileName;*/
	
	String fileName=verifyUpdateFilePresent(strMacAddress);
	File file=new File(fileName);
	String dirName="/home/"+getDirName()+"/"+file.getName();
	return dirName;
}

private String verifyUpdateFilePresent(String strMacAddress) {
	
	/*String fileName = getUpdateFileName(strMacAddress,null);*/
	String fileName=null;
	String endName = strMacAddress.replace(':', '_');
	File f = new File(MFile.getUpdatesPath());
    String[] fileList=f.list();
    for(String name:fileList){	
	if(name.endsWith(endName+".bin"))
		fileName = MFile.getUpdatesPath()+"\\"+name;
}
	return fileName;
}
public  String getDirName(){
	File dir= new File("C:\\cygwin64\\home");
	String[] arr=dir.list();
	String dirName="";
	for(String name:arr)
	 if(new File("C:\\cygwin64\\home\\"+name).isDirectory())
		 dirName=name;
	
	return dirName;
}
private void copyFile(String strMacAddress) throws IOException{
	String fileName=verifyUpdateFilePresent(strMacAddress);
	String dirName=getDirName();
	File f=new File(fileName); 
	File f1=new File("C:\\cygwin64\\home\\"+dirName+"\\"+f.getName());
	Files.copy(f.toPath(),f1.toPath());
}

}
