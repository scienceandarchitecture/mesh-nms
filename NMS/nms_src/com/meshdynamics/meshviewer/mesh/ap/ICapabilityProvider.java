/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ICapabilityProvider.java
 * Comments : 
 * Created  : Apr 13, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Apr 13, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh.ap;

interface ICapabilityProvider {

	boolean isCapable		(int imcpPacketType);
	boolean isCmdCapable	(int commandType);
	
	static final short  AP_CONFIG_CAPABILITY 					= 1;
	static final short 	IP_CONFIG_CAPABILITY		  			= 2;
	static final short 	VLAN_CONFIG_CAPABILITY					= 3;
	static final short 	DOT11E_CATEGORY_CONFIG_CAPABILITY		= 4;
	static final short 	DOT11E_CONFIG_CAPABILITY				= 5;
	static final short 	IF_HIDDEN_ESSID_CAPABILITY				= 6;
	static final short 	ACL_CONFIG_CAPABILITY					= 7;
	static final short 	EXTENSION_BYTE_CAPABILITY				= 8;
	static final short 	SIP_CONFIG_CAPABILITY					= 9;
	
	/*
	 * actual config capability types are extended over multiple bytes
	 * 1 in MSB of every byte marks for extension of additional byte.
	 * capabilities are from 1 to 8 in every byte hence their values repeat
	 * in every byte. hence to check the capability the byteNo has to be provided
	 * along with the capability value.
	 */
	boolean isConfigCapable		(int configCapability);
	
}
