/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : FileHandler.java
 * Comments : 
 * Created  : Dec 15, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  4  |June 21,2006 | Firmware Update rewritten	  			         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  3  |Jan 04, 2006 |  System.out.println()'s removed			     | Mithil |
 * --------------------------------------------------------------------------------
 * |  2  |Apr 25, 2004 | Removed Print statements				         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  1  |Apr 21, 2004 | Changed from command to packet handling         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  0  |Dec 15, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh.ap;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.meshupdates.updatepackets.MeshUpdatePacketFactory;
import com.meshdynamics.meshviewer.meshupdates.updatepackets.MeshUpdateResponsePacket;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.IpAddress;

public class FileHandler {

    public static final short 	SUCCESS				= 0;
	public static final short 	SOCKET_ERROR		= 1;
	public static final short 	OUT_SOCKET_ERROR	= 2;
	private final int 			SERVER_PORT 		= 65278; //0xfefe
	
 	private Socket 				socket;
 	private DataOutputStream 	sockOut;
 	private DataInputStream 	sockIn;
	
 	public static class FileData {
 		public FileData(){
 			fileName 	= "";
 			filePath 	= "";
 			updateType 	= -1;
 		}
 		
 		public String 	fileName;
 		public String 	filePath;
 		public int		updateType;
 	}
 	
 	
	public int openConnection(IpAddress ipAddress){
		
		try{
			socket 	= new Socket(ipAddress.toString(),SERVER_PORT);
		}catch(Exception e){
			System.out.println(e.getMessage());
			socket 	= null;
			sockOut = null;
			sockIn = null;
			
			return SOCKET_ERROR;
		}
		
		try {
			sockOut = new DataOutputStream(socket.getOutputStream());			
		} catch (IOException e1) {
			try {
				socket.close();
			} catch (IOException e3) {
				System.out.println(e3.getMessage());
			}
			socket = null;
			System.out.println(e1.getMessage());
			return OUT_SOCKET_ERROR;
		}
		
		try {			
			sockIn = new DataInputStream(socket.getInputStream());
		} catch (IOException e2) {
			try {
				socket.close();
			} catch (IOException e3) {
				System.out.println(e3.getMessage());
			}
			socket = null;
			try {
				sockOut.close();
			} catch (IOException e4) {
				System.out.println(e4.getMessage());
			}
			System.out.println(e2.getMessage());
			return SOCKET_ERROR;
		}
		
		return SUCCESS;	
	}
	
	public void closeConnection(){
		
		try {
			sockIn.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			sockOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			socket.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
			
	}

	public short sendFile(byte[] fileBytes) {
		short responseCode = SOCKET_ERROR; 
		try {
			OutputStream out 	= socket.getOutputStream();
			out.write(fileBytes,0, fileBytes.length);
			out.flush();				
			responseCode = receiveReply();
		} catch (IOException e1) {
			System.out.println(e1.getMessage());
			return SOCKET_ERROR;
		}
		
		return responseCode;
	}
	
	public short receiveReply(){

		byte[]	dataToRecv		= new byte[32];
		int 	bytesRead		= 0;
		short 	responseCode;
		
		try {
			bytesRead = sockIn.read(dataToRecv);
		} catch(IOException e1){
			System.out.println(e1.getMessage());
		}
		
        BufferReader br = new BufferReader(dataToRecv, bytesRead);
		for(int i = 0; i < MeshUpdatePacketFactory.UPDATE_PACKET_SIGN_LENGTH; i++) {
			short bt = br.readByte();
			if( bt != MeshUpdatePacketFactory.signature[i]) {
				System.out.println("MSUP Signature not found !");    				
				return MeshUpdateResponsePacket.UPDATE_PACKET_RESP_TYPE_SIGN_ERR;
			}
		}

        responseCode = br.readByte();
        return responseCode;

		
	}

	/**
	 * @param dataToSend
	 * @return
	 */
	public short sendPacket(byte[] dataToSend) {
		byte[]	dataToRecv		= new byte[32];
		short 	responseCode	= -1;
		int 	bytesRead;
		
		try {
			sockOut.write(dataToSend);		
			
            bytesRead = sockIn.read(dataToRecv);
            BufferReader br = new BufferReader(dataToRecv, bytesRead);
    		for(int i = 0; i < MeshUpdatePacketFactory.UPDATE_PACKET_SIGN_LENGTH; i++) {
    			if(br.readByte() != MeshUpdatePacketFactory.signature[i])
    				return MeshUpdateResponsePacket.UPDATE_PACKET_RESP_TYPE_SIGN_ERR;
    		}

            responseCode = br.readByte();
		} catch(IOException e) {
			Mesh.logException(e);
			return SOCKET_ERROR;
		}
		return responseCode;
	}	
}
