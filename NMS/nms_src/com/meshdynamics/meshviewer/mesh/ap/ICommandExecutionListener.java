package com.meshdynamics.meshviewer.mesh.ap;

public interface ICommandExecutionListener {
	public void commandExecutionResponse(AccessPoint ap, String command, String responseStr);
}
