/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : IAPropListener.java
 * Comments : Defination of AP Properties & Mask Values
 * Created  : Oct 8, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * | 17  |May 09,2007  | restructuring and modifications				 |Abhijit |
 * -------------------------------------------------------------------------------
 * | 16  |Feb 16, 2006 | Changes for Dot11e	  						  	 | Bindu  |
 *  --------------------------------------------------------------------------------
 * | 15  |Feb 15, 2006 | All Flags Removed Rboot Reqd put in general     | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 14  |Feb 13, 2006 | Hide ESSID Implemented						     | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 13  |Feb 13, 2006 | Reject Clients removed						     | Mithil  |
 *  --------------------------------------------------------------------------------
 * | 12  |Feb 03, 2006 | DIsplay in Property UI		 					 | Mithil |
 * --------------------------------------------------------------------------------
 * | 11  |Nov 25,2005  | Reg Domain and Ack Timeout changes 1015 and 1027| Mithil |
 * -------------------------------------------------------------------------------
 * |  10 |May 26, 2005 | VLANINFO_MASK_TX_RATE added		      		 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  9  |May 10, 2005 | ModelInfo added                                 | Anand  |
 * --------------------------------------------------------------------------------
 * |  8  |May 06, 2005 | Added Latitude & Longitude 	 		         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  7  |Apr 20, 2005 | HEARTBEAT_MASK_TEMPERATURE added                | Bindu  |
 * --------------------------------------------------------------------------------
 * |  6  |Mar 25, 2005 | Temperature Property added to Stats             | Anand  |
 * --------------------------------------------------------------------------------
 * |  5  |Mar 07, 2005 | DEF_DOT11E_ENABLED added		                 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  4  |Mar 07, 2005 | Changed dot11i to dot11e		                 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  3  |Mar 04, 2005 | Vlan Masks & Dynamic Masks added                | Bindu  |
 * --------------------------------------------------------------------------------
 * |  2  |Mar 01, 2005 | Vlan & Interface Sec Info added                 | Anand  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 11, 2005 | Change Detection-PropertyUI changes             | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 8, 2004  | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh.ap;


public interface IAPPropListener {
    
    //Filter Mask
	public static final int MASK_NONE                       = 0;
    public static final int MASK_ALL 	                    = 65535;
    
    //Main Filter
    public static final int MASK_HEARTBEAT				    = 1;
    public static final int MASK_HEARTBEAT2				    = 2;
    public static final int MASK_HWINFO   				    = 4;
    public static final int MASK_APCONF				     	= 8;
    public static final int MASK_IPCONF   				    = 16;
    public static final int MASK_VLANCONF				    = 32;
    public static final int MASK_AP_STATE				    = 64;
    public static final int MASK_AP_STA_CONF			    = 128;
    
    //HeartBeat Subfilters
    public static final int HEARTBEAT_MASK_PARENTADDRESS	= 1;    
    public static final int HEARTBEAT_MASK_KNOWNAPS			= 2;	
    public static final int HEARTBEAT_MASK_STAS				= 4;
    
    //AP Config Subfilters
    public static final int APCONF_MASK_ACK_TIMEOUT			= 1;
    public static final int APCONF_MASK_HIDDEN_ESSID		= 2;
    public static final int APCONF_MASK_REG_DOMAIN			= 4;
    
    public static final int	AP_STATE_DEAD					= 1;
	public static final int	AP_STATE_SCANNING				= 2;
	public static final int	AP_STATE_STARTUP				= 4;
	public static final int	AP_STATE_ALIVE					= 8;
	public static final int	AP_STATE_DISPOSED				= 16;
	public static final int	AP_STATE_HEARTBEAT_MISSED		= 32;
	
	public static final int	AP_STA_CONF_STA_ASSOC			= 1;
	public static final int	AP_STA_CONF_STA_DISASSOC		= 2;
	public static final int	AP_STA_CONF_STA_INFO			= 4;
	
    public void apPropertyChanged(int filter, int subFilter, AccessPoint src);
    
}
