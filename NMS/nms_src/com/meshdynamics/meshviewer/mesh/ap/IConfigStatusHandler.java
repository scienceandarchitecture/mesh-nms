/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : IConfigStatusHandler.java
 * Comments : 
 * Created  : Apr 16, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  4  |Aug  1, 2007 | essIdChangedFor method added					 | Imran  |
 * --------------------------------------------------------------------------------
 * |  3  |May 10, 2007 | iConfigStatusProvider merged					 | Abhijit|
 * --------------------------------------------------------------------------------
 * |  2  |May 07, 2007 | isPacketChanged added                           | Imran  |
 * --------------------------------------------------------------------------------
 * |  1  |May 02, 2007 | securityChanged added                           | Imran  |
 * --------------------------------------------------------------------------------
 * |  0  |Apr 16, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh.ap;

public interface IConfigStatusHandler {

	public void 	clearStatus						();
	public void 	clearPacketStatus				(int imcpPacketType);
	public void 	packetReceived					(int imcpPacketType);
	public void 	packetChanged					(int imcpPacketType);
	public void 	packetRequested					(int imcpPacketType);
	public void 	securityChanged					(boolean changed);
	public void 	essIdChangedFor					(String ifName);
	public void 	manualChannelChanged			(boolean changed);
	
	public boolean 	isPacketReceived				(int imcpPacketType);
	public boolean 	isPacketChanged					(int imcpPacketType);
	public boolean 	isPacketRequested				(int imcpPacketType);
		
	public boolean 	isAnyPacketChanged				();
	
	public int 		getTotalStatusCount				();
	public int 		getPendingStatusCount			();
	
	public void 	ifPacketRequested				(int imcpPacketType, String ifName);
	public void 	ifPacketReceived				(int imcpPacketType, String ifName);
	public boolean 	isIfPacketRequested				(int imcpPacketType, String ifName);
	public boolean 	isIfPacketReceived				(int imcpPacketType, String ifName);
	public void     advanceInfoChanged              (boolean changed);
	public void     isProtocolChanged               (boolean changed);
	public void     isHopCostChanged                (boolean changed);
	
	public void     isResponseStatus                (boolean status);
	public boolean  getResponseStatus               ();
	public void     setResponseStatus               (boolean status);
}
