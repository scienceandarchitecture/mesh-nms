/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ConfigurationStatus.java
 * Comments : 
 * Created  : Apr 16, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *   --------------------------------------------------------------------------------
 * |  4  |Aug 01, 2007 | essIdChangedFor method added 				     | Imran  |
 *  --------------------------------------------------------------------------------
 * |  3  |May 07, 2007 | changes due to IConfigStatusHandler merging     | Abhijit|
 * --------------------------------------------------------------------------------
 * |  2  |May 07, 2007 | isPacketChanged added                           | Imran  |
 * --------------------------------------------------------------------------------
 * |  1  |May 02, 2007 | securityChanged added                           | Imran  |
 * --------------------------------------------------------------------------------
 * |  0  |Apr 16, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh.ap;

import java.util.Hashtable;

import com.meshdynamics.meshviewer.imcppackets.PacketFactory;

public class ConfigurationStatus implements IConfigStatusHandler {

	private Hashtable<String, String> 	statusTable;
	private boolean 					rebootRequired;
	private int							packetCount;
	private boolean                     status;                  
	
	ConfigurationStatus() {
		statusTable = new Hashtable<String, String>();
	}
	
	public void clearStatus() {
		statusTable.clear();
		rebootRequired 	= false;
		status          = false;
		packetCount		= 0;
	}

	public void clearPacketStatus(int imcpPacketType) {
		statusTable.remove(""+imcpPacketType);
	}
	
	public void packetReceived(int imcpPacketType) {
		statusTable.put(""+imcpPacketType, ""+imcpPacketType);
		packetCount++;
	}
	
	public void packetChanged(int imcpPacketType) {
		
		if(statusTable.contains(""+imcpPacketType) == false){
		
			statusTable.put(""+imcpPacketType, ""+imcpPacketType);
			packetCount++;
		}
		
		if( imcpPacketType == PacketFactory.AP_VLAN_CONFIGURATION_INFO ||
			imcpPacketType == PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET ||
			imcpPacketType == PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET ||
			imcpPacketType == PacketFactory.EFFISTREAM_CONFIG_PACKET) {
			rebootRequired = true;
		}
	}
	
	public void packetRequested(int imcpPacketType) {
		statusTable.put(""+imcpPacketType, ""+imcpPacketType);
		packetCount++;
	}

	public boolean isPacketReceived(int imcpPacketType) {
		return statusTable.containsKey(""+imcpPacketType);
	}
	
	public boolean isPacketChanged(int imcpPacketType) {
		return statusTable.containsKey(""+imcpPacketType);
	}
	
	public boolean isPacketRequested(int imcpPacketType) {
		return statusTable.containsKey(""+imcpPacketType);
	}
	
	public boolean isRebootRequired() {
		return rebootRequired;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IConfigStatusHandler#securityChanged(boolean)
	 */
	public void securityChanged(boolean changed) {
		if(changed == true) {
			rebootRequired	= true;
		}
		//rebootRequired = (changed == true) ? true : false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IConfigStatusHandler#isPacketChanged()
	 */
	public boolean isAnyPacketChanged() {
		return (statusTable.size() == 0) ? false : true;
	}

	public int getPendingStatusCount() {
		return statusTable.size();
	}

	public int getTotalStatusCount() {
		return packetCount;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IConfigStatusHandler#essIdChangedFor(java.lang.String)
	 */
	public void essIdChangedFor(String ifName) {
		if((ifName.equalsIgnoreCase("ixp0")) || 
				(ifName.equalsIgnoreCase("ixp1"))) {
			rebootRequired	= true;
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IConfigStatusHandler#manualChannelChanged(boolean)
	 */
	public void manualChannelChanged(boolean changed) {
		if(changed == true) {
			rebootRequired = true;
		}
	}

	public void ifPacketReceived(int imcpPacketType, String ifName) {
		statusTable.put(imcpPacketType+":"+ifName, imcpPacketType+":"+ifName);
		packetCount++;
	}

	public void ifPacketRequested(int imcpPacketType, String ifName) {
		statusTable.put(imcpPacketType+":"+ifName, imcpPacketType+":"+ifName);
		packetCount++;
	}

	public boolean isIfPacketReceived(int imcpPacketType, String ifName) {
		return statusTable.containsKey(imcpPacketType+":"+ifName);
	}

	public boolean isIfPacketRequested(int imcpPacketType, String ifName) {
		return statusTable.containsKey(imcpPacketType+":"+ifName);	
	}

	@Override
	public void advanceInfoChanged(boolean changed) {
		if(changed == true) {
			rebootRequired	= true;
		}
	}

	@Override
	public void isProtocolChanged(boolean changed) {
		if(changed == true) {
			rebootRequired	= true;
		}
	}

	@Override
	public void isHopCostChanged(boolean changed) {
		if(changed == true) {
			rebootRequired	= true;
		}		
	}

	@Override
	public void isResponseStatus(boolean status) {
		if(status == true) {
			this.status	= true;
		}		
	}

	@Override
	public boolean getResponseStatus() {
		return status;
	}

	@Override
	public void setResponseStatus(boolean status) {
		if(status == false) {
			this.status	= false;
		}
	}
	
}
