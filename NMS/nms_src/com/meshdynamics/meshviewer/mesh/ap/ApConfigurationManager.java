/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApConfigurationManager.java
 * Comments : 
 * Created  : Apr 9, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  8  |Nov, 06,2007 | cCode changed check added setApRegDmnCntCodeInfo|Imran   |
 * --------------------------------------------------------------------------------
 * |  7  |Jun, 08,2007 | SaturationInfo copying corrected				 |Imran   |
 * --------------------------------------------------------------------------------
 * |  6  |May  24,2007 | setSaturationInfo impemented  					 |Imran   |
 * --------------------------------------------------------------------------------
 * |  5  |May  3, 2007 | copySecurityInfo and copyRSNPSKInfo modified    |Imran   |
   --------------------------------------------------------------------------------
 * |  4  |Apr 26, 2007 | apConfig info packet copying code added         |Imran   |
 * --------------------------------------------------------------------------------
 * |  3  |Apr 26, 2007 | packet copying code added                       |Abhijit |
 * --------------------------------------------------------------------------------
 * |  2  |Apr 26, 2007 | setIpConfig added                               |Abhishek|
 * --------------------------------------------------------------------------------
 * |  1  |Apr 26, 2007 | RFCustom,ApIfChannel,RegDomain pkt added        |Abhishek|
 * --------------------------------------------------------------------------------
 * |  0  |Apr 9, 2007  | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh.ap;

import java.util.Enumeration;

import com.meshdynamics.meshviewer.configuration.I80211eCategoryConfiguration;
import com.meshdynamics.meshviewer.configuration.I80211eCategoryInfo;
import com.meshdynamics.meshviewer.configuration.IACLConfiguration;
import com.meshdynamics.meshviewer.configuration.IACLInfo;
import com.meshdynamics.meshviewer.configuration.IChannelInfo;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IEffistreamAction;
import com.meshdynamics.meshviewer.configuration.IEffistreamConfiguration;
import com.meshdynamics.meshviewer.configuration.IEffistreamRule;
import com.meshdynamics.meshviewer.configuration.IEffistreamRuleCriteria;
import com.meshdynamics.meshviewer.configuration.IInterfaceAdvanceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceChannelConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;
import com.meshdynamics.meshviewer.configuration.INetworkConfiguration;
import com.meshdynamics.meshviewer.configuration.IPSKConfiguration;
import com.meshdynamics.meshviewer.configuration.IRadiusConfiguration;
import com.meshdynamics.meshviewer.configuration.ISaturationInfo;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipStaInfo;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.meshviewer.configuration.IWEPConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.InterfaceAdvanceConfiguration;
import com.meshdynamics.meshviewer.imcppackets.ACLInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApAckTimeoutInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApConfigInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApHwInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApIfChannelInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApIpConfigInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApRegDomainInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.DLSaturationInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.Dot11eCategoryPacket;
import com.meshdynamics.meshviewer.imcppackets.Dot11eIfInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.EffistreamConfigPacket;
import com.meshdynamics.meshviewer.imcppackets.EffistreamConfigPacket.ActionInfo;
import com.meshdynamics.meshviewer.imcppackets.EffistreamConfigPacket.RuleInfo;
import com.meshdynamics.meshviewer.imcppackets.IfHiddenESSIDInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.imcppackets.RFCustomChannelInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.RFCustomChannelInfoPacket.RFChannelInfo;
import com.meshdynamics.meshviewer.imcppackets.SipConfigPacket;
import com.meshdynamics.meshviewer.imcppackets.VLanConfigInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.helpers.ACLInfoDetails;
import com.meshdynamics.meshviewer.imcppackets.helpers.ApAckInterfaceTimeoutInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.ApIfChannelInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.Dot11eCategoryDetails;
import com.meshdynamics.meshviewer.imcppackets.helpers.Dot11eIfInfoDetails;
import com.meshdynamics.meshviewer.imcppackets.helpers.EffistreamHelper;
import com.meshdynamics.meshviewer.imcppackets.helpers.IfHiddenESSIDInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.InterfaceAdvanceInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.InterfaceConfigInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.RADSecurityInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.RSNPSKSecurityInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.SecurityInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.SipStaInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.VLanConfigInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.WEPSecurityInfo;
import com.meshdynamics.meshviewer.mesh.ap.HardwareInfo.InterfaceData;

public class ApConfigurationManager {

	private IConfiguration			configuration;
	private ConfigurationStatus		receiveStatus;
	private ConfigurationStatus		requestStatus;
	
	ApConfigurationManager(ApDataController dataManager,final IConfiguration configuration) {
		this.configuration		= configuration;
		receiveStatus			= new ConfigurationStatus();
		this.requestStatus		= new ConfigurationStatus();
	}

	void setHwConfigInfo(ApHwInfoPacket hwInfoPacket) {
		
		configuration.setMacAddress(hwInfoPacket.getDsMacAddress());
		configuration.setFirmwareMajorVersion(hwInfoPacket.getFirmwareMajorVersion());
		configuration.setFirmwareMinorVersion(hwInfoPacket.getFirmwareMinorVersion());
		configuration.setFirmwareVariantVersion(hwInfoPacket.getFirmwareVariantVersion());
		configuration.setNodeName(hwInfoPacket.getApName());
		configuration.setHardwareModel(hwInfoPacket.getHardwareModel());
		IInterfaceConfiguration ifConfig = configuration.getInterfaceConfiguration();
		
		InterfaceData 	hwIfData 	= hwInfoPacket.getDsInterfaceInfo();
		
		if(hwIfData != null) {
			
			IInterfaceInfo 	ifInfo 		= (IInterfaceInfo)ifConfig.addInterfaceByName(hwIfData.getName());
			if(ifInfo != null){
				ifInfo.setMacAddress(hwInfoPacket.getDsMacAddress());
				ifInfo.setMediumType((short) hwIfData.getIfType());
				ifInfo.setOperatingChannel((short) hwIfData.getChannel());
				
				InterfaceData[] wmInfo = hwInfoPacket.getWmInterfaceInfo();
				
				for(int i=0;i<wmInfo.length;i++) {
					ifInfo = (IInterfaceInfo)ifConfig.addInterfaceByName(wmInfo[i].getName());
					
					ifInfo.setMacAddress(wmInfo[i].getMacAddr());
					ifInfo.setMediumType((short) wmInfo[i].getIfType());
					ifInfo.setMediumSubType((short) wmInfo[i].getIfSubType());
					ifInfo.setOperatingChannel((short) wmInfo[i].getChannel());
				}
				hwIfData = hwInfoPacket.getMonInterface();
				if(hwIfData != null){
					ifInfo = (IInterfaceInfo)ifConfig.addInterfaceByName(hwIfData.getName());
					if(ifInfo != null){
						ifInfo.setMacAddress(hwIfData.getMacAddr());
						ifInfo.setMediumType((short) hwIfData.getIfType());
						ifInfo.setMediumSubType((short) hwIfData.getIfSubType());
					}
				}
			}
		}
		receiveStatus.packetReceived(PacketFactory.AP_HW_INFO);
	}
	
	void setApConfigInfo(ApConfigInfoPacket newApConfig) {
		
		int		ifIndex;
		short 	dcaListCount;
		int 	dcaListIndex;
		short[]	dcaList;
				
		configuration.setNodeName(newApConfig.getApName());
		configuration.setDescription(newApConfig.getDescription());
		configuration.setLatitude(newApConfig.getLatitude());
		configuration.setLongitude(newApConfig.getLongitude());
		configuration.setPreferedParent(newApConfig.getPreferredParent().toString());
		
		IMeshConfiguration	meshConfig	= configuration.getMeshConfiguration();
		if(meshConfig != null){
			meshConfig.setSignalMap(newApConfig.getSignalMap());
			meshConfig.setHeartbeatInterval(newApConfig.getHeartbeatInterval());
			meshConfig.setHeartbeatMissCount(newApConfig.getHeartbeatMissCount());
			meshConfig.setHopCost(newApConfig.getHopCost());
			meshConfig.setLocationAwarenessScanInterval(newApConfig.getLasi());
			meshConfig.setEssid(newApConfig.getEssid());
			meshConfig.setRTSThreshold(newApConfig.getRtsThreshold());
			meshConfig.setFragThreshold(newApConfig.getFragThreshold());
			meshConfig.setBeaconInterval(newApConfig.getBeaconInterval());
			meshConfig.setChangeResistanceThreshold(newApConfig.getCrThreshold());
			meshConfig.setMaxAllowableHops(newApConfig.getMaxAllowableHops());
			meshConfig.setFCCCertifiedOperation(newApConfig.getFCC());
			meshConfig.setETSICertifiedOperation(newApConfig.getETSI());
			meshConfig.setDSTxRate(newApConfig.getDsTxRate());
			meshConfig.setDSTxPower(newApConfig.getDsTxPower());
			meshConfig.setDynamicChannelAllocation(newApConfig.getDynamicChannelAlloc());
			meshConfig.setStayAwakeCount(newApConfig.getStayAwakeCount());
			meshConfig.setBridgeAgeingTime(newApConfig.getBridgeAgeingTime());
		}
		
		IInterfaceConfiguration	interfaceConfiguration	= configuration.getInterfaceConfiguration();
		if(interfaceConfiguration == null) {
			return;
		}
			
		InterfaceConfigInfo[]	receivedInterfaceInfo	= newApConfig.getInterfaces();
		if(receivedInterfaceInfo == null){
			return;
		}
		for(ifIndex = 0; ifIndex < receivedInterfaceInfo.length; ifIndex++) {
			
			String	ifName	= receivedInterfaceInfo[ifIndex].getName();
			
			IInterfaceInfo	interfaceInfo	= (IInterfaceInfo)interfaceConfiguration.addInterfaceByName(ifName);
			if(interfaceInfo == null){
				continue;
			}
			//id
			//length 
			interfaceInfo.setName(receivedInterfaceInfo[ifIndex].getName());
			interfaceInfo.setMediumType(receivedInterfaceInfo[ifIndex].getMediumType());
			interfaceInfo.setMediumSubType(receivedInterfaceInfo[ifIndex].getMediumSubType());
			interfaceInfo.setUsageType(receivedInterfaceInfo[ifIndex].getUsageType());
			interfaceInfo.setChannelBondingType(receivedInterfaceInfo[ifIndex].getChannelBondingType());
			interfaceInfo.setChannel(receivedInterfaceInfo[ifIndex].getChannel());
			interfaceInfo.setDynamicChannelAlloc(receivedInterfaceInfo[ifIndex].getDynamicChannelAlloc());
			interfaceInfo.setTxPower(receivedInterfaceInfo[ifIndex].getTxPower());
			interfaceInfo.setTxRate(receivedInterfaceInfo[ifIndex].getTxRate());
			interfaceInfo.setServiceType(receivedInterfaceInfo[ifIndex].getServiceType());
			interfaceInfo.setEssid(receivedInterfaceInfo[ifIndex].getEssid());
			interfaceInfo.setRtsThreshold(receivedInterfaceInfo[ifIndex].getRtsThreshold());
			interfaceInfo.setFragThreshold(receivedInterfaceInfo[ifIndex].getFragThreshold());
			interfaceInfo.setBeaconInterval(receivedInterfaceInfo[ifIndex].getBeaconInt());
			
			dcaListCount	= receivedInterfaceInfo[ifIndex].getDcaListCount();
			interfaceInfo.setDcaListCount(dcaListCount);
			
			dcaList	= receivedInterfaceInfo[ifIndex].getDcaList();
			
			for(dcaListIndex = 0; dcaListIndex < dcaListCount; dcaListIndex++) {
				interfaceInfo.setDcaChannel(dcaListIndex,dcaList[dcaListIndex]); 
			}
			
			ISecurityConfiguration	 dstSecurityInfo = (ISecurityConfiguration)interfaceInfo.getSecurityConfiguration(); 
			SecurityInfo			 srcSecurityInfo = receivedInterfaceInfo[ifIndex].getSecurityInfo(); 
			if((srcSecurityInfo == null) || (dstSecurityInfo == null)){
				continue;
			}
			copySecurityInfo(srcSecurityInfo,dstSecurityInfo); 
			/**
			 * find extension exist or not,
			 * if exist parse it 
			 */
			if (mostSignificantBit(receivedInterfaceInfo[ifIndex].getInterfaceLEN()) == 15){
				 IInterfaceAdvanceConfiguration interfaceadvanceInfo=new InterfaceAdvanceConfiguration();
						 interfaceInfo.getInterfaceAdvanceConfiguration();
						 InterfaceAdvanceInfo advanceInfo=receivedInterfaceInfo[ifIndex].getInterfaceAdvanceInfo();
						 if(advanceInfo != null){
				              copyInterfaceAdvanceInfo(advanceInfo,interfaceadvanceInfo);
				              interfaceInfo.setInterfaceAdvanceConfiguration(interfaceadvanceInfo);
						 }
			}else{
				interfaceInfo.setInterfaceAdvanceConfiguration(new InterfaceAdvanceConfiguration());
			}
			
		}
		receiveStatus.packetReceived(PacketFactory.AP_CONFIGURATION_INFO);
		
	}
	private void copyInterfaceAdvanceInfo(
			InterfaceAdvanceInfo interfaceAdvanceInfo, IInterfaceAdvanceConfiguration advanceInfo) {
		
		advanceInfo.setChannelBandwidth((byte)interfaceAdvanceInfo.getChannelBandwidth());
		advanceInfo.setSecondaryChannelPosition((byte)interfaceAdvanceInfo.getSecondaryChannelPosition());
		advanceInfo.setFrameAggregation((byte)interfaceAdvanceInfo.getFrameAggregation());
		advanceInfo.setGuardInterval_20((byte)interfaceAdvanceInfo.getGuardInterval_20());
		advanceInfo.setGuardInterval_40((byte)interfaceAdvanceInfo.getGuardInterval_40());
		advanceInfo.setGuardInterval_80((byte)interfaceAdvanceInfo.getGuardInterval_80());
		advanceInfo.setLDPC((byte)interfaceAdvanceInfo.getLdpc());
		advanceInfo.setTxSTBC((byte)interfaceAdvanceInfo.getTxSTBC());
		advanceInfo.setRxSTBC((byte)interfaceAdvanceInfo.getRxSTBC());
		advanceInfo.setCoexistence((byte)interfaceAdvanceInfo.getCoexistence());
		advanceInfo.setGFMode((byte)interfaceAdvanceInfo.getGfMode());
		advanceInfo.setMaxAMPDU((byte)interfaceAdvanceInfo.getMaxAMPDU());
		advanceInfo.setMaxAMSDU((byte)interfaceAdvanceInfo.getMaxAMSDU());
		advanceInfo.setMaxMPDU((byte)interfaceAdvanceInfo.getMaxMPDU());
		advanceInfo.setMaxAMPDUEnabled((byte) interfaceAdvanceInfo.getMaxAMPDUEnabled());
		advanceInfo.setMaxMPDU((short) interfaceAdvanceInfo.getMaxMPDU());
	}

	/**
	 * @param srcSecurityInfo
	 * @param dstSecurityInfo
	 */
	private void copySecurityInfo(SecurityInfo srcSecurityInfo, ISecurityConfiguration dstSecurityInfo) {
		
		if(srcSecurityInfo.getNoneEnabled() == 1) {
			dstSecurityInfo.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_NONE);
		}
		if(srcSecurityInfo.getRADEnabled() == 1) {
			dstSecurityInfo.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_WPA_ENTERPRISE);
			IRadiusConfiguration	dstRADInfo	= (IRadiusConfiguration)dstSecurityInfo.getRadiusConfiguration();
			RADSecurityInfo			srcRADInfo	= srcSecurityInfo.getRADInfo();
			if((dstRADInfo != null) && (srcRADInfo != null))
			copyRADInfo(dstRADInfo, srcRADInfo);
		}
		if(srcSecurityInfo.getRSNPSKEnabled() == 1){
			dstSecurityInfo.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_WPA_PERSONAL);
			IPSKConfiguration	dstPSKInfo	= (IPSKConfiguration)dstSecurityInfo.getPSKConfiguration();
			RSNPSKSecurityInfo	srcPSKInfo	= srcSecurityInfo.getRSNPSKInfo();
			if((dstPSKInfo != null) && (srcPSKInfo != null)) {
				copyRSNPSKInfo(dstPSKInfo, srcPSKInfo);
			}
		}
		if(srcSecurityInfo.getWEPEnabled()	== 1 ){
			dstSecurityInfo.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_WEP);
			IWEPConfiguration	dstWEPInfo	= (IWEPConfiguration)dstSecurityInfo.getWEPConfiguration();
			WEPSecurityInfo		srcWEPInfo	= srcSecurityInfo.getWEPInfo();
			if((dstWEPInfo != null) && (srcWEPInfo != null)){
				copyWEPInfo(srcWEPInfo, dstWEPInfo);
			}
		}
	}

	private void copyRADInfo(IRadiusConfiguration dstRADInfo, RADSecurityInfo srcRADInfo) {
		
		dstRADInfo.setCipherCCMP(srcRADInfo.getRADCipherCCMP());
		dstRADInfo.setCipherTKIP(srcRADInfo.getRADCipherTKIP());
		dstRADInfo.setGroupKeyRenewal(srcRADInfo.getRADGroupKeyRenewal());
		dstRADInfo.setKey(srcRADInfo.getRADKey());
		dstRADInfo.setMode(srcRADInfo.getRADMode());
		dstRADInfo.setPort(srcRADInfo.getRADPort());
		dstRADInfo.setServerIPAddress(srcRADInfo.getRADIPAddress());
		dstRADInfo.setWPAEnterpriseDecidesVlan(srcRADInfo.getRadDecidesVlan());
	}

	/**
	 * @param dstPSKInfo
	 * @param srcPSKInfo
	 */
	private void copyRSNPSKInfo(IPSKConfiguration dstPSKInfo, RSNPSKSecurityInfo srcPSKInfo) {
		
		dstPSKInfo.setPSKCipherCCMP(srcPSKInfo.getPSKCipherCCMP());
		dstPSKInfo.setPSKCipherTKIP(srcPSKInfo.getPSKCipherTKIP());
		dstPSKInfo.setPSKGroupKeyRenewal(srcPSKInfo.getPSKGroupKeyRenewal());
		dstPSKInfo.setPSKKeyLength(srcPSKInfo.getPSKKeyLength());
		dstPSKInfo.setPSKKey(srcPSKInfo.getPSKKey());
		dstPSKInfo.setPSKMode(srcPSKInfo.getPSKMode());
		dstPSKInfo.setPSKPassPhrase(srcPSKInfo.getPSKPassPhrase());
	}

	/**
	 * @param srcWEPInfo
	 * @param dstWEPInfo
	 */
	private void copyWEPInfo(WEPSecurityInfo srcWEPInfo, IWEPConfiguration dstWEPInfo) {
		
		dstWEPInfo.setKey(0, srcWEPInfo.getWEPKey0());
		dstWEPInfo.setKey(1, srcWEPInfo.getWEPKey1());
		dstWEPInfo.setKey(2, srcWEPInfo.getWEPKey2());
		dstWEPInfo.setKey(3, srcWEPInfo.getWEPKey3());
		
		dstWEPInfo.setKeyIndex(srcWEPInfo.getWEPKeyIndex());
		dstWEPInfo.setKeyStrength(srcWEPInfo.getWEPStrength());
		dstWEPInfo.setPassPhrase(srcWEPInfo.getWEPPassPhrase());
	}
	
	void setIpConfigInfo(ApIpConfigInfoPacket newIpConfig) {
		
		INetworkConfiguration	networkConfiguration	= configuration.getNetworkConfiguration();
		
		if(networkConfiguration == null) {
			return; 
		}
		networkConfiguration.setHostName(newIpConfig.getHostName());	
		
		networkConfiguration.setGateway(newIpConfig.getGateway());
		networkConfiguration.setIpAddress(newIpConfig.getIpAddress());
		networkConfiguration.setSubnetMask(newIpConfig.getSubnetMask());
		
		receiveStatus.packetReceived(PacketFactory.AP_IP_CONFIGURATION_INFO);
	}

	
	void setVlanConfigInfo(VLanConfigInfoPacket newVlanConfig) {
		
		IVlanConfiguration vlanConfig = configuration.getVlanConfiguration();
		
		vlanConfig.setDefault80211eCategoryIndex(newVlanConfig.getDefaultDot11e());
		vlanConfig.setDefault80211eEnabled(newVlanConfig.getDefaultDot11eEnabled());
		vlanConfig.setDefault8021pPriority(newVlanConfig.getDefaultDot1p());
		vlanConfig.setDefaultTag((short) newVlanConfig.getDefaultTag());
		
		vlanConfig.setVlanCount(newVlanConfig.getVlanCount());
		
		for(int i=0;i<vlanConfig.getVlanCount();i++) {
			
			IVlanInfo 		dstVlanInfo = (IVlanInfo)vlanConfig.getVlanInfoByIndex(i);
			VLanConfigInfo 	srcVlanInfo	= newVlanConfig.getVLanConfig(i);
			
			dstVlanInfo.set80211eCategoryIndex(srcVlanInfo.getDot11e());
			dstVlanInfo.set80211eEnabled(srcVlanInfo.getDot11eEnabled());
			dstVlanInfo.set8021pPriority(srcVlanInfo.getDot1p());
			dstVlanInfo.setBeaconInterval(srcVlanInfo.getBeaconInt());
			dstVlanInfo.setEssid(srcVlanInfo.getEssid());
			dstVlanInfo.setFragThreshold(srcVlanInfo.getFragThreshold());
			dstVlanInfo.setIpAddress(srcVlanInfo.getIpAddress());
			dstVlanInfo.setName(srcVlanInfo.getName());
			dstVlanInfo.setRtsThreshold(srcVlanInfo.getRtsThreshold());
			dstVlanInfo.setServiceType(srcVlanInfo.getServiceType());
			dstVlanInfo.setTag(srcVlanInfo.getTag());
			dstVlanInfo.setTxPower(srcVlanInfo.getTxPower());
			dstVlanInfo.setTxRate(srcVlanInfo.getTxRate());
			
			
			ISecurityConfiguration 	dstSecurityInfo = (ISecurityConfiguration)dstVlanInfo.getSecurityConfiguration();
			SecurityInfo 			srcSecurityInfo = srcVlanInfo.getSecurityInfo();
			if((dstSecurityInfo == null) || (srcSecurityInfo == null)){
				continue;
			}
				copySecurityInfo(srcSecurityInfo,dstSecurityInfo);
	
		}
		
		receiveStatus.packetReceived(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
	}
	
	
	void setApRegDmnCntCodeInfo(ApRegDomainInfoPacket regDmnCntCodeInfoConfig) {
		
		int cCode	= configuration.getCountryCode();
		if(cCode != regDmnCntCodeInfoConfig.getCountryCode()) {
			configuration.setCountryCodeChanged(true);
		} else {
			configuration.setCountryCodeChanged(false);
		}
		configuration.setCountryCode(regDmnCntCodeInfoConfig.getCountryCode());
		configuration.setRegulatoryDomain(regDmnCntCodeInfoConfig.getRegulatoryDomain());
		
		receiveStatus.packetReceived(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET); 
	}

	void setApAckTimeoutInfo(ApAckTimeoutInfoPacket ackTimeoutInfoConfig) {
		
		IInterfaceConfiguration ifConfig = configuration.getInterfaceConfiguration();
		
		for(int i=0;i<ackTimeoutInfoConfig.getICnt();i++) {
			
			ApAckInterfaceTimeoutInfo 	srcInfo = ackTimeoutInfoConfig.getIfRefByIndex(i);
			IInterfaceInfo 				dstInfo	= (IInterfaceInfo)ifConfig.getInterfaceByName(srcInfo.ifName);
			
			if(dstInfo == null) {
				continue;
			}
			
			dstInfo.setAckTimeout(srcInfo.ackTimeout);
		}
		
		receiveStatus.packetReceived(PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET);
	}

	void setIfHiddenESSIDInfo(IfHiddenESSIDInfoPacket hiddenESSIDInfoConfig) {

		IInterfaceConfiguration ifConfig = configuration.getInterfaceConfiguration();
		
		for(int i=0;i<hiddenESSIDInfoConfig.getICnt();i++) {
			
			IfHiddenESSIDInfo 	srcInfo = hiddenESSIDInfoConfig.getIfRefByIndex(i);
			IInterfaceInfo 		dstInfo	= (IInterfaceInfo)ifConfig.getInterfaceByName(srcInfo.ifName);
			
			if(dstInfo == null) {
				continue;
			}
			
			dstInfo.setEssidHidden(srcInfo.hidden);
		}
		
		receiveStatus.packetReceived(PacketFactory.IF_HIDDEN_ESSID_PACKET);
	}

	void setACLInfo(ACLInfoPacket aclInfopkt) {
		
		IACLConfiguration	aclConfiguration	= configuration.getACLConfiguration();
		
		if(aclConfiguration == null) {
			return;
		}
		
		int i	= 0;
		int cnt	= aclInfopkt.getACLCount();
		aclConfiguration.setCount(aclInfopkt.getACLCount());
		
		
		for(i = 0; i < cnt; i++) {
			
			IACLInfo	aclInfo	= (IACLInfo)aclConfiguration.getACLInfo(i);
			
			if(aclInfo == null) {
				continue;
			}
			ACLInfoDetails	aclPktInfo	= aclInfopkt.getRuleRefByIndex(i);
			aclInfo.set80211eCategoryIndex(aclPktInfo.getCategory11e());
			aclInfo.setAllow(aclPktInfo.getAllow());
			aclInfo.setEnable80211eCategory(aclPktInfo.getEnable11e());
			aclInfo.setStaAddress(aclPktInfo.getStaMac());
			aclInfo.setVLANTag(aclPktInfo.getVLANTag());
		}
		receiveStatus.packetReceived(PacketFactory.ACL_INFO_PACKET);			
	}

	void setDot11eCategoryInfo(Dot11eCategoryPacket newDot11eConfig) {

		I80211eCategoryConfiguration 	categoryConfig 	= configuration.get80211eCategoryConfiguration();
		int 							categoryCount 	= newDot11eConfig.getCategoryCount();
		
		categoryConfig.setCount(categoryCount);
		
		for(int i=0;i<categoryCount;i++) {
			
			Dot11eCategoryDetails 	srcInfo = newDot11eConfig.getdDot11eCategoryRefByIndex(i);
			I80211eCategoryInfo 	dstInfo	= (I80211eCategoryInfo)categoryConfig.getCategoryInfo(i);
			
			if(dstInfo == null) {
				continue;
			}
			
			dstInfo.setACWMax(srcInfo.aCWmax);
			dstInfo.setACWMin(srcInfo.aCWmin);
			dstInfo.setAIFSN(srcInfo.aifsn);
			dstInfo.setBurstTime(srcInfo.burstTime);
			dstInfo.setDisableBackoff(srcInfo.disableBackoff);
			dstInfo.setName(srcInfo.categoryName);
			
		}
		
		receiveStatus.packetReceived(PacketFactory.DOT11E_CATEGORY_INFO_PACKET);			
	}

	void setDot11eIfInfo(Dot11eIfInfoPacket newDot11eIfInfo) {

		IInterfaceConfiguration ifConfig = configuration.getInterfaceConfiguration();
		
		for(int i=0;i<newDot11eIfInfo.getInterfaceCount();i++) {
			
			Dot11eIfInfoDetails 	srcInfo = newDot11eIfInfo.getIfRefByIndex(i);
			IInterfaceInfo 			dstInfo	= (IInterfaceInfo)ifConfig.getInterfaceByName(srcInfo.getInterfaceName());
			
			if(dstInfo == null) {
				continue;
			}
			
			dstInfo.setDot11eCategory(srcInfo.getIfDot11eCategory());
			dstInfo.setDot11eEnabled(srcInfo.getIfDot11eEnabled());
		}
		
		receiveStatus.packetReceived(PacketFactory.DOT11E_IFCONFIG_INFO_PACKET);
	}
	
	void setApIfChannelInfo(ApIfChannelInfoPacket newApIfchannelInfo) {
		
		IInterfaceConfiguration	interfaceConfiguration		= configuration.getInterfaceConfiguration();
		if(interfaceConfiguration == null) {
			return;
		}
		IInterfaceInfo			interfaceInfo				= (IInterfaceInfo)interfaceConfiguration.getInterfaceByName(newApIfchannelInfo.getInterfaceName());
		if(interfaceInfo == null) {
			return;
		}
		IInterfaceChannelConfiguration	supportedChConfig	= (IInterfaceChannelConfiguration)interfaceInfo.getSupportedChannelConfiguration();
		if(supportedChConfig ==	null) {
			return;
		}
		
		int	i		= 0;	
		int chCount	= newApIfchannelInfo.getChannelCount();
		supportedChConfig.setChannelInfoCount(chCount);
		
		for(i = 0; i < chCount; i++) {
			
			ApIfChannelInfo	channelInfo		= newApIfchannelInfo.getChannelInfo(i);
			IChannelInfo	supportChInfo	= (IChannelInfo)supportedChConfig.getChannelInfo(i);
			
			if(supportChInfo ==	null) {
				continue;
			}
			supportChInfo.setChannelNumber(channelInfo.channelNumber);
			supportChInfo.setFlags(channelInfo.flags);
			supportChInfo.setFrequency(channelInfo.channelFrequency);
			supportChInfo.setMaxPower(channelInfo.maxPower);
			supportChInfo.setMaxRegPower(channelInfo.maxRegPower);
			supportChInfo.setMinPower(channelInfo.minPower);
			
		}
		
		receiveStatus.ifPacketReceived(PacketFactory.AP_IF_CHANNEL_INFO_PACKET, interfaceInfo.getName());
	}

	void setRfCustomChannelInfo(RFCustomChannelInfoPacket rfCustChInfo) {
		
		IInterfaceConfiguration	interfaceConfiguration		= configuration.getInterfaceConfiguration();
		if(interfaceConfiguration == null) {
			return;
		}
		IInterfaceInfo			interfaceInfo				= (IInterfaceInfo)interfaceConfiguration.getInterfaceByName(rfCustChInfo.getIfName());
		if(interfaceInfo == null) {
			return;
		}
		IInterfaceChannelConfiguration	customChConfig = (IInterfaceChannelConfiguration)interfaceInfo.getCustomChannelConfiguration();
		if(customChConfig == null) {
			return;
		}
		int	i		= 0;	
		int chCount	= rfCustChInfo.getChannelCount();
		customChConfig.setChannelInfoCount(chCount);
		
		customChConfig.setAntennaGain(rfCustChInfo.getAntennaGain());
		customChConfig.setAntennaPower(rfCustChInfo.getAntennaPower());
		customChConfig.setChannelBandwidth(rfCustChInfo.getChannelBandwith());
		customChConfig.setCtl(rfCustChInfo.getCtl());
		
		for(i = 0; i < chCount; i++) {
			
			RFChannelInfo	rfChannelInfo	= rfCustChInfo.getRFChannelInfoByIndex(i);
			IChannelInfo	customChInfo	= (IChannelInfo)customChConfig.getChannelInfo(i);
			
			if(customChInfo	== null) {
				continue;
			}
			
			customChInfo.setChannelNumber(rfChannelInfo.getChannel());
			customChInfo.setFrequency(rfChannelInfo.getFrequency());
			customChInfo.setPrivateChannelFlags(rfChannelInfo.getPrivateChannelFlag());
		}
		
		receiveStatus.packetReceived(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
	}			

	void setEffistreamInfo(EffistreamConfigPacket effistreamInfo) {
		
		IEffistreamConfiguration effistreamConfiguration	= configuration.getEffistreamConfiguration();
		if(effistreamConfiguration == null) {
			return;
		}
		
		EffistreamConfigPacket.RuleInfo	parentRule	= effistreamInfo.getRulesRoot();
		if(parentRule == null) {
			return;
		}
		effistreamConfiguration.clearData();
		IEffistreamRule	configRuleRoot	= (IEffistreamRule)effistreamConfiguration.getRulesRoot();
		setEffistreamRuleFromPkt(parentRule, configRuleRoot);
		receiveStatus.packetReceived(PacketFactory.EFFISTREAM_CONFIG_PACKET);
	}
	
	/**
	 * @param parentRule
	 * @param configRuleRoot
	 */
	private void setEffistreamRuleFromPkt(RuleInfo srcRule, IEffistreamRule dstRule) {
		
		String key;
		
		if(srcRule == null || dstRule == null) {
			return;
		}
		IEffistreamRuleCriteria	value 		= (IEffistreamRuleCriteria)dstRule.getCriteriaValue();
		EffistreamHelper.copyValueFromId(value, srcRule.value, srcRule.criteriaId);
		IEffistreamRuleCriteria dstCriteria	= (IEffistreamRuleCriteria)dstRule.getCriteriaValue();
		value.copyTo(dstCriteria);
		
		Enumeration<String>	keys = srcRule.childrenHash.keys();
		
		while(keys.hasMoreElements()) {
			key												 = (String)keys.nextElement();
			EffistreamConfigPacket.RuleInfo	srcEffiRuleChild = (EffistreamConfigPacket.RuleInfo)srcRule.childrenHash.get(key);
			if(srcEffiRuleChild == null) {
				continue;
			}
			IEffistreamRule	dstEffiRuleChild				= (IEffistreamRule)dstRule.createRule(srcEffiRuleChild.criteriaId);
			IEffistreamRuleCriteria	dstEffiRuleChildValue	= (IEffistreamRuleCriteria)dstEffiRuleChild.getCriteriaValue();
			EffistreamHelper.copyValueFromId(dstEffiRuleChildValue, srcEffiRuleChild.value, srcEffiRuleChild.criteriaId);
			dstRule.addRule(dstEffiRuleChild);
			setEffistreamRuleFromPkt(srcEffiRuleChild, dstEffiRuleChild);
		}
	
		if(srcRule.childCount == 0) {
			if(srcRule.action == null) {
				return;
			}
			IEffistreamAction	dstAction	= (IEffistreamAction)dstRule.addAction(IEffistreamAction.ACTION_TYPE_DEFAULT);
			setActionFrmPkt(srcRule.action, dstAction);			  
		}			
	}
	

	/**
	 * @param action
	 * @param dstAction
	 */
	private void setActionFrmPkt(ActionInfo srcAction, IEffistreamAction dstAction) {
		
		if(srcAction == null || dstAction == null) {
			return;
		}
		dstAction.clearData();
		dstAction.setDot11eCategory(srcAction.dot11eCategory);
		dstAction.setDropPacket(srcAction.dropPacket);
		dstAction.setNoAck(srcAction.noAck);
		dstAction.setBitRate(srcAction.bitRate);
		dstAction.setQueuedRetry(srcAction.queuedRetry);
	}

	void setSaturationInfo(DLSaturationInfoPacket info) {
		
		String	ifName;
		int 	channelCount;
		int 	channelIndex;
		int  	apIndex;
		int 	channelApCount;
		
		ifName	= info.getIfName();
		
		IInterfaceConfiguration	interfaceConfig	= configuration.getInterfaceConfiguration();
		IInterfaceInfo			interfaceInfo	= (IInterfaceInfo)interfaceConfig.getInterfaceByName(ifName);
		ISaturationInfo			saturationInfo	= (ISaturationInfo)interfaceInfo.getSaturationInfo();
		
		saturationInfo.setIfName(ifName);
		
		channelCount	= info.getChannelCount();
		saturationInfo.setChannelCount(channelCount);
	
		for(channelIndex = 0; channelIndex < channelCount; channelIndex++) {
			
			saturationInfo.setChannel(info.getChannel(channelIndex), channelIndex);
			saturationInfo.setChannelAvgSignal(info.getChannelAvgSignal(channelIndex), channelIndex);
			saturationInfo.setChannelMaxSignal(info.getChannelMaxSignal(channelIndex),channelIndex);
			saturationInfo.setChannelRetryDuration(info.getChannelRetryDuration(channelIndex), channelIndex);
			saturationInfo.setChannelTotalDuration(info.getChannelTotalDuration(channelIndex), channelIndex);
			saturationInfo.setChannelTxDuration(info.getChannelTxDuration(channelIndex), channelIndex);
				
			channelApCount	= info.getChannelAPCount(channelIndex);
			saturationInfo.setChannelAPCount(info.getChannelAPCount(channelIndex), channelIndex);	
			
			for(apIndex = 0; apIndex < channelApCount; apIndex++){
				
				saturationInfo.setChannelAPBssid(channelIndex, apIndex,info.getChannelAPBssid(channelIndex, apIndex));
				saturationInfo.setChannelAPEssid(channelIndex, apIndex,info.getChannelAPEssid(channelIndex, apIndex));
				saturationInfo.setChannelAPRetryDuration(channelIndex, apIndex, info.getChannelAPRetryDuration(channelIndex, apIndex));
				saturationInfo.setChannelAPSignal(channelIndex, apIndex,info.getChannelAPSignal(channelIndex, apIndex));
				saturationInfo.setChannelAPTxDuration(channelIndex, apIndex,info.getChannelAPTxDuration(channelIndex, apIndex));
			}
		}
		saturationInfo.setLastUpdateTime(info.getLastUpdateTime());
		saturationInfo.setSaturationInfoReceived();
	}
	
	void clearData() {
		configuration.clearData();
	}
	
	void clearStatus(){
		receiveStatus.clearStatus();
	}
	
	boolean allConfigurationReceived(){
		if( (receiveStatus.isPacketReceived(PacketFactory.AP_CONFIGURATION_INFO) == true) &&
			(receiveStatus.isPacketReceived(PacketFactory.AP_IP_CONFIGURATION_INFO) == true) &&
			(receiveStatus.isPacketReceived(PacketFactory.AP_VLAN_CONFIGURATION_INFO) == true)) {
			return true;
		}
	
		return false;	
	}
	
	IConfigStatusHandler getRequestStatusHandler() {
		return requestStatus;
	}
	
	IConfigStatusHandler getConfigStatusHandler() {
		return receiveStatus;
	}

	void setSipConfig(SipConfigPacket sipConfigPkt) {
		
		ISipConfiguration	sipConfiguration	= configuration.getSipConfiguration();
		
		if(sipConfiguration == null) {
			return;
		}
		
		sipConfiguration.setEnabled(sipConfigPkt.getEnabled());
		sipConfiguration.setEnabledInP3M(sipConfigPkt.getAdhocOnly());
		sipConfiguration.setServerIpAddress(sipConfigPkt.getServerIpAddress().getBytes());
		sipConfiguration.setPort(sipConfigPkt.getPort());
		
		short staCount	= sipConfigPkt.getStaCount();
		sipConfiguration.setStaInfoCount(staCount);
		
		for(short i=0;i<staCount;i++) {
			
			ISipStaInfo	iStaInfo	= sipConfiguration.getStaInfoByIndex(i);
			
			if(iStaInfo == null) {
				continue;
			}
			SipStaInfo staInfo = sipConfigPkt.getStaInfo(i);
			iStaInfo.setExtension(staInfo.getExtn());
			iStaInfo.setMacAddress(staInfo.getMacAddress().getBytes());

		}
		receiveStatus.packetReceived(PacketFactory.SIP_CONFIG_PACKET);
		
	}
	//find msb bit 1 or not
	private int mostSignificantBit(int length){
		  int mask = 1 << 15;
		  for(int bitIndex = 15; bitIndex >= 0; bitIndex--){
		    if((length & mask) != 0){
		      return bitIndex;
		    }
		    mask >>>= 1;
		  }
		  return -1;
		}
}
