/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApResponseHandler.java
 * Comments : 
 * Created  : Aug 06, 2007
 * Author   : Abhishek Patankar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 *  --------------------------------------------------------------------------------
 * |  7  |Nov 06, 2007 | cCode changed check added in responseReceived	 |Imran   |
 *  --------------------------------------------------------------------------------
 * |  0  |Aug 06, 2007 | Created                                         | Abhishek|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh.ap;

import com.meshdynamics.meshviewer.imcppackets.IMCPPacket;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;

public class ApResponseHandler {
	
	private ApDataController dataController;
	
	public ApResponseHandler(ApDataController dataController) {
		this.dataController = dataController;
	}

	void processResponse(IMCPPacket packet) {
		
		switch(packet.getPacketType()) {
		
		case PacketFactory.IMCP_KEY_UPDATE_RESPONSE:
			dataController.setApMoved(true);
			break;
		case PacketFactory.RF_CUSTOM_CHANNEL_REQUEST_RESPONSE_PACKET:
			/**
			 *  RF response handling:
			 *  If rf update is sent frm MV running on other console
			 *  it should set rfchanged flag in all running MVs.
			 */ 
	        dataController.setRfUpdateSent(true);
	        break;
	        
	    case PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET:
	    	/**
	    	 *  Country Code Response Handling
	    	 *  If country code is changed frm MV running on other console
	    	 *  is should set coundtyrCodeChanged im all running MVs
	    	 */
	    	if(dataController.isCountryCodeConfigAlreadyReceived()
	    			&& dataController.isCountryCodeChanged()) {
	        	dataController.setCountryCodeChanged(true);
	    	}
	    	
        	break;
		  }
	}
	
}
