package com.meshdynamics.meshviewer.mesh.ap;
 
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.URL;
import java.util.Random;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meshdynamics.macAddressVerification.DownloadImageRequest;
import com.meshdynamics.macAddressVerification.MacAddress_Config;
import com.meshdynamics.macAddressVerification.Status;
import com.meshdynamics.nmsui.MDBuildServer;
import com.meshdynamics.nmsui.util.MFile;
 
public class FirmwareRequest {
 
    public FirmwareRequest(){}
    private static String server;
    private static int    port;
    String localMacAddress=null;
    static{
        MDBuildServer.readSettings();
        server=MDBuildServer.server;
        port  =MDBuildServer.port;  
    }
    public String getMacOfCurrentDevice(){
       try{
            InetAddress inetaddress=InetAddress.getLocalHost(); //Get LocalHost refrence
            //get Network interface Refrence by InetAddress Refrence
            NetworkInterface network = NetworkInterface.getByInetAddress(inetaddress); 
            byte[] macArray = network.getHardwareAddress();  //get Harware address Array
            StringBuilder str = new StringBuilder();             
            // Convert Array to String 
            for (int i = 0; i < macArray.length; i++) {
                    str.append(String.format("%02X%s", macArray[i], (i < macArray.length - 1) ? ":" : ""));
            }
            String macAddress=str.toString();
         
            return macAddress; //return MAc Address
        }
        catch(Exception e){
             return null;
        } 
    }
    
    public Status buildImage(MacAddress_Config macConfig,String key){
        Status status=null;
        try {
            macConfig.setClientMacAddress(getMacOfCurrentDevice());            
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writeValueAsString(macConfig);
            String buildUrl="http://"+server+":"+port+"/MDBuild/build/generate";
            URL url=new URL(buildUrl);    
            HttpURLConnection connection=(HttpURLConnection)url.openConnection();
            connection.setDoOutput(true);  
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Authentication", key);
            OutputStream out = (OutputStream) connection.getOutputStream();
            out.write(jsonInString.getBytes());
            out.flush();
            System.out.println(jsonInString);
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.out.println("Failed : HTTP error code : "
                    + connection.getResponseCode());
                return null;
            }
              BufferedReader br = new BufferedReader(new InputStreamReader(
                    (connection.getInputStream())));
 
            String output;
            while ((output = br.readLine()) != null) {
                JSONObject  jsonObject = (JSONObject ) new JSONParser().parse(output);            
                System.out.println(output);
                status=getSevrerInfo(jsonObject);
            }
            connection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }
        return status;
    }
    public boolean downloadImage(String imageName,String signedKey,String uniquekey){
        boolean flag=false;
        try {
            DownloadImageRequest downloadImageRequest=new DownloadImageRequest();
            downloadImageRequest.setImageName(imageName);
            downloadImageRequest.setClientMacAddress(getMacOfCurrentDevice());
            downloadImageRequest.setUniqueId(uniquekey);
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writeValueAsString(downloadImageRequest);
            String urlStr="http://"+server+":"+port+"/MDBuild/build/download";
            URL url = new URL(urlStr);
            HttpURLConnection connection=(HttpURLConnection)url.openConnection();
             connection.setDoOutput(true);  
             connection.setRequestMethod("POST");
             connection.setRequestProperty("Authentication",signedKey);
             connection.setRequestProperty("Content-Type", "application/json");
           /*  connection.setRequestProperty("Accept", "application/json");*/
             OutputStream out = (OutputStream) connection.getOutputStream();
             out.write(jsonInString.getBytes());
             out.flush();
             System.out.println(jsonInString);
             if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                 String fileName = "";
                 String fileURL=MFile.getUpdatesPath();
                 String disposition = connection.getHeaderField("Content-Disposition");
                 int contentLength = connection.getContentLength();
                 if(contentLength >0){                   
                 if (disposition != null) {
                     // extracts file name from header field
                     int index = disposition.indexOf("filename=");
                     if (index > 0) {
                         fileName = disposition.substring(index + 9,
                                 disposition.length());
                       }          
                      // opens input stream from the HTTP connection
                      InputStream inputStream = connection.getInputStream();
                     // String saveFilePath = File.separator + fileName;
                      String path=fileURL+"/"+fileName;
                        
                      // opens an output stream to save into file
                      FileOutputStream outputStream = new FileOutputStream(path);
                      int bytesRead = -1;
                      byte[] buffer = new byte[contentLength];
                      while ((bytesRead = inputStream.read(buffer)) != -1) {
                          outputStream.write(buffer, 0, bytesRead);
                      }                                
                     outputStream.close();
                    inputStream.close();    
                    flag=true;
                    System.out.println("File downloaded");
                 }
                 } else {
                     // extracts file name from URL
                         BufferedReader br = new BufferedReader(new InputStreamReader(
                          (connection.getInputStream())));
                        String output;
                         while ((output = br.readLine()) != null) {
                         System.out.println(output);
                        }                            
                }
                 } else {
                 System.out.println("No file to download. Server replied HTTP code: " + connection.getResponseCode());
             }            
              
        } catch (IOException e) {
            e.printStackTrace();
        }
        return flag;
    }
    private Status getSevrerInfo(JSONObject jsonObject){
        Status status=new Status();
        
        if(jsonObject.get("macAddressDigest") != null)
        status.setMacAddressDigest((String)jsonObject.get("macAddressDigest"));
        
        if(jsonObject.get("responseBody") != null)
        status.setResponseBody((Object) jsonObject.get("responseBody"));
        
        if(jsonObject.get("responseCode") != null)
        status.setResponseCode((int)(long) jsonObject.get("responseCode"));
        
        if(jsonObject.get("responseMessage") != null)
        status.setResponseMessage((String) jsonObject.get("responseMessage"));
        
        if(jsonObject.get("signedKey") != null)
        status.setSignedKey((String) jsonObject.get("signedKey"));
        
        if(jsonObject.get("uniqueId") != null)
        status.setUniqueId((String) jsonObject.get("uniqueId"));
        
        return status;
    }
    }

