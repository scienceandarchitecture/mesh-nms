package com.meshdynamics.meshviewer.mesh.ap;

public interface IApFwUpdateListener {
	
	public void notifyUpdateStatus		(int ret, String status);
	public void notifyApFileUpdate		(int currentFileNo, int totalFileCount);
}
