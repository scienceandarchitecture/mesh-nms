/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : AccessPoint.java
 * Comments : 
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 
 * File Revision History 
 * -------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * | 88  |Nov 6,2007   |  isCountryCodeConfigRecved flag added           | Imran  |
 * --------------------------------------------------------------------------------
 * | 87  |Aug 28,2007  | heartbeatMissed flag added					     |Imran   |
 * --------------------------------------------------------------------------------
 * | 86  |Jul 30,2007  | notifictions to Meshnetwork for ap moved added  |Imran   |
 * --------------------------------------------------------------------------------
 * | 85  |Jul 25,2007  | UpdateConfiguration is added           	 	 |Abhishek |
 *  --------------------------------------------------------------------------------
 * | 84  |Jun 25, 2007 | isRoot calculation fixed			             |Abhijit |
 *  --------------------------------------------------------------------------------
 * | 83  |Jun 11, 2007 | updateAllConfiguration method added             | Imran  |
 *  --------------------------------------------------------------------------------
 * | 82  |May 27,2007  | Heartbeat1, heartbeat2 ref removed              |Abhishek|
 * -------------------------------------------------------------------------------
 * | 81  |May 25,2007  | runtimeManager added                            |Abhishek|
 * -------------------------------------------------------------------------------
 * | 80  |May 24,2007  | doNotification method modified					 |Imran   |
 * -------------------------------------------------------------------------------
 * | 79  |May 15,2007  | method getParentAp added						 |Imran   |
 * -------------------------------------------------------------------------------
 * | 78  |May 10,2007  | Message Listener functionality added			 |Abhijit |
 * -------------------------------------------------------------------------------
 * | 77  |May 09,2007  | changes due to modification of IAPPropListener	 |Abhijit |
 * -------------------------------------------------------------------------------
 * | 76  |May 3, 2007  | getMeshnetwork added                            |Abhishek|
 * -------------------------------------------------------------------------------
 * | 75  |May 3, 2007  | getUpdateStatusHandler added                    |Abhishek|
 * -------------------------------------------------------------------------------
 * | 74  |May 1, 2007  | getName is added                                |Abhishek|
 * --------------------------------------------------------------------------------
 * | 73  |Mar 14, 2007 | set/get effistreamConfig added					 |Bindu   |
 *---------------------------------------------------------------------------------
 * | 72  |Mar 26,2007  |  isRoot method added							 |Imran   |
 * -------------------------------------------------------------------------------
 * | 71  |Mar 26,2007  |  fipsDetected added							 |Abhishek|
 *---------------------------------------------------------------------------------
 * | 70  |Mar 26,2007  |inf file auto generation call added				 |Prachiti|
 *---------------------------------------------------------------------------------
 * | 69  |Mar 20,2007  |  getVoltage exposed							 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 68  |Mar 08,2007  |  changes for saturation info					 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 67  |Feb 27,2007  |  removed unused code							 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 66  |Feb 20, 2007 |isFIPSEnabled modified       					 |Abhishek|
 * -------------------------------------------------------------------------------
 * | 65  |Feb 15, 2007 | MeshNetwork given to this class				 |Abhijit |
 *  -------------------------------------------------------------------------------
 * | 64  |Feb 02, 2007 | versionGreaterThan2535 added for Eth Tab		 |Bindu	  |
 * -------------------------------------------------------------------------------
 * | 63  |Jan 30, 2007 | FIPSEnabled flag added       					 |Abhishek|
 * -------------------------------------------------------------------------------|
 * | 62  |Feb 2,  2007 |  allConfigurationChanged modified               |Imran  |
 * --------------------------------------------------------------------------------
 * | 61  |Jan 31, 2007 | VersionGreaterthan2534 added 					 |Abhishek|
 * -------------------------------------------------------------------------------
 * | 60  |Dec 26, 2006 | Accesspoint can configure added 				 |Imran   |
 * ------------------------------------------------------------------------------ 
 * | 59  |Dec 25, 2006 | VersionGreaterthan2530 added 					 |Abhishek|
 * -------------------------------------------------------------------------------
 * | 58  |Dec 19, 2006 | VersionGreaterthan2526 added 					 |Abhishek|
 * -------------------------------------------------------------------------------
 * | 57  |Dec 13, 2006 | Removed dirtyFlag,implemented rfCustomCannelInfo|Abhijit |
 * --------------------------------------------------------------------------------
 * | 56  |Nov 08, 2006 | APProperties Bug Fixed							 |Abhishek|
 * --------------------------------------------------------------------------------
 * | 55  |Oct 06, 2006 | reboot flag set for DCA/Channel change		 	 |Bindu   |
 * --------------------------------------------------------------------------------
 * | 54  |Oct 06, 2006 | versionGreaterThan251 added				 	 |Bindu   |
 * --------------------------------------------------------------------------------
 * | 53  |Jul 10, 2006 | versionGreaterThan98 added					 	 |Bindu   |
 * --------------------------------------------------------------------------------
 * | 52  |Jul 10, 2006 | Misc firware version check fixes			 	 |Bindu   |
 * --------------------------------------------------------------------------------
 * | 51  |Jun 30, 2006 | countryCodeChangeFlag saved in Accesspoint 	 |Prachiti|
 * --------------------------------------------------------------------------------
 * | 50  |Jun 20, 2006 | Version checking fix		      			     | Bindu  |
 * --------------------------------------------------------------------------------
 * | 49  |Jun 15, 2006 | RF Space Packet date fix       			     | Bindu  |
 * --------------------------------------------------------------------------------
 * | 48  |Jun 14, 2006 | config clear removed on stop(nodes data visible)| Bindu  |
 * --------------------------------------------------------------------------------
 * | 47  |Jun 12, 2006 | All config req sent when viewer re-started      | Bindu  |
 * --------------------------------------------------------------------------------
 * | 46  |May 22, 2006 | versionGreaterThan94 added                      |Bindu   |
 * --------------------------------------------------------------------------------
 * | 45  |May 22, 2006 | ifname param added to DLS dlg                   |Bindu   |
 * --------------------------------------------------------------------------------
 * | 44  | May 22, 2006| Misc Fixes     	                             |Bindu   |
 * --------------------------------------------------------------------------------
 * | 43  | May 19, 2006| UI Updation on Packet DL SAT receive            |Bindu   |
 * --------------------------------------------------------------------------------
 * | 42  |May 19, 2006 |Changes for security comparison		   		     | Mithil |
 *  -------------------------------------------------------------------------------
 * | 41  |May 12, 2006 |Changes for DL Saturation			   		     | Bindu  |
 *  -------------------------------------------------------------------------------
 * | 40  |May 11, 2006 | compare Configuration Added				     | Mithil |
 * --------------------------------------------------------------------------------
 * | 39  |May 5, 2006  |Arrow drawing and bitrate display                |Prachiti|
 * --------------------------------------------------------------------------------
 * | 38  |Apr 10, 2006 | ApIf Channel info crash				  	 	 | Mithil |
 * --------------------------------------------------------------------------------
 * | 37  |Mar 28, 2006 | Error message displayed if node rebooted  	 	 | Mithil |
 * --------------------------------------------------------------------------------
 * | 36  |Mar 16, 2006 |Changes for ACL							  		 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 35  |Mar 10, 2006 |Scan displayed if node is rebooted    			 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 34  |Mar 08, 2006 |VLAN QOS reboot after change	    			 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 33  |Feb 28, 2006 | implicit rebooting after reboot removed	  	 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 32  |Feb 27, 2006 |capabilities handled for backward compatibility	 |Prachiti|
 *  -------------------------------------------------------------------------------
 * | 31  |Feb 23, 2006 |Command capable added							 |Prachiti|
 *  -------------------------------------------------------------------------------
 * | 30  |Feb 22, 2006 |Comment Removed for Dot11E						 |Prachiti|
 *  -------------------------------------------------------------------------------
 * | 30  |Feb 22, 2006 |Exclamation removed,take changed conf automatly	 |Prachiti|
 *  -------------------------------------------------------------------------------
 * | 29  |Feb 20, 2006 | isCapable added           					  	 |Prachiti|
 *  -------------------------------------------------------------------------------
 * | 28  |Feb 20, 2006 | misc fixes for dot11e	 					  	 | Bindu  |
 *  -------------------------------------------------------------------------------
 * | 27  |Feb 17, 2006 | config sqnr minor fixes 					  	 |Prachiti|
 *  -------------------------------------------------------------------------------
 * | 26  |Feb 17, 2006 | Misc Fixes for Dot11e 						  	 | Bindu  |
 *  -------------------------------------------------------------------------------
 * | 25  |Feb 16, 2006 | Changes for Dot11e	  						  	 | Bindu  |
 *  -------------------------------------------------------------------------------
 * | 24  |Feb 16, 2006|Config Sequence Number change                     |Prachiti|
 *  -------------------------------------------------------------------------------
 * | 23  | Feb 15,2006 | STA Log setings menu added				  		 | Mithil |
 * --------------------------------------------------------------------------------
 * |  22 |Feb 15, 2006| File Size for Logging removed   			     | Mithil |
 * --------------------------------------------------------------------------------
 * |  21 |Feb 14, 2006| Clear Macro Message after reboot			     | Mithil |
 * --------------------------------------------------------------------------------
 * | 20  |Feb 13, 2006 | Hide ESSID Implemented		                     | Mithil |
 *  -------------------------------------------------------------------------------
 * |  19 |Feb 03, 2006 | DIsplay in Property UI		 					 | Mithil |
 * --------------------------------------------------------------------------------
 * |  18 |Feb 02, 2006 | STA Information Packet		 					 | Mithil |
 * --------------------------------------------------------------------------------
 * |  17 |Jan 27, 2006 | Default logging disabled	 					 | Mithil |
 * --------------------------------------------------------------------------------
 * |  16 |Jan 11, 2006 | Updates sent variable added 					 | Mithil |
 * --------------------------------------------------------------------------------
 * |  15 |Dec 08, 2005 | Packet logger data saved to file				 | Amit   |
 * --------------------------------------------------------------------------------
 * |  14 |Dec 08, 2005 | Packet logger parameters setting function added | Amit   |
 * --------------------------------------------------------------------------------
 * |  13 |Nov 25, 2005 | Added changes for Set Monitor Mode				 | Mithil |
 * -------------------------------------------------------------------------------- 
 * |  12 |Nov 25, 2005 | Added Ack Timeout and Reg Domain Country Code   | Mithil | 
 * 		 |			   | Changes 1015 and 1027							 |		  | 
 * -------------------------------------------------------------------------------- 
 * |  11 |Oct 21, 2005 | Added hb2 info for Storing						 | Mithil |
 * -------------------------------------------------------------------------------- 
 * |  10 |Sep 20, 2005 | Configuration get functions error handling done | Abhijit|
 * --------------------------------------------------------------------------------
 * |  9  |Sep 20, 2005 | removed update packets,added NodeConfiguration  | Abhijit|
 * --------------------------------------------------------------------------------
 * |  8  |Sep 13, 2005 | StaEntry association time updated			     | Abhijit|
 * --------------------------------------------------------------------------------
 * |  7  |Sep 09, 2005 | LOAD/SAVE of COnfig info disabled			     | Abhijit|
 * --------------------------------------------------------------------------------
 * |  6  |MaY 05, 2005 | LOAD/SAVE of HW info added					     | Bindu  |
 * --------------------------------------------------------------------------------
 * |  5  |Mar 04, 2005 | VLanConfigInfo Listeners called with dynMask    | Bindu  |
 * --------------------------------------------------------------------------------
 * |  4  |Feb 11, 2005 | Change Detection-PropertyUI changes             | Anand  |
 * --------------------------------------------------------------------------------
 * |  3  |Feb 10, 2005 | All Config Request Packets merged to IMCP#25    | Anand  |
 * --------------------------------------------------------------------------------
 * |  2  |Feb 09, 2005 | Changes idenfication logic added to APProp      | Anand  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 08, 2005 | Added VLAN Packet                               | Bindu  |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh.ap;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import com.meshdynamics.iperf.IperfHelper;
import com.meshdynamics.iperf.IperfListener;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipConfiguration;
import com.meshdynamics.meshviewer.configuration.IStaConfiguration;
import com.meshdynamics.meshviewer.configuration.runtimeconfiguration.IRuntimeConfigStatus;
import com.meshdynamics.meshviewer.imcppackets.ApHwInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.DLSaturationInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.IMCPPacket;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.imcppackets.StaAssocPacket;
import com.meshdynamics.meshviewer.imcppackets.StaDisassocPacket;
import com.meshdynamics.meshviewer.imcppackets.StaInfoPacket;
import com.meshdynamics.meshviewer.mesh.IMessageHandler;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.IMessageHandler.IMessageSource;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.miscd.MeshCommandHelper;
import com.meshdynamics.util.Bytes;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

public class AccessPoint implements IMessageHandler, ICapabilityProvider, IMessageSource, IRuntimeConfigListener {

    public static final int					STATUS_DEAD					= 1;
	public static final int					STATUS_SCANNING				= 2;
	public static final int					STATUS_STARTUP				= 3;
	public static final int					STATUS_ALIVE				= 4;

	private MacAddress										dsMacAddress;
	private ApHwInfoPacket									hardwareInfo;
    private int 											status;
    private Hashtable<String, IAPPropListener>				propertyListeners;
    private Hashtable<String, IMessageHandler>				messageListeners;
    private boolean 										selected;
	private boolean											nodeMoved;
    private MeshNetwork										meshNetwork;
    private ApDataController								dataController;
	private boolean											isRoot;
	private AccessPoint 									parentAp;
	private ApRuntimeConfigurationManager					runtimeManager;
	private boolean											heartBeatMissed;
	private boolean 										isCountryCodeConfigRecved;
	private boolean											nodeRebooted;
	private boolean											isLocal;
	private MeshCommandHelper								commandHelper;
	private Hashtable<String, ICommandExecutionListener> 	mapRemoteMeshCommands;
	private Hashtable<String, ICommandExecutionListener> 	mapRemoteGPSUpdates;
	private IApFwUpdateListener								fwUpdateListener;
	private IperfHelper										iperfHelper;
	
	private static final String 	ALCONFSET_CMD_ENABLE	= "alconfset gps 1 /dev/ttyS2 0.0.0.0 4800 2";
	private static final String 	ALCONFSET_CMD_DISABLE	= "alconfset gps 0";
	private static final String 	ALCONFSET_SAVE			= "alconfset save";
	
	public AccessPoint(MeshNetwork meshNetwork) {
		
		this.meshNetwork			= meshNetwork;
		
		dsMacAddress				= new MacAddress();
		status 						= STATUS_DEAD;
		propertyListeners			= new Hashtable<String, IAPPropListener>();
		messageListeners			= new Hashtable<String, IMessageHandler>();
		selected					= false;
		hardwareInfo				= null;
		nodeMoved 					= false;
		dataController				= new ApDataController(this,meshNetwork.getPacketSender());
		isRoot						= false;
		runtimeManager				= dataController.getRuntimeManager();
		heartBeatMissed				= false;
		isCountryCodeConfigRecved	= false;
		nodeRebooted				= false;
		isLocal						= true;
		commandHelper				= new MeshCommandHelper();
		mapRemoteMeshCommands		= new Hashtable<String, ICommandExecutionListener>();
		mapRemoteGPSUpdates			= new Hashtable<String, ICommandExecutionListener>();
		fwUpdateListener			= null;
		iperfHelper					= new IperfHelper();
	}
	
    public boolean equals(Object obj) {
        return ((AccessPoint)obj).getDsMacAddress().equals(dsMacAddress);
    }
    
	public void load(InputStream in) throws IOException {

		byte[] fourBytes	= new byte[4];
		byte[] buffer;
		
		in.read(fourBytes);
		int len = Bytes.bytesToInt(fourBytes);
		
		if(len > 0){
			buffer = new byte[len];
			in.read(buffer);
			String name = new String(buffer);
			dataController.setName(name);
		}
		
		in.read(fourBytes);
		len = Bytes.bytesToInt(fourBytes);
		
		if(len > 0) {
			buffer = new byte[len];
			in.read(buffer);
			String description = new String(buffer);
			dataController.setDescription(description);
		}
		
		dsMacAddress  = new MacAddress();
		dsMacAddress.read(in);
		
		this.hardwareInfo = (ApHwInfoPacket) PacketFactory.getNewIMCPInstance(PacketFactory.AP_HW_INFO);
		
		hardwareInfo.setMeshId(this.getNwName());
		hardwareInfo.read(in);
		processPacket(hardwareInfo);	
		dataController.clearStatus();
	}

	public void save(OutputStream out) throws IOException {

		byte[] 	fourBytes 	= new byte[4];
		
		String 	name		= dataController.getName();
		int 	len 		= name.length();		
		Bytes.intToBytes(len,fourBytes);		
		out.write(fourBytes);
		
		out.write(name.getBytes());

		String	description = dataController.getDescription();
		len 				= description.length();
		Bytes.intToBytes(len,fourBytes);		
		out.write(fourBytes);
		
		out.write(description.getBytes());
		
		dsMacAddress.write(out);
		
		if(hardwareInfo != null) {
		   hardwareInfo.write(out);
		}
	}

	public int getStatus(){
		return status;	
	}

	private void setStatus(int status){
		
	    if(this.status == status)
	        return;

	    this.status = status;
	    
	    switch(status) {
	    	case STATUS_STARTUP:
	    		notifyPropertyListeners(IAPPropListener.MASK_AP_STATE, IAPPropListener.AP_STATE_STARTUP);
	    		break;
	    	case STATUS_ALIVE:
	    		notifyPropertyListeners(IAPPropListener.MASK_AP_STATE, IAPPropListener.AP_STATE_ALIVE);
	    		break;
	    	case STATUS_DEAD:
	    		notifyPropertyListeners(IAPPropListener.MASK_AP_STATE, IAPPropListener.AP_STATE_DEAD);
	    		heartBeatMissed	= false;
	    		isCountryCodeConfigRecved	= false;
	    		break;
	    	case STATUS_SCANNING:
	    		notifyPropertyListeners(IAPPropListener.MASK_AP_STATE, IAPPropListener.AP_STATE_SCANNING);
	    		break;	    	
	    }
        
	}
    
    /**
     * @param dsMacAddress
     */
    public void setDSMacAddress(MacAddress dsMacAddress) {
        this.dsMacAddress = dsMacAddress;
    }


    /**
     * @return Returns the dsMacAddress.
     */
    public MacAddress getDsMacAddress() {
        return dsMacAddress;
    }

    /**
     * @param listener
     * @param object
     */
    public void addPropertyListener(IAPPropListener listener) {
    	propertyListeners.put(listener.toString(), listener);        
    }
    
    /**
     * @param listener
     * @param object
     */
    public void removePropertyListener(IAPPropListener listener) {
    	propertyListeners.remove(listener.toString());
    }
    private void notifyPropertyListeners(final int mainFilter, final int subFilter) {
    
    	Enumeration<IAPPropListener> enumeration 	=  propertyListeners.elements();
        final AccessPoint 			ap 				= this;
        
        while(enumeration.hasMoreElements()) {
        	final IAPPropListener apLst = (IAPPropListener)enumeration.nextElement();
            		apLst.apPropertyChanged(mainFilter, subFilter, ap);
        }
   }
    
    /**
     * @param 
     */
    public void stop(){
    	dataController.clearStatus();    	
        setStatus(STATUS_DEAD);
       
    }


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String str = dataController.getName() + " [" + dsMacAddress.toString()+"] ";
		return str;
	}

	public IConfiguration getConfiguration() {
		return dataController.getConfiguration();
	}
	
	public IRuntimeConfigStatus getRuntimeConfigStatus(){
		return runtimeManager.getRuntimeConfigStatus();
	}

	/**
	 * @return
	 */
	public int getIMCPChildCount(){
		
		int count=0;
		
		Enumeration<IStaConfiguration> enumSta	= runtimeManager.getStaList();
		
		if(enumSta.hasMoreElements()){
			
			IStaConfiguration	sta = (IStaConfiguration) enumSta.nextElement();
			AccessPoint 		ap 	= meshNetwork.getAccessPoint(sta.getStaMacAddress());
			
			if(ap != null){
				if(ap.getStatus() == STATUS_ALIVE)
					count++;
			}	
		}
		return count;
	}
	
	/**
	 * @return Returns the staActivityEnabled.
	 */
	public boolean isStaActivityEnabled() {
		return runtimeManager.isStaActivityEnabled();
	}
	
	/**
	 * @param staActivityEnabled The staActivityEnabled to set.
	 */
	public void setStaActivityEnabled(boolean staActivityEnabled) {
		runtimeManager.setStaClientActivityEnabled(staActivityEnabled);
	}
	
	public boolean isCapable(int packetType) {
	   	return runtimeManager.isCapable(packetType);	
	}
	
	public boolean isCmdCapable(int comType) {
		return runtimeManager.isCmdCapable(comType);	
	}
	
	public boolean isConfigCapable(int comType) {
		return runtimeManager.isConfigCapable(comType);	
	}
	
	/**
	 * @return Returns the nodeMoved.
	 */
	public boolean isNodeMoved() {
		return nodeMoved;
	}
	/**
	 * @param nodeMoved The nodeMoved to set.
	 */
	public void setNodeMoved(boolean nodeMoved) {
		this.nodeMoved = nodeMoved;
		if(nodeMoved == true) {
			meshNetwork.accesPointMoved(dsMacAddress.toString());
		}
	}

	/**
	 * @return Returns the selected.
	 */
	public boolean isSelected() {
		return selected;
	}
	
	/**
	 * @param selected The selected to set.
	 */
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public boolean canConfigure() {
		return dataController.canConfigure();
	}
	
	public boolean isFipsDetected() {
		return runtimeManager.isFIPSDetected();
	}
	
	public boolean isRoot(){
		return isRoot;
	}

	public void processPacket(IMCPPacket packet) {
		isLocal = (packet.isLocal() == true) ? true : false;
		dataController.processPacket(packet);
		doNotifications(packet);
	}

	private void doNotifications(IMCPPacket packet) {
		
		int packetType = packet.getPacketType();

		if (packetType == PacketFactory.AP_HW_INFO) {
			this.hardwareInfo = (ApHwInfoPacket) packet;
			isRoot = (hardwareInfo.getDsInterfaceInfo().getIfType() == Mesh.PHY_TYPE_802_3) ?
					true : false;
			notifyPropertyListeners(IAPPropListener.MASK_HWINFO, IAPPropListener.MASK_ALL);
		} else if (packetType == PacketFactory.HEARTBEAT) {
			parentAp = (isRoot() == false) ? 
					meshNetwork.getAccessPointFromBssid(runtimeManager.getParentBSSID()) : null; 
			if(heartBeatMissed == true) {
				notifyPropertyListeners(IAPPropListener.MASK_AP_STATE, IAPPropListener.AP_STATE_ALIVE);
				heartBeatMissed	= false;
			} else {
				notifyPropertyListeners(IAPPropListener.MASK_HEARTBEAT, IAPPropListener.MASK_ALL);
			}
		} else if (packetType == PacketFactory.HEARTBEAT2) {
			notifyPropertyListeners(IAPPropListener.MASK_HEARTBEAT2,IAPPropListener.MASK_ALL);
		} else if (packetType == PacketFactory.DN_LINK_SATURATION_INFO_PACKET) {
			DLSaturationInfoPacket	saturationInfo	= (DLSaturationInfoPacket)packet;
			meshNetwork.updateDLSaturationInfo(this, saturationInfo.getIfName());
		} else if (packetType == PacketFactory.CH_SCAN_LOCK) {
			setStatus(STATUS_SCANNING);
		} else if (packetType == PacketFactory.STA_ASSOC_NOTIFICATION) {
			StaAssocPacket staAssocPacket = (StaAssocPacket) packet;
			staAssociated(staAssocPacket.getWmMacAddress(),staAssocPacket.getStaMacAddress());
		} else if (packetType == PacketFactory.STA_DISASSOC_NOTIFICATION) {
			StaDisassocPacket staDisassocPacket = (StaDisassocPacket) packet;
			staDisassociated(staDisassocPacket.getStaMacAddress(),staDisassocPacket.getReason());
		} else if (packetType == PacketFactory.STA_INFO_PACKET) {
			processStaInfo((StaInfoPacket) packet);
		} else if (packetType == PacketFactory.AP_CONFIGURATION_INFO) {
			notifyPropertyListeners(IAPPropListener.MASK_APCONF, 	IAPPropListener.MASK_ALL);        
		} else if (packetType == PacketFactory.AP_IP_CONFIGURATION_INFO) {
			notifyPropertyListeners(IAPPropListener.MASK_IPCONF, IAPPropListener.MASK_ALL);
		} else if (packetType == PacketFactory.AP_VLAN_CONFIGURATION_INFO) {
			notifyPropertyListeners(IAPPropListener.MASK_VLANCONF, IAPPropListener.MASK_ALL);
		} else if (packetType == PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET) {
			notifyPropertyListeners(IAPPropListener.MASK_APCONF, IAPPropListener.APCONF_MASK_REG_DOMAIN);
		} else if (packetType == PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET) {
			notifyPropertyListeners(IAPPropListener.MASK_APCONF, IAPPropListener.APCONF_MASK_ACK_TIMEOUT);
		} else if (packetType == PacketFactory.IF_HIDDEN_ESSID_PACKET) {
			notifyPropertyListeners(IAPPropListener.MASK_APCONF, IAPPropListener.APCONF_MASK_HIDDEN_ESSID);
		}
	}
	
	private void processStaInfo(StaInfoPacket packet) {
		notifyPropertyListeners(IAPPropListener.MASK_AP_STA_CONF, IAPPropListener.AP_STA_CONF_STA_INFO);
	}

	private void staDisassociated(MacAddress staMacAddress, int reason) {
		notifyPropertyListeners(IAPPropListener.MASK_AP_STA_CONF, IAPPropListener.AP_STA_CONF_STA_DISASSOC);
	}

	private void staAssociated(MacAddress wmMacAddress, MacAddress staMacAddress) {
		notifyPropertyListeners(IAPPropListener.MASK_AP_STA_CONF, IAPPropListener.AP_STA_CONF_STA_ASSOC);
	}

	public IVersionInfo getVersionInfo() {
		return dataController.getVersionInfo();
	}
	
	public String getModel() {
		return dataController.getModel();
	}
	
	public String getFirmwareVersion() {
		return dataController.getFirmwareVersion();
	}
	
	public boolean updateChangedConfiguration() {
		return dataController.updateChangedConfiguration();
	}
	
	public boolean updateAllConfiguration() {
		return dataController.updateAllConfiguration();
	}
	
	String getNetworkKey() {
		return meshNetwork.getKey();
	}

	public boolean containsAddress(MacAddress bssid) {
		if(bssid.equals(MacAddress.resetAddress) == true)
			return false;
		
		return dataController.containsAddress(bssid);
	}
	
	public boolean changeNetwork(String newNetworkId,String newNetworkKey) {
		
		nodeMoved	= dataController.changeNetwork(newNetworkId,newNetworkKey, Mesh.MACRO_MOVE_NODES);
		if(nodeMoved == true) {
			meshNetwork.accesPointMoved(dsMacAddress.toString());
		}
		return nodeMoved;
	}

	public boolean isPacketReceived(int imcpPacketType) {
		return dataController.getConfigStatusHandler().isPacketReceived(imcpPacketType);
	}

	public void updateConfigurationFromFile(String fileName) {
		dataController.updateConfigurationFromFile(fileName);
	}

	public boolean updateConfiguration(int packetType) {
		return dataController.updateConfiguration(packetType);
	}
	
	public synchronized void reboot() {
		dataController.sendRebootRequest();
		stop();
		isCountryCodeConfigRecved	= false;
		nodeRebooted				= true;
	}
	/**
	 * reboot required request to device
	 */
   public void isRebootRequired(){
	dataController.isRebootRequired();
  }
	public void setFipsMode(boolean set) {
		
		IConfiguration config = dataController.getConfiguration();
		//TODO this code has to be moved to configuration

		/**
		 * FORMAT OF REG DOMAIN COUNTRY CODE INFO PACKET
		 *  15   14   13   12   11   10   9    8  | 7    6    5    4    3    2     1   0
		  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
		  |            RESERVED         |FIPS|DFS |           REG DOMAIN CODE             |
		  +----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
		*/ 

		int  tempFIPS 	= Mesh.FIPS_BIT_SET;
		int regDomain 	= config.getRegulatoryDomain();
		regDomain = (set == true) ? (regDomain | tempFIPS) : (regDomain & ~tempFIPS);
		config.setRegulatoryDomain(regDomain);
		dataController.updateConfiguration(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET);
		showMessage(this,Mesh.MACRO_UPDATE_NODE_SETTINGS,Mesh.STATUS_REBOOTREQD,"");
		
	}
	
	public void restoreDefaultSettings() {
		
		if(status != AccessPoint.STATUS_ALIVE) {
			showMessage(this,Mesh.MACRO_RESTORE_FACTORY,Mesh.STATUS_FAILED_APNOTRUNNING,"");
			return;
		}
		
		dataController.restoreDefaultSettings();
		if(meshNetwork.getNwName().equalsIgnoreCase(Mesh.DEFAULT_MESH_ID) == false) {
			nodeMoved = true;
			meshNetwork.accesPointMoved(dsMacAddress.toString());
		}else {
			nodeMoved = false;
		}
		showMessage(this,Mesh.MACRO_RESTORE_FACTORY,Mesh.STATUS_REBOOTREQD,"");
	}

	public HardwareInfo getHardwareInfo() {
		return (HardwareInfo) hardwareInfo;
	}

	public IpAddress getIpAddress() {
		return dataController.getIpAddress();
	}

	/**
	 * @return
	 */
	public String getName() {
		return dataController.getName();
	}
	
	public String getNwName() {
		return meshNetwork.getNwName();
	}

	MeshNetwork getMeshNetwork() {
		return meshNetwork;
	}
	
	/**
	 * @return
	 */
	public IConfigStatusHandler getUpdateStatusHandler() {
		return dataController.getUpdateStatusHandler();
	}

	public void showMessage(final IMessageSource msgSource, final String messageHeader, final String message, final String progress) {

		if(isRunning() == false) {
			return;
		}
		
    	Enumeration<IMessageHandler> enumeration 	=  messageListeners.elements();
        while(enumeration.hasMoreElements()) {
   			final IMessageHandler listener = (IMessageHandler)enumeration.nextElement();
			listener.showMessage(msgSource, messageHeader, message, progress);
        }
	
	}

	public void addMessageListener(IMessageHandler listener) {
		messageListeners.put(listener.toString(),listener);
	}
	
	public void removeMessageListener(IMessageHandler listener) {
		messageListeners.remove(listener.toString());
	}

	public String getDSMacAddress() {
		if(this.dsMacAddress != null)
			return this.dsMacAddress.toString();
		return dataController.getDSMacAddress();
	}

	public String getNetworkId() {
		return meshNetwork.getNwName();
	}

	public void dispose() {
		notifyPropertyListeners(IAPPropListener.MASK_AP_STATE, IAPPropListener.AP_STATE_DISPOSED);
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IMessageHandler.IMessageSource#getApName()
	 */
	public String getSrcName() {
		return getName();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IRuntimeConfigListener#notify(int)
	 */
	public void notify(int notification) {
	
		switch(notification){
			case IRuntimeConfigListener.FIRST_HEARTBEAT:	
				isRoot 	= (dsMacAddress.equals(runtimeManager.getParentBSSID()) == true) ?
						true : false;
				setStatus(STATUS_STARTUP);
				meshNetwork.accesspointStarted(this);
				nodeMoved	= false;
				if(nodeRebooted == true) {
					nodeRebooted = false;
					showMessage(this, Mesh.MACRO_REBOOT_NODES, Mesh.STATUS_SUCCESSFUL, "");
				}
				/* this flag is made false after node is moved from one n/w 
				to another and its first heart beat is received*/
				break;
			case IRuntimeConfigListener.PARENT_CHANGED:	
				notifyPropertyListeners(IAPPropListener.MASK_HEARTBEAT, IAPPropListener.HEARTBEAT_MASK_PARENTADDRESS);
				break;
			case IRuntimeConfigListener.HEARTBEAT:
				setStatus(STATUS_ALIVE);
				break;
		}			

	    if(runtimeManager.isLoggingEnabled()) {
	    	/*
	    	 * this code is for future packet logging 
	    	 */
	    }    	        	
        

	
	}	
	
	/**
	 * @return Returns the parentAp.
	 */
	public AccessPoint getParentAp() {
		return parentAp;
	}

	public void setNetworkKey(String meshNetworkId, String nwkKey) {
		dataController.changeNetwork(meshNetworkId, nwkKey, Mesh.MACRO_EDIT_NETWORK);
	}

	public String getApName() {
		return dataController.getName();
	}

	public IRuntimeConfiguration getRuntimeApConfiguration() {
		return runtimeManager.getRuntimeConfiguration();
	}

	public MacAddress getApDSMacAddress(MacAddress apWMMacAdddress) {
		AccessPoint ap = meshNetwork.getAccessPointFromBssid(apWMMacAdddress);
		
		return (ap != null) ? ap.getDsMacAddress() : null;
	}
	
	public boolean isRunning() {
		return ((status == STATUS_ALIVE) || (status == STATUS_STARTUP)) ? true : false;
	}

	public AccessPoint getAccessPoint(MacAddress address) {
		return meshNetwork.getAccessPoint(address);
	}
	
	public void heartbeatMissed() {
		heartBeatMissed	= true;
		notifyPropertyListeners(IAPPropListener.MASK_AP_STATE, IAPPropListener.AP_STATE_HEARTBEAT_MISSED);
	}	

	/**
	 * @param b
	 */
	public void setCountryCodeConfigurationReceived(boolean b) {
		isCountryCodeConfigRecved	= b;
	}	
	
	public boolean isCountryCodeConfigreceived() {
		return isCountryCodeConfigRecved;
	}

	public int isMobile() {
		return dataController.getConfiguration().getMeshConfiguration().getMobilityMode();
	}

	public boolean isLocal() {
		return isLocal;
	}
	
	public synchronized int updateFirmware(String fileName, IApFwUpdateListener listener) {
		if(isLocal == false) {
			if(listener == null)
				return Mesh.FW_UPDATE_RETURN_ERROR_NEED_CALLBACK;

			this.fwUpdateListener = listener;
			return meshNetwork.updateFirmware(dsMacAddress, fileName, listener);
		}

		this.fwUpdateListener = listener;
		int ret = dataController.updateFirmware(fileName, listener);
		return ret;
	}
	
	public synchronized String executeCommand(String command, ICommandExecutionListener listener) {
		if(isLocal == false) {
			if(listener == null) {
				return "AccessPoint is remote.Need callback listener.";
			} else {
				mapRemoteMeshCommands.put(command, listener);
				return meshNetwork.executeCommand(dsMacAddress, command);
			}
		}
		
		return commandHelper.executeCommand(getIpAddress().getBytes(), command);
	}

	public void remoteCommandResponseRcvd(String command, String output) {
		ICommandExecutionListener listener = mapRemoteMeshCommands.remove(command);
		if(listener == null) {
			handleRemoteGPSUpdateResponse(command, output);
			return;
		}
		
		listener.commandExecutionResponse(this, command, output);
	}

	public void remoteFwUpdateResponseRcvd(int ret, String statusText) {

		if(fwUpdateListener != null) {
			fwUpdateListener.notifyUpdateStatus(ret, statusText);
		}
		
		switch(ret) {
			case Mesh.FW_UPDATE_RETURN_SUCCESS:
				showMessage(this, Mesh.MACRO_FIRMWARE_UPDATE, Mesh.STATUS_SUCCESSFUL, Mesh.STATUS_UPDATE_AFT_REBOOT);
				break;
			case Mesh.FW_UPDATE_RETURN_FAILED:
			case Mesh.FW_UPDATE_RETURN_ERROR_AP_NOT_FOUND:				
				showMessage(this, Mesh.MACRO_FIRMWARE_UPDATE, Mesh.STATUS_FAILED, "");
				break;
			case Mesh.FW_UPDATE_RETURN_ERROR_FW_REQ_FAILED:
				showMessage(this, Mesh.MACRO_FIRMWARE_UPDATE, Mesh.STATUS_FAILED, Mesh.STATUS_FAILED_FIRMWAREREQ);
				break;
			case Mesh.FW_UPDATE_RETURN_ERROR_FW_FILE_OPEN:
				showMessage(this, Mesh.MACRO_FIRMWARE_UPDATE, Mesh.STATUS_FAILED, Mesh.STATUS_ERROR_FIRMWARE_OPEN);
				break;
			case Mesh.FW_UPDATE_RETURN_ERROR_CONNECTION:
				showMessage(this, Mesh.MACRO_FIRMWARE_UPDATE, Mesh.STATUS_FAILED, Mesh.STATUS_FAILED_CONNECTION);
				break;
		}
		
	}

	public void remoteFwUpdateFileUpdateRcvd(int fileNo, int fileCnt) {
		if(fwUpdateListener != null) {
			fwUpdateListener.notifyApFileUpdate(fileNo, fileCnt);
		}
		
		showMessage(this, Mesh.MACRO_FIRMWARE_UPDATE, "File Update", ""+fileNo+"/"+fileCnt);
	}

	public void importSipConfiguration(ISipConfiguration sipConfig) {
		IVersionInfo		versionInfo = getVersionInfo();
		if(versionInfo.supportsSIP() == false)
			return;
		
		IConfiguration 		config 			= getConfiguration();
		ISipConfiguration 	apSipConfig 	= config.getSipConfiguration();
		
		if(sipConfig.copyTo(apSipConfig) == true) {
			updateConfiguration(PacketFactory.SIP_CONFIG_PACKET);
		} else
			showMessage(this, "Import SIP configuration", "Failed", "");
	}
	
	public synchronized int updateGps(boolean enable, ICommandExecutionListener listener) {
		
		String command = (enable == true) ? ALCONFSET_CMD_ENABLE : ALCONFSET_CMD_DISABLE;
		
		
		if(isLocal == false) {
			if(listener == null) {
				return Mesh.GPS_UPDATE_RETURN_FAILED_NEED_REMOTE_LISTENER;
			} else {
				mapRemoteGPSUpdates.put(command, listener);
				meshNetwork.executeCommand(dsMacAddress, command);
				return Mesh.GPS_UPDATE_RETURN_INIT_REMOTE_UPDATE;
			}
		}
		
		
		IpAddress ip = getIpAddress();
		ip = dataController.getConfiguration().getNetworkConfiguration().getIpAddress();
		String result = null;
		
		if (enable == true) {
			String reCommand = command.replace("0.0.0.0", ip.toString());
			result = commandHelper.executeCommand(ip.getBytes(), reCommand);
		} else {
			result = commandHelper.executeCommand(ip.getBytes(), command);
		}
		
		if(result.startsWith("Sucess") == false) {
			return Mesh.GPS_UPDATE_RETURN_FAILED;
		}
		
		result = commandHelper.executeCommand(ip.getBytes(), ALCONFSET_SAVE);
		if(result.endsWith("Sucess[config saved]\n") == false) {
			return Mesh.GPS_UPDATE_RETURN_FAILED;
		}
		
		return Mesh.GPS_UPDATE_RETURN_SUCCESS;
	}
	
	private void handleRemoteGPSUpdateResponse(String command, String result) {
		
		ICommandExecutionListener listener = mapRemoteGPSUpdates.remove(command);
		
		if(command.equalsIgnoreCase(ALCONFSET_CMD_ENABLE) ||
			command.equalsIgnoreCase(ALCONFSET_CMD_DISABLE)) {

			if(result.startsWith("Sucess") == false) {
				if(listener != null)
					listener.commandExecutionResponse(this, command, "Failed");
				
				showMessage(this, "Remote GPS Update["+dsMacAddress.toString()+"]", "Failed", "");		
				return;
			}
			
			meshNetwork.executeCommand(dsMacAddress, ALCONFSET_SAVE);
			mapRemoteGPSUpdates.put(ALCONFSET_SAVE, listener);
			
		} else if(command.equalsIgnoreCase(ALCONFSET_SAVE)) {

			if(result.endsWith("Sucess[config saved]\n") == false) {
				if(listener != null)
					listener.commandExecutionResponse(this, command, "Failed");
				
				showMessage(this, "Remote GPS Update["+dsMacAddress.toString()+"]", "Failed", "");		
				return;
			}
			
			if(listener != null)
				listener.commandExecutionResponse(this, command, "Successful");
			
			showMessage(this, "Remote GPS Update["+dsMacAddress.toString()+"]", "Successful", "");		
			
		}
			
	}

	public String executePerformanceTest(int recordCount, short type, short protocol, int udpBandWidth, IperfListener listener) {
		if(isLocal == false) {
			if(listener == null) 
				return "AccessPoint is remote.Need callback listener.";
			else
				return meshNetwork.executePerformanceTest(getDsMacAddress(), recordCount, type, protocol, udpBandWidth, listener);
		}
		
		return iperfHelper.executeTest(getIpAddress(), recordCount, type, protocol, udpBandWidth, listener);
	}
	
	public boolean isP3MEnabled() {
		return dataController.isP3MEnabled();
	}
	
	public boolean isPBVEnabled() {
		return dataController.isPBVEnabled();
	}
	public synchronized  boolean buildImage(String macAddress,final IApFwUpdateListener listener,String deviceType,String version){
		return dataController.buildImage(macAddress,listener,deviceType,version);		
	}	
	public boolean isDHCPEnabled() {
		return dataController.isDHCPEnabled();
	}
	
	public boolean isIGMPEnabled() {
		return dataController.isIGMPEnabled();
	}
	
	public boolean moveNode_localNMSTOremoteNMS(String gatewayAddress,boolean enable){
		return dataController.moveNode_localNMSTORemoteNMS(gatewayAddress,enable);
	}
	public synchronized int firmwareUpgrade(){
	      return dataController.firwareupgrade();
}
}
