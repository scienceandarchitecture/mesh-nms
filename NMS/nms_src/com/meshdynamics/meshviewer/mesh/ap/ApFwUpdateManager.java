package com.meshdynamics.meshviewer.mesh.ap;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.meshdynamics.meshviewer.imcppackets.FirmwareUpdateRequestPacket;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.imcppackets.helpers.IMCPInfoIDS;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.PacketSender;
import com.meshdynamics.meshviewer.mesh.ap.FileHandler.FileData;
import com.meshdynamics.meshviewer.meshupdates.updatepackets.FileCountPacket;
import com.meshdynamics.meshviewer.meshupdates.updatepackets.MeshUpdateResponsePacket;
import com.meshdynamics.meshviewer.meshupdates.updatepackets.QuitPacket;
import com.meshdynamics.meshviewer.meshupdates.updatepackets.StorePacket;
import com.meshdynamics.util.HexHelper;
import com.meshdynamics.util.MD5;

public class ApFwUpdateManager {
	
	private static final int MSUP_UPDATE_TYPE_DRV 			= 1;	// driver	
	private static final int MSUP_UPDATE_TYPE_CFD 			= 2;	// config	
	private static final int MSUP_UPDATE_TYPE_MAP 			= 4;	// meshap		
	private static final int MSUP_UPDATE_TYPE_SCP 			= 8;	// scripts
	private static final int MSUP_UPDATE_TYPE_WEB 			= 16;	// web pages
	private static final int MSUP_UPDATE_TYPE_OTH 			= 32;	// other
	private static final int MSUP_UPDATE_TYPE_EXE 			= 64;	// executables
	private static final int MSUP_UPDATE_TYPE_DMN 			= 128;	// daemons
	
	private static final String MSUP_STR_UPDATE_TYPE_DRV 	= "DRV";
	private static final String MSUP_STR_UPDATE_TYPE_CFD 	= "CFD";	
	private static final String MSUP_STR_UPDATE_TYPE_MAP 	= "MAP";
	private static final String MSUP_STR_UPDATE_TYPE_SCP 	= "SCP";
	private static final String MSUP_STR_UPDATE_TYPE_WEB 	= "WEB";
	private static final String MSUP_STR_UPDATE_TYPE_OTH 	= "OTH";
	private static final String MSUP_STR_UPDATE_TYPE_EXE 	= "EXE";
	private static final String MSUP_STR_UPDATE_TYPE_DMN 	= "DMN";
	
	private AccessPoint 		ap;
	private FileHandler 		ftpHandler;
	private PacketSender		packetSender;
	private Vector<FileData> 	fileDataList;
	
	ApFwUpdateManager(AccessPoint ap, PacketSender packetSender) {
		this.ap 			= ap;
		this.packetSender 	= packetSender;
		ftpHandler			= null;
		fileDataList		= null;
	}
	
	private int verifyAndLoadSourceData(String filePath) {
		
		try {
			ZipFile srcZipFile = new ZipFile(filePath);
			ZipEntry iniEntry = srcZipFile.getEntry("installation.inf");
			if (iniEntry == null) {
				return Mesh.FW_UPDATE_RETURN_FAILED;
			}

			BufferedReader 	inReader 	= new BufferedReader(new InputStreamReader(
												srcZipFile.getInputStream(iniEntry)));
			fileDataList 				= new Vector<FileData>();
			String 			line 		= "";
			
			while ((line = inReader.readLine()) != null) {

				String[] tokens = line.split(",");
				if (tokens.length != 3) {
					//bad formatted line so ignore entry
					continue;
				}

				FileData fData = new FileData();
				
				fData.fileName = tokens[0];

				if (tokens[1].equals(MSUP_STR_UPDATE_TYPE_CFD)) 
					fData.updateType = MSUP_UPDATE_TYPE_CFD;
				else if (tokens[1].equals(MSUP_STR_UPDATE_TYPE_DRV))
					fData.updateType = MSUP_UPDATE_TYPE_DRV;
				else if (tokens[1].equals(MSUP_STR_UPDATE_TYPE_MAP))
					fData.updateType = MSUP_UPDATE_TYPE_MAP;
				else if (tokens[1].equals(MSUP_STR_UPDATE_TYPE_SCP)) 
					fData.updateType = MSUP_UPDATE_TYPE_SCP;
				else if (tokens[1].equals(MSUP_STR_UPDATE_TYPE_WEB)) 
					fData.updateType = MSUP_UPDATE_TYPE_WEB;
				else if (tokens[1].equals(MSUP_STR_UPDATE_TYPE_OTH)) 
					fData.updateType = MSUP_UPDATE_TYPE_OTH;
				else if (tokens[1].equals(MSUP_STR_UPDATE_TYPE_EXE)) 
					fData.updateType = MSUP_UPDATE_TYPE_EXE;
				else if (tokens[1].equals(MSUP_STR_UPDATE_TYPE_DMN)) 
					fData.updateType = MSUP_UPDATE_TYPE_DMN;
					
				fData.filePath = tokens[2];
				fileDataList.add(fData);

			}

			srcZipFile.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return Mesh.FW_UPDATE_RETURN_ERROR_FW_FILE_OPEN;
		}

		return Mesh.FW_UPDATE_RETURN_SUCCESS;
	}
	
	private int startUpdateServerOnAp() {

		/** Send Request Packet */
		
		FirmwareUpdateRequestPacket fwuRequest = (FirmwareUpdateRequestPacket) PacketFactory.getNewIMCPInstance(PacketFactory.AP_FW_UPDATE_REQUEST);
	  /**
	   * according to new IMCp structure 
	   */
		fwuRequest.setFIRMWARE_UPDATE_REQ_ID(IMCPInfoIDS.IMCP_SW_UPDATE_REQUEST_INFO_ID);
		fwuRequest.setFIRMWARE_UPDATE_REQ_LENGTH(fwuRequest.getPacketLength());
		
		fwuRequest.setDsMacAddress(ap.getDsMacAddress());
		fwuRequest.setMeshId(ap.getNwName());		
		fwuRequest.setUserId("Admin");
		fwuRequest.setFwVersion((long)5);		
		
		if(!packetSender.sendPacketWithResponse(fwuRequest, null, 5000)) { 
	        return Mesh.FW_UPDATE_RETURN_ERROR_FW_REQ_FAILED;
		}

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			Mesh.logException(e1);
		}
		
		return Mesh.FW_UPDATE_RETURN_SUCCESS;
	}
	
	private int openFtpConnection() {
		
		ftpHandler 	= new FileHandler();
		
		for(int i=0;i<3;i++) {
		    if(ftpHandler.openConnection(ap.getIpAddress()) == FileHandler.SUCCESS) {
		        return Mesh.FW_UPDATE_RETURN_SUCCESS;
		    }
		    try {
                Thread.sleep(1000);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
		}
		
		ftpHandler = null;
        return Mesh.FW_UPDATE_RETURN_ERROR_CONNECTION;

	}

	private int doLocalUpdate(String filePath, IApFwUpdateListener listener) {

		ZipFile srcZipFile = null;
		try {
			srcZipFile = new ZipFile(filePath);
		} catch (IOException e) {
			System.out.println(e.getMessage());
			return Mesh.FW_UPDATE_RETURN_ERROR_FW_FILE_OPEN;
		}
		
		FileCountPacket fcntPacket 	= new FileCountPacket();
		fcntPacket.setCount(fileDataList.size());
		
		short responseCode = ftpHandler.sendPacket(fcntPacket.createPacket());
		
		if(responseCode != MeshUpdateResponsePacket.UPDATE_PACKET_RESP_TYPE_SUCCESS)
			return Mesh.FW_UPDATE_RETURN_FAILED;
		
		responseCode = Mesh.FW_UPDATE_RETURN_SUCCESS;
	
		int fileNo = -1;
		if(listener != null)
			listener.notifyApFileUpdate(fileNo, fileDataList.size());
		
		for (FileData fData : fileDataList) {
			
			byte[] 			arr 		= null;
			ZipEntry 		zipEntry 	= srcZipFile.getEntry(fData.fileName);  
			
			fileNo++;
			
			if(zipEntry == null) 
				continue;
			
			try {
				DataInputStream dStream = new DataInputStream(srcZipFile.getInputStream(zipEntry));				
				arr 					= new byte[(int)zipEntry.getSize()];
				int offset 				= 0;
				long remainingBytes		= zipEntry.getSize();
				
				while (offset < remainingBytes) {
					offset += dStream.read(arr, offset, (int)(remainingBytes-offset));
				}
				dStream.close();
				
			} catch (Exception e) {
				Mesh.logException(e); 
			}			
			
 			/* Calling store command & then sending file*/
			int ret = sendFile(arr, fData);	
		
			if (ret == MeshUpdateResponsePacket.UPDATE_PACKET_RESP_TYPE_SUCCESS) {
				if(listener != null)
					listener.notifyApFileUpdate(fileNo, fileDataList.size());
			} else { 
				responseCode = Mesh.FW_UPDATE_RETURN_FAILED;
				break;
			}			
		}
		
		if(responseCode == Mesh.FW_UPDATE_RETURN_SUCCESS) {
			if(listener != null)
				listener.notifyUpdateStatus(Mesh.FW_UPDATE_RETURN_SUCCESS_UPDATE_STATUS, "Saving files. Please wait...");
		}
		
		QuitPacket quitPacket	= new QuitPacket();
		ftpHandler.sendPacket(quitPacket.createPacket());
		
		return responseCode;
		
	}
	
	private short sendFile(byte[] fileBytes, FileData fData) {

		MD5 	msgDigest 	= new MD5();
		String 	key128 		= "";		
		char[] 	data	 	= new char[16];
				
		msgDigest.keyGenerate(fileBytes, fileBytes.length, data);
		for(int i = 0;i<16;i++)
			key128 += data[i];			
			  			  			
		key128				= HexHelper.toHexString(key128);
		short[] checkSum 	= HexHelper.stringToHex(key128); 			
		
		StorePacket stoPacket 	= new StorePacket();
		stoPacket.setUpdateType((short)fData.updateType);
		stoPacket.setPath(fData.filePath + "/" + fData.fileName);
		stoPacket.setFileSize(fileBytes.length);
		stoPacket.setChecksum(checkSum);		
		
		byte[] 	dataToSend 		= stoPacket.createPacket();		
		short 	responseCode	= -1;
		
		try {
			responseCode = ftpHandler.sendPacket(dataToSend);
			if(responseCode != MeshUpdateResponsePacket.UPDATE_PACKET_RESP_TYPE_SUCCESS)
				return responseCode;
			
			responseCode = ftpHandler.sendFile(fileBytes);
			
		} catch(Exception e) {
			Mesh.logException(e);
			responseCode = MeshUpdateResponsePacket.UPDATE_PACKET_RESP_TYPE_FAILED;
		}
		
		return responseCode;
	}

	private int updateFirmwareLocal(String filePath, IApFwUpdateListener listener) {
		
		int ret;

		ret = startUpdateServerOnAp();
		if(ret != Mesh.FW_UPDATE_RETURN_SUCCESS)
			return ret;
		
		if(listener != null)
			listener.notifyUpdateStatus(Mesh.FW_UPDATE_RETURN_SUCCESS_UPDATE_STATUS, "Opening Connection ...");
		
		ret = openFtpConnection(); 
		if(ret != Mesh.FW_UPDATE_RETURN_SUCCESS) {
			return ret;
		}
		if(listener != null)
			listener.notifyUpdateStatus(Mesh.FW_UPDATE_RETURN_SUCCESS_UPDATE_STATUS, "Connection established...");
		
		ret = doLocalUpdate(filePath, listener);
		
		if(listener != null)
			listener.notifyUpdateStatus(Mesh.FW_UPDATE_RETURN_SUCCESS_UPDATE_STATUS, "Connection closed.");
		
		ftpHandler.closeConnection();
		
		return ret;
	}

	int updateFirmware(String filePath, IApFwUpdateListener listener) {
		int ret = verifyAndLoadSourceData(filePath);
		if(ret != Mesh.FW_UPDATE_RETURN_SUCCESS)
			return ret;
		
		return updateFirmwareLocal(filePath, listener);
	}
	
}
