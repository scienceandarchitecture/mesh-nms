/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApDataController.java
 * Comments : 
 * Created  : Apr 10, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  4  |Nov 06, 2007 | notification to the AP added for regDmn pkt     | Imran   |
 *  --------------------------------------------------------------------------------
 * |  3  |Jun 11, 2007 | updateAllConfiguration method added             | Imran  |
 * --------------------------------------------------------------------------------
 * |  2  |May 10, 2007 | IMessageSource implemented						 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  1  |May 03, 2007 | getUpdateStatusHandler added					 |Abhishek|
 * --------------------------------------------------------------------------------
 * |  0  |Apr 10, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh.ap;

import com.meshdynamics.macAddressVerification.ApMacAddressAuth_Manager;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.Configuration;
import com.meshdynamics.meshviewer.imcppackets.ACLInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApAckTimeoutInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApAckTimeoutRequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.ApConfigInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApConfigRequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.ApHwInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApIfChannelInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApIpConfigInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApRegDomainInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ApRegDomainRequestPacket;
import com.meshdynamics.meshviewer.imcppackets.ConfigRequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.DLSaturationInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.Dot11eCategoryPacket;
import com.meshdynamics.meshviewer.imcppackets.Dot11eIfInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.EffistreamConfigPacket;
import com.meshdynamics.meshviewer.imcppackets.EffistreamRequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.FirmwareUpdateResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.Firmware_SW_UPDATE_RESPONSE;
import com.meshdynamics.meshviewer.imcppackets.GenericCommandRequestResponse;
import com.meshdynamics.meshviewer.imcppackets.HeartBeat2Packet;
import com.meshdynamics.meshviewer.imcppackets.HeartbeatPacket;
import com.meshdynamics.meshviewer.imcppackets.IMCPPacket;
import com.meshdynamics.meshviewer.imcppackets.IfHiddenESSIDInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.ImcpKeyUpdateResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.MacAddressRequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.MgmtgwResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.imcppackets.RFCustomChannelInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.RFCustomChannelInfoRequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.SipConfigPacket;
import com.meshdynamics.meshviewer.imcppackets.StaAssocPacket;
import com.meshdynamics.meshviewer.imcppackets.StaDisassocPacket;
import com.meshdynamics.meshviewer.imcppackets.StaInfoPacket;
import com.meshdynamics.meshviewer.imcppackets.VLanConfigInfoPacket;
import com.meshdynamics.meshviewer.mesh.IMessageHandler.IMessageSource;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.PacketSender;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

public class ApDataController implements IMessageSource {
	
	private ApConfigurationManager 			configManager;
	private ApUpdateManager					updateManager;
	private Configuration					configuration;
	private AccessPoint						ap;
	private PacketSender					packetSender;
	private String 							meshId;
	private ApRuntimeConfigurationManager	runtimeManager;
	private ApResponseHandler				responseHandler;
	private ApFwUpdateManager				fwUpdateManager;
	private ApMacAddressAuth_Manager        apMacAddress_Manager;
	
	ApDataController(AccessPoint ap,PacketSender packetSender) {
		this.ap				= ap;
		this.packetSender	= packetSender;
		configuration		= new Configuration();
		configManager		= new ApConfigurationManager(this,configuration);
		updateManager		= new ApUpdateManager(this,configuration,ap);
		runtimeManager		= new ApRuntimeConfigurationManager(this,ap,ap.getMeshNetwork());
		responseHandler		= new ApResponseHandler(this);
		fwUpdateManager		= new ApFwUpdateManager(ap, packetSender);
		apMacAddress_Manager = new ApMacAddressAuth_Manager(this,ap);
	}
	
void processPacket(IMCPPacket packet) {
		
		meshId	= packet.getMeshId();
		switch(packet.getPacketType()) {
			case PacketFactory.HEARTBEAT:
				if(configManager.getConfigStatusHandler().isPacketReceived(PacketFactory.AP_HW_INFO) == false) {
					 clearData();
					 clearStatus(); 
					 configuration.setMacAddress(packet.getDsMacAddress());
					 updateManager.requestHardwareInfo();					
				 }
				if(configManager.getConfigStatusHandler().isPacketReceived(PacketFactory.IMCP_MAC_ADDR_REQUEST_RESPONSE) == false){
					//configuration.setMacAddress(packet.getDsMacAddress());
					updateManager.macAddr_request();
				  }
				 runtimeManager.setHeartBeatInfo((HeartbeatPacket)packet);
				 break;
			case PacketFactory.HEARTBEAT2:
				 runtimeManager.setHeartBeat2Info((HeartBeat2Packet)packet);
				 break;
			case PacketFactory.ACL_INFO_PACKET:
				configManager.setACLInfo((ACLInfoPacket) packet);
				break;
			case PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET:
				configManager.setApAckTimeoutInfo((ApAckTimeoutInfoPacket) packet);
				break;
			case PacketFactory.AP_ACK_TIMEOUT_REQUEST_PACKET:
				updateManager.ackTimeoutResponseReceived((ApAckTimeoutRequestResponsePacket)packet);
				break;
			case PacketFactory.AP_CONFIGURATION_INFO:
				configManager.setApConfigInfo((ApConfigInfoPacket) packet);
				break;
			case PacketFactory.AP_CONFIG_REQUEST_RESPONSE:
				updateManager.apConfigResponseReceived((ApConfigRequestResponsePacket)packet);
				break;
			case PacketFactory.AP_HW_INFO:
				configManager.setHwConfigInfo((ApHwInfoPacket) packet);
				break;
			case PacketFactory.AP_IF_CHANNEL_INFO_PACKET:
				configManager.setApIfChannelInfo((ApIfChannelInfoPacket) packet);
				break;
			case PacketFactory.AP_IP_CONFIGURATION_INFO:
				configManager.setIpConfigInfo((ApIpConfigInfoPacket) packet);
				break;
			case PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET:
				configManager.setApRegDmnCntCodeInfo((ApRegDomainInfoPacket) packet);
				checkCustomChannels((ApRegDomainInfoPacket) packet);
				ap.setCountryCodeConfigurationReceived(true);
				break;
			case PacketFactory.AP_REG_DOMAIN_CCODE_REQUEST_PACKET:
				updateManager.regDomainResponseReceived((ApRegDomainRequestPacket)packet);
				responseHandler.processResponse((ApRegDomainRequestPacket) packet);
				break;
			case PacketFactory.CONFIG_REQ_RES_PACKET:
				updateManager.configResponseReceived((ConfigRequestResponsePacket)packet);
				break;
			case PacketFactory.DN_LINK_SATURATION_INFO_PACKET:
				configManager.setSaturationInfo((DLSaturationInfoPacket) packet);
				break;
			case PacketFactory.DOT11E_CATEGORY_INFO_PACKET:
				configManager.setDot11eCategoryInfo((Dot11eCategoryPacket) packet);
				break;
			case PacketFactory.DOT11E_IFCONFIG_INFO_PACKET:
				configManager.setDot11eIfInfo((Dot11eIfInfoPacket) packet);
				break;
			case PacketFactory.EFFISTREAM_CONFIG_PACKET:
				configManager.setEffistreamInfo((EffistreamConfigPacket) packet);
				break;
			case PacketFactory.EFFISTREAM_CONFIG_REQUEST_RESPONSE_PACKET:
				updateManager.effistreamResponseReceived((EffistreamRequestResponsePacket)packet);
				break;
			case PacketFactory.GENERIC_COMMAND_REQUEST_RESPONSE_PACKET:
				updateManager.genericCommandResponseReceived((GenericCommandRequestResponse)packet);
				break;
			case PacketFactory.IMCP_KEY_UPDATE_RESPONSE:
				updateManager.imcpKeyUpdateResponseReceived((ImcpKeyUpdateResponsePacket)packet);
				//responseHandler.responseReceived((ImcpKeyUpdateResponsePacket)packet);
				break;
			case PacketFactory.IF_HIDDEN_ESSID_PACKET:
				configManager.setIfHiddenESSIDInfo((IfHiddenESSIDInfoPacket) packet);
				break;
			case PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET:
				configManager.setRfCustomChannelInfo((RFCustomChannelInfoPacket) packet);
				break;
			case PacketFactory.RF_CUSTOM_CHANNEL_REQUEST_RESPONSE_PACKET:
				updateManager.customChannelResponseReceived((RFCustomChannelInfoRequestResponsePacket)packet);
			    responseHandler.processResponse((RFCustomChannelInfoRequestResponsePacket)packet);
				break;
			case PacketFactory.AP_VLAN_CONFIGURATION_INFO:
				configManager.setVlanConfigInfo((VLanConfigInfoPacket) packet);
				break;
			case PacketFactory.STA_ASSOC_NOTIFICATION:
				runtimeManager.setStaAssocNotification((StaAssocPacket)packet);
				break;
			case PacketFactory.STA_DISASSOC_NOTIFICATION:
				runtimeManager.setStaDissocNotification((StaDisassocPacket)packet);
				break;
			case PacketFactory.STA_INFO_PACKET:
				runtimeManager.setStaInfoPacket((StaInfoPacket)packet);
				break;
			case PacketFactory.AP_FW_UPDATE_RESPONSE:
				PacketSender.responseReceived((FirmwareUpdateResponsePacket)packet);
				break;
			case PacketFactory.SIP_CONFIG_PACKET:
				configManager.setSipConfig((SipConfigPacket) packet);
				break;
			case PacketFactory.IMCP_MAC_ADDR_REQUEST_RESPONSE:
				 apMacAddress_Manager.updateMacInfo((MacAddressRequestResponsePacket)packet);
				break;
			case PacketFactory.MGMT_GW_RESPONSE:
				updateManager.mgmtgwResponseReceived((MgmtgwResponsePacket)packet);
				break;
			case PacketFactory.IMCP_SW_UPDATE_RESPONSE:
				 updateManager.updateFirmware((Firmware_SW_UPDATE_RESPONSE)packet);
				break;	
		}
		
	}



	private void checkCustomChannels(ApRegDomainInfoPacket packet) {
		
		if(packet.getCountryCode() != Mesh.CUSTOM_COUNTRY_CODE) {
			return;
		}
		updateManager.requestConfiguration(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);

	}

	boolean allConfigurationReceived() {
		return configManager.allConfigurationReceived();
	}
	
	void requestMissingConfiguration(boolean forceRequestAllConfig) {
		updateManager.requestMissingConfiguration(forceRequestAllConfig);
	}
	
	boolean canConfigure() {

		if (configManager.allConfigurationReceived() == false) {
			return false;
		}
		if (configuration.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,
												IVersionInfo.MINOR_VERSION_5,
												IVersionInfo.VARIANT_VERSION_30) == false) {
			return true;
		}
		if (configuration.getCountryCode() == Mesh.CUSTOM_COUNTRY_CODE) {
			if (configManager.getConfigStatusHandler().isPacketReceived(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET) == false) {
				return false;
			}
		}

		return true;
	}

	ICapabilityProvider getCapabilityProvider() {
		return ap;
	}

	PacketSender getPacketSender() {
		return packetSender;
	}

	IConfigStatusHandler getRequestStatusHandler() {
		return configManager.getRequestStatusHandler();
	}

	public IConfigStatusHandler getConfigStatusHandler() {
		return configManager.getConfigStatusHandler();
	}

	IVersionInfo getVersionInfo() {
		return configuration;
	}

	String getModel() {
		return configuration.getHardwareModel();
	}

	String getFirmwareVersion() {
		return configuration.getFirmwareVersion();
	}
	
	boolean updateChangedConfiguration() {
		return updateManager.updateChangedConfiguration();
	}
	
	boolean updateAllConfiguration() {
		return updateManager.updateAllConfiguration();
	}

	String getMeshNetworkId() {
		return meshId;
	}

	String getMeshNetworkKey() {
		return ap.getNetworkKey();
	}

	IConfiguration getConfiguration() {
		return configuration;
	}

	void requestStaInfo(MacAddress staAddress) {
		updateManager.requestStaInfo(staAddress);
	}

	boolean containsAddress(MacAddress bssid) {
		IInterfaceConfiguration ifConfig = configuration.getInterfaceConfiguration();
		
		for(int i=0;i<ifConfig.getInterfaceCount();i++) {
			IInterfaceInfo ifInfo = (IInterfaceInfo)ifConfig.getInterfaceByIndex(i);
			if(ifInfo == null) {
				continue;
			}
				
			if(bssid.equals(ifInfo.getMacAddress()) == true) {
				return true;
			}
		}
		return false;
	}

	boolean changeNetwork(String newNetworkId, String newNetworkKey, String macoMessage) {
		return updateManager.sendIMCPKeyUpdate(newNetworkId,newNetworkKey, macoMessage);
	}

	void updateConfigurationFromFile(String fileName) {
		updateManager.updateConfigurationFromFile(fileName);
	}

	void sendRebootRequest() {
		updateManager.sendRebootRequest();
	}
	
	void setName(String name) {
		configuration.setNodeName(name);
	}

	String getName() {
		return configuration.getNodeName();
	}
	
	void setDescription(String description) {
		configuration.setDescription(description);
	}
	
	String getDescription() {
		return configuration.getDescription();
	}
	
	void clearData() {
		runtimeManager.clearData();
		configManager.clearData();
		updateManager.clearData();
	}
	
	void clearStatus(){
		runtimeManager.clearStatus();
		configManager.clearStatus();
		updateManager.clearStatus();
	}

	void restoreDefaultSettings() {
		/*
		 * We no longer support versions less than 2.4.8 
		 * hence no need for follwing code
		 */
/*		
		if(configuration.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,
											IVersionInfo.MINOR_VERSION_3,
											IVersionInfo.VARIANT_VERSION_7
											) == false) {
			String fileName 	= 	MFile.getTemplatePath() + "/" + 
								configuration.getHardwareModel() + ".mtf";
			updateManager.updateConfigurationFromFile(fileName);
		}else{
			updateManager.restoreDefaultSettings();
		}
*/		
		updateManager.restoreDefaultSettings();
	}
	

	boolean updateConfiguration(int packetType) {
		return updateManager.updateConfiguration(packetType);
	}

	IpAddress getIpAddress() {
		return configuration.getNetworkConfiguration().getIpAddress();
	}
	
	IConfigStatusHandler getUpdateStatusHandler(){
		return updateManager.getUpdateStatusHandler();
	}
	
	public String getDSMacAddress() {
		return configuration.getDSMacAddress().toString();
	}

	public String getNetworkId() {
		return meshId;
	}
	
	IMessageSource getMessageSource() {
		return this;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IMessageHandler.IMessageSource#getApName()
	 */
	public String getSrcName() {
		return getName();
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IApDataManager#getRuntimeManager()
	 */
	public ApRuntimeConfigurationManager getRuntimeManager() {
		return runtimeManager;
	}

	MacAddress getApDsMacAddress(MacAddress apWMMacAdddress) {
		return ap.getApDSMacAddress(apWMMacAdddress);
	}

	AccessPoint getAccessPoint(MacAddress address){
		return ap.getAccessPoint(address);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IResponseNotifier#setApMoved(boolean)
	 */
	public void setApMoved(boolean moved) {
		ap.setNodeMoved(moved);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IResponseNotifier#isCountryCodeConfigReceived()
	 */
	public boolean isCountryCodeConfigAlreadyReceived() {
		return ap.isCountryCodeConfigreceived();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IResponseNotifier#isCountryCodeChanged()
	 */
	public boolean isCountryCodeChanged() {
		return configuration.isCountryCodeChanged();
	}

	int updateFirmware(String filePath, IApFwUpdateListener listener) {
		return fwUpdateManager.updateFirmware(filePath, listener);
	}

	void setRfUpdateSent(boolean b) {
		runtimeManager.setRfUpdateSent(b);
	}

	void setCountryCodeChanged(boolean b) {
		runtimeManager.setCountryCodeChanged(b);
	}

	boolean isIGMPEnabled() {

    	if(configuration.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2, IVersionInfo.MINOR_VERSION_5,IVersionInfo.VARIANT_VERSION_50) == false)
    		return false;
    	
		IMeshConfiguration	meshConfiguration	= configuration.getMeshConfiguration();
		if(meshConfiguration == null) {
			return false;
		}
		
		return meshConfiguration.getIGMPEnabled();
	}

	boolean isDHCPEnabled() {
		IMeshConfiguration	meshConfiguration	= configuration.getMeshConfiguration();
		if(meshConfiguration == null) {
			return false;
		}
		
		return meshConfiguration.getDHCPEnabled();
	}

	boolean isPBVEnabled() {
		IMeshConfiguration	meshConfiguration	= configuration.getMeshConfiguration();
		if(meshConfiguration == null) {
			return false;
		}
		return meshConfiguration.getPBVEnabled();
	}

	boolean isP3MEnabled() {
		IMeshConfiguration	meshConfiguration	= configuration.getMeshConfiguration();
		if(meshConfiguration == null) {
			return false;
		}
		
		return meshConfiguration.getP3MMode();
	}
	public void isRebootRequired(){
		updateManager.isRebootRequired();
	}
	public void haltNode(){
		updateManager.haltNode();
	}
	
	public boolean buildImage(String macAddress,IApFwUpdateListener listener,String deviceType,String version){
		return apMacAddress_Manager.buildImage(macAddress,listener,deviceType,version);
	}
	
	public boolean moveNode_localNMSTORemoteNMS(String gatewayAddress,boolean enable){
		return updateManager.remoteNMS(gatewayAddress,enable);
	}
	public int firwareupgrade(){
		return updateManager.firmwareUpgrading();
	}
}
