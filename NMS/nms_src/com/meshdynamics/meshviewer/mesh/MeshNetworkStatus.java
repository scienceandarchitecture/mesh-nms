package com.meshdynamics.meshviewer.mesh;

import java.util.Enumeration;

import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.configuration.IStaConfiguration;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;

public class MeshNetworkStatus implements IMeshNetworkStatus {
	
	private MeshNetwork	meshNetwork;
	
	private long	minSignal;
	private long	maxSignal;
	private long	minBitRate;
	private long 	maxBitRate;
	private long	upTime;
	
	public MeshNetworkStatus(MeshNetwork meshNetwork) {
		this.meshNetwork 	= meshNetwork;
		minSignal			= 0;
		maxSignal			= 0;
		minBitRate			= 0;
		maxBitRate			= 0;
	}
	
	public long getUpTime() {
		return upTime;
	}
	
	public String getNetworkName() {
		return meshNetwork.getNwName();
	}

	public int getActiveApCount() {
		return meshNetwork.getAliveApCount();
	}

	public long getMinSignal() {
		return minSignal;
	}

	public long getMaxSignal() {
		return maxSignal;
	}

	public long getMinBitRate() {
		return minBitRate;
	}

	public long getMaxBitRate() {
		return maxBitRate;
	}

	public int getTotalApCount() {
		return meshNetwork.getAPCount();
	}
	
	public void update() {

		upTime				= 0;
		minSignal			= 0;
		maxSignal			= -96;
		minBitRate			= 54;
		maxBitRate			= 0;
		
		Enumeration<AccessPoint> aps = meshNetwork.getAccessPoints();
		while(aps.hasMoreElements()) {
			AccessPoint ap = aps.nextElement();
			update(ap);
		}
		
	}
	
	private void update(AccessPoint src) {
		
		IRuntimeConfiguration runtimeConfig = src.getRuntimeApConfiguration();

		long upTime = runtimeConfig.getHeartbeat1SequenceNumber() * runtimeConfig.getHeartbeatInterval();
		this.upTime = (upTime > this.upTime) ? upTime : this.upTime;

		long signal 	= runtimeConfig.getParentSignal();
		if(signal != 0) {
			if(src.isRoot() == false) {
				this.minSignal 	= (signal < this.minSignal) ? signal : this.minSignal;
				this.maxSignal 	= (signal >= this.maxSignal) ? signal : this.maxSignal;
			}
		}
		
		long bitRate 	= runtimeConfig.getBitRate();
		if(bitRate > 0) {
			if(src.isRoot() == false) {
				this.minBitRate = (bitRate < this.minBitRate) ? bitRate : this.minBitRate;
				this.maxBitRate = (bitRate >= this.maxBitRate) ? bitRate : this.maxBitRate;
			}
		}
		
		if(src.isRoot() == false) {
			bitRate 		= runtimeConfig.getUplinkTransmitRate();
			if(bitRate > 0) {
				this.minBitRate = (bitRate < this.minBitRate) ? bitRate : this.minBitRate;
				this.maxBitRate = (bitRate >= this.maxBitRate) ? bitRate : this.maxBitRate;
			}
		}
		
		Enumeration<IStaConfiguration> staList = runtimeConfig.getStaList();
		while(staList.hasMoreElements()) {
			
			IStaConfiguration staConfig = staList.nextElement();
			
			if(staConfig.getIsIMCP() != 1)
				continue;
			
			signal 			= staConfig.getSignal();
			if(signal != 0) {
				this.minSignal 	= (signal < this.minSignal) ? signal : this.minSignal;
				this.maxSignal 	= (signal >= this.maxSignal) ? signal : this.maxSignal;
			}
		
			bitRate 		= staConfig.getBitRate();
			if(bitRate > 0) {
				this.minBitRate = (bitRate < this.minBitRate) ? bitRate : this.minBitRate;
				this.maxBitRate = (bitRate >= this.maxBitRate) ? bitRate : this.maxBitRate;
			}
			
		}
	
	}

}
