/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : IMessageHandler.java
 * Comments : 
 * Created  : Apr 13, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Apr 13, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh;

/**
 * 
 * @author Abhijit Ayarekar
 * This interface should be implemented by a class who wishes to handle
 * notifications sent out by non UI classes.
 */
public interface IMessageHandler {
	
	public interface IMessageSource {
		
		public String getDSMacAddress();
		public String getNetworkId();
		public String getSrcName();
	}
	
	public void showMessage(IMessageSource msgSource, String messageHeader, String message, String progress);
}
