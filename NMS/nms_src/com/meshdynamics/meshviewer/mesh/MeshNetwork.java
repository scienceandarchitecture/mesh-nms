/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshNetwork.java
 * Comments : 
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * | 75  |Sep 06, 2007 | Restructuring									 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 74  |Aug 14,2007  | Sorting of Ap on reboot added          	 	 |Abhishek| 
 * --------------------------------------------------------------------------------
 * | 73  |Jul 30,2007  | changes for ap moved and notification     	 	 |Imran   | 
 * --------------------------------------------------------------------------------
 * | 72  |Jul 25,2007  | UpdateConfiguration is added           	 	 |Abhishek| 
 * --------------------------------------------------------------------------------
 * | 71  |Jul 30, 2007 | network type added                              | Imran  |
 *  --------------------------------------------------------------------------------
 * | 70  |May 28,2007  |heartbeat references removed             		 |Abhishek|
 * --------------------------------------------------------------------------------
 * | 69  |May 25,2007  |updateDLSaturationInfo method modified			 |Imran   |
 *  -------------------------------------------------------------------------------
 * | 68  |Mar 14,2007  |additions for effistream						 | bindu  |
 * --------------------------------------------------------------------------------
 * | 67  |Mar 26,2007  |updateRegDomainPacket modiified for fips		 |Abhishek|
 * --------------------------------------------------------------------------------
 * | 66  |Mar 21,2007  |Changes due to removal of reboot checker		 |Abhijit |
 * |	 |			   |Viewer starting logic changed					 |		  |
 *---------------------------------------------------------------------------------
 * | 65  |Mar 15`,2007 |  hmRunning made false in initMeshnetwork		 | Imran  |
 *  -------------------------------------------------------------------------------
 * | 64  |Mar 08,2007  |  additions for saturation info					 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 63  |Feb 27,2007  |  removed unused code							 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 62  |Feb 12,2007  |interface IHealthStatusSubscriber implemented    |Abhijit |
 * --------------------------------------------------------------------------------
 * | 61  |Feb 5, 2007  |MacroAction message added for FIPS               | Abhishek|
 * --------------------------------------------------------------------------------
 * | 60  |Jan 30, 2007 |UpdateRegDomainPacket added (FIPS)          	 |Imran  |
 * --------------------------------------------------------------------------------
 * | 59  |Feb 2, 2007  |updateRecvd flag set to zero in updateAccesPoint | Imran  |
 * --------------------------------------------------------------------------------
 * | 58  |Jan 16, 2007 |Status msg for Update AP problem fixed           |Abhishek|
 * --------------------------------------------------------------------------------
 * | 57  |Dec 14, 2006 |isCapable function name renamed			  		 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 56  |Nov 20, 2006 |Status msg for run macro problem fixed           |Abhishek|
 * --------------------------------------------------------------------------------
 * | 55  |Nov 19, 2006 | Misc changes:Changes due to StatusDisplayUI     |Abhishek|
 * ----------------------------------------------------------------------------------
 * | 54  |Oct 18,2006  | sendRebootPacket mthd splited in two mthds      | Imran  |
 *  -------------------------------------------------------------------------------
 * | 53  |Oct 17, 2006 | Reboot Node dialog changes                      | Imran  |
 * --------------------------------------------------------------------------------
 * | 52  |Oct 06, 2006 | reboot flag set for DCA/Channel change		 	 | Bindu  |
 * --------------------------------------------------------------------------------
 * | 51  |May 19, 2006 | changes for request reboot						 | Mithil |
 * --------------------------------------------------------------------------------
 * | 50  |May 16, 2006 | addn changes for line drawing					 | Mithil |
 * --------------------------------------------------------------------------------
 * | 49  |May 11, 2006 | compare Configuration Added				  	 | Mithil |
 * --------------------------------------------------------------------------------
 * | 48  |Apr 21, 2006 | Cleanup										 | Mithil |
 * --------------------------------------------------------------------------------
 * | 47  |Apr 15, 2006 | Redraw Problems								 | Mithil |
 * --------------------------------------------------------------------------------
 * | 46  |Apr 13, 2006 | Reboot thread not stopped if meshviewer closed  | Mithil |
 * --------------------------------------------------------------------------------
 * | 45  |Apr 10, 2006 | Restore Default problem			 			 | Mithil |
 * --------------------------------------------------------------------------------
 * | 44  |Apr 03, 2006 | assoc Line not drawn			 				 | Mithil |
 * --------------------------------------------------------------------------------
 * | 43  |Mar 28, 2006  | Error message displayed if node rebooted  	 | Mithil |
 * --------------------------------------------------------------------------------
 * | 42  |Mar 27, 2006  | Reboot Checker							  	 | Mithil |
 * --------------------------------------------------------------------------------
 * | 41  |Mar 17, 2006  | Version in msg box generalised			  	 | Mithil |
 * --------------------------------------------------------------------------------
 * | 40  |Mar 16, 2006  |Changes for ACL							  	 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 39  |Feb 28, 2006  | Send reset reboot called from run macro		 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 38  |Feb 28, 2006  | sta request packet sent for all networks	  	 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 37  |Feb 28, 2006  | node not getting dead after reboot			 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 36  |Feb 28, 2006  | implicit rebooting after reboot removed	  	 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 35  |Feb 22, 2006  | isCapable added							  	 |Prachiti|
 *  -------------------------------------------------------------------------------
 * | 34  |Feb 22, 2006  | Dot11eIfInfoPacket sending uncommented		 | Bindu  |
 *  -------------------------------------------------------------------------------
 * | 33  |Feb 22, 2006  | Fix for neighbour lines in reboot ap		  	 |Prachiti|
 *  -------------------------------------------------------------------------------
 * | 32  |Feb 21, 2006  | config sent checking checking changed		  	 | Bindu  |
 *  -------------------------------------------------------------------------------
 * | 31  |Feb 20, 2006  | name change for dot11e if config			  	 | Bindu  |
 *  -------------------------------------------------------------------------------
 * | 30  |Feb 17, 2006  | reboot packet setmesh id					  	 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 29  |Feb 17, 2006  | fixes problems in progress					 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 28  |Feb 17, 2006  | Misc fixes for Dot11e	 					  	 | Bindu  |
 *  -------------------------------------------------------------------------------
 * | 27  |Feb 16, 2006  | Changes for Dot11e	  						 | Bindu  |
 *  -------------------------------------------------------------------------------
 * | 26  |Feb 14, 2006  | Clear Macro Message after reboot			     | Mithil |
 * --------------------------------------------------------------------------------
 * | 25  |Feb 13, 2006  | Hide ESSID Implemented						 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 24  |Feb 03, 2006  | Generic Request		   	 					 | abhijit|
 * --------------------------------------------------------------------------------
 * | 23  |Feb 02, 2006 | STA Information Packet		 					 | Mithil |
 * --------------------------------------------------------------------------------
 * | 22  |Jan 30, 2006 | Restructuring									 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 21  |Jan 27, 2006 | Grp select after starting crashes				 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 20  |Jan 24, 2006 | Restore old boards								 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 19  |Jan 20, 2006 | Group select done								 | Mithil |
 *  -------------------------------------------------------------------------------
 * | 18  |Jan 18, 2006 |SCAN mode persists after stopping viewer         |Mithil  |
 * --------------------------------------------------------------------------------
 * | 17  |Jan 17, 2006 |  Macro Action Log progress corrected for		 | Mithil | 
 * |					  Set/Reset Monitor Mode			     				  |	
 * --------------------------------------------------------------------------------
 * | 16  |Jan 13, 2006 | Macro Action Log progress shows 1/0 for         | Mithil |
 * |					 Set Monitor Mode 					 		 	 |  	  |
 * --------------------------------------------------------------------------------
 * | 15  |Jan 11, 2006 | Cannot makeout from Macro Action Log wether     | Mithil |
 * |					 config is Finished 					 		 |  	  |
 * --------------------------------------------------------------------------------
 * | 14  |Jan 04, 2006 |  System.out.println()'s removed			     | Mithil |
 * --------------------------------------------------------------------------------
 * | 13  |Jan 02, 2006 | Message Box text uniform				         | Mithil |
 * --------------------------------------------------------------------------------
 * | 12  |Dec 30, 2005 |SCAN mode persists after stopping viewer         | Mithil |
 * --------------------------------------------------------------------------------
 * | 11  |Dec 06, 2005 |Monitor mode problem fixed                       | Amit   |
 * --------------------------------------------------------------------------------
 * | 10  |Dec 02, 2005 |ConfigRec flag in NodeConfiguration reseted on   |        |
 * 						Restore default								 	 | Amit   |
 * --------------------------------------------------------------------------------
 * |  9  |Nov 25, 2005 | changes for Set monitor mode			         | Mithil | 
 * -------------------------------------------------------------------------------- 
 * |  8  |Nov 25, 2005 | Added Reg Domain and Ack Timeout changes        | Mithil | 
 * |	 |			   | 1015 and 1027									 |		  | 			      
 * -------------------------------------------------------------------------------- 
 * |  7  |Sep 23, 2005  | Added setMeshNetworkKey method 			     | Sneha  | 
 * --------------------------------------------------------------------------------
 * |  6  |Sep 2, 2005  | Added saveAccessPoints function 			     | Abhijit|
 * --------------------------------------------------------------------------------
 * |  5  |Aug 25, 2005 | Added isDefaultNetwork   					     | Abhijit|
 * --------------------------------------------------------------------------------
 * |  4  |May 15, 2005 | Batch move changes      					     | Anand  |
 * --------------------------------------------------------------------------------
 * |  3  |May 05, 2005 | unwanted imports removed					     | Bindu  |
 * --------------------------------------------------------------------------------
 * |  2  |May 03, 2005 | Bug fixed - Outgoing packet encrypted		     | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.meshdynamics.iperf.IperfListener;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipConfiguration;
import com.meshdynamics.meshviewer.configuration.IStaConfiguration;
import com.meshdynamics.meshviewer.imcppackets.IMCPPacket;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IApFwUpdateListener;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.meshviewer.mesh.healthmonitor.APHealthStatus;
import com.meshdynamics.meshviewer.mesh.healthmonitor.HealthMonitor;
import com.meshdynamics.meshviewer.mesh.healthmonitor.IHealthMonitor;
import com.meshdynamics.meshviewer.mesh.healthmonitor.IHealthStatusSubscriber;
import com.meshdynamics.meshviewer.util.MeshValidations;
import com.meshdynamics.util.MacAddress;

public class MeshNetwork {

    public static final byte		NETWORK_REGULAR			= 0;
    public static final byte		NETWORK_FIPS_ENABLED	= 1;
    
    private String 								meshNetworkId;
    private String 								networkEncKey;
    private byte								networkType;
    private Vector<INetworkListener>			networkListeners;
    private Hashtable<MacAddress, AccessPoint> 	accessPoints;
   
    private IHealthMonitor 						healthMonitor;
    private	boolean 							hmRunning;
    	
    private Vector<String>						vectorMovedNodes;
    private Hashtable<String, AccessPoint> 		hashMacList;
	
    private IAccessPointMoved					apMoveComplete;
    private MeshController 						meshController;
    private HealthMonitorProperties				healthMonitorProperties;
    private HMSubscriber						hmListener;
    
    private class HMSubscriber implements IHealthStatusSubscriber {

		@Override
		public String getName() {
			return meshNetworkId;
		}

		@Override
		public void notifyCurrentHealthStatus(int healthStatus) {
			for(INetworkListener l : networkListeners) {
				l.notifyHealthAlert(healthStatus, meshNetworkId);
			}
		}
    }
    
    /**
     * @param meshId2
     * @param meshNetworkKey
     */
    public MeshNetwork(MeshController meshController, String meshId, String meshNetworkKey, byte networkType, IAccessPointMoved apMoveComplete) {
    	this.meshController		= meshController;
    	this.meshNetworkId		= meshId;
        this.networkEncKey		= meshNetworkKey;
        this.networkType		= networkType;
        this.apMoveComplete		= apMoveComplete;
        
        accessPoints			= new Hashtable<MacAddress, AccessPoint>();
        
        hmListener				= new HMSubscriber();
        healthMonitor			= new HealthMonitor(hmListener);
        hmRunning 				= false;
        healthMonitorProperties	= new HealthMonitorProperties();
        
        vectorMovedNodes		= new Vector<String>();
        networkListeners		= new Vector<INetworkListener>();
    }
 
    /**
     * @return
     */ 
    public String getNwName() {
        return meshNetworkId;
    }
    
    /**
     * @return Returns the networkEncKey.
     */
    public String getKey() {
        return networkEncKey;
    }    

    public void addNetworkListener(INetworkListener listener) {

    	if(networkListeners.contains(listener) == true)
    		return;
    	
    	networkListeners.add(listener);
        if(listener != null) {
        	refreshProperties();
        }
    }

    public void removeNetworkListener(INetworkListener listener) {
    	networkListeners.remove(listener);
    }
    
    /**
     * @return
     */
    public Enumeration<AccessPoint> getAccessPoints() {
        return accessPoints.elements();
    }

    public int getAPCount() {
    	return accessPoints.size();
    }
    
    public int getAliveApCount() {
    	
    	int aliveCount = 0;
    	Enumeration<AccessPoint> e = accessPoints.elements();
    	while(e.hasMoreElements()) {
    		AccessPoint ap = (AccessPoint) e.nextElement();
    		if(ap == null) {
    			continue;
    		}
    		if(ap.isRunning() == true) {
    			aliveCount++;
    		}
    	}
    	
    	return aliveCount;
    }
    
    /**
     * @return
     */
    public AccessPoint getAccessPoint(MacAddress dsMac) {
        return (AccessPoint)accessPoints.get(dsMac);
    }
    
    private void notifyHealthMonitor(AccessPoint ap,int status) {
    	
    	if(hmRunning == false) {
    		return;
    	}
    	
		if(status == AccessPoint.STATUS_DEAD)
			healthMonitor.notifyAPHealthStatus(ap, IHealthMonitor.AP_HEALTH_STATUS_DEAD);
		else
			healthMonitor.notifyAPHealthStatus(ap, IHealthMonitor.AP_HEALTH_STATUS_ALIVE);
    }
    
    /**
     * @param meshNetwork
     * @param dsMacAddress
     * this ap is added ifrom hardware packet and hence is alive
     */
    
    public AccessPoint addAccessPointFromPacket(MacAddress dsMacAddress) {
        
        AccessPoint 	ap 	= getAccessPoint(dsMacAddress);
        
        if(ap == null) {
            ap 		= new AccessPoint(this);
            ap.setDSMacAddress(dsMacAddress);
            accessPoints.put(dsMacAddress, ap);
        }
        
        try {
	        for(INetworkListener l : networkListeners) {
	        	l.accessPointAdded(ap);
	        }
        } catch(Exception e) {
        	System.out.println(e.getMessage());
        }
        notifyHealthMonitor(ap,AccessPoint.STATUS_ALIVE);
        apMoveComplete.accessPointMoveComplete(dsMacAddress.toString());
        return ap;        
    }

    public AccessPoint addAccessPoint(MacAddress dsMacAddress) {
        
        AccessPoint ap = getAccessPoint(dsMacAddress); 
        
        if( ap != null) return ap;
        
        ap 	= new AccessPoint(this);
        ap.setDSMacAddress(dsMacAddress);
        accessPoints.put(dsMacAddress, ap);
        
        for(INetworkListener l : networkListeners) {
        	l.accessPointAdded(ap);
        }
        return ap;        
    }
    
    public AccessPoint getAccessPointFromBssid(MacAddress bssid) {
        Enumeration<AccessPoint> enumeration = accessPoints.elements();
        while(enumeration.hasMoreElements()) {
            AccessPoint ap = (AccessPoint)enumeration.nextElement();
            if(ap.containsAddress(bssid) == true) {
            	return ap;
            }
        }
        return null;
    }  
    
    public String getInterfaceFromBssid(MacAddress bssid) {
    	
    	 Enumeration<AccessPoint> enumeration = accessPoints.elements();
         while(enumeration.hasMoreElements()) {
             AccessPoint ap = (AccessPoint)enumeration.nextElement();
             if(ap.containsAddress(bssid) == true) {
             	IConfiguration			config		= ap.getConfiguration();
             	IInterfaceConfiguration	ifConfig	= config.getInterfaceConfiguration();
				for(int i = 0; i < ifConfig.getInterfaceCount(); i++) {
					IInterfaceInfo ifInfo	= (IInterfaceInfo)ifConfig.getInterfaceByIndex(i);
					if(ifInfo.getMacAddress().equals(bssid)) {
						return ifInfo.getName();
					}
				}             	
             }
         }
         return "";
    }
        
    public void meshViewerStopped() {
    	
        Enumeration<AccessPoint> enumeration =  accessPoints.elements();
        while(enumeration.hasMoreElements()) {
            ((AccessPoint)enumeration.nextElement()).stop();
        }
        
        if(hmRunning == true) {
        	healthMonitor.resetHealthStatus();
        	healthMonitor.stop();
        	for(INetworkListener l : networkListeners) {
        		l.notifyHealthAlert(IHealthMonitor.HEALTH_STATUS_DISABLED, meshNetworkId);
        	}
        }

        for(INetworkListener l : networkListeners) {
        	l.meshViewerStopped();
        }
    }

    public void meshViewerStarted() {

    	Enumeration<AccessPoint> aps = accessPoints.elements();
    	while(aps.hasMoreElements()){
    		AccessPoint 	ap 		= (AccessPoint)aps.nextElement();
    		ap.stop();
            if(hmRunning == true) {
            	notifyHealthMonitor(ap, AccessPoint.STATUS_DEAD);
            }
    	}
    	
    	for(INetworkListener l : networkListeners) {
    		l.meshViewerStarted();
    	}
    	refreshProperties();
    }

    public boolean restoreDefaultSettings(MacAddress macAddress){
    	
    	AccessPoint ap 	= getAccessPoint(macAddress);
		if(ap == null) {
			return false;
		}
			
		ap.restoreDefaultSettings();
		return true;
    }
    
    public void updateAccessPoint(MacAddress dsMacAddress) {

    	AccessPoint ap = getAccessPoint(dsMacAddress);
    	if(ap == null) {
    		return;
    	}
    	
    	ap.updateChangedConfiguration();
 
 	}

    
    /**
     * @param nwkKey
     */
    public void setMeshNetworkKeyToAps(String nwkKey) {
    	
    	/*
    	 * although meshviewer takes the new key immediately
    	 * accesspoint takes the new key only after reboot
    	 * hence meshviewer will not be able to see the ap
    	 * information until it is rebooted.
    	 */
    	
		Enumeration<AccessPoint> apEnum = accessPoints.elements();
		
		while(apEnum.hasMoreElements()) {
			
			AccessPoint ap = (AccessPoint) apEnum.nextElement();
			
			if(ap == null || ap.getStatus() != AccessPoint.STATUS_ALIVE) {
				continue;
			}
			
			ap.setNetworkKey(meshNetworkId, nwkKey);
			
		}
 
    }
  
    public void setMeshNetworkKey(String nwkKey){
        this.networkEncKey = nwkKey;
    }
    
    public void setMeshNetworkKeyToSelectedAp(String apMac, String nwkKey) {
    	
        MacAddress mac = new MacAddress();
        mac.setBytes(apMac);
        AccessPoint ap = getAccessPoint(mac);
		if(ap == null) {
			return; 
		}
		ap.setNetworkKey(meshNetworkId, nwkKey);
    }
    
	/**
	 * @param dsMacAddress
	 */
	public boolean rebootAccesspoint(MacAddress dsMacAddress) {
		AccessPoint ap = getAccessPoint(dsMacAddress);
		if(ap == null) {
			return false;
		}
		ap.reboot();
		notifyHealthMonitor(ap,AccessPoint.STATUS_DEAD);
	   	return true;
	}
	
	public int canRebootAp(MacAddress dsMacAddress) {
		
		AccessPoint ap = getAccessPoint(dsMacAddress);
		if(ap == null) {
			return -1;
		}
	    	
		if(ap.isNodeMoved() == true) {
			
			if(ap.getIMCPChildCount() > 0)
				return 1;

		}
		
		return 0;
	}
	
	public void refreshProperties() {
		
		if(healthMonitorProperties.getStatus() == HealthMonitorProperties.DISABLED) {
			healthMonitor.stop();
			healthMonitor.resetHealthStatus();
			for(INetworkListener l : networkListeners) {
				l.notifyHealthAlert(IHealthMonitor.HEALTH_STATUS_DISABLED, meshNetworkId);
			}
			hmRunning = false;
		} else {
			hmRunning = true;
			healthMonitor.setYellowStatusTimeout(healthMonitorProperties.getYellowAlertTime());
			healthMonitor.setRedStatusTimeout(healthMonitorProperties.getRedAlertTime());
			healthMonitor.start();
		}
		
	}

	public void onClose() {
		if(healthMonitor != null) {
			healthMonitor.resetHealthStatus();
			healthMonitor.stop();
		}
	}
	
	private void accesspointStopped(AccessPoint ap) {
		if(hmRunning == true) {
			healthMonitor.notifyAPHealthStatus(ap, IHealthMonitor.AP_HEALTH_STATUS_DEAD);
		}
	}

	public void accesspointStarted(AccessPoint ap) {
		if(hmRunning == true) {
			healthMonitor.notifyAPHealthStatus(ap, IHealthMonitor.AP_HEALTH_STATUS_ALIVE);
		}
	}
	
	public void resetHealthMonitorStatus() {
		healthMonitor.resetHealthStatus();
		for(INetworkListener l : networkListeners) {
			l.notifyHealthAlert(IHealthMonitor.HEALTH_STATUS_GREEN, meshNetworkId);
		}
	}
	
	public APHealthStatus[] getAPHealthStatus() {
		if(hmRunning == false)
			return null;
		
		return healthMonitor.getAPHealthStatus();
	}

	public int getCurrentHealthStatus() {
		if(hmRunning == true) {
			if(healthMonitor == null) {
				return IHealthMonitor.HEALTH_STATUS_DISABLED;
			}
			int healthStatus = healthMonitor.getCurrentHealthStatus();
			return healthStatus;
		}

		return IHealthMonitor.HEALTH_STATUS_DISABLED;
	}
	
	public void updateDLSaturationInfo(AccessPoint ap, String ifName) {
		for(INetworkListener l : networkListeners) {
			l.saturationInfoReceived(ap, ifName);
		}
	}

	public void processIMCPPacket(final IMCPPacket packet) {
		
		AccessPoint ap = (AccessPoint) accessPoints.get(packet.getDsMacAddress());

		if (ap == null) {
			int type = packet.getPacketType();
//			System.out.println(type);
			if(packet.getPacketType() != PacketFactory.HEARTBEAT) {
				return;
			}
			ap = addAccessPointFromPacket(packet.getDsMacAddress());
		}
		if(ap == null) {
			return;
		}
		ap.processPacket(packet);
	}
	
	public PacketSender getPacketSender() {
		return meshController.getPacketSender();
	}

	public synchronized void checkApLife() {
		
		long 		currentTime 	= System.currentTimeMillis();
		Enumeration<AccessPoint> e 	= accessPoints.elements();
		
		while (e.hasMoreElements()) {

			final AccessPoint accesspoint = (AccessPoint)e.nextElement();
			
			if(	accesspoint.isRunning() == false) {
				continue;
			}
			
			IRuntimeConfiguration 	runtimeConfig	= accesspoint.getRuntimeApConfiguration();
			
			long					lastHbTime		= runtimeConfig.getLastHeartbeatTime();
			
			if(lastHbTime <= 0)
				continue;
			
			int 					hbInterval		= runtimeConfig.getHeartbeatInterval();
    		int 					sleepInterval 	= (hbInterval <= 0) ? 15000 : hbInterval * 2000;
			long 					timeDiff 		= currentTime - lastHbTime;

			/*
			 * we wait until time equivalent to 2 heartbeats has
			 * elapsed since last heartbeat arrived.then we declare
			 * the ap as dead.
			 */
			
			if(timeDiff > (sleepInterval * 1) && timeDiff <= (sleepInterval * 2)) {
				accesspoint.heartbeatMissed();
			} else if(timeDiff > (sleepInterval * 3)) {	//we wait for 3 heartbeats
				accesspoint.stop();
			    accesspointStopped(accesspoint);
			}
		}
	}

	public AccessPoint[] getAccesspointsByName(String matchName, boolean partialMatch) {
		
		Vector<AccessPoint>			apVector 		= new Vector<AccessPoint>();
		String 						lcaseMatchName	= matchName.toLowerCase();
		Enumeration<AccessPoint> 	e 				= accessPoints.elements();
		
		while(e.hasMoreElements()) {
			AccessPoint ap 		= (AccessPoint) e.nextElement();
			if(ap == null) {
				continue;
			}
			
			String 		apName 	= ap.getName().toLowerCase();
			if(partialMatch == true) {
				if(apName.startsWith(lcaseMatchName) == true) {
					apVector.add(ap);
				}
			} else {
				if(apName.equals(lcaseMatchName) == true) {
					apVector.add(ap);
				}
			}
		}
		
		if(apVector.size() <= 0) {
			apVector = null;
			return null;
		}
		
		AccessPoint[] apList = new AccessPoint[apVector.size()];
		for(int i=0;i<apVector.size();i++) {
			apList[i] = (AccessPoint) apVector.get(i);
		}
		
		apVector.removeAllElements();
		apVector = null;
		
		return apList;
	}

	public AccessPoint getStaParent(MacAddress staAddress) {

		Enumeration<AccessPoint> e = accessPoints.elements();
		
		while(e.hasMoreElements()) {
		
			AccessPoint ap = (AccessPoint) e.nextElement();
			if(ap == null) {
				continue;
			}
			IRuntimeConfiguration runtimeConfig	= ap.getRuntimeApConfiguration();
			if(runtimeConfig.hasClient(staAddress) == true) {
				return ap;
			}
		}
		
		return null;
	}

	/**
	 * @param fileName2
	 * @param macAddress
	 */
	public boolean updateApFrmFile(String fileName, MacAddress macAddress) {
		AccessPoint	ap 	= getAccessPoint(macAddress);
		ap.updateConfigurationFromFile(fileName);
		return true;
		
	}

	public boolean moveNode(MacAddress macAddr, String networkName, String networkEncKey, short dstNetworkType) {
		
		AccessPoint	ap 	= getAccessPoint(macAddr);
		if(ap == null) {
			return false;
		}
		IConfigStatusHandler statusHandler	= ap.getUpdateStatusHandler();
		statusHandler.clearStatus();
		
		if(getNetworkType() == MeshNetwork.NETWORK_FIPS_ENABLED &&
				dstNetworkType == MeshNetwork.NETWORK_REGULAR) {
				ap.setFipsMode(false);
			}
		return ap.changeNetwork(networkName,networkEncKey);
	}

	/**
	 * @return Returns the networkType.
	 */
	public byte getNetworkType() {
		return networkType;
	}

	/**
	 * @param apMac
	 * @param ap_configuration_info
	 */
	public boolean updateConfiguration(String apMac, int imcpPacketType) {
		if(MeshValidations.isMacAddress(apMac) == false) {
			return false;
		}
		MacAddress mac	= new MacAddress();
		mac.setBytes(apMac);
		AccessPoint	 ap = getAccessPoint(mac);
		return ap.updateConfiguration(imcpPacketType);
	}

	/**
	 * @param string
	 */
	public void accesPointMoved(String strMacAddr) {
		if(vectorMovedNodes.contains(strMacAddr) == false) {
			vectorMovedNodes.add(strMacAddr);
		}
	}
	
	/**
	 * @param string
	 */
	public void accessPointMoveComplete(String strMacAddr) {

		if(vectorMovedNodes.remove(strMacAddr) == false)
			return;
		
		MacAddress	mac = new MacAddress();
		mac.setBytes(strMacAddr);
		AccessPoint ap	= accessPoints.remove(mac);
		if(ap == null)
			return;
		
        ap.stop();
       	healthMonitor.notifyAPHealthStatus(ap, IHealthMonitor.AP_HEALTH_STATUS_ALIVE);
       	ap.dispose();
    	for(INetworkListener l : networkListeners) {
    		l.accessPointDeleted(ap);
    	}

	}

	/**
	 * @param controller
	 */
	public void setParent(IAccessPointMoved apMoveComplete) {
		this.apMoveComplete = apMoveComplete;
		
	}

	/**
	 * @param macEnum
	 */
	public void reboot(Enumeration<String> macEnum) {
		
		Enumeration<String> macList	= initializeRebootList(macEnum);
		
		while(macList.hasMoreElements()) {
			
			String 		strMac 		= (String) macList.nextElement();
			AccessPoint	ap 			= (AccessPoint) hashMacList.get(strMac);
			if(ap == null) {
				continue;
			}
			if(ap.isRoot() == true) {
				
				/**
				 * If root 
				 * 1st chk for staCnt 
				 * if no stas present reboot ap.
				 * else check for stalist and then reboot.
				 */
				IRuntimeConfiguration	runtimeConfiguration	= ap.getRuntimeApConfiguration();
				if(runtimeConfiguration == null) {
					continue;
				}
				int staCnt	= runtimeConfiguration.getStaCount();
				if(staCnt == 0) {
					ap.reboot();
					hashMacList.remove(ap.getDSMacAddress());
				}else {
					Enumeration<IStaConfiguration> enumSTAList	= runtimeConfiguration.getStaList();
					chkStaList(enumSTAList);
					ap.reboot();
					hashMacList.remove(ap.getDSMacAddress());				
				}
			}else {
				/**
				 * If relay check parent ap present in main hash
				 * if not present directly reboot.
				 */
				AccessPoint	parentAp = ap.getParentAp();
				if(parentAp == null) {
					continue;
				}
				if(hashMacList.containsKey(parentAp.getDSMacAddress()) == false) {
					ap.reboot();
					hashMacList.remove(ap.getDSMacAddress());	
				}
			}
		}
	}

	/**
	 * Check for stas recursively and rebooting the node having 0 stas
	 */
	private void chkStaList(Enumeration<IStaConfiguration> enumSTAList) {
		
		while(enumSTAList.hasMoreElements()) {
			
			IStaConfiguration  staConf 		= (IStaConfiguration) enumSTAList.nextElement();
			if(staConf	== null) {
				continue;
			}
			MacAddress	staMac	= staConf.getStaMacAddress();
			AccessPoint	staAp	= (AccessPoint) hashMacList.get(staMac.toString());
			if(staAp == null) {
				continue;
			}
			IRuntimeConfiguration	runtimeConfiguration	= staAp.getRuntimeApConfiguration();
			if(runtimeConfiguration == null) {
				continue;
			}
			int staCnt	= runtimeConfiguration.getStaCount();
			if(staCnt == 0) {				
				staAp.reboot();
				hashMacList.remove(staAp.getDSMacAddress());
				
			}else {
				chkStaList(runtimeConfiguration.getStaList());
				staAp.reboot();
			}
		}		
	}

	/**
	 * @param macEnum
	 */
	private Enumeration<String> initializeRebootList(Enumeration<String> macEnum) {
		
		hashMacList							= new Hashtable<String, AccessPoint>(); // Main hash contains all the aps which we want to reboot
		Hashtable<String, String>  macList	= new Hashtable<String, String>(); // local hash 
		
		while(macEnum.hasMoreElements()) {
			
			String 		strMac 		= (String) macEnum.nextElement();
			MacAddress	macAddress	= new MacAddress();
			macAddress.setBytes(strMac);
			AccessPoint	ap			= getAccessPoint(macAddress);
			if(ap == null) {
				continue;
			}
			hashMacList.put(strMac, ap);
			macList.put(strMac, strMac);
		}
		return macList.elements(); 
	}

	
	public boolean isApRunning(String dsAddress) {
		Enumeration<AccessPoint> e = accessPoints.elements();
		while(e.hasMoreElements()) {
			AccessPoint ap = (AccessPoint)e.nextElement();
			if(dsAddress.equals(ap.getDSMacAddress()) == true) {
				return ap.isRunning();
			}
		}
		return false;
	}

	public void importSipConfiguration(MacAddress macAddr, ISipConfiguration sipConfig) {

		AccessPoint	ap = getAccessPoint(macAddr);
		if(ap == null) {
			return;
		}

		ap.importSipConfiguration(sipConfig);
	}

	public HealthMonitorProperties getHealthMonitorProperties() {
		return healthMonitorProperties;
	}

	public int updateFirmware(MacAddress dsMacAddress, String fileName, IApFwUpdateListener listener) {
		
		AccessPoint accessPoint = accessPoints.get(dsMacAddress);
		if(accessPoint == null)
			return Mesh.FW_UPDATE_RETURN_ERROR_AP_NOT_FOUND;
		
		if(accessPoint.isLocal() == true)
			return accessPoint.updateFirmware(fileName, listener);
		
		return meshController.updateRemoteAp(accessPoint, fileName);
	}

	void remoteFwUpdateResponseRcvd(MacAddress dsMac, int ret, String statusText) {
		AccessPoint ap = accessPoints.get(dsMac);
		if(ap == null)
			return;
		
		ap.remoteFwUpdateResponseRcvd(ret, statusText);
		
	}

	public String executeCommand(MacAddress dsMacAddress, String command) {
		
		AccessPoint accessPoint = accessPoints.get(dsMacAddress);
		if(accessPoint == null)
			return "AccessPoint not found.";
		
		if(accessPoint.isLocal() == true)
			return accessPoint.executeCommand(command, null);
		
		return meshController.executeRemoteCommand(accessPoint, command);
	}

	public void remoteCommandResponseRcvd(MacAddress dsMac, String command, String output) {
		AccessPoint ap = accessPoints.get(dsMac);
		if(ap == null)
			return;
	
		ap.remoteCommandResponseRcvd(command, output);
	}

	public void remoteFwUpdateFileUpdateRcvd(MacAddress dsMac, int fileNo, int fileCnt) {
		AccessPoint ap = accessPoints.get(dsMac);
		if(ap == null)
			return;
	
		ap.remoteFwUpdateFileUpdateRcvd(fileNo, fileCnt);
	}

	public String executePerformanceTest(MacAddress dsMacAddress, int recordCount, short type, short protocol, int udpBandWidth, IperfListener listener) {
		AccessPoint accessPoint = accessPoints.get(dsMacAddress);
		if(accessPoint == null)
			return "AccessPoint not found.";
		
		if(accessPoint.isLocal() == true)
			return accessPoint.executePerformanceTest(recordCount, type, protocol, udpBandWidth, listener);
		
		return meshController.executeRemoteIperf(accessPoint, recordCount, type, protocol, udpBandWidth, listener);
	}
		
		
}

