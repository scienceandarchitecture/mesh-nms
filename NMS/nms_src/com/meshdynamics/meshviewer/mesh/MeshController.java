/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshController.java
 * Comments : 
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * |  9  |Sep 06, 2007 | Restructuring									 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  8  |Jul 30, 2007 | interface IAccessPointMoved implemented         | Imran  |
 * --------------------------------------------------------------------------------
 * |  7  |Mar 21,2007  | Viewer starting logic changed					 |Abhijit |
 *---------------------------------------------------------------------------------
 * |  6  |Feb 27,2007  |  removed unused code							 |Abhijit |
 *---------------------------------------------------------------------------------
 * |  5  |Nov 27, 2006 | clean-up, SMG changes		  			         |  Bindu |
 * --------------------------------------------------------------------------------
 * |  4  |Aug 09,2006  | SMG Connection changed to TCP					 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  3  |Jan 02, 2006 | Message Box text uniform				         | Mithil |
 * --------------------------------------------------------------------------------
 * |  2  |Aug 29, 2005 | Duplicate Network Name not allowed fixed        |Prachiti|
 * --------------------------------------------------------------------------------
 * |  1  |May 12, 2005 | removeNetwork method added                      | Amit   |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import com.meshdynamics.iperf.IperfListener;
import com.meshdynamics.meshviewer.MeshViewer;
import com.meshdynamics.meshviewer.imcp.IMCPEngine;
import com.meshdynamics.meshviewer.imcppackets.IMCPPacket;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.imcppackets.SipPrivatePacket;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IApFwUpdateListener;
import com.meshdynamics.meshviewer.sip.ISipEventListener;
import com.meshdynamics.meshviewer.sip.SipPrivatePacketHandler;
import com.meshdynamics.mg.MGClient;
import com.meshdynamics.mg.MGClient.ImcpListener;
import com.meshdynamics.util.Bytes;
import com.meshdynamics.util.MacAddress;

public class MeshController implements IAccessPointMoved, ImcpListener {
    
    public static final int MESHNETWORKADDED		= 1;
    public static final int IMCP_UDP_PORT 		= 0xDEDE;
    public static final int SMGP_UDP_PORT		= 0xAEAE;
    
    
    private Hashtable<String, MeshNetwork> 			meshNetworks;
    private IMCPEngine								imcpEngine;
    private APLifeChecker 							apLifeChecker;
    private Thread									apLifeCheckerThread;
    private PacketSender							imcpPacketSender;
    private SipPrivatePacketHandler					sipHandler;
	private MGClient                        		mgClient;
	private boolean									mgClientRunning;
	private int 									mgClientOperatingMode;
    private int 									mgClientCommandId;
	private Hashtable<String, RemoteCommandData>	mgSentCommands;
	
	private static final int MG_COMMAND_TYPE_FW_UPDATE		= 1;
	private static final int MG_COMMAND_TYPE_MESHCOMMAND	= 2;
	private static final int MG_COMMAND_TYPE_IPERF			= 3;
	
	private class RemoteCommandData {
		public int 			commandId;
		public int 			type;
		public MacAddress	dsMac;
		
		RemoteCommandData(int commandId, int type, byte[] dsMacBytes) {
			this.dsMac 		= new MacAddress(dsMacBytes);
			this.commandId 	= commandId;
			this.type		= type;
		}
	}

	private class RemoteFwUpdateData extends RemoteCommandData {
		RemoteFwUpdateData(int commandId, byte[] dsMacBytes) {
			super(commandId, MG_COMMAND_TYPE_FW_UPDATE, dsMacBytes);
		}
	}
	
	private class RemoteMeshCommandData extends RemoteCommandData {
		public String command;
		
		RemoteMeshCommandData(int commandId, byte[] dsMacBytes, String command) {
			super(commandId, MG_COMMAND_TYPE_MESHCOMMAND, dsMacBytes);
			this.command = command;
		}
	}
	
	private class RemoteIperfData extends RemoteCommandData {
		public byte[]			commandData;
		public IperfListener	listener;
		
		RemoteIperfData(int commandId, byte[] dsMacBytes, byte[] commandData, IperfListener listener) {
			super(commandId, MG_COMMAND_TYPE_IPERF, dsMacBytes);
			this.commandData 	= commandData;
			this.listener		= listener;
		}
	}
	
    /**
     * 
     */
    public MeshController(MeshViewer meshViewer) {
        meshNetworks 		= new Hashtable<String, MeshNetwork>();
        imcpEngine			= new IMCPEngine(this);
        imcpPacketSender	= new PacketSender(this,PacketSender.PACKET_SEND_MODE_IMCP);
        sipHandler			= new SipPrivatePacketHandler();
        mgClient            = null;
        mgClientCommandId	= 1;
        mgSentCommands		= new Hashtable<String, RemoteCommandData>();
    }

    public int startMGClient(int       mode,
                             String    server,
                             int       port,
                             boolean   useSSL,
                             String    userName,
                             String    password,
                             boolean   ignoreLocalPackets) {
    	
        if(mgClient != null) {
            mgClient.stop();
        }
        
        mgClientOperatingMode 	= mode;
        mgClientRunning 		= true;
        mgClient 				= new MGClient(mode, server, port, useSSL, userName, password);
        
        mgClient.start();
        mgClient.addImcpListener(this);
        imcpEngine.setIgnoreLocalPackets(ignoreLocalPackets);
        return 0;
    }


    public int stopMGClient() {
        if(mgClient != null) {
            mgClient.stop();
        }
        
        mgClient 				= null;
        mgClientRunning 		= false;
        mgClientOperatingMode	= -1;
        
        imcpEngine.setIgnoreLocalPackets(false);
        
        return 0;
    }

    /**
     * @param meshId
     * @param meshNetworkKey
     */
    private MeshNetwork addMeshNetwork(String meshId, String meshNetworkKey, byte networkType) {
        
    	MeshNetwork network = (MeshNetwork) meshNetworks.get(meshId); 
        if (network == null){
        	network = new MeshNetwork(this, meshId, meshNetworkKey, networkType, this);
        	meshNetworks.put(meshId, network);
            return network;
        }
        
		return network;
    }

    public boolean startViewer() {
        Enumeration<MeshNetwork> networks = meshNetworks.elements();
        while(networks.hasMoreElements()){
        	MeshNetwork net = (MeshNetwork)networks.nextElement();
        	net.meshViewerStarted();
        }
    	
        apLifeChecker			= new APLifeChecker(this);
        apLifeCheckerThread 	= new Thread(apLifeChecker);
        apLifeCheckerThread.start();
        return imcpEngine.start();
    }

    public boolean stopViewer() {
        imcpEngine.stop();
        apLifeChecker.stop();
        apLifeCheckerThread.interrupt();
        
        Enumeration<MeshNetwork> enm = meshNetworks.elements();
        while(enm.hasMoreElements()) {
            enm.nextElement().meshViewerStopped();
        }
        return true;
    }

    /**
     * @param selectedNetworkId
     * @return
     */
    public MeshNetwork getMeshNetwork(String meshNetworkId) {
        return meshNetworks.get(meshNetworkId);
    }

    /**
     * @return
     */
    public Enumeration<MeshNetwork> getMeshNetworks() {
        return meshNetworks.elements();
    }

    public int getMeshNetworksCount(){
    	return meshNetworks.size(); 
    }
    
    public void removeNetwork(String networkName){
    	if(meshNetworks.size()!=0)
    		meshNetworks.remove(networkName);
    }

	/**
	 * @return Returns the packetSender.
	 * used for sending imcp packets
	 */
	public PacketSender getPacketSender() {
		return imcpPacketSender;
	}

	public boolean initialize() {
		if(meshNetworks.containsKey(Mesh.DEFAULT_MESH_ID) == false) {
			createMeshNetwork(Mesh.DEFAULT_MESH_ID, Mesh.DEFAULT_MESH_ENC_KEY, MeshNetwork.NETWORK_REGULAR);
		}
	    return true;
	}
	
	public MeshNetwork createMeshNetwork(String networkName, String networkKey, byte networkType) {
		
		MeshNetwork meshNetwork = addMeshNetwork(networkName, networkKey, networkType);
		
		if(meshNetwork == null) {
			return null;
		}
		return meshNetwork;
		
	}
	
	public boolean unInitialize() {
		stopMGClient();
	    return true;
	}

	/**
	 * @return
	 * 
	 */
	public boolean closeMeshNetwork(String networkName) {

		if(networkName.equalsIgnoreCase(Mesh.DEFAULT_MESH_ID) == true){
			return false;
		}
		MeshNetwork network = (MeshNetwork) meshNetworks.remove(networkName);
		
		if(network != null) {
			network.onClose();
		}
		
		return true;

	}

	public void processIMCPPacket(IMCPPacket packet,boolean encrypted) {

		if(packet.getPacketType() == PacketFactory.SIP_PRIVATE_PACKET)
			sipHandler.processPacket((SipPrivatePacket) packet);
		
		MeshNetwork	meshNetwork = (MeshNetwork) meshNetworks.get(packet.getMeshId());
		if(meshNetwork == null)
			return;

		meshNetwork.processIMCPPacket(packet);
		
		if(mgClientRunning == true && mgClientOperatingMode == MGClient.MODE_FORWARDER) {
			byte[] packetData = packet.getRawPacket();
			mgClient.sendImcpPacket(packet.getDsMacAddress().getBytes(), packetData);
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.IAccessPointMoved#accessPointMoveComplete(java.lang.String)
	 */
	public void accessPointMoveComplete(String strMacAddr) {
		String strNetworkName;
		MeshNetwork	network;
		Enumeration<String>	keys	= meshNetworks.keys();
		while(keys.hasMoreElements()) {
			strNetworkName	= (String)keys.nextElement();
			network			= (MeshNetwork)meshNetworks.get(strNetworkName); 
			network.accessPointMoveComplete(strMacAddr);
		}
	}

	public void addSipEventListener(ISipEventListener listener) {
		sipHandler.addListener(listener);
	}

	public void removeSipEventListener(ISipEventListener listener) {
		sipHandler.removeListener(listener);
	}

	public void forwardPacket(IMCPPacket imcpPacket) {
		if(mgClientRunning == false)
			return;
		
		byte[] packetData = imcpPacket.getRawPacket();
		mgClient.sendImcpPacket(imcpPacket.getDsMacAddress().getBytes(), packetData);
	}

	@Override
	public void commandReceive(int id, byte[] dsMacAddress, int type,
			byte[] data, long srcAuthToken) {

		switch(type) {
			case MG_COMMAND_TYPE_FW_UPDATE:
				processFwUpdateRequest(id, dsMacAddress, data, srcAuthToken);
				break;
			case MG_COMMAND_TYPE_MESHCOMMAND:
				processMeshCommandRequest(id, dsMacAddress, data, srcAuthToken);
				break;
			case MG_COMMAND_TYPE_IPERF:
				processIperfRequest(id, dsMacAddress, data, srcAuthToken);
				break;
		}

	}

	private MeshNetwork getNetworkForAp(MacAddress dsMac) {
	
		Enumeration<MeshNetwork> nwEnum = meshNetworks.elements();
		while(nwEnum.hasMoreElements()) {
			
			MeshNetwork network = nwEnum.nextElement();
			if(network.getAccessPoint(dsMac) != null)
				return network;
		}		
		
		return null;
	}

	private void sendUpdateResponse(int ret, String status, int id, byte[] dsMacAddress, long srcAuthToken) {
		byte[] 		responseData 	= new byte[4 + 4 + status.length()];
		
		Bytes.intToBytes(ret, responseData);
		Bytes.intToBytes(status.length(), responseData, 4);
		System.arraycopy(status.getBytes(), 0, responseData, 8, status.length());
		mgClient.sendCommandResponse(id, dsMacAddress, srcAuthToken, responseData);
	}

	private void sendMeshCommandResponse(String response, int id, byte[] dsMacAddress, long srcAuthToken) {
		mgClient.sendCommandResponse(id, dsMacAddress, srcAuthToken, response.getBytes());
	}
	
	private void processIperfRequest(final int id, final byte[] dsMacAddress, 
			byte[] data, final long srcAuthToken) {

		final MacAddress 	dsMac 	= new MacAddress(dsMacAddress);
		final MeshNetwork 	network = getNetworkForAp(dsMac);
		
		if(network == null) {
			sendMeshCommandResponse("AccessPoint not found.", id, dsMacAddress, srcAuthToken);
			return;
		}
		
		AccessPoint ap = network.getAccessPoint(dsMac);
		if(ap == null) {
			sendMeshCommandResponse("AccessPoint not found.", id, dsMacAddress, srcAuthToken);
			return;
		}
		
		int position = 0;
		
		final int 	recordCount 	= Bytes.bytesToInt(data, position);			position += 4;
		final short type 			= Bytes.bytesToShort(data, position);		position += 2;
		final short	protocol		= Bytes.bytesToShort(data, position);		position += 2;
		final int 	udpBandWidth	= Bytes.bytesToInt(data, position);			position += 4;
		
		Thread t = new Thread(new Runnable(){

			@Override
			public void run() {
				String result = network.executePerformanceTest(dsMac, recordCount, type, protocol, udpBandWidth, null);
				sendMeshCommandResponse(result, id, dsMacAddress, srcAuthToken);
			}
		});
		t.start();

	}
	
	private void processMeshCommandRequest(int id, byte[] dsMacAddress, 
			byte[] data, long srcAuthToken) {

		MacAddress 	dsMac 			= new MacAddress(dsMacAddress);
		MeshNetwork network 		= getNetworkForAp(dsMac);
		
		if(network == null) {
			sendMeshCommandResponse("AccessPoint not found.", id, dsMacAddress, srcAuthToken);
			return;
		}
		
		AccessPoint ap = network.getAccessPoint(dsMac);
		if(ap == null) {
			sendMeshCommandResponse("AccessPoint not found.", id, dsMacAddress, srcAuthToken);
			return;
		}
		
		String command 	= new String(data);
		String output 	= network.executeCommand(dsMac, command);
		sendMeshCommandResponse(output, id, dsMacAddress, srcAuthToken);
	}
	
	private void processFwUpdateRequest(final int id, final byte[] dsMacAddress, 
			byte[] data, final long srcAuthToken) {

		MacAddress 	dsMac 			= new MacAddress(dsMacAddress);
		MeshNetwork network 		= getNetworkForAp(dsMac);
		
		if(network == null) {
			sendUpdateResponse(Mesh.FW_UPDATE_RETURN_ERROR_AP_NOT_FOUND, "Accesspoint was not found", id, dsMacAddress, srcAuthToken);
			return;
		}
		
		AccessPoint ap = network.getAccessPoint(dsMac);
		if(ap == null) {
			sendUpdateResponse(Mesh.FW_UPDATE_RETURN_ERROR_AP_NOT_FOUND, "Accesspoint was not found", id, dsMacAddress, srcAuthToken);
			return;
		}
		
		String fileName = dsMac.toString().replace(':', '_');
		fileName = "firmware_" + fileName + ".zip";
		
		try {
			FileOutputStream fileOut = new FileOutputStream(fileName);
			fileOut.write(data);
			fileOut.close();
			File f 	= new File(fileName);
			System.out.println("fireware absolutePath : "+f.getAbsolutePath());
			int ret = network.updateFirmware(ap.getDsMacAddress(), f.getAbsolutePath(), 
					new IApFwUpdateListener() {

						@Override
						public void notifyApFileUpdate(int currentFileNo, int totalFileCount) {
							sendUpdateResponse(Mesh.REMOTE_FW_UPDATE_FILE_STATUS, ""+currentFileNo+"/"+totalFileCount, id, dsMacAddress, srcAuthToken);
						}
		
						@Override
						public void notifyUpdateStatus(int ret, String status) {
							sendUpdateResponse(ret, status, id, dsMacAddress, srcAuthToken);
						}
					});
			
			if(ret > 0)
				sendUpdateResponse(ret, "Successful", id, dsMacAddress, srcAuthToken);
			else
				sendUpdateResponse(ret, "Failed", id, dsMacAddress, srcAuthToken);
			return;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			sendUpdateResponse(Mesh.FW_UPDATE_RETURN_FAILED, "Failed", id, dsMacAddress, srcAuthToken);
		}
	}

	@Override
	public void commandResponseReceive(int id, byte[] dsMacAddress, byte[] data) {
		
		RemoteCommandData sentData = mgSentCommands.remove(""+id);
		if(sentData == null) {
			return;
		}
		
		MacAddress dsMac = new MacAddress(dsMacAddress);
		
		if(sentData.dsMac.equals(dsMac) == false) {
			return;
		}

		MeshNetwork network = getNetworkForAp(dsMac);
		if(network == null)
			return;
		
		if(sentData.type == MG_COMMAND_TYPE_FW_UPDATE) {
			
			int 	ret 	= Bytes.bytesToInt(data);
			int 	len 	= Bytes.bytesToInt(data, 4);
			String 	status 	= new String(data, 8, len);
			
			if(ret == Mesh.REMOTE_FW_UPDATE_FILE_STATUS) {
				String tokens[] = status.split("/");
				try {
					int fileNo 	= Integer.parseInt(tokens[0]);
					int fileCnt	= Integer.parseInt(tokens[1]);
					network.remoteFwUpdateFileUpdateRcvd(dsMac, fileNo, fileCnt);
				} catch(Exception e) {
				}
			} else {
				network.remoteFwUpdateResponseRcvd(dsMac, ret, status);
			}
			
			if(ret == Mesh.FW_UPDATE_RETURN_SUCCESS_UPDATE_STATUS ||
				ret == Mesh.REMOTE_FW_UPDATE_FILE_STATUS)
				mgSentCommands.put(""+id, sentData);
			
		} else if(sentData.type == MG_COMMAND_TYPE_MESHCOMMAND) {

			RemoteMeshCommandData mcData = (RemoteMeshCommandData)sentData;
			String 		output 	= new String(data);
			network.remoteCommandResponseRcvd(dsMac, mcData.command, output);
			
		} else if(sentData.type == MG_COMMAND_TYPE_IPERF) {
			
			RemoteIperfData iperfData = (RemoteIperfData)sentData;
			String 		output 	= new String(data);
			if(iperfData.listener != null)
				iperfData.listener.testResult(output);
			
		}

		
	}

	@Override
	public void imcpReceive(byte[] packet) {
		
		if(mgClientOperatingMode == MGClient.MODE_FORWARDER) {
			imcpPacketSender.sendRawPacket(packet);
		} else if(mgClientOperatingMode == MGClient.MODE_REMOTE_MANAGER) {
			imcpEngine.injectPacket(packet);
		}
		
	}

	public boolean isMgClientRunning() {
		return mgClientRunning;
	}

	protected int updateRemoteAp(AccessPoint ap, String fileName) {

		int commandId = mgClientCommandId++;
		try {
			FileInputStream fileIn 		= new FileInputStream(fileName);
			int 			totalBytes	= fileIn.available();
			byte[] 			commandData = new byte[totalBytes];
			int 			offset		= 0;

			while(offset < totalBytes) {
				offset += fileIn.read(commandData, offset, (totalBytes-offset));
			}
			
			byte[] dsMacBytes = ap.getDsMacAddress().getBytes();
			if(mgClient.sendCommand(commandId, dsMacBytes, MG_COMMAND_TYPE_FW_UPDATE, commandData) == false)
				return Mesh.FW_UPDATE_RETURN_FAILED;
	
			mgSentCommands.put(""+commandId, new RemoteFwUpdateData(commandId, dsMacBytes));
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			mgSentCommands.remove(""+commandId);
			return Mesh.FW_UPDATE_RETURN_FAILED;
		}
		
		return Mesh.FW_UPDATE_RETURN_SUCCESS_INIT_REMOTE_UPDATE;
	}

	public String executeRemoteCommand(AccessPoint ap, String command) {
		int commandId = mgClientCommandId++;
		try {
			byte[] dsMacBytes = ap.getDsMacAddress().getBytes();
			if(mgClient.sendCommand(commandId, dsMacBytes, MG_COMMAND_TYPE_MESHCOMMAND, command.getBytes()) == false)
				return "Command execution failed.";
	
			mgSentCommands.put(""+commandId, new RemoteMeshCommandData(commandId, dsMacBytes, command));
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			mgSentCommands.remove(""+commandId);
			return "Command execution failed.";
		}
		
		return "Executing command remotely.";
	}

	public String executeRemoteIperf(AccessPoint ap, int recordCount, short type, short protocol, int udpBandWidth, IperfListener listener) {
		int commandId = mgClientCommandId++;
		try {
			byte[] 	dsMacBytes 		= ap.getDsMacAddress().getBytes();
			byte[] 	commandData 	= new byte[4 + 2 + 2 + 4];
			int 	position		= 0;
			
			Bytes.intToBytes(recordCount, commandData, position);	position += 4;
			Bytes.shortToBytes(type, commandData, position);		position += 2;
			Bytes.shortToBytes(type, commandData, position);		position += 2;
			Bytes.intToBytes(recordCount, commandData, position);	position += 4;
			
			if(mgClient.sendCommand(commandId, dsMacBytes, MG_COMMAND_TYPE_IPERF, commandData) == false)
				return "Command execution failed.";
	
			mgSentCommands.put(""+commandId, new RemoteIperfData(commandId, dsMacBytes, commandData, listener));
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			mgSentCommands.remove(""+commandId);
			return "Command execution failed.";
		}
		
		return "Executing command remotely.";
	}

}

