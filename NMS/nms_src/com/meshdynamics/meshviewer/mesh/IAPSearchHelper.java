/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : IAPSearchHelper.java
 * Comments : 
 * Created  : Apr 30, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Apr 30, 2007 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh;

import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.util.MacAddress;

public interface IAPSearchHelper {
	
	public AccessPoint[] 	getAccesspointsByName	(String matchName,boolean partialMatch);
	public AccessPoint		getAccessPointFromBssid	(MacAddress bssid);
	public void 			selectAccesspoint		(AccessPoint ap);
	public AccessPoint		getStaParent			(MacAddress staAddress);
	
}
