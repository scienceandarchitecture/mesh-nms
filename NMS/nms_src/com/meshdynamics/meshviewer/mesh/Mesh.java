/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Mesh.java
 * Comments : Main Type defination class
 * Created  : Sep 30, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No   |Date         |  Comment                                       | Author  |
 *   --------------------------------------------------------------------------------
 * | 54   |Mar 15, 2007	| updateHeartbeatStatusSelection	implemented  |Abhishek |
 * ----------------------------------------------------------------------------------	  
 * | 53	  |Mar 15, 2007	| static variable ENABLED added                   |Imran    |
 *  --------------------------------------------------------------------------------
 * | 52	  |Mar 13, 2007	| Public safety subtype changes                  |Abhishek      |
 * ----------------------------------------------------------------------------------
 * | 51   | Mar 06,2007 |  RDF_MAX, RDF_MAX_NOT removed                  |Abhishek |
 * --------------------------------------------------------------------------------
 * | 50   |Jan 18, 2007| Changes for Fips 			  			         |Abhishek |
 * ----------------------------------------------------------------------------------
 * | 49   |Feb 02,2007 | DFS set is added, reset removed  				| Abhishek  |
 * --------------------------------------------------------------------------------
 * | 48   |Jan 22,2007 | DFS set,reset is added       		   			| Abhishek  |
 * --------------------------------------------------------------------------------
 * | 47   |Jan 15,2007 | Voltage, Temperature is added       		    | Abhishek  |
 * --------------------------------------------------------------------------------
 * | 46   |Jan 15,2007 | Flags are added                     		    | Abhishek  |
 * --------------------------------------------------------------------------------
 * | 45   |Jan 12, 2006 |  default values of RF Editor tab added		 | Imran   |
 * --------------------------------------------------------------------------------
 * | 44   |Jan 12, 2007 |  convertBytesToString method added             | Imran   |
 * --------------------------------------------------------------------------------
 * | 43   |Jan 12, 2007 |  convertTo16Bytes method added                 | Imran   |
 * --------------------------------------------------------------------------------
 * | 42   |Dec 13, 2006 | Custom Code 			 added             	     |Prachiti |
 * --------------------------------------------------------------------------------
 * | 41   |Nov 24, 2006 | Final variables for RDF added                 | Imran   |
 * --------------------------------------------------------------------------------
 * | 40   |Nov 07, 2006 | Changes for APPropertyUI   					 | Abhishek|
 * --------------------------------------------------------------------------------
 * | 39   |Oct 06, 2006 | Misc Fixes									 | Bindu   |
 * --------------------------------------------------------------------------------
 * | 38   |Apr 13, 2006 | Changes for Firmware Update  			         | Bindu   |
 * --------------------------------------------------------------------------------
 * | 37   |Mar 29, 2006 | Sta info constants added					 	 | Mithil  |
 * --------------------------------------------------------------------------------
 * | 36	  |Mar 16, 2006 |Changes for ACL							  	 | Mithil  |
 *  -------------------------------------------------------------------------------
 * | 35   |Mar 03, 2006 | import vlan template changes				     | Mithil  |
 *  -------------------------------------------------------------------------------
 * | 34   |Feb 20, 2006 | rmvd hb message not used						 | Mithil  |
 *  -------------------------------------------------------------------------------
 * | 33   |Feb 17, 2006 | Hidden ESSID not displayed					 | Mithil  |
 *  -------------------------------------------------------------------------------
 * | 32   |Feb 15, 2006 | All Flags Removed Rboot Reqd put in general    | Mithil  |
 *  -------------------------------------------------------------------------------
 * | 31   |Feb 13, 2006 | Hidden ESSID Implemented						 | Mithil  |
 *  -------------------------------------------------------------------------------
 * | 30   |Feb 13, 2006 | Reject Clients removed						 | Mithil  |
 *  -------------------------------------------------------------------------------
 * | 29   |Feb 08, 2006 | Reg Domain removed							 | Mithil  |
 *  -------------------------------------------------------------------------------
 * | 28  |Feb 03, 2006 | DIsplay in Property UI		 					 | Mithil |
 * --------------------------------------------------------------------------------
 * | 27  | Feb 03,2006 | Added two more macro actions		     		 | Mithil |
 * --------------------------------------------------------------------------------
 * | 26  | Dec 22,2005 | Build No Auto Increment process done     		 | Mithil |
 * --------------------------------------------------------------------------------
 * | 25  | Dec 21,2005 | Build No Incremented to 1 from 0.0		         | Mithil |
 * -------------------------------------------------------------------------------- 
 * | 24  | Nov 25,2005 | Ack Timeout items added 1027			         | Mithil |
 * -------------------------------------------------------------------------------- 
 * | 23  | Nov 15,2005 | Reg Domain and Country code items added  1015   | Amit   |
 * --------------------------------------------------------------------------------
 * | 22  | Nov 08,2005 | Change for displaying hb's for selected network | Mithil |
 * --------------------------------------------------------------------------------
 * | 21  | Oct 21,2005 | Changed method for adding heartbeat2 packet info| Mithil |
 * -------------------------------------------------------------------------------- 
 * | 20  | Oct 20,2005 | Added method for adding heartbeat2 packet info  | Mithil |
 * --------------------------------------------------------------------------------
 * | 19  | Oct 14,2005 | Calls to StatusDisplay UI for Name and Model    | Amit   |
 * --------------------------------------------------------------------------------
 * | 18  | Oct 11,2005 | Calls to Clear Station Activity done			 | Amit   |
 * --------------------------------------------------------------------------------
 * |  17 | Oct 11,2005 | Interface Service Types Added                   | Bindu  |
 * --------------------------------------------------------------------------------
 * |  16 | Oct 7,2005  | Vlan properties removed                         | Amit   |
 * --------------------------------------------------------------------------------
 * |  15 |Sep 21, 2005 | AddGroupSelectionMessage added     			 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  14 |Sep 21, 2005 | GetDSMacadress added		 					 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  13 |Sep 21, 2005 | HW_INFO properties modified 					 |Abhijit |
 * --------------------------------------------------------------------------------
 * |  12 |May 30, 2005 | TX_RATE added to VLAN properties       		 | Anand  |
 * --------------------------------------------------------------------------------
 * |  11 |May 10, 2005 | Model Info added                                | Anand  |
 * --------------------------------------------------------------------------------
 * |  10 |May 06, 2005 | Added Latitude & Longitude 	 		         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  9  |Apr 20, 2005 | HEARTBEAT_PROP_TEMPERATURE added                | Bindu  |
 * --------------------------------------------------------------------------------
 * |  8  |Mar 25, 2005 | Images moved to branding native dll.            | Anand  |
 * --------------------------------------------------------------------------------
 * |  7  |Mar 25, 2005 | Temperature Property added to Stats             | Anand  |
 * --------------------------------------------------------------------------------
 * |  6  |Mar 07, 2005 | DEF_DOT11E_ENABLED Added		                 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  5  |Mar 07, 2005 | Changed dot11i to dot11e		                 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  4  |Mar 04, 2005 | Vlan Individual Props added                 	 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  3  |Mar 01, 2005 | Vlan & Interface Sec Info added                 | Anand  |
 * --------------------------------------------------------------------------------
 * |  2  |Feb 11, 2005 | Change Detection-PropertyUI changes             | Anand  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 02, 2005 | Changes for Customized MeshViewer               | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 30, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh;


public class Mesh {
	
	public static final int 	PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ = 7;
	public static final int 	PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ = 6;
	public static final int 	PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ 	= 5;
	public static final int 	PHY_SUB_TYPE_802_11_B_G					= 4;
	public static final int 	PHY_SUB_TYPE_802_11_G					= 3;
	public static final int 	PHY_SUB_TYPE_802_11_B					= 2;
	public static final int 	PHY_SUB_TYPE_802_11_A					= 1;
	public static final int 	PHY_SUB_TYPE_802_11_24GHz_N			    = 15;
	public static final int 	PHY_SUB_TYPE_802_11_5GHz_N			    = 16;
	public static final int 	PHY_SUB_TYPE_802_11_AC					= 11;
	public static final int 	PHY_SUB_TYPE_802_11_BGN					= 12;
	public static final int 	PHY_SUB_TYPE_802_11_AN					= 13;
	public static final int 	PHY_SUB_TYPE_802_11_ANAC				= 14;
	public static final int 	PHY_SUB_TYPE_INGNORE					= 0;
	public static final int[]   subTypesFor5GHZ                         = {1,16,11,13,14};
	public static final int[]   subTypesFor24GHZ                        = {2,15,3,4,12};
	public static final int     channelList_40_Above                    = 0;
	public static final int     channelList_40_Below                    = 1;
	public static final int     channel_80MHz                           = 3;
	public static final int     mediamChannel                           = 6;
	public static final short   _40plus_ch[]                            = {36, 44, 149, 157};
    public static final short	_40minus_ch[]                           = {40, 48, 153,161};
    public static final short	_manual_ch[]                            = {36,40,44,48,52,56,60,64,149,153,157,161};/*{40, 48, 56, 64, 153,161}; by nagaraju */	
    public static final short	_manual_ch_24[]                         = {1,6,11};
    public static final short	_80_ch[]                                = {48,157};
    
	public static final int     radioCount_4000                         = 4;
	public static final int     radioCount_6000                         = 6;
	
	public static final int 	PHY_TYPE_802_3				= 0;
	public static final int 	PHY_TYPE_802_11				= 1;
	public static final int     PHY_TYPE_802_11_VIRTUAL     = 2;
	public static final int     length_passphrase           = 10;
	public static final short	PHY_USAGE_TYPE_DS			= 0;
	public static final short	PHY_USAGE_TYPE_WM			= 1;
	public static final short	PHY_USAGE_TYPE_PMON			= 2;
	public static final short	PHY_USAGE_TYPE_AMON			= 3;
	
	public static final short	IF_SERVICE_ALL				= 0;
	public static final short	IF_SERVICE_BACKHAUL_ONLY	= 1;
	public static final short	IF_SERVICE_CLIENT_ONLY		= 2;
			
   public static final String DEFAULT_MESH_ID 				= "default";
    public static final String DEFAULT_MESH_ENC_KEY 		= "cybernetics!";
     
    public static final int HARDWARE_TYPE_H200				= 1;
    public static final int HARDWARE_TYPE_H300				= 2;
    public static final int HARDWARE_TYPE_H300s				= 3;
	
    public static final int PROP_TOTAL_COUNT				= 15;
    public static final int PROP_TREES_COUNT				= 11;
    
    public static final int PROP_GENERAL  					= 0;
    public static final int PROP_RUNTIME_FLAG				= 1;
    public static final int PROP_OPTIONS					= 2;
    public static final int PROP_HEARTBEAT					= 3;
    public static final int PROP_BOARD_INFO					= 4;
    public static final int PROP_APCONF   					= 5;
    public static final int PROP_MESHCONF					= 6;
    public static final int PROP_IPCONF   					= 7;
    public static final int PROP_HWINFO   					= 8;
    public static final int PROP_STATS						= 9;
    public static final int PROP_VLANCONF					= 10;
    
    
    //General
    public static final int GENERAL_PROP_COUNT				= 5;

    public static final int GENERAL_PROP_NAME				= 0;
    public static final int GENERAL_PROP_DESC				= 1;
    public static final int GENERAL_PROP_LATITUDE			= 2;
    public static final int GENERAL_PROP_LONGITUDE			= 3;
    public static final int GENERAL_PROP_COUNTRY_CODE		= 4;
    
    //Mesh Flags
    public static final int RUNTIMEFLAG_PROP_COUNT			= 3;
    
    public static final int OPTIONS_PROP_COUNT				= 5;
    
    
    public static final int FLAG_PROP_REBOOT_REQUIRED		= 0;
    public static final int FLAG_PROP_RADAR_DETECT			= 1;
    public static final int FLAG_PROP_LOCATION				= 2;
    
    public static final int OPTIONS_PROP_FIPS_ENABLED		= 0;
    public static final int OPTIONS_PROP_IGMP_SNOOPING		= 1;
    public static final int OPTIONS_PROP_DHCP_ENABLED		= 2;
    public static final int OPTIONS_PROP_P3M				= 3;
    public static final int OPTIONS_PROP_PBV				= 4;
   
   
    //Mesh HeartBeat Runtime
    public static final int HEARTBEAT_PROP_COUNT			= 8;

    public static final int HEARTBEAT_PROP_SQNR				= 0;
    public static final int HEARTBEAT_PROP_PARENT_NODE		= 1;
    public static final int HEARTBEAT_PROP_SIGNALSTRNTH		= 2;
    public static final int HEARTBEAT_PROP_KNOWNAPS			= 3;
    public static final int HEARTBEAT_PROP_IMCP_CHILD		= 4;
    public static final int HEARTBEAT_PROP_TXRATE			= 5;
    public static final int HEARTBEAT_PROP_HPC 				= 6;
    public static final int HEARTBEAT_PROP_CLIENT			= 7;
    
    //Board information
    public static final int BOARD_PROP_COUNT				= 4;
    
    public static final int BOARD_PROP_TEMPERATURE			= 0;
    public static final int BOARD_PROP_VOLTAGE				= 1;
    public static final int BOARD_PROP_CPU_USAGE			= 2;
    public static final int BOARD_PROP_FREE_MEMORY			= 3;

    //AP COnfig
    public static final int APCONF_PROP_COUNT				= 2;

    public static final int APCONF_PROP_IF_SEC				= 0;
    public static final int APCONF_PROP_WM_INFO				= 1;
    
    public static final int APCONF_PROP_WM_COUNT			= 5;
    
    public static final int APCONF_PROP_WM_ESSID			= 0;
    public static final int APCONF_PROP_WM_RTS_THR	   		= 1;	
    public static final int APCONF_PROP_WM_FRAG_THR			= 2;				
    public static final int APCONF_PROP_WM_BEACON_INT		= 3;
    public static final int APCONF_PROP_WM_ACK_TIMEOUT		= 4;
    public static final int APCONF_PROP_WM_HIDE_ESSID		= 5;
    
    public static final int SEC_PROP_COUNT					= 4;
    public static final int SEC_PROP_IF_NONE_SEC			= 0;
    public static final int SEC_PROP_IF_WEP_SEC				= 1;
    public static final int SEC_PROP_IF_RSNPSK_SEC			= 2;
    public static final int SEC_PROP_IF_RAD_SEC				= 3;
    
    //Mesh_Config

    public static final int MESHCONF_PROP_COUNT		   		= 2;
    public static final int MESHCONF_PROP_HBI				= 0;
    public static final int MESHCONF_PREFFERED_PARENT		= 1;

    //IP Config
    public static final int IPCONF_PROP_COUNT				= 4;
    public static final int IPCONF_PROP_HOSTNAME			= 0;
    public static final int IPCONF_PROP_IPADDR				= 1;
    public static final int IPCONF_PROP_NETMASK		   		= 2;
    public static final int IPCONF_PROP_GATEWAY		   		= 3;
    
    //HWINFO
    public static final int HWINFO_PROP_COUNT				= 4;
    public static final int HWINFO_PROP_LINKS				= 0;
    public static final int HWINFO_PROP_MODELINFO			= 1;
    public static final int HWINFO_PROP_FW_VERSION			= 2;
    public static final int HWINFO_UNIT_ADDRESS				= 3;
    
    //Stats properties
    public static final int STATS_PROP_COUNT				= 5;
    public static final int STATS_PROP_BYTESENT		   		= 0;
    public static final int STATS_PROP_BYTERECEIVED	   		= 1;
    public static final int STATS_PROP_PACKETSENT			= 2;
    public static final int STATS_PROP_PACKETRECEIVED		= 3;
    public static final int STATS_PROP_ROUTECHANGED	   		= 4;
    public static final int STATS_PROP_TEMPERATURE			= 5;

    public static final int VLANCONF_PROP_COUNT					= 1;//5
    public static final int VLANCONF_PROP_DEF_VLAN_COUNT		= 4;//5
    public static final int VLANCONF_PROP_VLANS					= 0;
    
    public static final int VLANINFO_PROP_COUNT					= 5;
    public static final int VLANINFO_PROP_VLAN_NAME				= 0;
    public static final int VLANINFO_PROP_TAG 					= 0;
    public static final int VLANINFO_PROP_DOT11E_ENABLED 		= 1;
    public static final int VLANINFO_PROP_DOT11E 				= 2;
    public static final int VLANINFO_PROP_DOT1P 				= 3;
    public static final int VLANINFO_PROP_VLAN_ESSID 			= 4;

	public static final int AP_HEADER_SETTINGS                          = 8;
	
	public static final int AP_PROPERTY_ESSID							= 0;	
	public static final int AP_PROPERTY_RTS_THR							= 1;	
	public static final int AP_PROPERTY_FRAG_THR						= 2;				
	public static final int AP_PROPERTY_BEACON_INT						= 3;
	public static final int AP_PROPERTY_STA								= 4;
	
	public static final String MACRO_MOVE_NODES							= "Move Nodes";
	public static final String MACRO_EDIT_NETWORK						= "Edit Network";
	public static final String MACRO_REBOOT_NODES						= "Reboot Nodes";
	public static final String MACRO_RESTORE_FACTORY					= "Restore Default Settings";
	public static final String MACRO_UPDATE_NODE_SETTINGS				= "Update Node Settings";
	public static final String MACRO_CONFIGURATION_SENT					= "Configuration Sent";
	public static final String MACRO_CONFIGURATION_RECEIVED				= "Configuration Received";
	public static final String MACRO_GROUP_SELECTION					= "Group Selection";
	public static final String MACRO_CTM_REQUEST						= "Continuous Transmit Mode Request Sent";
	public static final String MACRO_DENY_CLIENTS_REQUEST				= "Reject Clients Request Sent";
	public static final String MACRO_FIRMWARE_UPDATE					= "Firmware Update";
	public static final String MACRO_FIRMWARE_IMAGE_VERIFY              = "Firmware Image Verify";
	public static final String MACRO_FIRMWARE_DOWNLOAD                  = "Firmware Image Download";
	public static final String FIPS_NETWORK_KEY_TOOLTIP					= "Network Key should contain only \" Hexadecimal \" chararacters. i.e. 0,1,...,9 and A,B..,F";
	public static final String MACRO_FIRMWARE                           = "FIRMWARE UPDATE";
	public static final String MACRO_IMPORT_UPDATE                      = "Import Configuration";
	public static final String MACRO_NODE_RIGISTER                      = "Device MAC verification";
	public static final String MACRO_MGMT_GW                            = "Management Gateway Status";
	
	public static final int ACCESS_POINT_STA_AUTH_STATE_NONE					=	0; 
	public static final int ACCESS_POINT_STA_AUTH_STATE_802_1X_INITIATED		=	1;// 802.1x authentication initiated 
	public static final int ACCESS_POINT_STA_AUTH_STATE_802_1X_AUTHENTICATED	=	2;// 802.1x authentication complete 
	public static final int ACCESS_POINT_STA_AUTH_STATE_ENCRYPTION				=	4; // Encryption enabled for the STA 
	public static final int ACCESS_POINT_STA_AUTH_STATE_RSN						=	8;// STA is a RSN STA
	public static final int ACCESS_POINT_STA_AUTH_STATE_ADHOC					=	64;// STA is a RSN STA
	
	public static final int AL_802_11_CAPABILITY_ESS							=0;
	public static final int AL_802_11_CAPABILITY_IBSS							=1;
	public static final int AL_802_11_CAPABILITY_CF_POLLABLE					=2;
	public static final int AL_802_11_CAPABILITY_CF_POLL_REQUEST				=3;
	public static final int AL_802_11_CAPABILITY_PRIVACY						=4;
	public static final int AL_802_11_CAPABILITY_SHORT_PREAMBLE					=5;
	public static final int AL_802_11_CAPABILITY_PBCC							=6;
	public static final int AL_802_11_CAPABILITY_CHANNEL_AGILITY				=7;
	public static final int AL_802_11_CAPABILITY_SHORT_SLOT_TIME				=10;
	public static final int AL_802_11_CAPABILITY_DSSS_OFDM						=13;
	
	public static final int REG_DOMN_CODE_NONE									=0;
	public static final int REG_DOMN_CODE_FCC									=1;
	public static final int REG_DOMN_CODE_ETSI									=2;
	public static final int REG_DOMN_CODE_CUSTOM								=3;
	public static final	int	CUSTOM_COUNTRY_CODE									= 1;
	public static final	int	DEFAULT_COUNTRY_CODE								= 0;
	
	public static final short 	CHANNEL_BANDWIDTH_5_MHZ							= 5;
	public static final short 	CHANNEL_BANDWIDTH_10_MHZ						= 10;
	public static final short 	CHANNEL_BANDWIDTH_20_MHZ						= 20;
	public static final short 	CHANNEL_BANDWIDTH_40_MHZ						= 40;
	public static final short 	DEFAULT_CTL										= 0;
	public static final short 	DEFAULT_ANTENNA_POWER							= 30;
	public static final short 	DEFAULT_ANTENNA_GAIN							= 0;
	public static final int 	DEFAULT_CHANNEL_COUNT							= 0;
	
	public static final int DFS_BIT_SET 										= 0x100; 
	public static final int FIPS_BIT_SET 										= 0x200; 
	
	public static final int 	FIPS_ENABLED									= 1;
	public static final int 	FIPS_VALID_NW_KEY_SIZE							= 16;
	
	public static final int 	ENABLED											= 1;
	public static final short   DISABLED										= 0;
	
	public 	static final String SELECTED							= "Selected";
	public 	static final String UNSELECTED							= " ";
	public 	static final String GROUP_SELECT_OFF					= " ";

	public 	static final String MESH_TYPE_HYBRID_STR				= "Hybrid";
	public 	static final String MESH_TYPE_INFRA_STR					= "Infrastructure ";
	public  static final String FIRMWARE_UPGRADE                    = "Firmware Upgrade Started";
	
	public 	static final String STATUS_SUCCESSFUL	 				= "Successful";
	public 	static final String STATUS_FAILED						= "Failed";
	public  static final String STATUS_PREPARE                      = "Firmware File preparing";
	public static  final String STATUS_DOWNLOAD                     = "Firmware File Downloading";
	public 	static final String STATUS_INVALID_TEMPLATE				= "Invalid template";
	public 	static final String STATUS_TEMPLATE_NOT_FOUND			= "Template not found";
	public 	static final String STATUS_TEMPLATE_SAMEAS_CONF			= "Template is same as Node Configuration";
	
	public 	static final String STATUS_FAILED_INCORRECT_FILE		= "Failed - Model mismatch.";
	public 	static final String STATUS_FAILED_APNOTFOUND			= "Failed - Accesspoint not found.";
	public 	static final String STATUS_FAILED_APNOTRUNNING			= "Failed - Accesspoint is not running.";
	public 	static final String STATUS_MOVE_REBOOTREQD 				= "Move complete, Reboot required";
	public 	static final String STATUS_REBOOTREQD 					= "Reboot required";
	public 	static final String STATUS_REBOOTING 					= "Rebooting";
	public 	static final String STATUS_CONFIG_SENT					= "Configuration Sent";
	public 	static final String STATUS_CONFIG_NOT_SENT				= "Configuration Not Sent";
	public 	static final String STATUS_FAILED_FIRMWAREREQ 			= "Firmware request failed.";
	public 	static final String STATUS_ERROR_FIRMWARE_OPEN 			= "Error opening firmware file.";
	public 	static final String STATUS_FAILED_CONNECTION			= "Connection failed.";
	public 	static final String STATUS_ERROR_CHECKSUM 				= "Checksum error.";
	public 	static final String STATUS_SUCCESSFUL_FIRMWAREUPDATE 	= " : Firmware updation successful.";
	public 	static final String STATUS_UPDATE_AFT_REBOOT 			= "Update reflected after reboot.";
	public 	static final String STATUS_FIRMWARE_NOTFOUND 			= "Firmware file not found.";
	public 	static final String STATUS_FIRMWARE_UPDATE_NOTSUPPORTED = "Firmware update not supported.";
	public 	static final String STATUS_CANCELLED 					= "Cancelled.";
	public 	static final String STATUS_MOVING_TO_NETWORK			= "Moving to network ''";
	public  static final String STATUS_CONFIG_SENT_ALL 				= "All configuration sent";
	
	public static final String STATUS_NODE_REGISTER                 = "Device Register";
	public static final String STATUS_NODE_NOT_REGISTER             = "Device Not Register";
	public static final String STATUS_FIRMWARE_UPGRADING            = "firmware upgrading..";
	public static final String STATUS_FIRMWARE_STARTING             = "firmware upgrading starting..";
	public static final String STATUS_FIRMWARE_IMAGE_COPY_FAILED    = "image copy failed";
	public static final String STATUS_FIRMWARE_UPGRADE_FAILED       = "failed";
	
	public static final String STATUS_NODE_FIRMWARE                 = "";
	
	
	public static final int RETURN_STATUS_SUCCESS					= 0;
	public static final int RETURN_STATUS_ERROR						= 1;
	
	public static final int FW_UPDATE_RETURN_FAILED 						= 1;
	public static final int FW_UPDATE_RETURN_ERROR_AP_NOT_FOUND 			= -1;	
	public static final int FW_UPDATE_RETURN_ERROR_FW_FILE_OPEN 			= -2;	
	public static final int FW_UPDATE_RETURN_ERROR_CONNECTION 				= -3;
	public static final int FW_UPDATE_RETURN_ERROR_FW_REQ_FAILED			= -4;
	public static final int FW_UPDATE_RETURN_ERROR_NEED_CALLBACK			= -5;
	public static final int FW_UPDATE_RETURN_ERROR_ALREADY_IN_PROGRESS		= -6;
	
	public static final int FW_UPDATE_RETURN_SUCCESS 						= 0;
	public static final int FW_UPDATE_RETURN_SUCCESS_INIT_REMOTE_UPDATE		= 2;
	public static final int FW_UPDATE_RETURN_SUCCESS_UPDATE_STATUS			= 3;
	
	public static final int FW_BUILD_IMAGE_REQUEST                          = 1;
	public static final int FW_BUILD_IMAGE_RESPONSE                         = 2;

	public static final int GPS_UPDATE_RETURN_SUCCESS 						= 1;
	public static final int GPS_UPDATE_RETURN_FAILED 						= 2;
	public static final int GPS_UPDATE_RETURN_FAILED_NEED_REMOTE_LISTENER	= 3;
	public static final int GPS_UPDATE_RETURN_INIT_REMOTE_UPDATE			= 4;
	
	/*
	 * Followng return values are for remote update
	 */
	public static final int REMOTE_FW_UPDATE_FILE_STATUS					= 5;
	
	
	public static enum PsuedoModels {
		MD_FIPS ("MD4000-FIPS");
		
		public final String model;
		
		PsuedoModels(String model) {
			this.model = model;
		}
	}
	
	
	public static void logException(final Exception e) {
/*		
		Display.getDefault().asyncExec (new Runnable () { 
	        public void run () {
	        	try {
	        		logger.logErrorMessage(e.getMessage());
		    		StackTraceElement[] sta = e.getStackTrace();
		    		for(int i=0; i < sta.length; i++) {
		    			logger.logDebugMessage(sta[i].toString());
		    		}
	        	} catch (Exception e) {
	        	}
	        }
	     });
*/		
	}

	/**
	 * @param accessPoint
	 */

	/*
	 * this method converts hex srting to byte array. Used for networks dialog
	 */
	public static byte[] convertTo16Bytes(String nwkKey) {
    	byte[] convertedKey = new byte[nwkKey.length()/2]; // nwkKey length is always 32
 		String[] brokenKey = new String[nwkKey.length()/2];
 		int pos = 0;
 		for(int i=0; i<nwkKey.length(); i++){
 			brokenKey[pos] = nwkKey.substring(i, i+2);
 			convertedKey[pos] = (byte) Integer.parseInt( brokenKey[pos].trim(), 16 /* radix */ );
 			i++;
 			pos++;
 		}	
		return convertedKey;
	}
	
	/*
	 * this method converts byte array into hex string. Used for networks dialog
	 */ 
	public static String convertBytesToString(byte[] decodedKey){
		 char[] hexChar = {
			   '0' , '1' , '2' , '3' ,
			   '4' , '5' , '6' , '7' ,
			   '8' , '9' , 'a' , 'b' ,
			   'c' , 'd' , 'e' , 'f'};
		
		StringBuffer keyBuff = new StringBuffer(decodedKey.length * 2);
		for(int i = 0 ; i< decodedKey.length ; i++){
			  // look up high nibble char
			keyBuff.append( hexChar [( decodedKey[i] & 0xf0 ) >>> 4] );

		      // look up low nibble char
			keyBuff.append( hexChar [decodedKey[i] & 0x0f] );
		}
		return keyBuff.toString();
	}

}