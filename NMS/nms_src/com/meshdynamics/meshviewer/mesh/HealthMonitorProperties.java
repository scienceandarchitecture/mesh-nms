package com.meshdynamics.meshviewer.mesh;

import java.io.InputStream;
import java.io.OutputStream;

import com.meshdynamics.util.Bytes;

public class HealthMonitorProperties {
	
	public static final int ENABLED 	= 1;
	public static final int DISABLED 	= 0;
	
	public 	int	status;
	private int yellowAlertTime;
	private int redAlertTime;
	private final int DEFAULT_YELLOW_ALERT_TIME 	= 120; 
	private final int DEFAULT_RED_ALERT_TIME 		= 300; 

	public HealthMonitorProperties(){
		yellowAlertTime	= DEFAULT_YELLOW_ALERT_TIME;
		redAlertTime	= DEFAULT_RED_ALERT_TIME;
		status			= DISABLED;
	}

	public void read(InputStream in){
		byte[] fourBytes = new byte[4];
		try {
			in.read(fourBytes);
			yellowAlertTime  = Bytes.bytesToInt(fourBytes);
			
			in.read(fourBytes);
			redAlertTime	 = Bytes.bytesToInt(fourBytes);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void write(OutputStream os){
		byte[] fourBytes    = new byte[4];
		try{
			Bytes.intToBytes(yellowAlertTime,fourBytes);
	    	os.write(fourBytes);
			
	    	Bytes.intToBytes(redAlertTime,fourBytes);
	    	os.write(fourBytes);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public int getYellowAlertTime() {
		return yellowAlertTime;
	}

	public void setYellowAlertTime(int yellowAlertTime) {
		this.yellowAlertTime = yellowAlertTime;
	}

	public int getRedAlertTime() {
		return redAlertTime;
	}

	public void setRedAlertTime(int redAlertTime) {
		this.redAlertTime = redAlertTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}
