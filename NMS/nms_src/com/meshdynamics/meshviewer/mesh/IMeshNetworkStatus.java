package com.meshdynamics.meshviewer.mesh;

public interface IMeshNetworkStatus {

	public String 	getNetworkName		();

	public long 	getUpTime			();
	
	public int 		getActiveApCount	();
	public int 		getTotalApCount		();
	
	public long 	getMinSignal		();
	public long 	getMaxSignal		();
	
	public long 	getMinBitRate		();
	public long 	getMaxBitRate		();
}
