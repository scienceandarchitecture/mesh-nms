/**
 * MeshDynamics 
 * -------------- 
 * File     : IHealthStatusSubscriber.java
 * Comments : 
 * Created  : Feb 12,2007
 * Author   : Abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Feb 12, 2007   | Created									  | Abhijit      |
 * ----------------------------------------------------------------------------------
****************************************************************************************/

package com.meshdynamics.meshviewer.mesh.healthmonitor;

public interface IHealthStatusSubscriber {

	public String	getName();
	public void 	notifyCurrentHealthStatus(int healthStatus);
	
}
