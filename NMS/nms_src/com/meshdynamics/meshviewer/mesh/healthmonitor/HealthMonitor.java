/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : HealthMonitor.java
 * Comments : 
 * Created  : Feb 08, 2004
 * Author   : Abhishek
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                       | Author         |
 * ---------------------------------------------------------------------------------------
 * |  1  |Feb 13, 2007 | IHealthMonitor implemented                     | Abhijit        |
 * ---------------------------------------------------------------------------------------
 * |  0  |Feb 08, 2007 | Created                                        | Abhishek/Imran |
 * ---------------------------------------------------------------------------------------
 ****************************************************************************************/


package com.meshdynamics.meshviewer.mesh.healthmonitor;

import java.util.Enumeration;
import java.util.Hashtable;

import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.util.MacAddress;

/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class HealthMonitor implements Runnable,IHealthMonitor {

	private	final int 		TWO_MINUTES		=	120;
	private final int		FIVE_MINUTES	=	300;
	
	private final int 		MAX_TICK_COUNT	= 480 * 60;
	
	boolean 	start;
	private		Hashtable<String, APHealthStatus>	healthObjHash;
	private 	int									currentHealthStatus;
	private 	IHealthStatusSubscriber				subscriber;
	private     HealthStatusFSMData					yellowAlertData;
	private     HealthStatusFSMData					redAlertData;
	private 	HealthStatusFSMData[]				healthFSM;
	private 	Thread								monitorThread;
	
	private class HealthStatusFSMData {
	
		public int 		healthStatus;
		public int	 	healthTimeOut;
		
	}
	
	public HealthMonitor(IHealthStatusSubscriber subscriber) {
		super();
		start 			 	= false;
		monitorThread		= null;
		this.subscriber 	= subscriber;
		initialize();
	}
	
	private void initHealthFSM() {
		healthFSM					= new HealthStatusFSMData[2];
		healthFSM[0]				= new HealthStatusFSMData();
		healthFSM[0].healthStatus	= IHealthMonitor.HEALTH_STATUS_YELLOW;
		healthFSM[0].healthTimeOut	= TWO_MINUTES;
		healthFSM[1]				= new HealthStatusFSMData();
		healthFSM[1].healthStatus	= IHealthMonitor.HEALTH_STATUS_RED;
		healthFSM[1].healthTimeOut	= FIVE_MINUTES;
	}
	
	private void initialize() {
		
		healthObjHash 		=	new	Hashtable<String, APHealthStatus>();
		currentHealthStatus	= 	IHealthMonitor.HEALTH_STATUS_GREEN;
		initHealthFSM();
		yellowAlertData		=	healthFSM[0];
		redAlertData 		=	healthFSM[1];
		
	}

	private void notifySubscriber() {
	
		if(subscriber != null) {
			subscriber.notifyCurrentHealthStatus(currentHealthStatus);
		}
		
	}
	
	public void run() {
	
		int	sleepInterval 	= 1000;
		start 				= true;
		currentHealthStatus = IHealthMonitor.HEALTH_STATUS_GREEN;
		
		notifySubscriber();
		
		while(start){
			
			try {
				Thread.sleep(sleepInterval); // sleep for 1 sec
			}catch (InterruptedException e){
				if(start == false) {
					System.out.println("Health monitor stopped for " + subscriber.getName());
					break;
				}
			}
			 
			if(healthObjHash.isEmpty() == true){
				
				int previousHealthStatus 	= currentHealthStatus; 
				currentHealthStatus 		= IHealthMonitor.HEALTH_STATUS_GREEN;
				
				if(previousHealthStatus != IHealthMonitor.HEALTH_STATUS_GREEN)
					notifySubscriber();
				
				continue;
			}
			
			Enumeration<APHealthStatus> e = healthObjHash.elements();
			while(e.hasMoreElements()) {
			
				APHealthStatus apHealthStat = (APHealthStatus) e.nextElement();
				
				if(apHealthStat == null)
					continue;

				apHealthStat.incrementTickCount();
				int tickCount 		= apHealthStat.getTickCount();
				int healthFSMIndex 	= apHealthStat.getHealthFSMIndex();
				
				if(tickCount > healthFSM[healthFSMIndex].healthTimeOut) {
					
					if(healthFSM[healthFSMIndex].healthStatus > currentHealthStatus)
						currentHealthStatus = healthFSM[healthFSMIndex].healthStatus;
					
					notifySubscriber();
					//now check for next state
					healthFSMIndex++;
					healthFSMIndex = (healthFSMIndex >= healthFSM.length) ? (healthFSM.length - 1) : healthFSMIndex;
					apHealthStat.setHealthFSMIndex(healthFSMIndex);
					apHealthStat.resetTickCount();
					
				}
					
				/*
				 * we overflow only in red state
				 */
				if(tickCount >= MAX_TICK_COUNT) {
					if(healthFSM[healthFSMIndex].healthStatus == IHealthMonitor.HEALTH_STATUS_RED) {
						apHealthStat.resetTickCount();
					}
				}
					 
			}
		}
		
		monitorThread = null;
	}

	public void notifyAPHealthStatus(AccessPoint ap, int healthStatus) {
		
		if(ap == null)
			return;
		MacAddress 	macAddress 		= ap.getDsMacAddress();
		String 		strMAcAddress	= macAddress.toString();
		
		if(healthStatus == IHealthMonitor.AP_HEALTH_STATUS_DEAD) {
			
			if(healthObjHash.containsKey(strMAcAddress) == false) {
				
				APHealthStatus apHealthStat = new APHealthStatus();
				apHealthStat.setAccessPoint(ap);
				apHealthStat.setDeathNotifyTime(System.currentTimeMillis());
				healthObjHash.put(strMAcAddress, apHealthStat);
				start();
				
			}
			
		} else {
			healthObjHash.remove(strMAcAddress);
		}
		
	}
	public void resetHealthStatus() {

		healthObjHash.clear();
		currentHealthStatus = IHealthMonitor.HEALTH_STATUS_GREEN;
	}


	public void setRedStatusTimeout(int timeInSeconds) {
		this.redAlertData.healthTimeOut  = timeInSeconds;
	}


	public void setSubscriber(IHealthStatusSubscriber subscriber) {
		this.subscriber = subscriber;
	}

	public void setYellowStatusTimeout(int timeInSeconds) {
		this.yellowAlertData.healthTimeOut	= timeInSeconds;
	}

	public void start() {
		start = true;
		if(monitorThread == null) {
			monitorThread	= new Thread(this);
			monitorThread.start();
		} else if(monitorThread.isAlive() == false) {
			monitorThread	= new Thread(this);
			monitorThread.start();
		}
	}

	public void stop() {
		start = false;
		currentHealthStatus = IHealthMonitor.HEALTH_STATUS_DISABLED;
		if(monitorThread != null)
			monitorThread.interrupt();
	}

	public APHealthStatus[] getAPHealthStatus() {
		
		int apCount = healthObjHash.size();
		
		if(apCount <= 0)
			return null;
		
		APHealthStatus 				apStatus[] 	= new APHealthStatus[apCount];
		Enumeration<APHealthStatus>	apEnum		= healthObjHash.elements();
		
		apCount = 0;
		while(apEnum.hasMoreElements()) {
			apStatus[apCount++] = (APHealthStatus) apEnum.nextElement();
		}
		
		return apStatus;
	}

	public int getCurrentHealthStatus() {
		return currentHealthStatus;
	}

}
