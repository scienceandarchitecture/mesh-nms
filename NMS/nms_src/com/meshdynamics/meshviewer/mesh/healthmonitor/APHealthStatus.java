/**
 * MeshDynamics 
 * -------------- 
 * File     : APHealthStatus.java
 * Comments : 
 * Created  : Feb 8, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0   | Feb 08, 2007  | Created									  | Abhishek     |
 * ----------------------------------------------------------------------------------
****************************************************************************************/
package com.meshdynamics.meshviewer.mesh.healthmonitor;

import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;


public class APHealthStatus {

	private	AccessPoint			accessPoint;
	private int					tickCount;
	private long				deathNotifyTime;
	private int 				healthFSMIndex;
	
	public APHealthStatus(){
		
		accessPoint		=	null;
		tickCount 		=	0;
		deathNotifyTime	= 	0;
		healthFSMIndex	= 	0;
	}
	
	/**
	 * @return Returns the accessPoint.
	 */
	public AccessPoint getAccessPoint() {
		return accessPoint;
	}
	
	/**
	 * @return Returns the tickCount.
	 */
	public int getTickCount() {
		return tickCount;
	}
	
	/**
	 * @param tickCount The tickCount to set.
	 */
	public void resetTickCount() {
		this.tickCount = 0;
	}
	
	public void incrementTickCount() {
		this.tickCount++;
	}
	
	/**
	 * @param accessPoint The accessPoint to set.
	 */
	public void setAccessPoint(AccessPoint accessPoint) {
		this.accessPoint = accessPoint;
	}

	public long getDeathNotifyTime() {
		return deathNotifyTime;
	}
	
	/**
	 * @param lastHBTime The lastHBTime to set.
	 */
	public void setDeathNotifyTime(long deathNotifyTime) {
		this.deathNotifyTime = deathNotifyTime;
	}

	public int getHealthFSMIndex() {
		return healthFSMIndex;
	}

	public void setHealthFSMIndex(int healthFSMIndex) {
		this.healthFSMIndex = healthFSMIndex;
	}
}
