/**
 * MeshDynamics 
 * -------------- 
 * File     : IHealthMonitor.java
 * Comments : 
 * Created  : Feb 12,2007
 * Author   : Abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Feb 12, 2007   | Created									  | Abhijit      |
 * ----------------------------------------------------------------------------------
****************************************************************************************/

package com.meshdynamics.meshviewer.mesh.healthmonitor;

import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;

public interface IHealthMonitor {

	public static final int HEALTH_STATUS_DISABLED	= 0;
	public static final int HEALTH_STATUS_GREEN		= 1;
	public static final int HEALTH_STATUS_YELLOW	= 2;
	public static final int HEALTH_STATUS_RED		= 3;
	
	public static final int AP_HEALTH_STATUS_ALIVE	= 1;
	public static final int AP_HEALTH_STATUS_DEAD	= 0;
	
	public void 			setSubscriber			(IHealthStatusSubscriber subscriber);
	public void 			notifyAPHealthStatus	(AccessPoint ap,int healthStatus);
	public void 			resetHealthStatus		();
	
	public void 			setYellowStatusTimeout	(int timeInSeconds);
	public void 			setRedStatusTimeout		(int timeInSeconds);
		
	public void 			start					();
	public void 			stop					();
	
	public APHealthStatus[]	getAPHealthStatus		();
	public int				getCurrentHealthStatus	();
}
