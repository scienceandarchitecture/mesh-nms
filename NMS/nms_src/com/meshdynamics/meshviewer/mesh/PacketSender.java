/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : PacketSender.java
 * Comments : 
 * Created  : Oct 4, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  3  |Feb 27,2007  |  removed unused code							 |Abhijit |
 *---------------------------------------------------------------------------------
 * |  2  |May 06, 2005 | Misc fixes						 		         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  1  |May 4, 2004  | Added responsePacket & storing logic			 | Bindu  |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 4, 2004  | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.mesh;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;

import com.meshdynamics.meshviewer.imcp.ResponseCallback;
import com.meshdynamics.meshviewer.imcppackets.IMCPPacket;
import com.meshdynamics.meshviewer.imcppackets.RequestResponsePacket;
import com.meshdynamics.util.Event;
import com.meshdynamics.util.MacAddress;

public class PacketSender {

    public static final int DO_NOT_WAIT_FOR_RESPONSE 	= 0;
	public static final int WAIT_FOR_RESPONSE 			= 1; 

	public static final int  PACKET_SEND_MODE_IMCP		= 1;
	public static final int  PACKET_SEND_MODE_SMGP		= 2;
	
    private static short 		requestIdUnique;
    private IMCPPacket 			imcpPacket;
    private int 				mode;
    private MeshController 		meshController;

    private static Hashtable<Short, WaitForResponseData> 	waitingForResponse 	= new Hashtable<Short, WaitForResponseData>();
    
    class WaitForResponseData {
        public	MacAddress 			dsMacAddress;
        public	short 				reqResId;
        public 	Event				waitingEvent;
        public  ResponseCallback	callBackObj;
        public  IMCPPacket			responsePacket;
    }
        
    public PacketSender(MeshController meshController,int mode) {
        super();
        this.meshController	= meshController;
        this.mode 			= mode;
        this.imcpPacket 	= null;
    }
    
    public boolean sendPacket(IMCPPacket imcpPacket,String key) {
        /**
         * 1) packet.getpacket
         * 2) Create Socket and send packet
         * 3) wait for response 
         * 4) if response got then return true
         * 5) else timeout return false
         */
    
    	if(mode != PACKET_SEND_MODE_IMCP)
    		return false;
        byte[] buffer = imcpPacket.createPacket(key);
		try{ 
			DatagramSocket udpSocket = new DatagramSocket();
			DatagramPacket packet = new DatagramPacket(buffer,buffer.length,InetAddress.getByName("255.255.255.255"),MeshController.IMCP_UDP_PORT); //$NON-NLS-1$
			//DatagramPacket packet = new DatagramPacket(buffer,buffer.length);
			udpSocket.send(packet);
			/*packet.setPort(MeshController.IMCP_UDP_PORT);
			 for (InetAddress address : getBroadcastAddresses()) {	
				   packet.setAddress(address);
			        udpSocket.send(packet);
			    }*/
			udpSocket.close();
			
			meshController.forwardPacket(imcpPacket);
			
		} catch(Exception e){		
		   return false;
		}

        return true;
        
    }
   
    synchronized public short getRequestId() {
    	requestIdUnique++;
		return requestIdUnique;
    	
    }

    public boolean sendPacketWithResponse(final IMCPPacket imcpPacket,final String key, final int timeout) {
    	
    	if(!(imcpPacket instanceof RequestResponsePacket)) {
    		System.out.println("Invalid Packet for response");
    	}
    	
    	final WaitForResponseData wfResData = new WaitForResponseData();
    	wfResData.dsMacAddress	= imcpPacket.getDsMacAddress();
   		wfResData.reqResId		= getRequestId();
    	wfResData.waitingEvent	= new Event();
    	wfResData.callBackObj 	=  null;
    	
    	RequestResponsePacket pkt = (RequestResponsePacket)imcpPacket;
    	pkt.setRequestResponseID(wfResData.reqResId);
    	
    	waitingForResponse.put(new Short(wfResData.reqResId), wfResData);
    	
    	sendPacket(imcpPacket,key);

    	/**
    	 * Waiting for response
    	 */    	
    	int returnType = wfResData.waitingEvent.WaitForSingleObject(timeout);
    	if(wfResData.responsePacket != null)
    		this.imcpPacket = wfResData.responsePacket; 
    	if(returnType == Event.NOTIFY) {
    		return true;
    	} else 
    		return false;    	
    }
    

    /**
     * @param packet
     */
    public static boolean responseReceived(RequestResponsePacket packet) {
    	short id = packet.getRequestResponseID();
    	Short idS = new Short(id);
    	WaitForResponseData wfrData = (WaitForResponseData) waitingForResponse.get(idS);
    	if(wfrData != null) {
    		waitingForResponse.remove(idS);
    		wfrData.waitingEvent.setEvent();
    		return true;
    	}
		return false;   		
    }
    
    /**
     * @param packet
     */
	public IMCPPacket getImcpPacket() {
		return imcpPacket;
	}

	public void sendRawPacket(byte[] packetData) {
		try{
			DatagramSocket udpSocket = new DatagramSocket();
			DatagramPacket packet = new DatagramPacket(packetData, packetData.length,InetAddress.getByName("255.255.255.255"),MeshController.IMCP_UDP_PORT); //$NON-NLS-1$
			udpSocket.send(packet);
			udpSocket.close();
		} catch(Exception e){
		   e.printStackTrace();
		}
	}
	public static Collection<InetAddress> getBroadcastAddresses() throws UnknownHostException {
	    try {
	        Collection<InetAddress> result = new LinkedList<InetAddress>();
	        Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
	        for (NetworkInterface netint : Collections.list(nets))
	                for (InterfaceAddress address : netint.getInterfaceAddresses()) {
	                    InetAddress broadcastAddress = address.getBroadcast();
	                    if (broadcastAddress != null)
	                        result.add(broadcastAddress);
	                }
	        result.add(InetAddress.getByName("255.255.255.255"));
	        result.add(InetAddress.getByName("255.255.255.0"));
	        return result;
	    } catch (SocketException e) {
	        throw new RuntimeException(e);
	    }
	}

}
