
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ILog.java 
 * Comments : Log Handlers Interface 
 * Created  : 29/11/2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date      |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  1  |22/9/2005 | HeartBeatMessage Added                          |Prachiti|
 * -----------------------------------------------------------------------------
 * |  0  |29/11/2004| Created                                         | Anand  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.meshviewer;



public interface ILog {
	public void logErrorMessage(String errorMsg);
	public void logDebugMessage(String debugMsg);
	public void logInfoMessage(String infoMsg);
	public void logSystemMessage(String infoMsg);
}
