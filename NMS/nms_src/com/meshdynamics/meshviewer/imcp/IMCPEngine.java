/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : IMCPEngine.java
 * Comments : 
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcp;

import com.meshdynamics.meshviewer.mesh.MeshController;

public class IMCPEngine {
    
    private PacketProcessor     processor;
    private PacketReceiver 		receiver;
    private MeshController		meshController;
    private boolean				isRunning;
    
    public IMCPEngine(MeshController meshController) {
        this.meshController = meshController;
        isRunning			= false;
    }

    public boolean start() {

        processor 			= new PacketProcessor(meshController);
        receiver 			= new PacketReceiver(meshController, processor);
        if(startReceiver() == true) {
            isRunning = true;
            return true;
        }
        return false;
    }
    
    private boolean startReceiver() {
        if(receiver.isRunning() == false) {
            receiver.start();
            return true;
        }
        return false;
    }
    
    private boolean stopReceiver() {
        if(receiver != null) {
            receiver.stopReceiver();
            return true;
        }
        return false;
    }
    
    public boolean stop() {
        if(processor != null)
            processor.stopProcessor();

        stopReceiver();
        
        isRunning = false;
        return true;
    }  
    
	/**
	 * @return Returns the isRunning.
	 */
	public boolean isRunning() {
		return isRunning;
	}

	public void injectPacket(byte[] packet) {
		if(isRunning == false)
			return;
		
		receiver.packetReceived(packet, packet.length, false);
	}

	public void setIgnoreLocalPackets(boolean ignoreLocalPackets) {
		if(isRunning == true)
			receiver.setIgnoreLocalPackets(ignoreLocalPackets);
	}
	
}
