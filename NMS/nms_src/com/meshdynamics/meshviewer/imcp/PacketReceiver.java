/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : PacketReceiver.java
 * Comments : 
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

import com.meshdynamics.aes.AES;
import com.meshdynamics.aes.AESException;
import com.meshdynamics.meshviewer.imcppackets.IMCPPacket;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.MeshController;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.util.PacketWrapper;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.Bytes;

public class PacketReceiver extends Thread {
    
    private AES 										aes;
    private MeshController 								meshController;

    public PacketReceiver(MeshController meshController, PacketProcessor packetProcessor) {

		this.packetProcessor 	= packetProcessor;
		this.meshController		= meshController;
		this.running			= false;
		aes						= new AES();
		buffer 					= new byte[1500];
		udpSocket				= null;
	}
    
    private void openSocket() {
    	try {
			udpSocket = new DatagramSocket(null);
			udpSocket.setBroadcast(true);
			udpSocket.setReuseAddress(true);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return;
		}
    }
    
    public synchronized void start() {
		
    	if(udpSocket == null)
    		openSocket();
    	
    	try {
    		if(udpSocket.isBound() == false)
    			udpSocket.bind(new InetSocketAddress(MeshController.IMCP_UDP_PORT));
		} catch (SocketException e) {
			e.printStackTrace();
			return;
		}
		
    	if(running == true)
    		return;
    	
    	running = true;		
		super.start();
		
    }
    
    protected synchronized boolean packetReceived(byte[] packetData, int packetLength, boolean isLocal) {
    	
       //	System.out.println("Recv" + new String(packetData));

		int length 			= packetLength;
		BufferReader br 	= new BufferReader(packetData, length);
  		//byte[] buf 			= br.getBytes(IMCPPacket.IMCP_SIGNATURE_SIZE);
		//short packet_type 	= IMCPPacket.checkIMCP(buf);
		byte[] buf 			= br.getBytes(IMCPPacket.NEW_IMCP_SIGNATURE_SIZE);
		int packet_type        = IMCPPacket.checkIMCPNewStructure(buf);
		short  flags=Bytes.bytesToShort(buf, 6);
		if((packet_type & 0x7FFF)  <= 0)
		return false;
		 
		           String 	meshId  = br.readString();
		           int 	packetFlags = PacketWrapper.PACKET_FLAGS_NONE;
		           if((flags & 1<<15) != 0){
				MeshNetwork meshNetwork = meshController.getMeshNetwork(meshId);
			if(meshNetwork != null) {
				br 			= 	decryptPacket(meshId,meshNetwork.getKey(),br);
				packetFlags |= 	PacketWrapper.PACKET_FLAGS_ENCRYPTED;
			} else {
				/*System.out.println("Mesh network for MeshID (" + meshId +") not found");
				 * 
			 */
				return false;
			}		    
		    if(br == null) {
		        return false;
		    }	
			}
		
		packet_type  = (short) (packet_type & 0x7FFF);		
	    IMCPPacket imcpPacket = PacketFactory.getNewIMCPInstance(packet_type);
		if(imcpPacket == null)
		    return false;
		imcpPacket.setLocal(isLocal);
		imcpPacket.setRawPacket(packetData, packetLength);
	    imcpPacket.setMeshId(meshId);
		if(imcpPacket.readPacket(br) == false)
		    return false;
	    PacketWrapper packetWrapper = PacketFactory.getNewPacketWrapperInstance();
	    if(packetWrapper == null)
	        return false;
	    
	    packetWrapper.setPacket(imcpPacket,PacketWrapper.PACKET_TYPE_IMCP,packetFlags);
	    
	    packetProcessor.queuePacket(packetWrapper);
	    
		if (!packetProcessor.isAlive()) {
			packetProcessor.start();
		}
    	
		return true;
    }
    
	public void run() {
					
		System.out.println("IMCP packetreceiver started.");
		while (running == true) {
			
			try {
				
				DatagramPacket packet 		= new DatagramPacket(buffer, 1500);
				
				udpSocket.receive(packet);
				
				if(ignoreLocalPackets == true)
					continue;
				packetReceived(packet.getData(), packet.getLength(), true);
              
			} catch (Exception e) {
				if(running == false) {
					System.out.println("IMCP packetreceiver stopped.");
					break;
				}
			}
		} //end while

		if(udpSocket.isClosed())
		    udpSocket.close();

		udpSocket 	= null;
		running 	= false;
	}
	
	/**
     * @param packet_type
     * @param buffer2
     * @return
     */
    private synchronized BufferReader decryptPacket(String meshId,String key, BufferReader bufIn) {
        byte bytes[] = bufIn.getRemainingBytes();
        try {
            bytes = aes.aesDecrypt(bytes, bytes.length, key.getBytes(), key.length());
        } catch (AESException e) {
        	e.printStackTrace();
            System.out.println("Error occured while decrypting packet for network " + meshId);
        	Mesh.logException(e);
            return null;
        }
        return new BufferReader(bytes, bytes.length);        
    }

    public boolean isRunning(){
		return running;	
	}
	
	public void stopReceiver() {
	    running = false;
	    if(udpSocket != null)
	    	udpSocket.close();
    }

	private DatagramSocket 			udpSocket;
	private byte[] 					buffer;
	private boolean 				running;
	private PacketProcessor 		packetProcessor;
	private boolean					ignoreLocalPackets;
	
	public void setIgnoreLocalPackets(boolean ignoreLocalPackets) {
		this.ignoreLocalPackets	= ignoreLocalPackets;
	}
 
}
