/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : PacketProcessor.java
 * Comments : 
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcp;

import java.util.List;
import java.util.Vector;

import com.meshdynamics.meshviewer.imcppackets.IMCPPacket;
import com.meshdynamics.meshviewer.mesh.MeshController;
import com.meshdynamics.meshviewer.util.PacketWrapper;

public class PacketProcessor extends Thread {
	
    boolean 			notDead;
	List<PacketWrapper>	packetQueue;
	MeshController 		meshController;
	
	public PacketProcessor(MeshController meshController) {
		notDead 			= true;
		packetQueue			= new Vector<PacketWrapper>();
		this.meshController	= meshController;
	}

	public void queuePacket(PacketWrapper packetWrapper) {
		synchronized (packetQueue) {
			packetQueue.add(packetWrapper);
			packetQueue.notifyAll();
		}	    
	}
	
	public void run() {

		int size = 0;
		//System.err.println("In packet server thread");
		while (notDead) {

			synchronized (packetQueue) {
				size = packetQueue.size();
				packetQueue.notifyAll();
			}

			while (size > 0) {

				IMCPPacket packet 			= null;
				PacketWrapper packetWrapper = null;
				
				synchronized (packetQueue) {
				    packetWrapper = (PacketWrapper) packetQueue.remove(0);
					packetQueue.notifyAll();
				}

				if (packetWrapper != null) {
					try {
					    if(packetWrapper.getPacketType() != PacketWrapper.PACKET_TYPE_IMCP)
					        continue;
					    
					    packet = (IMCPPacket)packetWrapper.getPacket();
					    
					    if(packet == null)
					        continue;
					    
					    boolean encrypt = false;
				    
				        if((packetWrapper.getPacketFlags() & PacketWrapper.PACKET_FLAGS_ENCRYPTED) == PacketWrapper.PACKET_FLAGS_ENCRYPTED)
				            encrypt = true;
					        
				        meshController.processIMCPPacket(packet, encrypt);
				        
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				synchronized (packetQueue) {
					size = packetQueue.size();
					packetQueue.notifyAll();
				}

			}
			try {
				sleep(1000);
			} catch (Exception e) {
			}
		}
	}
	
	public void stopProcessor() {
	    notDead = false;
	}	
}
