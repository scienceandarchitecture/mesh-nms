/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ResponseCallback.java
 * Comments : 
 * Created  : Oct 16, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 16, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcp;

import com.meshdynamics.meshviewer.imcppackets.IMCPPacket;

public interface ResponseCallback {
    public void responseSuccessful(IMCPPacket packet);
    public void responseFailed(IMCPPacket packet);
}
