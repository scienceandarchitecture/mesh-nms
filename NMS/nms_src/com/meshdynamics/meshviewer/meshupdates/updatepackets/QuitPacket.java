/*
 * Created on Apr 5, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.meshupdates.updatepackets;

import com.meshdynamics.util.BufferWriter;

/**
 * @author bindu
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class QuitPacket {
	public static final short 	QUIT_PACKET_LENGTH	= 1;
	
	private final void initPacket(BufferWriter writer) {
		
		short[] signature = MeshUpdatePacketFactory.signature;
		for(int i=0; i<MeshUpdatePacketFactory.UPDATE_PACKET_SIGN_LENGTH; i++ )			
			writer.writeByte(signature[i]);
		
		writer.writeByte(getPacketType());		
	}
	
	public final byte[] createPacket() {
		int len 		= getPacketLength();
		BufferWriter bw = new BufferWriter(len);
		
		initPacket(bw);
		return bw.getBytes();
	}

	/**
	 * @return
	 */
	private int getPacketLength() {
		return MeshUpdatePacketFactory.UPDATE_PACKET_SIGN_LENGTH + QUIT_PACKET_LENGTH;		
	}

	private short getPacketType() {
		return MeshUpdatePacketFactory.UPDATE_PACKET_TYPE_QUIT;
	}
	
}
