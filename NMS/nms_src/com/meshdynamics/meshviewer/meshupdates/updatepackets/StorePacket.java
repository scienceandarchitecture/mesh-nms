
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : StorePacket.java
 * Comments : 
 * Created  : Mar 11, 2005
 * Author   : Bindu Khare
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date        |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  1  |Apr 13, 2006| Changes for Firmware Update       			    | Bindu  |
 * -----------------------------------------------------------------------------
 * |  0  |Mar 11, 2005| Created                                         | Bindu  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.meshviewer.meshupdates.updatepackets;

import com.meshdynamics.util.BufferWriter;

public class StorePacket {
	
	public static final short STOR_PACKET_LENGTH	= 8;
	
	public static final short UPDATE_TYPE_CFD		= 1;
	public static final short UPDATE_TYPE_DRV		= 2;
	public static final short UPDATE_TYPE_MAP		= 4;
	public static final short UPDATE_TYPE_SCP		= 8;
	public static final short UPDATE_TYPE_WEB		= 16;
	public static final short UPDATE_TYPE_OTH		= 32;
	public static final short UPDATE_TYPE_EXE		= 64;
	public static final short UPDATE_TYPE_DMN		= 128;
	
	private short 		updateType;	
	private String 		path;
	private long 		fileSize;	
	private short[]		checksum = new short[16];	
	
	
	private final void initPacket(BufferWriter writer) {
		
		short[] signature = MeshUpdatePacketFactory.signature;
		for(int i=0; i<MeshUpdatePacketFactory.UPDATE_PACKET_SIGN_LENGTH; i++ )			
			writer.writeByte(signature[i]);
		
		writer.writeByte((short)getPacketType());		
	}

	public final byte[] createPacket() {
		int len 		=  getPacketLength();
		BufferWriter bw = new BufferWriter(len); 
		initPacket(bw);
 
		getPacket(bw);
//		byte[] bytes = neBW.getBytes();
//		try {
//			/bytes = aes.aesEncrypt(bytes, bytes.length, key.getBytes(), key.length());
//		} catch (AESException e) {
//			Mesh.logException(e);
//		}
		return bw.getBytes();
	}
	
	/**
	 * @param bw
	 */
	private void getPacket(BufferWriter bw) {
		bw.writeByte(updateType);
		bw.writeString(path);
		bw.writeInt(fileSize);
		bw.writeByte((short)checksum.length);
		for(int i = 0; i < checksum.length;i++)
			bw.writeByte(checksum[i]);
	}
	
	/**
	 * @return
	 */
	private int getPacketLength() {
		return MeshUpdatePacketFactory.UPDATE_PACKET_SIGN_LENGTH + STOR_PACKET_LENGTH + path.length() + getChecksum().length;

	}

	private short getPacketType() {
		return MeshUpdatePacketFactory.UPDATE_PACKET_TYPE_STOR;
	}
	
	public short[] getChecksum() {
		return checksum;
	}

	public void setChecksum(short[] checksum) {
		this.checksum = checksum;
	}

	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public short getUpdateType() {
		return updateType;
	}
	
	public void setUpdateType(short updateType) {
		this.updateType = updateType;
	}

}
