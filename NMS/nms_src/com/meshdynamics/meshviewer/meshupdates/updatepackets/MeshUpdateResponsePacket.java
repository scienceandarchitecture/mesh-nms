
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MeshUpdateResponsePacket.java
 * Comments : 
 * Created  : Mar 11, 2005
 * Author   : Bindu Khare
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date        |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |Mar 11, 2005| Created                                         | Bindu  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.meshviewer.meshupdates.updatepackets;


public class MeshUpdateResponsePacket {
	
	public static final short RESP_PACKET_LENGTH					= 1;	
	public static final short UPDATE_PACKET_RESP_TYPE_SIGN_ERR		= 1;
	
	public static final short UPDATE_PACKET_RESP_TYPE_SUCCESS		= 123;
	public static final short UPDATE_PACKET_RESP_TYPE_NOT_IMPL		= 124;
	public static final short UPDATE_PACKET_RESP_TYPE_INVALID		= 125;
	public static final short UPDATE_PACKET_RESP_TYPE_FAILED		= 126;
	public static final short UPDATE_PACKET_RESP_TYPE_PARAMS_ERR	= 127;
	public static final short UPDATE_PACKET_RESP_TYPE_CHKSUM_ERR	= 128;
	public static final short UPDATE_PACKET_RESP_TYPE_FNF			= 404;
	
}
