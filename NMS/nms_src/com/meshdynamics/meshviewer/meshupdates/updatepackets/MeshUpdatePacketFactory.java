
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : UpdatePacketFactory.java
 * Comments : 
 * Created  : Mar 11, 2005
 * Author   : Bindu Khare
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date        |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |Mar 11, 2005| Created                                         | Bindu  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.meshviewer.meshupdates.updatepackets;


public class MeshUpdatePacketFactory {

	public static final int UPDATE_PACKET_SIGN_LENGTH					= 4;
	
	public static final short UPDATE_PACKET_TYPE_FILE_COUNT				= 1;
	public static final short UPDATE_PACKET_TYPE_STOR					= 2;
	public static final short UPDATE_PACKET_TYPE_RECV					= 3;
	public static final short UPDATE_PACKET_TYPE_STAT					= 4;
	public static final short UPDATE_PACKET_TYPE_QUIT					= 5;
	public static final short UPDATE_PACKET_TYPE_RESP					= 6;
	
	public static short[] signature = {'M', 'S', 'U', 'P'};	
	
}
