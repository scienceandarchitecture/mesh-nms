
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : FileCountPacket.java
 * Comments : 
 * Created  : Mar 11, 2005
 * Author   : Bindu Khare
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date        |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  0  |Mar 11, 2005| Created                                         | Bindu  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.meshviewer.meshupdates.updatepackets;

import com.meshdynamics.util.BufferWriter;

public class FileCountPacket {
	
	public static final short FCNT_PACKET_LENGTH	= 2;	

	private int count;
	
	
	private final void initPacket(BufferWriter writer) {
		
		short[] signature = MeshUpdatePacketFactory.signature;
		for(int i=0; i<MeshUpdatePacketFactory.UPDATE_PACKET_SIGN_LENGTH; i++ )			
			writer.writeByte(signature[i]);
		
		writer.writeByte(getPacketType());		
	}
	
	public final byte[] createPacket() {
		int len 		= getPacketLength();
		BufferWriter bw = new BufferWriter(len);
		
		initPacket(bw); 
		getPacket(bw);

		return bw.getBytes();
	}
	/**
	 * @param bw
	 */
	private void getPacket(BufferWriter bw) {
		bw.writeByte((short)count);		
	}

	/**
	 * @return
	 */
	private int getPacketLength() {
		return MeshUpdatePacketFactory.UPDATE_PACKET_SIGN_LENGTH + FCNT_PACKET_LENGTH;		
	}

	private short getPacketType() {
		return MeshUpdatePacketFactory.UPDATE_PACKET_TYPE_FILE_COUNT;
	}
	
	/**
	 * @return Returns the count.
	 */
	public int getCount() {
		return count;
	}
	
	/**
	 * @param count The count to set.
	 */
	public void setCount(int count) {
		this.count = count;
	}
}
