package com.meshdynamics.meshviewer;

public interface IMeshViewerListener {

	public void viewerStarted		();
	public void viewerStopped		();
	public void meshGatewayStarted	();
	public void meshGatewayStopped	();
}
