package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class SipPrivatePacket extends IMCPPacket {
	public int		dataLength;
	public byte[]	data;
	
	@Override
	protected void copyPacket(IMCPPacket packet) {
	}

	@Override
	public void getPacket(BufferWriter buffer) {
	}

	@Override
	public int getPacketLength() {
		return 6 + 2 + data.length;
	}

	@Override
	public byte getPacketType() {
		return PacketFactory.SIP_PRIVATE_PACKET;
	}

	@Override
	public boolean readPacket(BufferReader buffer) {
		dsMacAddress 	= buffer.readMacAddress();  
		dataLength 		= buffer.readShort();
		if(dataLength > 0)
			data = buffer.getBytes(dataLength);
		
		return true;
	}

}
