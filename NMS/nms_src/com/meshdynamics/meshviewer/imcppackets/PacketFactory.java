/**
 * MeshDynamics 
 * -------------- 
 * File     : PacketFactory.java
 * Comments : 
 * Created  : Sep 24, 2004
 * Author   : abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author     |
 * --------------------------------------------------------------------------------
 * | 16  |Mar 14, 2007   | Effistream Packet Added		      		  | Bindu      |
 *  --------------------------------------------------------------------------------
 * | 15  |Dec 13, 2006   | RF Custom Channel Packet Modified	      | Abhijit    |
 *  --------------------------------------------------------------------------------
 * | 14  |Dec 12, 2006   | RF Custom Channel Packet Added		      | Bindu      |
 *  --------------------------------------------------------------------------------
 * | 13  |Nov 27, 2006   | SMG Heartbeat Added					      | Bindu      |
 *  --------------------------------------------------------------------------------
 * | 12  |May 19, 2006   |Reboot Request Packet						  | Mithil     |
 *  --------------------------------------------------------------------------------
 * | 11  |May 12, 2006   |Changes for DL Saturation					  | Bindu      |
 *  --------------------------------------------------------------------------------
 * | 10  |Mar 16, 2006   |Changes for ACL							  | Mithil 	   |
 *  --------------------------------------------------------------------------------
 * |  9  |Feb 20, 2006   | Name change for Dot11e IfInfo			  | Bindu      |
 *  --------------------------------------------------------------------------------
 * |  8  |Feb 17, 2006   | Misc Fixes for Dot11e	  				  | Bindu      |
 *  --------------------------------------------------------------------------------
 * |  7  |Feb 16, 2006   | Changes for Dot11e	  					  | Bindu      |
 *  --------------------------------------------------------------------------------
 * |  6  |Feb 13, 2006   | Hide ESSID Implemented					  | Mithil     |
 *  --------------------------------------------------------------------------------
 * |  5  |Feb 03, 2006   | Generic Request		   	 				  | Abhijit	   |
 * --------------------------------------------------------------------------------
 * |  4  |Feb 02, 2006 	 | STA Information Packet		 			  | Mithil 	   |
 * --------------------------------------------------------------------------------
 * |  3  |Nov 25,2005    | Reg Domain and Ack Timeout changes 1015    | Mithil     |
 * 		 |				 | and 1027									  | 		   |	
 * -------------------------------------------------------------------------------
 * |  2  |Feb 09, 2005   | CONFIG_REQ_RES_PACKET Packet Added         | Bindu      |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 07, 2005   | VLAN Packet Added                          | Bindu      |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004   | Created	                                  | abhijit    |
 * --------------------------------------------------------------------------------
 */
package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.util.PacketWrapper;

public class PacketFactory {
	
	public static final int HEARTBEAT									= 1;
	public static final int STA_ASSOC_NOTIFICATION						= 2;
	public static final int STA_DISASSOC_NOTIFICATION					= 3;
	public static final int HANDSHAKE_REQUEST							= 4;
	public static final int HANDSHAKE_RESPONSE							= 5;
	public static final int CH_SCAN_LOCK								= 6;
	public static final int CH_SCAN_UNLOCK								= 7;
	public static final int CH_SCAN_LOCK_OBJECTION						= 8;
	public static final int AP_RESET_REQUEST							= 9;
	public static final int AP_HW_INFO									= 10;
	public static final int AP_CONFIGURATION_INFO						= 11;
	public static final int AP_CONFIG_REQUEST_RESPONSE					= 12;
	public static final int AP_HW_INFO_REQUEST							= 13;	
	public static final int AP_SUBTREE_INFO								= 14;
	public static final int AP_IP_CONFIGURATION_INFO					= 15;
	public static final int AP_IP_CONFIG_REQUEST_RESPONSE				= 16;// Deprecated
	public static final int AP_START_IP_INTERFACE						= 17;
	public static final int AP_FW_UPDATE_STATUS							= 20;
	public static final int AP_DEBUG_INFO                   			= 21;
	public static final int IMCP_KEY_UPDATE                 			= 22;
	public static final int IMCP_KEY_UPDATE_RESPONSE	    			= 23;
	public static final int AP_VLAN_CONFIGURATION_INFO     				= 24;
	public static final int CONFIG_REQ_RES_PACKET          				= 25;
	public static final int HEARTBEAT2	                				= 33;
	public static final int AP_RESTORE_DEFAULTS_REQUEST					= 34;
	public static final int AP_REG_DOMAIN_CCODE_REQUEST_PACKET			= 35;
	public static final int AP_REG_DOMAIN_CCODE_INFO_PACKET				= 36;
	public static final int AP_ACK_TIMEOUT_REQUEST_PACKET 				= 37;
	public static final int AP_ACK_TIMEOUT_INFO_PACKET 					= 38;
	public static final int AP_IF_CHANNEL_INFO_REQUEST_PACKET 			= 39;
	public static final int AP_IF_CHANNEL_INFO_PACKET					= 40;
	public static final int STA_INFO_REQUEST_PACKET						= 41;
	public static final int STA_INFO_PACKET								= 42;
	public static final int DOT11E_CATEGORY_INFO_PACKET					= 43;
	public static final int DOT11E_IFCONFIG_INFO_PACKET					= 44;
	public static final int GENERIC_COMMAND_REQUEST_RESPONSE_PACKET		= 45;
	public static final int IF_HIDDEN_ESSID_PACKET 						= 46;
	public static final int ACL_INFO_PACKET 							= 47;
	public static final int DN_LINK_SATURATION_INFO_PACKET 				= 48;
	public static final int REBOOT_REQUEST_PACKET		 				= 49;
	public static final int RF_CUSTOM_CHANNEL_REQUEST_RESPONSE_PACKET 	= 50;
	public static final int RF_CUSTOM_CHANNEL_INFO_PACKET 				= 51;
	public static final int EFFISTREAM_CONFIG_REQUEST_RESPONSE_PACKET 	= 52;
	public static final int EFFISTREAM_CONFIG_PACKET 					= 53;
	public static final int SIP_PRIVATE_PACKET 							= 60;
	public static final int SIP_CONFIG_PACKET 							= 61;
	public static final int IMCP_MAC_ADDR_REQUEST_RESPONSE              = 62;
	public static final int IMCP_MAC_ADDR_INFO_REQUEST                  = 63;
	public static final int IMCP_SW_UPDATE_REQUEST                      = 18;
	public static final int IMCP_SW_UPDATE_RESPONSE                     = 19;
	public static final int AP_FW_UPDATE_REQUEST						= 66;
	public static final int AP_FW_UPDATE_RESPONSE						= 67;	
	public static final int MGMT_GW_CONFIGREQUEST                       = 64;
	public static final int MGMT_GW_RESPONSE                            = 65;
	
	
	
	public static IMCPPacket getNewIMCPInstance(int packetType){

		IMCPPacket packetObj 	= null;
		
		switch (packetType) {
			
			case HEARTBEAT :
				packetObj = new HeartbeatPacket();
				break;
				
			case STA_ASSOC_NOTIFICATION :
				packetObj = new StaAssocPacket();
				break;
				
			case STA_DISASSOC_NOTIFICATION :
				packetObj = new StaDisassocPacket();
				break;
				
			case CH_SCAN_LOCK:
			case CH_SCAN_UNLOCK:
			case CH_SCAN_LOCK_OBJECTION:
				packetObj = new ChannelScanPacket((byte) packetType);
				break;
				
			case AP_RESET_REQUEST:
			    packetObj = new ApResetPacket();
			    break;
			    
			case AP_HW_INFO:
				packetObj = new ApHwInfoPacket();
				break;
			
			case AP_HW_INFO_REQUEST:
			    packetObj = new ApHwInfoRequestPacket();
			    break;
			    
			case AP_CONFIGURATION_INFO:
				packetObj = new ApConfigInfoPacket();
				break;
				
			case AP_CONFIG_REQUEST_RESPONSE:
				packetObj = new ApConfigRequestResponsePacket();
				break;
				
			case AP_IP_CONFIGURATION_INFO:
				packetObj = new ApIpConfigInfoPacket();
				break;
				
			case AP_FW_UPDATE_REQUEST:
				packetObj = new FirmwareUpdateRequestPacket();
				break;
				
			case AP_FW_UPDATE_RESPONSE:
				packetObj = new FirmwareUpdateResponsePacket();
				break;
				
			case IMCP_KEY_UPDATE:
				packetObj = new ImcpKeyUpdatePacket();
				break;
				
			case IMCP_KEY_UPDATE_RESPONSE:
				packetObj = new ImcpKeyUpdateResponsePacket();
				break;
				
			case AP_VLAN_CONFIGURATION_INFO:
				packetObj = new VLanConfigInfoPacket();
				break;
				
			case CONFIG_REQ_RES_PACKET :
				packetObj = new ConfigRequestResponsePacket();
				break;
			case HEARTBEAT2 :
				packetObj = new HeartBeat2Packet();
				break;
				
			case  AP_REG_DOMAIN_CCODE_REQUEST_PACKET :
				packetObj = new ApRegDomainRequestPacket();
				break;	
				
			case  AP_REG_DOMAIN_CCODE_INFO_PACKET :
				packetObj = new ApRegDomainInfoPacket();
				break;
				
			case  AP_ACK_TIMEOUT_REQUEST_PACKET :
				packetObj = new ApAckTimeoutRequestResponsePacket();
				break;
				
			case  AP_ACK_TIMEOUT_INFO_PACKET :
				packetObj = new ApAckTimeoutInfoPacket();
				break;	
			
			case AP_IF_CHANNEL_INFO_REQUEST_PACKET:
			    packetObj = new ApIfChannelInfoRequestPacket();
			    break;
			    
			case AP_RESTORE_DEFAULTS_REQUEST :
				packetObj = new ApRestoreDefaultsRequestPacket();
				break;
				
			case AP_IF_CHANNEL_INFO_PACKET	:
				packetObj = new ApIfChannelInfoPacket();
				break;
				
			case STA_INFO_REQUEST_PACKET :
				packetObj = new StaInfoRequestPacket();
				break;
				
			case STA_INFO_PACKET :
				packetObj = new StaInfoPacket();
				break;	
			
			case GENERIC_COMMAND_REQUEST_RESPONSE_PACKET :
				packetObj = new GenericCommandRequestResponse();
				break;	
				
			case IF_HIDDEN_ESSID_PACKET:
				packetObj = new IfHiddenESSIDInfoPacket();
				break;
				
			case DOT11E_CATEGORY_INFO_PACKET:
				packetObj = new Dot11eCategoryPacket();
				break;
				
			case DOT11E_IFCONFIG_INFO_PACKET:
				packetObj = new Dot11eIfInfoPacket();
				break;
				
			case ACL_INFO_PACKET:
				packetObj = new ACLInfoPacket();
				break;
				
			case DN_LINK_SATURATION_INFO_PACKET:
				packetObj = new DLSaturationInfoPacket();
				break;	
			
			case REBOOT_REQUEST_PACKET:
				packetObj = new RebootRequestPacket();
				break;		
				
			case RF_CUSTOM_CHANNEL_REQUEST_RESPONSE_PACKET:
				packetObj = new RFCustomChannelInfoRequestResponsePacket();
				break;
				
			case RF_CUSTOM_CHANNEL_INFO_PACKET:
				packetObj = new RFCustomChannelInfoPacket();
				break;

			case EFFISTREAM_CONFIG_REQUEST_RESPONSE_PACKET:
				packetObj = new EffistreamRequestResponsePacket();
				break;
				
			case EFFISTREAM_CONFIG_PACKET:
				packetObj = new EffistreamConfigPacket();
				break;

			case SIP_CONFIG_PACKET:
				packetObj = new SipConfigPacket();
				break;
				
			case SIP_PRIVATE_PACKET:
				packetObj = new SipPrivatePacket();
				break;
				
			case IMCP_MAC_ADDR_INFO_REQUEST:
				packetObj=new MacAddrInfoRequestPacket();
				break;
				
			case IMCP_MAC_ADDR_REQUEST_RESPONSE:
				packetObj=new MacAddressRequestResponsePacket();
				break;
				
			case IMCP_SW_UPDATE_REQUEST:
				packetObj=new Firmware_SW_UPDATE_REQUEST();
				break;
				
			case IMCP_SW_UPDATE_RESPONSE:
				packetObj=new Firmware_SW_UPDATE_RESPONSE();
				break;
				
			case MGMT_GW_CONFIGREQUEST:
				 packetObj=new MgmtgwConfigRequestPacket();
				break;
				
			case MGMT_GW_RESPONSE:
				 packetObj=new MgmtgwResponsePacket();
				 break;
			default:
				return null;
					
		}
		
		return packetObj;
		
	}
	
	public static PacketWrapper getNewPacketWrapperInstance(){
	    return new PacketWrapper();
	}
	
}
