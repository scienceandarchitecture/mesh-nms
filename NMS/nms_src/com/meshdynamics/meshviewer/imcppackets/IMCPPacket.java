/**
 * MeshDynamics 
 * -------------- 
 * File     : ImcpPacket.java
 * Comments : IMCP Base class 
 * Created  : Sep 24, 2004
 * Author   : abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ---------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                       | Author |
 *  ---------------------------------------------------------------------------------
 * |  4  |Jan 12, 2007   |  isEncrypted method implemented                | Imran  |
 * ---------------------------------------------------------------------------------
 * |  3  |May 10, 2005   |isEncrypted method added for encryption fix     | Anand  |
 * --------------------------------------------------------------------------------
 * |  2  |May 03, 2005   | Bug fixed - Outgoing packet encrypted		  | Anand  |
 * ---------------------------------------------------------------------------------
 * |  1  |Sep 30, 2004   | Abstract functions added                       | Anand  |
 * ---------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004   | Created	                                      | abhijit|
 * ---------------------------------------------------------------------------------
 ***********************************************************************************/
package com.meshdynamics.meshviewer.imcppackets;

import java.util.Arrays;

import com.meshdynamics.aes.AES;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.Bytes;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;

public abstract class IMCPPacket {
	
	public static final int 	  	IMCP_SIGNATURE_SIZE		= 7;
	public static final int         NEW_IMCP_SIGNATURE_SIZE = 10;
	public static final boolean   	ENCRYPTED				= true;//testing purpose
	
	private static AES		aes;

	protected MacAddress 	dsMacAddress;
	protected String 		meshId;
	protected byte			majorVersion;
	protected byte			minorVersion;
	private byte[]			rawPacket;
	private boolean			isLocal;
	
	static {
		aes = new AES();
	}

	public IMCPPacket() {
	    meshId          = "default";
	    dsMacAddress 	= new MacAddress();
	    rawPacket		= null;
	    isLocal 		= true;
	}
	
	public static byte checkIMCP(byte[] buf) {

		if (buf[0] == 'I'
			&& buf[1] == 'M'
			&& buf[2] == 'C'
			&& buf[3] == 'P') {
		    // Version Check
		    if( buf[4] == 5 &&  buf[5] == 0) {
		        return buf[6];
		    }
		}		
		
		return 0;  //Return Packet Type
	}	
	//according to new imcp structure   
	public static short checkIMCPNewStructure(byte[] buf){
		
		if (buf[0] == 'I'
				&& buf[1] == 'M'
				&& buf[2] == 'C'
				&& buf[3] == 'P'){
			if( buf[4] == 5 &&  buf[5] == 0){
			   short val=Bytes.bytesToShort(buf,8);
			   return  val;
			}
		}
		return 0;
	}
	public int readIpAddress(IpAddress ipAddress,byte[] buffer,int position){

		byte[] ip = new byte[4];
		int i=0;
		
		for(i=0;i<4;i++){
			ip[i] = buffer[position + i];	
		}
		ipAddress.setBytes(ip);

		return i;
		
	}
	
	public final byte[] createPacket(String key) {
		int len = getSignatureLength();
		int pktLen =  getPacketLength();
		BufferWriter bw = null; 
		if(key != null && isEncrypted()) {
			bw = new BufferWriter(pktLen);
			getPacket(bw);
			byte[] bytes = bw.getBytes();
			try {
				bytes = aes.aesEncrypt(bytes, bytes.length, key.getBytes(), key.length());
				bw = new BufferWriter(len + bytes.length);
				initPacket(bw, key);
				bw.writeBytes(bytes);
			} catch (Exception e) {
				Mesh.logException(e);
			}
		} else {
			bw = new BufferWriter(len + pktLen); 
			initPacket(bw, key);
			getPacket(bw);
		}
		byte[] packetBytes = bw.getBytes();
		this.rawPacket = new byte[packetBytes.length];
		System.arraycopy(packetBytes, 0, this.rawPacket, 0, packetBytes.length);
		return packetBytes;
	}

	/**
	 * @return length for IMCP Signature
	 * 4 Bytes - IMCP
	 * 2 Bytes - Version Info
	 * 1 Bytes - Packet Type
	 * 1 Bytes - Mesh Id Length
	 * n Bytes - Mesh Id (n = Mesh Id Length)
	 */
	/*private int getSignatureLengthOld() {
		return 4 + 2 + 1 + 1 + meshId.length();
	}*/
	/**
	 * @return length for IMCP Signature
	 * 4 Bytes - IMCP
	 * 2 Bytes - Version Info
	 * 2 Bytes - Packet Type
	 * 2 bytes - for flags (for feature use purpose),not yet add
	 * 1 Bytes - Mesh Id Length
	 * n Bytes - Mesh Id (n = Mesh Id Length)
		*/
	private int getSignatureLength() {
		return 4 + 2 + 2 + 2 +1 + meshId.length();
	}
	
	private final int initPacket(BufferWriter writer, String key){
		
	    writer.writeByte((short) 'I');
	    writer.writeByte((short) 'M');
	    writer.writeByte((short) 'C');
	    writer.writeByte((short) 'P');
	    writer.writeByte((short) 5);
	    writer.writeByte((short) 0);
	    int packet_type = getPacketType();
	     if(isEncrypted())
	       writer.writeShort(0 | 1<<15);
	    else
	       writer.writeShort(0);
	    writer.writeShort(packet_type);
	    writer.writeString(meshId);
	    
		return packet_type;
	}
	
	/*private final int initPacket(BufferWriter writer, String key){
		
	    writer.writeByte((short) 'I');
	    writer.writeByte((short) 'M');
	    writer.writeByte((short) 'C');
	    writer.writeByte((short) 'P');
	    writer.writeByte((short) 5);
	    writer.writeByte((short) 0);
	    short packet_type = getPacketType();
	    if(key != null && isEncrypted())
	    	packet_type = (short) (0x80 | packet_type);
	    writer.writeByte(packet_type);
	    writer.writeString(meshId);
		return packet_type;
	}*/
	
	/**
	 * @return Returns the dsMacAddress.
	 */
	public MacAddress getDsMacAddress() {
		return dsMacAddress;
	}
	/**
	 * @param dsMacAddress The dsMacAddress to set.
	 */
	public void setDsMacAddress(MacAddress dsMacAddress) {
		this.dsMacAddress = dsMacAddress;
	}
	
    /**
     * @return Returns the meshId.
     */
    public String getMeshId() {
        return meshId;
    }
    /**
     * @param meshId The meshId to set.
     */
    public void setMeshId(String meshId) {
        this.meshId = meshId;
    }
    
    /**
     * @return Returns the encryption flag.
     */
    public	boolean isEncrypted(){
		return ENCRYPTED;
	}
    
    public IMCPPacket getPacketCopy() {
    	IMCPPacket packet = PacketFactory.getNewIMCPInstance(this.getPacketType());
    	/**
    	 * Copy base class members
    	 */
    	packet.dsMacAddress.setBytes(this.dsMacAddress.getBytes());
    	packet.meshId 		= new String(this.meshId.getBytes());
    	packet.majorVersion = this.majorVersion;
    	packet.minorVersion = this.minorVersion;
    	copyPacket(packet);
    	return packet;
    }
    
    public boolean canProcessPacket() {
    	return true;
    }
    
    public 		abstract boolean 	readPacket(BufferReader buffer);
	public		abstract void 		getPacket(BufferWriter buffer);
	public		abstract int 		getPacketLength();
	public		abstract byte 		getPacketType();
	protected   abstract void  		copyPacket(IMCPPacket packet);

	public byte[] getRawPacket() {
		return rawPacket;
	}

	public void setRawPacket(byte[] rawPacket, int length) {
    	this.rawPacket = new byte[length];
    	System.arraycopy(rawPacket, 0, this.rawPacket, 0, length);
	}

	public boolean isLocal() {
		return isLocal;
	}

	public void setLocal(boolean isLocal) {
		this.isLocal = isLocal;
	}
	
}
