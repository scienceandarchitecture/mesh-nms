/*
 * Created on Mar 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.imcppackets;

import java.util.Vector;

import com.meshdynamics.meshviewer.imcppackets.helpers.SipStaInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.IpAddress;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SipConfigPacket extends RequestResponsePacket {

	private static final int BASE_PACKET_LENGTH	= 17;
	
	private int         SIP_CONFIG_ID;
	private int         SIP_CONFIG_LENGTH;
	private IpAddress 	serverIpAddress;
	private int 		port;
	private short		enabled;
	private short		adhocOnly;
	private short		staCount;
	private Vector<SipStaInfo>		staEntries;
	
	/**
	 * 
	 */
	public SipConfigPacket() {
		super();
		staEntries = new Vector<SipStaInfo>();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter buffer) {
		
		try {
			buffer.writeShort(SIP_CONFIG_ID);
			buffer.writeShort(SIP_CONFIG_LENGTH);
			buffer.writeMacAddress(dsMacAddress);
			buffer.writeByte(requestResponseType);
			buffer.writeByte(requestResponseID);
			buffer.writeByte(this.enabled);
			buffer.writeByte(this.adhocOnly);
			buffer.writeIPAddress(this.serverIpAddress);
			buffer.writeShort(this.port);
			buffer.writeByte(this.staCount);
			
			for(int i=0;i<staEntries.size();i++) {
				SipStaInfo staInfo = (SipStaInfo)staEntries.get(i);
				buffer.writeMacAddress(staInfo.getMacAddress());
				buffer.writeByte(staInfo.getExtn());
			}
		}catch(Exception e) {
			Mesh.logException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return BASE_PACKET_LENGTH + (staEntries.size() * 7);	//6 bytes mac + 1 byte extn
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.SIP_CONFIG_PACKET;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		
		try {
			SIP_CONFIG_ID            = buffer.readShort();
			SIP_CONFIG_LENGTH        = buffer.readShort();
			dsMacAddress 			= buffer.readMacAddress();			
			requestResponseType 	= buffer.readByte(); 
			requestResponseID  		= buffer.readByte();
			enabled					= buffer.readByte();
			adhocOnly				= buffer.readByte();
			serverIpAddress			= buffer.readIPAddress();
			port					= buffer.readShort();
			staCount				= buffer.readByte();
			staEntries.clear();
			for(int i=0;i<staCount;i++) {
				SipStaInfo staInfo = new SipStaInfo();
				staInfo.setMacAddress(buffer.readMacAddress().getBytes());
				staInfo.setExtn(buffer.readByte());
				staEntries.add(staInfo);
			}
			
		} catch(Exception e) {
			Mesh.logException(e);
			return false;
		}
		
		return true;
	}
	/**
	 * @return Returns the enabled.
	 */
	public short getEnabled() {
		return enabled;
	}
	/**
	 * @param enabled The enabled to set.
	 */
	public void setEnabled(short enabled) {
		this.enabled = enabled;
	}
	/**
	 * @return Returns the port.
	 */
	public int getPort() {
		return port;
	}
	/**
	 * @param port The port to set.
	 */
	public void setPort(int port) {
		this.port = port;
	}
	/**
	 * @return Returns the serverIpAddress.
	 */
	public IpAddress getServerIpAddress() {
		return serverIpAddress;
	}
	/**
	 * @param serverIpAddress The serverIpAddress to set.
	 */
	public void setServerIpAddress(IpAddress serverIpAddress) {
		this.serverIpAddress = serverIpAddress;
	}
	/**
	 * @return Returns the staCount.
	 */
	public short getStaCount() {
		return staCount;
	}
	/**
	 * @param staCount The staCount to set.
	 */
	public void setStaCount(short staCount) {
		this.staCount = staCount;
		this.staEntries.clear();
		
		for(int i=0;i<staCount;i++) {
			SipStaInfo staInfo = new SipStaInfo();
			staEntries.add(staInfo);
		}
	}
	
	public SipStaInfo getStaInfo(int index) {
		return (SipStaInfo)staEntries.get(index);
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#isEncrypted()
	 */
	public boolean isEncrypted() {
		return false;
	}

	public short getAdhocOnly() {
		return adhocOnly;
	}

	public void setAdhocOnly(short adhocOnly) {
		this.adhocOnly = adhocOnly;
	}

	public int getSIP_CONFIG_ID() {
		return SIP_CONFIG_ID;
	}

	public void setSIP_CONFIG_ID(int sIP_CONFIG_ID) {
		SIP_CONFIG_ID = sIP_CONFIG_ID;
	}

	public int getSIP_CONFIG_LENGTH() {
		return SIP_CONFIG_LENGTH;
	}

	public void setSIP_CONFIG_LENGTH(int sIP_CONFIG_LENGTH) {
		SIP_CONFIG_LENGTH = sIP_CONFIG_LENGTH;
	}
	
}
