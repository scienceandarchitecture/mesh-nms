/**
 * MeshDynamics 
 * -------------- 
 * File     : ApIpConfigRequestResponsePacket.java
 * Comments : 
 * Created  : Sep 24, 2004
 * Author   : abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -------------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author      |
 * ------------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method removed                      | Imran  	  |
 * -------------------------------------------------------------------------------------
 * |  1  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand       |
 * -------------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004 | Created	                                     | abhijit     |
 * -------------------------------------------------------------------------------------
 */

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class ApIpConfigRequestResponsePacket extends RequestResponsePacket {
	
	private static final int PACKET_LENGTH = 7;
	private int APIP_CONFIG_REQ_RES_ID;
	private int APIP_CONFIG_REQ_RES_LENGTH;
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			APIP_CONFIG_REQ_RES_ID        = buffer.readShort();
			APIP_CONFIG_REQ_RES_LENGTH    = buffer.readShort();
			dsMacAddress 		= buffer.readMacAddress();
			requestResponseType = buffer.readByte();
			requestResponseID 	= buffer.readByte();
		}catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter writer) {
		writer.writeShort(APIP_CONFIG_REQ_RES_ID);
		writer.writeShort(APIP_CONFIG_REQ_RES_LENGTH);
	    writer.writeMacAddress(dsMacAddress);
	    writer.writeByte(requestResponseType);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
	public byte getPacketType() {
        return PacketFactory.AP_IP_CONFIG_REQUEST_RESPONSE;
    }

	public int getAPIP_CONFIG_REQ_RES_ID() {
		return APIP_CONFIG_REQ_RES_ID;
	}

	public void setAPIP_CONFIG_REQ_RES_ID(int aPIP_CONFIG_REQ_RES_ID) {
		APIP_CONFIG_REQ_RES_ID = aPIP_CONFIG_REQ_RES_ID;
	}

	public int getAPIP_CONFIG_REQ_RES_LENGTH() {
		return APIP_CONFIG_REQ_RES_LENGTH;
	}

	public void setAPIP_CONFIG_REQ_RES_LENGTH(int aPIP_CONFIG_REQ_RES_LENGTH) {
		APIP_CONFIG_REQ_RES_LENGTH = aPIP_CONFIG_REQ_RES_LENGTH;
	}

	
}
