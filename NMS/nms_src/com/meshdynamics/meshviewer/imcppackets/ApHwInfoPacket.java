/**
 * MeshDynamics 
 * -------------- 
 * File     : ApHwInfoPacket.java
 * Comments : 
 * Created  : Sep 24, 2004
 * Author   : abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ------------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                     | Author      |
 * ------------------------------------------------------------------------------------
 * | 28  |Mar 08,2007    | changes for saturation info					|Abhijit      |
 *-------------------------------------------------------------------------------------
 * | 27  |Feb 27,2007    |  removed unused code							|Abhijit      |
 *-------------------------------------------------------------------------------------
 * | 15  |Jan 12, 2007   | isEncrypted method removed                   | Imran  	  |
 * ------------------------------------------------------------------------------------
 * | 14  | Oct 06, 2006  | Misc fixes		 							|Bindu        |
 * ------------------------------------------------------------------------------------
 * | 13  | Jul 10, 2006  | Misc firmware version check fixes		 	|Bindu        |
 * ------------------------------------------------------------------------------------
 * | 12  |Jun 13, 2006   | Problems loading saved network               |Mithil       |
 * ------------------------------------------------------------------------------------
 * | 11  |Jun 12, 2006   | Misc changes for mobility                    |Bindu        |
 * ------------------------------------------------------------------------------------
 * | 10  |May 31, 2006   | Misc changes 		                        |Bindu        |
 * ------------------------------------------------------------------------------------
 * |  9  |May 25,2006    | Misc Changes									| Bindu       |
 * ------------------------------------------------------------------------------------
 * |  8  |May 22, 2006   | Scanner changes for DL Sat					| Bindu       |
 * ------------------------------------------------------------------------------------
 * |  7  |May 19, 2006   | Scanner channel list changes					| Mithil      |
 * ------------------------------------------------------------------------------------
 * |  6  |May 19, 2006   | Changes for DL Saturation Info				| Bindu	      |
 * ------------------------------------------------------------------------------------
 * |  5  |Feb 28, 2006   | Apifchannel info request sent with network id| Mithil      |
 * ------------------------------------------------------------------------------------
 * |  4  |May 17, 2005   | Hardware info writing to file updated        | Anand       |
 * ------------------------------------------------------------------------------------
 * |  3  |May 10, 2005   | isEncrypted method added for encryption fix  | Anand       |
 * ------------------------------------------------------------------------------------
 * |  2  |May 05, 2005   | getPacket()/LOAD/SAVE of HW info added       | Bindu       |
 * ------------------------------------------------------------------------------------
 * |  1  |Apr 27, 2005   | Hardware Model info added                    | Anand       |
 * ------------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004   | Created	                                    | abhijit     |
 * ------------------------------------------------------------------------------------
 */


package com.meshdynamics.meshviewer.imcppackets;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import com.meshdynamics.meshviewer.imcppackets.helpers.IMCPInfoIDS;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.HardwareInfo;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.Bytes;

public class ApHwInfoPacket extends IMCPPacket implements HardwareInfo {

	private static final int BASE_PACKET_LENGTH = 24; //20+4
	
	private int                 info_id;
	private int                 info_length;
	private short 				firmwareMajorVersion;
	private short 				firmwareMinorVersion;
	private short 				firmwareVariantVersion;
	private String 				apName;
	private String 				hardwareModel;
	private short 				hardwareType;
	private InterfaceData       dsInterface;
	private short 				wmCount;
	private InterfaceData[]     wmInterfaces;
	private InterfaceData     	monInterface;
	
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean  readPacket(BufferReader buffer) {
		try{
			info_id                     = buffer.readShort();
			info_length                 = buffer.readShort();
			dsMacAddress 				= buffer.readMacAddress();
			firmwareMajorVersion		= buffer.readByte();
			firmwareMinorVersion		= buffer.readByte();
			firmwareVariantVersion		= buffer.readByte();
			apName 						= buffer.readString();
			hardwareModel				= buffer.readString();
			dsInterface					= new InterfaceData();
			dsInterface.setIfType(buffer.readByte());
			dsInterface.setChannel(buffer.readByte());
			dsInterface.setName(buffer.readString());
			
			wmCount 		= buffer.readByte();			
			wmInterfaces 	= new InterfaceData[wmCount];
		
			for(int wmNo=0; wmNo < wmCount; wmNo++){
				wmInterfaces[wmNo] = new InterfaceData();
				wmInterfaces[wmNo].setWmMacAddr(buffer.readMacAddress());
				wmInterfaces[wmNo].setIfType(buffer.readByte());
				wmInterfaces[wmNo].setIfSubType(buffer.readByte());
				wmInterfaces[wmNo].setChannel(buffer.readByte());				
				wmInterfaces[wmNo].setName(buffer.readString());
			
			}
			
	        try {
				if(((firmwareMajorVersion == 2 && firmwareMinorVersion == 4 && firmwareVariantVersion >= 96) || 
					(firmwareMajorVersion == 2 && firmwareMinorVersion > 4) || (firmwareMajorVersion > 2))) {
					monInterface = new InterfaceData();
					monInterface.setWmMacAddr(buffer.readMacAddress());
					monInterface.setIfType(buffer.readByte());
					monInterface.setIfSubType(buffer.readByte());
					monInterface.setName(buffer.readString());
				}
	        } catch(Exception e) {
	        	monInterface = null;
	        }
			return true;
		}catch (Exception e){
//			e.printStackTrace();
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter buffer) {
		
		buffer.writeShort(IMCPInfoIDS.IMCP_AP_HARDWARE_INFO_ID);
		buffer.writeShort(getPacketLength());
		buffer.writeMacAddress(dsMacAddress);
		buffer.writeByte(firmwareMajorVersion);
		buffer.writeByte(firmwareMinorVersion);
		buffer.writeByte(firmwareVariantVersion);
		buffer.writeString(apName);
		buffer.writeString(hardwareModel);
		buffer.writeByte((short)dsInterface.getIfType());
		buffer.writeByte((short)dsInterface.getChannel());
		buffer.writeString(dsInterface.getName());
		
		buffer.writeByte(wmCount);
		for(int i = 0; i < wmCount; i++) {
			buffer.writeMacAddress(wmInterfaces[i].getMacAddr());
			buffer.writeByte((short)wmInterfaces[i].getIfType());
			buffer.writeByte((short)wmInterfaces[i].getIfSubType());
			buffer.writeByte((short)wmInterfaces[i].getChannel());
			buffer.writeString(wmInterfaces[i].getName());
		}
		
        boolean scanner = false;
        if(hardwareModel != null || hardwareModel != ""){
        	 String model = hardwareModel.substring(0,hardwareModel.indexOf("-"));
        	 if(String.valueOf(model.charAt(model.length()-1)).equals("5"))
        	 	scanner = true;
        	 else
        	 	scanner = false;
        }
        
		if(((firmwareMajorVersion == 2 && firmwareMinorVersion == 4 && firmwareVariantVersion >= 96) || 
				(firmwareMajorVersion == 2 && firmwareMinorVersion > 4) || (firmwareMajorVersion > 2)) && scanner){

			if(monInterface != null ){
				buffer.writeMacAddress(monInterface.getMacAddr());
				buffer.writeByte((short)monInterface.getIfType());
				buffer.writeByte((short)monInterface.getIfSubType());
				buffer.writeString(monInterface.getName());
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		int len = 		BASE_PACKET_LENGTH 				+
						apName.length()					+
						hardwareModel.length()			+
						dsInterface.getName().length();
		
		for(int i=0; i < wmInterfaces.length; i++) {
			len += 		10 + wmInterfaces[i].getName().length(); 
		}
		
        boolean scanner = false;
        if(hardwareModel != null || hardwareModel != ""){
        	 String model = hardwareModel.substring(0,hardwareModel.indexOf("-"));
        	 if(String.valueOf(model.charAt(model.length()-1)).equals("5"))
        	 	scanner = true;
        	 else
        	 	scanner = false;
        }
		if(((firmwareMajorVersion == 2 && firmwareMinorVersion == 4 && firmwareVariantVersion >= 96) || 
				(firmwareMajorVersion == 2 && firmwareMinorVersion > 4) || (firmwareMajorVersion > 2)) && scanner){

			if(monInterface != null ){
				len +=  9 + monInterface.getName().length();
			}
		}
		
		return len;
	}

	/**
	 * @return Returns the apName.
	 */
	public String getApName() {
		return apName;
	}

	/**
	 * @return Returns the dsType.
	 */
	public InterfaceData getDsInterfaceInfo() {
		return dsInterface;
	}
	
	/**
	 * @return Returns the wmCount.
	 */
	public short getWmCount() {
		return wmCount;
	}
	/**
	 * @param wmCount The wmCount to set.
	 */
	public void setWmCount(short wmCount) {
		this.wmCount = wmCount;
	}

    /**
     * @param in
     * @throws IOException
     */
    public void read(InputStream in) {
	    try {
	    	byte[] fourBytes = new byte[4];
	    	in.read(fourBytes);
	        int len = Bytes.bytesToInt(fourBytes);
	    	byte[] bytes = new byte[len];
	    	in.read(bytes);
	    	BufferReader br = new BufferReader(bytes,len);
	        readPacket(br);
	    } catch(Exception e) {
	    	e.printStackTrace();
	    }
    }

    /**	
     * @param out
     * @throws IOException
     */
    public void write(OutputStream out) {
		try {
			byte[] fourBytes = new byte[4];
			BufferWriter bw = new BufferWriter(getPacketLength());
			getPacket(bw);
			byte[] bytes = bw.getBytes();
			Bytes.intToBytes(bytes.length,fourBytes);
			out.write(fourBytes);
			out.write(bytes);
		} catch (Exception e) {
			Mesh.logException(e);
		}   
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
    public byte getPacketType() {
        return PacketFactory.AP_HW_INFO;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.mesh.HardwareInfo#getWmInterfaceInfo()
     */
    public InterfaceData[] getWmInterfaceInfo() {
        return wmInterfaces;
    }

    /**
	 * @return Returns the hardwareType.
	 */
	public short getHardwareType() {
		return hardwareType;
	}

	/**
	 * @return Returns the majorVersion.
	 */
	public short getFirmwareMajorVersion() {
		return firmwareMajorVersion;
	}

	/**
	 * @return Returns the minorVersion.
	 */
	public short getFirmwareMinorVersion() {
		return firmwareMinorVersion;
	}

	/**
	 * @return Returns the variantVersion.
	 */
	public short getFirmwareVariantVersion() {
		return firmwareVariantVersion;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		
		
	}
	/**
	 * @return Returns the hardwareModel.
	 */
	public String getHardwareModel() {
		return hardwareModel;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.HardwareInfo#getVersionStr()
	 */
	public String getVersionStr() {
		return "" + firmwareMajorVersion + "." + firmwareMinorVersion + "." + firmwareVariantVersion;
	}

	public void setApName(String name) {
		// TODO Auto-generated method stub
		
	}

	public InterfaceData getMonInterface() {
		return monInterface;
	}

	public int getInfo_id() {
		return info_id;
	}

	public void setInfo_id(int info_id) {
		this.info_id = info_id;
	}

	public int getInfo_length() {
		return info_length;
	}

	public void setInfo_length(int info_length) {
		this.info_length = info_length;
	}

}
