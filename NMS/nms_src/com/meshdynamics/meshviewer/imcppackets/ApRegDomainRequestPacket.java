/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApRegDmnCntCodeRequestPacket.java
 * Comments : 
 * Created  : Nov 16, 2005
 * Author   : Mithil Wane
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  1  |Nov 19, 2006 | Misc changes:Changes due to StatusDisplayUI     |Abhishek|
 * ----------------------------------------------------------------------------------
 * | 2  |Feb 16, 2006|Config Sequence Number change                    |Prachiti |
 *  --------------------------------------------------------------------------------
 * |  1   |Feb 08, 2006 | Reg Domain removed							 | Mithil  |
 *  --------------------------------------------------------------------------------
 * |  0  |Nov 16, 2005 | Created                                         | Mithil |
 * --------------------------------------------------------------------------------
**********************************************************************************/
package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.imcppackets.helpers.IMCPInfoIDS;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class ApRegDomainRequestPacket extends RequestResponsePacket {
	
	private static final int PACKET_LENGTH = 12;//12+4
	
	private int    APREG_DOMAIN_REQ_ID;
	private int    APREG_DOMAIN_REQ_LENGTH;

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			APREG_DOMAIN_REQ_ID        = buffer.readShort();
			APREG_DOMAIN_REQ_LENGTH    = buffer.readShort();
			dsMacAddress 		       = buffer.readMacAddress();
			requestResponseType        = buffer.readByte();
			requestResponseID 	       = buffer.readByte();
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter writer) {
		writer.writeShort(IMCPInfoIDS.IMCP_REG_DOMAIN_REQ_RES_INFO_ID);
		writer.writeShort(getPacketLength());
		writer.writeMacAddress(dsMacAddress);
		writer.writeByte(requestResponseType);
		writer.writeByte(requestResponseID);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return 	PacketFactory.AP_REG_DOMAIN_CCODE_REQUEST_PACKET;
	}

	public int getAPREG_DOMAIN_REQ_ID() {
		return APREG_DOMAIN_REQ_ID;
	}

	public void setAPREG_DOMAIN_REQ_ID(int aPREG_DOMAIN_REQ_ID) {
		APREG_DOMAIN_REQ_ID = aPREG_DOMAIN_REQ_ID;
	}

	public int getAPREG_DOMAIN_REQ_LENGTH() {
		return APREG_DOMAIN_REQ_LENGTH;
	}

	public void setAPREG_DOMAIN_REQ_LENGTH(int aPREG_DOMAIN_REQ_LENGTH) {
		APREG_DOMAIN_REQ_LENGTH = aPREG_DOMAIN_REQ_LENGTH;
	}

}
