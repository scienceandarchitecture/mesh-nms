/**
 * MeshDynamics 
 * -------------- 
 * File     : StaInfoPacket.java
 * Comments : 
 * Created  : Jan 31, 2006
 * Author   : Mithil
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ------------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                     | Author      |
 * ------------------------------------------------------------------------------------
 * |  1  |Jan 12, 2007   | isEncrypted method removed                   | Imran   	  |
 * ------------------------------------------------------------------------------------
 * |  0  |Jan 31, 2006   | Created	                                    | Mithil      |
 * ------------------------------------------------------------------------------------
 */


package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.DateTime;
import com.meshdynamics.util.MacAddress;

public class StaInfoPacket extends IMCPPacket {

	private static final int BASE_PACKET_LENGTH = 38;
	
	private int                 stationInfo_id;
	private int                 stationInfo_length;
	private MacAddress 			staMacAddress;
	private MacAddress 			parentwmMacAddress;
	private String 				parentESSID;
	private short 				authState;
	private short				bclient;
	private short 				isIMCP;
	private int					listenInterval;
	private short				capability;
	private short				keyIndex;
	private long				bytesReceived;
	private long				bytesSent;
	private long				activeTime;
	private String				lastResponseRcvd;
    
	
	public StaInfoPacket(){
		staMacAddress 		= null;
		parentwmMacAddress  = null;
		parentESSID 		= null;
		authState 			= 0;
		bclient				= 0;
		isIMCP				= 0;
		listenInterval		= 0;
		capability 			= 0;
		keyIndex			= 0;
		bytesReceived		= 0;
		bytesSent 			= 0;
		activeTime 			= 0;
		lastResponseRcvd	= null;

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean  readPacket(BufferReader buffer) {
		try{
			stationInfo_id      = buffer.readShort();
			stationInfo_length  = buffer.readShort();
			dsMacAddress 		= buffer.readMacAddress();
			staMacAddress		= buffer.readMacAddress();
			parentwmMacAddress  = buffer.readMacAddress();
			parentESSID			= buffer.readString();
			authState			= buffer.readByte();
			bclient				= buffer.readByte();
			isIMCP				= buffer.readByte();
			listenInterval		= buffer.readShort();
			capability			= buffer.readByte();
			keyIndex			= buffer.readByte();
			bytesReceived		= buffer.readInt();
			bytesSent			= buffer.readInt();
			activeTime			= buffer.readInt();
			lastResponseRcvd	= DateTime.getDateTime();
			return true;
		}catch (Exception e){
			Mesh.logException(e);
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter buffer) {
		buffer.writeShort(stationInfo_id);
		buffer.writeShort(stationInfo_length);
		buffer.writeMacAddress(dsMacAddress);
		buffer.writeMacAddress(staMacAddress);
		buffer.writeMacAddress(parentwmMacAddress);
		buffer.writeString(parentESSID);
		buffer.writeByte(authState);
		buffer.writeByte(bclient);
		buffer.writeByte(isIMCP);
		buffer.writeShort(listenInterval);
		buffer.writeByte(capability);
		buffer.writeByte(keyIndex);
		buffer.writeInt(bytesReceived);
		buffer.writeInt(bytesSent);
		buffer.writeInt(activeTime);
		
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		int len = 		BASE_PACKET_LENGTH 				+
						parentESSID.length()			;
						
		return len;
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
    public byte getPacketType() {
        return PacketFactory.STA_INFO_PACKET;
    }

    /* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * @return Returns the activeTime.
	 */
	public long getActiveTime() {
		return activeTime;
	}

	/**
	 * @return Returns the authState.
	 */
	public short getAuthState() {
		return authState;
	}

	/**
	 * @return Returns the bclient.
	 */
	public short getBclient() {
		return bclient;
	}

	/**
	 * @return Returns the bytesReceived.
	 */
	public long getBytesReceived() {
		return bytesReceived;
	}

	/**
	 * @return Returns the bytesSent.
	 */
	public long getBytesSent() {
		return bytesSent;
	}

	/**
	 * @return Returns the capability.
	 */
	public short getCapability() {
		return capability;
	}

	/**
	 * @return Returns the isICMP.
	 */
	public short getIsIMCP() {
		return isIMCP;
	}

	/**
	 * @return Returns the keyIndex.
	 */
	public short getKeyIndex() {
		return keyIndex;
	}

	/**
	 * @return Returns the listenInterval.
	 */
	public int getListenInterval() {
		return listenInterval;
	}

	/**
	 * @return Returns the parentESSID.
	 */
	public String getParentESSID() {
		return parentESSID;
	}

	/**
	 * @return Returns the parentwmMacAddress.
	 */
	public MacAddress getParentwmMacAddress() {
		return parentwmMacAddress;
	}

	/**
	 * @return Returns the staMacAddress.
	 */
	public MacAddress getStaMacAddress() {
		return staMacAddress;
	}

	/**
	 * @return Returns the lastResponseRcvd.
	 */
	public String getLastResponseRcvd() {
		return lastResponseRcvd;
	}

}
