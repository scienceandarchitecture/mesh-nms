package com.meshdynamics.meshviewer.imcppackets;

import java.util.ArrayList;
import java.util.List;

import com.meshdynamics.meshviewer.imcppackets.helpers.IMCP_MacInfo;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;


public class MacAddressRequestResponsePacket extends IMCPPacket{
	private static final int BASE_PACKET_LENGTH	=13;
	private int                id;
	private int                length;
	private short              rCount;
	private short              vCount;
	private short              appCount;
	private List<IMCP_MacInfo> raddr;
	private List<IMCP_MacInfo> vaddr;
	private List<IMCP_MacInfo> app_addr;
	@Override
	public boolean readPacket(BufferReader buffer) {
		try {
			/**/			
			id         = buffer.readShort();
			length     = buffer.readShort();
			dsMacAddress = buffer.readMacAddress();
			rCount     = buffer.readByte();
			vCount     = buffer.readByte();
			appCount   = buffer.readByte();
			raddr=new ArrayList<IMCP_MacInfo>();		
			for(short i=0;i<rCount;i++){
				IMCP_MacInfo macInfo=new IMCP_MacInfo();
				macInfo.setIfName(buffer.readString());
				macInfo.setMacAddress(buffer.readMacAddress());
				raddr.add(macInfo);
			}
			vaddr=new ArrayList<IMCP_MacInfo>();
			for(short i=0;i<vCount;i++){
				IMCP_MacInfo macInfo=new IMCP_MacInfo();
				macInfo.setIfName(buffer.readString());
				macInfo.setMacAddress(buffer.readMacAddress());
				vaddr.add(macInfo);
			}
			app_addr=new ArrayList<IMCP_MacInfo>();	
			for(short i=0;i<appCount;i++){
				IMCP_MacInfo macInfo=new IMCP_MacInfo();
				macInfo.setIfName(buffer.readString());
				macInfo.setMacAddress(buffer.readMacAddress());
				app_addr.add(macInfo);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}		
		return true;
	}

	@Override
	public void getPacket(BufferWriter buffer) {
		
		
	}

	@Override
	public int getPacketLength() {
		int length=BASE_PACKET_LENGTH;
		for(IMCP_MacInfo macInfo:raddr){
			length+=6;
			length=macInfo.getIfName().length();
		}
		for(IMCP_MacInfo macInfo:vaddr){
			length+=6;
			length=macInfo.getIfName().length();
		}
		for(IMCP_MacInfo macInfo:app_addr){
			length+=6;
			length=macInfo.getIfName().length();
		}
		return length;
	}

	@Override
	public byte getPacketType() {		
		return PacketFactory.IMCP_MAC_ADDR_REQUEST_RESPONSE;
	}

	@Override
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	public short getrCount() {
		return rCount;
	}

	public void setrCount(short rCount) {
		this.rCount = rCount;
	}

	public short getvCount() {
		return vCount;
	}

	public void setvCount(short vCount) {
		this.vCount = vCount;
	}

	public short getAppCount() {
		return appCount;
	}

	public void setAppCount(short appCount) {
		this.appCount = appCount;
	}

	public List<IMCP_MacInfo> getRaddr() {
		return raddr;
	}

	public void setRaddr(List<IMCP_MacInfo> raddr) {
		this.raddr = raddr;
	}

	public List<IMCP_MacInfo> getVaddr() {
		return vaddr;
	}

	public void setVaddr(List<IMCP_MacInfo> vaddr) {
		this.vaddr = vaddr;
	}

	public List<IMCP_MacInfo> getApp_addr() {
		return app_addr;
	}

	public void setApp_addr(List<IMCP_MacInfo> app_addr) {
		this.app_addr = app_addr;
	}	

}
