/*
 * Created on Oct 21, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 * ------------------------------------------------------------------------------------
 * | No  |Date         | Comment                                         | Auther  	  |
 * ------------------------------------------------------------------------------------
 * |  2  |Feb  6, 2007 | is Encrypetd method added                       | Imran   	  |
 * ------------------------------------------------------------------------------------
 * |  1  |Jan 12, 2007 | isEncrypted method removed                      | Imran  	  |
 * ------------------------------------------------------------------------------------
 */
package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

/**
 * @author Prachiti Gaikwad
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ApRestoreDefaultsRequestPacket  extends IMCPPacket {

	private static final int PACKET_LENGTH = 10;//6 + 2 + 2
	
	private int   apRestoreInfo_id;
	private int   apRestoreInfo_length;
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			apRestoreInfo_id     = buffer.readShort();
			apRestoreInfo_length = buffer.readShort();
			dsMacAddress         = buffer.readMacAddress();	
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		
		return true;
	}

	public void getPacket(BufferWriter writer){
		writer.writeShort(apRestoreInfo_id);
		writer.writeShort(apRestoreInfo_length);
	    writer.writeMacAddress(dsMacAddress);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {		
		return PACKET_LENGTH ;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
	   return 	PacketFactory.AP_RESTORE_DEFAULTS_REQUEST;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#isEncrypted()
	 */
	public boolean isEncrypted() {
		// TODO Auto-generated method stub
		return false;
	}

    public boolean canProcessPacket() {
    	return false;
    }

	public int getApRestoreInfo_id() {
		return apRestoreInfo_id;
	}

	public void setApRestoreInfo_id(int apRestoreInfo_id) {
		this.apRestoreInfo_id = apRestoreInfo_id;
	}

	public int getApRestoreInfo_length() {
		return apRestoreInfo_length;
	}

	public void setApRestoreInfo_length(int apRestoreInfo_length) {
		this.apRestoreInfo_length = apRestoreInfo_length;
	}
	
    
  }
