package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class Firmware_SW_UPDATE_REQUEST extends IMCPPacket{
	
    private static final int PACKET_LENGTH = 11; 
	
    public final static short   GET_KEY            = 0;
    public final static short   SET_KEY            = 1;
    public final static short   UPGRADE_INIT       = 2;
    public final static short   UPGRADE_STARTING   = 3;
    public final static short   IMAGE_COPY_FAILED  = 4;
    public final static short   UPGRADE_FAILED     = 5;
    public final static short   UNKNOWN_ACTION     = 6;
    public final static short   UPGRADE_TIMEOUT    = 7;
    
    private int     firmware_update_Id;
    private int     firmware_update_length;
    private short   action;
    private String  userName;
    private String  filePath;
    private short   OSType;
	
	
	@Override
	public boolean readPacket(BufferReader buffer) {
		firmware_update_Id     = buffer.readShort();
		firmware_update_length = buffer.readShort();
		dsMacAddress           = buffer.readMacAddress();
		return true;
	}

	@Override
	public void getPacket(BufferWriter buffer) {
		if(action == GET_KEY){
		buffer.writeShort(firmware_update_Id);
		buffer.writeShort(firmware_update_length);
		buffer.writeMacAddress(dsMacAddress);
		buffer.writeByte(action);
		}else{
			buffer.writeShort(firmware_update_Id);
			buffer.writeShort(firmware_update_length);
			buffer.writeMacAddress(dsMacAddress);
			buffer.writeByte(action);
	    	buffer.writeString(userName);
		    buffer.writeString(filePath);
		    //buffer.writeByte(OSType);
		}
	}

	@Override 
	public int getPacketLength() {
		if(getAction()== GET_KEY)
		   return PACKET_LENGTH;
		else
		 return PACKET_LENGTH+userName.length()+filePath.length()+18;
	}

	@Override
	public byte getPacketType() {		
		return PacketFactory.IMCP_SW_UPDATE_REQUEST;
	}

	@Override
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub		
	}

	public int getFirmware_update_Id() {
		return firmware_update_Id;
	}

	public void setFirmware_update_Id(int firmware_update_Id) {
		this.firmware_update_Id = firmware_update_Id;
	}

	public int getFirmware_update_length() {
		return firmware_update_length;
	}

	public void setFirmware_update_length(int firmware_update_length) {
		this.firmware_update_length = firmware_update_length;
	}

	public short getAction() {
		return action;
	}

	public void setAction(short action) {
		this.action = action;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public short getOSType() {
		return OSType;
	}

	public void setOSType(short oSType) {
		OSType = oSType;
	}

}
