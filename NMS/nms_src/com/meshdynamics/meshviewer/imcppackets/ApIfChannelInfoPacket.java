/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApIfChannelInfoPacket
 * Comments : AccessPoint COnfiguration ApIfChannel Info Packet 
 * Created  : Dec 16, 2005
 * Author   : Sneha Puranam
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  1  |Nov 27, 2006 | clean-up					  			         |  Bindu |
 * --------------------------------------------------------------------------------
 * |  0  |Dec 16, 2005 | Created                                         | Sneha  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import java.util.Vector;

import com.meshdynamics.meshviewer.imcppackets.helpers.ApIfChannelInfo;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.MacAddress;

public class ApIfChannelInfoPacket extends IMCPPacket {


	private int     apInfo_id;
	private int     apInfo_length;
	private String 	interfaceName;
	private short	channelCount;
	private Vector<ApIfChannelInfo>	tblChannelList;
	private int 	packetLength;	//calculated in readPacket
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader reader) {
	    packetLength 	= 0;
	    apInfo_id       = reader.readShort();           packetLength += 2;  
	    apInfo_length   = reader.readShort();           packetLength += 2;
		dsMacAddress	= reader.readMacAddress();		packetLength += MacAddress.GetLength();	
		interfaceName	= reader.readString();			packetLength +=	interfaceName.length() + 1;
		channelCount	= reader.readByte();			packetLength += 1;
		
		tblChannelList = new Vector<ApIfChannelInfo>();
		for(int i=0;i<channelCount;i++){
			ApIfChannelInfo	channelListInfo 	= new ApIfChannelInfo();
			channelListInfo.channelNumber		= reader.readByte();		packetLength += 1;
			channelListInfo.channelFrequency	= reader.readInt();			packetLength += 4;
			channelListInfo.flags				= reader.readInt();			packetLength += 4;
			channelListInfo.maxRegPower			= reader.readByte();		packetLength += 1;
			channelListInfo.maxPower			= reader.readByte();		packetLength += 1;
			channelListInfo.minPower			= reader.readByte();		packetLength += 1;
			tblChannelList.add(i,channelListInfo);
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter writer) {
        writer.writeShort(apInfo_id);
        writer.writeShort(apInfo_length);
	    writer.writeMacAddress(dsMacAddress);
	    writer.writeString(interfaceName);
	    writer.writeByte(channelCount);
		
	    for(int i=0;i<tblChannelList.size();i++) {
	        
	        ApIfChannelInfo	channelListInfo = (ApIfChannelInfo) tblChannelList.get(i);
	        
	        writer.writeByte(channelListInfo.channelNumber);
	        writer.writeInt(channelListInfo.channelFrequency);	
			writer.writeInt(channelListInfo.flags);
			writer.writeByte(channelListInfo.maxRegPower);
			writer.writeByte(channelListInfo.maxPower);
			writer.writeByte(channelListInfo.minPower);
		}
	    
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return packetLength;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.AP_IF_CHANNEL_INFO_PACKET;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
	}

	/**
	 * @return Returns the interfaceName.
	 */
	public String getInterfaceName() {
		return interfaceName;
	}

	public int getChannelCount() {
		return tblChannelList.size();
	}
	
	public ApIfChannelInfo getChannelInfo(int index) {
		return (ApIfChannelInfo) tblChannelList.get(index);
	}

	public int getApInfo_id() {
		return apInfo_id;
	}

	public void setApInfo_id(int apInfo_id) {
		this.apInfo_id = apInfo_id;
	}

	public int getApInfo_length() {
		return apInfo_length;
	}

	public void setApInfo_length(int apInfo_length) {
		this.apInfo_length = apInfo_length;
	}

	
	
}
