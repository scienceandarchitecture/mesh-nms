/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApConfigInfoPacket.java
 * Comments : AccessPoint COnfiguration Info Packet IMCP(#11)
 * Created  : Sep 24, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * | 27  |Oct 30, 2007 | getters and setters added for stayAwakeCount    |Imran   |
 * --------------------------------------------------------------------------------
 * | 26  |Oct 30, 2007 | getters and setters added for bridgeAgeingTime  |Imran   |
 * --------------------------------------------------------------------------------
 * | 25  |Oct 30, 2007 | setInterfaceCount modified                      |Imran   |
 --------------------------------------------------------------------------------
 * | 24  |May 03, 2007 | setInterfaceCount added                         |Abhishek|
 --------------------------------------------------------------------------------
 * | 23  |Apr 27, 2007 | getWirelessCount  removed                       |Abhishek|
 --------------------------------------------------------------------------------
 * | 22  |Apr 26, 2007 | getBridgeAgeingTime added                       | Imran  |
 --------------------------------------------------------------------------------
 * | 21  |Apr 26, 2007 | getStayAwakeCount added                         | Imran  |
 --------------------------------------------------------------------------------
 * | 20  |Apr 26, 2007 | getLocationAwarenessScanInterval  added         | Imran  |
 *  --------------------------------------------------------------------------------
 * | 19  |Mar 12, 2007 | getWirelessIfCount added			 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 18  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * | 17  |Nov 27, 2006 | clean-up					  			         |  Bindu |
 * --------------------------------------------------------------------------------
 * | 16  |Nov 8 , 2006 |  Latitude/Longitude set to 0 by default         | Imran  |
 * --------------------------------------------------------------------------------
 * | 15  | May 25,2006 | Changes for Mobility Settings		       	     | Bindu  |
 * --------------------------------------------------------------------------------
 * | 14  |Jan 24, 2006 | Restore old boards								 | Mithil |
 *  --------------------------------------------------------------------------------
 * | 13  |Sep 20, 2005 | Default Essid set 		 	 					 |Prachiti|
 * --------------------------------------------------------------------------------
 * | 12  |Sep 20, 2005 | getInterfaceByIndex added.	 					 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 11  |Sep 09, 2005 | getInterfaceByName added.	 					 |Abhijit |
 * --------------------------------------------------------------------------------
 * | 10  |May 20, 2005 | FCC,ETSI dsTxrate & dsTxPower added to config.	 | Anand  |
 * --------------------------------------------------------------------------------
 * |  9  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand  |
 * --------------------------------------------------------------------------------
 * |  8  |May 06, 2005 | Added Description, Latitude & Longitude fields  | Bindu  |
 * --------------------------------------------------------------------------------
 * |  7  |May 05, 2005 | checking for polling pkt in processPacket       | Bindu  |
 * --------------------------------------------------------------------------------
 * |  6  |Apr 27, 2005 | Batch Configure-CopyPacket Bug fixed            | Anand  |
 * --------------------------------------------------------------------------------
 * |  5  |Apr 20, 2005 | Added wmIfInfo for WM interfaces; 				 | Bindu  |
 * |     |             | Reading/Wrting WEP/PSK passPhrases logic added  |  	  |
 * --------------------------------------------------------------------------------
 * |  4  |Apr 13, 2005 | Added calculation of Security Length            | Bindu  |
 * --------------------------------------------------------------------------------
 * |  3  |Mar 01, 2005 | Changes for Vlan & If Sec in Property Window    | Anand  |
 * --------------------------------------------------------------------------------
 * |  2  |Feb 11, 2005 | Change Detection-PropertyUI changes             | Anand  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 10, 2005 | verifyUpdates Func added for change detection   | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import com.meshdynamics.meshviewer.imcppackets.helpers.InterfaceAdvanceInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.InterfaceConfigInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.SecurityInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.WEPSecurityInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.MacAddress;

public class ApConfigInfoPacket extends RequestResponsePacket {

	private static final int BASE_PACKET_LENGTH = 64;//64+4
	private int                     NODE_INFO_ID;
	private int                     NODE_INFO_LENGTH;
	private MacAddress 				preferredParent;
	private short[] 				signalMap;
	private short 					heartbeatInterval;
	private short 					heartbeatMissCount;
	private short 					hopCost;
	private short 					maxAllowableHops;
	private short 					lasi;
	private short 					crThreshold;
	private short  					interfaceCount;
	private String 					apName  	= "";
	private String 					essid		= "StructuredMesh";
	private String 					description	= "";
	private String 					latitude 	= "0";
	private String 					longitude 	= "0";
	private short					fcc;
	private short					etsi;
	private long					dsTxRate;
	private short					dsTxPower;
	private long 					rtsThreshold;
	private long 					fragThreshold;
	private long 					beaconInterval;
	private short 					dynamicChannelAlloc;
	private short        			stayAwakeCount;
	private short        			bridgeAgeingTime;
	
	private Vector<InterfaceConfigInfo>	interfaces;
	private Vector<InterfaceConfigInfo>	wmIfInfo;
	
    public static final short USAGE_TYPE_DS		= 0;
    public static final short USAGE_TYPE_WM		= 1;
    public static final short USAGE_TYPE_PMON	= 2;
    public static final short USAGE_TYPE_AMON	= 3;
    
    public ApConfigInfoPacket() {
        signalMap 			= new short[8];
        interfaces 			= new Vector<InterfaceConfigInfo>();
        wmIfInfo			= new Vector<InterfaceConfigInfo>();
        preferredParent 	= new MacAddress();
        heartbeatInterval	= 5;
        heartbeatMissCount  = 5;
        crThreshold 		= 30;
        lasi				= 0;
        maxAllowableHops	= 6;
        dsTxPower 			= 100;
		dsTxRate			= 0; 
		fcc					= 1;
		etsi				= 1;
		rtsThreshold 		= 2347;
		fragThreshold 		= 2346;
		beaconInterval	 	= 100;
		stayAwakeCount	 	= 500;
		bridgeAgeingTime	= 0;
    }
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean readPacket(BufferReader buffer) {
		
		try{
			NODE_INFO_ID            =   buffer.readShort();
			NODE_INFO_LENGTH        =   buffer.readShort();
			dsMacAddress 			=  	buffer.readMacAddress(); 
			requestResponseType 	=	buffer.readByte(); 
	        requestResponseID  		=  	buffer.readByte();
	        preferredParent   		= 	buffer.readMacAddress();
	        for(int i=0;i<8;i++){
	        	signalMap[i] = buffer.readByte();
	        }
	        heartbeatInterval  		= 	buffer.readByte(); 
	        heartbeatMissCount  	= 	buffer.readByte();
	        hopCost 				= 	buffer.readByte(); 
	        maxAllowableHops  		= 	buffer.readByte(); 
	        lasi					= 	buffer.readByte(); 
	        crThreshold 			= 	buffer.readByte(); 
	        apName 					= 	buffer.readString();
	        essid  					=   buffer.readString();
	        description				=   buffer.readString();
	        latitude				= 	buffer.readString();
	        longitude				= 	buffer.readString();
	        rtsThreshold 			=   buffer.readInt();
	        fragThreshold 			=   buffer.readInt();
	        beaconInterval 			=	buffer.readInt();
	        dynamicChannelAlloc  	=   buffer.readByte();
	        stayAwakeCount 			=   buffer.readByte();
	        bridgeAgeingTime		=   buffer.readByte();
	        fcc						=   buffer.readByte();
	        etsi					=   buffer.readByte();
	        dsTxRate				=   buffer.readInt();
	        dsTxPower				=   buffer.readByte();
	        interfaceCount 			=	buffer.readByte();
	        
	        InterfaceConfigInfo inf = null;
	        for(int count=0; count < interfaceCount; count++){
	        	inf = new InterfaceConfigInfo();
	        	inf.read(buffer);

				if(inf.getMediumType() 	== Mesh.PHY_TYPE_802_11) {

					if(	inf.getMediumSubType()!= Mesh.PHY_SUB_TYPE_INGNORE && 
						inf.getUsageType()	== Mesh.PHY_USAGE_TYPE_WM) {
						wmIfInfo.add(inf);
					}
					
				}
	        
	        	interfaces.add(inf);
	        }

	        /*
	        wmIfInfo = new InterfaceConfigInfo[wmCount];
	        wmCount = 0;
			InterfaceConfigInfo[] ifInfo = getInterfaces();
			for(int i=0; i < ifInfo.length; i++) {
				if(ifInfo[i].getMediumType() 	== Mesh.PHY_TYPE_802_11 && 
					ifInfo[i].getMediumSubType()!= Mesh.PHY_SUB_TYPE_INGNORE && 
					ifInfo[i].getUsageType()	== Mesh.PHY_USAGE_TYPE_WM) { 
						wmIfInfo[wmCount++] = ifInfo[i];
				}
			}
	        */
	        
			return true;
		} catch(Exception e){
			Mesh.logException(e);
			return false;
		}
	}


	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter buffer) {
		try {
			buffer.writeShort       (NODE_INFO_ID);
			buffer.writeShort       (NODE_INFO_LENGTH);
			buffer.writeMacAddress	(dsMacAddress);
			buffer.writeByte		(requestResponseType);
			buffer.writeByte		(requestResponseID);
			buffer.writeMacAddress	(preferredParent);					
			for(int i=0;i<8;i++){
				buffer.writeByte	(signalMap[i]);				
			}
			
			buffer.writeByte		(heartbeatInterval);
			buffer.writeByte		(heartbeatMissCount);
			buffer.writeByte		(hopCost);
			buffer.writeByte		(maxAllowableHops);
			buffer.writeByte		(lasi);
			buffer.writeByte		(crThreshold);
			buffer.writeString		(apName);
			buffer.writeString		(essid);
			buffer.writeString		(description);
			buffer.writeString		(latitude);
			buffer.writeString		(longitude);
			buffer.writeInt			(rtsThreshold);
			buffer.writeInt			(fragThreshold);
			buffer.writeInt			(beaconInterval);
				
			buffer.writeByte		(dynamicChannelAlloc);
			buffer.writeByte		(stayAwakeCount);  	
			buffer.writeByte		(bridgeAgeingTime);
			buffer.writeByte		(fcc);
			buffer.writeByte		(etsi);
			buffer.writeInt			(dsTxRate);
	        buffer.writeByte		(dsTxPower);
			
			buffer.writeByte		((short)interfaces.size());
			
			Iterator<InterfaceConfigInfo> it = interfaces.iterator();
			while(it.hasNext()) {
				InterfaceConfigInfo inf = (InterfaceConfigInfo)it.next();
	        	inf.write(buffer);
			}
		} catch(Exception  e) {
			Mesh.logException(e);
			e.printStackTrace();
		}
	}
	
	/**
	 * @return Returns the apName.
	 */
	public String getApName() {
		return apName;
	}
	/**
	 * @param apName The apName to set.
	 */
	public void setApName(String apName) {
		this.apName = apName;
	}

	/**
	 * @return Returns the beaconInterval.
	 */
	public long getBeaconInterval() {
		return beaconInterval;
	}
	/**
	 * @param beaconInterval The beaconInterval to set.
	 */
	public void setBeaconInterval(int beaconInterval) {
		this.beaconInterval = beaconInterval;
	}

	/**
	 * @return Returns the crThreshold.
	 */
	public short getCrThreshold() {
		return crThreshold;
	}
	/**
	 * @param crThreshold The crThreshold to set.
	 */
	public void setCrThreshold(short crThreshold) {
		this.crThreshold = crThreshold;
	}

	/**
	 * @return Returns the dynamicChannelAlloc.
	 */
	public short getDynamicChannelAlloc() {
		return dynamicChannelAlloc;
	}
	/**
	 * @param dynamicChannelAlloc The dynamicChannelAlloc to set.
	 */
	public void setDynamicChannelAlloc(short dynamicChannelAlloc) {
		this.dynamicChannelAlloc = dynamicChannelAlloc;
	}
	/**
	 * @return Returns the essid.
	 */
	public String getEssid() {
		return essid;
	}
	/**
	 * @param essid The essid to set.
	 */
	public void setEssid(String essid) {
		this.essid = essid;
	}

	/**
	 * @return Returns the fragThreshold.
	 */
	public long getFragThreshold() {
		return fragThreshold;
	}
	/**
	 * @param fragThreshold The fragThreshold to set.
	 */
	public void setFragThreshold(long fragThreshold) {
		this.fragThreshold = fragThreshold;
	}
	
	/**
	 * @return Returns the heartbeatInterval.
	 */
	public short getHeartbeatInterval() {
		return heartbeatInterval;
	}
	/**
	 * @param heartbeatInterval The heartbeatInterval to set.
	 */
	public void setHeartbeatInterval(short heartbeatInterval) {
		this.heartbeatInterval = heartbeatInterval;
	}
	/**
	 * @return Returns the heartbeatMissCount.
	 */
	public short getHeartbeatMissCount() {
		return heartbeatMissCount;
	}
	/**
	 * @param heartbeatMissCount The heartbeatMissCount to set.
	 */
	public void setHeartbeatMissCount(short heartbeatMissCount) {
		this.heartbeatMissCount = heartbeatMissCount;
	}
	/**
	 * @return Returns the hopCost.
	 */
	public short getHopCost() {
		return hopCost;
	}
	/**
	 * @param hopCost The hopCost to set.
	 */
	public void setHopCost(short hopCost) {
		this.hopCost = hopCost;
	}

	/**
	 * @return Returns the maxAllowedHops.
	 */
	public short getMaxAllowableHops() {
		return maxAllowableHops;
	}
	/**
	 * @param maxAllowedHops The maxAllowedHops to set.
	 */
	public void setMaxAllowableHops(short maxAllowableHops) {
		this.maxAllowableHops = maxAllowableHops;
	}
	/**
	 * @return Returns the preferredParent.
	 */
	public MacAddress getPreferredParent() {
		return preferredParent;
	}
	/**
	 * @param preferredParent The preferredParent to set.
	 */
	public void setPreferredParent(MacAddress preferredParent) {
		this.preferredParent = preferredParent;
	}
	/**
	 * @return Returns the rtsThreshold.
	 */
	public long getRtsThreshold() {
		return rtsThreshold;
	}
	/**
	 * @param rtsThreshold The rtsThreshold to set.
	 */
	public void setRtsThreshold(long rtsThreshold) {
		this.rtsThreshold = rtsThreshold;
	}
	/**
	 * @return Returns the signalMap.
	 */
	public short[] getSignalMap() {
		return signalMap;
	}
	/**
	 * @param signalMap The signalMap to set.
	 */
	public void setSignalMap(short[] signalMap) {
		this.signalMap = signalMap;
	}
	

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength(){
		int len = BASE_PACKET_LENGTH 	+ apName.length() 	+ essid.length() + 
				  description.length() 	+ latitude.length() + longitude.length();
		Iterator<InterfaceConfigInfo> it = interfaces.iterator();
		while(it.hasNext()) {
			InterfaceConfigInfo inf = (InterfaceConfigInfo)it.next();
			if(inf.getInterfaceName().equals("eth0") || inf.getInterfaceName().equals("eth1")){
				len += 7;
				len += inf.getInterfaceName().length();
			}else{
			len += 27;
			len += inf.getInterfaceName().length();
			len += inf.getEssid().length();
			len += inf.getDcaListCount();			
			len += SecurityInfo.SECURITY_LENGTH;
			
			SecurityInfo secInfo = inf.getSecurityInfo();
			if(secInfo != null)	{		
			short enabled = secInfo.getWEPEnabled();
			
			if(enabled == 1) {
				if(secInfo.getWEPInfo().getWEPStrength() == WEPSecurityInfo.WEP_64_BIT) {
					len += SecurityInfo.WEP_LENGTH + WEPSecurityInfo.WEP_64_BIT_KEY_LENGTH;					
				} else if(secInfo.getWEPInfo().getWEPStrength() == WEPSecurityInfo.WEP_128_BIT) {
					len += SecurityInfo.WEP_LENGTH + WEPSecurityInfo.WEP_128_BIT_KEY_LENGTH;
				}
			}
			
			len += SecurityInfo.PSK_LENGTH + secInfo.getRSNPSKInfo().getPSKKey().length;
			len += SecurityInfo.RAD_LENGTH + secInfo.getRADInfo().getRADKey().length();
			}
			InterfaceAdvanceInfo advanceInfo= inf.getInterfaceAdvanceInfo();
			if(advanceInfo != null){
				if(inf.getMediumSubType() == 14)
			    len +=InterfaceAdvanceInfo.INTERFACE_ADVANCEINFO_LENGTH_FOR_N_AC;
				 if(inf.getMediumSubType() == 11)
					  len +=InterfaceAdvanceInfo.INTERFACE_ADVANCEINFO_LENGTH_FOR_AC;
				if(inf.getMediumSubType() == 10 || 
						inf.getMediumSubType()== 12||
								inf.getMediumSubType() ==13){
					  len +=InterfaceAdvanceInfo.INTERFACE_ADVANCEINFO_LENGTH_FOR_N;
			}
		}//end of 
			}}
		return len;
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
	public byte getPacketType() {
        return PacketFactory.AP_CONFIGURATION_INFO;
    }
	
	/**
	 * @return Returns the interfaces.
	 */
	public InterfaceConfigInfo[] getInterfaces() {
		InterfaceConfigInfo[] infs = new InterfaceConfigInfo[interfaces.size()];
		for(int i=0; i< infs.length; i++) {
			infs[i] = (InterfaceConfigInfo) interfaces.get(i);
		}
		return infs;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "AP-Configuration Packet";
	}

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getLatitude() {
		return latitude;
	}
	
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	public String getLongitude() {
		return longitude;
	}
	
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		ApConfigInfoPacket apConfigInfoNew 		= (ApConfigInfoPacket)packet;
		apConfigInfoNew.requestResponseType		=  this.requestResponseType;
		apConfigInfoNew.requestResponseID		=  this.requestResponseID;
		apConfigInfoNew.preferredParent.setBytes(this.preferredParent.getBytes());
		apConfigInfoNew.signalMap  				= new short[8];
		for(int i=0; i<8; i++) {
			apConfigInfoNew.signalMap[i]  				=	this.signalMap[i];
		}
		apConfigInfoNew.heartbeatInterval		=	this.heartbeatInterval;
		apConfigInfoNew.heartbeatMissCount		=	this.heartbeatMissCount;
		apConfigInfoNew.hopCost					=	this.hopCost;
		apConfigInfoNew.maxAllowableHops		=	this.maxAllowableHops;
		apConfigInfoNew.lasi					=	this.lasi;
		apConfigInfoNew.crThreshold				=	this.crThreshold;
		apConfigInfoNew.interfaceCount			=	this.interfaceCount;
		apConfigInfoNew.apName					=	new String(this.apName.getBytes());
		apConfigInfoNew.essid					=   new String(this.essid.getBytes());
		apConfigInfoNew.description				= 	new String(this.description.getBytes());
		apConfigInfoNew.latitude				= 	new String(this.latitude.getBytes());
		apConfigInfoNew.longitude				= 	new String(this.longitude.getBytes());
		apConfigInfoNew.rtsThreshold			=	this.rtsThreshold;
		apConfigInfoNew.fragThreshold			=	this.fragThreshold;
		apConfigInfoNew.beaconInterval			=	this.beaconInterval;
		apConfigInfoNew.dynamicChannelAlloc  	= 	this.dynamicChannelAlloc;
		apConfigInfoNew.stayAwakeCount			=	this.stayAwakeCount;
		apConfigInfoNew.bridgeAgeingTime		=	this.bridgeAgeingTime;
		apConfigInfoNew.fcc						= 	this.fcc;
		apConfigInfoNew.etsi					=	this.etsi;
		apConfigInfoNew.dsTxRate				=	this.dsTxRate;
		apConfigInfoNew.dsTxPower				=	this.dsTxPower;
		
		Enumeration<InterfaceConfigInfo> enm = this.interfaces.elements();
		while(enm.hasMoreElements()) {
			InterfaceConfigInfo ifConfOld 		= (InterfaceConfigInfo)enm.nextElement();
			InterfaceConfigInfo ifConf 			= new InterfaceConfigInfo();
			ifConfOld.copyPacket(ifConf);			
			apConfigInfoNew.interfaces.add(ifConf);
			if(ifConfOld.getMediumType() 	== Mesh.PHY_TYPE_802_11 && 
					ifConfOld.getMediumSubType()!= Mesh.PHY_SUB_TYPE_INGNORE && 
					ifConfOld.getUsageType()	== Mesh.PHY_USAGE_TYPE_WM) { 
					apConfigInfoNew.wmIfInfo.add(ifConf);
			}
		}		
	}

	/**
	 * @return Returns the dsTxPower.
	 */
	public short getDsTxPower() {
		return dsTxPower;
	}
	/**
	 * @param dsTxPower The dsTxPower to set.
	 */
	public void setDsTxPower(short dsTxPower) {
		this.dsTxPower = dsTxPower;
	}
	/**
	 * @return Returns the dsTxRate.
	 */
	public long getDsTxRate() {
		return dsTxRate;
	}
	/**
	 * @param dsTxRate The dsTxRate to set.
	 */
	public void setDsTxRate(long dsTxRate) {
		this.dsTxRate = dsTxRate;
	}
	/**
	 * @return Returns the etsi.
	 */
	public short getETSI() {
		return etsi;
	}
	/**
	 * @param etsi The etsi to set.
	 */
	public void setETSI(short etsi) {
		this.etsi = etsi;
	}
	/**
	 * @return Returns the fcc.
	 */
	public short getFCC() {
		return fcc;
	}
	/**
	 * @param fcc The fcc to set.
	 */
	public void setFCC(short fcc) {
		this.fcc = fcc;
	}
	
	public InterfaceConfigInfo getInterfaceByName(String ifName) {
		
		InterfaceConfigInfo ifInfo = null;
		
		for(int i=0; i< interfaces.size(); i++) {
			ifInfo = (InterfaceConfigInfo) interfaces.get(i);
			if(ifInfo.getName().equalsIgnoreCase(ifName)){
				return ifInfo;
			}
		}
		return null;
		
	}
	
	public InterfaceConfigInfo getIfRefByIndex(int index) {
		if(index < 0 || index > interfaces.size()){
			return null;
		}
		
		return (InterfaceConfigInfo) interfaces.elementAt(index);
	}
	
	
	/**
	 * @return
	 */
	public short getStayAwakeCount() {
		return stayAwakeCount;
	}

	/**
	 * @return
	 */
	public short getBridgeAgeingTime() {
		return bridgeAgeingTime;
	}

	/**
	 * @param count
	 */
	public void setInterfaceCount(short count) {
		interfaceCount	= count;
		for(int i =0; i < count; i++) {
			InterfaceConfigInfo ifConfInfo	=	new InterfaceConfigInfo();
			interfaces.add(i,ifConfInfo);
		}
	}
	/**
	 * @return Returns the lasi.
	 */
	public short getLasi() {
		return lasi;
	}
	/**
	 * @param lasi The lasi to set.
	 */
	public void setLasi(short lasi) {
		this.lasi = lasi;
	}

	/**
	 * @param stayAwakeCount2
	 */
	public void setStayAwakeCount(short stayAwakeCount) {
		this.stayAwakeCount	= stayAwakeCount; 
		
	}

	/**
	 * @param bridgeAgeingTime2
	 */
	public void setBridgeAgeingTime(short bridgeAgeingTime) {
		this.bridgeAgeingTime	= bridgeAgeingTime;
	}

	public int getNODE_INFO_ID() {
		return NODE_INFO_ID;
	}

	public void setNODE_INFO_ID(int nODE_INFO_ID) {
		NODE_INFO_ID = nODE_INFO_ID;
	}

	public int getNODE_INFO_LENGTH() {
		return NODE_INFO_LENGTH;
	}

	public void setNODE_INFO_LENGTH(int nODE_INFO_LENGTH) {
		NODE_INFO_LENGTH = nODE_INFO_LENGTH;
	}
	
	
}
