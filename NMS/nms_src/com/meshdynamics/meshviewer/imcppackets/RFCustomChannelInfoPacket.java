/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RFCustomChannelPacket
 * Comments : RF Custom Channel Packet 
 * Created  : Dec 12, 2006
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * | 7   | Apr 28, 2007|getRFChannelInfoByIndex added					 |Abhishek|
 * --------------------------------------------------------------------------------
 * | 6   |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * | 5   |Jan 02, 2007 | Private channel flag added                      |Abhishek| 
 * --------------------------------------------------------------------------------
 * | 4   |Dec 29, 2006 | sequence modified in copy/read/get packet method| Imran  |
 * --------------------------------------------------------------------------------
 * | 3   |Dec 18, 2006 | setIfName method added                          | Imran  |
 * --------------------------------------------------------------------------------
 * | 2   |Dec 13, 2006 | Getters for Rf editor fields are added          | Imran  |
 * --------------------------------------------------------------------------------
 * | 1   |Dec 12, 2006 | ifName  added                                   | Bindu  |
 * --------------------------------------------------------------------------------
 * | 0   |Dec 12, 2006 | Created                                         | Bindu  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;


import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class RFCustomChannelInfoPacket  extends RequestResponsePacket {

	private static final int    PACKET_LENGTH 		= 18;//14+4

    public static final short 	DFS_BIT_CHECKING	= 0x01;
    private int                  RF_CUSTOMCHANNEL_INFO_ID;
    private int                  RF_CUSTOMCHANNEL_INFO_LENGTH;
    
	private String 				ifName;
	private short 				channelBandwith;
	private short 				antennaPower;
	private short				ctl;				//Conformance Test
	private short 				antennaGain;
	private int					channelCount;
	private RFChannelInfo[]		rfChannelInfo;
	
	public class RFChannelInfo {
		
		private static final int    PACKET_LENGTH 		= 4;
		
		private short 	channel;
		private int 	frequency;
		private short 	privateChannelFlag;
		
		public RFChannelInfo() {
			channel 	= 0;
			frequency 	= 0;
			privateChannelFlag	= 0;
		}

		private boolean readPacket(BufferReader buffer) {
			try {				
				channel 	= buffer.readByte();
				frequency 	= buffer.readShort();
				privateChannelFlag  = buffer.readByte();
				
			} catch(Exception e) {						
				e.printStackTrace();
				return false;
			}
		 return true;
		}
		
		public void getPacket(BufferWriter buffer) {
			buffer.writeByte(channel);
			buffer.writeShort(frequency);
			buffer.writeByte(privateChannelFlag);
		}
		
		public void copyPacket(RFChannelInfo infoOut) {
			infoOut.channel		= this.channel;
			infoOut.frequency	= this.frequency;
			infoOut.privateChannelFlag	= this.privateChannelFlag; 		 
		}

		public short getChannel() {
			return channel;
		}

		public int getFrequency() {
			return frequency;
		}	
		
		public void setChannel(short channel) {
			this.channel = channel;
		}

		public void setFrequency(int frequency) {
			this.frequency = frequency;
		}		
		
		public short getPrivateChannelFlag() {
			return privateChannelFlag;
		}
		
		public void setPrivateChannelFlag(short privateChannelFlag) {
			  this.privateChannelFlag = privateChannelFlag;
		}
	}
	
	public RFCustomChannelInfoPacket() {
		rfChannelInfo 	= null;
		ifName			= "";
	}
	
	protected void copyPacket(IMCPPacket packetOut) {
	
		RFCustomChannelInfoPacket packet	= (RFCustomChannelInfoPacket)packetOut;
		packet.requestResponseID			= this.requestResponseID;
		packet.requestResponseType 			= this.requestResponseType;
		packet.ifName						= this.ifName;
		packet.channelBandwith				= this.channelBandwith;
		packet.antennaGain					= this.antennaGain;
		packet.ctl							= this.ctl;
		packet.antennaPower					= this.antennaPower;
		packet.channelCount					= this.channelCount;
		packet.rfChannelInfo				= new RFChannelInfo[channelCount];
		
		for(int i = 0; i < channelCount; i++){
			packet.rfChannelInfo[i] = new RFChannelInfo();
			this.rfChannelInfo[i].copyPacket(packet.rfChannelInfo[i]);
		}		
	}
	
	public void getPacket(BufferWriter buffer) {
		buffer.writeShort       (RF_CUSTOMCHANNEL_INFO_ID);
		buffer.writeShort       (RF_CUSTOMCHANNEL_INFO_LENGTH);
		buffer.writeMacAddress	(dsMacAddress);
		buffer.writeByte		(requestResponseType);
		buffer.writeByte		(requestResponseID);
		buffer.writeString		(ifName);		
		buffer.writeByte		(channelBandwith);
		buffer.writeByte		(antennaGain);
		buffer.writeByte		(ctl);
		buffer.writeByte		(antennaPower);
		buffer.writeByte		((short)channelCount); 
		
		for(int i = 0; i < channelCount; i++){
			rfChannelInfo[i].getPacket(buffer);
		}				
	}
	
	public int getPacketLength() {

		int packetLength 	= PACKET_LENGTH + ifName.length();
		packetLength 		+= (channelCount * RFChannelInfo.PACKET_LENGTH);
		
		return packetLength;
	
	}
	
	public byte getPacketType() {
		return PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET;
	}
		
	public boolean readPacket(BufferReader buffer) {
		try {
			RF_CUSTOMCHANNEL_INFO_ID            =   buffer.readShort();
			RF_CUSTOMCHANNEL_INFO_LENGTH        =   buffer.readShort();
			dsMacAddress 			=  	buffer.readMacAddress();
	        requestResponseType 	=	buffer.readByte(); 
	        requestResponseID		=  	buffer.readByte();
	        ifName					= 	buffer.readString();
	        channelBandwith			= 	buffer.readByte();
	        antennaGain				= 	buffer.readByte();
	        ctl						= 	buffer.readByte();
	        antennaPower			= 	buffer.readByte();
	        channelCount			= 	buffer.readByte();
	        
	        rfChannelInfo = new RFChannelInfo[channelCount];
	        
	        for(int i = 0; i < channelCount; i++) {
	        	rfChannelInfo[i] = new RFChannelInfo();
	        	if(rfChannelInfo[i].readPacket(buffer) == false)
	        		return false;
	        }
	        
		} catch(Exception e) {
			e.printStackTrace();
		}        

		return true;
	}
	
	public String getIfName() {
		return ifName;
	}
	
	public void setIfName(String ifName){
		this.ifName = ifName;
	}
	
	public int getChannelCount() {
		return channelCount;
	}

	public void setChannelCount(int count) {
		channelCount = count;
		rfChannelInfo = new RFChannelInfo[channelCount];
		for(int i=0;i < channelCount ; i++)
		{
			rfChannelInfo[i]= new RFChannelInfo(); 
		}
	}
	
	public short getChannelBandwith() {
		return channelBandwith;
	}
	
	public void setChannelBandwith(short chBandwidth) {
		channelBandwith = chBandwidth;
	}
	
	public short getCtl() {
		return ctl;
	}
	
	public void setCtl(short ctl) {
		this.ctl = ctl;
	}

	public short getAntennaPower() {
		return antennaPower;
	}
	
	public void setAntennaPower(short antPower) {
		 antennaPower = antPower;
	}
	
	public short getAntennaGain() {
		return antennaGain ;
	}
	
	public void setAntennaGain(short antGain) {
		antennaGain = antGain;
	}
	
	public 	RFChannelInfo getRFChannelInfoByIndex(int index ){
		return rfChannelInfo[index];
	}

	public int getRF_CUSTOMCHANNEL_INFO_ID() {
		return RF_CUSTOMCHANNEL_INFO_ID;
	}

	public void setRF_CUSTOMCHANNEL_INFO_ID(int rF_CUSTOMCHANNEL_INFO_ID) {
		RF_CUSTOMCHANNEL_INFO_ID = rF_CUSTOMCHANNEL_INFO_ID;
	}

	public int getRF_CUSTOMCHANNEL_INFO_LENGTH() {
		return RF_CUSTOMCHANNEL_INFO_LENGTH;
	}

	public void setRF_CUSTOMCHANNEL_INFO_LENGTH(int rF_CUSTOMCHANNEL_INFO_LENGTH) {
		RF_CUSTOMCHANNEL_INFO_LENGTH = rF_CUSTOMCHANNEL_INFO_LENGTH;
	}
	
	
	
}
