/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApResponseHandler.java
 * Comments : 
 * Created  : Aug 07, 2007
 * Author   : Abhishek Patankar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 *  --------------------------------------------------------------------------------
 * |  1  |Nov  6, 2007 |  countrycode related functions added            | Imran   |
 * --------------------------------------------------------------------------------
 * |  0  |Aug 07, 2007 | Created                                         | Abhishek|
 * --------------------------------------------------------------------------------*/
 
package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.ap.ApRuntimeConfigurationManager;

public interface IResponseNotifier {

	ApRuntimeConfigurationManager 	getRuntimeManager();
	void 							setApMoved(boolean moved);
	
	boolean 						isCountryCodeConfigAlreadyReceived();
	boolean 						isCountryCodeChanged();

}
