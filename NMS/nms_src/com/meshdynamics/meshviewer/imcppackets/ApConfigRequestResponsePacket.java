/**
 * MeshDynamics 
 * -------------- 
 * File     : ApConfigRequestResponsePacket.java
 * Comments : 
 * Created  : Sep 24, 2004
 * Author   : abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -------------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author      |
 * -------------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method removed                      | Imran  	   |
 * -------------------------------------------------------------------------------------
 * |  1  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand       |
 * -------------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004 | Created	                                     | abhijit     |
 * -------------------------------------------------------------------------------------
 */
package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;


public class ApConfigRequestResponsePacket extends RequestResponsePacket {

	private static final int PACKET_LENGTH					= 12;  //7+4
    
	private int apConfig_req_res_id;
	private int apConfig_req_res_length;
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean  readPacket(BufferReader buffer) {
		try{
			apConfig_req_res_id            =   buffer.readShort();
			apConfig_req_res_length        =   buffer.readShort();
			dsMacAddress 		    =   buffer.readMacAddress();
			requestResponseType     =   buffer.readByte();
			requestResponseID	    =   buffer.readByte();
			return true;	
		} catch (Exception e){
			e.printStackTrace();
			Mesh.logException(e);
			return false;
		}
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter writer){
	    writer.writeShort(apConfig_req_res_id);
	    writer.writeShort(apConfig_req_res_length);
		writer.writeMacAddress(dsMacAddress);
	    writer.writeByte(requestResponseType);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
	public byte getPacketType() {
        return PacketFactory.AP_CONFIG_REQUEST_RESPONSE;
    }

	public int getApConfig_req_res_id() {
		return apConfig_req_res_id;
	}

	public void setApConfig_req_res_id(int apConfig_req_res_id) {
		this.apConfig_req_res_id = apConfig_req_res_id;
	}

	public int getApConfig_req_res_length() {
		return apConfig_req_res_length;
	}

	public void setApConfig_req_res_length(int apConfig_req_res_length) {
		this.apConfig_req_res_length = apConfig_req_res_length;
	}

}
