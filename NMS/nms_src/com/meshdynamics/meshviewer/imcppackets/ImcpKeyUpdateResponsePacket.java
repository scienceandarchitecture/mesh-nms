/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ResponsePacket.java
 * Comments : Generic response packet (#23)
 * Created  : Nov 25, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  7  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  6  |Nov 19, 2006 | Misc changes:Changes due to StatusDisplayUI     |Abhishek|
 * ----------------------------------------------------------------------------------
 * |  5  |Mar 17, 2006 | macros actions updated after response for move	 | Mithil |
 *  -------------------------------------------------------------------------------
 * |  4  |Feb 28, 2006  | implicit rebooting after reboot removed	  	 | Mithil |
 *  -------------------------------------------------------------------------------
 * |  3  |Feb 18, 2006 | reboot packet set mesh id					  	 | Mithil |
 *  -------------------------------------------------------------------------------
 * |  2  |Feb 16, 2006 | Changes for Dot11e	  						 	 | Bindu  |
 *  --------------------------------------------------------------------------------
 * |  1  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Nov 25, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class ImcpKeyUpdateResponsePacket extends IMCPPacket {

	private static final int PACKET_LENGTH					= 8;
	public static final int RESPONSE_SUCCESS				= 0;
	public static final int RESPONSE_ERROR					= 1;
	public static final int RESPONSE_ENCR_NOT_SUPPORT		= 2;
	
	private int  node_info_id;
	private int  node_info_length;
	private short responseType;
	private short responseId;
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		try {
			node_info_id    = buffer.readShort();
			node_info_length= buffer.readShort();
			dsMacAddress 	= buffer.readMacAddress();
			responseType	= buffer.readByte();
			responseId		= buffer.readByte();
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter writer) {
	    writer.writeShort(node_info_id);
	    writer.writeShort(node_info_length);
	    writer.writeMacAddress(dsMacAddress);
	    writer.writeByte(responseType);
	    writer.writeByte(responseId);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.IMCP_KEY_UPDATE_RESPONSE;
	}

	/**
	 * @return Returns the responseId.
	 */
	public short getResponseId() {
		return responseId;
	}
	/**
	 * @param responseId The responseId to set.
	 */
	public void setResponseId(short responseId) {
		this.responseId = responseId;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}

}
