
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ConfigRequestResponsePacket.java
 * Comments : 
 * Created  : Feb 9, 2005
 * Author   : Bindu Khare
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *  --------------------------------------------------------------------------------
 * | 11  |Mar 28, 2007 | MacroAction call modified in processPacket      | Imran  |
 * --------------------------------------------------------------------------------
 * | 10  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  9  |Nov 19, 2006 | Misc changes:Changes due to StatusDisplayUI     |Abhishek|
 * ----------------------------------------------------------------------------------
 * |  8  |Mar 16, 2006|Changes for ACL							  		| Mithil |
 *  --------------------------------------------------------------------------------
 * |  7  |Feb 17, 2006| Misc Fixes for Dot11e category Dlg		 		| Bindu  |
 *  --------------------------------------------------------------------------------
 * |  6  |Feb 16, 2006| Changes for Dot11e	  					        | Bindu  |
 *  --------------------------------------------------------------------------------
 * |  5  |Feb 16, 2006|Config Sequence Number change                    |Prachiti|
 *  --------------------------------------------------------------------------------
 * |  4  |Feb 13, 2006 | Hide ESSID Implemented						    | Mithil |
 *  --------------------------------------------------------------------------------
 * |  3  |Nov 25,2005 | Reg Domain and Ack Timeout changes 1015 and 1027| Mithil | 
 * -------------------------------------------------------------------------------  
 * |  2  |May 10, 2005| isEncrypted method added for encryption fix     | Anand  |
 * -------------------------------------------------------------------------------
 * |  1  |May 05, 2005| implements MeshViewerResponsePacket             | Bindu  |
 * -------------------------------------------------------------------------------
 * |  0  |Feb 9, 2005 | Created                                         | Bindu  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class ConfigRequestResponsePacket extends RequestResponsePacket {

	public static final short  	AP_CONFIG_REQ_RES 					= 0x01;
	public static final short 	IP_CONFIG_REQ_RES		  			= 0x02;
	public static final short 	VLAN_CONFIG_REQ_RES					= 0x04;
	public static final short 	DOT11E_CATEGORY_CONFIG_REQ_RES		= 0x08;
	public static final short 	DOT11E_CONFIG_REQ_RES				= 0x10;
	public static final short 	IF_HIDDEN_ESSID_REQ_RES				= 0x20;
	public static final short 	ACL_CONFIG_REQ_RES					= 0x40;
	public static final short 	EXTENSION_TO_BYTE_2					= 0x80;

	/*
	 * these fields are for extension byte 2
	 */
	public static final short 	SIP_CONFIG_REQ_RES					= 0x01;
	
	private static final short 	PACKET_LENGTH						= 15;
	
	private short 	configPktType;
	private short	configPktType2;
	private int     CONFIG_REQ_RES_ID;
	private int     CONFIG_REQ_RES_LENGTH;
	private short   errorCode;
	private short   categoryID;
	private String  ifName;
	private String  paramName;
	private String  valueName;

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
		CONFIG_REQ_RES_ID       =   buffer.readShort(); 
		CONFIG_REQ_RES_LENGTH   =   buffer.readShort();
		dsMacAddress			=   buffer.readMacAddress();
		requestResponseType		=   buffer.readByte();
		configPktType			=   buffer.readByte();
		requestResponseID		=   buffer.readByte();
		if((configPktType & EXTENSION_TO_BYTE_2) != 0) {
			configPktType2		= buffer.readByte();
		}
		errorCode=buffer.readByte();//success 0 & failure non Zero
		if(errorCode !=0){
			categoryID  = buffer.readByte();;
			ifName      = buffer.readString();
			paramName   = buffer.readString();
			valueName   = buffer.readString();
		}
		}catch(Exception e){
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter buffer) {
		buffer.writeShort(CONFIG_REQ_RES_ID);
		buffer.writeShort(CONFIG_REQ_RES_LENGTH);
		buffer.writeMacAddress(dsMacAddress	);
		buffer.writeByte(requestResponseType);
		buffer.writeByte(configPktType);
		buffer.writeByte(requestResponseID);		
		if((configPktType & EXTENSION_TO_BYTE_2) != 0) {
			buffer.writeByte(configPktType2);
		}
		}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.CONFIG_REQ_RES_PACKET;
	}

	/**
	 * @return Returns the configPktType.
	 */
	public short getConfigPktType() {
		return configPktType;
	}
	/**
	 * @param configPktType The configPktType to set.
	 */
	public void setConfigPktType(short configPktType) {
		this.configPktType = configPktType;
	}

	/**
	 * @return Returns the configPktType2.
	 */
	public short getConfigPktType2() {
		return configPktType2;
	}
	/**
	 * @param configPktType2 The configPktType2 to set.
	 */
	public void setConfigPktType2(short configPktType2) {
		this.configPktType2 = configPktType2;
	}

	public int getCONFIG_REQ_RES_ID() {
		return CONFIG_REQ_RES_ID;
	}

	public void setCONFIG_REQ_RES_ID(int cONFIG_REQ_RES_ID) {
		CONFIG_REQ_RES_ID = cONFIG_REQ_RES_ID;
	}

	public int getCONFIG_REQ_RES_LENGTH() {
		return CONFIG_REQ_RES_LENGTH;
	}

	public void setCONFIG_REQ_RES_LENGTH(int cONFIG_REQ_RES_LENGTH) {
		CONFIG_REQ_RES_LENGTH = cONFIG_REQ_RES_LENGTH;
	}

	public short getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(short errorCode) {
		this.errorCode = errorCode;
	}

	public short getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(short categoryID) {
		this.categoryID = categoryID;
	}

	public String getIfName() {
		return ifName;
	}

	public void setIfName(String ifName) {
		this.ifName = ifName;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getValueName() {
		return valueName;
	}

	public void setValueName(String valueName) {
		this.valueName = valueName;
	}
	
	
}
