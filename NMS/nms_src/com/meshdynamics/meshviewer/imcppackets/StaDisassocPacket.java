/**
 * MeshDynamics 
 * -------------- 
 * File     : StaDisassocPacket.java
 * Comments : 
 * Created  : Sep 24, 2004
 * Author   : abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -------------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author      |
 * -------------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method removed                      | Imran  	   |
 * -------------------------------------------------------------------------------------
 * |  1  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand       |
 * -------------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004 | Created	                                     | abhijit     |
 * -------------------------------------------------------------------------------------
 */
package com.meshdynamics.meshviewer.imcppackets;


import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.MacAddress;

public class StaDisassocPacket extends IMCPPacket{
	
    private static final int PACKET_LENGTH	= 14;
	
    private int              stadis_info_id;
    private int              stadis_info_length;
    private MacAddress 		 staMacAddress;
	private int 			 reason;

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean readPacket(BufferReader buffer) {
		try {
			stadis_info_id     = buffer.readShort();
			stadis_info_length = buffer.readShort();
			dsMacAddress       = buffer.readMacAddress();
			staMacAddress      = buffer.readMacAddress();
			reason 		       = buffer.readShort();
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter writer) {
	    writer.writeShort(stadis_info_id);
	    writer.writeShort(stadis_info_length);
		writer.writeMacAddress(dsMacAddress);
	    writer.writeMacAddress(staMacAddress);
	    writer.writeShort(reason);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
	public byte getPacketType() {        
        return PacketFactory.STA_DISASSOC_NOTIFICATION;
    }

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}

	public int getReason() {
		return reason;
	}

	public MacAddress getStaMacAddress() {
		return staMacAddress;
	}

	public int getStadis_info_id() {
		return stadis_info_id;
	}

	public int getStadis_info_length() {
		return stadis_info_length;
	}
	
	
}
