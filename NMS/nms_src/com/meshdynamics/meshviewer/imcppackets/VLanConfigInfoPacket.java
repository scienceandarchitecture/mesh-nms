/**
 * MeshDynamics 
 * -------------- 
 * File     : VLanConfigInfoPacket.java
 * Comments : 
 * Created  : Feb 1, 2005
 * Author   : Bindu
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ------------------------------------------------------------------------------------
 * | 17  |Jan 12, 2007   | isEncrypted method removed                 | Imran        |
 * -----------------------------------------------------------------------------------
 * | 16  |Nov 27, 2006   | clean-up								      | Bindu        |
 *  --------------------------------------------------------------------------------
 * | 15  | Oct  06,2005  | setVlanConfigs=null when all vlans deleted | Bindu        |
 * -----------------------------------------------------------------------------------
 * | 14  | Sept 26,2005  | VlanCount fixed						      | Amit         |
 * -----------------------------------------------------------------------------------
 * | 13  | Sept 26,2005  | Vlanconfiginfo array initialized           | Amit         |
 * -----------------------------------------------------------------------------------   
 * | 12  | Sept 20,2005	 | vlancount bug fixed in setVlanCount func   | Amit         |
 * -----------------------------------------------------------------------------------
 * | 11  |May 10, 2005   | isEncrypted method added for encryption fix| Anand        |
 * -----------------------------------------------------------------------------------
 * | 10  |May 05, 2005   | checking for polling pkt in processPacket  | Bindu  		 |
 * -----------------------------------------------------------------------------------
 * |  9  |Apr 26, 2005   | Bug fixed in copy packet                   | Anand        |
 * -----------------------------------------------------------------------------------
 * |  8  |Apr 20, 2005   | Misc Fixes                                 | Bindu        |
 * -----------------------------------------------------------------------------------
 * |  7  |Apr 13, 2005   | Misc Changes								  | Bindu        |
 * -----------------------------------------------------------------------------------
 * |  6  |Mar 07, 2005   | verifyVLansUpdates()defDot11eEn mask added | Bindu        |
 * -----------------------------------------------------------------------------------
 * |  5  |Mar 07, 2005   | defaultDot11eEnabled added,packet len = 14 | Bindu        |
 * -----------------------------------------------------------------------------------
 * |  4  |Mar 04, 2005   | verifyVLanPacketUpdate() Added	          | Bindu        |
 * -----------------------------------------------------------------------------------
 * |  3  |Mar 02, 2005   | toString() Implemented			          | Bindu        |
 * -----------------------------------------------------------------------------------
 * |  2  |Mar 01, 2005   | Changes for interface security             | Anand        |
 * -----------------------------------------------------------------------------------
 * |  1  |Feb 15, 2005   | WEPKey Packet changes                      | Bindu        |
 * -----------------------------------------------------------------------------------
 * |  0  |Feb 01, 2005   | Created	                                  | Bindu        |
 * -----------------------------------------------------------------------------------
 */


package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.imcppackets.helpers.SecurityInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.VLanConfigInfo;
import com.meshdynamics.meshviewer.imcppackets.helpers.WEPSecurityInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class VLanConfigInfoPacket extends RequestResponsePacket {
	
	private static final int    	PACKET_LENGTH 		= 18;//14+4
	public static final int 		UNTAGGED 			= -1;
	
	private static final String		DEFAULT_VLAN_NAME 	= "default";
	
	
	private short 					vlanCount;
	private int 					defaultTag;
	private short 					defaultDot1p;
	private short 					defaultDot11eEnabled;
	private short 					defaultDot11e;	
	private int                     VLAN_CONFIG_ID;
	private int                     VLAN_CONFIG_LENGTH;
	private VLanConfigInfo[] 		vLanConfigs;
	
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {		
		try {
			VLAN_CONFIG_ID           =   buffer.readShort();
			VLAN_CONFIG_LENGTH       =   buffer.readShort();
			dsMacAddress 			= 	buffer.readMacAddress();
			requestResponseType 	=   buffer.readByte();
			requestResponseID		= 	buffer.readByte();
			vlanCount				= 	buffer.readByte();
			defaultTag				= 	buffer.readShort();		
			defaultDot1p			= 	buffer.readByte();
			defaultDot11eEnabled	=	buffer.readByte();
			defaultDot11e			=	buffer.readByte(); 
			
			/*
			 * vlan count includes default vlan but we have 
			 * already read info for dedfault vlan and info for default
			 * vlan is actually not present further hence we 
			 * decrement vlan count   
			 */
			vlanCount--;
			
			if(vlanCount < 0 ){
				return false;
			}
			vLanConfigs 			= 	new VLanConfigInfo[vlanCount];			
			
			for(int i = 0; i < vlanCount; i++) {				
				vLanConfigs[i]	= new VLanConfigInfo();				
				if(!vLanConfigs[i].readPacket(buffer))
					return false;

			}
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter buffer) {
		
		short vcnt;
		buffer.writeShort(VLAN_CONFIG_ID);
		buffer.writeShort(VLAN_CONFIG_LENGTH);
		buffer.writeMacAddress(dsMacAddress);
		buffer.writeByte(requestResponseType);
		buffer.writeByte(requestResponseID);
		
		vcnt = (short)(getVlanCount() + 1);
		/* vlan count is incremented by one because  vlan count
		inclusive of default vlan also*/
		buffer.writeByte(vcnt); 
		buffer.writeShort(defaultTag);
		buffer.writeByte(defaultDot1p);
		buffer.writeByte(defaultDot11eEnabled);
		buffer.writeByte(defaultDot11e);
		
		for(int i = 0; i < vlanCount; i++){
			vLanConfigs[i].getPacket(buffer);
		}		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		int packetLength = 0;
		packetLength = PACKET_LENGTH;
		
		for(int i = 0; i < vlanCount; i++ ){
			packetLength += VLanConfigInfo.PACKET_LENGTH;
			packetLength += vLanConfigs[i].getName().length();
			packetLength += vLanConfigs[i].getEssid().length();
			packetLength += SecurityInfo.SECURITY_LENGTH;
			
			SecurityInfo secInfo = vLanConfigs[i].getSecurityInfo();
			//TODO CHECKNG FOR null
			if(secInfo == null)
				continue;
			short enabled = secInfo.getWEPEnabled();
			if(enabled == 1) {
				if(secInfo.getWEPInfo().getWEPStrength() == WEPSecurityInfo.WEP_64_BIT) {
					packetLength += SecurityInfo.WEP_LENGTH + WEPSecurityInfo.WEP_64_BIT_KEY_LENGTH;					
				} else if(secInfo.getWEPInfo().getWEPStrength() == WEPSecurityInfo.WEP_128_BIT) {
					packetLength += SecurityInfo.WEP_LENGTH + WEPSecurityInfo.WEP_128_BIT_KEY_LENGTH;
				}
			}
			
			packetLength += SecurityInfo.PSK_LENGTH + secInfo.getRSNPSKInfo().getPSKKey().length;
			packetLength += SecurityInfo.RAD_LENGTH + secInfo.getRADInfo().getRADKey().length();
		
		}			
		return packetLength ;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.AP_VLAN_CONFIGURATION_INFO;
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		VLanConfigInfoPacket vLanConfigInfoNew  = (VLanConfigInfoPacket)packet;
		vLanConfigInfoNew.requestResponseID		= this.requestResponseID;
		vLanConfigInfoNew.requestResponseType 	= this.requestResponseType;
		vLanConfigInfoNew.vlanCount				= this.vlanCount;
		vLanConfigInfoNew.defaultTag			= this.defaultTag;
		vLanConfigInfoNew.defaultDot1p			= this.defaultDot1p;
		vLanConfigInfoNew.defaultDot11eEnabled	= this.defaultDot11eEnabled;
		vLanConfigInfoNew.defaultDot11e			= this.defaultDot11e;
		vLanConfigInfoNew.vlanCount       		= this.vlanCount;
		vLanConfigInfoNew.vLanConfigs			= new VLanConfigInfo[vlanCount];
		
		for(int i = 0; i < vlanCount; i++){
			vLanConfigInfoNew.vLanConfigs[i] = new VLanConfigInfo();
			this.vLanConfigs[i].copyPacket(vLanConfigInfoNew.vLanConfigs[i]);
		}
 		
	}

	public VLanConfigInfo getVLanConfig(int index) {
		return vLanConfigs[index];
	}	
	
	/**
	 * @return Returns the defaultVLanName.
	 */
	public static String getDefaultVLanName() {
		return DEFAULT_VLAN_NAME;
	}
	
	/**
	 * @return Returns the defaultDot1e.
	 */
	public short getDefaultDot11e() {
		return defaultDot11e;
	}
	
	/**
	 * @param defaultDot1e The defaultDot1e to set.
	 */
	public void setDefaultDot11e(short defaultDot11e) {
		this.defaultDot11e = defaultDot11e;
	}
	
	/**
	 * @return Returns the defaultDot1p.
	 */
	public short getDefaultDot1p() {
		return defaultDot1p;
	}
	
	/**
	 * @param defaultDot1p The defaultDot1p to set.
	 */
	public void setDefaultDot1p(short defaultDot1p) {
		this.defaultDot1p = defaultDot1p;
	}
	
	/**
	 * @return Returns the defaultTag.
	 */
	public int getDefaultTag() {
		return defaultTag;
	}
	
	/**
	 * @param defaultTag The defaultTag to set.
	 */
	public void setDefaultTag(int defaultTag) {
		this.defaultTag = defaultTag;
	}
	
	/**
	 * @return Returns the vlanCount.
	 */
	public short getVlanCount() {
		return vlanCount;
	}
	
	/**
	 * @param vlanCount The vlanCount to set.
	 */
	public void setVlanCount(short vlanConfigCount) {
		this.vlanCount  = vlanConfigCount;
		vLanConfigs = new VLanConfigInfo[vlanCount];
		for(int i=0;i<vlanCount;i++) {
			vLanConfigs[i] = new VLanConfigInfo();
		}
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "VLAN-Configuration Packet";
	}
	
	/**
	 * @return Returns the defaultDot11eEnabled.
	 */
	public short getDefaultDot11eEnabled() {
		return defaultDot11eEnabled;
	}

	/**
	 * @param defaultDot11eEnabled The defaultDot11eEnabled to set.
	 */
	public void setDefaultDot11eEnabled(short defaultDot11eEnabled) {
		this.defaultDot11eEnabled = defaultDot11eEnabled;
	}

	public int getVLAN_CONFIG_ID() {
		return VLAN_CONFIG_ID;
	}

	public void setVLAN_CONFIG_ID(int vLAN_CONFIG_ID) {
		VLAN_CONFIG_ID = vLAN_CONFIG_ID;
	}

	public int getVLAN_CONFIG_LENGTH() {
		return VLAN_CONFIG_LENGTH;
	}

	public void setVLAN_CONFIG_LENGTH(int vLAN_CONFIG_LENGTH) {
		VLAN_CONFIG_LENGTH = vLAN_CONFIG_LENGTH;
	}

	
}
	