/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Dot11eIfInfoPacket
 * Comments : 802.11e IfInfo Configuration Class for MeshViewer
 * Created  : Feb 13, 2006
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date          |  Comment                                        | Author |
 * ------------------------------------------------------------------------------------
 * |  5  |Jan 12, 2007  | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  4  | Feb 21, 2006 | Misc Fixes for dot11e 						  |Bindu   |
 * --------------------------------------------------------------------------------
 * |  3  | Feb 17, 2006 | Misc Fixes for dot11e 						  |Mithil  |
 * --------------------------------------------------------------------------------
 * |  2  | Feb 17, 2006 | Misc Fixes for dot11e 						  |Bindu   |
 * --------------------------------------------------------------------------------
 * |  1  | Feb 17, 2006 | Misc Fixes for dot11e IfInfo					  |Bindu   |
 * -------------------------------------------------------------------------------- 
 * |  0  | Feb 13, 2006 | Created                                         |Bindu   |
 * --------------------------------------------------------------------------------
**********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import com.meshdynamics.meshviewer.imcppackets.helpers.Dot11eIfInfoDetails;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class Dot11eIfInfoPacket extends RequestResponsePacket {

	private static final int BASE_PACKET_LENGTH	= 13;//9+4

	private short 	interfaceCount;
	private Vector<Dot11eIfInfoDetails>	interfaces;
	
	private int     DOT11E_INFO_ID;
	private int     DOT11E_INFO_LENGTH;
	
	public Dot11eIfInfoPacket() {
		super();
		interfaces = new Vector<Dot11eIfInfoDetails>();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			DOT11E_INFO_ID            =   buffer.readShort();
			DOT11E_INFO_LENGTH        =   buffer.readShort();
			dsMacAddress 			=  	buffer.readMacAddress();			
	        requestResponseType 	=	buffer.readByte(); 
	        requestResponseID  		=  	buffer.readByte();
	        interfaceCount			=  	buffer.readByte();
        
	        interfaces	 			=	new Vector<Dot11eIfInfoDetails>();
	        for (int i = 0; i < interfaceCount; i++) {
	        	Dot11eIfInfoDetails info = new Dot11eIfInfoDetails();	        	
	        	info.setInterfaceName	(buffer.readString());
	        	info.setIfDot11eEnabled	(buffer.readByte());
	        	info.setIfDot11eCategory(buffer.readByte());
	        	info.setIfEssid			(buffer.readString());

				interfaces.add(info);				
			}
	        
	        return true;
		} catch(Exception e){
			Mesh.logException(e);
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter buffer) {
		try {
			buffer.writeShort       (DOT11E_INFO_ID);
			buffer.writeShort       (DOT11E_INFO_LENGTH);
			buffer.writeMacAddress	(dsMacAddress);
			buffer.writeByte		(requestResponseType);
			buffer.writeByte		(requestResponseID);
			buffer.writeByte		((short)interfaces.size());
			
			Iterator<Dot11eIfInfoDetails> it = interfaces.iterator();
			while(it.hasNext()) {
				Dot11eIfInfoDetails ifDetails = (Dot11eIfInfoDetails)it.next();
				ifDetails.write(buffer);
			}
		} catch(Exception  e) {
			Mesh.logException(e);
		}

		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		int 		length 	= BASE_PACKET_LENGTH;
		Iterator<Dot11eIfInfoDetails> 	itr 	= interfaces.iterator();
		
		while(itr.hasNext()) {
			Dot11eIfInfoDetails dot11eIfDetails = (Dot11eIfInfoDetails)itr.next();
			length += 1; 									//Name length 		= 1 Byte
			length += dot11eIfDetails.getInterfaceName().length(); 	//Name Length Bytes
			length += 3;
			length += dot11eIfDetails.getIfEssid().length();
		}		
		return length;

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.DOT11E_IFCONFIG_INFO_PACKET;		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		Dot11eIfInfoPacket dot11eIfInfoNew	=  (Dot11eIfInfoPacket)packet;
		dot11eIfInfoNew.requestResponseType	=  this.requestResponseType;
		dot11eIfInfoNew.requestResponseID	=  this.requestResponseID;
		dot11eIfInfoNew.interfaceCount		=  this.interfaceCount;
		
		Enumeration<Dot11eIfInfoDetails> enm = this.interfaces.elements();
		while(enm.hasMoreElements()) {
			Dot11eIfInfoDetails confOld			= (Dot11eIfInfoDetails)enm.nextElement();
			Dot11eIfInfoDetails confNew 	= new Dot11eIfInfoDetails();
			confOld.copyPacket(confNew);			
			dot11eIfInfoNew.interfaces.add(confNew);
		}		
		
	}

	/**
	 * @return Returns the interfaceCount.
	 */
	public short getInterfaceCount() {
		return interfaceCount;
	}
	/**
	 * @param interfaceCount The interfaceCount to set.
	 */
	public void setInterfaceCount(short interfaceCount) {
		this.interfaceCount = interfaceCount;
		this.interfaces.clear();
		for(int i=0;i<interfaceCount;i++) {
			Dot11eIfInfoDetails details = new Dot11eIfInfoDetails();
			this.interfaces.add(details);
		}
	}
	
	public Dot11eIfInfoDetails getIfRefByIndex(int index) {
		if(index < 0 || index >= interfaces.size()){
			return null;
		}
		
		return (Dot11eIfInfoDetails) interfaces.elementAt(index);
	}

	public int getDOT11E_INFO_ID() {
		return DOT11E_INFO_ID;
	}

	public void setDOT11E_INFO_ID(int dOT11E_INFO_ID) {
		DOT11E_INFO_ID = dOT11E_INFO_ID;
	}

	public int getDOT11E_INFO_LENGTH() {
		return DOT11E_INFO_LENGTH;
	}

	public void setDOT11E_INFO_LENGTH(int dOT11E_INFO_LENGTH) {
		DOT11E_INFO_LENGTH = dOT11E_INFO_LENGTH;
	}
	
	
	
}
