package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class MgmtgwResponsePacket extends IMCPPacket {

	private static final int PACKET_LENGTH = 11;
	public static final int mgmt_gw_enable  = 1;
	private int id;
	private int length;
	private short status;
	@Override
	public boolean readPacket(BufferReader buffer) {
		try {
			id          = buffer.readShort();
			length      = buffer.readShort();
			dsMacAddress= buffer.readMacAddress();
			status      = buffer.readByte();
		} catch (Exception e) {
			Mesh.logException(e);
		}
		return true;
	}

	@Override
	public void getPacket(BufferWriter buffer) {
	try {
		
	} catch (Exception e) {
		Mesh.logException(e);
	}
		
	}

	@Override
	public int getPacketLength() {
		return PACKET_LENGTH;
	}
	@Override
	public byte getPacketType() {
		return PacketFactory.MGMT_GW_RESPONSE;
	}

	@Override
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

}
