/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamRequestResponsePacket.java
 * Comments : 
 * Created  : Feb 20, 2007
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author   |
 * ----------------------------------------------------------------------------------
 * |  0  | Feb 20, 2007 | Created                                         |  Bindu   |
 * ----------------------------------------------------------------------------------
 **********************************************************************************/


package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.imcppackets.helpers.IMCPInfoIDS;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class EffistreamRequestResponsePacket extends RequestResponsePacket {

	private static final int 	PACKET_LENGTH 					= 12;//8+4

	private int EFFISTREAM_REQ_RES_ID;
	private int  EFFISTREAM_REQ_RES_LENGTH;
	public void getPacket(BufferWriter buffer) {
		try{
			buffer.writeShort(IMCPInfoIDS.IMCP_EFFISTREAM_REQ_RES_INFO_ID);
			buffer.writeShort(getPacketLength());
			buffer.writeMacAddress(dsMacAddress);
			buffer.writeByte(requestResponseType);
			buffer.writeByte(requestResponseID);
		} catch (Exception e){
			Mesh.logException(e);
		}		
	}

	public int getPacketLength() {
		return PACKET_LENGTH;
	}

	public byte getPacketType() {
		return PacketFactory.EFFISTREAM_CONFIG_REQUEST_RESPONSE_PACKET;
	}

	public boolean readPacket(BufferReader buffer) {
		try {
			EFFISTREAM_REQ_RES_ID        = buffer.readShort();
			EFFISTREAM_REQ_RES_LENGTH    = buffer.readShort();
			dsMacAddress 		= buffer.readMacAddress();
			requestResponseType = buffer.readByte();
			requestResponseID 	= buffer.readByte();
			return true;
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
	}
	
	public	boolean isEncrypted(){
		return false;
	}

	public int getEFFISTREAM_REQ_RES_ID() {
		return EFFISTREAM_REQ_RES_ID;
	}

	public void setEFFISTREAM_REQ_RES_ID(int eFFISTREAM_REQ_RES_ID) {
		EFFISTREAM_REQ_RES_ID = eFFISTREAM_REQ_RES_ID;
	}

	public int getEFFISTREAM_REQ_RES_LENGTH() {
		return EFFISTREAM_REQ_RES_LENGTH;
	}

	public void setEFFISTREAM_REQ_RES_LENGTH(int eFFISTREAM_REQ_RES_LENGTH) {
		EFFISTREAM_REQ_RES_LENGTH = eFFISTREAM_REQ_RES_LENGTH;
	}

}
