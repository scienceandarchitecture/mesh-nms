/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : IfHiddenESSIDInfoPacket
 * Comments : 
 * Created  : Nov 16, 2005
 * Author   : Mithil Wane
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * ------------------------------------------------------------------------------------
 * |  3  |Apr 27, 2007 | setIfName added                                 |Abhishek|
 * --------------------------------------------------------------------------------
 * |  2  |Apr 27, 2007 | setHiddenESSID added                            |Abhishek|
 * --------------------------------------------------------------------------------
 * |  1  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  0  |Nov 16, 2005 | Created                                         | Mithil |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import com.meshdynamics.meshviewer.imcppackets.helpers.IfHiddenESSIDInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class IfHiddenESSIDInfoPacket extends RequestResponsePacket {

	private static final int BASE_PACKET_LENGTH = 13;//9+4
    
	private short		iCnt;
	private Vector<IfHiddenESSIDInfo> 	    ifhiddenESSIDInfo;
	
	private int       IFHIDDENESSID_INFO_ID;
	private int       IFHIDDENESSID_INFO_LENGTH;
	/**
	 * @param ackTimeout
	 */
	public IfHiddenESSIDInfoPacket() {
		ifhiddenESSIDInfo = new Vector<IfHiddenESSIDInfo>();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			IFHIDDENESSID_INFO_ID            =   buffer.readShort();
			IFHIDDENESSID_INFO_LENGTH        =   buffer.readShort();
			dsMacAddress 			=  	buffer.readMacAddress();
	        requestResponseType 	=	buffer.readByte(); 
	        requestResponseID  		=  	buffer.readByte();
	        iCnt  					=  	buffer.readByte();
	        
	        for (int i = 0; i < iCnt; i++) {
	        	IfHiddenESSIDInfo info 	= new IfHiddenESSIDInfo();
	        	info.ifName 			= buffer.readString();
				info.hidden 			= buffer.readByte();
				ifhiddenESSIDInfo.add(info);
			}
	        return true;
		} catch(Exception e){
			Mesh.logException(e);
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter buffer) {
		try {
			buffer.writeShort       (IFHIDDENESSID_INFO_ID);
			buffer.writeShort       (IFHIDDENESSID_INFO_LENGTH);
			buffer.writeMacAddress	(dsMacAddress);
			buffer.writeByte		(requestResponseType);
			buffer.writeByte		(requestResponseID);
			buffer.writeByte		(iCnt);	
			for(int i=0;i<iCnt;i++) {
				IfHiddenESSIDInfo info = (IfHiddenESSIDInfo)ifhiddenESSIDInfo.get(i);
				buffer.writeString	(info.ifName);
				buffer.writeByte	(info.hidden);
			}
		} catch(Exception  e) {
		Mesh.logException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		int len = BASE_PACKET_LENGTH;
		Iterator<IfHiddenESSIDInfo> it = ifhiddenESSIDInfo.iterator();
		while(it.hasNext()) {
			IfHiddenESSIDInfo inf = (IfHiddenESSIDInfo)it.next();
			len += 1;
			len += inf.ifName.length();
			len += 1;
		}
		
		return len;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.IF_HIDDEN_ESSID_PACKET;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		IfHiddenESSIDInfoPacket ifhiddenInfoPacket = (IfHiddenESSIDInfoPacket)packet;
		ifhiddenInfoPacket.requestResponseID = this.requestResponseID;
		ifhiddenInfoPacket.requestResponseType = this.requestResponseType;
		ifhiddenInfoPacket.iCnt = this.iCnt;
		Enumeration<IfHiddenESSIDInfo> enm = this.ifhiddenESSIDInfo.elements();
        while(enm.hasMoreElements()) {
        	IfHiddenESSIDInfo ifConfOld = (IfHiddenESSIDInfo)enm.nextElement();
        	IfHiddenESSIDInfo ifConf 	= new IfHiddenESSIDInfo();
			ifConf.ifName = ifConfOld.ifName;
			ifConf.hidden = ifConfOld.hidden;
			ifhiddenInfoPacket.ifhiddenESSIDInfo.add(ifConf);
        }
	}
	
	/**
	 * @return Returns the iCnt.
	 */
	public short getICnt() {
		return iCnt;
	}
	
	/**
	 * @param cnt The iCnt to set.
	 */
	public void setICnt(short cnt) {
		iCnt = cnt;
		ifhiddenESSIDInfo.clear();
		for(int i=0;i<iCnt;i++) {
			IfHiddenESSIDInfo info = new IfHiddenESSIDInfo();
			ifhiddenESSIDInfo.add(info);
		}
	}
	
	public IfHiddenESSIDInfo getIfRefByIndex(int index) {
		if(ifhiddenESSIDInfo.size() == 0){
			return null;
		}
		return (IfHiddenESSIDInfo)ifhiddenESSIDInfo.elementAt(index);
	}

	/**
	 * @param j
	 * @param name
	 */
	public void setIfName(int j, String name) {
		
		IfHiddenESSIDInfo info = (IfHiddenESSIDInfo)ifhiddenESSIDInfo.get(j);
		if(info == null) {
			return;
		}
		info.ifName	= name;
		
	}

	/**
	 * @param j
	 * @param essidHidden
	 */
	public void setHiddenESSID(int j, short essidHidden) {
		
		IfHiddenESSIDInfo info = (IfHiddenESSIDInfo)ifhiddenESSIDInfo.get(j);
		if(info == null) {
			return;
		}
		info.hidden	= essidHidden;
	}

	public int getIFHIDDENESSID_INFO_ID() {
		return IFHIDDENESSID_INFO_ID;
	}

	public void setIFHIDDENESSID_INFO_ID(int iFHIDDENESSID_INFO_ID) {
		IFHIDDENESSID_INFO_ID = iFHIDDENESSID_INFO_ID;
	}

	public int getIFHIDDENESSID_INFO_LENGTH() {
		return IFHIDDENESSID_INFO_LENGTH;
	}

	public void setIFHIDDENESSID_INFO_LENGTH(int iFHIDDENESSID_INFO_LENGTH) {
		IFHIDDENESSID_INFO_LENGTH = iFHIDDENESSID_INFO_LENGTH;
	}

}
