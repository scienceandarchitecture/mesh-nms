/**
 * MeshDynamics 
 * -------------- 
 * File     : StaAssocPacket.java
 * Comments : 
 * Created  : Sep 24, 2004
 * Author   : abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -------------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author      |
 * -------------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method removed                      | Imran  	   |
 * -------------------------------------------------------------------------------------
 * |  1  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand       |
 * -------------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004 | Created	                                     | abhijit     |
 * -------------------------------------------------------------------------------------
 */

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.MacAddress;

public class StaAssocPacket extends IMCPPacket{

	private static final int PACKET_LENGTH = 18;
	private int         sta_info_id;
	private int         sta_info_length;
	private MacAddress 	wmMacAddress;
	private MacAddress 	staMacAddress;

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			sta_info_id     = buffer.readShort();
			sta_info_length = buffer.readShort();
			dsMacAddress 	= buffer.readMacAddress();	
			wmMacAddress 	= buffer.readMacAddress();	
			staMacAddress 	= buffer.readMacAddress();	
		}catch(Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter writer) {
	    writer.writeShort(sta_info_id);
	    writer.writeShort(sta_info_length);
	    writer.writeMacAddress(dsMacAddress);
	    writer.writeMacAddress(wmMacAddress);
	    writer.writeMacAddress(staMacAddress);
	    
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

	/**
	 * @return Returns the staMacAddress.
	 */
	public MacAddress getStaMacAddress() {
		return staMacAddress;
	}

	/**
	 * @return Returns the wmMacAddress.
	 */
	public MacAddress getWmMacAddress() {
		return wmMacAddress;
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
	public byte getPacketType() {
        return PacketFactory.STA_ASSOC_NOTIFICATION;
    }

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}

}
