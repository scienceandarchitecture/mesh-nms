/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Heartbeat2Packet.java
 * Comments : AccessPoint Heartbeat2 Info Packet IMCP(#1)
 * Created  : Sep 24, 2004
 * Author   : Mithil Wane
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * | 23  |Feb 27,2007  |  removed unused code							 |Abhijit |
 *---------------------------------------------------------------------------------
 * | 22  |Jan 19, 2007 | FIPS 140-2 detect flag added 					 |Abhishek |
 * --------------------------------------------------------------------------------
 * | 21  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 *  --------------------------------------------------------------------------------
 * | 20  |Nov 27, 2006 | radar detect flag added 					     | Bindu  |
 *  --------------------------------------------------------------------------------
 * | 19  |Apr 10, 2006 | APType throws Null pointer if HWinfo !recvd     | Mithil |
 *  --------------------------------------------------------------------------------
 * | 18  |Mar 17, 2006 | Reboot Flag updation reflect on Node            | Mithil |
 *  --------------------------------------------------------------------------------
 * | 17  |Feb 20, 2006 |capabilities divided to cap and cmd cap & cfg cap| Mithil |
 *  --------------------------------------------------------------------------------
 * | 16  |Feb 20, 2006 |Get Set added for capabilities			         |Prachiti|
 *  --------------------------------------------------------------------------------
 * | 15  |Feb 20, 2006 |capabilities divided to cap and cmd cap          | Mithil |
 *  --------------------------------------------------------------------------------
 * | 14  |Feb 17, 2006 |Config Sequence Number change uncommented        |Prachiti|
 *  --------------------------------------------------------------------------------
 * | 13  |Feb 16, 2006 |Config Sequence Number change                    |Prachiti|
 *  --------------------------------------------------------------------------------
 * | 12 |Feb 15, 2006  | Reboot Flag updation		                     | Mithil |
 *  --------------------------------------------------------------------------------
 * | 11 |Feb 15, 2006  | STA Info Displayed on flag                      | Mithil |
 *  --------------------------------------------------------------------------------
 * | 10 |Feb 14, 2006  | STA Info Displayed		        			     | Mithil |
 *  --------------------------------------------------------------------------------
 * | 9  |Feb 13, 2006 | Deny Clients handling removed					 | Mithil |
 * --------------------------------------------------------------------------------
 * | 8  |Feb 13, 2006 | flags and capabilities read later				 | Mithil |
 * --------------------------------------------------------------------------------
 * | 7  |Feb 03, 2006 | DIsplay in Property UI		 					 | Mithil |
 * --------------------------------------------------------------------------------
 * | 6  |Feb 03, 2006  | added flags and capabilities  					 | Mithil |
 * --------------------------------------------------------------------------------
 * | 5  | Jan 18, 2006 | HeartBeat2 Dnlnk Rate and Dnlnk mismatch in log | Mithil | 
 * --------------------------------------------------------------------------------
 * | 4  | Jan 16, 2006 | HeartBeat2 not coming in log			  		 | Mithil | 
 * --------------------------------------------------------------------------------
 * | 3  | Oct 26, 2005 | removed parent dw link signal strength  		 | Mithil | 
 * -------------------------------------------------------------------------------- 
 * | 2  | Oct 21, 2005 | Added sequence Number and stored hb2 information|        | 
 * 						 in Access Point								 | Mithil |
 * -------------------------------------------------------------------------------- 
 * | 1  | Oct 21, 2005 | Changed field name to parentDwnlinkSigStrength  | Mithil |
 * -------------------------------------------------------------------------------- 
 * | 0  | Oct 20, 2005 | Created                                         | Mithil |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.util.StaSignalEntry;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.MacAddress;

public class HeartBeat2Packet extends IMCPPacket {

	private int                 NODE_INFO_ID;
	private int                 NODE_INFO_LENGTH; 
	private long 			 	sequenceNumber;
	private long 			 	uplinkTransmitRate;
	private long 			 	dwnlinkTransmitRate;
	private short 			 	staCount;
	private StaSignalEntry[] 	staSignalEntries;
	private long				flags;
	private long				configSeqNumber;
	private short				capabilities;				
	private short				cmdcapabilities;
	private short				configcapabilities;
	private int 				packetLength;
	
	public HeartBeat2Packet() {
		dsMacAddress 			= null;
		uplinkTransmitRate	 	= 0;
		dwnlinkTransmitRate 	= 0;
		flags					= 0;
		configSeqNumber			= -1;
		capabilities			= 0;
		cmdcapabilities			= 0;
		configcapabilities		= 0;
		packetLength			= 0;
		//packetLogger			= new ApPacketLogger();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean readPacket(BufferReader buffer) {
		
	    packetLength = 0;
		try {
			NODE_INFO_ID                    = buffer.readShort();           packetLength    += 2;
			NODE_INFO_LENGTH                = buffer.readShort();           packetLength    += 2;
			dsMacAddress 					= buffer.readMacAddress();		packetLength += MacAddress.GetLength();
			sequenceNumber					= buffer.readInt();				packetLength += 4;
			uplinkTransmitRate 				= buffer.readInt();				packetLength += 4;
			staCount 						= buffer.readByte();			packetLength += 1;
		    	
			staSignalEntries = new StaSignalEntry[staCount];			
			for(int staNo=0; staNo<staCount; staNo++){
				staSignalEntries[staNo] 					= new StaSignalEntry();
				staSignalEntries[staNo].macAddress  		= buffer.readMacAddress();	packetLength += MacAddress.GetLength();
				staSignalEntries[staNo].dwnlinkTransmitRate	= buffer.readInt();			packetLength += 4;
				staSignalEntries[staNo].signal 				= buffer.readByte();		packetLength += 1;
			}
			
			flags							= buffer.readInt();		packetLength += 4;
			configSeqNumber					= buffer.readInt();		packetLength += 4;
			capabilities					= buffer.readByte();	packetLength += 1;
			configcapabilities				= buffer.readByte();	packetLength += 1;
			cmdcapabilities					= buffer.readByte();	packetLength += 1;
			
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}

    	return true;
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
     */
    public void getPacket(BufferWriter writer) {

    	writer.writeShort(NODE_INFO_ID);
    	writer.writeShort(NODE_INFO_LENGTH);
        writer.writeMacAddress(dsMacAddress);
        writer.writeInt(sequenceNumber);
        writer.writeInt(uplinkTransmitRate);
        writer.writeByte(staCount);
	    	
		for(int staNo=0; staNo<staCount; staNo++){
		    writer.writeMacAddress(staSignalEntries[staNo].macAddress);
		    writer.writeInt(staSignalEntries[staNo].dwnlinkTransmitRate);
		    writer.writeByte(staSignalEntries[staNo].signal);
		}
		
		writer.writeInt(flags);
		writer.writeInt(configSeqNumber);
		writer.writeByte(capabilities);
		writer.writeByte(configcapabilities);
		writer.writeByte(cmdcapabilities);
        
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
     */
    public int getPacketLength() {
        return packetLength;
    }
    
    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
    public byte getPacketType() {
        return PacketFactory.HEARTBEAT2;
    }    
       
    /**
     * @return Returns the staCount.
     */
    public short getStaCount() {
        return staCount;
    }
    
    /* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}
   
	/**
	 * @return Returns the dwnlinkTransmitRate.
	 */
	public long getDwnlinkTransmitRate() {
		return dwnlinkTransmitRate;
	}
	
	/**
	 * @return Returns the staSignalEntries.
	 */
	public StaSignalEntry[] getStaSignalEntries() {
		return staSignalEntries;
	}
	
	/**
	 * @return Returns the uplinkTransmitRate.
	 */
	public long getUplinkTransmitRate() {
		return uplinkTransmitRate;
	}

	/**
	 * @return Returns the sequenceNumber.
	 */
	public long getSequenceNumber() {
		return sequenceNumber;
	}
	
	/**
	 * @return Returns the capabilities.
	 */
	public short getCapabilities() {
		return capabilities;
	}

	/**
	 * @return Returns the flags.
	 */
	public long getFlags() {
		return flags;
	}
	
    /**
     * @return Returns the cmdcapabilities.
     */
    public short getCmdcapabilities() {
        return cmdcapabilities;
    }

    /**
	 * @return Returns the configcapabilities.
	 */
	public short getConfigcapabilities() {
		return configcapabilities;
	}
	
	public String  getHeartbeat2Data(){
		String dump = "";
/*		
		if(this.getAPType() == Mesh.PHY_TYPE_802_3)
			dump	 		= " " + "-";
		else
			dump    	= " "  		 + uplinkTransmitRate;
		if(this.getAPType() == Mesh.PHY_TYPE_802_3)
			dump	 		= dump + ",-";
		else{
		String dwnlinkTransmitRate = getDownlinkTransmitRate();
		dump 	 	= dump + "," + dwnlinkTransmitRate;
		}
		
		if(this.getAPType() == Mesh.PHY_TYPE_802_3)
			dump	 		= dump + ",-";
		else{
			int downlinkSgnStr = getDownlinkSignalStrength();	
			dump		= dump + "," + downlinkSgnStr;
		}
		dump	 	= dump + "," + staCount;
*/		
		return dump;
	}

/*	
	private int getDownlinkSignalStrength() {
		AccessPoint parentAp = ap.getHeartBeatInfo().getParentAP();
		if(parentAp != null && parentAp.getHeartBeat2Info() != null){
			StaSignalEntry stasiglist[] = parentAp.getHeartBeat2Info().getStaSignalEntries();
			if(stasiglist != null) {
				for (int j = 0; j < stasiglist.length; j++) {
					if(stasiglist[j].macAddress.toString().equals(dsMacAddress.toString()))
						return -96 + stasiglist[j].signal;
				}
			}
		}
		return 0;
	}

	private String getDownlinkTransmitRate() {
		AccessPoint parentAp = ap.getHeartBeatInfo().getParentAP();
		if(parentAp != null && parentAp.getHeartBeat2Info() != null){
			StaSignalEntry stasiglist[] = parentAp.getHeartBeat2Info().getStaSignalEntries();
			if(stasiglist != null) {
				for (int j = 0; j < stasiglist.length; j++) {
					if(stasiglist[j].macAddress.toString().equals(dsMacAddress.toString()))
						return (new Long(stasiglist[j].dwnlinkTransmitRate)).toString();
				}
			}
		}
		return (new Long(0)).toString();
	}
*/
	public long getConfigSeqNumber() {
		return configSeqNumber;
	}

	
}
