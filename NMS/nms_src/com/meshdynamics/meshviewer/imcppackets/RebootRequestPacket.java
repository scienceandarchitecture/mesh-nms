/**
 * MeshDynamics 
 * -------------- 
 * File     : RebootRequestPacket.java
 * Comments : 
 * Created  : May 19, 2006
 * Author   : Mithil
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -------------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author      |
 * * -------------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method added                        | Imran       |
 * -------------------------------------------------------------------------------------
 * |  1  |Jan 12, 2007 | isEncrypted method removed                      | Imran  	   |
 * -------------------------------------------------------------------------------------
 * |  0  |May 19, 2006 | Created	                                     | Mithil      |
 * -------------------------------------------------------------------------------------
 */

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class RebootRequestPacket extends IMCPPacket{
   
	private static final int PACKET_LENGTH = 10; //6+4
	
	private int   nodeId;
	private int   nodeLength;
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			nodeId       = buffer.readShort();
			nodeLength   = buffer.readShort();
			dsMacAddress = buffer.readMacAddress();	
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter writer){
		writer.writeShort(nodeId);
		writer.writeShort(nodeLength);
	    writer.writeMacAddress(dsMacAddress);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
	public byte getPacketType() {
        return 	PacketFactory.REBOOT_REQUEST_PACKET;
    }

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}
	
	public	boolean isEncrypted(){
		return !ENCRYPTED;
	}

	public int getNodeId() {
		return nodeId;
	}

	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	public int getNodeLength() {
		return nodeLength;
	}

	public void setNodeLength(int nodeLength) {
		this.nodeLength = nodeLength;
	}
	
	
}
