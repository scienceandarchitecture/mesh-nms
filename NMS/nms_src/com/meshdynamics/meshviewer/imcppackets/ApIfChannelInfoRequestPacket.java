/*
 * Created on Dec 16, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 * ------------------------------------------------------------------------------------
 * | No	 |    Date     |   Comment										 | Auther	  |
 * ------------------------------------------------------------------------------------
 * | 15  |Jan 12, 2007 | isEncrypted method removed                      | Imran  	  |
 * ------------------------------------------------------------------------------------
 */
package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class ApIfChannelInfoRequestPacket extends IMCPPacket {

	private int    info_id;
	private int    info_length;
	private String interfaceName;
	private static final int PACKET_LENGTH = 10; 

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		info_id      = buffer.readShort();
		info_length  = buffer.readShort();
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter writer) {
		writer.writeShort(info_id);
		writer.writeShort(info_length);
		writer.writeMacAddress(dsMacAddress);
		writer.writeString(interfaceName);		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {  
		//int len = interfaceName.length();// testing purpose
		int len="wlan0".length();
		return PACKET_LENGTH+len+3;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.AP_IF_CHANNEL_INFO_REQUEST_PACKET;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
	}

	/**
	 * @return Returns the interfaceName.
	 */
	public String getInterfaceName() {
		return interfaceName;
	}
	/**
	 * @param interfaceName The interfaceName to set.
	 */
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public boolean	canProcessPacket() {
		return false;
	}

	public int getInfo_id() {
		return info_id;
	}

	public void setInfo_id(int info_id) {
		this.info_id = info_id;
	}

	public int getInfo_length() {
		return info_length;
	}

	public void setInfo_length(int info_length) {
		this.info_length = info_length;
	}
	
	
}
