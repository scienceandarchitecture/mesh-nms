/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : DLSaturationInfoPacket
 * Comments : Downlink Saturation Info Packet Class for MeshViewer
 * Created  : May 12, 2006
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date          |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  4  |Mar 08, 2007  | changes for saturation info rewrite             |Abhijit |
 * --------------------------------------------------------------------------------
 * |  3  |Jan 12, 2007  | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  2  | May 31, 2006 | Misc changes 			                          |Bindu   |
 * --------------------------------------------------------------------------------
 * |  1  | May 19, 2006 | UI Updation on Packet receive                   |Bindu   |
 * --------------------------------------------------------------------------------
 * |  0  | Mar 12, 2006 | Created                                         |Bindu   |
 * --------------------------------------------------------------------------------
**********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.DateTime;
import com.meshdynamics.util.MacAddress;

public class DLSaturationInfoPacket extends IMCPPacket {
	
	private String 						IfName;
	private short 						channelCount;
	private DLSaturationChannelInfo[] 	channelInfo;	
	private int							packetLength;
	private String 						updateTime;
	
	private class DLSaturationChannelInfo {
		public short 					channel;
		public short 					apCount;
		public DLSaturationAPInfo[]		apInfo;
		public short 					maxSignal;
		public short 					avgSignal;
		public long 					totalDuration;
		public long 					retryDuration;
		public long 					txDuration;	
		
		public DLSaturationChannelInfo() {
			apInfo = null;
		}
		
		/**
		 * @param apInfo The apInfo to set.
		 */
		public void setApInfo(int count, DLSaturationAPInfo[] info) {
			this.apInfo = new DLSaturationAPInfo[count];
			for(int i = 0; i < count; i++) {
				this.apInfo[i] 					= new DLSaturationAPInfo();
				this.apInfo[i].bssid.setBytes(info[i].bssid.getBytes());
				this.apInfo[i].essid 			= info[i].essid;
				this.apInfo[i].signal 			=  info[i].signal;
				this.apInfo[i].retryDuration	= info[i].retryDuration;
				this.apInfo[i].txDuration 		= info[i].txDuration;				
			}
		}
	}
	
	private class DLSaturationAPInfo {
		public MacAddress	bssid;
		public String 		essid;
		public short 		signal;
		public long 		retryDuration;
		public long 		txDuration;
		
		public DLSaturationAPInfo() {
			bssid = new MacAddress();
			essid = "";
		}
		
	}		
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		
		updateTime	= DateTime.getDateTime(System.currentTimeMillis());
		
		try{
			packetLength 		= 0;
			dsMacAddress 		= buffer.readMacAddress();			packetLength += MacAddress.GetLength();
			IfName				= buffer.readString();				packetLength += 1 + IfName.length();
			channelCount		= buffer.readByte();				packetLength += 1; 
			channelInfo			= new DLSaturationChannelInfo[channelCount];	
			for(int i = 0; i < channelCount; i++) {
				channelInfo[i] 					= new DLSaturationChannelInfo();
				channelInfo[i].channel 			= buffer.readByte();					packetLength += 1; 
				channelInfo[i].apCount			= buffer.readByte();					packetLength += 1;
				channelInfo[i].apInfo			= new DLSaturationAPInfo[channelInfo[i].apCount];			
				channelInfo[i].maxSignal 		= buffer.readByte();					packetLength += 1; 				
				channelInfo[i].avgSignal 		= buffer.readByte();					packetLength +=	1;			
				channelInfo[i].totalDuration 	= buffer.readInt();						packetLength += 4;
				channelInfo[i].retryDuration 	= buffer.readInt();						packetLength += 4;
				channelInfo[i].txDuration 		= buffer.readInt();						packetLength += 4;

				for(int j = 0; j < channelInfo[i].apCount; j++) {
					channelInfo[i].apInfo[j] 				= new DLSaturationAPInfo();			
					channelInfo[i].apInfo[j].bssid 			= buffer.readMacAddress();	packetLength += MacAddress.GetLength();
					channelInfo[i].apInfo[j].essid 			= buffer.readString();		packetLength += 1 + channelInfo[i].apInfo[j].essid.length();
					channelInfo[i].apInfo[j].signal 		= buffer.readByte();		packetLength += 1;
					channelInfo[i].apInfo[j].retryDuration 	= buffer.readInt();			packetLength += 4;
					channelInfo[i].apInfo[j].txDuration 	= buffer.readInt();			packetLength += 4;	
				}				
			}
			
			return true;
		}catch (Exception e){
			Mesh.logException(e);
			return false;
		}
	}

	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter writer) {
		
		writer.writeMacAddress(dsMacAddress);
		writer.writeString(IfName);
		writer.writeByte(channelCount);

		for(int i = 0; i < channelCount; i++) {
			writer.writeByte(channelInfo[i].channel);
			writer.writeByte(channelInfo[i].apCount);			
			writer.writeByte(channelInfo[i].maxSignal);
			writer.writeByte(channelInfo[i].avgSignal);
			writer.writeInt(channelInfo[i].totalDuration);
			writer.writeInt(channelInfo[i].retryDuration);
			writer.writeInt(channelInfo[i].txDuration);

			for(int j = 0; j < channelInfo[i].apCount; j++) {
				writer.writeMacAddress(channelInfo[i].apInfo[j].bssid);
				writer.writeString(channelInfo[i].apInfo[j].essid);
				writer.writeByte(channelInfo[i].apInfo[j].signal);
				writer.writeInt(channelInfo[i].apInfo[j].retryDuration);
				writer.writeInt(channelInfo[i].apInfo[j].txDuration);					
			}				
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return packetLength;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.DN_LINK_SATURATION_INFO_PACKET;
	}

	/**
	 * @return Returns the ifName.
	 */
	public String getIfName() {
		return IfName;
	}

	public short getChannel(int index) {
		return channelInfo[index].channel;
	}
	
	public long getChannelTxDuration(int index) {
		return channelInfo[index].txDuration;
	}

	public long getChannelTotalDuration(int index) {
		return channelInfo[index].totalDuration;
	}

	public long getChannelRetryDuration(int index) {
		return channelInfo[index].retryDuration;
	}

	public short getChannelAPCount(int index) {
		return channelInfo[index].apCount;
	}

	public short getChannelAvgSignal(int index) {
		return channelInfo[index].avgSignal;
	}

	public short getChannelMaxSignal(int index) {
		return channelInfo[index].maxSignal;
	}
	
	public MacAddress getChannelAPBssid(int channelIndex, int apIndex) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].apInfo;
		return info[apIndex].bssid;		
	}

	public String getChannelAPEssid(int channelIndex, int apIndex) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].apInfo;
		return info[apIndex].essid;		
	}

	public short getChannelAPSignal(int channelIndex, int apIndex) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].apInfo;
		return info[apIndex].signal;		
	}

	public long getChannelAPTxDuration(int channelIndex, int apIndex) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].apInfo;
		return info[apIndex].txDuration;		
	}

	public long getChannelAPRetryDuration(int channelIndex, int apIndex) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].apInfo;
		return info[apIndex].retryDuration;		
	}

	/**
	 * @return Returns the channelCount.
	 */
	public short getChannelCount() {
		return channelCount;
	}
	
	public String getLastUpdateTime() {
		return updateTime;
	}
	
}
