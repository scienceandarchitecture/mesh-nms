package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class Firmware_SW_UPDATE_RESPONSE extends IMCPPacket {
   private final static int PACKET_LENGTH =8;
	
		   private   int      firmaware_response_id;
           private   int      firmware_response_length;
           private   short    action;
           private   String   key;
	@Override
	public boolean readPacket(BufferReader buffer) {
		
		firmaware_response_id    = buffer.readShort(); 
		firmware_response_length = buffer.readShort(); 
		dsMacAddress             = buffer.readMacAddress(); 
		action                   = buffer.readByte(); 
		if(action == Firmware_SW_UPDATE_REQUEST.SET_KEY){
		int length               = buffer.readShort();  
		key                      = buffer.readString_value(length);
		}
		
		return true;
	}

	@Override
	public void getPacket(BufferWriter buffer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getPacketLength() {
		// TODO Auto-generated method stub
		return PACKET_LENGTH;
	}

	@Override
	public byte getPacketType() {
		// TODO Auto-generated method stub
		return PacketFactory.IMCP_SW_UPDATE_RESPONSE;
	}

	@Override
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}

	public int getFirmaware_response_id() {
		return firmaware_response_id;
	}

	public void setFirmaware_response_id(int firmaware_response_id) {
		this.firmaware_response_id = firmaware_response_id;
	}

	public int getFirmware_response_length() {
		return firmware_response_length;
	}

	public void setFirmware_response_length(int firmware_response_length) {
		this.firmware_response_length = firmware_response_length;
	}

	public short getAction() {
		return action;
	}

	public void setAction(short action) {
		this.action = action;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}	
}
