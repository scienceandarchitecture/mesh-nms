package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class MgmtgwConfigRequestPacket extends IMCPPacket {
	private static final int 	PACKET_LENGTH 					= 11;
	private int      id;
	private int      length;
	private short    mgmt_gw_enable;
	private String   remoteAddress;
	private int      keylength;
	private byte[]   key;
	private int    certlength;
	private byte[]   cert;

	@Override
	public boolean readPacket(BufferReader buffer) {
		try {
			id            = buffer.readShort();
			length        = buffer.readShort();
			dsMacAddress  = buffer.readMacAddress();
			mgmt_gw_enable= buffer.readByte();
			
		} catch (Exception e) {
			Mesh.logException(e);
			return false;
		}
		return false;
	}

	@Override
	public void getPacket(BufferWriter buffer) {
	try {
		buffer.writeShort(id);
		buffer.writeShort(length);
		buffer.writeMacAddress(dsMacAddress);
		buffer.writeByte(mgmt_gw_enable);
		buffer.writeString(remoteAddress);
		buffer.writeShort(key.length);
		buffer.writeBytes(key);
		buffer.writeShort(cert.length);
		buffer.writeBytes(cert);
			} catch (Exception e) {
		Mesh.logException(e);
	}		
		}

	@Override
	public int getPacketLength() {
			return PACKET_LENGTH+remoteAddress.length()+key.length+cert.length;
	}

	@Override
	public byte getPacketType() {
		return PacketFactory.MGMT_GW_CONFIGREQUEST;
	}

	@Override
	protected void copyPacket(IMCPPacket packet) {
				
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public short getMgmt_gw_enable() {
		return mgmt_gw_enable;
	}

	public void setMgmt_gw_enable(short mgmt_gw_enable) {
		this.mgmt_gw_enable = mgmt_gw_enable;
	}

	public int getKeylength() {
		return keylength;
	}

	public void setKeylength(int keylength) {
		this.keylength = keylength;
	}
	public byte[] getKey() {
		return key;
	}

	public void setKey(byte[] key) {
		this.key = key;
	}

	public byte[] getCert() {
		return cert;
	}

	public void setCert(byte[] cert) {
		this.cert = cert;
	}

	public int getCertlength() {
		return certlength;
	}

	public void setCertlength(int certlength) {
		this.certlength = certlength;
	}

	

	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}	
	
	
}
