/**
 * MeshDynamics 
 * -------------- 
 * File     : ChannelScanPacket.java
 * Comments : 
 * Created  : Sep 24, 2004
 * Author   : abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -------------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author      |
 * -------------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method removed                      | Imran  	   |
 * -------------------------------------------------------------------------------------
 * |  1  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand       |
 * -------------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004 | Created	                                     | abhijit     |
 * -------------------------------------------------------------------------------------
 */

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class ChannelScanPacket extends IMCPPacket{
	
	private static final int PACKET_LENGTH	= 7;//7 + 2 + 2
	
	private int     channelInfo_id;
	private int     channelInfo_length;
	private short  	phyID;
	private byte 	packetType;
	
	public ChannelScanPacket(byte pktChType) {
		super();
		packetType = pktChType;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean readPacket(BufferReader buffer) {
		try {
			channelInfo_id     = buffer.readShort();
			channelInfo_length = buffer.readShort();
			dsMacAddress 	   = buffer.readMacAddress();
			phyID 			   = buffer.readByte();	
		}catch(Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter writer) {
	    writer.writeShort(channelInfo_id);
	    writer.writeShort(channelInfo_length);
	    writer.writeMacAddress(dsMacAddress);
	    writer.writeByte(phyID);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

	/**
	 * @return Returns the phyID.
	 */
	public short getPhyID() {
		return phyID;
	}
	/**
	 * @param phyID The phyID to set.
	 */
	public void setPhyID(short phyID) {
		this.phyID = phyID;
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
	public byte getPacketType() {
        return packetType;
    }

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}

	public int getChannelInfo_id() {
		return channelInfo_id;
	}

	public void setChannelInfo_id(int channelInfo_id) {
		this.channelInfo_id = channelInfo_id;
	}

	public int getChannelInfo_length() {
		return channelInfo_length;
	}

	public void setChannelInfo_length(int channelInfo_length) {
		this.channelInfo_length = channelInfo_length;
	}	
	
}
