/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApAckTimeoutRequestPacket.java
 * Comments : 
 * Created  : Nov 16, 2005
 * Author   : Mithil Wane
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * * --------------------------------------------------------------------------------
 * | 2  |Jan 12,2007  |  isEncrypted Method removed                     | Imran  |
 * --------------------------------------------------------------------------------
 * |  1  |Nov 19, 2006 | Misc changes:Changes due to StatusDisplayUI     |Abhishek|
 * ----------------------------------------------------------------------------------
 * |  0  |Nov 16, 2005 | Created                                         | Mithil |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.imcppackets.helpers.IMCPInfoIDS;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class ApAckTimeoutRequestResponsePacket extends RequestResponsePacket {

	private static final int PACKET_LENGTH = 16;//12+4

	private int apack_timout_req_id;
	private int apack_timout_req_length;
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			apack_timout_req_id        =   buffer.readShort();
			apack_timout_req_length    =   buffer.readShort();
			dsMacAddress 		       =   buffer.readMacAddress();
			requestResponseType        =   buffer.readByte();
			requestResponseID 	       =   buffer.readByte();
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter writer) {
		writer.writeShort(IMCPInfoIDS.IMCP_ACK_TIMEOUT_REQ_RES_INFO_ID);
		writer.writeShort(getPacketLength());
		writer.writeMacAddress(dsMacAddress);
		writer.writeByte(requestResponseType);
		writer.writeByte(requestResponseID);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return 	PacketFactory.AP_ACK_TIMEOUT_REQUEST_PACKET;
	}

	public int getApack_timout_req_id() {
		return apack_timout_req_id;
	}

	public void setApack_timout_req_id(int apack_timout_req_id) {
		this.apack_timout_req_id = apack_timout_req_id;
	}

	public int getApack_timout_req_length() {
		return apack_timout_req_length;
	}

	public void setApack_timout_req_length(int apack_timout_req_length) {
		this.apack_timout_req_length = apack_timout_req_length;
	}

}
