/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RFCustomChannelInfoRequestResponsePacket
 * Comments : RF Custom Channel Request Response Packet 
 * Created  : Dec 13, 2006
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  0  |Dec 13, 2006 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class RFCustomChannelInfoRequestResponsePacket extends RequestResponsePacket {

	private static final int FIXED_PACKET_LENGTH 				= 13;//9+4

	private String		ifName;
	private int         RF_CUSTOMCHANNEL_REQ_RES_ID;
	private int         RF_CUSTOMCHANNEL_REQ_RES_LENGTH;

	public void getPacket(BufferWriter buffer) {
		try{
			buffer.writeShort(RF_CUSTOMCHANNEL_REQ_RES_ID);
			buffer.writeShort(RF_CUSTOMCHANNEL_REQ_RES_LENGTH);
			buffer.writeMacAddress(dsMacAddress);
			buffer.writeByte(requestResponseType);
			buffer.writeByte(requestResponseID);
			buffer.writeString(ifName);
		} catch (Exception e){
			Mesh.logException(e);
		}
	}

	public int getPacketLength() {
		return FIXED_PACKET_LENGTH + ifName.length();
	}

	public byte getPacketType() {
		return PacketFactory.RF_CUSTOM_CHANNEL_REQUEST_RESPONSE_PACKET;
	}

	public boolean readPacket(BufferReader buffer) {
		try{
			RF_CUSTOMCHANNEL_REQ_RES_ID        = buffer.readShort();
			RF_CUSTOMCHANNEL_REQ_RES_LENGTH    = buffer.readShort();
			dsMacAddress 		= buffer.readMacAddress();
			requestResponseType = buffer.readByte();
			requestResponseID 	= buffer.readByte();
			ifName 				= buffer.readString();
			return true;
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
	}
	
	public String getIfName() {
		return ifName;
	}

	public void setIfName(String ifName) {
		this.ifName = ifName;
	}

	public int getRF_CUSTOMCHANNEL_REQ_RES_ID() {
		return RF_CUSTOMCHANNEL_REQ_RES_ID;
	}

	public void setRF_CUSTOMCHANNEL_REQ_RES_ID(int rF_CUSTOMCHANNEL_REQ_RES_ID) {
		RF_CUSTOMCHANNEL_REQ_RES_ID = rF_CUSTOMCHANNEL_REQ_RES_ID;
	}

	public int getRF_CUSTOMCHANNEL_REQ_RES_LENGTH() {
		return RF_CUSTOMCHANNEL_REQ_RES_LENGTH;
	}

	public void setRF_CUSTOMCHANNEL_REQ_RES_LENGTH(int rF_CUSTOMCHANNEL_REQ_RES_LENGTH) {
		RF_CUSTOMCHANNEL_REQ_RES_LENGTH = rF_CUSTOMCHANNEL_REQ_RES_LENGTH;
	}

	
}
