/**
 * MeshDynamics 
 * -------------- 
 * File     : FirmwareUpdatePacket.java
 * Comments : 
 * Created  : Sep 24, 2004
 * Author   : abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -------------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author      |
 * -------------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method removed                      | Imran  	   |
 * -------------------------------------------------------------------------------------
 * |  1  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand       |
 * -------------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004 | Created	                                     | abhijit     |
 * -------------------------------------------------------------------------------------
 */

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class FirmwareUpdateRequestPacket extends RequestResponsePacket {
	
	private static final int MIN_PACKET_LENGTH				= 16;//12+4
	
	private long 	fwVersion;
	private String 	userId;	
	private int     FIRMWARE_UPDATE_REQ_ID;
	private int     FIRMWARE_UPDATE_REQ_LENGTH;
	
	/**
	 * @return Returns the fwVersion.
	 */
	public long getFwVersion() {
		return fwVersion;
	}
	/**
	 * @param fwVersion The fwVersion to set.
	 */
	public void setFwVersion(long fwVersion) {
		this.fwVersion = fwVersion;
	}
	/**
	 * @return Returns the userId.
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId The userId to set.
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			FIRMWARE_UPDATE_REQ_ID            = buffer.readShort();
			FIRMWARE_UPDATE_REQ_LENGTH        = buffer.readShort();
			dsMacAddress 		    = buffer.readMacAddress();
			requestResponseID 	    = buffer.readByte();
			fwVersion			    = buffer.readInt();
			userId				    = buffer.readString();
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter writer) {
		writer.writeShort(FIRMWARE_UPDATE_REQ_ID);
		writer.writeShort(FIRMWARE_UPDATE_REQ_LENGTH);
	    writer.writeMacAddress(dsMacAddress);
	    writer.writeByte(requestResponseID);
	    writer.writeInt(fwVersion);
	    writer.writeString(userId);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return MIN_PACKET_LENGTH + userId.length();
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
	public byte getPacketType() {
        return PacketFactory.AP_FW_UPDATE_REQUEST;
    }
	
    public boolean canProcessPacket() {
    	return false;
    }
	public int getFIRMWARE_UPDATE_REQ_ID() {
		return FIRMWARE_UPDATE_REQ_ID;
	}
	public void setFIRMWARE_UPDATE_REQ_ID(int fIRMWARE_UPDATE_REQ_ID) {
		FIRMWARE_UPDATE_REQ_ID = fIRMWARE_UPDATE_REQ_ID;
	}
	public int getFIRMWARE_UPDATE_REQ_LENGTH() {
		return FIRMWARE_UPDATE_REQ_LENGTH;
	}
	public void setFIRMWARE_UPDATE_REQ_LENGTH(int fIRMWARE_UPDATE_REQ_LENGTH) {
		FIRMWARE_UPDATE_REQ_LENGTH = fIRMWARE_UPDATE_REQ_LENGTH;
	}	
    
}
