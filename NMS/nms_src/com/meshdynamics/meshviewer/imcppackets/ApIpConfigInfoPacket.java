/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApApConfigInfoPacket.java
 * Comments : AccessPoint IP configuration Info Packet IMCP(#15)
 * Created  : Sep 24, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  6  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  5  |Nov 27, 2006 | clean-up					  			         |  Bindu |
 * --------------------------------------------------------------------------------
 * |  4  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand  |
 * --------------------------------------------------------------------------------
 * |  3  |May 05, 2005 | checking for polling pkt in processPacket       | Bindu  |
 * --------------------------------------------------------------------------------
 * |  2  |Feb 11, 2005 | Change Detection-PropertyUI changes             | Anand  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 10, 2005 | verifyUpdates Func added for change detection   | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.IpAddress;

public class ApIpConfigInfoPacket extends RequestResponsePacket {

	private static final int    PACKET_LENGTH 	= 25;//21+4
		
	private String 		hostName;
	private IpAddress 	ipAddress;
	private IpAddress 	subnetMask;
	private IpAddress 	gateway;
	private int         AP_IP_CONFIG_ID;
	private int         AP_IP_CONFIG_LENGTH;

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean readPacket(BufferReader buffer) {
		try {
			AP_IP_CONFIG_ID        =   buffer.readShort();
			AP_IP_CONFIG_LENGTH    =   buffer.readShort();
			dsMacAddress 		= 	buffer.readMacAddress(); 
			requestResponseType = 	buffer.readByte(); 
			requestResponseID   =   buffer.readByte(); 
			hostName 			= 	buffer.readString(); 
	 		
			ipAddress  			= 	buffer.readIPAddress();
			subnetMask          =	buffer.readIPAddress();
			gateway				=   buffer.readIPAddress();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter buffer) {
		buffer.writeShort(AP_IP_CONFIG_ID);
		buffer.writeShort(AP_IP_CONFIG_LENGTH);
		buffer.writeMacAddress(dsMacAddress);
		buffer.writeByte(requestResponseType);
		buffer.writeByte(requestResponseID);
		buffer.writeString(hostName);
		buffer.writeIPAddress(ipAddress);
		buffer.writeIPAddress(subnetMask);
		buffer.writeIPAddress(gateway);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH + hostName.length();
	}

	/**
	 * @return Returns the hostName.
	 */
	public String getHostName() {
		return hostName;
	}
	
	/**
	 * @param hostName The hostName to set.
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	
	/**
	 * @return Returns the ipAddress.
	 */
	public IpAddress getIpAddress() {
		return ipAddress;
	}
	
	/**
	 * @param ipAddress The ipAddress to set.
	 */
	public void setIpAddress(IpAddress ipAddress) {
		if(this.ipAddress == null) {
			this.ipAddress = new IpAddress();
		}
		this.ipAddress.setBytes(ipAddress.getBytes());
	}
	
	/**
	 * @return Returns the subnetMask.
	 */
	public IpAddress getSubnetMask() {
		return subnetMask;
	}
	
	/**
	 * @param subnetMask The subnetMask to set.
	 */
	public void setSubnetMask(IpAddress subnetMask) {
		if(this.subnetMask == null) {
			this.subnetMask = new IpAddress();
		}
		this.subnetMask.setBytes(subnetMask.getBytes());
	}
	
    /**
     * @return Returns the gateway.
     */
    public IpAddress getGateway() {
        return gateway;
    }
    /**
     * @param gateway The gateway to set.
     */
    public void setGateway(IpAddress gateway) {
		if(this.gateway == null) {
			this.gateway = new IpAddress();
		}
		this.gateway.setBytes(gateway.getBytes());
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
    public byte getPacketType() {
        return PacketFactory.AP_IP_CONFIGURATION_INFO;
    }	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "IP-Configuration Packet";
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketCopy()
	 */
	protected void copyPacket(IMCPPacket packet) {
		ApIpConfigInfoPacket apIPConfigInfoNew  = (ApIpConfigInfoPacket)packet;
		apIPConfigInfoNew.requestResponseID 	= this.requestResponseID;
		apIPConfigInfoNew.requestResponseType 	= this.requestResponseType;
		apIPConfigInfoNew.hostName				= new String(hostName.getBytes());
		apIPConfigInfoNew.ipAddress.setBytes(this.ipAddress.getBytes());
		apIPConfigInfoNew.subnetMask.setBytes(this.subnetMask.getBytes());
		apIPConfigInfoNew.gateway.setBytes(this.gateway.getBytes());
	}

	public int getAP_IP_CONFIG_ID() {
		return AP_IP_CONFIG_ID;
	}

	public void setAP_IP_CONFIG_ID(int aP_IP_CONFIG_ID) {
		AP_IP_CONFIG_ID = aP_IP_CONFIG_ID;
	}

	public int getAP_IP_CONFIG_LENGTH() {
		return AP_IP_CONFIG_LENGTH;
	}

	public void setAP_IP_CONFIG_LENGTH(int aP_IP_CONFIG_LENGTH) {
		AP_IP_CONFIG_LENGTH = aP_IP_CONFIG_LENGTH;
	}
	
	
}
