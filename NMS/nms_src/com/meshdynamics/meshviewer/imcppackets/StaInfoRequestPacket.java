/**
 * MeshDynamics 
 * -------------- 
 * File     : StaInfoRequestPacket.java
 * Comments : 
 * Created  : Jan 31, 2006
 * Author   : abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -------------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author      |
 * -------------------------------------------------------------------------------------
 * |  1  |Jan 12, 2007 | isEncrypted method removed                      | Imran  	   |
 * -------------------------------------------------------------------------------------
 * |  0  |Jan 31, 2006 | Created	                                     | Mithil      |
 * -------------------------------------------------------------------------------------
 */

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.MacAddress;

public class StaInfoRequestPacket extends IMCPPacket{
   
	private static final int PACKET_LENGTH = 16; //12+4
	
	private int        staInfo_id;
	private int        staInfo_length;
	private MacAddress staMacAddress;
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			staInfo_id     = buffer.readShort();
			staInfo_length = buffer.readShort();
			dsMacAddress   = buffer.readMacAddress();
			staMacAddress  = buffer.readMacAddress();
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter writer){
	    writer.writeShort(staInfo_id);
	    writer.writeShort(staInfo_length);
		writer.writeMacAddress(dsMacAddress);
	    writer.writeMacAddress(staMacAddress);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
	public byte getPacketType() {
        return 	PacketFactory.STA_INFO_REQUEST_PACKET;
    }

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @return Returns the staMacAddress.
	 */
	public MacAddress getStaMacAddress() {
		return staMacAddress;
	}
	/**
	 * @param staMacAddress The staMacAddress to set.
	 */
	public void setStaMacAddress(MacAddress staMacAddress) {
		if(this.staMacAddress == null) {
			this.staMacAddress = new MacAddress();
		}
		
		this.staMacAddress.setBytes(staMacAddress.getBytes());
	}
	
    public boolean canProcessPacket() {
    	return false;
    }

	public int getStaInfo_id() {
		return staInfo_id;
	}

	public void setStaInfo_id(int staInfo_id) {
		this.staInfo_id = staInfo_id;
	}

	public int getStaInfo_length() {
		return staInfo_length;
	}

	public void setStaInfo_length(int staInfo_length) {
		this.staInfo_length = staInfo_length;
	}
    
}
