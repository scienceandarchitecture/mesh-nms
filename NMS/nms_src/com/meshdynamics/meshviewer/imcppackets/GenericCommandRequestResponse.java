/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : GenericCommandRequestResponse
 * Comments : Generic Command Request Response Packet Class for MeshViewer
 * Created  : Feb 2, 2006
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * ------------------------------------------------------------------------------------
 * |  3  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  2  |May 12, 2006 | Added D/L Saturation Request packet	         | Bindu  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 03, 2006 | Disable Reject Client if request received       | Mithil |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 02, 2006 | Created                                         | Abhijit |
 * --------------------------------------------------------------------------------
**********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class GenericCommandRequestResponse extends RequestResponsePacket {

	public static final int 	REJECT_CLIENTS_REQUEST_RESPONSE			= 1;
	public static final int 	SET_CT_MODE_REQUEST_RESPONSE			= 2;
	public static final int 	RESET_CT_MODE_REQUEST_RESPONSE			= 3;
	public static final int 	DNLINK_DCA_REQUEST_RESPONSE				= 4;
	public static final int 	UPLINK_DCA_REQUEST_RESPONSE				= 5;
	public static final int 	DN_LINK_SATURATION_REQUEST_RESPONSE		= 6;

	private static final short 	PACKET_LENGTH							= 13;//9+4
	
	private int 	commandType;
	private int     generic_command_req_id;
	private int     generic_command_req_length;

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		generic_command_req_id            = buffer.readShort();
		generic_command_req_length        = buffer.readShort();
		dsMacAddress			= buffer.readMacAddress();
		commandType				= buffer.readShort();
		requestResponseType		= buffer.readByte();
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter buffer) {
		buffer.writeShort(generic_command_req_id);
		buffer.writeShort(generic_command_req_length);
		buffer.writeMacAddress(dsMacAddress	);
		buffer.writeShort(commandType);
		buffer.writeByte(requestResponseType);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.GENERIC_COMMAND_REQUEST_RESPONSE_PACKET;
	}

	/**
	 * @return Returns the commandType.
	 */
	public int getCommandType() {
		return commandType;
	}
	/**
	 * @param configPktType The configPktType to set.
	 */
	public void setCommandType(int commandType) {
		this.commandType = commandType;
	}

	public int getGeneric_command_req_id() {
		return generic_command_req_id;
	}

	public void setGeneric_command_req_id(int generic_command_req_id) {
		this.generic_command_req_id = generic_command_req_id;
	}

	public int getGeneric_command_req_length() {
		return generic_command_req_length;
	}

	public void setGeneric_command_req_length(int generic_command_req_length) {
		this.generic_command_req_length = generic_command_req_length;
	}

	
}
