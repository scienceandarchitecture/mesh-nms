package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class MacAddrInfoRequestPacket extends IMCPPacket{
	private static final int BASE_PACKET_LENGTH	= 10;
	private int        macAddressInfoID;
	private int        macAddressInfoLength;
		
	@Override
	public boolean readPacket(BufferReader buffer) {
		try {
			macAddressInfoID    = buffer.readShort();
			macAddressInfoLength= buffer.readShort();
			dsMacAddress          = buffer.readMacAddress();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return true;
	}

	@Override
	public void getPacket(BufferWriter buffer) {
		buffer.writeShort(macAddressInfoID);
		buffer.writeShort(macAddressInfoLength);
		buffer.writeMacAddress(dsMacAddress);	
		}

	@Override
	public int getPacketLength() {
		// TODO Auto-generated method stub
		return BASE_PACKET_LENGTH;
	}

	@Override
	public byte getPacketType() {
		// TODO Auto-generated method stub
		return PacketFactory.IMCP_MAC_ADDR_INFO_REQUEST;
	}

	@Override
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}

	public int getMacAddressInfoID() {
		return macAddressInfoID;
	}

	public void setMacAddressInfoID(int macAddressInfoID) {
		this.macAddressInfoID = macAddressInfoID;
	}

	public int getMacAddressInfoLength() {
		return macAddressInfoLength;
	}

	public void setMacAddressInfoLength(int macAddressInfoLength) {
		this.macAddressInfoLength = macAddressInfoLength;
	}

}
