/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ACLInfoPacket.java
 * Comments : ACL Info Configuration Class for MeshViewer
 * Created  : Mar 13, 2006
 * Author   : Mithil Wane
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date          |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  2  | Jan 12, 2006 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  1  | Apr 13, 2006 | Conf not sent to board	                      |Mithil  |
 * --------------------------------------------------------------------------------
 * |  0  | Mar 13, 2006 | Created                                         |Mithil  |
 * --------------------------------------------------------------------------------
**********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;


import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import com.meshdynamics.meshviewer.imcppackets.helpers.ACLInfoDetails;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

/**
 * @author Mithil
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class ACLInfoPacket extends RequestResponsePacket {

	private static final int BASE_PACKET_LENGTH	= 13;//9+4
		
		private short 	aclCount;
		private Vector<ACLInfoDetails>	rules;
	    private int     ACL_INFO_ID;
	    private int     ACL_INFO_LENGTH;
		public ACLInfoPacket() {
			super();
			rules = new Vector<ACLInfoDetails>();
		}


		/* (non-Javadoc)
		 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
		 */
		public boolean readPacket(BufferReader buffer) {
			try{
				ACL_INFO_ID             =   buffer.readShort();
				ACL_INFO_LENGTH         =   buffer.readShort();
				dsMacAddress 			=  	buffer.readMacAddress();			
				requestResponseType 	=	buffer.readByte(); 
				requestResponseID  		=  	buffer.readByte();
				aclCount				=  	buffer.readByte();
        
				rules	 			=	new Vector<ACLInfoDetails>();
				for (int i = 0; i < aclCount; i++) {
					ACLInfoDetails info = new ACLInfoDetails();
					info.setStaMac(buffer.readMacAddress());
					
					int tag_allow_11e 	= buffer.readShort();
					int tag 			= (short)(tag_allow_11e & 0x0FFF);
					short allow 		= (short)((tag_allow_11e >> 12)& 1);
					short enable11e 	= (short)((tag_allow_11e >> 13) & 1);
					short category11e 	= (short)((tag_allow_11e >> 14) & 3);
										
					if(tag == 0xFFF)
						info.setVLANTag(-1);
					else
						info.setVLANTag(tag);
					
					info.setAllow(allow);
					info.setEnable11e(enable11e);
					info.setCategory11e(category11e);
					rules.add(info);				
				}
	        
				return true;
			} catch(Exception e){
				Mesh.logException(e);
				return false;
			}
		}

		/* (non-Javadoc)
		 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
		 */
		public void getPacket(BufferWriter buffer) {
			try {
				buffer.writeShort       (ACL_INFO_ID);
				buffer.writeShort       (ACL_INFO_LENGTH);
				buffer.writeMacAddress	(dsMacAddress);
				buffer.writeByte		(requestResponseType);
				buffer.writeByte		(requestResponseID);
				buffer.writeByte		((short)rules.size());
			
				Iterator<ACLInfoDetails> it = rules.iterator();
				while(it.hasNext()) {
					ACLInfoDetails aclDetails = (ACLInfoDetails)it.next();
					buffer.writeMacAddress	(aclDetails.getStaMac());
					int temp = 0;
					temp |= (aclDetails.getVLANTag() & 0x0FFF);
					temp |= (aclDetails.getAllow() << 12);
					temp |= (aclDetails.getEnable11e() << 13);
					temp |= (aclDetails.getCategory11e() << 14);
					buffer.writeShort(temp);
				}
			} catch(Exception  e) {
				Mesh.logException(e);
			}

		}

		/* (non-Javadoc)
		 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
		 */
		public int getPacketLength() {
			return BASE_PACKET_LENGTH + (rules.size() * 8);	//6 bytes sta mac 2 bytes vlan tag
		}

		/* (non-Javadoc)
		 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
		 */
		public byte getPacketType() {
			return PacketFactory.ACL_INFO_PACKET;		
		}

		/* (non-Javadoc)
		 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
		 */
		protected void copyPacket(IMCPPacket packet) {
			ACLInfoPacket aclInfoNew	=  (ACLInfoPacket)packet;
			aclInfoNew.requestResponseType	=  this.requestResponseType;
			aclInfoNew.requestResponseID	=  this.requestResponseID;
			aclInfoNew.aclCount				=  this.aclCount;
		
			Enumeration<ACLInfoDetails> enm = this.rules.elements();
			while(enm.hasMoreElements()) {
				ACLInfoDetails confOld			= (ACLInfoDetails)enm.nextElement();
				ACLInfoDetails confNew 	= new ACLInfoDetails();
				confOld.copyPacket(confNew);			
				aclInfoNew.rules.add(confNew);
			}		
		
		}

		/**
		 * @return Returns the interfaceCount.
		 */
		public short getACLCount() {
			return aclCount;
		}
		
		/**
		 * @param interfaceCount The interfaceCount to set.
		 */
		public void setACLCount(short aclCount) {
			resetRules();
			this.aclCount = aclCount;
			for(int i=0;i<aclCount;i++) {
				ACLInfoDetails aclInfo = new ACLInfoDetails();
				this.rules.add(i, aclInfo);
			}
		}
	
		public void resetRules() {
			aclCount = 0;
			rules.clear();
		}

	
		/**
		 * @param categoryInfo2
		 */
		public void addRule(ACLInfoDetails itf) {
			aclCount++;		
			this.rules.add(aclCount-1,itf);		
		}

		public ACLInfoDetails getRuleRefByIndex(int index) {
			if(index < 0 || index >= rules.size()){
				return null;
			}
		
			return (ACLInfoDetails) rules.get(index);
		}


		public int getACL_INFO_ID() {
			return ACL_INFO_ID;
		}


		public void setACL_INFO_ID(int aCL_INFO_ID) {
			ACL_INFO_ID = aCL_INFO_ID;
		}


		public int getACL_INFO_LENGTH() {
			return ACL_INFO_LENGTH;
		}


		public void setACL_INFO_LENGTH(int aCL_INFO_LENGTH) {
			ACL_INFO_LENGTH = aCL_INFO_LENGTH;
		}
			
		
}
