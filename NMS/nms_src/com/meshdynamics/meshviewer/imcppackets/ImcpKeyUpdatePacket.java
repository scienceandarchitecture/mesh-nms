/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ImcpKeyUpdatePacket.java
 * Comments : 
 * Created  : Oct 16, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  1  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 16, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class ImcpKeyUpdatePacket extends RequestResponsePacket {

	private static final int MIN_PACKET_LENGTH					= 13;//9+4
	
    private String 			newMeshId;
    private String			newMeshIMCPKey;
    private int             IMCPKEY_UPDATE_ID;
    private int             IMCPKEY_UPDATE_LENGTH;
    
    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
     */
    public boolean readPacket(BufferReader buffer) {

        try{
        	IMCPKEY_UPDATE_ID        = buffer.readShort();
        	IMCPKEY_UPDATE_LENGTH    = buffer.readShort();
            dsMacAddress 		= buffer.readMacAddress();
            requestResponseID	= buffer.readByte();
        	newMeshId 			= buffer.readString();
            newMeshIMCPKey		= buffer.readString();	
        }catch (Exception e){
        	Mesh.logException(e);
        	return false;
        }
    	return true;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
     */
    public void getPacket(BufferWriter writer) {
    	writer.writeShort(IMCPKEY_UPDATE_ID);
    	writer.writeShort(IMCPKEY_UPDATE_LENGTH);
	    writer.writeMacAddress(dsMacAddress);
	    writer.writeByte(requestResponseID);
	    writer.writeString(newMeshId);
	    writer.writeString(newMeshIMCPKey);
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
     */
    public int getPacketLength() {
        return MIN_PACKET_LENGTH + newMeshId.length() + newMeshIMCPKey.length();
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
    public byte getPacketType() {
        return PacketFactory.IMCP_KEY_UPDATE;
    }

	/**
	 * @return Returns the newMeshIMCPKey.
	 */
	public String getNewMeshIMCPKey() {
		return newMeshIMCPKey;
	}
	
	/**
	 * @param newMeshIMCPKey The newMeshIMCPKey to set.
	 */
	public void setNewMeshIMCPKey(String newMeshIMCPKey) {
		this.newMeshIMCPKey = newMeshIMCPKey;
	}
	
	/**
	 * @return Returns the newMeshId.
	 */
	public String getNewMeshId() {
		return newMeshId;
	}
	
	/**
	 * @param newMeshId The newMeshId to set.
	 */
	public void setNewMeshId(String newMeshId) {
		this.newMeshId = newMeshId;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		ImcpKeyUpdatePacket imcpKeyUpdNew = (ImcpKeyUpdatePacket)packet;
		imcpKeyUpdNew.newMeshId 			= new String(this.newMeshId);
		imcpKeyUpdNew.newMeshIMCPKey		= new String(this.newMeshIMCPKey);
		imcpKeyUpdNew.requestResponseID		= this.requestResponseID;		
	}

	public int getIMCPKEY_UPDATE_ID() {
		return IMCPKEY_UPDATE_ID;
	}

	public void setIMCPKEY_UPDATE_ID(int iMCPKEY_UPDATE_ID) {
		IMCPKEY_UPDATE_ID = iMCPKEY_UPDATE_ID;
	}

	public int getIMCPKEY_UPDATE_LENGTH() {
		return IMCPKEY_UPDATE_LENGTH;
	}

	public void setIMCPKEY_UPDATE_LENGTH(int iMCPKEY_UPDATE_LENGTH) {
		IMCPKEY_UPDATE_LENGTH = iMCPKEY_UPDATE_LENGTH;
	}

	
}
