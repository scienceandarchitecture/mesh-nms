/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RequestResponsePacket.java
 * Comments : 
 * Created  : Apr 17, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Apr 17, 2007  | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class RequestResponsePacket extends IMCPPacket {

    public static final short 		TYPE_REQUEST  			= 0;
    public static final short 		TYPE_RESPONSE 			= 1;
	
	protected short 	requestResponseType;
	protected short 	requestResponseID;
	
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}

	public void getPacket(BufferWriter buffer) {
		// TODO Auto-generated method stub
		
	}

	public int getPacketLength() {
		// TODO Auto-generated method stub
		return 0;
	}

	public byte getPacketType() {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean readPacket(BufferReader buffer) {
		// TODO Auto-generated method stub
		return false;
	}

	public short getRequestResponseType() {
		return requestResponseType;
	}

	public void setRequestResponseType(short requestResponseType) {
		this.requestResponseType = requestResponseType;
	}

	public short getRequestResponseID() {
		return requestResponseID;
	}

	public void setRequestResponseID(short requestResponseID) {
		this.requestResponseID = requestResponseID;
	}

	public boolean canProcessPacket() {
    	return (requestResponseType == TYPE_REQUEST) ? false : true;
    }

}
