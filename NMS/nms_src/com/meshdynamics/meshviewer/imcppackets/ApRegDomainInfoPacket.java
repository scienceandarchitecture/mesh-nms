/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApRegDmnCntCodeInfoPacket.java
 * Comments : 
 * Created  : Nov 16, 2005
 * Author   : Mithil Wane
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * ------------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  1  |Jan 04, 2006 |  System.out.println()'s removed			     | Mithil |
 * ----------------------------------------------------------------------------------
 * |  0  |Nov 16, 2005 | Created                                         | Mithil |
 * --------------------------------------------------------------------------------
**********************************************************************************/
package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class ApRegDomainInfoPacket extends RequestResponsePacket {

	private static final int PACKET_LENGTH = 16;//12+4
	
	private int 		regulatoryDomain;
	private int			countryCode;
	private int         AP_REGDOMAIN_INFO_ID;
	private int         AP_REGDOMAIN_INFO_LENGTH;
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		try {
			AP_REGDOMAIN_INFO_ID        =   buffer.readShort();
			AP_REGDOMAIN_INFO_LENGTH    =   buffer.readShort();
			dsMacAddress 		= 	buffer.readMacAddress();
			requestResponseType = 	buffer.readByte(); 
			requestResponseID   =   buffer.readByte();
			regulatoryDomain 	= 	buffer.readShort(); 
			countryCode   		=   buffer.readShort(); 
		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter buffer) {
		
		buffer.writeShort(AP_REGDOMAIN_INFO_ID);
		buffer.writeShort(AP_REGDOMAIN_INFO_LENGTH);
		buffer.writeMacAddress(dsMacAddress);
		buffer.writeByte(requestResponseType);
		buffer.writeByte(requestResponseID);
		buffer.writeShort(regulatoryDomain);
		buffer.writeShort(countryCode);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		ApRegDomainInfoPacket apRegDmnCntCodeInfoPacket = (ApRegDomainInfoPacket)packet;
		apRegDmnCntCodeInfoPacket.requestResponseType = this.requestResponseType;
		apRegDmnCntCodeInfoPacket.requestResponseID = this.requestResponseID;
		apRegDmnCntCodeInfoPacket.regulatoryDomain = this.regulatoryDomain;
		apRegDmnCntCodeInfoPacket.countryCode = this.countryCode;
	}
	
	/**
	 * @return Returns the countryCode.
	 */
	public int getCountryCode() {
		return countryCode;
	}
	
	/**
	 * @param countryCode The countryCode to set.
	 */
	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}
	
	/**
	 * @return Returns the regulatoryDomain.
	 */
	public int getRegulatoryDomain() {
		return regulatoryDomain;
	}
	
	/**
	 * @param regulatoryDomain The regulatoryDomain to set.
	 */
	public void setRegulatoryDomain(int regulatoryDomain) {
		this.regulatoryDomain = regulatoryDomain;
	}

	public int getAP_REGDOMAIN_INFO_ID() {
		return AP_REGDOMAIN_INFO_ID;
	}

	public void setAP_REGDOMAIN_INFO_ID(int aP_REGDOMAIN_INFO_ID) {
		AP_REGDOMAIN_INFO_ID = aP_REGDOMAIN_INFO_ID;
	}

	public int getAP_REGDOMAIN_INFO_LENGTH() {
		return AP_REGDOMAIN_INFO_LENGTH;
	}

	public void setAP_REGDOMAIN_INFO_LENGTH(int aP_REGDOMAIN_INFO_LENGTH) {
		AP_REGDOMAIN_INFO_LENGTH = aP_REGDOMAIN_INFO_LENGTH;
	}	
	
	
}
