/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamConfigPacket.java
 * Comments : 
 * Created  : Feb 20, 2007
 * Author   : Abhishek Patankar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author   |
 * ----------------------------------------------------------------------------------
 * |  0  | Jul 09, 2007 | Created                                         | Abhishek/|
 * |	 |				|												  | Imran    |	
 **********************************************************************************/


package com.meshdynamics.meshviewer.imcppackets;

import java.util.Enumeration;
import java.util.Hashtable;

import com.meshdynamics.meshviewer.imcppackets.helpers.EffistreamHelper;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;
import com.meshdynamics.util.Range;



public class EffistreamConfigPacket extends RequestResponsePacket {

	private static final 	int    	PACKET_LENGTH 			= 20;//10+4
	/**
	 *
	 *
	 |0	|1 | 2|	3| 4| 5| 6| 7|
	 |0	|AP DS MAC ADDRESS
	 |6	|REQUEST ID 
	 |12|RESPONSE ID
	 |18|ROOT CHILD COUNT
	 
	 Root Child count is the count of rules
	 +
	 ------------------- 
	 Rule Information
	 -------------------
	 |0  |1 | 2| 3| 4| 5| 6| 7|
	 |0  |CRITERIA ID 
	 |6  |VALUE						 
	 |12+|CHILD COUNT
	 |value count
	 
	 Value can be of any of the four types mentioned below
	 
	 1. Long value (4 bytes)
	 2. Mac Address(6 bytes)
	 3. Ip Address (4 bytes)
	 4. Range	   (8 bytes)
	 
	 If rule has childcount = 0, it follows action
	 depending upon the child count, rule information is iterated in packet.
	 
	 +  
	 -------------------- 
	 Action Information
	 --------------------
	 |0  |1 | 2| 3| 4| 5| 6| 7|
	 |0  |              |DP|NA|   DP = Drop Packet
	 |6  |DOT 11E CATEGORY        NA = No Ack
	 |12 |BIT RATE 
	 |18 |RESERVED        
	 |24 |RESERVED
	 |30 |RESERVED
	 |36 |RESERVED
	 **/	 

    private int         EFFISTREAM_CONFIG_ID;
    private int         EFFISTREAM_CONFIG_LENGTH;
	private RuleInfo	rulesRoot;
	
	public class RuleInfo {
		
		public short					criteriaId;
		public Object 					value;
		public short					childCount;
		public Hashtable<String, RuleInfo>				childrenHash;
		public ActionInfo				action;
		public boolean					isRoot;
		
		
		public RuleInfo() {
			init();
			childrenHash		= new Hashtable<String, RuleInfo>();
		}

		private void init() {
			this.criteriaId 	= 0;
			this.value			= "";
			this.childCount		= 0;
			this.action			= null;
			this.isRoot			= false;
		}
		
		public RuleInfo addRule() {
			RuleInfo	newChild	= new RuleInfo();
			childrenHash.put(newChild.toString(),newChild);
			return newChild;
		}
		
		public ActionInfo setAction() {
			action	= new ActionInfo();
			return action;
		}
		
		public void clearRuleData() {
			init();
			childrenHash.clear();
		}
		
	}
	public class ActionInfo {
		
		public short	noAck; 			
		public short 	dropPacket;
		public short 	dot11eCategory;
		public short	bitRate;
		public short	queuedRetry;
		
		public ActionInfo() {
			this.dropPacket 	= 0;
			this.dot11eCategory = 0xFF;
			this.noAck 			= 0;
			this.bitRate		= 0;
			this.queuedRetry	= 0;
		}
	}
	
	
	public EffistreamConfigPacket() {
		/**
		 * 1st rule is Root rule 
		 */
		rulesRoot 			= new RuleInfo();		
		rulesRoot.isRoot	= true;
	}
	
	public void getPacket(BufferWriter buffer) {
		buffer.writeShort       (EFFISTREAM_CONFIG_ID);
		buffer.writeShort       (EFFISTREAM_CONFIG_LENGTH);
		buffer.writeMacAddress	(dsMacAddress);
		buffer.writeByte		(requestResponseType);
		buffer.writeByte		(requestResponseID);
		buffer.writeByte		(rulesRoot.childCount);		
		
		Enumeration<RuleInfo>	enumRulesRoot	= rulesRoot.childrenHash.elements();
		while(enumRulesRoot.hasMoreElements()){
			RuleInfo child	= (RuleInfo) enumRulesRoot.nextElement();
			writeTreeToPacket(buffer, child);
		}
    }
	
	
	private int writeTreeToPacket(BufferWriter buffer, RuleInfo child) {
		
		short			options;
		int				length;
		short			zero;
		
		options 	= 0;		
		length		= 0;
		
		if(buffer != null) 
			buffer.writeByte(child.criteriaId);		
		length++;
		
		length	+= setValueFromId(child.criteriaId, child.value, buffer);
		
		if(buffer != null)
			buffer.writeByte(child.childCount);
		length++;
		
		if(child.childCount > 0 ) {
			Enumeration<RuleInfo> enumChild	= child.childrenHash.elements();
			
			while(enumChild.hasMoreElements()) {
				RuleInfo	newchild	= (RuleInfo) enumChild.nextElement();
				length	+= writeTreeToPacket(buffer, newchild);
			}
		
		} else {
			zero		= 0;
			if(child.action == null) {
				return length;
			}
			options		|= (child.action.noAck);
			options		|= (short)(child.action.dropPacket << 1);
			options		|= (short)(child.action.queuedRetry << 2);
			if(buffer != null)
				buffer.writeByte(options);
			length++;
			if(buffer != null)
				buffer.writeByte(child.action.dot11eCategory);
			length++;
			if(buffer != null)
				buffer.writeByte(child.action.bitRate);
			length++;
			
			if(buffer != null)
				for(int i = 0; i < 4; i++)
					buffer.writeByte(zero);
			length += 5;
		}
		
		return length;
	}

	public int getPacketLength() {
		
		int 			packetLength;
		packetLength 	 = PACKET_LENGTH;
		packetLength	+= getTreeSize();
		return packetLength;
	}

	public byte getPacketType() {
		return PacketFactory.EFFISTREAM_CONFIG_PACKET;
	}

	private int getTreeSize() {
		return writeTreeToPacket(null,this.rulesRoot);
	}
	
	public boolean readPacket(BufferReader buffer) {

		try {			
			EFFISTREAM_CONFIG_ID            = buffer.readShort();
			EFFISTREAM_CONFIG_LENGTH        = buffer.readShort();
			dsMacAddress 			= buffer.readMacAddress();
		   	requestResponseType		= buffer.readByte(); 
	        requestResponseID  		= buffer.readByte();
	        rulesRoot	 			= new RuleInfo();
	        rulesRoot.isRoot		= true;
	        rulesRoot.childCount	= (buffer.readByte());
	        
	        for(int i = 0; i < rulesRoot.childCount; i++) {
	        	RuleInfo child	= rulesRoot.addRule();
	        	readTreeFromPacket(buffer, child);
	        }	        
	        
		} catch(Exception e) {						
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/**
	 * @param buffer
	 * @param child
	 */
	private void readTreeFromPacket(BufferReader buffer, RuleInfo child) {
		
		short 				options;
		short 				dot11eCategory;
		short 				noAck;
		short				dropPacket;
		short				bitRate;
		short				queuedRetry;
				
		options 		= 0;
		dot11eCategory 	= 0xFF;
		bitRate			= 0;
		queuedRetry		= 0;
		
		child.criteriaId 		= buffer.readByte();
		child.value 			= getValueFromId(child.criteriaId, buffer);
		child.childCount 		= buffer.readByte();
		
		for(int i = 0; i < child.childCount; i++) {
		
			RuleInfo	newChild	= child.addRule();
			readTreeFromPacket(buffer, newChild);
		}
		if(child.childCount == 0) {
				
				options 		= buffer.readByte();
				noAck			= (short)(options & 0x01);
				dropPacket		= (short)((options >> 1) & 0x01);
				queuedRetry		= (short)((options >> 2) & 0x01);
				dot11eCategory 	= buffer.readByte();
				bitRate			= buffer.readByte();
					
				child.action 				= new ActionInfo();
				child.action.dot11eCategory	= dot11eCategory;
				child.action.dropPacket		= dropPacket;
				child.action.noAck			= noAck;
				child.action.bitRate		= bitRate;
				child.action.queuedRetry	= queuedRetry;
				
			for(int i = 0; i < 4; i++) {
				buffer.readByte();
			}
		 }
	}
	
	private int setValueFromId(short matchId, Object matchValue, BufferWriter buffer) {
		
		int		length;
		Range 	range;
		long 	value;
		
		range	= null;
		value	= 0;
		length	= 0;
		
		switch(matchId) {
		
		case EffistreamHelper.ID_TYPE_ETH_TYPE:	
		case EffistreamHelper.ID_TYPE_IP_TOS:
		case EffistreamHelper.ID_TYPE_IP_DIFFSRV:
		case EffistreamHelper.ID_TYPE_IP_PROTO:
		case EffistreamHelper.ID_TYPE_RTP_VERSION:
		case EffistreamHelper.ID_TYPE_RTP_PAYLOAD:			
			length	+= 4;
			if(buffer != null) {
				value = ((Long)matchValue).longValue();
				buffer.writeInt(value);
			}
			break;
			
		case EffistreamHelper.ID_TYPE_ETH_DST:
		case EffistreamHelper.ID_TYPE_ETH_SRC:
			if(buffer != null)
				buffer.writeMacAddress((MacAddress)matchValue);
			length	+= 6;
			break;
			
		case EffistreamHelper.ID_TYPE_IP_SRC:
		case EffistreamHelper.ID_TYPE_IP_DST:
			if(buffer != null)
				buffer.writeIPAddress((IpAddress)matchValue);
			length	+= 4;
			break;
			
		case EffistreamHelper.ID_TYPE_UDP_SRC_PORT:
		case EffistreamHelper.ID_TYPE_UDP_DST_PORT:
		case EffistreamHelper.ID_TYPE_UDP_LENGTH:
		case EffistreamHelper.ID_TYPE_TCP_SRC_PORT:
		case EffistreamHelper.ID_TYPE_TCP_DST_PORT:
		case EffistreamHelper.ID_TYPE_TCP_LENGTH:
		case EffistreamHelper.ID_TYPE_RTP_LENGTH:
			
			if(buffer != null) {
				range = (Range)matchValue;
				buffer.writeInt(range.getLowerLimit());
				buffer.writeInt(range.getUpperLimit());
			}
			length	+= 8;
			break;
		}

		return length;
	}

	private Object getValueFromId(short matchId, BufferReader buffer) {
		
		Range 	range;
		Long	longValue;
		
		range 		= null;
		longValue 	= null;
		
		switch(matchId) {
	
		case EffistreamHelper.ID_TYPE_ETH_TYPE:
		case EffistreamHelper.ID_TYPE_IP_PROTO:
		case EffistreamHelper.ID_TYPE_IP_TOS:
		case EffistreamHelper.ID_TYPE_IP_DIFFSRV:
		case EffistreamHelper.ID_TYPE_RTP_VERSION:
		case EffistreamHelper.ID_TYPE_RTP_PAYLOAD:
			longValue = new Long(buffer.readInt());
			return longValue;
			
		case EffistreamHelper.ID_TYPE_ETH_DST:
		case EffistreamHelper.ID_TYPE_ETH_SRC:
			return buffer.readMacAddress();
			
		case EffistreamHelper.ID_TYPE_IP_SRC:
		case EffistreamHelper.ID_TYPE_IP_DST:
			return buffer.readIPAddress();
			
		case EffistreamHelper.ID_TYPE_UDP_SRC_PORT:
		case EffistreamHelper.ID_TYPE_UDP_DST_PORT:
		case EffistreamHelper.ID_TYPE_UDP_LENGTH:
		case EffistreamHelper.ID_TYPE_TCP_SRC_PORT:
		case EffistreamHelper.ID_TYPE_TCP_DST_PORT:
		case EffistreamHelper.ID_TYPE_TCP_LENGTH:
		case EffistreamHelper.ID_TYPE_RTP_LENGTH:
			range = new Range();
			range.setLowerLimit(buffer.readInt());
			range.setUpperLimit(buffer.readInt());
			return range;

		}
		return null;
	}
	
	
	public RuleInfo getRulesRoot() {
		return rulesRoot;
	}

	public int getEFFISTREAM_CONFIG_ID() {
		return EFFISTREAM_CONFIG_ID;
	}

	public void setEFFISTREAM_CONFIG_ID(int eFFISTREAM_CONFIG_ID) {
		EFFISTREAM_CONFIG_ID = eFFISTREAM_CONFIG_ID;
	}

	public int getEFFISTREAM_CONFIG_LENGTH() {
		return EFFISTREAM_CONFIG_LENGTH;
	}

	public void setEFFISTREAM_CONFIG_LENGTH(int eFFISTREAM_CONFIG_LENGTH) {
		EFFISTREAM_CONFIG_LENGTH = eFFISTREAM_CONFIG_LENGTH;
	}	
	
	
}
