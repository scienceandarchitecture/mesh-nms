/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : FirmwareUpdateResponsePacket.java
 * Comments : 
 * Created  : Nov 3, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  1  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Nov 3, 2004  | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;


public class FirmwareUpdateResponsePacket extends RequestResponsePacket {
	
	private static final int PACKET_LENGTH					= 8;

	public static final byte FWUR_NOT_READY_FOR_UPDATE		= 0;
	public static final byte FWUR_READY_FOR_UPDATE			= 1;
	public static final byte FWUR_UPDATE_ALREADY_DONE		= 2;
	public static final byte FWUR_UPDATE_INVALID_USER		= 3;
	
	private int  FIRMWARE_UPDATE_RES_ID;
	private int  FIRMWARE_UPDATE_RES_LENGTH;
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		FIRMWARE_UPDATE_RES_ID        = buffer.readShort();
		FIRMWARE_UPDATE_RES_LENGTH    = buffer.readShort();
		dsMacAddress 		= buffer.readMacAddress();
		requestResponseType	= buffer.readByte();
		requestResponseID 	= buffer.readByte(); 
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter writer) {
		writer.writeShort(FIRMWARE_UPDATE_RES_ID);
		writer.writeShort(FIRMWARE_UPDATE_RES_LENGTH);
		writer.writeMacAddress(dsMacAddress);
		writer.writeByte(requestResponseType);
		writer.writeByte(requestResponseID);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.AP_FW_UPDATE_RESPONSE;
	}

	public int getFIRMWARE_UPDATE_RES_ID() {
		return FIRMWARE_UPDATE_RES_ID;
	}

	public void setFIRMWARE_UPDATE_RES_ID(int fIRMWARE_UPDATE_RES_ID) {
		FIRMWARE_UPDATE_RES_ID = fIRMWARE_UPDATE_RES_ID;
	}

	public int getFIRMWARE_UPDATE_RES_LENGTH() {
		return FIRMWARE_UPDATE_RES_LENGTH;
	}

	public void setFIRMWARE_UPDATE_RES_LENGTH(int fIRMWARE_UPDATE_RES_LENGTH) {
		FIRMWARE_UPDATE_RES_LENGTH = fIRMWARE_UPDATE_RES_LENGTH;
	}


}
