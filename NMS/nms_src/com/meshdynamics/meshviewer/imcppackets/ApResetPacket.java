/**
 * MeshDynamics 
 * -------------- 
 * File     : ApResetPacket.java
 * Comments : 
 * Created  : Sep 24, 2004
 * Author   : abhijit
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -------------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author      |
 * -------------------------------------------------------------------------------------
 * |  2  |Jan 12, 2007 | isEncrypted method removed                      | Imran  	   |
 * -------------------------------------------------------------------------------------
 * |  1  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand       |
 * -------------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004 | Created	                                     | abhijit     |
 * -------------------------------------------------------------------------------------
 */
package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class ApResetPacket extends IMCPPacket {
	
	private static final int PACKET_LENGTH					= 11;
	
	public static final byte RESET_TYPE_REBOOT_SYSTEM		= 1;
	public static final byte RESET_TYPE_RESTART_MESH		= 2;
	public static final byte RESET_TYPE_HALT_MESH           = 3;
	
	private int   info_id;
	private int   info_length;
	private short resetType;
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			info_id      = buffer.readShort();
			info_length  = buffer.readShort();
			dsMacAddress = buffer.readMacAddress();
			resetType    = buffer.readByte();	
		}catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
	 */
	public void getPacket(BufferWriter writer) {
		writer.writeShort(info_id);
		writer.writeShort(info_length);
		writer.writeMacAddress(dsMacAddress);
	    writer.writeByte(resetType);
	}

	/**
	 * @return Returns the resetType.
	 */
	public short getResetType() {
		return resetType;
	}
	/**
	 * @param resetType The resetType to set.
	 */
	public void setResetType(short resetType) {
		this.resetType = resetType;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	
	public int getPacketLength() {
		return PACKET_LENGTH;
	}

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
	public byte getPacketType() {
        return PacketFactory.AP_RESET_REQUEST;
    }

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#isEncrypted()
	 */
	public boolean isEncrypted() {
		// TODO Auto-generated method stub
		return true;
	}
	
    public boolean canProcessPacket() {
    	return false;
    }

     // according to new imcp structure
	public int getInfo_id() {
		return info_id;
	}

	public void setInfo_id(int info_id) {
		this.info_id = info_id;
	}

	public int getInfo_length() {
		return info_length;
	}


	public void setInfo_length(int info_length) {
		this.info_length = info_length;
	}
    
    
 
}
