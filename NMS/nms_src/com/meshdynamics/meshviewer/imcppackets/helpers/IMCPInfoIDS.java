package com.meshdynamics.meshviewer.imcppackets.helpers;

public class IMCPInfoIDS {
	
	public final static int IMCP_NODE_INFO_ID	                              = 1;
	
	public final static int IMCP_IF_802_3_INFO_ID	                          = 2;
	
	public final static int IMCP_IF_802_11_INFO_ID	                          = 3;
	
	public final static int IMCP_IF_802_15_4_INFO_ID	                      = 4;
	
	public final static int IMCP_HEARTBEAT_INFO_ID	                          = 5;
	
	public final static int IMCP_STA_ASSOC_NOTIFICATION_INFO_ID	              = 6;
	
	public final static int IMCP_STA_DISASSOC_NOTIFICATION_INFO_ID	          = 7;
	
	public final static int IMCP_BUFFER_COMMAND_INFO_ID	                      = 8;
	
	public final static int IMCP_BUFFER_COMMAND_ACK_ID	                      = 9;
	
	public final static int IMCP_CHANNEL_SCAN_LOCK_INFO_ID	                  = 10;
	
	public final static int IMCP_CHANNEL_SCAN_UNLOCK_INFO_ID	              = 11;
	
	public final static int IMCP_CHANNEL_SCAN_LOCK_OBJECTION_INFO_ID	      = 12;
	
	public final static int IMCP_RESET_INFO_ID	                              = 13;
	
	public final static int IMCP_AP_HARDWARE_INFO_ID	                      = 14;
	
	public final static int IMCP_AP_HARDWARE_INFO_REQUEST_INFO_ID	          = 15;
	
	public final static int IMCP_AP_SUB_TREE_INFO_ID	                      = 16;
	
	public final static int IMCP_IP_CONFIG_INFO_ID	                          = 17;
	
	public final static int IMCP_IP_CONFIG_INFO_REQ_RES_INFO_ID	              = 18;
	
	public final static int IMCP_SW_UPDATE_REQUEST_INFO_ID	                  = 19;
	
	public final static int IMCP_SW_UPDATE_RESPONSE_INFO_ID	                  = 20;
	
	public final static int IMCP_SW_UPDATE_STATUS_INFO_ID	                  = 21;
	
	public final static int IMCP_MESH_INIT_STATUS_INFO_ID	                  = 22;
	
	public final static int IMCP_MESH_IMCP_KEY_UPDATE_INFO_ID	              = 23;
	
	public final static int IMCP_RESPONSE_PACKET_INFO_ID	                  = 24;
	
	public final static int IMCP_VLAN_CONFIG_INFO_ID	                      = 25;
	
	public final static int IMCP_CHANNEL_CHANGE_INFO_ID	                      = 26;
	
	public final static int IMCP_CONFIGD_WATCHDOG_REQUEST_INFO_ID	          = 27;
	
	public final static int IMCP_CONFIGD_WATCHDOG_RESPONSE_INFO_ID	          = 28;
	
	public final static int IMCP_DS_EXCERCISER_INFO_ID	                      = 29;
	
	public final static int IMCP_WM_INFO_REQUEST_ID	                          = 30;
	
	public final static int IMCP_WM_INFO_RESPONSE_ID	                      = 31;
	
	public final static int IMCP_SET_TESTER_TESTEE_MODE_REQUEST_INFO_ID	      = 32;
	
	public final static int IMCP_HEARTBEAT2_INFO_ID	                          = 33;
	
	public final static int IMCP_RESTORE_DEFAULTS_REQ_INFO_ID	              = 34;
	
	public final static int IMCP_REG_DOMAIN_REQ_RES_INFO_ID	                  = 35;
	
	public final static int IMCP_REG_DOMAIN_INFO_ID	                          = 36;
	
	public final static int IMCP_ACK_TIMEOUT_REQ_RES_INFO_ID	              = 37;
	
	public final static int IMCP_ACK_TIMEOUT_INFO_ID	                      = 38;
	
	public final static int IMCP_IF_SUPPORTED_CHANNELS_REQUEST_INFO_ID        = 39;
	
	public final static int IMCP_IF_SUPPORTED_CHANNELS_INFO_ID	              = 40;
	
	public final static int IMCP_STA_INFO_REQUEST_INFO_ID	                  = 41;
	
	public final static int IMCP_STA_INFO_RESPONSE_ID	                      = 42;
	
	public final static int IMCP_DOT11E_CATEGORY_INFO_ID	                  = 43;
	
	public final static int IMCP_DOT11E_CONFIG_INFO_ID	                      = 44;
	
	public final static int IMCP_GENERIC_COMMAND_REQ_RES_INFO_ID	          = 45;
	
	public final static int IMCP_PRIVATE_CHANNEL_INFO_ID	                  = 46;
	
	public final static int IMCP_EFFISTREAM_REQ_RES_INFO_ID	                  = 47;
	
	public final static int IMCP_EFFISTREAM_INFO_ID	                          = 48;
	
	public final static int IMCP_RADIO_DIAGNOSTIC_COMMAND_ID	              = 49;
	
	public final static int IMCP_RADIO_DIAGNOSTIC_DATA_ID	                  = 50;
	
	public final static int IMCP_RADIO_DATA_REQ_RES_ID	                      = 51;
	
	public final static int IMCP_RADIO_DATA_INFO_ID	                          = 52;
	
	public final static int IMCP_NODE_MOVE_NOTIFICATION_INFO_ID	              = 53;
	
	public final static int IMCP_CLIENT_SIGNAL_INFO_ID	                      = 54;
	
	public final static int IMCP_SIP_PRIVATE_INFO_ID	                      = 55;
	
	public final static int IMCP_SIP_CONFIG_INFO_ID	                          = 56;
	
	public final static int IMCP_CAPABILITIES_INFO_ID                         = 57;
	
	public final static int IMCP_AP_CONFIGURATION_REQ_RES_ID	              = 58;
	
	public final static int IMCP_HIDE_SSID_CONFIG_INFO_ID                    = 59;
	
	public final static int IMCP_HIDE_SSID_CONFIG_INFO_REQ_RES_INFO_ID       = 60;
	
	public final static int IMCP_CONFIG_UPDATED_ID                           = 61;
	
	public final static int IMCP_ACL_CONFIG_INFO_ID                          = 62;
	
	public final static int IMCP_ACL_CONFIG_INFO_REQ_RES_INFO_ID             = 63;
	
	public final static int IMCP_SIP_CONFIG_INFO_REQ_RES_INFO_ID             = 64;
	
	public final static int IMCP_PRIVATE_CHANNEL_INFO_REQ_RES_INFO_ID        = 65;
	
	public final static int IMCP_FIRMWARE_REQUEST_ID                         = 66;
	
	public final static int IMCP_IF_802_11_EXT_11N_INFO_ID                       = 111;

	public final static int IMCP_IF_802_11_EXT_11N_11AC_INFO_ID                  = 112;

}
