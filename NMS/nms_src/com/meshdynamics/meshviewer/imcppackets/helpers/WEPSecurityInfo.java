/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : WEPSecurityInfo.java
 * Comments : 
 * Created  : Feb 28, 2005
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  4  |Apr 22, 2005 | WepIndex bug for 128-Bit fixed(copying of keys) | Bindu  |
 * |	 |			   | Default keys changed to zero.   		         | 		  | 
 * --------------------------------------------------------------------------------
 * |  3  |Apr 20, 2005 | WEPPassPhrase field added & Misc Fixes done     | Bindu  |
 * --------------------------------------------------------------------------------
 * |  2  |Mar 02, 2005 | WEP_64_BIT_KEY Bug Fixed				     	 | Bindu  |
 * ----------------------------------------------------------------------------------
 * |  1  |Mar 01, 2005 | Changed WEP_64_BIT_KEY_LENGTH & Misc Changes	 | Bindu  |
 * ----------------------------------------------------------------------------------
 * |  0  |Feb 28, 2005 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets.helpers;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class WEPSecurityInfo {

	public static final short 		TYPE_WEP 						= 2;
	public static final int    		WEP_64_BIT						= 40;	
	public static final int    		WEP_64_BIT_KEY_LENGTH			= 20;
	public static final int    		WEP_64_BIT_SINGLE_KEY_LENGTH	= 5;
	public static final int    		WEP_128_BIT						= 104;
	public static final int    		WEP_128_BIT_KEY_LENGTH			= 13;
	//public static final short[] 	WEP_KEY0 = {0x88, 0x90, 0xD1, 0x76, 0xFD};
	//public static final short[] 	WEP_KEY1 = {0xF4, 0x9A, 0x73, 0x39, 0xE2};
	//public static final short[] 	WEP_KEY2 = {0x26, 0x13, 0xD4, 0xE9, 0xD3};
	//public static final short[] 	WEP_KEY3 = {0x05, 0x4B, 0xDA, 0x3F, 0x46};
	public static final short[] 	WEP_KEY0 = {0x00, 0x00, 0x00, 0x00, 0x00};
	public static final short[] 	WEP_KEY1 = {0x00, 0x00, 0x00, 0x00, 0x00};
	public static final short[] 	WEP_KEY2 = {0x00, 0x00, 0x00, 0x00, 0x00};
	public static final short[] 	WEP_KEY3 = {0x00, 0x00, 0x00, 0x00, 0x00};
	
	private short 		WEPStrength;
	private short 		WEPKeyIndex;
	private short[]		WEPKey0;
	private short[]		WEPKey1;
	private short[]		WEPKey2;
	private short[]		WEPKey3;
	private String		WEPPassPhrase;
	
	/**
	 * 
	 */
	public WEPSecurityInfo() {
		WEPStrength			= WEP_64_BIT;
		WEPKeyIndex			= 0;
		WEPKey0				= WEP_KEY0;
		WEPKey1				= WEP_KEY1;
		WEPKey2				= WEP_KEY2;
		WEPKey3				= WEP_KEY3;
		WEPPassPhrase		= "";
	}
	
	/**
	 * @return Returns the wEPKey0.
	 */
	public short[] getWEPKey0() {
		return WEPKey0;
	}
	/**
	 * @param key0 The wEPKey0 to set.
	 */
	public void setWEPKey0(short[] key0) {
		WEPKey0 = key0;
	}
	/**
	 * @return Returns the wEPKey1.
	 */
	public short[] getWEPKey1() {
		return WEPKey1;
	}
	/**
	 * @param key1 The wEPKey1 to set.
	 */
	public void setWEPKey1(short[] key1) {
		WEPKey1 = key1;
	}
	/**
	 * @return Returns the wEPKey2.
	 */
	public short[] getWEPKey2() {
		return WEPKey2;
	}
	/**
	 * @param key2 The wEPKey2 to set.
	 */
	public void setWEPKey2(short[] key2) {
		WEPKey2 = key2;
	}
	/**
	 * @return Returns the wEPKey3.
	 */
	public short[] getWEPKey3() {
		return WEPKey3;
	}
	/**
	 * @param key3 The wEPKey3 to set.
	 */
	public void setWEPKey3(short[] key3) {
		WEPKey3 = key3;
	}
	/**
	 * @return Returns the wEPKeyIndex.
	 */
	public short getWEPKeyIndex() {
		return WEPKeyIndex;
	}
	/**
	 * @param keyIndex The wEPKeyIndex to set.
	 */
	public void setWEPKeyIndex(short keyIndex) {
		WEPKeyIndex = keyIndex;
	}
	/**
	 * @return Returns the wEPStrength.
	 */
	public short getWEPStrength() {
		return WEPStrength;
	}
	/**
	 * @param strength The wEPStrength to set.
	 */
	public void setWEPStrength(short strength) {
		WEPStrength = strength;
	}

	public void copyPacket(WEPSecurityInfo wepInfo) {
		wepInfo.WEPStrength			= this.WEPStrength;
		wepInfo.WEPKeyIndex 		= this.WEPKeyIndex;
		if(this.WEPStrength == WEP_64_BIT) {
			
			wepInfo.WEPKey0 = new short[WEP_64_BIT_SINGLE_KEY_LENGTH];
			wepInfo.WEPKey1 = new short[WEP_64_BIT_SINGLE_KEY_LENGTH];
			wepInfo.WEPKey2 = new short[WEP_64_BIT_SINGLE_KEY_LENGTH];
			wepInfo.WEPKey3 = new short[WEP_64_BIT_SINGLE_KEY_LENGTH];
			
			for(int i = 0; i < WEP_64_BIT_SINGLE_KEY_LENGTH; i++)
				wepInfo.WEPKey0[i]	= this.WEPKey0[i];
			
			for(int i = 0; i < WEP_64_BIT_SINGLE_KEY_LENGTH; i++)
				wepInfo.WEPKey1[i]	= this.WEPKey1[i];
			
			for(int i = 0; i < WEP_64_BIT_SINGLE_KEY_LENGTH; i++)
				wepInfo.WEPKey2[i]	= this.WEPKey2[i];
			
			for(int i = 0; i < WEP_64_BIT_SINGLE_KEY_LENGTH; i++)
				wepInfo.WEPKey3[i]	= this.WEPKey3[i];
			
		}else if(this.WEPStrength == WEP_128_BIT) {
			switch(this.WEPKeyIndex) {
			case 0:
				wepInfo.WEPKey0		= new short[WEP_128_BIT_KEY_LENGTH];				
				for(int i = 0; i < WEP_128_BIT_KEY_LENGTH; i++)
					wepInfo.WEPKey0[i]	= this.WEPKey0[i];
				break;
			case 1:
				wepInfo.WEPKey0		= new short[WEP_128_BIT_KEY_LENGTH];				
				for(int i = 0; i < WEP_128_BIT_KEY_LENGTH; i++)
					wepInfo.WEPKey0[i]	= this.WEPKey1[i];
				break;
			case 2:
				wepInfo.WEPKey0		= new short[WEP_128_BIT_KEY_LENGTH];				
				for(int i = 0; i < WEP_128_BIT_KEY_LENGTH; i++)
					wepInfo.WEPKey0[i]	= this.WEPKey2[i];
				break;
			case 3:
				wepInfo.WEPKey0		= new short[WEP_128_BIT_KEY_LENGTH];				
				for(int i = 0; i < WEP_128_BIT_KEY_LENGTH; i++)
					wepInfo.WEPKey0[i]	= this.WEPKey3[i];
				break;				
			}						
			wepInfo.WEPKey1		= wepInfo.WEPKey0;
			wepInfo.WEPKey2		= wepInfo.WEPKey0;
			wepInfo.WEPKey3		= wepInfo.WEPKey0;				
		}	
		
		wepInfo.WEPPassPhrase 	= this.WEPPassPhrase;
	}
	
	public void getPacket(BufferWriter buffer) {
		buffer.writeByte(WEPStrength);
		buffer.writeByte(WEPKeyIndex);
		
		if(WEPStrength == WEP_64_BIT) {
			
			buffer.writeByte((short)WEP_64_BIT_KEY_LENGTH);			
			for(int i = 0; i < WEP_64_BIT_SINGLE_KEY_LENGTH; i++)
				buffer.writeByte(WEPKey0[i]);
			
			for(int i = 0; i < WEP_64_BIT_SINGLE_KEY_LENGTH; i++)
				buffer.writeByte(WEPKey1[i]);
			
			for(int i = 0; i < WEP_64_BIT_SINGLE_KEY_LENGTH; i++)
				buffer.writeByte(WEPKey2[i]);
			
			for(int i = 0; i < WEP_64_BIT_SINGLE_KEY_LENGTH; i++)
				buffer.writeByte(WEPKey3[i]);
			
		} else if(WEPStrength == WEP_128_BIT) {
			
			buffer.writeByte((short)WEP_128_BIT_KEY_LENGTH);
			
			switch(WEPKeyIndex) {
			case 0:
				for(int i = 0; i < WEP_128_BIT_KEY_LENGTH; i++)
					buffer.writeByte(WEPKey0[i]);
				break;
			case 1:
				for(int i = 0; i < WEP_128_BIT_KEY_LENGTH; i++)
					buffer.writeByte(WEPKey1[i]);
				break;
			case 2:
				for(int i = 0; i < WEP_128_BIT_KEY_LENGTH; i++)
					buffer.writeByte(WEPKey2[i]);
				break;
			case 3:
				for(int i = 0; i < WEP_128_BIT_KEY_LENGTH; i++)
					buffer.writeByte(WEPKey3[i]);
				break;				
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {		
		try {		
			
			WEPStrength		= buffer.readByte();
			WEPKeyIndex		= buffer.readByte();
			if(WEPStrength == WEP_64_BIT) {
				int i 		= 0;
				WEPKey0		= new short[WEP_64_BIT_SINGLE_KEY_LENGTH];
				WEPKey1		= new short[WEP_64_BIT_SINGLE_KEY_LENGTH];
				WEPKey2		= new short[WEP_64_BIT_SINGLE_KEY_LENGTH];
				WEPKey3		= new short[WEP_64_BIT_SINGLE_KEY_LENGTH];
				short length = buffer.readByte();
				byte[] data = new byte[length];//TODO BINDU CHECKING FOR LENGTH
				data = buffer.getBytes(WEP_64_BIT_KEY_LENGTH);
				
				for(; i < WEP_64_BIT_SINGLE_KEY_LENGTH; i++)
					WEPKey0[i]	= data[i];
				
				for(int j = 0; i < WEP_64_BIT_SINGLE_KEY_LENGTH * 2; i++, j++)
					WEPKey1[j]	= data[i];
				
				for(int j = 0; i < WEP_64_BIT_SINGLE_KEY_LENGTH * 3; i++, j++)
					WEPKey2[j]	= data[i];
				
				for(int j = 0; i < WEP_64_BIT_SINGLE_KEY_LENGTH * 4; i++, j++)
					WEPKey3[j]	= data[i];
				
			} else if(WEPStrength == WEP_128_BIT){
				short length = buffer.readByte();
				byte[] data = new byte[length];
									
				WEPKey0		= new short[WEP_128_BIT_KEY_LENGTH];				
				data 		= buffer.getBytes(WEP_128_BIT_KEY_LENGTH);
				for(int i = 0; i < WEP_128_BIT_KEY_LENGTH; i++)
					WEPKey0[i]	= data[i];
				WEPKey1		= WEPKey0;
				WEPKey2		= WEPKey0;
				WEPKey3		= WEPKey0;
			}
		}catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
	}
	
	/**
	 * @return Returns the wEPPassPhrase.
	 */
	public String getWEPPassPhrase() {
		return WEPPassPhrase;
	}
	/**
	 * @param passPhrase The wEPPassPhrase to set.
	 */
	public void setWEPPassPhrase(String passPhrase) {
		WEPPassPhrase = passPhrase;
	}
}
