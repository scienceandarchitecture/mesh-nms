/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RSNPSKSecurityInfo.java
 * Comments : 
 * Created  : Feb 28, 2005
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  4  |Sep 28, 2005 | default Key set to 0	                        | Bindu  |
 * --------------------------------------------------------------------------------
 * |  3  |May 05, 2005 | CCMP selected by default                        | Bindu  |
 * --------------------------------------------------------------------------------
 * |  2  |Apr 20, 2005 | Misc Fixes                                      | Bindu  |
 * --------------------------------------------------------------------------------
 * |  1  |Apr 13, 2005 | Added PassPhrase field                          | Bindu  |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 28, 2005 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets.helpers;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class RSNPSKSecurityInfo {

	public static final short TYPE_RSNPSK = 3;
	public static final short RSNPSK_GRP_REN = 30;
	public static final short[] RSNPSK_KEY = 
			{0x9E, 0xA4, 0x4A, 0x6F, 0x16, 0xE8, 0x18, 0x88, 0x4D, 0x31, 
			 0x31, 0x04, 0xB8, 0x99, 0xA9, 0x18, 0xF9, 0x16, 0x7B, 0xB7, 
			 0x53, 0x8B, 0x14, 0x16, 0xC3, 0x75, 0x2E, 0xFE, 0xC1, 0xB5, 
			 0x45, 0xC4, 0xBA, 0xBC, 0x2F, 0xA7, 0xC7, 0x2F, 0x88, 0xA9}; 
		
	
	private short 		PSKMode;
	private	int 		PSKKeyLength;
	private short[] 	PSKKey;
	private short 		PSKCipherTKIP;
	private short 		PSKCipherCCMP;
	private int 		PSKGroupKeyRenewal;
	private String 		PSKPassPhrase;
	

	/**
	 * 
	 */
	public RSNPSKSecurityInfo() {
		PSKMode					= 0;
		PSKKeyLength			= RSNPSK_KEY.length;
		//PSKKey					= RSNPSK_KEY;
		PSKKey					= new short[40];
		PSKCipherTKIP			= SecurityInfo.NOT_ENABLED;
		PSKCipherCCMP			= SecurityInfo.ENABLED;	
		PSKGroupKeyRenewal		= RSNPSK_GRP_REN;
		PSKPassPhrase			= "";
	}
	
	
	/**
	 * @return Returns the pSKCipherCCMP.
	 */
	public short getPSKCipherCCMP() {
		return PSKCipherCCMP;
	}
	/**
	 * @param cipherCCMP The pSKCipherCCMP to set.
	 */
	public void setPSKCipherCCMP(short cipherCCMP) {
		PSKCipherCCMP = cipherCCMP;
	}
	/**
	 * @return Returns the pSKCipherTKIP.
	 */
	public short getPSKCipherTKIP() {
		return PSKCipherTKIP;
	}
	/**
	 * @param cipherTKIP The pSKCipherTKIP to set.
	 */
	public void setPSKCipherTKIP(short cipherTKIP) {
		PSKCipherTKIP = cipherTKIP;
	}
	
	/**
	 * @return Returns the pSKGroupKeyRenewal.
	 */
	public int getPSKGroupKeyRenewal() {
		return PSKGroupKeyRenewal;
	}
	/**
	 * @param groupKeyRenewal The pSKGroupKeyRenewal to set.
	 */
	public void setPSKGroupKeyRenewal(int groupKeyRenewal) {
		PSKGroupKeyRenewal = groupKeyRenewal;
	}
	/**
	 * @return Returns the pSKKey.
	 */
	public short[] getPSKKey() {
		return PSKKey;
	}
	/**
	 * @param key The pSKKey to set.
	 */
	public void setPSKKey(short[] key) {
		PSKKey = key;
	}
	/**
	 * @return Returns the pSKMode.
	 */
	public short getPSKMode() {
		return PSKMode;
	}
	/**
	 * @param mode The pSKMode to set.
	 */
	public void setPSKMode(short mode) {
		PSKMode = mode;
	}

	/**
	 * @return Returns the pSKKeyLength.
	 */
	public int getPSKKeyLength() {
		return PSKKeyLength;
	}
	/**
	 * @param keyLength The pSKKeyLength to set.
	 */
	public void setPSKKeyLength(int keyLength) {
		PSKKeyLength = keyLength;
	}

	public void copyPacket(RSNPSKSecurityInfo newRSNPSKInfo) {
		
		newRSNPSKInfo.PSKMode				= this.PSKMode;
		newRSNPSKInfo.PSKKeyLength			= this.PSKKeyLength;
		newRSNPSKInfo.PSKKey				= this.PSKKey;
		newRSNPSKInfo.PSKCipherTKIP			= this.PSKCipherTKIP;
		newRSNPSKInfo.PSKCipherCCMP			= this.PSKCipherCCMP;
		newRSNPSKInfo.PSKGroupKeyRenewal	= this.PSKGroupKeyRenewal;
		newRSNPSKInfo.PSKPassPhrase 		= this.PSKPassPhrase;
	}
	
	public void getPacket(BufferWriter buffer) {
		
		buffer.writeByte(PSKMode);
		buffer.writeByte((short)PSKKeyLength);
		for(int i = 0; i < PSKKeyLength; i++)
			buffer.writeByte(PSKKey[i]);			
		
		buffer.writeByte(PSKCipherTKIP);
		buffer.writeByte(PSKCipherCCMP);
		buffer.writeInt(PSKGroupKeyRenewal);					
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {		
		try {		

			PSKMode				= buffer.readByte();
			PSKKeyLength		= buffer.readByte();
			PSKKey				= new short[PSKKeyLength];
			for(int i = 0; i < PSKKeyLength; i++)
				PSKKey[i]		= buffer.readByte();
			
			PSKCipherTKIP		= buffer.readByte();
			PSKCipherCCMP		= buffer.readByte();
			PSKGroupKeyRenewal	= (int) buffer.readInt();
		}catch (Exception e){
			Mesh.logException(e);
			return false; 
		}
		return true;
	}


	/**
	 * @param passPhrase
	 */
	public void setPSKPassPhrase(String passPhrase) {
		this.PSKPassPhrase = passPhrase;
		
	}	
	/**
	 * @return Returns the pSKPassPhrase.
	 */
	public String getPSKPassPhrase() {
		return PSKPassPhrase;
	}
}
