/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InterfaceHiddenESSIDInfo.java
 * Comments : 
 * Created  : Feb 08, 2005
 * Author   : Mithil Wane
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 08, 2005 | Created                                         | Mithil |
 * --------------------------------------------------------------------------------
 **********************************************************************************/
package com.meshdynamics.meshviewer.imcppackets.helpers;


public class IfHiddenESSIDInfo {

	public 	short 	hidden;
	public 	String 	ifName;
	
	public IfHiddenESSIDInfo(){
		hidden = 0;
		ifName = new String();
	}
}
