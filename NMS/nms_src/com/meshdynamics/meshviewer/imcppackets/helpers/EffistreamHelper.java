/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamHelper.java
 * Comments : 
 * Created  : Jul 9, 2007
 * Author   : Abhishek
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  | Jul 9, 2007 | Created                                         |Abhishek|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets.helpers;

import com.meshdynamics.meshviewer.configuration.IEffistreamRuleCriteria;
import com.meshdynamics.meshviewer.imcppackets.EffistreamConfigPacket;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;
import com.meshdynamics.util.Range;


public class EffistreamHelper {

	public static final 	short 	ID_TYPE_ETH_TYPE		= 1;
    public static final 	short 	ID_TYPE_ETH_DST			= 2;
	public static final 	short 	ID_TYPE_ETH_SRC			= 3;
    public static final 	short 	ID_TYPE_IP_TOS 			= 4;
	public static final 	short 	ID_TYPE_IP_DIFFSRV		= 5;
    public static final 	short 	ID_TYPE_IP_SRC 			= 6;
	public static final 	short 	ID_TYPE_IP_DST 			= 7;
    public static final 	short 	ID_TYPE_IP_PROTO		= 8;
	public static final 	short 	ID_TYPE_UDP_SRC_PORT	= 9;
    public static final 	short 	ID_TYPE_UDP_DST_PORT	= 10;
	public static final 	short 	ID_TYPE_UDP_LENGTH		= 11;
    public static final 	short 	ID_TYPE_TCP_SRC_PORT 	= 12;
	public static final 	short 	ID_TYPE_TCP_DST_PORT	= 13;
    public static final 	short 	ID_TYPE_TCP_LENGTH 		= 14;
	public static final 	short 	ID_TYPE_RTP_VERSION 	= 15;
    public static final 	short 	ID_TYPE_RTP_PAYLOAD		= 16;
    public static final 	short 	ID_TYPE_RTP_LENGTH		= 17;

    public static final 	short 	VALUE_TYPE_NONE			= 0;
	public static final 	short 	VALUE_TYPE_NUM 			= 1;
	public static final 	short 	VALUE_TYPE_RANGE 		= 2;
    public static final 	short 	VALUE_TYPE_MAC_ADDRESS	= 3;
    public static final 	short 	VALUE_TYPE_IP_ADDRESS	= 4;
    
    

    public static int getValueTypeFromId(short matchId) {
		
		switch(matchId) {
		
		case ID_TYPE_ETH_TYPE:
		case ID_TYPE_IP_TOS:
		case ID_TYPE_IP_DIFFSRV:
		case ID_TYPE_IP_PROTO:
		case ID_TYPE_RTP_VERSION:
		case ID_TYPE_RTP_PAYLOAD:
			return VALUE_TYPE_NUM;
			
		case ID_TYPE_ETH_DST:
		case ID_TYPE_ETH_SRC:
			return VALUE_TYPE_MAC_ADDRESS;
			
		case ID_TYPE_IP_SRC:
		case ID_TYPE_IP_DST:
			return VALUE_TYPE_IP_ADDRESS;
			
		case ID_TYPE_UDP_SRC_PORT:
		case ID_TYPE_UDP_DST_PORT:
		case ID_TYPE_UDP_LENGTH:
		case ID_TYPE_TCP_SRC_PORT:
		case ID_TYPE_TCP_DST_PORT:
		case ID_TYPE_TCP_LENGTH:
		case ID_TYPE_RTP_LENGTH:
			return VALUE_TYPE_RANGE;
		}

		return 0;
	}
    
    public static void copyValueFromId(IEffistreamRuleCriteria value, Object obj, short criteriaId) {
		
		switch(criteriaId) {
	
		case ID_TYPE_ETH_TYPE:	
		case ID_TYPE_IP_TOS:
		case ID_TYPE_IP_DIFFSRV:
		case ID_TYPE_IP_PROTO:
		case ID_TYPE_RTP_VERSION:
		case ID_TYPE_RTP_PAYLOAD:
			 value.setLongValue(((Long)obj).longValue());	
			 break;
			
		case ID_TYPE_ETH_DST:
		case ID_TYPE_ETH_SRC:
			 value.setMacAddressValue(((MacAddress)obj).getBytes());
			 break;
			
		case ID_TYPE_IP_SRC:
		case ID_TYPE_IP_DST:
			 value.setIPAddressValue(((IpAddress)obj).getBytes());
			 break;
			
		case ID_TYPE_UDP_SRC_PORT:
		case ID_TYPE_UDP_DST_PORT:
		case ID_TYPE_UDP_LENGTH:
		case ID_TYPE_TCP_SRC_PORT:
		case ID_TYPE_TCP_DST_PORT:
		case ID_TYPE_TCP_LENGTH:
		case ID_TYPE_RTP_LENGTH:
			 value.setRangeValue(((Range)obj).getLowerLimit(),
					((Range)obj).getUpperLimit());
			break;
		}
	}

    public static void copyToValueFromId(EffistreamConfigPacket.RuleInfo rule, IEffistreamRuleCriteria value, short criteriaId) {
		
		switch(criteriaId) {
	
		case ID_TYPE_ETH_TYPE:	
		case ID_TYPE_IP_TOS:
		case ID_TYPE_IP_DIFFSRV:
		case ID_TYPE_IP_PROTO:
		case ID_TYPE_RTP_VERSION:
		case ID_TYPE_RTP_PAYLOAD:
			rule.value	= value.getLongValue();		
			break;
			
		case ID_TYPE_ETH_DST:
		case ID_TYPE_ETH_SRC:
			MacAddress	macAddress = new MacAddress();
			macAddress.setBytes(value.getMacAddressValue().getBytes());
			rule.value	= macAddress;
			break;
			
		case ID_TYPE_IP_SRC:
		case ID_TYPE_IP_DST:
			 IpAddress	ipAddress = new IpAddress();
			 ipAddress.setBytes(value.getIPAddressValue().getBytes());
			 rule.value = ipAddress;
			break;
			
		case ID_TYPE_UDP_SRC_PORT:
		case ID_TYPE_UDP_DST_PORT:
		case ID_TYPE_UDP_LENGTH:
		case ID_TYPE_TCP_SRC_PORT:
		case ID_TYPE_TCP_DST_PORT:
		case ID_TYPE_TCP_LENGTH:
		case ID_TYPE_RTP_LENGTH:
			Range	range	= new Range();
			range.setLowerLimit(value.getRangeValue().getLowerLimit());
			range.setUpperLimit(value.getRangeValue().getUpperLimit());
			rule.value = range;
			break;
		}
	}
    
    /**
     * key = criteriaID_(String RuleCriteriaValue)
     */
    public static String generateKey(IEffistreamRuleCriteria value) {
    	String	key;
    	key 	=  ""+value.getCriteria() +"_";
    	
		switch(value.getCriteria()) {
		
		case ID_TYPE_ETH_TYPE:
		case ID_TYPE_IP_TOS:
		case ID_TYPE_IP_DIFFSRV:
		case ID_TYPE_IP_PROTO:
		case ID_TYPE_RTP_VERSION:
		case ID_TYPE_RTP_PAYLOAD:
			return key	+= value.getLongValue().toString();
			
		case ID_TYPE_ETH_DST:
		case ID_TYPE_ETH_SRC:
			return key	+= value.getMacAddressValue().toString();
			
		case ID_TYPE_IP_SRC:
		case ID_TYPE_IP_DST:
			return key	+= value.getIPAddressValue().toString();
			
		case ID_TYPE_UDP_SRC_PORT:
		case ID_TYPE_UDP_DST_PORT:
		case ID_TYPE_UDP_LENGTH:
		case ID_TYPE_TCP_SRC_PORT:
		case ID_TYPE_TCP_DST_PORT:
		case ID_TYPE_TCP_LENGTH:
		case ID_TYPE_RTP_LENGTH:
			Range	range =  value.getRangeValue();
			key			  += ""+range.getLowerLimit()+":";
			key     	  += range.getUpperLimit();
			return key;
		}

		return "";
	}

}
