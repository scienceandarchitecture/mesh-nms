/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApIfChannelInfo
 * Comments : AccessPoint COnfiguration ApIfChannelInfo 
 * Created  : Dec 16, 2005
 * Author   : Sneha Puranam
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |Nov 27, 2006 | clean-up					  			         |  Bindu |
 * --------------------------------------------------------------------------------
 * |  0  |Dec 16, 2005 | Created                                         | Sneha  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets.helpers;

public class ApIfChannelInfo {

	public short 		channelNumber;
	public short		maxRegPower;
	public short		maxPower;
	public short		minPower;
	public long 		channelFrequency;
	public long 		flags;
	
}
