/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Dot11eCategoryDetails
 * Comments : 802.11e IfInfo Configuration Class for MeshViewer
 * Created  : Feb 13, 2006
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date          |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  | Apr 13, 2006 | Null Exception while handling category name	  |Mithil  |
 * -------------------------------------------------------------------------------- 
 * |  0  | Feb 13, 2006 | Created                                         |Bindu   |
 * --------------------------------------------------------------------------------
**********************************************************************************/
package com.meshdynamics.meshviewer.imcppackets.helpers;

public class Dot11eCategoryDetails {
	
	/**
	 * @param categoryName
	 * @param burstTime
	 * @param wmin
	 * @param wmax
	 * @param aifsn
	 * @param disableBackoff
	 */
	public Dot11eCategoryDetails() {
		this.categoryName = "";
	}
	
	public String	categoryName;
	public long 	burstTime;
	public int 		aCWmin;
	public int 		aCWmax;
	public short 	aifsn;
	public short 	disableBackoff;	
	
}
