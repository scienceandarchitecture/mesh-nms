package com.meshdynamics.meshviewer.imcppackets.helpers;

import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;



public class InterfaceAdvanceInfo {
	public  static final int    INTERFACE_ADVANCEINFO_LENGTH_FOR_N	   = 25;
	public  static final int    INTERFACE_ADVANCEINFO_LENGTH_FOR_AC	   = 29;
	public  static final int    INTERFACE_ADVANCEINFO_LENGTH_FOR_N_AC  = 29;
	
	public final  static   int  protocalid_N    = 1;
	public final  static   int  protocalid_AC   = 2;
	public final  static   int  protocalid_N_AC = 3;

	private int    protocal_Info_ID;
	private int    protocal_Info_len;
	private short  channelBandwidth;
	private long   secondaryChannelPosition;
	private short  guardInterval_20;
	private short  guardInterval_40;
	private short  guardInterval_80;
/*	private short  bandWidth_20;
	private short  bandWidth_40;
	private short  bandWidth_80;*/
	private short  frameAggregation;
	private long   MaxAMPDU;
	private long   MaxAMSDU;
	private short  txSTBC;
	private short  rxSTBC;
	private short  ldpc;
	private short  gfMode;
	private short  coexistence;
	private long    MaxMPDU;
	private short  MaxAMPDUEnabled;
	private int    protocolID;
	
	public void copyPacket(InterfaceAdvanceInfo newAdvanceInfo){
		newAdvanceInfo.channelBandwidth         = this.channelBandwidth;
		newAdvanceInfo.secondaryChannelPosition = this.secondaryChannelPosition;
		newAdvanceInfo.guardInterval_20         = this.guardInterval_20;
		newAdvanceInfo.guardInterval_40         = this.guardInterval_40;
		newAdvanceInfo.guardInterval_80         = this.guardInterval_80;
		newAdvanceInfo.frameAggregation         = this.frameAggregation;
		newAdvanceInfo.ldpc                     = this.ldpc;
		newAdvanceInfo.txSTBC                   = this.txSTBC;
		newAdvanceInfo.rxSTBC                   = this.rxSTBC;
		newAdvanceInfo.MaxAMPDU                 = this.MaxAMPDU;
		newAdvanceInfo.MaxAMSDU                 = this.MaxAMSDU;
		newAdvanceInfo.MaxMPDU                  = this.MaxMPDU;
		newAdvanceInfo.MaxAMPDUEnabled          = this.MaxAMPDUEnabled;
		newAdvanceInfo.protocolID               = this.protocolID;
	}
	
	public void getPacket(BufferWriter buffer){
		try {
			buffer.writeShort(protocal_Info_ID);
			buffer.writeShort(protocal_Info_len);
			buffer.writeByte(ldpc);
			buffer.writeByte(channelBandwidth);
			buffer.writeInt(secondaryChannelPosition);
			buffer.writeByte(guardInterval_20);
		    buffer.writeByte(guardInterval_40);
			buffer.writeByte(txSTBC);
			buffer.writeByte(rxSTBC);
			buffer.writeByte(gfMode);
			buffer.writeInt(MaxAMSDU);
			buffer.writeByte(coexistence);
			buffer.writeByte(MaxAMPDUEnabled);
			buffer.writeInt(MaxAMPDU);	
			if(getProtocolID() == protocalid_AC || 
			   getProtocolID() == protocalid_N_AC){
			buffer.writeInt(MaxMPDU);
			buffer.writeByte(guardInterval_80);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public boolean readPacket(BufferReader buffer) {
		try {
			protocal_Info_ID         = buffer.readShort();
			protocal_Info_len        = buffer.readShort();
			ldpc                     = buffer.readByte();
			channelBandwidth         = buffer.readByte();
			secondaryChannelPosition = buffer.readInt();
			guardInterval_20         = buffer.readByte();
			guardInterval_40         = buffer.readByte();
			txSTBC                   = buffer.readByte();
			rxSTBC                   = buffer.readByte();
			gfMode                   = buffer.readByte();
			MaxAMSDU                 = buffer.readInt();
			coexistence              = buffer.readByte();
			MaxAMPDUEnabled          = buffer.readByte();
			MaxAMPDU                 = buffer.readInt();
			if(protocal_Info_ID ==(long)IMCPInfoIDS.IMCP_IF_802_11_EXT_11N_11AC_INFO_ID){
				MaxMPDU              = buffer.readInt();
				guardInterval_80   = buffer.readByte();
			}
			 
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}	
	
	public int getProtocal_Info_ID() {
		return protocal_Info_ID;
	}

	public void setProtocal_Info_ID(int protocal_Info_ID) {
		this.protocal_Info_ID = protocal_Info_ID;
	}

	public int getProtocal_Info_len() {
		return protocal_Info_len;
	}

	public void setProtocal_Info_len(int protocal_Info_len) {
		this.protocal_Info_len = protocal_Info_len;
	}

	public short getChannelBandwidth() {
		return channelBandwidth;
	}

	public void setChannelBandwidth(short channelBandwidth) {
		this.channelBandwidth = channelBandwidth;
	}

	public long getSecondaryChannelPosition() {
		return secondaryChannelPosition;
	}

	public void setSecondaryChannelPosition(long secondaryChannelPosition) {
		this.secondaryChannelPosition = secondaryChannelPosition;
	}

	public short getGuardInterval_20() {
		return guardInterval_20;
	}

	public void setGuardInterval_20(short guardInterval_20) {
		this.guardInterval_20 = guardInterval_20;
	}

	public short getGuardInterval_40() {
		return guardInterval_40;
	}

	public void setGuardInterval_40(short guardInterval_40) {
		this.guardInterval_40 = guardInterval_40;
	}

	public short getFrameAggregation() {
		return frameAggregation;
	}

	public void setFrameAggregation(short frameAggregation) {
		this.frameAggregation = frameAggregation;
	}

	public long getMaxAMPDU() {
		return MaxAMPDU;
	}

	public void setMaxAMPDU(long maxAMPDU) {
		MaxAMPDU = maxAMPDU;
	}

	public long getMaxAMSDU() {
		return MaxAMSDU;
	}

	public void setMaxAMSDU(long maxAMSDU) {
		MaxAMSDU = maxAMSDU;
	}

	public short getTxSTBC() {
		return txSTBC;
	}

	public void setTxSTBC(short txSTBC) {
		this.txSTBC = txSTBC;
	}

	public short getRxSTBC() {
		return rxSTBC;
	}

	public void setRxSTBC(short rxSTBC) {
		this.rxSTBC = rxSTBC;
	}

	public short getLdpc() {
		return ldpc;
	}

	public void setLdpc(short ldpc) {
		this.ldpc = ldpc;
	}

	public short getGfMode() {
		return gfMode;
	}

	public void setGfMode(short gfMode) {
		this.gfMode = gfMode;
	}

	public short getCoexistence() {
		return coexistence;
	}

	public void setCoexistence(short coexistence) {
		this.coexistence = coexistence;
	}

	public long getMaxMPDU() {
		return MaxMPDU;
	}

	public void setMaxMPDU(long maxMPDU) {
		MaxMPDU = maxMPDU;
	}

	public int getProtocolID() {
		return protocolID;
	}

	public void setProtocolID(int protocolID) {
		this.protocolID = protocolID;
	}

	public short getMaxAMPDUEnabled() {
		return MaxAMPDUEnabled;
	}

	public void setMaxAMPDUEnabled(short maxAMPDUEnabled) {
		MaxAMPDUEnabled = maxAMPDUEnabled;
	}

	public short getGuardInterval_80() {
		return guardInterval_80;
	}

	public void setGuardInterval_80(short guardInterval_80) {
		this.guardInterval_80 = guardInterval_80;
	}
	
	
	
}
