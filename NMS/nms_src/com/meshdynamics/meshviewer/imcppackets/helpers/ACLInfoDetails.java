/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ACLInfoDeatils.java
 * Comments : ACL Info Configuration Class for MeshViewer
 * Created  : Mar 13, 2006
 * Author   : Mithil Wane
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date          |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  | Mar 13, 2006 | Created                                         |Mithil  |
 * --------------------------------------------------------------------------------
**********************************************************************************/
package com.meshdynamics.meshviewer.imcppackets.helpers;

import com.meshdynamics.util.MacAddress;

public class ACLInfoDetails {
	
	private MacAddress  staMacAddress;
	private int 		VLANTag;
	private short		allow;
	private short 		enable11e;
	private short 		category11e;
	
	public ACLInfoDetails(){
		staMacAddress = new MacAddress();
		VLANTag = -1;
		allow = 0;
	}
	/**
	 * @return Returns the allow.
	 */
	public short getAllow() {
		return allow;
	}

	/**
	 * @param allow The allow to set.
	 */
	public void setAllow(short allow) {
		this.allow = allow;
	}

	/**
	 * @return Returns the staMac.
	 */
	public MacAddress getStaMac() {
		return staMacAddress;
	}

	/**
	 * @param staMac The staMac to set.
	 */
	public void setStaMac(MacAddress staMac) {
		this.staMacAddress = staMac;
	}

	/**
	 * @return Returns the vLANTag.
	 */
	public int getVLANTag() {
		return VLANTag;
	}

	/**
	 * @param tag The vLANTag to set.
	 */
	public void setVLANTag(int tag) {
		VLANTag = tag;
	}

	public void copyPacket(ACLInfoDetails dest) {
		dest.staMacAddress	= staMacAddress;
		dest.VLANTag = VLANTag;
		dest.allow = allow;
		dest.enable11e= enable11e;
		dest.category11e= category11e;
	}
	
	/**
	 * @return Returns the category11e.
	 */
	public short getCategory11e() {
		return category11e;
	}
	/**
	 * @return Returns the enable11e.
	 */
	public short getEnable11e() {
		return enable11e;
	}
	/**
	 * @param category11e The category11e to set.
	 */
	public void setCategory11e(short category11e) {
		this.category11e = category11e;
	}
	/**
	 * @param enable11e The enable11e to set.
	 */
	public void setEnable11e(short enable11e) {
		this.enable11e = enable11e;
	}
}
