/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApAckInterfaceTimeoutInfo.java
 * Comments : 
 * Created  : Nov 16, 2005
 * Author   : Mithil Wane
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Nov 16, 2005 | Created                                         | Mithil |
 * --------------------------------------------------------------------------------
 **********************************************************************************/
package com.meshdynamics.meshviewer.imcppackets.helpers;

public class ApAckInterfaceTimeoutInfo {

	public short ackTimeout;
	public String ifName;
	
	public ApAckInterfaceTimeoutInfo(){
		ackTimeout 	= 0;
	}

}
