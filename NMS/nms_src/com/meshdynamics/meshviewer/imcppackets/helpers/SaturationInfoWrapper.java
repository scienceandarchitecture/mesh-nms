/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : APDLSaturationInfo
 * Comments : AccessPoint COnfiguration Daturation Info Packet 
 * Created  : Sep 24, 2004
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |Nov 27, 2006 | clean-up					  			         |  Bindu |
 * --------------------------------------------------------------------------------
 * |  0  |May 12, 2006 | Created                                         | Bindu  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets.helpers;

import java.util.Hashtable;

import com.meshdynamics.meshviewer.imcppackets.DLSaturationInfoPacket;

public class SaturationInfoWrapper {
	private int 						wmCount;
	private Hashtable<String, DLSaturationInfoPacket>					saturationInfo;
	
	public SaturationInfoWrapper() {
		saturationInfo 	= new Hashtable<String, DLSaturationInfoPacket>();
		wmCount			= 0;
	}
	
	/**
	 * @return Returns the wmCount.
	 */
	public int getWmCount() {
		return wmCount;		
	}
	
	/**
	 * @return Returns the saturationInfo.
	 */
	public DLSaturationInfoPacket getSaturationInfo(String ifName) {
		return (DLSaturationInfoPacket) saturationInfo.get(ifName);
	}
	
	public void setSaturationInfo(String ifName,DLSaturationInfoPacket packet) {
		saturationInfo.put(ifName, packet);
		wmCount = saturationInfo.size();
	}
	
	public void clear() {
		wmCount = 0;
		saturationInfo.clear();
	}

}
