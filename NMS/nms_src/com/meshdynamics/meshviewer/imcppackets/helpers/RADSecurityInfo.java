/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RADSecurityInfo.java
 * Comments : 
 * Created  : Feb 28, 2005
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 *  ---------------------------------------------------------------------------------
 * |  4  |Apr 27, 2007 | radDecidesVlan made short		 		  		 | Imran  |
 * --------------------------------------------------------------------------------
 * |  3  |Dec 20, 2006 | RAD decides VLAN added                          | Abhishek|
 * --------------------------------------------------------------------------------
 * |  2  |May 05, 2005 | CCMP selected by default                        | Bindu  |
 * --------------------------------------------------------------------------------
 * |  1  |Apr 20, 2005 | Misc Fixes                                      | Bindu  |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 28, 2005 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets.helpers;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.IpAddress;

public class RADSecurityInfo {
	
	public  static final short	TYPE_RAD	= 4;
	
	public static final String 	DEF_RAD_IP_BYTES 	= "192.168.0.2";
	public static final short	DEF_RAD_PORT 		= 1812;
	public static final String 	DEF_RAD_KEY 		= "meshap";
	public static final short 	DEF_RAD_GRP_REN 	= 30;
	
	private short 		RADMode;
	private IpAddress	RADIPAddress;
	private long 		RADPort;
	private String 		RADKey;
	private short 		RADCipherTKIP;
	private short 		RADCipherCCMP;
	private int 		RADGroupKeyRenewal;
	private short		RADDecidesVLAN;

	/**
	 * 
	 */
	public RADSecurityInfo() {
		RADIPAddress			= new IpAddress();
		RADIPAddress.setBytes(DEF_RAD_IP_BYTES);
		RADPort					= DEF_RAD_PORT;
		RADKey					= DEF_RAD_KEY;
		RADMode					= 0;
		RADCipherTKIP			= SecurityInfo.NOT_ENABLED;
		RADCipherCCMP			= SecurityInfo.ENABLED;
		RADGroupKeyRenewal		= DEF_RAD_GRP_REN;		
		RADDecidesVLAN			= 0;
	}
	
	/**
	 * @return Returns the rADCipherCCMP.
	 */
	public short getRADCipherCCMP() {
		return RADCipherCCMP;
	}
	/**
	 * @param cipherCCMP The rADCipherCCMP to set.
	 */
	public void setRADCipherCCMP(short cipherCCMP) {
		RADCipherCCMP = cipherCCMP;
	}
	/**
	 * @return Returns the rADCipherTKIP.
	 */
	public short getRADCipherTKIP() {
		return RADCipherTKIP;
	}
	/**
	 * @param cipherTKIP The rADCipherTKIP to set.
	 */
	public void setRADCipherTKIP(short cipherTKIP) {
		RADCipherTKIP = cipherTKIP;
	}
	
	/**
	 * @return Returns the rADGroupKeyRenewal.
	 */
	public int getRADGroupKeyRenewal() {
		return RADGroupKeyRenewal;
	}
	/**
	 * @param groupKeyRenewal The rADGroupKeyRenewal to set.
	 */
	public void setRADGroupKeyRenewal(int groupKeyRenewal) {
		RADGroupKeyRenewal = groupKeyRenewal;
	}
	/**
	 * @return Returns the rADIPAddress.
	 */
	public IpAddress getRADIPAddress() {
		return RADIPAddress;
	}
	/**
	 * @param address The rADIPAddress to set.
	 */
	public void setRADIPAddress(IpAddress address) {
		RADIPAddress = address;
	}
	/**
	 * @return Returns the rADKey.
	 */
	public String getRADKey() {
		return RADKey;
	}
	/**
	 * @param key The rADKey to set.
	 */
	public void setRADKey(String key) {
		RADKey = key;
	}
	/**
	 * @return Returns the rADMode.
	 */
	public short getRADMode() {
		return RADMode;
	}
	/**
	 * @param mode The rADMode to set.
	 */
	public void setRADMode(short mode) {
		RADMode = mode;
	}
	/**
	 * @return Returns the rADPort.
	 */
	public long getRADPort() {
		return RADPort;
	}
	/**
	 * @param port The rADPort to set.
	 */
	public void setRADPort(long port) {
		RADPort = port;
	}

	public void copyPacket(RADSecurityInfo newRADInfo) {
		
		newRADInfo.RADMode				= this.RADMode;
		newRADInfo.RADIPAddress.setBytes(this.RADIPAddress.getBytes());
		newRADInfo.RADPort				= this.RADPort;			
		newRADInfo.RADKey				= new String(this.RADKey.getBytes());
		newRADInfo.RADCipherTKIP		= this.RADCipherTKIP;
		newRADInfo.RADCipherCCMP		= this.RADCipherCCMP;
		newRADInfo.RADGroupKeyRenewal	= this.RADGroupKeyRenewal;
		newRADInfo.RADDecidesVLAN 		= this.RADDecidesVLAN; 	 
	}
	
	public void getPacket(BufferWriter buffer) {
		
		buffer.writeByte(RADMode);
		buffer.writeIPAddress(RADIPAddress);
		buffer.writeInt(RADPort);
		buffer.writeString(RADKey);
		buffer.writeByte(RADCipherTKIP);
		buffer.writeByte(RADCipherCCMP);
		buffer.writeInt(RADGroupKeyRenewal);
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {		
		try {			
			RADMode				= buffer.readByte();
			RADIPAddress		= buffer.readIPAddress();
			RADPort				= buffer.readInt();
			RADKey				= buffer.readString();
			RADCipherTKIP		= buffer.readByte();
			RADCipherCCMP		= buffer.readByte();
			RADGroupKeyRenewal	= (int) buffer.readInt();
			
		}catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
	}
	
	/**
	 * @param decidesVLAN The rADDecidesVLAN to set.
	 */
	public void setRADDecidesVLAN(short decidesVLAN) {
		RADDecidesVLAN = decidesVLAN;
	}

	/**
	 * @return
	 */
	public short getRadDecidesVlan() {
		return RADDecidesVLAN;
	}
}
