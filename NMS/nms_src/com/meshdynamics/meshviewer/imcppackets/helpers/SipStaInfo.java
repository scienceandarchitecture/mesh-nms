/*
 * Created on Mar 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.imcppackets.helpers;

import com.meshdynamics.util.MacAddress;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SipStaInfo {
	
	private MacAddress 	macAddress;
	private short		extn;
	
	/**
	 * 
	 */
	public SipStaInfo() {
		super();
		macAddress 	= new MacAddress();
		extn 		= 0;
	}
	
	

	/**
	 * @return Returns the extn.
	 */
	public short getExtn() {
		return extn;
	}
	/**
	 * @param extn The extn to set.
	 */
	public void setExtn(short extn) {
		this.extn = extn;
	}
	/**
	 * @return Returns the macAddress.
	 */
	public MacAddress getMacAddress() {
		return macAddress;
	}
	/**
	 * @param macAddress The macAddress to set.
	 */
	public void setMacAddress(byte[] macAddressBytes) {
		this.macAddress.setBytes(macAddressBytes);
	}
}
