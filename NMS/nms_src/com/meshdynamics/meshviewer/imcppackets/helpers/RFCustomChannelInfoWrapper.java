/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RFCustomChannelInfoWrapper
 * Comments : Holds RF Custom Channel Info Packets for all interfaces 
 * Created  : Dec 13, 2006
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Dec 13, 2006 | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets.helpers;

import java.util.Hashtable;

import com.meshdynamics.meshviewer.imcppackets.RFCustomChannelInfoPacket;

public class RFCustomChannelInfoWrapper {

	private Hashtable<String, RFCustomChannelInfoPacket> 	rfInfoPackets;
	private int 		maxIfCount;
	
	public RFCustomChannelInfoWrapper() {
		rfInfoPackets = new Hashtable<String, RFCustomChannelInfoPacket>();
	}

	public void RFChannelPacketReceived(RFCustomChannelInfoPacket packet) {
		rfInfoPackets.put(packet.getIfName(), packet);
	}
	
	public boolean configurationReceived() {
		return(rfInfoPackets.size() == maxIfCount);
	}
	
	public RFCustomChannelInfoPacket getIfInfo(String ifName) {
		return (RFCustomChannelInfoPacket) rfInfoPackets.get(ifName);
	}
	
	public void setMaxRFCustChInfoCount(int size) {
        maxIfCount = size;
	}
	
}
