package com.meshdynamics.meshviewer.imcppackets.helpers;

import com.meshdynamics.util.MacAddress;

public class IMCP_MacInfo {

 private String    ifName;
 private MacAddress macAddress;
public String getIfName() {
	return ifName;
}
public void setIfName(String ifName) {
	this.ifName = ifName;
}
public MacAddress getMacAddress() {
	return macAddress;
}
public void setMacAddress(MacAddress macAddress) {
	this.macAddress = macAddress;
}

}
