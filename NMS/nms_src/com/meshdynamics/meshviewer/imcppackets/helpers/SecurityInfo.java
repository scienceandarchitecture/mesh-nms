/**
 * MeshDynamics 
 * -------------- 
 * File     : VLanSecurityInfo.java
 * Comments : 
 * Created  : Feb 2, 2005
 * Author   : Bindu
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ---------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author     |
 * ---------------------------------------------------------------------------------
 * |  7  |Apr 27, 2007   | radDecidesVlan made short		 		  | imran     |
 * ---------------------------------------------------------------------------------
 * |  6  |Dec 19, 2006   | 1 bit is added for RAD decides VLAN 		  | Abhishek |
 * --------------------------------------------------------------------------------
 * |  5  |Apr 27, 2005   | Batch Configure-CopyPacket Bug fixed       | Anand      |
 * ---------------------------------------------------------------------------------
 * |  4  |Apr 13, 2005   | 3 Security lengths added					  | Bindu      |
 * ---------------------------------------------------------------------------------
 * |  3  |Mar 01, 2005   | Default values changes					  | Anand      |
 * ---------------------------------------------------------------------------------
 * |  2  |Feb 28, 2005   | Name Changed for interface security changes| Anand      |
 * ---------------------------------------------------------------------------------
 * |  1  |Feb 15, 2005   | WEPKey Packet changes                      | Bindu      |
 * ---------------------------------------------------------------------------------
 * |  0  |Feb 02, 2005   | Created	                                  | Bindu      |
 * ---------------------------------------------------------------------------------
 */
package com.meshdynamics.meshviewer.imcppackets.helpers;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class SecurityInfo {
	
	private static final short 	TYPE_NONE 		= 0;
	public  static final int    SECURITY_LENGTH	= 8;

	public static final short 	ENABLED 			= 1;
	public static final short 	NOT_ENABLED			= 0;
	public static final short 	RADIUS_DECIDES_VLAN	= 2;
	
	public static final short 	ENABLED_CHECKING			= 0x01;
	public static final short 	RAD_DECIDES_VLAN_CHECKING	= 0x02;
	
	public static final int    	WEP_LENGTH 			= 3;
	public static final int    	PSK_LENGTH 			= 6;
	public static final int    	RAD_LENGTH 			= 14;

	// Parameters for Security = None	
	private short		noneEnabled;
	
	// Parameters for Security = WEP	
	private short				wepEnabled;
	private WEPSecurityInfo		wepInfo;
	
	// Parameters for Security = RSN_PSK
	private short				rsnPSKEnabled;
	private RSNPSKSecurityInfo	rsnPSKInfo;
	
	// Parameters for Security = RSN_RADIUS
	private short				radEnabled;
	private RADSecurityInfo		radInfo;

	//flag for Radius Server decides VLAN checkbox Radius server
	private	short				radDecidesVlan;
	
	public SecurityInfo(){
		
		noneEnabled		= ENABLED;
		wepEnabled		= NOT_ENABLED;
		rsnPSKEnabled	= NOT_ENABLED;
		radEnabled		= NOT_ENABLED;
		wepInfo			= new WEPSecurityInfo();
		rsnPSKInfo		= new RSNPSKSecurityInfo();
		radInfo			= new RADSecurityInfo();
		radDecidesVlan	= 0;	
	}
	
	public void copyPacket(SecurityInfo newSecInfo) {
		newSecInfo.noneEnabled			= this.noneEnabled;
		
		if(wepEnabled == ENABLED) {
			newSecInfo.wepInfo			= new WEPSecurityInfo();
			newSecInfo.setWEPEnabled(ENABLED);
			this.wepInfo.copyPacket(newSecInfo.wepInfo);
		} else {
			newSecInfo.setWEPEnabled(NOT_ENABLED);
		}

		if(rsnPSKEnabled == ENABLED) {
			newSecInfo.setRSNPSKEnabled(ENABLED);
			newSecInfo.rsnPSKInfo		= new RSNPSKSecurityInfo();
			this.rsnPSKInfo.copyPacket(newSecInfo.rsnPSKInfo);
		} else {
			newSecInfo.setRSNPSKEnabled(NOT_ENABLED);
		}
		
		if(radEnabled == ENABLED) {
			newSecInfo.radInfo			= new RADSecurityInfo();
			newSecInfo.setRADEnabled(ENABLED);
			if(radDecidesVlan == 1)
				newSecInfo.radInfo.setRADDecidesVLAN((short)1);
			this.radInfo.copyPacket(newSecInfo.radInfo);
		} else {
			newSecInfo.setRADEnabled(NOT_ENABLED);
		}
	}
	
	public void getPacket(BufferWriter buffer) {
		buffer.writeByte(TYPE_NONE);
		
		buffer.writeByte(noneEnabled);

		buffer.writeByte(WEPSecurityInfo.TYPE_WEP);
		buffer.writeByte(wepEnabled);
		if(wepEnabled == ENABLED) {
			wepInfo.getPacket(buffer);
		}
		
		buffer.writeByte(RSNPSKSecurityInfo.TYPE_RSNPSK);
		buffer.writeByte(rsnPSKEnabled);
		if(rsnPSKEnabled == ENABLED) {
			rsnPSKInfo.getPacket(buffer);
		}
		
		buffer.writeByte(RADSecurityInfo.TYPE_RAD);
		
		short tempRadEnabled = 0;
		radDecidesVlan    = radInfo.getRadDecidesVlan();   
		if(radDecidesVlan == 1 && getRADEnabled()==ENABLED)
		{
			tempRadEnabled	=	RADIUS_DECIDES_VLAN | ENABLED;	
		}else if(radDecidesVlan == 0 && getRADEnabled()== ENABLED)
		{
			tempRadEnabled	=	ENABLED;
		}else{
			tempRadEnabled	=	NOT_ENABLED;
		}
		
		buffer.writeByte(tempRadEnabled);
		if(radEnabled == ENABLED) {
			radInfo.getPacket(buffer);
		} 	
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {		
		try {		
			
			buffer.readByte();
			noneEnabled			= buffer.readByte();
			
			buffer.readByte();
			wepEnabled			= buffer.readByte();
			if(wepEnabled == ENABLED) {
				wepInfo			= new WEPSecurityInfo();
				wepInfo.readPacket(buffer);
			}

			buffer.readByte();
			rsnPSKEnabled		= buffer.readByte();
			
			if(rsnPSKEnabled == ENABLED) {
				rsnPSKInfo		= new RSNPSKSecurityInfo();
				rsnPSKInfo.readPacket(buffer);
			}
			
			buffer.readByte();
			short tempRadEnabled	= buffer.readByte();
			

			if((short)(tempRadEnabled & ENABLED_CHECKING) == 1){
				radEnabled	= ENABLED;
			}
			
			if(radEnabled == ENABLED) {
				
				if((short)(tempRadEnabled & RAD_DECIDES_VLAN_CHECKING) == 2){
					radDecidesVlan = 1;
				}
				radInfo			= new RADSecurityInfo();
				radInfo.readPacket(buffer);
				if(radDecidesVlan == 1)
				{
					radInfo.setRADDecidesVLAN((short)1); 
				}
			}			
		}catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
	}

	
	/**
	 * @return Returns the noneEnabled.
	 */
	public short getNoneEnabled() {
		return noneEnabled;
	}
	/**
	 * @param noneEnabled The noneEnabled to set.
	 */
	public void setNoneEnabled(short noneEnabled) {
		this.noneEnabled = noneEnabled;
	}
	

	/**
	 * @return Returns the radInfo.
	 */
	public RADSecurityInfo getRADInfo() {
		return radInfo;
	}
	/**
	 * @param radInfo The radInfo to set.
	 */
	public void setRADInfo(RADSecurityInfo radInfo) {
		this.radInfo = radInfo;
	}
	/**
	 * @return Returns the rsnPSKInfo.
	 */
	public RSNPSKSecurityInfo getRSNPSKInfo() {
		return rsnPSKInfo;
	}
	/**
	 * @param rsnPSKInfo The rsnPSKInfo to set.
	 */
	public void setRSNPSKInfo(RSNPSKSecurityInfo rsnPSKInfo) {
		this.rsnPSKInfo = rsnPSKInfo;
	}
	/**
	 * @return Returns the wepInfo.
	 */
	public WEPSecurityInfo getWEPInfo() {
		return wepInfo;
	}
	
	/**
	 * @param wepInfo The wepInfo to set.
	 */
	public void setWEPInfo(WEPSecurityInfo wepInfo) {
		this.wepInfo = wepInfo;
	}
	
	/**
	 * @return Returns the radEnabled.
	 */
	public short getRADEnabled() {
		return radEnabled;
	}
	/**
	 * @param radEnabled The radEnabled to set.
	 */
	public void setRADEnabled(short radEnabled) {
		this.radEnabled = radEnabled;
	}
	/**
	 * @return Returns the rsnPSKEnabled.
	 */
	public short getRSNPSKEnabled() {
		return rsnPSKEnabled;
	}
	/**
	 * @param rsnPSKEnabled The rsnPSKEnabled to set.
	 */
	public void setRSNPSKEnabled(short rsnPSKEnabled) {
		this.rsnPSKEnabled = rsnPSKEnabled;
	}
	/**
	 * @return Returns the wepEnabled.
	 */
	public short getWEPEnabled() {
		return wepEnabled;
	}
	/**
	 * @param wepEnabled The wepEnabled to set.
	 */
	public void setWEPEnabled(short wepEnabled) {
		this.wepEnabled = wepEnabled;
	}

	/**
	 * @param radDecidesVlan The radDecidesVlan to set.
	 */
	public void setRadDecidesVlan(short radDecidesVlan) {
		this.radDecidesVlan = radDecidesVlan;
	}
}





