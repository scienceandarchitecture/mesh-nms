/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : SecurityPacket
 * Comments : Interface for packets having security info.
 * Created  : Feb 28, 2005
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Feb 28, 2005 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets.helpers;


public interface ISecurityInfo {
	public String 		getName();
	public String  		getEssid(); 

	public SecurityInfo getSecurityInfo();
	public void 		setSecurityInfo(SecurityInfo secInfo);
}
