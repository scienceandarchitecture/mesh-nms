/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Dot11eIfInfoDetails
 * Comments : 802.11e IfInfo Configuration Class for MeshViewer
 * Created  : Feb 13, 2006
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date          |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  | Feb 17, 2006 | Misc fixes dor dot11e IfInfo					  |Bindu   |
 * -------------------------------------------------------------------------------- 
 * |  0  | Feb 13, 2006 | Created                                         |Bindu   |
 * --------------------------------------------------------------------------------
**********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets.helpers;

import com.meshdynamics.util.BufferWriter;

public class Dot11eIfInfoDetails {
	
	private String	interfaceName;
	private short 	ifDot11eEnabled;
	private short 	ifDot11eCategory;	
	private String 	ifEssid;

	/**
	 * @return Returns the ifDot11eCategory.
	 */
	public short getIfDot11eCategory() {
		return ifDot11eCategory;
	}
	/**
	 * @param ifDot11eCategory The ifDot11eCategory to set.
	 */
	public void setIfDot11eCategory(short ifDot11eCategory) {
		this.ifDot11eCategory = ifDot11eCategory;
	}
	/**
	 * @return Returns the ifDot11eEnabled.
	 */
	public short getIfDot11eEnabled() {
		return ifDot11eEnabled;
	}
	/**
	 * @param ifDot11eEnabled The ifDot11eEnabled to set.
	 */
	public void setIfDot11eEnabled(short ifDot11eEnabled) {
		this.ifDot11eEnabled = ifDot11eEnabled;
	}
	/**
	 * @return Returns the ifEssid.
	 */
	public String getIfEssid() {
		return ifEssid;
	}
	/**
	 * @param ifEssid The ifEssid to set.
	 */
	public void setIfEssid(String ifEssid) {
		this.ifEssid = ifEssid;
	}
	/**
	 * @return Returns the interfaceName.
	 */
	public String getInterfaceName() {
		return interfaceName;
	}
	/**
	 * @param interfaceName The interfaceName to set.
	 */
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}
	/**
	 * @param buffer
	 */
	public void write(BufferWriter buffer) {
		
		buffer.writeString		(interfaceName);
		buffer.writeByte		(ifDot11eEnabled);
		buffer.writeByte		(ifDot11eCategory);
		buffer.writeString		(ifEssid);		
	}
	
	public void copyPacket(Dot11eIfInfoDetails dest) {
		dest.interfaceName 	= new String(interfaceName.getBytes());
		dest.ifDot11eEnabled = ifDot11eEnabled;
		dest.ifDot11eCategory = ifDot11eCategory;
		dest.ifEssid = new String(ifEssid.getBytes());
	}
}
