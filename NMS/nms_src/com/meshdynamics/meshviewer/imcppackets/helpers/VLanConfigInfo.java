/**
 * MeshDynamics 
 * -------------- 
 * File     : VLanConfigInfo.java
 * Comments : 
 * Created  : Feb 1, 2005
 * Author   : Bindu
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * |  8  |Nov 27, 2006   | clean-up								      | Bindu        |
 *  ---------------------------------------------------------------------------------
 * |  7  |May 26, 2005   | tx rate added		      				  | Abhijit      |
 * ----------------------------------------------------------------------------------
 * |  6  |Apr 20, 2005   | Misc Fixes                                 | Bindu  		 |
 * ----------------------------------------------------------------------------------
 * |  5  |Mar 07, 2005   |  !! Changed DOT11I to DOT11E				  | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  4  |Mar 04, 2005   | verifyVlanUpdate() added					  | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  3  |Mar 01, 2005   | Changed DOT11E to DOT11I					  | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  2  |Mar 01, 2005   | Changes for interface security             | Anand        |
 * ----------------------------------------------------------------------------------
 * |  1  |Feb 15, 2005   | WEPKey Packet changes                      | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  0  |Feb 01, 2005   | Created	                                  | Bindu        |
 * ----------------------------------------------------------------------------------
 */

package com.meshdynamics.meshviewer.imcppackets.helpers;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.IpAddress;

public class VLanConfigInfo {
	
	public static final int    	PACKET_LENGTH 	= 25;	
	
	private String 					name;
	private IpAddress 				ipAddress;
	private int 					tag;
	private short 					dot11eEnabled;
	private short 					dot1p;
	private short 					dot11e;
	private String 					essid;
	private long 					rtsThreshold;
	private long 					fragThreshold;
	private long 					beaconInt;
	private short					serviceType;
	private short 					txPower;
	private long 					txRate;

	private SecurityInfo			secInfo;
	
	public VLanConfigInfo(){
		name	 		= "";
		ipAddress 		= new IpAddress();
		tag				= 0; //TODO BINDU DEFAULT ???
		dot11eEnabled	= 0; 
		dot1p			= 0; 
		dot11e			= 0;
		essid			= "";
		rtsThreshold	= 0; 
		fragThreshold	= 0; 
		beaconInt		= 0; 
		serviceType		= 0;
		txPower			= 0;
		txRate			= 0;
		
		secInfo = new SecurityInfo();
	}
	
	public void getPacket(BufferWriter buffer) {
		buffer.writeString(name);
		buffer.writeIPAddress(ipAddress);
		buffer.writeShort(tag);
		buffer.writeByte(dot11eEnabled);
		buffer.writeByte(dot1p);
		buffer.writeByte(dot11e);
		buffer.writeString(essid);
		buffer.writeInt(rtsThreshold);
		buffer.writeInt(fragThreshold);
		buffer.writeInt(beaconInt);
		buffer.writeByte(serviceType);
		buffer.writeByte(txPower);
		buffer.writeInt(txRate);
		secInfo.getPacket(buffer);		
	
	}
	
	public boolean readPacket(BufferReader buffer) {		
		try {
			name	 			= buffer.readString();
			ipAddress 			= buffer.readIPAddress();
			tag					= buffer.readShort();	
			dot11eEnabled  		= buffer.readByte();
			dot1p				= buffer.readByte();
			dot11e				= buffer.readByte();
			essid				= buffer.readString();
			rtsThreshold		= buffer.readInt();
			fragThreshold		= buffer.readInt();
			beaconInt			= buffer.readInt();
			serviceType			= buffer.readByte();
			txPower				= buffer.readByte();
			txRate 				= buffer.readInt();
			if(!secInfo.readPacket(buffer))
				return false;

		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
		return true;
	}
	
	public void copyPacket(VLanConfigInfo newVLanInfo) {
		newVLanInfo.name			= new String(this.name.getBytes());
		newVLanInfo.ipAddress.setBytes(this.ipAddress.getBytes());
		newVLanInfo.tag				= this.tag;
		newVLanInfo.dot11eEnabled	= this.dot11eEnabled;
		newVLanInfo.dot1p			= this.dot1p;
		newVLanInfo.dot11e			= this.dot11e;
		newVLanInfo.essid			= new String(this.essid.getBytes());
		newVLanInfo.rtsThreshold	= this.rtsThreshold;
		newVLanInfo.fragThreshold	= this.fragThreshold;
		newVLanInfo.beaconInt		= this.beaconInt;
		newVLanInfo.serviceType		= this.serviceType;
		newVLanInfo.txPower			= this.txPower;
		newVLanInfo.txRate			= this.txRate;
		this.secInfo.copyPacket(newVLanInfo.secInfo);		
	}
	
	/**
	 * @return Returns the txPower.
	 */
	public short getTxPower() {
		return txPower;
	}
	
	/**
	 * @param txPower The txPower to set.
	 */
	public void setTxPower(short txPower) {
		this.txPower = txPower;
	}
	
	/**
	 * @return Returns the dot11eEnabled.
	 */
	public short getDot11eEnabled() {
		return dot11eEnabled;
	}
	
	/**
	 * @param dot11eEnabled The dot11eEnabled to set.
	 */
	public void setDot11eEnabled(short dot11eEnabled) {
		this.dot11eEnabled = dot11eEnabled;
	}
	
	/**
	 * @return Returns the beaconInt.
	 */
	public long getBeaconInt() {
		return beaconInt;
	}
	
	/**
	 * @param beaconInt The beaconInt to set.
	 */
	public void setBeaconInt(long beaconInt) {
		this.beaconInt = beaconInt;
	}
	
	/**
	 * @return Returns the dot1e.
	 */
	public short getDot11e() {
		return dot11e;
	}
	
	/**
	 * @param dot1e The dot1e to set.
	 */
	public void setDot11e(short dot11e) {
		this.dot11e = dot11e;
	}
	
	/**
	 * @return Returns the dot1p.
	 */
	public short getDot1p() {
		return dot1p;
	}
	
	/**
	 * @param dot1p The dot1p to set.
	 */
	public void setDot1p(short dot1p) {
		this.dot1p = dot1p;
	}
	
	/**
	 * @return Returns the fragThreshold.
	 */
	public long getFragThreshold() {
		return fragThreshold;
	}
	
	/**
	 * @param fragThreshold The fragThreshold to set.
	 */
	public void setFragThreshold(long fragThreshold) {
		this.fragThreshold = fragThreshold;
	}
	
	/**
	 * @return Returns the ipAddress.
	 */
	public IpAddress getIpAddress() {
		return ipAddress;
	}
	
	/**
	 * @param ipAddress The ipAddress to set.
	 */
	public void setIpAddress(IpAddress ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	/**
	 * @return Returns the rtsThreshold.
	 */
	public long getRtsThreshold() {
		return rtsThreshold;
	}
	
	/**
	 * @param rtsThreshold The rtsThreshold to set.
	 */
	public void setRtsThreshold(long rtsThreshold) {
		this.rtsThreshold = rtsThreshold;
	}
	
	/**
	 * @return Returns the serviceType.
	 */
	public short getServiceType() {
		return serviceType;
	}
	
	/**
	 * @param serviceType The serviceType to set.
	 */
	public void setServiceType(short serviceType) {
		this.serviceType = serviceType;
	}
	
	/**
	 * @return Returns the tag.
	 */
	public int getTag() {
		return tag;
	}
	
	/**
	 * @param tag The tag to set.
	 */
	public void setTag(int tag) {
		this.tag = tag;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return name;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.SecurityPacket#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.SecurityPacket#getEssid()
	 */
	public String getEssid() {
		return essid;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.SecurityPacket#getSecurityInfo()
	 */
	public SecurityInfo getSecurityInfo() {
		return secInfo;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.SecurityPacket#etSecurityInfo(com.meshdynamics.meshviewer.imcppackets.SecurityInfo)
	 */
	public void setSecurityInfo(SecurityInfo secInfo) {
		this.secInfo = secInfo;		
	}
	

	/**
	 * @return Returns the txRate.
	 */
	public long getTxRate() {
		return txRate;
	}
	
	/**
	 * @param txRate The txRate to set.
	 */
	public void setTxRate(long txRate) {
		this.txRate = txRate;
	}

	public void setEssid(String essid) {
		this.essid = essid;
	}

	public void setName(String name) {
		this.name = name;
	}
}
