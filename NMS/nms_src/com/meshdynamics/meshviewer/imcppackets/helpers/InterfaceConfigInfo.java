
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : InterfaceConfigInfo.java
 * Comments : SubClass for ApConfigInfoPacket.
 * Created  : Oct 04, 2005
 * Author   : Amit Chavan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author |
 * -----------------------------------------------------------------------------
 * |  11 |NOV 2,2016     | HT and VHT capabilities added 			  |Venkat  | 	
 *  -----------------------------------------------------------------------------
 * |  10 |Apr 27,2007    | ifDescription, dot11eEnabled,			  |Abhishek| 	
 * 							dot11eCategoryIndex removed				  |		   |
 * -----------------------------------------------------------------------------	
 * |  9  |Feb 19, 2006   | DCA List length		  					  | Mithil |
 *  --------------------------------------------------------------------------------
 * |  8  |Feb 16, 2006   | Changes for Dot11e	  					  | Bindu  |
 *  --------------------------------------------------------------------------------
 * |  7  | Oct 4,2005    | Client connectivity parameter added        | Amit   |
 * -----------------------------------------------------------------------------
 * |  6  |Sep 25, 2005   | ifDescription initialized	 		  	  | Abhijit|
 * -----------------------------------------------------------------------------
 * |  5  |May 20, 2005   | TxRate added to InterfaceConfig.	 		  | Anand  |
 * -----------------------------------------------------------------------------
 * |  4  |May 12, 2005   | Interface Desc added to interface.inf      | Anand  |
 * -----------------------------------------------------------------------------
 * |  3  |Apr 27, 2005   | Batch Configure-CopyPacket Bug fixed       | Anand  |
 * -----------------------------------------------------------------------------
 * |  2  |Apr 13, 2005   | Added reading/writing of IF security       | Bindu  |
 * -----------------------------------------------------------------------------
 * |  1  |Mar 01, 2005   | Changes for interface security             | Anand  |
 * -----------------------------------------------------------------------------
 * |  0  |Oct 04, 2005   | Created                                    | Amit   |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets.helpers;

import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class InterfaceConfigInfo {
	
	public static final short AC_BK		= 0;
	public static final short AC_BE		= 1;
	public static final short AC_VI		= 2;
	public static final short AC_VO		= 3;	

	private  int                    interfaceID;
	private  int                    interfaceLEN;
	private  String 				interfaceName;	
	private  short      			mediumType;
	private  short      			mediumSubType;
	private  short					usageType;
	private  short  				channel;
	private  short					channelBondingType;
	private  short					dynamicChannelAlloc;
	private  short      			txPower;
	private  long 					txRate;
	private  short					serviceType;
	private  short      			dcaListCount;	
	private  short[]    			dcaList;
	private String 					essid;
	private long 					rtsThreshold;
	private long 					fragThreshold;
	private long 					beaconInt;
	private SecurityInfo 			secInfo;
	private short					appCtrl;
	
	private InterfaceAdvanceInfo    advanceInfo;
	
	
	/**
	 * 
	 */
	public InterfaceConfigInfo() {
		interfaceName		= "";
		txPower 			= 100;
		dcaList				= null;
		essid				= "";
		secInfo 			= new SecurityInfo();
		advanceInfo         = new InterfaceAdvanceInfo();
	}
	
	
	public int getInterfaceID() {
		return interfaceID;
	}


	public void setInterfaceID(int interfaceID) {
		this.interfaceID = interfaceID;
	}


	public int getInterfaceLEN() {
		return interfaceLEN;
	}


	public void setInterfaceLEN(int interfaceLEN) {
		this.interfaceLEN = interfaceLEN;
	}


	/**
	 * @return Returns the bonding.
	 */
	public short getChannelBondingType() {
		return channelBondingType;
	}
	
	/**
	 * @param bonding The bonding to set.
	 */
	public void setChannelBondingType(short bonding) {
		this.channelBondingType = bonding;
	}
	
	/**
	 * @return Returns the channel.
	 */
	public short getChannel() {
		return channel;
	}
	
	/**
	 * @param channel The channel to set.
	 */
	public void setChannel(short channel) {
		this.channel = channel;
	}
	
	/**
	 * @return Returns the dcaList.
	 */
	public short[] getDcaList() {
		return dcaList;
	}
	
	/**
	 * @param dcaList The dcaList to set.
	 */
	public void setDcaListChannel(int index, short channel) {
		this.dcaList[index]	= channel;
	}
	
	/**
	 * @return Returns the dcaListCount.
	 */
	public short getDcaListCount() {
		return this.dcaListCount;
	}
	
	/**
	 * @param dcaListCount The dcaListCount to set.
	 */
	public void setDcaListCount(short dcaListCount) {
		this.dcaListCount 	= dcaListCount;
		this.dcaList		= new short[dcaListCount];
	}
	
	/**
	 * @return Returns the dynamicChannelAlloc.
	 */
	public short getDynamicChannelAlloc() {
		return dynamicChannelAlloc;
	}
	
	/**
	 * @param dynamicChannelAlloc The dynamicChannelAlloc to set.
	 */
	public void setDynamicChannelAlloc(short dynamicChannelAlloc) {
		this.dynamicChannelAlloc = dynamicChannelAlloc;
	}
	
	/**
	 * @return Returns the interfaceName.
	 */
	public String getInterfaceName() {
		return interfaceName;
	}
	
	/**
	 * @param interfaceName The interfaceName to set.
	 */
	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	/**
	 * @return Returns the ist.
	 */
	public short getServiceType() {
		return serviceType;
	}
	
	/**
	 * @param ist The ist to set.
	 */
	public void setServiceType(short ist) {
		this.serviceType = ist;
	}
	
	/**
	 * @return Returns the mediumSubType.
	 */
	public short getMediumSubType() {
		return mediumSubType;
	}
	
	/**
	 * @param mediumSubType The mediumSubType to set.
	 */
	public void setMediumSubType(short mediumSubType) {
		this.mediumSubType = mediumSubType;
	}
	
	/**
	 * @return Returns the mediumType.
	 */
	public short getMediumType() {
		return mediumType;
	}
	
	/**
	 * @param mediumType The mediumType to set.
	 */
	public void setMediumType(short mediumType) {
		this.mediumType = mediumType;
	}
	
	/**
	 * @return Returns the txPower.
	 */
	public short getTxPower() {
		return txPower;
	}
	
	/**
	 * @param txPower The txPower to set.
	 */
	public void setTxPower(short txPower) {
		this.txPower = txPower;
	}
	
	/**
	 * @return Returns the usageType.
	 */
	public short getUsageType() {
		return usageType;
	}
	
	/**
	 * @return Returns the essid.
	 */
	public String getEssid() {
		return essid;
	}
	
	/**
	 * @param essid The essid to set.
	 */
	public void setEssid(String essid) {
		this.essid = essid;
	}
	
	/**
	 * @return Returns the fragThreshold.
	 */
	public long getFragThreshold() {
		return fragThreshold;
	}
	
	/**
	 * @param fragThreshold The fragThreshold to set.
	 */
	public void setFragThreshold(long fragThreshold) {
		this.fragThreshold = fragThreshold;
	}
	
	/**
	 * @return Returns the rtsThreshold.
	 */
	public long getRtsThreshold() {
		return rtsThreshold;
	}
	
	/**
	 * @param rtsThreshold The rtsThreshold to set.
	 */
	public void setRtsThreshold(long rtsThreshold) {
		this.rtsThreshold = rtsThreshold;
	}
	
	/**
	 * @param usageType The usageType to set.
	 */
	public void setUsageType(short usageType) {
		this.usageType = usageType;
	}
	
	/**
	 * @return Returns the beaconInt.
	 */
	public long getBeaconInt() {
		return beaconInt;
	}
	
	/**
	 * @param beaconInt The beaconInt to set.
	 */
	public void setBeaconInt(long beaconInt) {
		this.beaconInt = beaconInt;
	}
	
	public short getAppCtrl() {
		return appCtrl;
	}

	public void setAppCtrl(short appCtrl) {
		this.appCtrl = appCtrl;
	}

	public void copyPacket(InterfaceConfigInfo dest) {
		dest.interfaceName 	= new String(interfaceName.getBytes());	
		dest.mediumType		= mediumType;
		dest.mediumSubType	= mediumSubType;
		dest.usageType		= usageType;
		dest.channel 		= channel;
		dest.channelBondingType 	= channelBondingType;
		dest.dynamicChannelAlloc 	= dynamicChannelAlloc;
		dest.txPower 				= txPower;
		dest.txRate					= txRate;
		dest.serviceType 			= serviceType;
		dest.dcaListCount 			= dcaListCount;
		
		dest.essid					= essid;
		dest.rtsThreshold			= rtsThreshold;
		dest.fragThreshold			= fragThreshold;
		dest.beaconInt 				= beaconInt;
		dest.appCtrl				= appCtrl;
		
		dest.dcaList 				= new short[dcaListCount];
		for(int i=0; i< dcaListCount; i++) {
			dest.dcaList[i] = dcaList[i];
		}
		this.secInfo.copyPacket(dest.secInfo);
		this.advanceInfo.copyPacket(dest.advanceInfo);
	}
	
	/**
	 * @param buffer
	 */
	public void read(BufferReader buffer) {
        interfaceID         = buffer.readShort();
		interfaceLEN        = buffer.readShort();
		if(interfaceID == IMCPInfoIDS.IMCP_IF_802_3_INFO_ID){
			interfaceName		= buffer.readString();
			mediumType			= buffer.readByte();
    		mediumSubType		= buffer.readByte();
    		usageType			= buffer.readByte();
    		txRate				= buffer.readInt();
    		appCtrl				= buffer.readByte();
    		serviceType			= buffer.readByte();
    	}else{ 	   
    	interfaceName		= buffer.readString();
    	mediumType			= buffer.readByte();   
		mediumSubType		= buffer.readByte();
		usageType			= buffer.readByte();    	
    	channel				= buffer.readByte();
    	channelBondingType	= buffer.readByte();
    	dynamicChannelAlloc	= buffer.readByte();
    	txPower				= buffer.readByte();
    	txRate				= buffer.readInt();
    	serviceType			= buffer.readByte();
    	essid				= buffer.readString();
    	rtsThreshold		= buffer.readInt();
		fragThreshold		= buffer.readInt();
		beaconInt 			= buffer.readInt();	
    	dcaListCount		= buffer.readByte();
    	
    	dcaList = new short[dcaListCount];			
    	for(int i=0;i<dcaListCount;i++){
    		dcaList[i] = buffer.readByte();
    	}
    	if(secInfo != null)
    		secInfo.readPacket(buffer);
    	if((interfaceLEN & 1<<15) != 0){
    		advanceInfo         = new InterfaceAdvanceInfo();
    		advanceInfo.readPacket(buffer);	
    	}
    	}
    	}
	
	/**
	 * @param buffer
	 */
	
	public void write(BufferWriter buffer) {
		buffer.writeShort       (interfaceID);
		buffer.writeShort       (interfaceLEN);
		if(interfaceID == IMCPInfoIDS.IMCP_IF_802_3_INFO_ID){
			buffer.writeString		(interfaceName);
			buffer.writeByte		(mediumType);
			buffer.writeByte		(mediumSubType);
			buffer.writeByte		(usageType);
			buffer.writeInt			(txRate);
			buffer.writeByte		(appCtrl);
			buffer.writeByte		(serviceType);
		}else{
		   buffer.writeString	(interfaceName);
		   buffer.writeByte		(mediumType);
		   buffer.writeByte		(mediumSubType);
		   buffer.writeByte		(usageType);
	       buffer.writeByte		(channel);
		   buffer.writeByte		(channelBondingType);
		   buffer.writeByte		(dynamicChannelAlloc);
		   buffer.writeByte		(txPower);
		   buffer.writeInt		(txRate);
		   buffer.writeByte		(serviceType);
		   buffer.writeString	(essid);
		   buffer.writeInt		(rtsThreshold);
		   buffer.writeInt		(fragThreshold);
		   buffer.writeInt      (beaconInt);  
		   buffer.writeByte		((short)dcaList.length);
        for(int cnt=0;cnt<dcaList.length;cnt++){
        	buffer.writeByte	(dcaList[cnt]);
		}		
        if(secInfo != null)
        	secInfo.getPacket(buffer);
        if(advanceInfo.getProtocolID() == 1 || 
				advanceInfo.getProtocolID() ==2 || 
				advanceInfo.getProtocolID() ==3)
		   advanceInfo.getPacket(buffer);	  
        }
		
				
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.SecurityPacket#getName()
	 */
	public String getName() {
		return interfaceName;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.SecurityPacket#getSecurityInfo()
	 */
	public SecurityInfo getSecurityInfo() {
		return secInfo;
	}
	
	/**
	 * @return Returns the txRate.
	 */
	public long getTxRate() {
		return txRate;
	}
	
	/**
	 * @param txRate The txRate to set.
	 */
	public void setTxRate(long txRate) {
		this.txRate = txRate;
	}

	public InterfaceAdvanceInfo getInterfaceAdvanceInfo(){
		return advanceInfo;
	}
	public int getPacketLength(){
		int len=0;
		if(getInterfaceName().equals("eth0") || getInterfaceName().equals("eth1")){
			len += 7;
			len += getInterfaceName().length();
			if(getInterfaceName().equals("eth0")){
				len += 1;
			}
		}else{
		len += 27;
		len += getInterfaceName().length();
		len += getEssid().length();
		len += getDcaListCount();		
		len += SecurityInfo.SECURITY_LENGTH;
		
		SecurityInfo secInfo = getSecurityInfo();
		if(secInfo != null)	{		
		short enabled = secInfo.getWEPEnabled();
		
		if(enabled == 1) {
			if(secInfo.getWEPInfo().getWEPStrength() == WEPSecurityInfo.WEP_64_BIT) {
				len += SecurityInfo.WEP_LENGTH + WEPSecurityInfo.WEP_64_BIT_KEY_LENGTH;					
			} else if(secInfo.getWEPInfo().getWEPStrength() == WEPSecurityInfo.WEP_128_BIT) {
				len += SecurityInfo.WEP_LENGTH + WEPSecurityInfo.WEP_128_BIT_KEY_LENGTH;
			}
		}
		
		len += SecurityInfo.PSK_LENGTH + secInfo.getRSNPSKInfo().getPSKKey().length;
		len += SecurityInfo.RAD_LENGTH + secInfo.getRADInfo().getRADKey().length();
		}
		InterfaceAdvanceInfo advanceInfo= getInterfaceAdvanceInfo();
		if(advanceInfo != null){
			if(getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_ANAC || getMediumSubType()==Mesh.PHY_SUB_TYPE_802_11_AC)
		    len +=InterfaceAdvanceInfo.INTERFACE_ADVANCEINFO_LENGTH_FOR_N_AC;
			 
			if(getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_24GHz_N  ||
					getMediumSubType() == Mesh.PHY_SUB_TYPE_802_11_5GHz_N ||
					getMediumSubType()== Mesh.PHY_SUB_TYPE_802_11_AN ||
					getMediumSubType() ==Mesh.PHY_SUB_TYPE_802_11_BGN){
				  len +=InterfaceAdvanceInfo.INTERFACE_ADVANCEINFO_LENGTH_FOR_N;
		}
	}//end of 
		}
		return len;
	}
	
}
