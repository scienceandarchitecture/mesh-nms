/**********************************************************************************
 * MeshDynamics
 * --------------
 * File     : HeartbeatPacket.java
 * Comments : AccessPoint Heartbeat Info Packet IMCP(#1)
 * Created  : Sep 24, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  20 |Jan 20, 2007 | voltage calculation moved to accesspoint        |Abhijit |
 * --------------------------------------------------------------------------------
 * |  19 |Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  18 |Nov 27, 2006 | clean-up, voltage added	  			         |  Bindu |
 * --------------------------------------------------------------------------------
 * |  17 |Nov 19, 2006 | Misc changes:Changes due to StatusDisplayUI     |Abhishek|
 * --------------------------------------------------------------------------------
 * |  16 |Oct 10, 2006 | getMeshType function added			             |Abhijit |
 * --------------------------------------------------------------------------------
 * |  15 |Oct 06, 2006 | signal added to 		             			 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  14 |May 16, 2006 | getBitRateFromKnownAP				             |Prachiti|
 * --------------------------------------------------------------------------------
 * |  13 |May 5, 2006  | bitrate display from knownap  	                 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  12 |Feb 14, 2006 | Clear Macro Message after reboot				 | Mithil |
 * --------------------------------------------------------------------------------
 * |  11 | Nov 09,2005 | parameter changed to display message in statusUI| Amit   |
 * --------------------------------------------------------------------------------
 * |  10 | Nov 08,2005 | Change for displaying hb's for selected network | Mithil |
 * --------------------------------------------------------------------------------
 * |  9  | Oct 14,2005 | Call for adding AP Name and Model name added    | Amit   |
 * --------------------------------------------------------------------------------
 * |  8  |Sept 22, 2005|GetDSMacAddress added						     |Prachiti|
 * --------------------------------------------------------------------------------
 * |  7  |Sept 21, 2005|HeartbeatMessage added						     |Prachiti|
 * --------------------------------------------------------------------------------
 * |  6  |May 10, 2005 | isEncrypted method added for encryption fix     | Anand  |
 * --------------------------------------------------------------------------------
 * |  5  |May 03, 2005 | Bug fixed - Outgoing packet encrypted			 | Anand  |
 * --------------------------------------------------------------------------------
 * |  4  |Apr 20, 2005 | Board Temp listener property checking added     | Bindu  |
 * --------------------------------------------------------------------------------
 * |  3  |Mar 09, 2005 | Board Temp added to Heartbeat                   | Anand  |
 * --------------------------------------------------------------------------------
 * |  2  |Feb 11, 2005 | Change Detection-PropertyUI changes             | Anand  |
 * --------------------------------------------------------------------------------
 * |  1  |Feb 10, 2005 | verifyUpdates Func added for change detection   | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 24, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import com.meshdynamics.meshviewer.configuration.KnownAp;
import com.meshdynamics.meshviewer.configuration.KnownAp.KnownApInfo;
import com.meshdynamics.meshviewer.mesh.IHeartbeatInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.util.StaEntry;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;
import com.meshdynamics.util.DateTime;
import com.meshdynamics.util.MacAddress;


public class HeartbeatPacket extends IMCPPacket implements IHeartbeatInfo {

	private int                 NODE_INFO_ID;
	private int                 NODE_INFO_LENGTH; 
	private long 				sequenceNumber;
	private long 				cumulativeTollCost;
	private short 				hopCount;
	private MacAddress 			rootBssid;
	private long 				cumulativeRSSI;
	private short 				healthIndex;
	private short 				heartbeatInterval;
	private long 				bitRate;
	private short 				staCount;
	private MacAddress 			parentBssid;
	private short 				apCount;
	private long 				txBytes;
	private long 				rxBytes;
	private long 				txPackets;
	private long 				rxPackets;
	private long				parentSignal;
	private KnownAp[] 			knownAPs;
	private StaEntry[] 			staEntries;
	private AccessPoint			parentAP;
	private double 				boardTemperature;
	private long				longitude;
	private long				latitude;
	private int					altitude;
	private int					speed;
	private int 				packetLength; // calculated when packet is read
	
	private static final int	HYBRID_MESH_FLAG = 0x800000; 
	
	public HeartbeatPacket() {
	    parentBssid            	= null;
	    rootBssid              	= null;
		sequenceNumber			= 0;
		cumulativeTollCost		= 0;
		cumulativeRSSI			= 0;
		healthIndex				= 0;
		bitRate					= 0;
		hopCount				= 0;
		parentSignal 			= 0;
		knownAPs				= null;
		longitude				= 0;
		latitude				= 0;		
		altitude				= 0;
		speed					= 0;
		
		packetLength			= 0;
	}
	
    /* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.ImcpPacket#readPacket(byte[])
	 */
	public boolean readPacket(BufferReader buffer) {

	    packetLength = 0;
		try {
			NODE_INFO_ID            = buffer.readShort();       packetLength    += 2;
			NODE_INFO_LENGTH        = buffer.readShort();       packetLength    += 2;
			dsMacAddress 			= buffer.readMacAddress();  packetLength	+= MacAddress.GetLength();
			sequenceNumber 			= buffer.readInt();			packetLength	+= 4;
			cumulativeTollCost 		= buffer.readInt();			packetLength	+= 4;
			hopCount 				= buffer.readByte();		packetLength	+= 1;
			rootBssid				= buffer.readMacAddress();	packetLength	+= MacAddress.GetLength();
			cumulativeRSSI 			= buffer.readInt();			packetLength	+= 4;
			healthIndex 			= buffer.readByte();		packetLength	+= 1;
			heartbeatInterval 		= buffer.readByte();		packetLength	+= 1;
			bitRate	 				= buffer.readInt();			packetLength	+= 4;
			staCount 				= buffer.readByte();		packetLength	+= 1;
			boardTemperature		= buffer.readInt(); 		packetLength	+= 4;
			parentBssid				= buffer.readMacAddress();	packetLength	+= MacAddress.GetLength();
			apCount 				= buffer.readByte();		packetLength	+= 1;
			txPackets				= buffer.readInt();			packetLength	+= 4;
			rxPackets				= buffer.readInt();			packetLength	+= 4;
			txBytes					= buffer.readInt();			packetLength	+= 4;
			rxBytes					= buffer.readInt();			packetLength	+= 4;
			
		    knownAPs = new KnownAp[apCount];
			for(int apNo=0; apNo<apCount; apNo++){
				
			    knownAPs[apNo] 				= new KnownAp();
			    
			    short kapBitRate			= buffer.readByte();   			packetLength	+= 1;
			    knownAPs[apNo].macAddress   = buffer.readMacAddress();		packetLength	+= MacAddress.GetLength();
			    short kapSignal				= buffer.readByte();			packetLength	+= 1;
			    
			    knownAPs[apNo].addInfo((short) (-96 + kapSignal), kapBitRate);
			    
			}

			staEntries = new StaEntry[staCount];
			for(int staNo=0; staNo<staCount; staNo++){
				staEntries[staNo] = new StaEntry();
				staEntries[staNo].macAddress  		= buffer.readMacAddress();	packetLength	+= MacAddress.GetLength();
				staEntries[staNo].associationTime	= DateTime.getDateTime();		
			}
			
			try {
			    longitude 	= buffer.readInt();			packetLength	 += 4;
			    latitude 	= buffer.readInt();			packetLength	 += 4;
			    altitude 	= buffer.readShort();		packetLength	 += 2;
			    speed 		= buffer.readShort();		packetLength	 += 2;
			    
			}catch(Exception e) {
			    //System.out.println("GPS Info not for version less than 2.5.69");
			} 
			
			if(parentBssid.equals(dsMacAddress))
			    parentSignal = 100;

		} catch (Exception e){
			Mesh.logException(e);
			return false;
		}
    	return true;
	}


    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(int, byte[])
     */
    public void getPacket(BufferWriter writer) {

        try {
        	writer.writeShort(NODE_INFO_ID);
        	writer.writeShort(NODE_INFO_LENGTH);
		    writer.writeMacAddress(dsMacAddress);
		    writer.writeInt(sequenceNumber);
		    writer.writeInt(cumulativeTollCost);
			writer.writeByte(hopCount);
			writer.writeMacAddress(rootBssid);
			writer.writeInt(cumulativeRSSI);	
			writer.writeByte(healthIndex);
			writer.writeByte(heartbeatInterval);
			writer.writeInt(bitRate);
			writer.writeByte(staCount);
			writer.writeInt((long)boardTemperature);
			writer.writeMacAddress(parentBssid);
			writer.writeByte(apCount);
			writer.writeInt(txPackets);
			writer.writeInt(rxPackets);
			writer.writeInt(txBytes);
			writer.writeInt(rxBytes);

			for(int apNo=0; apNo<knownAPs.length; apNo++)
			{
					KnownApInfo kap = (KnownApInfo) knownAPs[apNo].knownApInfo.get(0);
					writer.writeByte(kap.bitRate);
				    writer.writeMacAddress(knownAPs[apNo].macAddress);
				    writer.writeByte((short)(kap.signal + 96));
			}
			
			for(int staNo=0; staNo<staCount; staNo++) {
			    writer.writeMacAddress(staEntries[staNo].macAddress);
			}
			
			try {
				writer.writeInt(longitude);
				writer.writeInt(latitude);
				writer.writeShort(altitude);
				writer.writeShort(speed);
				
			}catch(Exception e1) {
			    //System.out.println("GPS Info not for version less than 2.5.69");
			}

		} catch (Exception e){
			Mesh.logException(e);
			return;
		}
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
     */
    public int getPacketLength() {
        return  packetLength;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
     */
    public byte getPacketType() {
        return PacketFactory.HEARTBEAT;
    }

    /**
     * @return Returns the apCount.
     */
    public short getApCount() {
        return apCount;
    }
    /**
     * @return Returns the cumulativeRSSI.
     */
    public long getCumulativeRSSI() {
        return cumulativeRSSI;
    }
    /**
     * @return Returns the cumulativeTollCost.
     */
    public long getCumulativeTollCost() {
        return cumulativeTollCost;
    }
    /**
     * @return Returns the healthIndex.
     */
    public short getHealthIndex() {
        return healthIndex;
    }
    /**
     * @return Returns the heartbeatInterval.
     */
    public short getHeartbeatInterval() {
        return heartbeatInterval;
    }
    /**
     * @return Returns the hopCount.
     */
    public short getHopCount() {
        return hopCount;
    }
    /**
     * @return Returns the knownAPs.
     */
    public KnownAp[] getKnownAPs() {
        return knownAPs;
    }
    /**
     * @return Returns the parentBssid.
     */
    public MacAddress getParentBssid() {
        return parentBssid;
    }
    /**
     * @return Returns the rootBssid.
     */
    public MacAddress getRootBssid() {
        return rootBssid;
    }
    /**
     * @return Returns the sequenceNumber.
     */
    public long getSequenceNumber() {
        return sequenceNumber;
    }
    /**
     * @return Returns the staCount.
     */
    public short getStaCount() {
        return staCount;
    }
    /**
     * @return Returns the staEntries.
     */
    public StaEntry[] getStaEntries() {
        return staEntries;
    }
    /**
     * @return Returns the txRate.
     */
    public long getBitRate() {
        return bitRate;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.mesh.HeartbeatInfo#getSignalfromParent()
     */
    public long getSignalfromParent() {
        return parentSignal;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.mesh.HeartbeatInfo#getByteSent()
     */
    public long getByteSent() {
        return txBytes;
    }

    /* (non-Javadoc)
     * @see com.meshdynamics.meshviewer.mesh.HeartbeatInfo#getPacketSent()
     */
    public long getPacketSent() {
        return txPackets;
    }
    
    public byte getTreeRate() {
        return (byte) (cumulativeTollCost & 0xFF);
    }
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.HeartbeatInfo#getParentAP()
	 */
	public AccessPoint getParentAP() {
		return parentAP;
	}

   	/**
	 * @return Returns the boardTempareture.
	 */
	public double getBoardTempareture() {
		return boardTemperature;
	}

	public int getMeshType() {
	
		if((cumulativeTollCost & HYBRID_MESH_FLAG) != 0)
			return MESH_TYPE_HYBRID;
		
		return MESH_TYPE_INFRA;
	}

	public String getHeartBeatData(){
		String dump;
		
		dump     		= " " + dsMacAddress.toString();
		dump			= dump + "," + DateTime.getDateTime();
		dump			= dump + "," + parentBssid.toString();
		dump	 		= dump + "," + boardTemperature;
		
		//if(this.getAPType() == Mesh.PHY_TYPE_802_3)
		//	dump	 		= dump + ",-";
		//else if(this.getAPType() == Mesh.PHY_TYPE_802_11)
			dump 			= dump + "," + (parentSignal - 96);
		
		dump 			= dump + "," + cumulativeTollCost;
		dump			= dump + "," + hopCount;
		dump			= dump + "," + heartbeatInterval;
		dump			= dump + "," + healthIndex;
		dump			= dump + "," + bitRate;	
		dump			= dump + "," + txBytes;
		dump			= dump + "," + rxBytes;
		dump			= dump + "," + txPackets;
		dump			= dump + "," + rxPackets;
		
  		return dump;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.HeartbeatInfo#getRxBytes()
	 */
	public long getRxBytes() {
		return rxBytes;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.mesh.HeartbeatInfo#getRxPackets()
	 */
	public long getRxPackets() {
		return rxPackets ;
	}
	
    public int getAltitude() {
        return altitude;
    }
    
    public long getLatitude() {
        return latitude;
    }
    
    public long getLongitude() {
        return longitude;
    }
    
    public int getSpeed() {
        return speed;
    }

	public int getNODE_INFO_ID() {
		return NODE_INFO_ID;
	}

	public void setNODE_INFO_ID(int NODE_INFO_ID) {
		this.NODE_INFO_ID = NODE_INFO_ID;
	}

	public int getNODE_INFO_LENGTH() {
		return NODE_INFO_LENGTH;
	}

	public void setNODE_INFO_LENGTH(int NODE_INFO_LENGTH) {
		this.NODE_INFO_LENGTH = NODE_INFO_LENGTH;
	}
    
    
}
