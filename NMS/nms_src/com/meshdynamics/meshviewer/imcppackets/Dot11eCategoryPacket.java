/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Dot11eIfInfoPacket
 * Comments : 802.11e IfInfo Configuration Class for MeshViewer
 * Created  : Feb 13, 2006
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date          |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  3  | Jan 12, 2007 | isEncrypted method removed                      | Imran  |
 * --------------------------------------------------------------------------------
 * |  2  | Apr 13, 2006 | Null Exception while handling category name	  |Mithil  |
 * -------------------------------------------------------------------------------- 
 * |  1  | Apr 10, 2006 | Misc fixes dor dot11e IfInfo					  |Mithil  |
 * -------------------------------------------------------------------------------- 
 * |  0  | Feb 13, 2006 | Created                                         |Bindu   |
 * --------------------------------------------------------------------------------
**********************************************************************************/
package com.meshdynamics.meshviewer.imcppackets;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import com.meshdynamics.meshviewer.imcppackets.helpers.Dot11eCategoryDetails;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class Dot11eCategoryPacket extends RequestResponsePacket {

	private static final int BASE_PACKET_LENGTH	= 13;//9+4

	private short 	categoryCount;
	private Vector<Dot11eCategoryDetails>	categories;

	private int    DOT11E_CATEGORY_INFO_ID;
	private int    DOT11E_CATEGORY_INFO_LENGTH;
	/**
	 * 
	 */
	public Dot11eCategoryPacket() {
		super();
		categories = new Vector<Dot11eCategoryDetails>();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			DOT11E_CATEGORY_INFO_ID            =   buffer.readShort();
			DOT11E_CATEGORY_INFO_LENGTH        =   buffer.readShort();
			dsMacAddress 			=  	buffer.readMacAddress();			
	        requestResponseType 	=	buffer.readByte(); 
	        requestResponseID  		=  	buffer.readByte();
	        categoryCount			=  	buffer.readByte();
        
	        categories 			=	new Vector<Dot11eCategoryDetails>();
	        for (int i = 0; i < categoryCount; i++) {
	        	Dot11eCategoryDetails info 	= new Dot11eCategoryDetails();	        	
	        	info.categoryName			= buffer.readString();
	        	info.burstTime				= buffer.readInt();
	        	info.aCWmin					= buffer.readShort();
	        	info.aCWmax					= buffer.readShort();
	        	info.aifsn					= buffer.readByte();
	        	info.disableBackoff			= buffer.readByte();

				categories.add(info);				
			}
	        
	        return true;
		} catch(Exception e){
			Mesh.logException(e);
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter buffer) {
		try {
			buffer.writeShort       (DOT11E_CATEGORY_INFO_ID);
			buffer.writeShort       (DOT11E_CATEGORY_INFO_LENGTH);
			buffer.writeMacAddress	(dsMacAddress);
			buffer.writeByte		(requestResponseType);
			buffer.writeByte		(requestResponseID);
			
			buffer.writeByte		((short)categories.size());
			
			Iterator<Dot11eCategoryDetails> itr = categories.iterator();
			while(itr.hasNext()) {
				Dot11eCategoryDetails categoryDetails = (Dot11eCategoryDetails)itr.next();
				buffer.writeString		(categoryDetails.categoryName);
				buffer.writeInt			(categoryDetails.burstTime);
				buffer.writeShort		(categoryDetails.aCWmin);
				buffer.writeShort		(categoryDetails.aCWmax);
				buffer.writeByte		(categoryDetails.aifsn);
				buffer.writeByte		(categoryDetails.disableBackoff);
			}

		} catch(Exception  e) {
			Mesh.logException(e);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		int 		length 	= BASE_PACKET_LENGTH;
		Iterator<Dot11eCategoryDetails> 	itr 	= categories.iterator();
		
		while(itr.hasNext()) {
			Dot11eCategoryDetails category = (Dot11eCategoryDetails)itr.next();
			length += 1; 									//Name length 		= 1 Byte
			length += category.categoryName.length(); 	//Name Length Bytes
			length += 10;									
			//Burst Time 		= 4 Bytes
			//aCWmin 			= 2 Bytes
			//aCWmax 			= 2 Bytes
			//AIFSN 			= 1 Byte
			//Disable Backoff 	= 1 Byte			
		}
		
		return length;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.DOT11E_CATEGORY_INFO_PACKET;		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		Dot11eCategoryPacket dot11eCategoryNew	= (Dot11eCategoryPacket)packet;
		dot11eCategoryNew.requestResponseType	=  this.requestResponseType;
		dot11eCategoryNew.requestResponseID		=  this.requestResponseID;
		dot11eCategoryNew.categoryCount			=	this.categoryCount;
		
		Enumeration<Dot11eCategoryDetails> enm 	= this.categories.elements();
		while(enm.hasMoreElements()) {
			Dot11eCategoryDetails categoryConfOld	= (Dot11eCategoryDetails)enm.nextElement();
			Dot11eCategoryDetails categoryConf 		= new Dot11eCategoryDetails();
			categoryConf.aCWmax 					= categoryConfOld.aCWmax;
			categoryConf.aCWmin						= categoryConfOld.aCWmin;
			categoryConf.aifsn						= categoryConfOld.aifsn;
			categoryConf.burstTime					= categoryConfOld.burstTime;
			categoryConf.categoryName				= categoryConfOld.categoryName;
			categoryConf.disableBackoff				= categoryConfOld.disableBackoff;
			
			dot11eCategoryNew.categories.add(categoryConf);
		}		
	}

	public Dot11eCategoryDetails getdDot11eCategoryRefByIndex(int index) {
		if(index < 0 || index >= categories.size()){
			return null;
		}
		return (Dot11eCategoryDetails) categories.elementAt(index);
	}

	/**
	 * @return Returns the categoryCount.
	 */
	public short getCategoryCount() {
		return categoryCount;
	}
	
	/**
	 * @param categoryCount The categoryCount to set.
	 */
	public void setCategoryCount(short categoryCount) {
		resetCategories();
		this.categoryCount = categoryCount;
		if(categoryCount>0){ 
			for(int i=0;i<categoryCount;i++){				
				Dot11eCategoryDetails que = new Dot11eCategoryDetails();
				categories.add(i, que);
			}
		}
	}
	
	public void resetCategories() {
		categoryCount = 0;
		categories.clear();
	}

	public int getDOT11E_CATEGORY_INFO_ID() {
		return DOT11E_CATEGORY_INFO_ID;
	}

	public void setDOT11E_CATEGORY_INFO_ID(int dOT11E_CATEGORY_INFO_ID) {
		DOT11E_CATEGORY_INFO_ID = dOT11E_CATEGORY_INFO_ID;
	}

	public int getDOT11E_CATEGORY_INFO_LENGTH() {
		return DOT11E_CATEGORY_INFO_LENGTH;
	}

	public void setDOT11E_CATEGORY_INFO_LENGTH(int dOT11E_CATEGORY_INFO_LENGTH) {
		DOT11E_CATEGORY_INFO_LENGTH = dOT11E_CATEGORY_INFO_LENGTH;
	}
	
	

}
