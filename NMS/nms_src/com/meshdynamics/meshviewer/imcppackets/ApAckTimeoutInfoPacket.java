/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ApAckTimeoutInfoPacket.java
 * Comments : 
 * Created  : Nov 16, 2005
 * Author   : Mithil Wane
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * * --------------------------------------------------------------------------------
 * |  4  |Apr 27, 2007 | setAckTimeout added                             |Abhishek|
 * --------------------------------------------------------------------------------
 * |  3  |Apr 27, 2007 | setIfName added                                 |Abhishek|
 * --------------------------------------------------------------------------------
 * |  2  |Jan 12, 2006 | isEncrypted removed                             | Imran  |
 * --------------------------------------------------------------------------------
 * |  1  |Nov 27, 2006 | clean-up					  			         |  Bindu |
 * --------------------------------------------------------------------------------
 * |  0  |Nov 16, 2005 | Created                                         | Mithil |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.imcppackets;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import com.meshdynamics.meshviewer.imcppackets.helpers.ApAckInterfaceTimeoutInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.util.BufferReader;
import com.meshdynamics.util.BufferWriter;

public class ApAckTimeoutInfoPacket extends RequestResponsePacket {

	private static final int BASE_PACKET_LENGTH = 13;//9+4

	private short								iCnt;
	private Vector<ApAckInterfaceTimeoutInfo>	ackTimeoutInfo;
	
	private int                                 AP_ACK_TIMEOUT_INFO_ID;
	private int                                 AP_ACK_TIMEOUT_INFO_LENGTH;
	/**
	 * @param ackTimeout
	 */
	public ApAckTimeoutInfoPacket() {
		ackTimeoutInfo = new Vector<ApAckInterfaceTimeoutInfo>();
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#readPacket(com.meshdynamics.util.BufferReader)
	 */
	public boolean readPacket(BufferReader buffer) {
		try{
			AP_ACK_TIMEOUT_INFO_ID            =   buffer.readShort();
			AP_ACK_TIMEOUT_INFO_LENGTH        =   buffer.readShort();
			dsMacAddress 			=  	buffer.readMacAddress();
	        requestResponseType 	=	buffer.readByte(); 
	        requestResponseID  		=  	buffer.readByte();
	        iCnt  					=  	buffer.readByte();
        
	        ackTimeoutInfo = new Vector<ApAckInterfaceTimeoutInfo>();
	        for (int i = 0; i < iCnt; i++) {
	        	ApAckInterfaceTimeoutInfo info = new ApAckInterfaceTimeoutInfo();
	        	info.ifName 	= buffer.readString();
				info.ackTimeout = buffer.readByte();
				ackTimeoutInfo.add(info);
			}
	        
	        return true;
		} catch(Exception e){
			Mesh.logException(e);
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacket(com.meshdynamics.util.BufferWriter)
	 */
	public void getPacket(BufferWriter buffer) {
		try {
			buffer.writeShort       (AP_ACK_TIMEOUT_INFO_ID);
			buffer.writeShort       (AP_ACK_TIMEOUT_INFO_LENGTH);
			buffer.writeMacAddress	(dsMacAddress);
			buffer.writeByte		(requestResponseType);
			buffer.writeByte		(requestResponseID);
			buffer.writeByte		(iCnt);	
			for(int i=0;i<iCnt;i++) {
				ApAckInterfaceTimeoutInfo info = (ApAckInterfaceTimeoutInfo)ackTimeoutInfo.get(i);
				buffer.writeString	(info.ifName);
				buffer.writeByte	(info.ackTimeout);
			}
				
		} catch(Exception  e) {
		Mesh.logException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketLength()
	 */
	public int getPacketLength() {
		int 		len = BASE_PACKET_LENGTH;
		Iterator<ApAckInterfaceTimeoutInfo> 	it 	= ackTimeoutInfo.iterator();
		
		while(it.hasNext()) {
			ApAckInterfaceTimeoutInfo inf = (ApAckInterfaceTimeoutInfo)it.next();
			len += 1;
			len += inf.ifName.length();
			len += 1;
		}
		
		return len;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#getPacketType()
	 */
	public byte getPacketType() {
		return PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.imcppackets.IMCPPacket#copyPacket(com.meshdynamics.meshviewer.imcppackets.IMCPPacket)
	 */
	protected void copyPacket(IMCPPacket packet) {
		ApAckTimeoutInfoPacket apAckTimeoutInfoPacket 	= (ApAckTimeoutInfoPacket)packet;
		apAckTimeoutInfoPacket.requestResponseID 		= this.requestResponseID;
		apAckTimeoutInfoPacket.requestResponseType 		= this.requestResponseType;
		apAckTimeoutInfoPacket.iCnt 					= this.iCnt;
		Enumeration<ApAckInterfaceTimeoutInfo> enm 								= this.ackTimeoutInfo.elements();
        while(enm.hasMoreElements()) {
			ApAckInterfaceTimeoutInfo ifConfOld 	= (ApAckInterfaceTimeoutInfo)enm.nextElement();
			ApAckInterfaceTimeoutInfo ifConf 		= new ApAckInterfaceTimeoutInfo();
			ifConf.ifName 							= ifConfOld.ifName;
			ifConf.ackTimeout 						= ifConfOld.ackTimeout;
			apAckTimeoutInfoPacket.ackTimeoutInfo.add(ifConf);
        }
	}
	
	/**
	 * @return Returns the iCnt.
	 */
	public short getICnt() {
		return iCnt;
	}
	
	/**
	 * @param cnt The iCnt to set.
	 */
	public void setICnt(short cnt) {
		iCnt = cnt;
		this.ackTimeoutInfo.clear();
		for(int i=0;i<iCnt;i++) {
			ApAckInterfaceTimeoutInfo info = new ApAckInterfaceTimeoutInfo();
			ackTimeoutInfo.add(info);
		}
	}
	
	public ApAckInterfaceTimeoutInfo getIfRefByIndex(int index) {
		if(ackTimeoutInfo.size() == 0){
			return null;
		}
		return (ApAckInterfaceTimeoutInfo)ackTimeoutInfo.elementAt(index);
	}

	/**
	 * @param j
	 * @param name
	 */
	public void setIfName(int j, String name) {
		ApAckInterfaceTimeoutInfo ackInfo	= (ApAckInterfaceTimeoutInfo)this.ackTimeoutInfo.get(j);
		if(ackInfo == null) {
			return;
		}
		ackInfo.ifName						= name;
	}

	/**
	 * @param j
	 * @param ackTimeout
	 */
	public void setAckTimeout(int j, short ackTimeout) {
		ApAckInterfaceTimeoutInfo ackInfo	= (ApAckInterfaceTimeoutInfo)this.ackTimeoutInfo.get(j);
		if(ackInfo == null) {
			return;
		}
		ackInfo.ackTimeout					= ackTimeout;
	}

	public int getAP_ACK_TIMEOUT_INFO_ID() {
		return AP_ACK_TIMEOUT_INFO_ID;
	}

	public void setAP_ACK_TIMEOUT_INFO_ID(int aP_ACK_TIMEOUT_INFO_ID) {
		AP_ACK_TIMEOUT_INFO_ID = aP_ACK_TIMEOUT_INFO_ID;
	}

	public int getAP_ACK_TIMEOUT_INFO_LENGTH() {
		return AP_ACK_TIMEOUT_INFO_LENGTH;
	}

	public void setAP_ACK_TIMEOUT_INFO_LENGTH(int aP_ACK_TIMEOUT_INFO_LENGTH) {
		AP_ACK_TIMEOUT_INFO_LENGTH = aP_ACK_TIMEOUT_INFO_LENGTH;
	}

}
