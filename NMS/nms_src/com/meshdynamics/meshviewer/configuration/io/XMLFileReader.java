package com.meshdynamics.meshviewer.configuration.io;

import java.io.InputStream;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLFileReader extends ConfigurationReader{

	private 	Document		doc;
	private 	Node 			currentNode;
	private 	int				state;
	private 	Vector<Node>	stack;
	private 	int				stackPtr;
	
	private final int 			STATE_VALID		= 0;
	private final int 			STATE_INVALID	= 1;
	

	
	
	public XMLFileReader(InputStream inputStream) {
		super(inputStream);
		currentNode	= null;
		stack		= new Vector<Node>();
		stackPtr	= 0;
		state		= parseFile();;
		
	}
	
	private void push(Node node) {
		stack.add(stackPtr, node);
		stackPtr++;
	}
	
	private Node pop() {
		if(stackPtr <= 0)
			return null;
		
		return (Node) stack.remove(--stackPtr);
	}
		
	private int parseFile(){
		try{
			DocumentBuilderFactory	docFac 	= DocumentBuilderFactory.newInstance();
			DocumentBuilder builder 		= docFac.newDocumentBuilder();
			doc			 					= builder.parse(inputStream);
			if(doc == null)
				return STATE_INVALID;
			
			NodeList nodeList = doc.getChildNodes();
			if(nodeList.getLength() != 1)
				return STATE_INVALID;
			
			Node node = nodeList.item(0);
			if(node == null)
				return STATE_INVALID;
			
			if(node.getNodeName().equalsIgnoreCase(ConfigXMLTokens.mdHeader) == false)
				return STATE_INVALID;
			
			currentNode = node.getFirstChild();
			if(currentNode == null)
				return STATE_INVALID;
			
			currentNode = skipNonElementSiblings(currentNode);
			if(currentNode == null)
				return STATE_INVALID;

		}catch(Exception e){
			e.printStackTrace();
			return STATE_INVALID;
		}
		
		return STATE_VALID;
	}

	private Node skipNonElementSiblings(Node node) {

		if(node == null)
			return null;
		
		int type = node.getNodeType();
		
		while(type != Node.ELEMENT_NODE) {
			node = node.getNextSibling();
			if(node == null)
				return null;
			
			type = node.getNodeType();
		}
		
		return node;
	}
	
	public boolean getNextToken() {
		
		if(state == STATE_INVALID)
			return false;

		String nodeName = currentNode.getNodeName();
		
		int tokenKey 	= ConfigXMLTokens.getHeaderIndex(nodeName);
		if(tokenKey == -1) {
			tokenKey 	= ConfigXMLTokens.getKeyIndex(nodeName);
			if(tokenKey == -1) {
				currentTokenType	= IConfigurationIODefs.TOKEN_TYPE_UNDEFINED;
			} else {
				currentTokenType	= IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE;
				currentTokenKey		= tokenKey;
				push(currentNode);
				currentNode			= currentNode.getFirstChild();
				currentTokenValue	= currentNode.getNodeValue();
			}
		} else {
			currentTokenType	= IConfigurationIODefs.TOKEN_TYPE_HEADER;
			currentHeader		= tokenKey;
		}
		
		NodeList nodeList = currentNode.getChildNodes();
		if(nodeList.getLength() > 0) {
			push(currentNode);
			currentNode = currentNode.getFirstChild();
		} else {
			Node sibling = currentNode.getNextSibling();
			if(sibling == null) {
				currentNode = pop();
				if(currentNode == null)
					return false;
				
				nodeName 			= currentNode.getNodeName();
				tokenKey 			= ConfigXMLTokens.getHeaderIndex(nodeName);
				if(tokenKey >= 0) {
					currentTokenType	= IConfigurationIODefs.TOKEN_TYPE_HEADER;
					currentHeader		= tokenKey + 1;
				}
				
				currentNode 		= currentNode.getNextSibling();
				
			}else
				currentNode = sibling;
		}
			
		return true;
	}

}
