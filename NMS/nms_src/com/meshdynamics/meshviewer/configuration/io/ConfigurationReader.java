package com.meshdynamics.meshviewer.configuration.io;

import java.io.InputStream;

public class ConfigurationReader {

	protected int 			currentTokenType;
	protected int 			currentHeader;
	protected int 			currentTokenKey;
	protected String 		currentTokenValue;
	protected InputStream	inputStream;
	
	public ConfigurationReader(InputStream inputStream) {
		currentTokenType 	= IConfigurationIODefs.TOKEN_TYPE_UNDEFINED;
		currentHeader		= IConfigurationIODefs.CONFIG_HEADER_UNDEFINED;
		currentTokenKey		= IConfigurationIODefs.CONFIG_KEY_UNDEFINED;
		currentTokenValue	= "";
		this.inputStream	= inputStream;
	}
	
	public boolean getNextToken() {
		return true;
	}
	
	public int getCurrentTokenType() {
		return currentTokenType;
	}
	
	public int getCurrentHeader() {
		return currentHeader;
	}
	
	public int getCurrentToken() {
		return currentTokenKey;
	}
	
	public String getCurrentTokenValue() {
		return currentTokenValue;
	}
}
