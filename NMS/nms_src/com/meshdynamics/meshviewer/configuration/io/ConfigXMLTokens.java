package com.meshdynamics.meshviewer.configuration.io;

public class ConfigXMLTokens {

	public static final String mdHeader = "MeshDynamics";
	
	public static final String[]strHeaderTokens	=
	{
		"MODEL_INFO",
		"/MODEL_INFO",
		"SOFTWARE_VERSION_INFO",
		"/SOFTWARE_VERSION_INFO",
		"NODE_CONFIGURATION",
		"/NODE_CONFIGURATION",
		"<NETWORK_INFO>\r\n",
		"</NETWORK_INFO>\r\n",
		"<MESH_INFO>\r\n",
		"</MESH_INFO>\r\n",
		"<MOBILITY_INFO>\r\n",
		"</MOBILITY_INFO>\r\n",
		"<PORT_CONFIG>\r\n",
		"</PORT_CONFIG>\r\n",
		"<PORT_INFO>\r\n",
		"</PORT_INFO>\r\n",
		"<CHANNEL_INFO>\r\n",
		"</CHANNEL_INFO>\r\n",
		"<SECURITY_CONFIG>\r\n",
		"</SECURITY_CONFIG>\r\n",
		"<WEP_INFO>\r\n",
		"</WEP_INFO>\r\n",
		"<WPA_PERSONAL_INFO>\r\n",
		"</WPA_PERSONAL_INFO>\r\n",
		"<WPA_ENTERPRISE_INFO>\r\n",
		"</WPA_ENTERPRISE_INFO>\r\n",
		"<CUSTOM_CHANNEL_CONFIG_INFO>\r\n",
		"</CUSTOM_CHANNEL_CONFIG_INFO>\r\n",
		"<CUSTOM_CHANNEL_INFO>\r\n",
		"</CUSTOM_CHANNEL_INFO>\r\n",
		"<VLAN_CONFIG>\r\n",
		"</VLAN_CONFIG>\r\n",
		"<VLAN_INFO>\r\n",
		"</VLAN_INFO>\r\n",
		"<_802.11E_CATEGORY_CONFIG>\r\n",
		"</_802.11E_CATEGORY_CONFIG>\r\n",
		"<_802.11E_CATEGORY_INFO>\r\n",
		"</_802.11E_CATEGORY_INFO>\r\n",
		"<ACL_CONFIG>\r\n",
		"</ACL_CONFIG>\r\n",
		"<ACL_INFO>\r\n",
		"</ACL_INFO>\r\n"
	};
	
	public static String[] strStartKeyTokens	=
	{
		"HardwareModel",
		
		"SoftwareVersion",
		
		"NodeName",
		"Description",
		"Latitude",
		"Longitude",
		"PreferredParent",
		
		"<Host_Name>",
		"<IP_Address>",
		"<Subnet_Mask>",
		"<Gateway_Address>",
		
		"<Heartbeat_Interval>",
		"<Hop_Cost>",
		"<Change_Resistance>",
		"<Signal_Map>",
		"<Preferred_Parent>",
		
		"<Mobility_Mode>",
		"<Mobilty_Index>",
		"<Mobility_Distance>",
		"<Mobility_Speed>",
		
		"<Port_Count>",
		
		"<Port_Name>",
		"<Port_ESSID>",
		"<TX_Rate>",
		"<TX_Power>",
		"<Frag_Threschold>",
		"<RTS_Threschold>",
		"<DCA_Enabled>",
		"<Channel>",
		"<DCAList>",
		"<Beacon_Interval>",
		"<Usage_Type>",
		"<Medium_Subtype>",
		"<Medium_Type>",
		"<Service_Type>",
		"<ACK_Timeout>",
		"<Hide_ESSID>",
		
		"<Channel_Count>",
		"<Bandwidth>",
		"<Conformance>",
		"<Max_Power>",
		"<Max_Antenna_Gain>",
		
		"<Channel_Number>",
		"<Center_Frequency>",
		"<Flags>",
		"<Max_Power>",
		"<Max_Reg_Power>",
		"<Min_Power>",
		
		"<Security_None>",
		
		"<Security_WEP_Enabled>",
		"<WEP_Key_Index>",
		"<WEP_Key1>",
		"<WEP_Key2>",
		"<WEP_Key3>",
		"<WEP_Key4>",
		"<WEP_Passphrase>",
		"<WEP_Strength>",
		
		"<Security_WPA_Personal_Enabled>",
		"<Cipher_CCMP>",
		"<Cipher_TKIP>",
		"<Group_Renewal>",
		"<WPA_Key_Length>",
		"<WPA_Key>",
		"<WPA_Mode>",
		"<WPA_Passphrase>",
		
		"<Security_WPA_Enterprise_Enabled>",
		"<Cipher_CCMP>",
		"<Cipher_TKIP>",
		"<Group_Renewal>",
		"<Server_IP_Address>",
		"<Server_Port>",
		"<WPA_Mode>",
		"<WPA_Enterprise_decides_VLAN>",
		
		"<Vlan_Count>",
		"<Vlan_Default_Tag>",
		"<Vlan_Default_802.11E_Enabled>",
		"<Vlan_Default_802.11E>",
		"<Vlan_Default_802.11P>",
		
		"<Vlan_Name>",
		"<Vlan_IP_Address>",
		"<Vlan_Tag>",
		"<Vlan_802.11P_Priority>",
		"<Vlan_802.11E_Enabled>",
		"<Vlan_802.11E_Priority>",
		"<Vlan_ESSID>",
		
		"<_802.11E_Category_Count>",
		
		"<Category_Name>",
		"<Category_Burst_Time>",
		"<Category_ACWMin>",
		"<Category_ACWMax>",
		"<Category_AIFSN>",
		"<Category_Disable_Backoff>",
		
		"<Config_Key_ACL_Count>",
		
		"<Sta_Address>",
		"<Vlan_Tag>",
		"<Allow_Connection>",
		"<_802.11E_Enabled>",
		"<_802.11E_Category>"	
	};
	
	public static String[] strEndKeyTokens	=
	{
		"</Hardware_Model>",
		
		"</Software_Version>",
		
		"</Node_Name>",
		"</Node_Description>",
		"</Latitude>",
		"</Longitude>",
		"</Regulatory_Domain>",
		
		"</Host_Name>",
		"</IP_Address>",
		"</Subnet_Mask>",
		"</Gateway_Address>",
		
		"</Heartbeat_Interval>",
		"</Hop_Cost>",
		"</Change_Resistance>",
		"</Signal_Map>",
		"</Preferred_Parent>",
		
		"</Mobility_Mode>",
		"</Mobilty_Index>",
		"</Mobility_Distance>",
		"</Mobility_Speed>",
		
		"</Port_Count>",
		
		"</Port_Name>",
		"</Port_ESSID>",
		"</TX_Rate>",
		"</TX_Power>",
		"</Frag_Threschold>",
		"</RTS_Threschold>",
		"</DCA_Enabled>",
		"</Channel>",
		"</DCAList>",
		"</Beacon_Interval>",
		"</Usage_Type>",
		"</Medium_Subtype>",
		"</Medium_Type>",
		"</Service_Type>",
		"</ACK_Timeout>",
		"</Hide_ESSID>",
		
		"</Channel_Count>",
		"</Bandwidth>",
		"</Conformance>",
		"</Max_Power>",
		"</Max_Antenna_Gain>",
		
		"</Channel_Number>",
		"</Center_Frequency>",
		"</Flags>",
		"</Max_Power>",
		"</Max_Reg_Power>",
		"</Min_Power>",
		
		"</Security_None>",
		
		"</Security_WEP_Enabled>",
		"</WEP_Key_Index>",
		"</WEP_Key1>",
		"</WEP_Key2>",
		"</WEP_Key3>",
		"</WEP_Key4>",
		"</WEP_Passphrase>",
		"</WEP_Strength>",
		
		"</Security_WPA_Personal_Enabled>",
		"</Cipher_CCMP>",
		"</Cipher_TKIP>",
		"</Group_Renewal>",
		"</WPA_Key_Length>",
		"</WPA_Key>",
		"</WPA_Mode>",
		"</WPA_Passphrase>",
		
		"</Security_WPA_Enterprise_Enabled>",
		"</Cipher_CCMP>",
		"</Cipher_TKIP>",
		"</Group_Renewal>",
		"</Server_IP_Address>",
		"</Server_Port>",
		"</WPA_Mode>",
		"</WPA_Enterprise_decides_VLAN>",
		
		"</Vlan_Count>",
		"</Vlan_Default_Tag>",
		"</Vlan_Default_802.11E_Enabled>",
		"</Vlan_Default_802.11E>",
		"</Vlan_Default_802.11P>",
		
		"</Vlan_Name>",
		"</Vlan_IP_Address>",
		"</Vlan_Tag>",
		"</Vlan_802.11P_Priority>",
		"</Vlan_802.11E_Enabled>",
		"</Vlan_802.11E_Priority>",
		"</Vlan_ESSID>",
		
		"</_802.11E_Category_Count>",
		
		"</Category_Name>",
		"</Category_Burst_Time>",
		"</Category_ACWMin>",
		"</Category_ACWMax>",
		"</Category_AIFSN>",
		"</Category_Disable_Backoff>",
		
		"</Config_Key_ACL_Count>",
		
		"</Sta_Address>",
		"</Vlan_Tag>",
		"</Allow_Connection>",
		"</_802.11E_Enabled>",
		"</_802.11E_Category>"	
	};
	
	public static String getStrHeaderToken(int index){
		return strHeaderTokens[index]; 
	}
	
	public static String getStartKeyToken(int index){
		return strStartKeyTokens[index]; 
	}
	
	public static String getEndKeyToken(int index){
		return strEndKeyTokens[index]; 
	}
	
	public static int getHeaderIndex(String header){
		
		for(int i=0;i<strHeaderTokens.length;i++) {
			if(header.equalsIgnoreCase(strHeaderTokens[i]) == true)
				return i;
		}
		
		return -1;
	}
	
	public static int getKeyIndex(String key){
		for(int i=0;i<strStartKeyTokens.length;i++) {
			if(key.equalsIgnoreCase(strStartKeyTokens[i]) == true)
				return i;
		}
		
		return -1;
	}
}
