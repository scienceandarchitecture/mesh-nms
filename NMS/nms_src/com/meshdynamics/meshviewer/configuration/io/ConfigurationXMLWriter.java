/*
 * Created on Feb 27, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.configuration.io;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ConfigurationXMLWriter extends ConfigurationWriter{

	private String          	strEntry=null;
	private	OutputStream 		bout;
	private OutputStreamWriter 	out;
	
	public ConfigurationXMLWriter(OutputStream outputStream){
		
		super(outputStream);
		
		try{
			   bout	   = new BufferedOutputStream(outputStream);
		       out	   = new OutputStreamWriter(bout, "UTF8");
		        
		}catch(Exception e){
			e.printStackTrace(); 
		}
	}

	/**
	 * 
	 */
	public void initTag() {
		try {
		      out.write("<?xml version=\"1.0\" ");
		      out.write("encoding=\"UTF-8\"?>\r\n");
		      out.write("<MeshDynamics>\r\n");  
	        
			} catch (IOException e) {
				e.printStackTrace();
		    } 
	}
	
	public void endTag(){
		try {
			 out.write("</MeshDynamics>\r\n");
			 out.flush();  
		     out.close();
		} catch (IOException e) {
			e.printStackTrace();
	    }
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter#writeHeader(int)
	 */
	public boolean writeHeader(int header) {
		
		try {
			out.write(ConfigXMLTokens.getStrHeaderToken(header));
			
		}catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter#writeEntry(java.lang.String, java.lang.String)
	 */
	public boolean writeEntry(String entryName, String entryValue) {
		
		int keyIndex = ConfigFileTokens.getKeyIndex(entryName); 
		strEntry 	 = ConfigXMLTokens.getStartKeyToken(keyIndex)+ entryValue+ConfigXMLTokens.getEndKeyToken(keyIndex);
		
		try{
			out.write(strEntry);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

}
