package com.meshdynamics.meshviewer.configuration.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class ConfigurationTextWriter extends ConfigurationWriter{
	
	private BufferedWriter 	bw;
	private String          strEntry;
	
	public ConfigurationTextWriter(OutputStream outputStream) {
		super(outputStream);
		 bw = new BufferedWriter(new OutputStreamWriter(outputStream));
	}

	
	public boolean writeEntry(int key, String entryValue) {
		
		strEntry = ConfigFileTokens.getStrKeyToken(key)+ entryValue;	
		try {
			bw.write(strEntry);
			bw.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return true;
	}

	
	public boolean writeHeader(int header) {
		
		strEntry = ConfigFileTokens.getStrHeaderToken(header);
		
		try {
			bw.write(strEntry);
			bw.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter#writeEntry(int, java.lang.String, int)
	 */
	public boolean writeEntry(int key, String entryValue, int tabCount) {
		
		String space	= "     ";
		String tab 		= "";
		for(int i = 0; i < tabCount; i++) {
			tab	+= space;
		}
		strEntry = ConfigFileTokens.getStrKeyToken(key)+ entryValue;	
		try {
			bw.write(tab+strEntry);
			bw.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter#writeHeader(int, int)
	 */
	public boolean writeHeader(int header, int tabCount) {
		
		String space	= "     ";
		String tab 		= "";
		for(int i = 0; i < tabCount; i++) {
			tab	+= space;
		}
		strEntry = ConfigFileTokens.getStrHeaderToken(header);
		
		try {
			bw.write(tab+strEntry);
			bw.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	public void close() {
		try {
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}