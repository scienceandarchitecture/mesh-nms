package com.meshdynamics.meshviewer.configuration.io;

public interface IConfigurationIODefs {

	/*
	 *	Token type can be a header or name value pair 
	 */
	public static final int TOKEN_TYPE_UNDEFINED									= 0;			
	public static final int TOKEN_TYPE_HEADER 										= 1;
	public static final int TOKEN_TYPE_KEY_VALUE 									= 2;
	
	/*
	 *  Header types defined for configuration
	 */
	public static final int CONFIG_HEADER_UNDEFINED		 							= -1;
	
	public static final int CONFIG_HEADER_MESHDYNAMICS	 							= 0;
	public static final int CONFIG_HEADER_MODEL_INFO_START 							= 1;
	public static final int CONFIG_HEADER_MODEL_INFO_END 							= 2;
	
	public static final int CONFIG_HEADER_VERSION_INFO_START						= 3;
	public static final int CONFIG_HEADER_VERSION_INFO_END	 						= 4;

	public static final int CONFIG_HEADER_NODE_INFO_START	 						= 5;
	public static final int CONFIG_HEADER_NODE_INFO_END		 						= 6;
			
	public static final int CONFIG_HEADER_NETWORK_INFO_START 						= 7;
	public static final int CONFIG_HEADER_NETWORK_INFO_END	 						= 8;
	
	public static final int CONFIG_HEADER_MESH_INFO_START	 						= 9;
	public static final int CONFIG_HEADER_MESH_INFO_END		 						= 10;
	
	public static final int CONFIG_HEADER_MOBILITY_INFO_START						= 11;
	public static final int CONFIG_HEADER_MOBILITY_INFO_END							= 12;
		
	public static final int CONFIG_HEADER_PORT_INFO_START	 						= 13;
	public static final int CONFIG_HEADER_PORT_INFO_END		 						= 14;
	
	public static final int CONFIG_HEADER_PORT_CONFIG_START 						= 15;
	public static final int CONFIG_HEADER_PORT_CONFIG_END	 						= 16;
	
	public static final int CONFIG_HEADER_CHANNEL_INFO_START	 					= 17;
	public static final int CONFIG_HEADER_CHANNEL_INFO_END		 					= 18;

	public static final int CONFIG_HEADER_CHANNEL_CONFIG_START	 					= 19;
	public static final int CONFIG_HEADER_CHANNEL_CONFIG_END	 					= 20;
	
	public static final int CONFIG_HEADER_SECURITY_CONFIG_START	 					= 21;
	public static final int CONFIG_HEADER_SECURITY_CONFIG_END	 					= 22;
			
	public static final int CONFIG_HEADER_WEP_INFO_START	 						= 23;
	public static final int CONFIG_HEADER_WEP_INFO_END		 						= 24;
	
	public static final int CONFIG_HEADER_WPA_PERSONAL_INFO_START	 				= 25;
	public static final int CONFIG_HEADER_WPA_PERSONAL_INFO_END	 					= 26;
	
	public static final int CONFIG_HEADER_WPA_ENTERPRISE_INFO_START					= 27;
	public static final int CONFIG_HEADER_WPA_ENTERPRISE_INFO_END					= 28;

	public static final int CONFIG_HEADER_CUSTOM_CHANNEL_INFO_START	 				= 29;
	public static final int CONFIG_HEADER_CUSTOM_CHANNEL_INFO_END	 				= 30;
	
	public static final int CONFIG_HEADER_CUSTOM_CHANNEL_CONFIG_START	 			= 31;
	public static final int CONFIG_HEADER_CUSTOM_CHANNEL_CONFIG_END	 				= 32;

	public static final int CONFIG_HEADER_VLAN_INFO_START	 						= 33;
	public static final int CONFIG_HEADER_VLAN_INFO_END	 							= 34;
		
	public static final int CONFIG_HEADER_VLAN_CONFIG_START	 						= 35;
	public static final int CONFIG_HEADER_VLAN_CONFIG_END	 						= 36;
	
	public static final int CONFIG_HEADER_80211E_CATEGORY_CONFIG_START	 			= 37;
	public static final int CONFIG_HEADER_80211E_CATEGORY_CONFIG_END	 			= 38;
	
	public static final int CONFIG_HEADER_80211E_CATEGORY_INFO_START	 			= 39;
	public static final int CONFIG_HEADER_80211E_CATEGORY_INFO_END	 				= 40;

	public static final int CONFIG_HEADER_ACL_CONFIG_START	 						= 41;
	public static final int CONFIG_HEADER_ACL_CONFIG_END	 						= 42;
		
	public static final int CONFIG_HEADER_ACL_INFO_START	 						= 43;
	public static final int CONFIG_HEADER_ACL_INFO_END	 							= 44;
	
	public static final int CONFIG_HEADER_EFFISTREAM_CONFIG_START					= 45;
	public static final int CONFIG_HEADER_EFFISTREAM_CONFIG_END						= 46;
	
	public static final int CONFIG_HEADER_EFFISTREAM_RULE_START						= 47;
	public static final int CONFIG_HEADER_EFFISTREAM_RULE_END						= 48;
	
	public static final int CONFIG_HEADER_EFFISTREAM_ACTION_START					= 49;
	public static final int CONFIG_HEADER_EFFISTREAM_ACTION_END						= 50;
	
	public static final int RF_PROFILE_HEADER_START									= 51;
	public static final int RF_PROFILE_HEADER_END									= 52;
	
	public static final int RF_PROFILE_INFO_START									= 53;
	public static final int RF_PROFILE_INFO_END										= 54;
	
	public static final int CONFIG_HEADER_SIP_CONFIG_START	 						= 55;
	public static final int CONFIG_HEADER_SIP_CONFIG_END	 						= 56;
		
	public static final int CONFIG_HEADER_SIP_STA_INFO_START	 					= 57;
	public static final int CONFIG_HEADER_SIP_STA_INFO_END	 						= 58;
	
	/*
	 *  Keys that will be used under appropriate headers
	 */

	public static final int CONFIG_KEY_UNDEFINED			 						= -1;
	
	//CONFIG_HEADER_MODEL_INFO
	public static final int CONFIG_KEY_HARDWARE_MODEL		 						= 0;
	
	//CONFIG_HEADER_VERSION_INFO	
	public static final int CONFIG_KEY_SOFTWARE_VERSION		 						= 1;
	
	//CONFIG_HEADER_NODE_INFO		
	public static final int CONFIG_KEY_NODE_NAME			 						= 2;
	public static final int CONFIG_KEY_NODE_DESCRIPTION		 						= 3;
	public static final int CONFIG_KEY_LATITUDE				 						= 4;
	public static final int CONFIG_KEY_LONGITUDE		 							= 5;
	public static final int CONFIG_KEY_REGULATORY_DOMAIN	 						= 6;
	public static final int CONFIG_KEY_COUNTRY_CODE		 							= 7;
	
	
	//CONFIG_HEADER_NETWORK_INFO		
	public static final int CONFIG_KEY_HOST_NAME			 						= 8;
	public static final int CONFIG_KEY_IP_ADDRESS			 						= 9;
	public static final int CONFIG_KEY_SUBNET_MASK			 						= 10;
	public static final int CONFIG_KEY_GATEWAY_ADDRESS		 						= 11;

	//CONFIG_HEADER_MESH_INFO		
	public static final int CONFIG_KEY_HEARTBEAT_INTERVAL			 				= 12;
	public static final int CONFIG_KEY_HOP_COST				 						= 13;
	public static final int CONFIG_KEY_CHANGE_RESISTANCE	 						= 14;
	public static final int CONFIG_KEY_FRAGMENTATION_THRESHOLD	 					= 15;
	public static final int CONFIG_KEY_SIGNAL_MAP			 						= 16;
	public static final int CONFIG_KEY_PREFERRED_PARENT		 						= 17;
	
	//CONFIG_HEADER_MOBILITY_INFO
	public static final int CONFIG_KEY_MOBILITY_MODE		 						= 18;
	public static final int CONFIG_KEY_MOBILITY_INDEX		 						= 19;
	public static final int CONFIG_KEY_MOBILITY_DISTANCE	 						= 20;
	public static final int CONFIG_KEY_MOBILITY_SPEED		 						= 21;
	
	//CONFIG_HEADER_PORT_CONFIG	
	public static final int CONFIG_KEY_PORT_COUNT			 						= 22;
	
	//CONFIG_HEADER_PORT_INFO		
	public static final int CONFIG_KEY_PORT_NAME			 						= 23;
	public static final int CONFIG_KEY_PORT_ESSID			 						= 24;
	public static final int CONFIG_KEY_PORT_TX_RATE			 						= 25;
	public static final int CONFIG_KEY_PORT_TX_POWER		 						= 26;
	public static final int CONFIG_KEY_PORT_FRAG_THRESHOLD	 						= 27;
	public static final int CONFIG_KEY_PORT_RTS_THRESHOLD	 						= 28;
	public static final int CONFIG_KEY_PORT_DCA_ENABLED		 						= 29;
	public static final int CONFIG_KEY_PORT_CHANNEL			 						= 30;
	public static final int CONFIG_KEY_PORT_DCA_LIST		 						= 31;
	public static final int CONFIG_KEY_PORT_BEACON_INTERVAL	 						= 32;
	public static final int CONFIG_KEY_PORT_USAGE_TYPE		 						= 33;
	public static final int CONFIG_KEY_PORT_MEDIUM_SUB_TYPE	 						= 34;
	public static final int CONFIG_KEY_PORT_MEDIUM_TYPE		 						= 35;
	public static final int CONFIG_KEY_PORT_SERVICE_TYPE	 						= 36;
	public static final int CONFIG_KEY_PORT_ACK_TIMEOUT		 						= 37;
	public static final int CONFIG_KEY_PORT_HIDE_ESSID		 						= 38;
	
	
	//CONFIG_HEADER_CUSTOM_CHANNEL_CONFIG
	public static final int CONFIG_KEY_CHANNEL_CONFIG_COUNT		 					= 39;
	public static final int CONFIG_KEY_CHANNEL_CONFIG_BANDWIDTH	 					= 40;
	public static final int CONFIG_KEY_CHANNEL_CONFIG_CONFORMANCE 					= 41;
	public static final int CONFIG_KEY_CHANNEL_CONFIG_MAX_POWER 					= 42;
	public static final int CONFIG_KEY_CHANNEL_CONFIG_MAX_ANTENNA_GAIN				= 43;

	//CONFIG_HEADER_CHANNEL_INFO,CONFIG_HEADER_CUSTOM_CHANNEL_INFO	
	public static final int CONFIG_KEY_CHANNEL_NUMBER			 					= 44;
	public static final int CONFIG_KEY_CHANNEL_CENTER_FREQUENCY 					= 45;
	public static final int CONFIG_KEY_CHANNEL_FLAGS			 					= 46;
	public static final int CONFIG_KEY_CHANNEL_MAX_POWER		 					= 47;
	public static final int CONFIG_KEY_CHANNEL_MAX_REG_POWER	 					= 48;
	public static final int CONFIG_KEY_CHANNEL_MIN_POWER		 					= 49;
	
	//CONFIG_HEADER_SECURITY_CONFIG				
	public static final int CONFIG_KEY_SECURITY_TYPE			 					= 50;
	
	//CONFIG_HEADER_WEP_INFO		
	public static final int CONFIG_KEY_SECURITY_WEP_KEY_INDEX	 					= 51;
	public static final int CONFIG_KEY_SECURITY_WEP_KEY1		 					= 52;
	public static final int CONFIG_KEY_SECURITY_WEP_KEY2		 					= 53;
	public static final int CONFIG_KEY_SECURITY_WEP_KEY3		 					= 54;
	public static final int CONFIG_KEY_SECURITY_WEP_KEY4		 					= 55;
	public static final int CONFIG_KEY_SECURITY_WEP_PASSPHRASE	 					= 56;
	public static final int CONFIG_KEY_SECURITY_WEP_STRENGTH	 					= 57;
	
	//CONFIG_HEADER_WPA_PERSONAL_INFO
	public static final int CONFIG_KEY_SECURITY_WPA_PESONAL_CIPHER_CCMP 			= 58;
	public static final int CONFIG_KEY_SECURITY_WPA_PESONAL_CIPHER_TKIP 			= 59;
	public static final int CONFIG_KEY_SECURITY_WPA_PESONAL_GROUP_RENEWAL			= 60;
	public static final int CONFIG_KEY_SECURITY_WPA_PESONAL_KEY_LENGTH				= 61;
	public static final int CONFIG_KEY_SECURITY_WPA_PESONAL_KEY			 			= 62;
	public static final int CONFIG_KEY_SECURITY_WPA_PESONAL_MODE		 			= 63;
	public static final int CONFIG_KEY_SECURITY_WPA_PESONAL_PASSPHRASE	 			= 64;
	
	//CONFIG_HEADER_WPA_ENTERPRISE_INFO
	public static final int CONFIG_KEY_SECURITY_WPA_ENTERPRISE_CIPHER_CCMP			= 65;
	public static final int CONFIG_KEY_SECURITY_WPA_ENTERPRISE_CIPHER_TKIP			= 66;
	public static final int CONFIG_KEY_SECURITY_WPA_ENTERPRISE_GROUP_KEY_RENEWAL	= 67;
	public static final int CONFIG_KEY_SECURITY_WPA_ENTERPRISE_SERVER_IP_ADDRESS	= 68;
	public static final int CONFIG_KEY_SECURITY_WPA_ENTERPRISE_SERVER_PORT			= 69;
	public static final int CONFIG_KEY_SECURITY_WPA_ENTERPRISE_KEY					= 70;
	public static final int CONFIG_KEY_SECURITY_WPA_ENTERPRISE_MODE					= 71;
	public static final int CONFIG_KEY_SECURITY_WPA_ENTERPRISE_DECIDES_VLAN			= 72;
	
	//CONFIG_HEADER_VLAN_CONFIG
	public static final int CONFIG_KEY_VLAN_CONFIG_COUNT							= 73;
	public static final int CONFIG_KEY_VLAN_CONFIG_DEFAULT_TAG						= 74;
	public static final int CONFIG_KEY_VLAN_CONFIG_DEFAULT_80211E_ENABLED			= 75;
	public static final int CONFIG_KEY_VLAN_CONFIG_DEFAULT_80211E					= 76;
	public static final int CONFIG_KEY_VLAN_CONFIG_DEFAULT_8021P					= 77;
	
	//CONFIG_HEADER_VLAN_INFO
	public static final int CONFIG_KEY_VLAN_INFO_NAME								= 78;
	public static final int CONFIG_KEY_VLAN_INFO_IP_ADDRESS							= 79;
	public static final int CONFIG_KEY_VLAN_INFO_TAG								= 80;
	public static final int CONFIG_KEY_VLAN_INFO_8021P_PRIORITY						= 81;
	public static final int CONFIG_KEY_VLAN_INFO_80211E_ENABLED						= 82;
	public static final int CONFIG_KEY_VLAN_INFO_80211E_PRIORITY					= 83;
	public static final int CONFIG_KEY_VLAN_INFO_ESSID								= 84;
	
	//CONFIG_HEADER_80211E_CATEGORY_CONFIG
	public static final int CONFIG_KEY_80211E_CATEGORY_COUNT						= 85;

	//CONFIG_HEADER_80211E_CATEGORY_INFO
	public static final int CONFIG_KEY_80211E_CATEGORY_NAME							= 86;
	public static final int CONFIG_KEY_80211E_CATEGORY_BURST_TIME					= 87;
	public static final int CONFIG_KEY_80211E_CATEGORY_ACWMIN						= 88;
	public static final int CONFIG_KEY_80211E_CATEGORY_ACWMAX						= 89;
	public static final int CONFIG_KEY_80211E_CATEGORY_AIFSN						= 90;
	public static final int CONFIG_KEY_80211E_CATEGORY_DISABLE_BACKOFF				= 91;

	//CONFIG_HEADER_ACL_CONFIG
	public static final int CONFIG_KEY_ACL_COUNT									= 92;
	
	//CONFIG_HEADER_ACL_INFO
	public static final int CONFIG_KEY_ACL_STA_ADDRESS								= 93;
	public static final int CONFIG_KEY_ACL_VLAN_TAG									= 94;
	public static final int CONFIG_KEY_ACL_ALLOW_CONNECTION							= 95;
	public static final int CONFIG_KEY_ACL_80211E_ENABLED							= 96;
	public static final int CONFIG_KEY_ACL_80211E_CATEGORY							= 97;
	
	//CONFIG_HEADER_EFFISTREAM_RULE_INFO

	public static final int CONFIG_KEY_EFFISTREAM_RULE_COUNT	 					= 98;
	
	public static final int CONFIG_KEY_EFFISTREAM_RULE_CRITERIA_ID 					= 99;
	public static final int CONFIG_KEY_EFFISTREAM_RULE_VALUE 						= 100;
	public static final int CONFIG_KEY_EFFISTREAM_RULE_CHILD_COUNT 					= 101;
	
	//CONFIG_HEADER_EFFISTREAM_ACTION_INFO
	public static final int CONFIG_KEY_EFFISTREAM_ACTION_80211E_CATEGORY			= 102;
	public static final int CONFIG_KEY_EFFISTREAM_ACTION_NO_ACK_POLICY				= 103;
	public static final int CONFIG_KEY_EFFISTREAM_ACTION_DROP_PACKET				= 104;
	public static final int CONFIG_KEY_EFFISTREAM_ACTION_BIT_RATE   				= 105;
	public static final int CONFIG_KEY_EFFISTREAM_ACTION_QUEUED_RETRY   			= 106;
	
	public static final int CONFIG_KEY_GLOBAL_DYNAMIC_CHANNEL_ALLOC		   			= 107;
	
	//RF PROFILE HEADERS
	public static final int RF_PROFILE_NAME											= 108;

	//CONFIG_HEADER_SIP_CONFIG
	public static final int CONFIG_KEY_SIP_ENABLED									= 109;
	public static final int CONFIG_KEY_SIP_ENABLE_IN_P3M							= 110;
	public static final int CONFIG_KEY_SIP_IP_ADDRESS								= 111;
	public static final int CONFIG_KEY_SIP_PORT										= 112;
	public static final int CONFIG_KEY_SIP_STA_COUNT								= 113;

	//CONFIG_HEADER_SIP_STA_INFO
	public static final int CONFIG_KEY_SIP_STA_INFO_EXTN							= 114;
	public static final int CONFIG_KEY_SIP_STA_INFO_MAC_ADDR						= 115;
	
}
