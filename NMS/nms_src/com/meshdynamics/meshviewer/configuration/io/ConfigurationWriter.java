package com.meshdynamics.meshviewer.configuration.io;

import java.io.OutputStream;

public class ConfigurationWriter {
	protected OutputStream outputStream;
	
	public ConfigurationWriter(OutputStream outputStream) {
		this.outputStream = outputStream;
	}
	
	public boolean writeHeader(int header) {
		return true;
	}
	
	public boolean writeEntry(int key,String entryValue) {
		return true;
	}
	public boolean writeHeader(int header, int tabCount) {
		return true;
	}
	
	public boolean writeEntry(int key,String entryValue, int tabCount) {
		return true;
	}
	
}
