package com.meshdynamics.meshviewer.configuration.io;


public class ConfigFileTokens {

	public static String[]strHeaderTokens	=
	{
		"#MeshDynamics",

		"[MODEL INFO START]",
		"[MODEL INFO END]",

		"[VERSION INFO START]",
		"[VERSION INFO END]",

		"[NODE INFO START]",
		"[NODE INFO END]",

		"[NETWORK INFO START]",
		"[NETWORK INFO END]",

		"[MESH INFO START]",
		"[MESH INFO END]",

		"[MOBILITY INFO START]",
		"[MOBILITY INFO END]",

		"[PORT INFO START]",
		"[PORT INFO END]",

		"[PORT CONFIG START]",
		"[PORT CONFIG END]",

		"[CHANNEL INFO START]",
		"[CHANNEL INFO END]",

		"[CHANNEL CONFIG START]",
		"[CHANNEL CONFIG END]",

		"[SECURITY CONFIG START]",
		"[SECURITY CONFIG END]",

		"[WEP INFO START]",
		"[WEP INFO END]",

		"[WPA PERSONAL INFO START]",
		"[WPA PERSONAL INFO END]",

		"[WPA ENTERPRISE INFO START]",
		"[WPA ENTERPRISE INFO END]",

		"[CUSTOM CHANNEL INFO START]",
		"[CUSTOM CHANNEL INFO END]",

		"[CUSTOM CHANNEL CONFIG START]",
		"[CUSTOM CHANNEL CONFIG END]",
		
		"[VLAN INFO START]",
		"[VLAN INFO END]",

		"[VLAN CONFIG START]",
		"[VLAN CONFIG END]",

		"[802.11E CATEGORY CONFIG START]",
		"[802.11E CATEGORY CONFIG END]",

		"[802.11E CATEGORY INFO START]",
		"[802.11E CATEGORY INFO END]",

		"[ACL CONFIG START]",
		"[ACL CONFIG END]",

		"[ACL INFO START]",
		"[ACL INFO END]",
		
		"[EFFISTREAM CONFIG START]",
		"[EFFISTREAM CONFIG END]",
		
		"[EFFISTREAM RULE START]",
		"[EFFISTREAM RULE END]",
		
		"[EFFISTREAM ACTION START]",
		"[EFFISTREAM ACTION END]",
		
		"[RF PROFILE START]",
		"[RF PROFILE END]",
		
		"[RF PROFILE INFO START]",
		"[RF PROFILE INFO END]",
		
		"[PBV CONFIG START]",
		"[PBV CONFIG END]",

		"[PBV STA INFO START]",
		"[PBV STA INFO END]"
		
	};

	public static String[] strKeyTokens	=
	{
		"Hardware Model                = ",	
		"Software Version              = ",
		"Node Name                     = ",
		"Node Description              = ",
		"Latitude                      = ",
		"Longitude                     = ",
		"Regulatory Domain             = ",
		"Country Code                  = ",
		"Host Name                     = ",
		"IP Address                    = ",
		"Subnet Mask                   = ",
		"Gateway Address               = ",
		"Heartbeat Interval            = ",
		"Hop Cost                      = ",
		"Change Resistance             = ",
		"Fragmentation Threshold       = ",
		"Signal Map                    = ",
		"Preferred Parent              = ",
		"Mobility Mode                 = ",
		"Mobility Index                = ",
		"Mobility Distance             = ",
		"Mobility Speed                = ",
		"Port Count                    = ",
		"Port Name                     = ",
		"Port ESSID                    = ",
		"TX Rate                       = ",
		"TX Power                      = ",
		"Frag Threshold                = ",
		"RTS Threshold                 = ",
		"DCA Enabled                   = ",
		"Channel                       = ",
		"DCA List                      = ",
		"Beacon Interval               = ",
		"Usage Type                    = ",
		"Medium Subtype                = ",
		"Medium Type                   = ",
		"Service Type                  = ",
		"Ack Timeout                   = ",
		"Hide ESSID                    = ",
		"Channel Count                 = ",
		"Bandwidth                     = ",
		"Conformance                   = ",
		"Max Power                     = ",
		"Max Antenna Gain              = ",
		"Channel Number                = ",
		"Center Frequency              = ",
		"Flags                         = ",
		"Max Power                     = ",
		"Max Reg Power                 = ",
		"Min Power                     = ",
		"Security Type                 = ",
		"WEP Key Index                 = ",
		"WEP Key1                      = ",
		"WEP Key2                      = ",
		"WEP Key3                      = ",
		"WEP Key4                      = ",
		"WEP Passphrase                = ",
		"WEP Strength                  = ",
		"WPA Personal Cipher CCMP      = ",
		"WPA Personal Cipher TKIP      = ",
		"WPA Personal Group Renewal    = ",
		"WPA Personal Key Length       = ",
		"WPA Personal KEY	           = ",
		"WPA Personal Mode             = ",
		"WPA Personal Passphrase       = ",
		"WPA Enterprise Cipher CCMP         = ",
		"WPA Enterprise Cipher TKIP         = ",
		"WPA Enterprise Group Renewal       = ",
		"WPA Enterprise Server IP Address   = ",
		"WPA Enterprise Server Port         = ",
		"WPA Enterprise KEY                 = ",
		"WPA Enterprise Mode                = ",
		"WPA Enterprise decides VLAN        = ",
		"Vlan Count                    = ",
		"Vlan Default Tag              = ",
		"Vlan Default 802.11E Enabled  = ",
		"Vlan Default 802.11E          = ",
		"Vlan Default 802.1P           = ",
		"Vlan Name                     = ",
		"Vlan IP Address               = ",
		"Vlan Tag                      = ",
		"Vlan 802.11P Priority         = ",
		"Vlan 802.11E Enabled          = ",
		"Vlan 802.11E Priority         = ",
		"Vlan ESSID                    = ",
		"802.11E Category Count        = ",
		"Category Name                 = ",
		"Category Burst Time           = ",
		"Category ACWMin               = ",
		"Category ACWMax               = ",
		"Category AIFSN                = ",
		"Category Disable Backoff      = ",
		"ACL Count                     = ",
		"Sta Address                   = ",
		"ACL Vlan Tag                  = ",
		"Allow Connection              = ",
		"802.11E Enabled               = ",
		"802.11E Category Index        = ",
		"Rule count = ",
		"Criteria ID = ",
		"Value       = ",
		"Child count = ",
		"802.11e Category = ",
		"No Ack Policy    = ",
		"Drop Packet      = ",
		"Bitrate          = ",
		"Queued Retry      = ", /*106*/
		"Global DCA       = ", /*107*/
		"Profile Name     = ",  /*108*/
		
		"PBV Enabled			= ", /* 109 */
		"PBV Enabled in P3M		= ",
		"PBV Server Ip Address	= ",
		"PBV Port				= ",
		"PBV Sta Count			= ",
		"PBV Sta Extn			= ",
		"PBV Sta Mac Address 	= " /* 115 */
		
	};

	public static String getStrHeaderToken(int index){
		if(index < 0)
			return "";
		return strHeaderTokens[index];
	}

	public static String getStrKeyToken(int index){
		return strKeyTokens[index];
	}

	public static int getHeaderIndex(String header){

		for(int i=0; i<strHeaderTokens.length;i++){
			if(strHeaderTokens[i].startsWith(header)== true){
				return i;
			}
		}
		return -1;
	}

	public static int getKeyIndex(String key){

		for(int i=0; i<strKeyTokens.length;i++){
			if(strKeyTokens[i].startsWith(key)== true){
				return i;
			}
		}
		return -1;
	}

}






