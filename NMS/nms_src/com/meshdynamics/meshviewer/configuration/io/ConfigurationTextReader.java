/*
 * Created on Feb 27, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.configuration.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ConfigurationTextReader extends ConfigurationReader{

	private		BufferedReader 		bufferedReader;
	private 	String				input;
	
	public ConfigurationTextReader(InputStream inputStream) {
		super(inputStream);
		bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	}
	
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.io.ConfigurationReader#getNextToken()
	 */
	public boolean getNextToken() {
		
		try {
				input		=	bufferedReader.readLine();
				if(input	==	null)
					return false;
				
				if(input.equals("#MeshDynamics")){
					return true;
				}
				
				String [] values = input.split("=");
			
				if(values.length == 1){
					currentTokenType = IConfigurationIODefs.TOKEN_TYPE_HEADER;
					currentHeader	 = ConfigFileTokens.getHeaderIndex(values[0].trim());
					
				}else if(values.length == 2){
					currentTokenType  = IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE;
					currentTokenKey   = ConfigFileTokens.getKeyIndex(values[0].trim());
					currentTokenValue = values[1].trim();
				}
		
			}catch (IOException e){
				e.printStackTrace();
			}
		return true;
	}

	
}
