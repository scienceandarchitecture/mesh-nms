package com.meshdynamics.meshviewer.configuration;

public interface IVlanConfiguration extends ISerializableConfiguration, ICopyable, IComparable{
	
    public	void					setDefaultTag(short tag);
	public short					getDefaultTag();
	
	public void						setDefault8021pPriority(short priority);
	public short					getDefault8021pPriority();
	
	public void						setDefault80211eEnabled(short enabled);
	public short					getDefault80211eEnabled();
	
	public void						setDefault80211eCategoryIndex(short value);			
	public short					getDefault80211eCategoryIndex();

	public void						setVlanCount(int count);
	public int						getVlanCount();
		
	public IVlanInfo				getVlanInfoByName(String vlanName);
	public IVlanInfo				getVlanInfoByIndex(int index);
	public IVlanInfo				getVlanInfoByEssid(String vlanEssid);
	
	/*
	 * return matching vlanInfo or create new info
	 */
	public IVlanInfo				addVlanInfo(String vlanName);
	public void 					deleteVlanInfo(String vlanName);
	
	public IVlanInfo              	addVlanInfo(String essid, short tag);
	public boolean                  deleteVlanInfo(short tag);
	
}
