package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.util.MacAddress;

public interface IInterfaceInfo extends ISerializableConfiguration, ICopyable, IComparable{
	
	public void 		setName(String interfaceName);
	public String 		getName(); 

	public void 		setChannel(short channel);
	public short 		getChannel(); 

	public void 		setChannelBondingType(short bonding);
	public short 		getChannelBondingType(); 
	
	public void 		setMediumType(short mediumType);
	public short 		getMediumType();

	public void 		setMediumSubType(short mediumSubType);
	public short 		getMediumSubType();
	
	public void 		setUsageType(short usageType);
	public short 		getUsageType();
	
	public void 		setServiceType(short serviceType);
	public short 		getServiceType();

	public void			setEssid(String essId);
	public String 		getEssid();
	
	public void			setEssidHidden(short hidden);
	public short 		getEssidHidden();
	
	public void 		setTxRate(long txRate);	
	public long 		getTxRate();
	
	public void 		setTxPower(short txPower);	
	public short 		getTxPower();

	public short	    getAckTimeout();
	public void 		setAckTimeout(short ackTimeout);

	public void			setRtsThreshold(long rts);
	public long			getRtsThreshold();
	
	public void			setFragThreshold(long frg);
	public long 		getFragThreshold();
	
	public void 		setDynamicChannelAlloc(short dynamicChannelAlloc);
	public short 		getDynamicChannelAlloc();

	public void 		setDcaListCount(short dcaListCount);
	public short 		getDcaListCount();

	public void 		setDcaChannel(int index,short channel);
	public short 		getDcaChannel(int index);
	
	public long 		getBeaconInterval();
	public void			setBeaconInterval(long beaconInterval); 
	
	public MacAddress	getMacAddress();
	public void 		setMacAddress(MacAddress newAddress);
	
	public IInterfaceChannelConfiguration 
						getSupportedChannelConfiguration();

	public IInterfaceChannelConfiguration 
						getCustomChannelConfiguration();
	
	public ISecurityConfiguration 
						getSecurityConfiguration();

	public void 		setOperatingChannel(short channel);
	public short 		getOperatingChannel(); 
	
	public void 		setDot11eCategory(short dot11eCategory);
	public short 		getDot11eCategory();
	
	public void 		setDot11eEnabled(short dot11eEnabled);
	public short 		getDot11eEnabled();
	
	public void 		setAppCtrl(short appCtrl);
	public short 		getAppCtrl();
	
	public ISaturationInfo
						getSaturationInfo();
	//new entry
	public short getSupportedProtocol();
	public void setSupportedProtocol(short supportedProtocol);
	
	public IInterfaceAdvanceConfiguration getInterfaceAdvanceConfiguration();
	public void setInterfaceAdvanceConfiguration(IInterfaceAdvanceConfiguration interfaceAdvanceConfiguration);
	
	
	
}
