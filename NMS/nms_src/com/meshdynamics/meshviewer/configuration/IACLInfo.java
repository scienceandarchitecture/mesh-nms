package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.util.MacAddress;

public interface IACLInfo extends ISerializableConfiguration, ICopyable, IComparable{

    public static final short DOT_11_E_CATEGORY_BACKGROUND		= 0;
	public static final short DOT_11_E_CATEGORY_BEST_EFFORT		= 1;
	public static final short DOT_11_E_CATEGORY_VIDEO			= 2;
	public static final short DOT_11_E_CATEGORY_VOICE			= 3;	
	
	public void 		setStaAddress(MacAddress staAddress);
	public MacAddress 	getStaAddress();
	
	public void	 		setVLANTag(int tag);
	public int 			getVLANTag();
	
	public void 		setAllow(short allow);
	public short 		getAllow();
	
	public void 		setEnable80211eCategory(short enabled);
	public short 		getEnable80211eCategory();
	
	public void 		set80211eCategoryIndex(short category);
	public short 		get80211eCategoryIndex();
	
}
