package com.meshdynamics.meshviewer.configuration;




public interface I80211eCategoryInfo extends ISerializableConfiguration, ICopyable, IComparable{

    public void 		setName(String categoryName);
	public String 		getName();
	
	public void			setBurstTime(long burstTime);
	public long 		getBurstTime();
	
	public void			setACWMin(int aCWMin);
	public int	 		getACWMin();

	public void			setACWMax(int aCWMax);
	public int	 		getACWMax();
	
	public void			setAIFSN(short aifsn);
	public short 		getAIFSN();
	
	public void			setDisableBackoff(short disableBackoff);
	public short 		getDisableBackoff();
	
}
