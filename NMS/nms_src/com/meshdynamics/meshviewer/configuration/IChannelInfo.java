package com.meshdynamics.meshviewer.configuration;

public interface IChannelInfo extends ISerializableConfiguration, ICopyable, IComparable{
	
	public void 	setChannelNumber(short channelNumber);
	public short	getChannelNumber();
	
	public void 	setFrequency(long frequency);
	public long 	getFrequency();
	
	public void 	setFlags(long flags);
	public long 	getFlags();
	
	public void 	setMaxRegPower(long maxRegPower);
	public long 	getMaxRegPower();

	public void 	setMaxPower(long maxPower);
	public long 	getMaxPower();

	public void 	setMinPower(long minPower);
	public long 	getMinPower();
	
	public void 	setPrivateChannelFlags(short flag);
	public short 	getPrivateChannelFlags();
	
}
