/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : IRuntimeConfigStatus.java
 * Comments : 
 * Created  : Jun 18, 2007
 * Author   : Abhishek
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  | Jun 18, 2007  | Created                                         |Abhishek|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.configuration.runtimeconfiguration;


public interface IRuntimeConfigStatus {
	
	public boolean	isRfUpdateSent();
	public void 	setRfUpdateSent(boolean enable);

	public boolean	isCountryCodeChanged();
	public void 	setCountryCodeChanged(boolean enable);
	
	public void 	clear();
}
