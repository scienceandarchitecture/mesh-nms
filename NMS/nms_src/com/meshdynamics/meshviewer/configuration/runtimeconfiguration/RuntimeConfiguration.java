/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RuntimeConfiguration.java
 * Comments : 
 * Created  : May 8, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date          |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  2  |June 5, 2007  | sta list changed to hashtable			          |Abhijit |
 * --------------------------------------------------------------------------------
 * |  1  |May 31, 2007  | C'tor changed,parentDSAddress stored            |Abhijit |
 * --------------------------------------------------------------------------------
 * |  0  |May 18, 2007  | Created                                         |Abhishek|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.configuration.runtimeconfiguration;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.configuration.IStaConfiguration;
import com.meshdynamics.meshviewer.configuration.KnownAp;
import com.meshdynamics.meshviewer.configuration.KnownAp.KnownApInfo;
import com.meshdynamics.meshviewer.imcppackets.HeartBeat2Packet;
import com.meshdynamics.meshviewer.imcppackets.StaAssocPacket;
import com.meshdynamics.meshviewer.imcppackets.StaDisassocPacket;
import com.meshdynamics.meshviewer.imcppackets.StaInfoPacket;
import com.meshdynamics.meshviewer.mesh.IHeartbeatInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.ApRuntimeConfigurationManager;
import com.meshdynamics.meshviewer.util.StaEntry;
import com.meshdynamics.meshviewer.util.StaSignalEntry;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.util.MacAddress;

public class RuntimeConfiguration implements IRuntimeConfiguration {
    
    private static final int MESH_FLAG_REBOOT_REQUIRED 	= 2;
    private static final int MESH_FLAG_RADAR_DETECT 	= 8;
    private static final int MESH_FLAG_FIPS				= 16;
	
	private boolean									rebootRequired;
	private boolean									radarDetected;
	private boolean									firstHeartbeat;
	private boolean									fipsDetected;
	
	private Hashtable<String, IStaConfiguration>	staListHashTable;
	private Hashtable<String, KnownAp>				knownApTable;
	private long									lastHeartbeatTime;
	private IVersionInfo							versionInfo;
	
	//Heartbeat2 packet
	
	private long 			 						uplinkTransmitRate;
	private long 			 						dwnlinkTransmitRate;
	private long									flags;
	private long									configSeqNumber;
	private short									capabilities;				
	private short									cmdcapabilities;
	private short									configcapabilities;
	
	//Heartbeat Packet
	private long 									hb1SequenceNumber;
	private long 									hb2SequenceNumber;
	private long 									cumulativeTollCost;
	private short 									hopCount;
	private MacAddress 								rootBssid;
	private long 									cumulativeRSSI;
	private short 									cpuUsage;
	private short 									heartbeatInterval;
	private long 									bitRate;
	private short 									staCount;
	private MacAddress 								parentBssid;
	private long 									txBytes;
	private long 									rxBytes;
	private long 									txPackets;
	private long 									rxPackets;
	private double 									boardTemperature;
	private ApRuntimeConfigurationManager			manager;
	private MacAddress 								parentDSMacAddress;
	private Vector<KnownApInfo>						parentInfo;
	
	/*decoded GPS info*/
	private String									longitude;
	private String									latitude;
	private String									altitude;
	private String									speed;
		
	/**
	 * @param info
	 * 
	 */
	public RuntimeConfiguration(ApRuntimeConfigurationManager manager, IVersionInfo info) {
		
		this.manager			= manager;
		this.versionInfo		= info;
		
		staListHashTable		= new Hashtable<String, IStaConfiguration>();	
		knownApTable			= new Hashtable<String, KnownAp>();
		parentInfo				= new Vector<KnownApInfo>();
		init();
	}

	private void init() {

		rebootRequired			= false;
		radarDetected			= false;
		fipsDetected			= false;
		firstHeartbeat			= true;
		
		initLastHeartbeatTime();
		hb1SequenceNumber		= 0;

		latitude				= "";
		longitude				= "";
		altitude				= "";
		speed					= "";
		
		staListHashTable.clear();
		knownApTable.clear();
		parentInfo.clear();
		parentInfo.add(new KnownAp.KnownApInfo());
	
	}
	
	private synchronized void initLastHeartbeatTime() {
		lastHeartbeatTime = 0;
	}

	/**
	 * @param heartBeat2Info
	 * 
	 */
	private void updateHb2Info(HeartBeat2Packet heartBeat2Info) {
		
		if(heartBeat2Info == null) {
			return;
		}
		hb2SequenceNumber		= heartBeat2Info.getSequenceNumber();
		uplinkTransmitRate		= heartBeat2Info.getUplinkTransmitRate();
		dwnlinkTransmitRate		= heartBeat2Info.getDwnlinkTransmitRate();
		flags					= heartBeat2Info.getFlags();
		
		radarDetected = ((flags & MESH_FLAG_RADAR_DETECT) != 0) ?
			true : false;
	
		rebootRequired = ((flags & MESH_FLAG_REBOOT_REQUIRED) != 0) ?
					true : false;
		
		fipsDetected  =  ((flags & MESH_FLAG_FIPS) != 0) ?
				true : false;		
		
		configSeqNumber			= heartBeat2Info.getConfigSeqNumber();
		capabilities			= heartBeat2Info.getCapabilities();
		cmdcapabilities			= heartBeat2Info.getCmdcapabilities();
		configcapabilities		= heartBeat2Info.getConfigcapabilities();
		
		StaSignalEntry 	[]staSignalEntries 	= heartBeat2Info.getStaSignalEntries();
	
		for(int i = 0; i < staSignalEntries.length; i++) {
		
			String 			 staMacAddress 	= staSignalEntries[i].macAddress.toString();
			IStaConfiguration staConf		= getStaInfoFromHash(staMacAddress); 
			
			if(manager.isStaIMCP(staSignalEntries[i].macAddress) == true) {
				staConf.setIsIMCP((short)Mesh.ENABLED);
				manager.setMeshClientNodeName(staConf);
			}
			staConf.setStaMacAddress(staSignalEntries[i].macAddress);
			staConf.setSignal(-96 + staSignalEntries[i].signal);
			staConf.setBitRate(staSignalEntries[i].dwnlinkTransmitRate);
		}
		
		Enumeration<String> enumStas	= staListHashTable.keys();
		while(enumStas.hasMoreElements()){
			
			String 	key		= (String) enumStas.nextElement();
			boolean found 	= false;
			
			for(int i = 0; i < staSignalEntries.length; i++) {
			
				String staMacAddress 	= staSignalEntries[i].macAddress.toString();
				if(staMacAddress.equalsIgnoreCase(key)) {
					found = true;
					break;
				}
			}
			if(found == false) {
				staListHashTable.remove(key);
			}
		}
		
		staCount			= (short)staListHashTable.size();
	}

	
	/**
	 * @param heartBeat2Info The heartBeat2Info to set.
	 */
	public void setHeartBeat2Info(HeartBeat2Packet heartBeat2InfoPkt) {
		
		setLastHeartbeatTime();
		updateHb2Info(heartBeat2InfoPkt);
		
	}
	
	
	/**
	 * @param staMacAddress
	 * @return
	 */
	private IStaConfiguration getStaInfoFromHash(String staMacAddress) {
		
		IStaConfiguration	staConf	= (IStaConfiguration) staListHashTable.get(staMacAddress);
		if(staConf == null) {
			staConf	= new StaConfiguration();
			staListHashTable.put(staMacAddress, staConf);		
		}		
		return (IStaConfiguration)staConf;
	}

	/**
	 * @param heartBeatInfo
	 * 
	 */
	private void updateHbInfo(IHeartbeatInfo heartBeatInfo) {
		
		if(heartBeatInfo == null) {
			return;
		}
		
		hb1SequenceNumber		= heartBeatInfo.getSequenceNumber();
		cumulativeTollCost		= heartBeatInfo.getCumulativeTollCost();
		hopCount				= heartBeatInfo.getHopCount();
		parentBssid				= heartBeatInfo.getParentBssid();
		parentDSMacAddress		= manager.getParentDSMacAddress(parentBssid);
		cumulativeRSSI			= heartBeatInfo.getCumulativeRSSI();
		cpuUsage				= heartBeatInfo.getHealthIndex();
		heartbeatInterval		= heartBeatInfo.getHeartbeatInterval();
		bitRate					= heartBeatInfo.getBitRate();
		txBytes					= heartBeatInfo.getByteSent();
		txPackets				= heartBeatInfo.getPacketSent();
		rxBytes					= heartBeatInfo.getRxBytes();
		rxPackets				= heartBeatInfo.getRxPackets();
		boardTemperature		= heartBeatInfo.getBoardTempareture();
		
		longitude				= decodeGpsInfo(heartBeatInfo.getLongitude());
		latitude				= decodeGpsInfo(heartBeatInfo.getLatitude());
		altitude				= String.valueOf(heartBeatInfo.getAltitude());
		speed					= String.valueOf(heartBeatInfo.getSpeed());
		
		KnownAp[] knownApList	= heartBeatInfo.getKnownAPs();
		knownApTable.clear();
		
		boolean parentInfoUpdated = false;
		for(int i = 0; i < knownApList.length; i++) {

			KnownAp knownAp = (KnownAp) knownApTable.get(knownApList[i].macAddress.toString());

			if(knownAp == null) {
				knownAp =  knownApList[i];
				knownApTable.put(knownApList[i].macAddress.toString(),knownAp);
			} else {
				knownAp.knownApInfo.add(knownApList[i].knownApInfo.get(0));
			}
			
			manager.setKnownAPNodeName(knownAp);
			
			if(parentInfoUpdated == true)
				continue;
			
			if(parentDSMacAddress != null) {
				if(knownApList[i].macAddress.equals(parentDSMacAddress)){
					KnownApInfo knownApInfo 	= (KnownApInfo) knownApList[i].knownApInfo.get(0);
					KnownApInfo parentApInfo 	= (KnownApInfo) parentInfo.get(0);
					parentApInfo.signal			= knownApInfo.signal;
					parentApInfo.bitRate		= knownApInfo.bitRate;
					parentInfoUpdated 			= true;
				}
			}
			
		}
		
		StaEntry	[]staEntries	= heartBeatInfo.getStaEntries();
		
		for(int i = 0; i < staEntries.length; i++) {
			
			String 			 staMacAddress  = staEntries[i].macAddress.toString();
			IStaConfiguration staConf		= getStaInfoFromHash(staMacAddress); 
			
			if(manager.isStaIMCP(staEntries[i].macAddress) == true){
				staConf.setIsIMCP((short)Mesh.ENABLED);
			}
			staConf.setStaMacAddress(staEntries[i].macAddress);
			staConf.setAssociationTime(staEntries[i].associationTime.toString());
			
		}

		Enumeration<String> enumStas	= staListHashTable.keys();
		while(enumStas.hasMoreElements()){
			
			String 	key		= (String) enumStas.nextElement();
			boolean found 	= false;
			
			for(int i = 0; i < staEntries.length; i++) {
			
				String staMacAddress 	= staEntries[i].macAddress.toString();
				if(staMacAddress.equalsIgnoreCase(key)) {
					found = true;
					break;
				}
			}
			if(found == false) {
				staListHashTable.remove(key);
			}
		}
		
		staCount	= (short) staListHashTable.size();
	}

	
	
	/**
	 * @param heartBeatInfo The heartBeatInfo to set.
	 */
	public void setHeartBeatInfo(IHeartbeatInfo heartBeatInfo) {
	
		setLastHeartbeatTime();
		updateHbInfo(heartBeatInfo);
		firstHeartbeat	= false;
	}
	
	/**
	 * @param staAssocInfo
	 */
	public void setStaAssocInfo(StaAssocPacket staAssocInfo) {
		
		IStaConfiguration staConf	= getStaInfoFromHash(staAssocInfo.getStaMacAddress().toString());
	
		staConf.setStaMacAddress(staAssocInfo.getStaMacAddress());
		staConf.setParentMacAddress(staAssocInfo.getDsMacAddress());
		staConf.setParentwmMacAddress(staAssocInfo.getWmMacAddress());
	
	}

	/**
	 * @param staDissocInfo
	 */
	public void setDissocInfo(StaDisassocPacket staDissocInfo) {
		staListHashTable.remove(staDissocInfo.getStaMacAddress().toString());
	}
	

	/**
	 * @param staInfo
	 */
	public void setStaInfo(StaInfoPacket staInfo) {
		
		if(staInfo == null) {
			return;
		}
		
		IStaConfiguration	newStaInfo	= getStaInfoFromHash(staInfo.getStaMacAddress().toString());
		
		newStaInfo.setStaMacAddress(staInfo.getStaMacAddress());
		newStaInfo.setParentwmMacAddress(staInfo.getParentwmMacAddress());
		newStaInfo.setParentESSID(staInfo.getParentESSID());
		newStaInfo.setAuthState(staInfo.getAuthState());
		newStaInfo.setBclient(staInfo.getBclient());
		newStaInfo.setIsIMCP(staInfo.getIsIMCP());
		newStaInfo.setListenInterval(staInfo.getListenInterval());
		newStaInfo.setCapability(staInfo.getCapability());
		newStaInfo.setKeyIndex(staInfo.getKeyIndex());
		newStaInfo.setBytesReceived(staInfo.getBytesReceived());
		newStaInfo.setBytesSent(staInfo.getBytesSent());
		newStaInfo.setActiveTime(staInfo.getActiveTime());
		newStaInfo.setLastResponseRcvd(staInfo.getLastResponseRcvd());
		
	}


	/**
	 * @return Returns the radarDetected.
	 */
	public boolean isRadarDetected() {
		return radarDetected;
	}
	
	/**
	 * @return Returns the rebootRequired.
	 */
	public boolean isRebootRequired() {
		return rebootRequired;
	}
	
	
	public boolean isFIPSDetected() {
		return fipsDetected;
	}
	
	public boolean isCapable(int packetType) {
	 	   
    	if(capabilities == 0){
 			return true;
 		}
 		
 		if(packetType >  capabilities) {
 		    return false;
 		}
 		return true;	
	 }
	 	
 	
	public boolean isCmdCapable(int comType) {
 		   
 		
 		if(cmdcapabilities == 0){
 			return true;
 		}
 		
 		if(comType > cmdcapabilities) {
 		    return false;
 		}
 		return true;	
 	}
 	
 	public boolean isConfigCapable(int comType) {
 		
 		if(configcapabilities == 0){
 			return true;
 		}
 		 		
 		if(comType <= configcapabilities) {
 			return true;
 		}
 		
 		return false;	
 	}
	 

	public boolean hasClient(MacAddress staAddress) {
		
		Enumeration<IStaConfiguration> staEnum	= staListHashTable.elements();
		
		while(staEnum.hasMoreElements()) {
			IStaConfiguration staInfo	= (IStaConfiguration)staEnum.nextElement();
			if(staAddress.equals(staInfo.getStaMacAddress()) == true) {
				return true;
			}	
		}
		return false;
	}


	public int getVoltage() {

		if(versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,IVersionInfo.MINOR_VERSION_5,IVersionInfo.VARIANT_VERSION_28) == false) { 
			return 0;
		}
		return (int)((cumulativeTollCost >> 8) & 0xFF);
	}


	public int getBoardFreeMemory() {
		
		if(versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,IVersionInfo.MINOR_VERSION_5,IVersionInfo.VARIANT_VERSION_65) == false) { 
			return 0;
		}
		return (int)((cumulativeTollCost >> 16) & 0xFF);		
	}
	
	public	int	getBoardCPUUsage() {
		
		if(versionInfo.versionGreaterThan(IVersionInfo.MAJOR_VERSION_2,IVersionInfo.MINOR_VERSION_5,IVersionInfo.VARIANT_VERSION_65) == false) { 
			return 0;
		}
		return cpuUsage;
		
	}
	
	/**
	 * @return Returns the bitRate.
	 */
	public long getBitRate() {
		return bitRate;
	}
	
	
	/**
	 * @return Returns the boardTemperature.
	 */
	public double getBoardTemperature() {
		return boardTemperature;
	}
	
	/**
	 * @return Returns the configSeqNumber.
	 */
	public long getConfigSeqNumber() {
		return configSeqNumber;
	}
	
	
	/**
	 * @return Returns the dwnlinkTransmitRate.
	 */
	public long getDwnlinkTransmitRate() {
		return dwnlinkTransmitRate;
	}
	
	
	/**
	 * @return Returns the heartbeatInterval.
	 */
	public short getHeartbeatInterval() {
		return heartbeatInterval;
	}
	
	
	/**
	 * @return Returns the hopCount.
	 */
	public short getHopCount() {
		return hopCount;
	}
	
	/**
	 * @return Returns the lastHeartbeatTime.
	 */
	public synchronized long getLastHeartbeatTime() {
		return lastHeartbeatTime;
	}
	
	/**
	 * @return Returns the parentBssid.
	 */
	public MacAddress getParentBssid() {
		return parentBssid;
	}
	
	/**
	 * @return Returns the parentSignal.
	 */
	public long getParentSignal() {
		return parentInfo.get(0).signal;
	}
	
	/**
	 * @return Returns the rootBssid.
	 */
	public MacAddress getRootBssid() {
		return rootBssid;
	}
	
	
	/**
	 * @return Returns the rxBytes.
	 */
	public long getRxBytes() {
		return rxBytes;
	}
	
	/**
	 * @return Returns the rxPackets.
	 */
	public long getRxPackets() {
		return rxPackets;
	}
	
	/**
	 * @return Returns the staCount.
	 */
	public short getStaCount() {
		return staCount;
	}
	
	
	/**
	 * @return Returns the txBytes.
	 */
	public long getTxBytes() {
		return txBytes;
	}
	
	/**
	 * @return Returns the txPackets.
	 */
	public long getTxPackets() {
		return txPackets;
	}
	
	/**
	 * @return Returns the uplinkTransmitRate.
	 */
	public long getUplinkTransmitRate() {
		return uplinkTransmitRate;
	}
	
	/**
	 * @return
	 */
	public Enumeration<IStaConfiguration> getStaList() {
		return staListHashTable.elements();
	}


	public IStaConfiguration getStaInformation(MacAddress staAddress) {
		return (IStaConfiguration) staListHashTable.get(staAddress.toString());
	}


	/**
	 * @return
	 */
	public Enumeration<KnownAp> getKnownAPs() {
		return knownApTable.elements();
	}

	public int getKnownApCount() {
		return knownApTable.size();
	}

	public KnownAp getKnownAp(MacAddress kapMacAddress) {
		return (KnownAp) knownApTable.get(kapMacAddress.toString());
	}
	
	public long getHeartbeat1SequenceNumber() {
		return hb1SequenceNumber;
	}
	
	public long getHeartbeat2SequenceNumber() {
		return hb2SequenceNumber;
	}
	
	/**
	 * @return
	 */
	public boolean is1stHeartbeat() {
		return firstHeartbeat;
	}



	public MacAddress getParentDSMacAddress() {
		return parentDSMacAddress;
	}

	public void clearData() {
		init();
	}

	public Enumeration<KnownApInfo> getParentInfo() {
		return parentInfo.elements();
	}

	public void setRebootRequired(boolean enable) {
		rebootRequired = enable;
	}
	
    public String getAltitude() {
        return altitude;
    }
    
    public String getLatitude() {
        return latitude;
    }
    
    public String getLongitude() {
        return longitude;
    }
    
    public String getSpeed() {
        return speed;
    }
    
    
    public static String decodeGpsInfo(long coordinate) {
        
        String	coor;
        int 	degree;
        int		mantissa;
        int		sign;
        double 	dblMantissa;
        
        coor		= new String("");
        sign		= (int)(coordinate >> 31);
        degree 		= (int)(coordinate & 0xFF);
        mantissa 	= (int)((coordinate >> 8) & 0x7FFFFF);
        
        dblMantissa	= mantissa / (1000000.0);
        dblMantissa	+= degree;
        
        if(sign != 0) {
            coor = "-";
        }
        
        //DecimalFormat df = new DecimalFormat("##000.000000");
        
        coor = coor + String.valueOf(dblMantissa);
        return coor;
    }
    
    public synchronized void setLastHeartbeatTime() {
		lastHeartbeatTime = System.currentTimeMillis();
	}
}
