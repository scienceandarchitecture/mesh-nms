/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RuntimeConfigStatus.java
 * Comments : 
 * Created  : Jun 18, 2007
 * Author   : Abhishek
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  | Jun 18, 2007  | Created                                         |Abhishek|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.configuration.runtimeconfiguration;

import com.meshdynamics.meshviewer.mesh.ap.ApDataController;

public class RuntimeConfigStatus implements IRuntimeConfigStatus{

	
	private boolean 		 	rfConfigUpdateSent;
	private boolean 		 	countryCodeChanged;
	
	/**
	 * @param dataManager
	 * 
	 */
	public RuntimeConfigStatus(ApDataController dataManager) {
		rfConfigUpdateSent		= false;
		countryCodeChanged		= false;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.runtimeconfiguration.IRuntimeConfigStatus#isRfUpdateSent()
	 */
	public boolean isRfUpdateSent() {
		return rfConfigUpdateSent;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.runtimeconfiguration.IRuntimeConfigStatus#setRfUpdateSent()
	 */
	public void setRfUpdateSent(boolean enable) {
		rfConfigUpdateSent	= enable;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.runtimeconfiguration.IRuntimeConfigStatus#isCountryCodeChanged()
	 */
	public boolean isCountryCodeChanged() {
		return countryCodeChanged;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.runtimeconfiguration.IRuntimeConfigStatus#setCountryCodeChanged(boolean)
	 */
	public void setCountryCodeChanged(boolean enable) {
		countryCodeChanged	= enable;	
	}

	public void clear() {
		this.countryCodeChanged = false;
		this.rfConfigUpdateSent = false;
	}
	
	
}
