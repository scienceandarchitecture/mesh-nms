/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : StaConfiguration.java
 * Comments : 
 * Created  : May 8, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |May 18, 2007  | Created                                         |Abhishek|
 * --------------------------------------------------------------------------------
 **********************************************************************************/


package com.meshdynamics.meshviewer.configuration.runtimeconfiguration;

import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IStaConfiguration;
import com.meshdynamics.util.MacAddress;

public class StaConfiguration implements IStaConfiguration, ICopyable {

	
	private MacAddress 			staMacAddress;
	private MacAddress 			parentwmMacAddress;
	private String 				parentESSID;
	private short 				authState;
	private short				bclient;
	private short 				isIMCP;
	private int					listenInterval;
	private short				capability;
	private short				keyIndex;
	private long				bytesReceived;
	private long				bytesSent;
	private long				activeTime;
	private String				lastResponseRcvd;
    
	private long 			 	dwnlinkTransmitRate;
	private long				signal;
	
	private String				associationTime;
	
	private MacAddress 			parentMacAddress;
	
	private MacAddress 			dissocStaMacAddress;
	private int 				dissocReason;
	
	/*if sta is MESH Node*/
	private String				nodeName;
	
	public StaConfiguration() {

		staMacAddress 		= null;
		parentwmMacAddress  = null;
		parentESSID 		= null;
		authState 			= 0;
		bclient				= 0;
		isIMCP				= 0;
		listenInterval		= 0;
		capability 			= 0;
		keyIndex			= 0;
		bytesReceived		= -1;
		bytesSent 			= -1;
		activeTime 			= 0;
		lastResponseRcvd	= null;

		dwnlinkTransmitRate	= 0;
		signal				= 0;
		
		associationTime		= "";
		
		parentMacAddress	= null;
		
		dissocReason		= 0;
		dissocStaMacAddress	= null;
		nodeName			= "";			
	}
	
	
	
	/**
	 * @return Returns the associationTime.
	 */
	public String getAssociationTime() {
		return associationTime;
	}
	/**
	 * @param associationTime The associationTime to set.
	 */
	public void setAssociationTime(String associationTime) {
		this.associationTime = associationTime;
	}
	/**
	 * @return Returns the activeTime.
	 */
	public long getActiveTime() {
		return activeTime;
	}
	/**
	 * @param activeTime The activeTime to set.
	 */
	public void setActiveTime(long activeTime) {
		this.activeTime = activeTime;
	}
	/**
	 * @return Returns the authState.
	 */
	public short getAuthState() {
		return authState;
	}
	/**
	 * @param authState The authState to set.
	 */
	public void setAuthState(short authState) {
		this.authState = authState;
	}
	/**
	 * @return Returns the bclient.
	 */
	public short getBclient() {
		return bclient;
	}
	/**
	 * @param bclient The bclient to set.
	 */
	public void setBclient(short bclient) {
		this.bclient = bclient;
	}
	/**
	 * @return Returns the bytesReceived.
	 */
	public long getBytesReceived() {
		return bytesReceived;
	}
	/**
	 * @param bytesReceived The bytesReceived to set.
	 */
	public void setBytesReceived(long bytesReceived) {
		this.bytesReceived = bytesReceived;
	}
	/**
	 * @return Returns the bytesSent.
	 */
	public long getBytesSent() {
		return bytesSent;
	}
	/**
	 * @param bytesSent The bytesSent to set.
	 */
	public void setBytesSent(long bytesSent) {
		this.bytesSent = bytesSent;
	}
	/**
	 * @return Returns the capability.
	 */
	public short getCapability() {
		return capability;
	}
	/**
	 * @param capability The capability to set.
	 */
	public void setCapability(short capability) {
		this.capability = capability;
	}
	/**
	 * @return Returns the isIMCP.
	 */
	public short getIsIMCP() {
		return isIMCP;
	}
	/**
	 * @param isIMCP The isIMCP to set.
	 */
	public void setIsIMCP(short isIMCP) {
		this.isIMCP = isIMCP;
	}
	/**
	 * @return Returns the keyIndex.
	 */
	public short getKeyIndex() {
		return keyIndex;
	}
	/**
	 * @param keyIndex The keyIndex to set.
	 */
	public void setKeyIndex(short keyIndex) {
		this.keyIndex = keyIndex;
	}
	/**
	 * @return Returns the lastResponseRcvd.
	 */
	public String getLastResponseRcvd() {
		return lastResponseRcvd;
	}
	/**
	 * @param lastResponseRcvd The lastResponseRcvd to set.
	 */
	public void setLastResponseRcvd(String lastResponseRcvd) {
		this.lastResponseRcvd = lastResponseRcvd;
	}
	/**
	 * @return Returns the listenInterval.
	 */
	public int getListenInterval() {
		return listenInterval;
	}
	/**
	 * @param listenInterval The listenInterval to set.
	 */
	public void setListenInterval(int listenInterval) {
		this.listenInterval = listenInterval;
	}
	/**
	 * @return Returns the parentESSID.
	 */
	public String getParentESSID() {
		return parentESSID;
	}
	/**
	 * @param parentESSID The parentESSID to set.
	 */
	public void setParentESSID(String parentESSID) {
		this.parentESSID = parentESSID;
	}
	/**
	 * @return Returns the parentwmMacAddress.
	 */
	public MacAddress getParentwmMacAddress() {
		return parentwmMacAddress;
	}
	/**
	 * @param parentwmMacAddress The parentwmMacAddress to set.
	 */
	public void setParentwmMacAddress(MacAddress  parentwmMacAddress) {
		this.parentwmMacAddress = parentwmMacAddress;
	}
	/**
	 * @return Returns the signal.
	 */
	public long getSignal() {
		return signal;
	}
	/**
	 * @param signal The signal to set.
	 */
	public void setSignal(long signal) {
		this.signal = signal;
	}
	/**
	 * @return Returns the staMacAddress.
	 */
	public MacAddress getStaMacAddress() {
		return staMacAddress;
	}
	/**
	 * @param staMacAddress The staMacAddress to set.
	 */
	public void setStaMacAddress(MacAddress staMacAddress) {
		this.staMacAddress = staMacAddress;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IStaConfiguration#getParentMacAddress()
	 */
	public MacAddress getParentMacAddress() {
		return parentMacAddress;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IStaConfiguration#setParentMacAddress(com.meshdynamics.util.MacAddress)
	 */
	public void setParentMacAddress(MacAddress parentMacAddress) {
		this.parentMacAddress	= parentMacAddress;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IStaConfiguration#getDissocReason()
	 */
	public int getDissocReason() {
		return dissocReason;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IStaConfiguration#getDissocStaMacAddress()
	 */
	public MacAddress getDissocStaMacAddress() {
		return dissocStaMacAddress;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IStaConfiguration#setDissocReason(java.lang.String)
	 */
	public void setDissocReason(int reason) {
		this.dissocReason	= reason;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IStaConfiguration#setDissocStaMacAddress(com.meshdynamics.util.MacAddress)
	 */
	public void setDissocStaMacAddress(MacAddress staMacAddress) {
		this.dissocStaMacAddress	= staMacAddress;
	}
	
	
	public long getBitRate(){
		return dwnlinkTransmitRate;
	}

	/**
	 * @param bitRate
	 */
	public void setBitRate(long bitRate) {
		this.dwnlinkTransmitRate	= bitRate;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.interfaces.ICopyable#copyTo(com.meshdynamics.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
	
		if(destination == null) {
			return false;
		}
		
		if(destination instanceof IStaConfiguration) {
			
			((IStaConfiguration)destination).setStaMacAddress(getStaMacAddress());
			((IStaConfiguration)destination).setParentwmMacAddress(getParentwmMacAddress());
			((IStaConfiguration)destination).setParentESSID(getParentESSID());
			((IStaConfiguration)destination).setAuthState(getAuthState());
			((IStaConfiguration)destination).setBclient(getBclient());
			((IStaConfiguration)destination).setIsIMCP(getIsIMCP());
			((IStaConfiguration)destination).setListenInterval(getListenInterval());
			((IStaConfiguration)destination).setCapability(getCapability());
			((IStaConfiguration)destination).setKeyIndex(getKeyIndex());
			((IStaConfiguration)destination).setBytesReceived(getBytesReceived());
			((IStaConfiguration)destination).setBytesSent(getBytesSent());
			((IStaConfiguration)destination).setActiveTime(getActiveTime());
			((IStaConfiguration)destination).setLastResponseRcvd(getLastResponseRcvd());
			
			((IStaConfiguration)destination).setDissocReason(getDissocReason());
			((IStaConfiguration)destination).setDissocStaMacAddress(getDissocStaMacAddress());
			((IStaConfiguration)destination).setParentMacAddress(getParentMacAddress());
			((IStaConfiguration)destination).setSignal(getSignal());
			((IStaConfiguration)destination).setBitRate(getBitRate());
			((IStaConfiguration)destination).setName(getName());
			return true;
		
		}
		return false;
	}

	public String getName() {
		return nodeName;
	}

	public void setName(String nodeName) {
		this.nodeName = nodeName;
	}
}
