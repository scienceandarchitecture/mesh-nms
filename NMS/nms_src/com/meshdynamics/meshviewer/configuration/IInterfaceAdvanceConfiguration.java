package com.meshdynamics.meshviewer.configuration;

public interface IInterfaceAdvanceConfiguration extends ISerializableConfiguration, ICopyable, IComparable {
	
	public byte getChannelBandwidth();
	public void setChannelBandwidth(byte channel);
	
	public byte getSecondaryChannelPosition();
	public void setSecondaryChannelPosition(byte position);
	
	public byte getPreamble();
	public void setPreamble(byte preamble);
	
	public byte getGuardInterval_20();
	public void setGuardInterval_20(byte guardInterval_20);
	
	public byte getGuardInterval_40();
	public void setGuardInterval_40(byte guardInterval_40);
	
	public byte getGuardInterval_80();
	public void setGuardInterval_80(byte guardInterval_80);
	
	public byte getFrameAggregation();
	public void setFrameAggregation(byte frameAggregation);
	
	public byte getMaxAMPDU();
	public void setMaxAMPDU(byte maxAMPDU);
	
	public byte getMaxAMSDU();
	public void setMaxAMSDU(byte maxAMSDU);
	
	public byte getTxSTBC();
	public void setTxSTBC(byte txSTBC);
	
	public byte getRxSTBC();
	public void setRxSTBC(byte rxSTBC);
	
	public byte getLDPC();
	public void setLDPC(byte ldpc);
	
	public byte getGFMode();
	public void setGFMode(byte gfMode);
	
	public byte getCoexistence();
	public void setCoexistence(byte coexistence);
	
	public short getMaxMPDU();
	public void setMaxMPDU(short maxMPDU);
	
	public byte getMaxAMPDUEnabled();
	public void setMaxAMPDUEnabled(byte maxAMPDUEnabled);
	
	public String getProtocalName();
	public void setProtocalName(String protocol);
	
	public int getProtocolID();
	public void setProtocolID(int protocolid);

}
