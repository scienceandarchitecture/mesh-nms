package com.meshdynamics.meshviewer.configuration;

public interface ICopyable {
	public boolean copyTo(ICopyable destination);
}
