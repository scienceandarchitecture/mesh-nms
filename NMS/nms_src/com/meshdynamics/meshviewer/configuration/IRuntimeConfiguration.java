/*
 * Created on Jun 5, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.configuration;

import java.util.Enumeration;

import com.meshdynamics.meshviewer.configuration.KnownAp.KnownApInfo;
import com.meshdynamics.util.MacAddress;

/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IRuntimeConfiguration {

	public 		MacAddress 						getParentBssid();
	public 		MacAddress 						getParentDSMacAddress();
	public 		MacAddress						getRootBssid() ;
	
	public 		double 							getBoardTemperature();
	public 		long 							getBitRate();
	public 		long 							getParentSignal();
	public 		long 							getHeartbeat1SequenceNumber();
	public 		long 							getHeartbeat2SequenceNumber() ;
	public 		long 							getConfigSeqNumber();
	public 		long 							getDwnlinkTransmitRate();
	public 		long 							getLastHeartbeatTime();
	public 		long 							getRxBytes();
	public 		long 							getRxPackets() ;
	public 		long 							getTxBytes();
	public 		long 							getTxPackets();
	public 		long 							getUplinkTransmitRate();
	
	public 		boolean 						isRebootRequired();
	public 		boolean 						isCapable(int packetType);
	public 		boolean 						isCmdCapable(int comType);
	public 		boolean 						isConfigCapable(int comType);
	public 		boolean 						isRadarDetected();
	public 		boolean 						hasClient(MacAddress staAddress);
	public 		boolean 						is1stHeartbeat();
	public 		boolean 						isFIPSDetected(); 
	
	public 		int 							getVoltage();
	public 		int 							getBoardCPUUsage();
	public 		int 							getBoardFreeMemory();
	public 		short 							getHopCount();
	public 		short 							getHeartbeatInterval();
	
	public 		int 							getKnownApCount() ;
	public 		Enumeration<KnownAp> 			getKnownAPs();
	public 		KnownAp 						getKnownAp(MacAddress kapMacAddress) ;
	
	public 		short 							getStaCount();
	public 		Enumeration<IStaConfiguration> 	getStaList() ;
	public 		IStaConfiguration				getStaInformation(MacAddress staAddress) ;
	
	public 		void 							clearData();
	public 		Enumeration<KnownApInfo> 		getParentInfo();
	
	public 		String 							getAltitude();
	public 		String 							getLatitude();
	public 		String 							getLongitude();
	public 		String 							getSpeed();
	
}
