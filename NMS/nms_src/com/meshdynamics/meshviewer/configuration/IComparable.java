package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;

public interface IComparable {
	
	public static final short	CONFIG_CHANGED		=	0;
	public static final short	CONFIG_NOT_CHANGED	=	1;
	public static final short	CONFIG_NULL			=	2;
	
	public short compare(IComparable destination, IConfigStatusHandler statusHandler);
	
	
}
