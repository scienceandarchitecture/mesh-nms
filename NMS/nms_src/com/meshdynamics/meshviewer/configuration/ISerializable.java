package com.meshdynamics.meshviewer.configuration;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public interface ISerializable {

	public boolean 		load(InputStreamReader inputStream);
	public boolean 		save(OutputStreamWriter outputStream);
	
}
