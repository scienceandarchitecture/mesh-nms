package com.meshdynamics.meshviewer.configuration;

public interface ISecurityConfiguration extends ISerializableConfiguration, ICopyable, IComparable{

    public static final int	SECURITY_TYPE_NONE				= 1;
	public static final int	SECURITY_TYPE_WEP				= 2;
	public static final int	SECURITY_TYPE_WPA_PERSONAL		= 4;
	public static final int	SECURITY_TYPE_WPA_ENTERPRISE	= 8;
	
	public int 							getEnabledSecurity();
	public void 						setEnabledSecurity(int securityType);
	
	public IWEPConfiguration	 		getWEPConfiguration();
	public void					 		setWEPConfiguration(IWEPConfiguration config);
	
	public IPSKConfiguration			getPSKConfiguration();
	public void							setPSKConfiguration(IPSKConfiguration config);
	
	public IRadiusConfiguration			getRadiusConfiguration();
	public void 						setRadiusConfiguration(IRadiusConfiguration config);
	
}
