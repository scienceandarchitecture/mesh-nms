/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : KnownAp.java
 * Comments : 
 * Created  : Apr 17, 2007
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Apr 17, 2007  | Created                                        | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.configuration;

import java.util.Enumeration;
import java.util.Vector;

import com.meshdynamics.util.MacAddress;

public class KnownAp {
	
    public MacAddress getMacAddress() {
		return macAddress;
	}
	public MacAddress 			macAddress;
    public Vector<KnownApInfo>	knownApInfo;
    public String				nodeName;
    
    public static class KnownApInfo {
        public short			signal;
        public short			bitRate;
 
        public KnownApInfo() {
            signal		= 0;
            bitRate		= 0;
        }
    }
    
    public KnownAp() {
        macAddress  	= null;
        knownApInfo		= new Vector<KnownApInfo>();
        nodeName		= "";
    }
    
    public void addInfo(short signal,short bitRate) {
    	KnownApInfo info 	= new KnownApInfo();
    	info.signal 		= signal;
    	info.bitRate		= bitRate;
    	knownApInfo.add(info);
    }
    
    public Enumeration<KnownApInfo> getInfo() {
    	return knownApInfo.elements();
    }
}
