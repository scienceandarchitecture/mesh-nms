/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamConfiguration.java
 * Comments : 
 * Created  : Jul 09, 2007
 * Author   : Imran Khan        
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author   |
 * ----------------------------------------------------------------------------------
 * |  0  |jul 10,2007   | Created                                         | Imran    |
 * ----------------------------------------------------------------------------------*/
 
package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;
import com.meshdynamics.util.Range;

public interface IEffistreamRuleCriteria extends IComparable, ICopyable, ISerializableConfiguration{
		
	public MacAddress	getMacAddressValue();
	public void 		setMacAddressValue(byte[] macAddrBytes);
	
	public IpAddress	getIPAddressValue();
	public void 		setIPAddressValue(byte[] ipAddrBytes);
	
	public Long			getLongValue();
	public void 		setLongValue(long longValue);
	
	public Range		getRangeValue();
	public void 		setRangeValue(long lowerLimit, long upperLimit);
	
	public int 			getCriteria();
	public void 		setCriteriaId(short criteriaId);
	
}
