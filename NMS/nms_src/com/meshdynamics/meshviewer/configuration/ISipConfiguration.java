/*
 * Created on Mar 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.util.IpAddress;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ISipConfiguration extends ISerializableConfiguration, ICopyable, IComparable {
	
	public void 		setServerIpAddress(byte[] ipAddressBytes);
	public IpAddress	getServerIpAddress();
	
	public void 		setPort(int port);
	public int 			getPort();
	
	public void 		setEnabled(short enabled);
	public short 		getEnabled();

	public void 		setEnabledInP3M(short enabled);
	public short 		getEnabledInP3M();
	
	public short 		getStaInfoCount();
	public void 		setStaInfoCount(short newCount);
	
	public ISipStaInfo	getStaInfoByIndex(int index);

}
