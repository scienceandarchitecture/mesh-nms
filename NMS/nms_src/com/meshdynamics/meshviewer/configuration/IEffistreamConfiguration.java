/*
 * Created on Jul 2, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.configuration;

public interface IEffistreamConfiguration extends ISerializableConfiguration, ICopyable, IComparable{
	public IEffistreamRule		getRulesRoot();
}
