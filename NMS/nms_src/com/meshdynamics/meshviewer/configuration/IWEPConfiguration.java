package com.meshdynamics.meshviewer.configuration;

public interface IWEPConfiguration extends ISerializableConfiguration, ICopyable, IComparable{

    public static final short	 KEY_STRENGTH_64_BIT	= 40;
	public static final short	 KEY_STRENGTH_128_BIT	= 104;
	
	public short[]		getKey(int keyIndex);	
	public void 		setKey(int keyIndex,short[] key0);
	
	public short 		getKeyStrength();
	public void 		setKeyStrength(short strength); 
	
	public void			setPassPhrase(String passphrase);
	public String		getPassPhrase();
	
	public void			setKeyIndex(short keyIndex);
	public short		getKeyIndex();
	
}
