/*
 * Created on May 18, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.util.MacAddress;


/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IStaConfiguration {

	
	public long 		getActiveTime();
	public short 		getAuthState() ;
	public short 		getBclient() ;
	public long 		getBytesReceived() ;
	public long 		getBytesSent();
	public short 		getCapability() ;
	public short 		getIsIMCP();
	public short 		getKeyIndex();
	public String 		getLastResponseRcvd() ;
	public int 			getListenInterval() ;
	public String 		getParentESSID() ;
	public MacAddress	getParentwmMacAddress();
	public MacAddress	getStaMacAddress();
	public MacAddress	getParentMacAddress();
						
	public void 		setActiveTime(long activeTime); 
	public void 		setAuthState(short authState);
	public void 		setBclient(short bclient);
	public void 		setBytesReceived(long bytesReceived);
	public void 		setBytesSent(long bytesSent);
	public void 		setCapability(short capability);
	public void 		setIsIMCP(short isIMCP);
	public void 		setKeyIndex(short keyIndex) ;
	public void			setLastResponseRcvd(String lastResponseRcvd) ;
	public void 		setListenInterval(int listenInterval) ;
	public void 		setParentESSID(String parentESSID) ;
	public void 		setParentwmMacAddress(MacAddress parentwmMacAddress) ;
	public void 		setStaMacAddress(MacAddress staMacAddress);
	public void 		setParentMacAddress(MacAddress parentMacAddress);
	
	public long 		getSignal() ;
	public void 		setSignal(long signal) ;
	
	public int			getDissocReason();
	public MacAddress	getDissocStaMacAddress();
	public void 		setDissocReason(int reason);
	public void 		setDissocStaMacAddress(MacAddress staMacAddress);

	public void 		setAssociationTime(String time);
	public String		getAssociationTime();
	
	public long			getBitRate();
	public void 		setBitRate(long bitRate);
	
	public String		getName();
	public void			setName(String nodeName);

}
