package com.meshdynamics.meshviewer.configuration;

public interface IPSKConfiguration extends ISerializableConfiguration, ICopyable, IComparable{
	
	public static final short WPA_PERSONAL_MODE_WPA		= 0;
	public static final short WPA_PERSONAL_MODE_WPA2	= 1;
	
	public short 		getPSKCipherCCMP();
	public void 		setPSKCipherCCMP(short psk);
	
	public short 		getPSKCipherTKIP();
	public void 		setPSKCipherTKIP(short psk);
			
	public int 			getPSKGroupKeyRenewal();
	public void 		setPSKGroupKeyRenewal(int grpKeyRenew);
		
	public short[] 		getPSKKey();
	public void 		setPSKKey(short[] key);
	
	public short 		getPSKMode(); 
	public void 		setPSKMode(short mode); 

	public int 			getPSKKeyLength();
	public void 		setPSKKeyLength(int keyLength); 

	public void			setPSKPassPhrase(String passphrase);
	public String		getPSKPassPhrase();
	
}
