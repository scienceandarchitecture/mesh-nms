package com.meshdynamics.meshviewer.configuration;

public interface IMeshConfiguration extends ISerializableConfiguration, ICopyable, IComparable {

    public void 		setHeartbeatInterval(short hbi);
	public short        getHeartbeatInterval();

	/*
	 * Following mobility functions reuse
	 * fragThreshold,lasi variables
	 */

	public final static int MOBILITY_MODE_STATIONARY 	= 0;
	public final static int MOBILITY_MODE_MOBILE 		= 1;
	

	public final static int MOBILITY_DIST_FEET 			= 0;
	public final static int MOBILITY_DIST_METERS 		= 1;
	public final static int MOBILITY_DIST_MILES 		= 2;

	public final static int MOBILITY_SPEED_MPH 			= 0;
	public final static int MOBILITY_SPEED_KMPH 		= 1;	
	
	/***
	 * Frag Threshold = SpeedUnit (2 bits) + Dist Unit(2 bits) + Speed(12 bits) + Distance(16 bits)
	 * Max Mobility Distance	= 65535 (16 bits) 
	 * Max Mobility Speed		= 4095  (12 bits)	
	 */
	
	public static final int    FRAG_THRESHOLD					= 2346;
	
	public static final double MAX_MOBILITY_DISTANCE_IN_FEET 	= 346024800.0;
	public static final double MAX_MOBILITY_DISTANCE_IN_METERS 	= 105468359.04;
	public static final double MAX_MOBILITY_DISTANCE_IN_MILES 	= 65535;
	
	public static final double MAX_MOBILITY_SPEED_IN_MPH 		= 4095;
	public static final double MAX_MOBILITY_SPEED_IN_KMPH 		= 6590.26368;

	public void 		setMobilityMode(short mode);
	public short 		getMobilityMode();
	
	public void 		setMobilityIndex(short index);
	public short 		getMobilityIndex();
	
	public double	 	getDeployDistance();

	public byte 		getDeployDistanceUnit();

	public short 		getSpeed();

	public byte 		getSpeedUnit();
	
	/*
	 * Following functions use hopCost field 
	 */
	
	public boolean 		getP3MMode();
	public void			setP3MMode(boolean set);
	
	public boolean 		getP3MModeInfraStartup();
	public void			setP3MModeInfraStartup(boolean set);
 
	public boolean 		getP3MModeSectoredUse();
	public void			setP3MModeSectoredUse(boolean set);
	
	/*
	 * DHCP has to be enabled using alconfset
	 */
	public boolean		getDHCPEnabled();
	
	public boolean		getIGMPEnabled();
	public void 		setIGMPEnabled(boolean enabled);

	/*
	 * PBV is enabled depending on configured options
	 * options can be changed only through alconfset
	 */
	public boolean		getPBVEnabled();
	
	/*
	 *  Following fields are not exposed and most of them have default values
	 */
	public void 		setSignalMap(short[] signalMap);
	public short[] 		getSignalMap();
	
	public void 		setHeartbeatMissCount(short hbmc);
	public short        getHeartbeatMissCount();
 
	public void			setHopCost(short hopCost);
	public short		getHopCost();
	
	public void 		setMaxAllowableHops(short mah);
	public short        getMaxAllowableHops();
	
	public void 		setLocationAwarenessScanInterval(short lasi);
	public short        getLocationAwarenessScanInterval();
	
	public void 		setChangeResistanceThreshold(short value);
	public short 		getChangeResistanceThreshold();

	public void 		setEssid(String essid);
	public String 		getEssid();

	public void 		setRTSThreshold(long value);
	public long 		getRTSThreshold();

	
	/**
	 * Frag threshold value is dependent on speed, distance,
	 * speed unit, distance unit.
	 * Frag threshold value is calculated using speed, distance,
	 * speed unit, distance unit.
	 */
	
	public void 		setFragThreshold(long value);
	public long 		getFragThreshold();

	public void 		setBeaconInterval(long value);
	public long 		getBeaconInterval();

	public void 		setDynamicChannelAllocation(short set);
	public short 		getDynamicChannelAllocation();

	public void 		setStayAwakeCount(short count);
	public short 		getStayAwakeCount();
	
	public void 		setBridgeAgeingTime(short bat);
	public short 		getBridgeAgeingTime();
	
	public void 		setFCCCertifiedOperation(short fcc);
	public short 		getFCCCertifiedOperation();
	
	public void 		setETSICertifiedOperation(short etsi);
	public short 		getETSICertifiedOperation();
	
	public void 		setDSTxRate(long rate);
	public long 		getDSTxRate();
	
	public void 		setDSTxPower(short txPower);
	public short 		getDSTxPower();
	
	/*
	 * End of unexposed values
	 */
		
}

