package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.util.MacAddress;


public interface IACLConfiguration extends ISerializableConfiguration, ICopyable, IComparable {

    public void		 		setCount(int count);
	public int				getCount();
	
	public IACLInfo		getACLInfo(int index);
	public IACLInfo		getACLInfo(IACLInfo	aclInfo);
	
	public IACLInfo		addAclInfo(MacAddress staAddress);
	
	/*
	 * find info that matches with the information in matchACLInfo
	 * and delete it
	 */
	public void         deleteAclInfo					(IACLInfo matchACLInfo);
	public boolean 		containsAclEntry				(IACLInfo aclInfo);
	public boolean		containsAclEntryWithStaAddr 	(MacAddress	staAddr);
	public IACLInfo     aclEntryWithStaAddr             (MacAddress staAddr);
	
}
