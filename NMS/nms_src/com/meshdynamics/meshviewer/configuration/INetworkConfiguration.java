package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.util.IpAddress;

public interface INetworkConfiguration extends ISerializableConfiguration, ICopyable, IComparable{
	
	public void	 		setHostName(String name);
	public String 		getHostName();
	
	public void 		setIpAddress(IpAddress ipAddress);
	public IpAddress	getIpAddress();
	
	public void			setSubnetMask(IpAddress subnet);
	public IpAddress	getSubnetMask();
	
	public void 		setGateway(IpAddress gateway);
	public IpAddress	getGateway();
	
}
