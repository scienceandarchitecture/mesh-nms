package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.util.MacAddress;

public interface IConfiguration extends ISerializableConfiguration,ICopyable,IComparable {

    public MacAddress				getDSMacAddress();
	
	public String 					getFirmwareVersion();

	public void 					setFirmwareMajorVersion(short firmwareMajorVersion);
	public short 					getFirmwareMajorVersion();
		
	public void 					setFirmwareMinorVersion(short firmwareMinorVersion);
	public short 					getFirmwareMinorVersion();
	
	public void 					setFirmwareVariantVersion(short firmwareVariantVersion);
	public short 					getFirmwareVariantVersion();
		
	public void 					setHardwareModel(String hardwareModel);
	public String      	 			getHardwareModel();
		
	public void 					setNodeName(String name);
	public String 					getNodeName();

	public void 					setDescription(String desc);
	public String 					getDescription();
	
	public void 					setLatitude(String lat);
	public String					getLatitude();
		
	public void 					setLongitude(String longt);
	public String 					getLongitude();
	
	public void						setPreferedParent(String prefParent);
	public String					getPreferedParent();
	
	public int    					getRegulatoryDomain();
	public void 					setRegulatoryDomain(int regDomain);
	
	public boolean					isFipsEnabled();
	public void 					setFipsEnabled();
	
	public int    					getCountryCode();
	public void 					setCountryCode(int countryCode);
	
	public void 					setCountryCodeChanged(boolean changed);
	public boolean 					isCountryCodeChanged();
	
    public void 					setMacAddress(MacAddress macAddress);
    
    public INetworkConfiguration 	getNetworkConfiguration();
	public IMeshConfiguration		getMeshConfiguration();
	public IInterfaceConfiguration	getInterfaceConfiguration();
	public IVlanConfiguration		getVlanConfiguration();
	public IACLConfiguration		getACLConfiguration();
	public I80211eCategoryConfiguration
									get80211eCategoryConfiguration();
	public IEffistreamConfiguration getEffistreamConfiguration();
	public ISipConfiguration 		getSipConfiguration();

}
