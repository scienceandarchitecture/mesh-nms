package com.meshdynamics.meshviewer.configuration;


public interface I80211eCategoryConfiguration extends ISerializableConfiguration, ICopyable, IComparable {
	
    public void						setCount(int count);
	public int						getCount();
	
	public I80211eCategoryInfo 	getCategoryInfo(int index);
	public I80211eCategoryInfo	getCategoryInfoByName(String categoryName);
	
}
