package com.meshdynamics.meshviewer.configuration;


public interface IInterfaceConfiguration extends ISerializableConfiguration, ICopyable, IComparable{

    public void 			setInterfaceCount(int count);
	public int 				getInterfaceCount();
	
	public IInterfaceInfo	getInterfaceByIndex(int index);
	public IInterfaceInfo	getInterfaceByName(String ifName);
	
	/*
	 * Add new interface if not already present
	 */
	public IInterfaceInfo	addInterfaceByName(String ifName);
	
}
