package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;

public interface ISerializableConfiguration{

	public boolean 		load(ConfigurationReader configReader);
	public boolean 		save(ConfigurationWriter configWriter);
	public void			clearData();
}
