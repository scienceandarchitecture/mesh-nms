/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : IEffistreamAction.java
 * Comments : 
 * Created  : Jul 4, 2007
 * Author   : Abhishek
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  | Jul 4, 2007 | Created                                         |Abhishek|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.configuration;

public interface IEffistreamAction extends IComparable, ICopyable, ISerializableConfiguration{

	public static final short 	AC_NN		= 0xFF;
	public static final short 	AC_BK		= 0;
	public static final short 	AC_BE		= 1;	
	public static final short 	AC_VI		= 2;	
	public static final short 	AC_VO		= 3;
	
	public static final int 	ACTION_TYPE_DEFAULT 		= 0;
	public static final int 	ACTION_TYPE_802_11_CATEGORY = 1;
	public static final int 	ACTION_TYPE_DROP_PACKET 	= 2;
	public static final int 	ACTION_TYPE_NO_ACK			= 3;
	public static final int 	ACTION_TYPE_BIT_RATE		= 4;
	
	
	public short 		getDot11eCategory();
	public void 		setDot11eCategory(short dot11eCategory);
	
	public short 		getDropPacket();
	public void 		setDropPacket(short dropPacket);
	
	public short 		getNoAck();
	public void 		setNoAck(short noAck);
	
	public short		getBitRate();
	public void 		setBitRate(short bitRate);
	public int 			getActionType		();
	
	public short 		getQueuedRetry();
    public void 		setQueuedRetry(short queuedRetry);
	
}
