/*
 * Created on May 18, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.util.MacAddress;

/**
 * @author imran
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ISaturationInfo extends ISerializableConfiguration, ICopyable, IComparable{
		
    public String 	getIfName(); 
	public void 	setIfName(String ifName); 
	
	public int  	getChannel(int index); 
	public void 	setChannel(int channel, int chIndex); 
	
	public long 	getChannelTxDuration(int channel);
	public void 	setChannelTxDuration(long txDuration,int chIndex);
	
	public long 	getChannelTotalDuration(int channel);
	public void 	setChannelTotalDuration(long totalDuration, int chIndex);
	
	public long 	getChannelRetryDuration(int channel) ;
	public void 	setChannelRetryDuration(long retryDuration,  int chIndex) ;
	
	public int 		getChannelAPCount(int channel) ;
	public void 	setChannelAPCount(short apCount, int chIndex);
	
	public short 	getChannelAvgSignal(int channel);
	public void  	setChannelAvgSignal(short avgSignal, int chIndex) ;
	
	public short 	getChannelMaxSignal(int channel); 
	public void  	setChannelMaxSignal(short maxSignal, int chIndex); 
	
	public MacAddress getChannelAPBssid(int channelIndex, int apIndex); 
	public void 	setChannelAPBssid(int channelIndex, int apIndex, MacAddress apBssId); 
	
	public String 	getChannelAPEssid(int channelIndex, int apIndex); 
	public void 	setChannelAPEssid(int channelIndex, int apIndex, String apEssId); 
	
	public short 	getChannelAPSignal(int channelIndex, int apIndex); 
	public void 	setChannelAPSignal(int channelIndex, int apIndex,short apSignal); 
	
	public long 	getChannelAPTxDuration(int channelIndex, int apIndex); 
	public void 	setChannelAPTxDuration(int channelIndex, int apIndex, long txDuration); 
	
	public long 	getChannelAPRetryDuration(int channelIndex, int apIndex); 
	public void 	setChannelAPRetryDuration(int channelIndex, int apIndex, long apRetryDuration);
	
	public int 		getChannelCount();
	public void 	setChannelCount(int chCount);
	
	public String 	getLastUpdateTime();
	public void 	setLastUpdateTime(String lastUpdateTime);
	
	public void 	setSaturationInfoReceived();
	public boolean 	isAvailable();
	
}
