/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamConfigPage.java
 * Comments : 
 * Created  : Jul 4, 2007
 * Author   : Abhishek
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  | Jul 4, 2007 | Created                                         |Abhishek|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.configuration;

import java.util.Enumeration;

public interface IEffistreamRule extends IComparable, ICopyable, ISerializableConfiguration {

	public IEffistreamRule				createRule			(short criteriaId);
	public boolean						addRule		    	(IEffistreamRule rule);
	public boolean						deleteRule			(String strIdentifier);
	
	/*
	 * action will be initialized in setAction according to criteria id
	 */
	public IEffistreamAction			addAction				(int actionType);
	public void							deleteAction			();
	public IEffistreamAction			getAction				();
	
	public IEffistreamRuleCriteria		getCriteriaValue		();
	public short	 					getCriteriaId			();
	
	public IEffistreamRule				getParent				();
	public short						getChildCount			();
	public Enumeration<String>			getChildrenIdentifiers	();
	public IEffistreamRule				getChild				(String srcIdentifier);
	
	public boolean						isRoot					();
	public void 						setRoot					(boolean enable);
	
}
