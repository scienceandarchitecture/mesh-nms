package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.util.IpAddress;

public interface IVlanInfo extends ISerializableConfiguration, ICopyable, IComparable {

    public void						setName(String name);
	public String 					getName();

	public void 					setTag(int tag);
	public int 						getTag(); 

	public void 					setIpAddress(IpAddress ip);
	public IpAddress				getIpAddress();
	
	public void 					setEssid(String essid);
	public String					getEssid();
	
	public void 					setBeaconInterval(long becaonInterval);
	public long						getBeaconInterval();	
	
	public void 					setTxPower(short val);
	public short					getTxPower();
	
	public void						setTxRate(long val);
	public	long					getTxRate();
		
	public void						setRtsThreshold(long val);
	public long						getRtsThreshold();
	
	public void						setFragThreshold(long val);
	public long						getfragThreshold();
	
	public void						set8021pPriority(short val);
	public short					get8021pPriority();
		
	public void						set80211eCategoryIndex(short val);
	public short					get80211eCategoryIndex();
	
	public void						set80211eEnabled(short val);
	public short					get80211eEnabled();
	
	public void						setServiceType(short val);
	public short					getServiceType();

	public void 					setSecurityConfiguration(ISecurityConfiguration securityConfig);
	public ISecurityConfiguration	getSecurityConfiguration();
	
}
