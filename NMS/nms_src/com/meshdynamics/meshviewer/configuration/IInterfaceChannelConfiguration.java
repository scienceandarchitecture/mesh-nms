package com.meshdynamics.meshviewer.configuration;

public interface IInterfaceChannelConfiguration extends ISerializableConfiguration, 
												ICopyable, IComparable {

    public short 			getChannelBandwidth();
	public void 			setChannelBandwidth(short bandWidth);

	public short 			getCtl();
	public void 			setCtl(short ctl);
	
	public short 			getAntennaPower();
	public void 			setAntennaPower(short antPower);
	
	public short 			getAntennaGain();
	public void 			setAntennaGain(short antGain);
	
	public void 			setChannelInfoCount(int count);
	public int 				getChannelInfoCount();
	
	public IChannelInfo		getChannelInfo(int index);
	public IChannelInfo		getChannelInfoByChNumber(int chNumber);
	
}
