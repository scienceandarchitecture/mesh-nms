/*
 * Created on Mar 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.util.MacAddress;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ISipStaInfo extends ISerializableConfiguration, ICopyable, IComparable {

	public void 		setMacAddress(byte[] macAddressBytes);
	public MacAddress 	getMacAddress();
	
	public void 		setExtension(short extension);
	public short		getExtension();
	
}
