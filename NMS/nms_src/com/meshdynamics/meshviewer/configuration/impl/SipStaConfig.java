/*
 * Created on Mar 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.ISipStaInfo;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.MacAddress;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SipStaConfig implements ISipStaInfo {

	private MacAddress	macAddress;
	private short		extn;
	
	/**
	 * 
	 */
	public SipStaConfig() {
		super();
		macAddress 	= new MacAddress();
		extn		= 0;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipStaInfo#setMacAddress(byte[])
	 */
	public void setMacAddress(byte[] macAddressBytes) {
		this.macAddress.setBytes(macAddressBytes);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipStaInfo#getMacAddress()
	 */
	public MacAddress getMacAddress() {
		return this.macAddress;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipStaInfo#setExtension(short)
	 */
	public void setExtension(short extension) {
		this.extn = extension;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipStaInfo#getExtension()
	 */
	public short getExtension() {
		return this.extn;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		try {
			
			while (configReader.getNextToken() == true) {
			
				int currentTokenType = configReader.getCurrentTokenType();
				
				if(currentTokenType == IConfigurationIODefs.TOKEN_TYPE_HEADER) {
					
					if(configReader.getCurrentHeader() == IConfigurationIODefs.CONFIG_HEADER_SIP_STA_INFO_END)
						return true;
					
				} else if(currentTokenType == IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE) {
	
					String	tokenValue 	= configReader.getCurrentTokenValue();
					int		tokenCode	= configReader.getCurrentToken();
					
					if(tokenCode == IConfigurationIODefs.CONFIG_KEY_SIP_STA_INFO_EXTN) {
						try{
							extn = Short.parseShort(tokenValue.trim());
						}catch(Exception e){
							System.out.println(e);
							return false;
						}
					} else if(tokenCode == IConfigurationIODefs.CONFIG_KEY_SIP_STA_INFO_MAC_ADDR) {
						try{
							macAddress.setBytes(tokenValue.trim());
						}catch(Exception e){
							System.out.println(e);
							return false;
						}
					}
					
				}
			}
		} catch(Exception e){
			System.out.println(e);
			return false;
		}
		return false;

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {

		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_SIP_STA_INFO_START);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SIP_STA_INFO_EXTN, ""+extn);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SIP_STA_INFO_MAC_ADDR, macAddress.toString());
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_SIP_STA_INFO_END);
		return true;

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		macAddress.setBytes(MacAddress.resetAddress.getBytes());
		extn = 0;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		ISipStaInfo	dstStaInfo;
		
		if(destination == null)
			return false;
		
		if((destination instanceof ISipStaInfo) == false)
			return false;

		dstStaInfo = (ISipStaInfo)destination;
		
		dstStaInfo.setExtension(this.getExtension());
		dstStaInfo.setMacAddress(this.macAddress.getBytes());
		return true;

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.IComparable#compare(com.meshdynamics.meshviewer.util.interfaces.IComparable, com.meshdynamics.meshviewer.mesh.IConfigStatusHandler)
	 */
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {

		if(destination == null)
			return IComparable.CONFIG_NULL;
		
		ISipStaInfo dstSipStaInfo	= (ISipStaInfo)destination;
		
		if(this.macAddress.equals(dstSipStaInfo.getMacAddress()) == false ||
			this.extn != dstSipStaInfo.getExtension()) {
			statusHandler.packetChanged(PacketFactory.SIP_CONFIG_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		
		return IComparable.CONFIG_NOT_CHANGED;
	}

}
