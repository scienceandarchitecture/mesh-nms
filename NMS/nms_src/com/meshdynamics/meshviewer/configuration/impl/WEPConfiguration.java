/**
 * MeshDynamics 
 * -------------- 
 * File     : WEPConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/
 
package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IWEPConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.HexHelper;



public class WEPConfiguration implements IWEPConfiguration {

	private static final int NO_OF_KEYS		=	4;
	
	private short[][]	key;
	private short		keyIndex;
	private short 		keyStrength;
	
	private String 		passphrase;
	
	public WEPConfiguration(){
		init();
	}
	
	private void init() {
		
		keyIndex	= 0;
		keyStrength = 0;
		passphrase  = "";
		key			= new short[NO_OF_KEYS][1];
		
	}
	
	public short[] getKey(int keyIndex) {
		return key[keyIndex];
	}

	public short getKeyIndex() {
		return keyIndex;
	}

	public short getKeyStrength() {
		return keyStrength;
	}

	public String getPassPhrase() {
		return passphrase;
	}

	public void setKey(int keyIndex, short[] key0) {
		key[keyIndex]= key0;
	}

	public void setKeyIndex(short keyIndex) {
		this.keyIndex = keyIndex;
	}

	public void setKeyStrength(short strength) {
		this.keyStrength = strength;
	}

	public void setPassPhrase(String passphrase) {
		this.passphrase = passphrase;
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		
		while (configReader.getNextToken() == true){
			
			int currentTokenType = configReader.getCurrentTokenType();
			
			if(currentTokenType == IConfigurationIODefs.TOKEN_TYPE_HEADER){
				
				if(configReader.getCurrentHeader() == IConfigurationIODefs.CONFIG_HEADER_WEP_INFO_END){
					return true;
				}
				
			}else if(currentTokenType == IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
			
				String tokenValue =	configReader.getCurrentTokenValue();
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_KEY_INDEX){
					try{
						short wepIndex = Short.parseShort(tokenValue.trim());
						setKeyIndex(wepIndex);
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_KEY1){
					short [] wepKey = HexHelper.stringToHex(tokenValue.trim());
					setKey(0,wepKey);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_KEY2){
					short [] wepKey = HexHelper.stringToHex(tokenValue.trim());
					setKey(1,wepKey);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_KEY3){
					short [] wepKey = HexHelper.stringToHex(tokenValue.trim());
					setKey(2,wepKey);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_KEY4){
					short [] wepKey = HexHelper.stringToHex(tokenValue.trim());
					setKey(3,wepKey);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_PASSPHRASE){
					setPassPhrase(tokenValue);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_STRENGTH){
					try{
						short strength = Short.parseShort(tokenValue.trim());
						setKeyStrength(strength); 
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {
	
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_WEP_INFO_START);

		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_KEY_INDEX, Short.toString(getKeyIndex()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_KEY1, HexHelper.toHex(getKey(0)));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_KEY2, HexHelper.toHex(getKey(1)));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_KEY3, HexHelper.toHex(getKey(2)));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_KEY4, HexHelper.toHex(getKey(3)));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_PASSPHRASE, getPassPhrase());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WEP_STRENGTH, Short.toString(getKeyStrength()));
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_WEP_INFO_END);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof IWEPConfiguration){
			
			((IWEPConfiguration)destination).setKeyIndex(getKeyIndex());
			((IWEPConfiguration)destination).setKeyStrength(getKeyStrength());
			((IWEPConfiguration)destination).setPassPhrase(getPassPhrase());
			
			for(int i=0; i<NO_OF_KEYS; i++){
				((IWEPConfiguration)destination).setKey(i,getKey(i));
			}
			return true;
		}
		return false;
	}
	
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		IWEPConfiguration	iWepConfigDest	=(IWEPConfiguration)destination;
		
		if(iWepConfigDest.getKeyIndex() != getKeyIndex()){
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;
		}
		if(HexHelper.compareArray(iWepConfigDest.getKey(0), getKey(0)) == false){
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;
		}
		if(HexHelper.compareArray(iWepConfigDest.getKey(1), getKey(1)) == false){
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;
		}
		if(HexHelper.compareArray(iWepConfigDest.getKey(2), getKey(2)) == false){
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;
		}
		if(HexHelper.compareArray(iWepConfigDest.getKey(3), getKey(3)) == false){
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;
		}
		
		if((iWepConfigDest.getPassPhrase().equals(getPassPhrase())) == false){
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;
		}
		if(iWepConfigDest.getKeyStrength() != getKeyStrength()){
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;
		}
		return IComparable.CONFIG_NOT_CHANGED;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
	}
	
	
}
