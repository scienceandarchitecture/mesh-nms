/**
 * M  eshDynamics 
 * -------------- 
 * File     : InterfaceInfo.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 8  | May 24, 2007   | saturationInfo added            			  | Imran        |
 * ----------------------------------------------------------------------------------
 * | 7  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 6  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 5  | Apr 26, 2007   | 802.11e fields added           			  | Abhijit      |
 * ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 3  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/
package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IInterfaceAdvanceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceChannelConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.ISaturationInfo;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.MacAddress;

public class InterfaceInfo implements IInterfaceInfo {

	private MacAddress							macAddress;
	private short								ackTimeout;
	private long								beaconInterval;
	private short								channel;
	private	short								bondingType;
	private short[]								dcaList;
	private short 								dcaListCount;
	private	short								dcaEnabled;
	private String								essid;
	private short								essidHidden;
	private long								fragThreshold;
	private short								mediumSubType;
	private short								mediumType;
	private String								name;
	private long								rtsThreshold;
	private short								serviceType;
	private short								txPower;
	private long								txRate;
	private short								usageType;
	private short								operatingChannel;
	private short 								dot11eEnabled;
	private short 								dot11eCategory;	
	//new field
	private short                               supportedProtocol;
	private short								appCtrl;

	private InterfaceSupportedChannelConfig		supportedChannelConfig;
	private InterfaceCustomChannelConfig		customChannelConfig;
	private ISecurityConfiguration				securityConfig;
	private ISaturationInfo						saturationInfo;
	//new entry
	private InterfaceAdvanceConfiguration       interfaceAdvanceConfiguration;
	
	

	public InterfaceInfo() {
	
		init();
		
		securityConfig			      = new SecurityConfiguration();
		supportedChannelConfig        = new InterfaceSupportedChannelConfig();
		customChannelConfig		      = new InterfaceCustomChannelConfig();
		saturationInfo			      = new SaturationConfiguration();
		interfaceAdvanceConfiguration = new InterfaceAdvanceConfiguration();
	
	}
	
	private void init() {
		
		macAddress		= new MacAddress();
		ackTimeout		= 0;
		beaconInterval	= 0;
		channel			= 0;
		bondingType		= 0;
		dcaList			= null;
		dcaListCount	= 0;
		dcaEnabled		= 0;
		essid			= "";
		essidHidden		= 0;
		fragThreshold	= 0;
		mediumSubType	= 0;
		mediumType		= 0;
		name 			= "";
		rtsThreshold	= 0;
		serviceType		= 0;
		txPower			= 0;
		txRate			= 0;
		usageType		= 0;
		supportedProtocol = 0;
		appCtrl		= 0;
	}
	public short getAckTimeout() {
		return ackTimeout;
	}

	public long getBeaconInterval() {
		return beaconInterval;
	}

	public short getChannel() {
		return channel;
	}

	public short getChannelBondingType() {
		return bondingType;
	}

	public IInterfaceChannelConfiguration getCustomChannelConfiguration() {
		return customChannelConfig;
	}

	public short getDcaChannel(int index) {
		return dcaList[index];
	}

	public short getDcaListCount() {
		return dcaListCount;
	}

	public short getDynamicChannelAlloc() {
		return dcaEnabled;
	}

	public String getEssid() {
		return essid;
	}

	public short getEssidHidden() {
		return essidHidden;
	}

	public long getFragThreshold() {
		return fragThreshold;
	}

	public short getMediumSubType() {
		return mediumSubType;
	}

	public short getMediumType() {
		return mediumType;
	}

	public String getName() {
		return name;
	}

	public long getRtsThreshold() {
		return rtsThreshold;
	}

	public short getServiceType() {
		return serviceType;
	}

	public IInterfaceChannelConfiguration getSupportedChannelConfiguration() {
		return supportedChannelConfig;
	}

	public short getTxPower() {
		return txPower;
	}

	public long getTxRate() {
		return txRate;
	}

	public short getUsageType() {
		return usageType;
	}

	public short[] getDcaList() {
		return dcaList;
	}

	public void setAckTimeout(short ackTimeout) {
		this.ackTimeout	= ackTimeout;
	}

	public void setBeaconInterval(long beaconInterval) {
		this.beaconInterval	= beaconInterval;
	}

	public void setChannel(short channel) {
		this.channel	= channel;
	}

	public void setChannelBondingType(short bonding) {
		this.bondingType	= bonding;
	}

	public void setDcaChannel(int index,short channel) {
		this.dcaList[index]	= channel;
	}

	public void setDcaList(short[] dcaList) {
		for(int i=0; i<this.dcaList.length; i++){
			this.dcaList[i] = dcaList[i];
		}
	}

	public void setDcaListCount(short dcaListCount) {
		this.dcaListCount	= dcaListCount;
		dcaList				= new short[dcaListCount];
	}

	public void setDynamicChannelAlloc(short dynamicChannelAlloc) {
		this.dcaEnabled	= dynamicChannelAlloc;
	}

	public void setEssid(String essId) {
		this.essid	= essId;
	}

	public void setEssidHidden(short hidden) {
		this.essidHidden	= hidden;
	}

	public void setFragThreshold(long frg) {
		this.fragThreshold	= frg;
	}

	public void setMediumSubType(short mediumSubType) {
		this.mediumSubType	= mediumSubType;
	}

	public void setMediumType(short mediumType) {
		this.mediumType	= mediumType;
	}

	public void setName(String interfaceName) {
		this.name	= interfaceName;
	}

	public void setRtsThreshold(long rts) {
		this.rtsThreshold	= rts;
	}

	public void setServiceType(short serviceType) {
		this.serviceType	= serviceType;
	}

	public void setTxPower(short txPower) {
		this.txPower	= txPower;
	}

	public void setTxRate(long txRate) {
		this.txRate	= txRate;
	}

	public void setUsageType(short usageType) {
		this.usageType	= usageType;
	}
	
	public ISecurityConfiguration getSecurityConfiguration() {
		return securityConfig;
	}

	public InterfaceAdvanceConfiguration getInterfaceAdvanceConfiguration() {
		return  interfaceAdvanceConfiguration;
	}
	
	public void setInterfaceAdvanceConfiguration(IInterfaceAdvanceConfiguration interfaceAdvanceConfiguration){
		this.interfaceAdvanceConfiguration=(InterfaceAdvanceConfiguration) interfaceAdvanceConfiguration;
		}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {

		while (configReader.getNextToken() == true) {
			
			int currentTokenType	=	configReader.getCurrentTokenType();
			
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER) {
				
				if(configReader.getCurrentHeader()			==	IConfigurationIODefs.CONFIG_HEADER_PORT_INFO_END){
					return true;
				}else if(configReader.getCurrentHeader()	==	IConfigurationIODefs.CONFIG_HEADER_CHANNEL_CONFIG_START){
					supportedChannelConfig.load(configReader);
				}else if(configReader.getCurrentHeader()	==	IConfigurationIODefs.CONFIG_HEADER_CUSTOM_CHANNEL_CONFIG_START){
					customChannelConfig.load(configReader);
				}else if(configReader.getCurrentHeader()	==	IConfigurationIODefs.CONFIG_HEADER_SECURITY_CONFIG_START){
					securityConfig.load(configReader);
				}
			
			}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE) {
				
				String tokenValue	=	configReader.getCurrentTokenValue();
				
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_ESSID){
					setEssid(tokenValue);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_TX_RATE){
					try{
						long txRate	=	Long.parseLong(tokenValue.trim());
						setTxRate(txRate);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
					
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_TX_POWER){
					try{
						short txPower	=	Short.parseShort(tokenValue.trim());
						setTxPower(txPower);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_FRAG_THRESHOLD){
					try{
						long frgThreschold	=	Long.parseLong(tokenValue.trim());
						setFragThreshold(frgThreschold);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_RTS_THRESHOLD){
					try{
						long rtsThreschold	=	Long.parseLong(tokenValue.trim());
						setRtsThreshold(rtsThreschold);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_CHANNEL){
					try{
						short channel	=	Short.parseShort(tokenValue.trim());
						setChannel(channel);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_DCA_ENABLED){
					try{
						short dcaChAlloc	=	Short.parseShort(tokenValue.trim());
						setDynamicChannelAlloc(dcaChAlloc);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_DCA_LIST){
					
					if(!tokenValue.equals("")){
						String[] dcaStr = tokenValue.split(",");
						if(dcaStr.length > 0){
							if(dcaStr.length == 1 && dcaStr[0].equals("0")){
								setDcaListCount((short)0);
								setDcaList(null);
							}else{
								setDcaListCount((short)dcaStr.length); 
								short[]  dcaList = new short[dcaStr.length];
								for(int i=0;i<dcaStr.length;i++) {
									if(dcaStr[i].trim().equalsIgnoreCase(""))
										continue;
									dcaList[i] = Short.parseShort(dcaStr[i].trim());
								}
								setDcaListCount((short)dcaStr.length);
								setDcaList (dcaList);
								supportedChannelConfig.setChannelInfoCount(dcaStr.length); 
							}
						}
					}	
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_BEACON_INTERVAL){
					try{
						long beaconInterval	=	Long.parseLong(tokenValue.trim());
						setBeaconInterval(beaconInterval);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_ACK_TIMEOUT){
					try{
						short ackTimeOut	=	Short.parseShort(tokenValue.trim());
						setAckTimeout(ackTimeOut);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_USAGE_TYPE){
					try{
						short usageType 	=	Short.parseShort(tokenValue.trim());
						setUsageType(usageType);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_MEDIUM_TYPE){
					try{
						short mediumType	=	Short.parseShort(tokenValue.trim());
						setMediumType(mediumType);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_MEDIUM_SUB_TYPE){
					try{
						short mediumSubtype =	Short.parseShort(tokenValue.trim());
						setMediumSubType(mediumSubtype);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_SERVICE_TYPE){
					try{
						short serviceType 	=	Short.parseShort(tokenValue.trim());
						setServiceType(serviceType);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_NAME){
					setName(tokenValue.trim());
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_HIDE_ESSID){
					try{
						short hiddenESSID 	=	Short.parseShort(tokenValue.trim());
						setEssidHidden(hiddenESSID);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}
		 }
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_PORT_INFO_START);
		
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_NAME,getName());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_ESSID,getEssid());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_TX_RATE,Long.toString(getTxRate()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_TX_POWER,Long.toString(getTxPower()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_FRAG_THRESHOLD,Long.toString(getFragThreshold()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_RTS_THRESHOLD, Long.toString(getRtsThreshold()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_DCA_ENABLED, Short.toString(getDynamicChannelAlloc()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_CHANNEL, Long.toString(getChannel()));
		
		String dca = "";
		if(getDcaListCount() > 0){
			short[] dca_list = getDcaList();
			
			for(int j=0;j<dca_list.length;j++){
				dca = dca.concat(dca_list[j] + ",");
			}
			if(dca != ""){
				dca = dca.substring(0,dca.length()-1);
			}
		}else{
			dca = "0";
		}
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_DCA_LIST, dca);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_BEACON_INTERVAL, Long.toString(getBeaconInterval()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_USAGE_TYPE, Short.toString(getUsageType()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_MEDIUM_TYPE, Short.toString(getMediumType()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_MEDIUM_SUB_TYPE, Short.toString(getMediumSubType()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_SERVICE_TYPE, Short.toString(getServiceType()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_ACK_TIMEOUT, Short.toString(getAckTimeout()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_HIDE_ESSID, Short.toString(getEssidHidden()));
		
		/**
		 * Saving Supported channels in to the nodefile is temporary commented.
		 */
		/*if(supportedChannelConfig != null){
			
			configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_CHANNEL_CONFIG_START);				
			supportedChannelConfig.save(configWriter);
			configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_CHANNEL_CONFIG_END);
		}*/
		if(customChannelConfig != null){
		
			configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_CUSTOM_CHANNEL_CONFIG_START);
			customChannelConfig.save(configWriter);
			configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_CUSTOM_CHANNEL_CONFIG_END);
		}
		if(securityConfig != null)
			securityConfig.save(configWriter);
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_PORT_INFO_END);
		return true;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof InterfaceInfo){
			
			IInterfaceChannelConfiguration destInterfaceChannelConfig 	= (IInterfaceChannelConfiguration)((InterfaceInfo)destination).getSupportedChannelConfiguration();
			InterfaceCustomChannelConfig   destCustomChannelConfig 		= (InterfaceCustomChannelConfig)((InterfaceInfo)destination).getCustomChannelConfiguration();
			ISecurityConfiguration		   destSecurityConfig			= (ISecurityConfiguration)((InterfaceInfo)destination).getSecurityConfiguration();
			IInterfaceAdvanceConfiguration destInterfaceAdvanceConfig 	= (IInterfaceAdvanceConfiguration)((InterfaceInfo)destination).getInterfaceAdvanceConfiguration();
			if(supportedChannelConfig != null){
			   if(supportedChannelConfig.copyTo(destInterfaceChannelConfig) == false)
					return false;
			}
			if(customChannelConfig != null){
					if(customChannelConfig.copyTo(destCustomChannelConfig)       == false)
							return false;
			}
			if(securityConfig!=null){
				if(securityConfig.copyTo(destSecurityConfig)				 == false)
					return false;
			}	
			//new entry 
			if(interfaceAdvanceConfiguration != null)
				 if(interfaceAdvanceConfiguration.copyTo(destInterfaceAdvanceConfig) == false)
					 return false;
			
			((InterfaceInfo)destination).setMacAddress(getMacAddress());
			
			((InterfaceInfo)destination).setEssid(getEssid());
			((InterfaceInfo)destination).setTxRate(getTxRate());
			((InterfaceInfo)destination).setTxPower(getTxPower());
			((InterfaceInfo)destination).setFragThreshold(getFragThreshold());
			((InterfaceInfo)destination).setRtsThreshold(getRtsThreshold());
			((InterfaceInfo)destination).setChannel(getChannel());
			((InterfaceInfo)destination).setDynamicChannelAlloc(getDynamicChannelAlloc());
			
			short dcaListCnt	=	getDcaListCount();
			((InterfaceInfo)destination).setDcaListCount(dcaListCnt);
			for(int i=0; i<dcaListCnt; i++){
				((InterfaceInfo)destination).setDcaChannel(i,getDcaChannel(i));	
			}
			/**DCA LIST DOUBT**/
			((InterfaceInfo)destination).setBeaconInterval(getBeaconInterval());
			((InterfaceInfo)destination).setAckTimeout(getAckTimeout());
			((InterfaceInfo)destination).setUsageType(getUsageType());
			((InterfaceInfo)destination).setMediumType(getMediumType());
			((InterfaceInfo)destination).setMediumSubType(getMediumSubType());
			((InterfaceInfo)destination).setServiceType(getServiceType());
			((InterfaceInfo)destination).setName(getName());
			((InterfaceInfo)destination).setEssidHidden(getEssidHidden());
			//new entry
			((InterfaceInfo)destination).setSupportedProtocol(getSupportedProtocol());
			((InterfaceInfo)destination).setAppCtrl(getAppCtrl());
			return true;
		}
		return false;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		int 	configChangedFlag = 0;
		int 	channel;
		short   configChanged = IComparable.CONFIG_NOT_CHANGED;
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		IInterfaceInfo	iInterfaceInfo	= (IInterfaceInfo)destination;
		
		if(iInterfaceInfo.getAckTimeout() != getAckTimeout()) {
			statusHandler.packetChanged(PacketFactory.AP_ACK_TIMEOUT_INFO_PACKET);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if(iInterfaceInfo.getEssidHidden() != getEssidHidden()) {
			statusHandler.packetChanged(PacketFactory.IF_HIDDEN_ESSID_PACKET);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if((iInterfaceInfo.getName().equalsIgnoreCase(getName())) == false) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if((iInterfaceInfo.getEssid().equalsIgnoreCase(getEssid())) == false) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			statusHandler.essIdChangedFor(iInterfaceInfo.getName());
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if(iInterfaceInfo.getTxRate() != getTxRate()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if(iInterfaceInfo.getTxPower() != getTxPower()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if(iInterfaceInfo.getFragThreshold() != getFragThreshold()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if(iInterfaceInfo.getRtsThreshold() != getRtsThreshold()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if(iInterfaceInfo.getDynamicChannelAlloc() != getDynamicChannelAlloc()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			statusHandler.manualChannelChanged(true);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if(iInterfaceInfo.getChannel() != getChannel()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			statusHandler.manualChannelChanged(true);
			configChanged = IComparable.CONFIG_CHANGED;
		}
	
		if(iInterfaceInfo.getDcaListCount() != getDcaListCount()){
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		
		for(int i=0; i < iInterfaceInfo.getDcaListCount(); i++){
			channel = iInterfaceInfo.getDcaChannel(i);
			/* this inner for loop is for removing index mismatch error*/
			for(int j=0; j < dcaListCount; j++) {
				
				if(channel == getDcaChannel(j)) {
					configChangedFlag++;
					break;
				}
			}
		}
		if(configChangedFlag != dcaListCount) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChanged = IComparable.CONFIG_CHANGED;
		} /* if one channel entry is deleted and new one is added then above condition 
			will get true and flag for dca list changed is set*/ 
		
		if(iInterfaceInfo.getBeaconInterval() != getBeaconInterval()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if(	iInterfaceInfo.getUsageType() != getUsageType()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if(iInterfaceInfo.getMediumType() != getMediumType()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if(iInterfaceInfo.getMediumSubType() != getMediumSubType()) {
			 statusHandler.isProtocolChanged(true);
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChanged = IComparable.CONFIG_CHANGED;
		}
		if(iInterfaceInfo.getServiceType() != getServiceType()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChanged = IComparable.CONFIG_CHANGED;
		}

		IInterfaceChannelConfiguration destInterfaceChannelConfig 	= (IInterfaceChannelConfiguration)((InterfaceInfo)destination).getSupportedChannelConfiguration();
		InterfaceCustomChannelConfig   destCustomChannelConfig 		= (InterfaceCustomChannelConfig)((InterfaceInfo)destination).getCustomChannelConfiguration();
		ISecurityConfiguration		   destSecurityConfig			= (ISecurityConfiguration)((InterfaceInfo)destination).getSecurityConfiguration();
		IInterfaceAdvanceConfiguration destInterfaceAdvanceConfig 	= (IInterfaceAdvanceConfiguration)((InterfaceInfo)destination).getInterfaceAdvanceConfiguration();
		//new entry
		if(iInterfaceInfo.getSupportedProtocol() != getSupportedProtocol()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 configChanged = IComparable.CONFIG_CHANGED;
		}	
		if(interfaceAdvanceConfiguration.compare(destInterfaceAdvanceConfig, statusHandler)  == IComparable.CONFIG_CHANGED){
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		    configChanged = IComparable.CONFIG_CHANGED;
		}
			
		if(supportedChannelConfig.compare(destInterfaceChannelConfig, statusHandler) == IComparable.CONFIG_CHANGED) {
		    configChanged = IComparable.CONFIG_CHANGED;
		}  
		    
		if(customChannelConfig.compare(destCustomChannelConfig, statusHandler) == IComparable.CONFIG_CHANGED) {
		    configChanged = IComparable.CONFIG_CHANGED;
		}
		if(securityConfig.compare(destSecurityConfig, statusHandler) == IComparable.CONFIG_CHANGED){
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
		    configChanged = IComparable.CONFIG_CHANGED;
		}
		
		
		return configChanged;
	}

	public MacAddress getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(MacAddress newAddress) {
		if(newAddress == null) {
			return;
		}
		
		this.macAddress.setBytes(newAddress.getBytes());
	}

	public short getOperatingChannel() {
		return operatingChannel;
	}

	public void setOperatingChannel(short channel) {
		this.operatingChannel = channel;
	}

	public short getDot11eCategory() {
		return dot11eCategory;
	}

	public void setDot11eCategory(short dot11eCategory) {
		this.dot11eCategory = dot11eCategory;
	}

	public short getDot11eEnabled() {
		return dot11eEnabled;
	}

	public void setDot11eEnabled(short dot11eEnabled) {
		this.dot11eEnabled = dot11eEnabled;
	}
	
	/**
	 * @return Returns the saturationInfo.
	 */
	public ISaturationInfo getSaturationInfo() {
		return saturationInfo;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		
		init();
		
		securityConfig.clearData();
		supportedChannelConfig.clearData();
		customChannelConfig.clearData();
		saturationInfo.clearData();
		interfaceAdvanceConfiguration.clearData();
	}

	@Override
	public short getSupportedProtocol() {		
		return supportedProtocol;
	}

	@Override
	public void setSupportedProtocol(short supportedProtocol) {
		this.supportedProtocol=supportedProtocol;		
	}

	@Override
	public void setAppCtrl(short appCtrl) {
		this.appCtrl = appCtrl;
	}

	@Override
	public short getAppCtrl() {
		return appCtrl;
	}
}
