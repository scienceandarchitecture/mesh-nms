/*
 * Created on Mar 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.configuration.impl;

import java.util.Vector;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.ISipConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipStaInfo;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.IpAddress;

/**
 * @author AbhijitAyarekar
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SipConfiguration implements ISipConfiguration {

	private short 				enabled;
	private short				enabledInP3M;
	private IpAddress			serverIpAddress;
	private int 				port;
	private short				staCount;
	private Vector<ISipStaInfo>	staEntries;
	
	/**
	 * 
	 */
	public SipConfiguration() {
		super();
		staCount			= 0;
		staEntries 			= new Vector<ISipStaInfo>();
		serverIpAddress		= new IpAddress();
		port				= 0;
		enabled				= 0;
		enabledInP3M		= 0;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipConfiguration#setServerIpAddress(byte[])
	 */
	public void setServerIpAddress(byte[] ipAddressBytes) {
		this.serverIpAddress.setBytes(ipAddressBytes);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipConfiguration#getServerIpAddress()
	 */
	public IpAddress getServerIpAddress() {
		return serverIpAddress;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipConfiguration#setPort(int)
	 */
	public void setPort(int port) {
		this.port	= port;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipConfiguration#getPort()
	 */
	public int getPort() {
		return this.port;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipConfiguration#setEnabled(short)
	 */
	public void setEnabled(short enabled) {
		this.enabled = enabled;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipConfiguration#getEnabled()
	 */
	public short getEnabled() {
		return enabled;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipConfiguration#getStaInfoCount()
	 */
	public short getStaInfoCount() {
		return staCount;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipConfiguration#setStaInfoCount(short)
	 */
	public void setStaInfoCount(short newCount) {
		this.staCount = newCount;
		this.staEntries.clear();
		for(int i=0;i<staCount;i++) {
			SipStaConfig staConfig = new SipStaConfig();
			staEntries.add(staConfig);
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISipConfiguration#getStaInfoByIndex(int)
	 */
	public ISipStaInfo getStaInfoByIndex(int index) {
		return (ISipStaInfo) staEntries.get(index);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {

		try {
			
			while (configReader.getNextToken() == true) {
			
				int currentTokenType = configReader.getCurrentTokenType();
				
				if(currentTokenType == IConfigurationIODefs.TOKEN_TYPE_HEADER) {
					
					int currentHeader =  configReader.getCurrentHeader(); 
					
					if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_SIP_CONFIG_END) {
						return true;
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_SIP_STA_INFO_START) {
						for(int i=0;i<staCount; i++){
							ISipStaInfo	staInfo	  =	new SipStaConfig();
							if(staInfo.load(configReader) == false)
								return false;
							staEntries.add(i, staInfo);
						}
					}
					
				} else if(currentTokenType == IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE) {
					
					String tokenValue =	configReader.getCurrentTokenValue();
					if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SIP_STA_COUNT) {
						try{
							short cnt = Short.parseShort(tokenValue.trim());
							setStaInfoCount(cnt);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					} else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SIP_ENABLED) {
						this.enabled = Short.parseShort(tokenValue.trim());
					} else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SIP_ENABLE_IN_P3M) {
						this.enabledInP3M = Short.parseShort(tokenValue.trim());
					} else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SIP_IP_ADDRESS) {
						try {
							this.serverIpAddress.setBytes(tokenValue.trim());
						} catch(Exception e) {
							System.out.println(e);
							return false;
						}
					} else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SIP_PORT) {
						this.port = Integer.parseInt(tokenValue.trim());
					}

				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return false;

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {

		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_SIP_CONFIG_START);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SIP_ENABLED, ""+enabled);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SIP_ENABLE_IN_P3M, ""+enabledInP3M);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SIP_IP_ADDRESS, serverIpAddress.toString());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SIP_PORT, ""+port);
	
		if(staCount <= 0){
			configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_SIP_CONFIG_END);
			return true;
		}
			
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SIP_STA_COUNT, ""+staCount);
		for(int i=0;i<staCount;i++){
			ISipStaInfo	staInfo	= (ISipStaInfo)staEntries.get(i);
			if(staInfo	==	null)
				continue;
			
			if(staInfo.save(configWriter) == false)
				return false;
		}
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_SIP_CONFIG_END);
		return true;

	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		staCount			= 0;
		port				= 0;
		enabled				= 0;
		enabledInP3M		= 0;
		staEntries.clear();
		serverIpAddress.setBytes(IpAddress.resetAddress.getBytes());
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		ISipConfiguration dstSipConfig;
		
		if(destination == null)
			return false;
		
		if((destination instanceof ISipConfiguration) == false)
			return false;

		dstSipConfig = (ISipConfiguration)destination;
		
		dstSipConfig.setEnabled(this.enabled);
		dstSipConfig.setEnabledInP3M(this.enabledInP3M);
		dstSipConfig.setPort(this.port);
		dstSipConfig.setServerIpAddress(this.serverIpAddress.getBytes());
		dstSipConfig.setStaInfoCount(this.staCount);
		
		for(int i=0; i<this.staCount; i++){
			
			ISipStaInfo dstSipStaInfo = dstSipConfig.getStaInfoByIndex(i);
			if(dstSipStaInfo == null){
				continue;
			}
			
			ISipStaInfo srcSipStaInfo = (ISipStaInfo)this.staEntries.get(i);
			
			if(srcSipStaInfo == null) {
				continue;
			}
			
			if(srcSipStaInfo.copyTo(dstSipStaInfo) == false)
				return false;
			
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.IComparable#compare(com.meshdynamics.meshviewer.util.interfaces.IComparable, com.meshdynamics.meshviewer.mesh.IConfigStatusHandler)
	 */
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {

		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		if(!(destination instanceof ISipConfiguration))
			return IComparable.CONFIG_NULL;
		
		ISipConfiguration dstSipConfig	= (ISipConfiguration)destination;
		if(this.enabled != dstSipConfig.getEnabled() 			||
			this.enabledInP3M != dstSipConfig.getEnabledInP3M() ||
			this.port != dstSipConfig.getPort()					||
			this.serverIpAddress.equals(dstSipConfig.getServerIpAddress()) == false ||
			this.staCount != dstSipConfig.getStaInfoCount()) {
			
			statusHandler.packetChanged(PacketFactory.SIP_CONFIG_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
			
		for(int i=0; i<staCount; i++) {
			
			ISipStaInfo	srcSipStaInfo	= (ISipStaInfo)staEntries.get(i);
			ISipStaInfo	dstSipStaInfo	= dstSipConfig.getStaInfoByIndex(i);
			
			if(srcSipStaInfo.compare(dstSipStaInfo, statusHandler) == CONFIG_CHANGED) {
				statusHandler.packetChanged(PacketFactory.SIP_CONFIG_PACKET);
				return IComparable.CONFIG_CHANGED;
			}
	
		}
		
		return IComparable.CONFIG_NOT_CHANGED;

	}

	@Override
	public short getEnabledInP3M() {
		return enabledInP3M;
	}

	@Override
	public void setEnabledInP3M(short enabled) {
		enabledInP3M = enabled;
	}

}
