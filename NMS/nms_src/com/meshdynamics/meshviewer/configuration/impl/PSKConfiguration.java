/**
 * MeshDynamics 
 * -------------- 
 * File     : PSKConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 6  | Aug 10, 2007   | passphrase comparison commented 			  | Imran        |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/
 
package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IPSKConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.HexHelper;



public class PSKConfiguration implements IPSKConfiguration {
	
	private short	pskCipherCCMP;
	private short   pskCipherTKIP;
	private short[] pskKey;
	private short   pskMode;
	
	private int 	pskKeyLength;
	private int		pskGroupKeyRenewal;
	
	private String  pskPassPhrase;
	
	public PSKConfiguration(){
		init();
	}
	
	private void init() {
	
		this.pskCipherCCMP		= 0;
		this.pskCipherTKIP		= 0;
		this.pskKey				= null;
		this.pskKeyLength		= 0;
		this.pskMode			= 0;
		this.pskGroupKeyRenewal = 0;
		this.pskPassPhrase      = "";
		
	}

	public short getPSKCipherCCMP() {
		return pskCipherCCMP;
	}

	public short getPSKCipherTKIP() {
		return pskCipherTKIP;
	}

	public int getPSKGroupKeyRenewal() {
		return pskGroupKeyRenewal;
	}

	public short[] getPSKKey() {
		return pskKey;
	}

	public int getPSKKeyLength() {
		return pskKeyLength;
	}

	public short getPSKMode() {
		return pskMode;
	}

	public String getPSKPassPhrase() {
		return pskPassPhrase;
	}

	public void setPSKCipherCCMP(short psk) {
		this.pskCipherCCMP		= psk;		
	}

	public void setPSKCipherTKIP(short psk) {
		this.pskCipherTKIP		= psk;
	}

	public void setPSKGroupKeyRenewal(int grpKeyRenew) {
		this.pskGroupKeyRenewal = grpKeyRenew;
	}

	public void setPSKKey(short[] key) {
		pskKey		 = new short[pskKeyLength];
		for(int i=0; i<pskKey.length; i++)
			pskKey[i] = key[i];
	}

	public void setPSKKeyLength(int keyLength) {
		pskKeyLength = keyLength;
	}

	public void setPSKMode(short mode) {
		pskMode	= mode;
	}

	public void setPSKPassPhrase(String passphrase) {
		pskPassPhrase = passphrase;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {

		while (configReader.getNextToken() == true){
			
			int currentTokenType	=	configReader.getCurrentTokenType();
			
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				
				if(configReader.getCurrentHeader()			==	IConfigurationIODefs.CONFIG_HEADER_WPA_PERSONAL_INFO_END){
					return true;
				}
			}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
			
				String tokenValue	=	configReader.getCurrentTokenValue();
				if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_CIPHER_CCMP){
					try{
						short cipherCCMP = Short.parseShort(tokenValue.trim());
						setPSKCipherCCMP(cipherCCMP);
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_CIPHER_TKIP){
					try{
						short cipherTKIP = Short.parseShort(tokenValue.trim());
						setPSKCipherTKIP(cipherTKIP); 
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_GROUP_RENEWAL){
					try{
						int grpKeyRenewal  = Integer.parseInt(tokenValue.trim());
						setPSKGroupKeyRenewal(grpKeyRenewal);
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_KEY){
					short [] wpaKey = HexHelper.stringToHex(tokenValue.trim());
					setPSKKey(wpaKey);
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_KEY_LENGTH){
					try{
						int pskKeyLength  = Integer.parseInt(tokenValue.trim());
						setPSKKeyLength(pskKeyLength);
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_MODE){
					try{
						short mode = Short.parseShort(tokenValue.trim());
						setPSKMode(mode);
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_PASSPHRASE){
					setPSKPassPhrase(tokenValue.trim());
				}
			}
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(java.io.OutputStream)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_WPA_PERSONAL_INFO_START);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_CIPHER_CCMP, Short.toString(getPSKCipherCCMP()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_CIPHER_TKIP, Short.toString(getPSKCipherTKIP()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_GROUP_RENEWAL, Integer.toString(getPSKGroupKeyRenewal()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_KEY_LENGTH, Integer.toString(getPSKKeyLength()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_KEY, HexHelper.toHex(getPSKKey()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_MODE, Short.toString(getPSKMode()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_PESONAL_PASSPHRASE, getPSKPassPhrase());
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_WPA_PERSONAL_INFO_END);
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof IPSKConfiguration){
			
			((IPSKConfiguration)destination).setPSKCipherCCMP(getPSKCipherCCMP());
			((IPSKConfiguration)destination).setPSKCipherTKIP(getPSKCipherTKIP());
			((IPSKConfiguration)destination).setPSKGroupKeyRenewal(getPSKGroupKeyRenewal());
			((IPSKConfiguration)destination).setPSKKeyLength(getPSKKeyLength());
			((IPSKConfiguration)destination).setPSKKey(getPSKKey());
			((IPSKConfiguration)destination).setPSKMode(getPSKMode());
			((IPSKConfiguration)destination).setPSKPassPhrase(getPSKPassPhrase());
			
			return true;
		}
		return false;
	}
	
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		IPSKConfiguration	iWpaPersonalConfig	= (IPSKConfiguration)destination;
		
		if(iWpaPersonalConfig.getPSKCipherCCMP() != getPSKCipherCCMP()) {
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;
		}
		if(iWpaPersonalConfig.getPSKCipherTKIP() != getPSKCipherTKIP()) {
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if(iWpaPersonalConfig.getPSKGroupKeyRenewal() != getPSKGroupKeyRenewal()) {	
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if(iWpaPersonalConfig.getPSKKeyLength()	!= getPSKKeyLength()) {
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if((HexHelper.compareArray(iWpaPersonalConfig.getPSKKey(), getPSKKey())) == false){
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if(iWpaPersonalConfig.getPSKMode() != getPSKMode()) {
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if((iWpaPersonalConfig.getPSKPassPhrase().equalsIgnoreCase(getPSKPassPhrase())) == false) {
			//statusHandler.securityChanged(true);
			//return IComparable.CONFIG_CHANGED;
		}		
		return IComparable.CONFIG_NOT_CHANGED;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
	}

}
