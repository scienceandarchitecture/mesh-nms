/**
 * MeshDynamics 
 * -------------- 
 * File     : ChannelInfo.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 2  | Mar 20, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IChannelInfo;
import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;


public class ChannelInfo implements IChannelInfo{

	private	short	channelNumber;
	private	short	privateChannelFlags;
	private long	flags;
	private	long	frequency;
	private long	maxPower;
	private	long	maxRegPower;
	private	long	minPower;
	
	
	public ChannelInfo(){
		init();
	}
	
	private void init(){
	
		channelNumber		= 0;
		flags				= 0;
		frequency			= 0;
		maxPower			= 0;
		maxRegPower			= 0;
		minPower			= 0;
		privateChannelFlags = 0;
		
	}
	public short getChannelNumber() {
		return channelNumber;
	}

	public long getFlags() {
		return flags;
	}

	public long getFrequency() {
		return frequency;
	}

	public long getMaxPower() {
		return maxPower;
	}

	public long getMaxRegPower() {
		return maxRegPower;
	}

	public long getMinPower() {
		return minPower;
	}

	public short getPrivateChannelFlags() {
		return privateChannelFlags;
	}

	public void setChannelNumber(short channelNumber) {
		this.channelNumber = channelNumber;
	}

	public void setFlags(long flags) {
		this.flags = flags;
	}

	public void setFrequency(long frequency) {
		this.frequency = frequency;
	}

	public void setMaxPower(long maxPower) {
		this.maxPower = maxPower;
	}

	public void setMaxRegPower(long maxRegPower) {
		this.maxRegPower = maxRegPower;	
	}

	public void setMinPower(long minPower) {
		this.minPower =	minPower;	
	}

	public void setPrivateChannelFlags(short flag) {
		this.privateChannelFlags = flag;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(java.io.InputStream)
	 */
	public boolean load(ConfigurationReader configReader) {

		while (configReader.getNextToken() == true){
				
				int currentTokenType	=	configReader.getCurrentTokenType();
				
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
					
					if(configReader.getCurrentHeader()			==	IConfigurationIODefs.CONFIG_HEADER_CHANNEL_INFO_END){
						return true;
					}
				}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
				
					String tokenValue	=	configReader.getCurrentTokenValue();
					if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_NUMBER){
						try{
							short channelNo	 =	Short.parseShort(tokenValue.trim());
							setChannelNumber(channelNo);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_CENTER_FREQUENCY){
						try{
							long channelNo	 =	Long.parseLong(tokenValue.trim());
							setFrequency(channelNo);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_FLAGS){
						try{
							long flags    	 =	Long.parseLong(tokenValue.trim());
							setFlags(flags);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_MAX_REG_POWER){
						try{
							long maxRegPower =	Long.parseLong(tokenValue.trim());
							setMaxRegPower(maxRegPower);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_MAX_POWER){
						try{
							long maxPower   =	Long.parseLong(tokenValue.trim());
							setMaxPower(maxPower);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_MIN_POWER){
						try{
							long minPower   =	Long.parseLong(tokenValue.trim());
							setMinPower(minPower);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}
				}
		}
		return false;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(java.io.OutputStream)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_CHANNEL_INFO_START);
		
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_NUMBER, Short.toString(getChannelNumber()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_CENTER_FREQUENCY, Long.toString(getFrequency()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_FLAGS, Long.toString(getFlags()));
		
		/***
		 *  These are values required by supported channels. All these values are run time values.
		 *  No need to write in to the file. 
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_MAX_REG_POWER, Long.toString(getMaxRegPower()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_MAX_POWER, Long.toString(getMaxPower()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_MIN_POWER, Long.toString(getMinPower()));
		*/
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_CHANNEL_INFO_END);
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof ChannelInfo){
			
			((ChannelInfo)destination).setChannelNumber(getChannelNumber());
			((ChannelInfo)destination).setFrequency(getFrequency());
			((ChannelInfo)destination).setFlags(getFlags());
			((ChannelInfo)destination).setMaxRegPower(getMaxRegPower());
			((ChannelInfo)destination).setMaxPower(getMaxPower());
			((ChannelInfo)destination).setMinPower(getMinPower());
			((ChannelInfo)destination).setPrivateChannelFlags(getPrivateChannelFlags());
		
			return true;
		}
		return false;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		IChannelInfo	iChannelInfo	= (IChannelInfo)destination;
		
		if(iChannelInfo.getChannelNumber() != getChannelNumber()) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iChannelInfo.getFrequency() != getFrequency()) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iChannelInfo.getFlags() != getFlags()) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iChannelInfo.getMaxRegPower() != getMaxRegPower()) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iChannelInfo.getMaxPower() != getMaxPower()) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iChannelInfo.getMinPower() != getMinPower()) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iChannelInfo.getPrivateChannelFlags() != getPrivateChannelFlags()) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		return IComparable.CONFIG_NOT_CHANGED;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
	}

}
