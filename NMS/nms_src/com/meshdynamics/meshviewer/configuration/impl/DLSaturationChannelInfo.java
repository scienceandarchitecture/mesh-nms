/*
 * Created on May 18, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.configuration.impl;

/**
 * @author imran
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class DLSaturationChannelInfo {
	
	private int 					channel;
	private int 					apCount;
	private DLSaturationAPInfo[]	apInfo;
	private short 				maxSignal;
	private short 				avgSignal;
	private long 				totalDuration;
	private long 				retryDuration;
	private long 				txDuration;	
	
	public DLSaturationChannelInfo() {
		apInfo = null;
	}
	
	public int getApCount() {
		return apCount;
	}
	
	public void setApCount(int apCount) {
		int index;
		
		this.apCount 	= apCount;
		apInfo			= new DLSaturationAPInfo[this.apCount];
		for(index = 0; index < apCount; index++) {
			apInfo[index] = new DLSaturationAPInfo();
		}
	}
	
	public DLSaturationAPInfo[] getApInfo() {
		return apInfo;
	}
	
	public void setApInfo(DLSaturationAPInfo[] apInfo) {
		this.apInfo = apInfo;
	}
	
	public short getAvgSignal() {
		return avgSignal;
	}
	
	public void setAvgSignal(short avgSignal) {
		this.avgSignal = avgSignal;
	}
	
	public int getChannel() {
		return channel;
	}
	
	public void setChannel(int channel) {
		this.channel = channel;
	}
	
	public short getMaxSignal() {
		return maxSignal;
	}
	
	public void setMaxSignal(short maxSignal) {
		this.maxSignal = maxSignal;
	}
	
	public long getRetryDuration() {
		return retryDuration;
	}

	public void setRetryDuration(long retryDuration) {
		this.retryDuration = retryDuration;
	}
	
	public long getTotalDuration() {
		return totalDuration;
	}
	
	public void setTotalDuration(long totalDuration) {
		this.totalDuration = totalDuration;
	}
	
	public long getTxDuration() {
		return txDuration;
	}
	
	public void setTxDuration(long txDuration) {
		this.txDuration = txDuration;
	}
}
