/**
 * MeshDynamics 
 * -------------- 
 * File     : VlanConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/
 
package com.meshdynamics.meshviewer.configuration.impl;

import java.util.Vector;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;

public class VLANConfiguration implements IVlanConfiguration {

	private Vector<IVlanInfo>	vlanInfoHolder;
	private int					vlanInfoCount;
	
	private short 				default80211eCategoryIndex;
	private short 				default80211eEnabled;
	private short 				default80211pPriority;
	private short				defaultTag;
	
	
	public VLANConfiguration() {
		init();
		vlanInfoHolder				= new Vector<IVlanInfo>();
	}
	
	private void init () {

		vlanInfoCount				= 0;
		default80211eCategoryIndex	= 0;
		default80211eEnabled		= 0;
		default80211pPriority		= 0;
		defaultTag					= 0;
	}
	
	public short getDefault80211eCategoryIndex() {
		return default80211eCategoryIndex;
	}

	public short getDefault80211eEnabled() {
		return default80211eEnabled;
	}

	public short getDefault8021pPriority() {
		return default80211pPriority;
	}

	public short getDefaultTag() {
		return defaultTag;
	}

	public int getVlanCount() {
		return vlanInfoCount;
	}

	public IVlanInfo getVlanInfoByEssid(String vlanEssid) {
		
		for(int i=0; i<vlanInfoCount; i++){
			IVlanInfo	vlanInfo	= (IVlanInfo)vlanInfoHolder.get(i); 
			if(vlanInfo == null) {
				continue;
			}
			
			if(vlanInfo.getEssid().equals(vlanEssid))
				return vlanInfo;
		}
		
		return null;
	}

	public IVlanInfo getVlanInfoByIndex(int index) {
		return (IVlanInfo)vlanInfoHolder.get(index);
	}

	public IVlanInfo getVlanInfoByName(String vlanName) {
		
		for(int i=0; i<vlanInfoCount; i++){
		
			IVlanInfo	vlanInfo	= (IVlanInfo)vlanInfoHolder.get(i); 
			if(vlanInfo == null) {
				continue;
			}
			
			if(vlanInfo.getName().equals(vlanName))
				return vlanInfo;
		}
		
		return null;
	}

	public void setDefault80211eCategoryIndex(short value) {
		default80211eCategoryIndex = value;
	}

	public void setDefault80211eEnabled(short enabled) {
		default80211eEnabled = enabled;
	}

	public void setDefault8021pPriority(short priority) {
		default80211pPriority = priority;
	}

	public void setDefaultTag(short tag) {
		defaultTag = tag;
	}

	public void setVlanCount(int count) {
		vlanInfoCount = count;
		for(int i=0; i<count; i++) {
			IVlanInfo	vlanInfo	= new VLANInfo();
			vlanInfoHolder.add(i,vlanInfo);
		}
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		try {
			while (configReader.getNextToken() == true){
			
				int currentTokenType	=	configReader.getCurrentTokenType();
				
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
					
					int currentHeader   =   configReader.getCurrentHeader(); 
					
					if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_VLAN_CONFIG_END){
						return true;
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_VLAN_INFO_START){
						IVlanInfo vlanInfo	  =	new VLANInfo();
						vlanInfo.load(configReader);
						mergeVlan(vlanInfo);
					}
			}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
				String tokenValue	=	configReader.getCurrentTokenValue();
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_VLAN_CONFIG_DEFAULT_TAG){
					try{
						short tag = Short.parseShort(tokenValue.trim());
						setDefaultTag(tag);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_VLAN_CONFIG_DEFAULT_80211E_ENABLED){
					try{
						short _802_11Enabled = Short.parseShort(tokenValue.trim());
						setDefault80211eEnabled(_802_11Enabled);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_VLAN_CONFIG_DEFAULT_80211E){
					try{
						short _802_11E = Short.parseShort(tokenValue.trim());
						setDefault80211eCategoryIndex(_802_11E);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_VLAN_CONFIG_DEFAULT_8021P){
					try{
						short _802_11P = Short.parseShort(tokenValue.trim());
						setDefault8021pPriority(_802_11P);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}
			  }
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return false;
	}


	/**
     * @param name
     */
    private boolean mergeVlan(IVlanInfo vlanInfo) {
        /*check duplicate essid,tag and name*/
        
        if(vlanInfo == null) {
			return false;
		}
        
        for(int i=0;i<vlanInfoHolder.size();i++){
            VLANInfo vinfo = (VLANInfo)vlanInfoHolder.get(i); 
            
            if(vinfo.getEssid().equalsIgnoreCase(vlanInfo.getEssid()) == true){
                return false;
            }
            if(vinfo.getName().equalsIgnoreCase(vlanInfo.getName()) == true){
                return false;
            }
            if(vinfo.getTag() == vlanInfo.getTag()){
                return false;
            }
        }
        
        vlanInfoHolder.add(vlanInfo);
		vlanInfoCount++;
        return true;
    }

    
    /* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_VLAN_CONFIG_START);
		
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_VLAN_CONFIG_DEFAULT_TAG,Short.toString(getDefaultTag()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_VLAN_CONFIG_DEFAULT_80211E_ENABLED,Short.toString(getDefault80211eEnabled()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_VLAN_CONFIG_DEFAULT_80211E,Short.toString(getDefault80211eCategoryIndex()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_VLAN_CONFIG_DEFAULT_8021P,Short.toString(getDefault8021pPriority()));
		
		//configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_VLAN_CONFIG_COUNT,Integer.toString(getVlanCount()));
		
					/***temp comment*/
		for(int i = 0; i<vlanInfoCount;i++){
			
			IVlanInfo	vlanInfo	= (IVlanInfo)vlanInfoHolder.get(i);
			if(vlanInfo	==	null)
				continue;
			
			vlanInfo.save(configWriter);
		}
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_VLAN_CONFIG_END);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof IVlanConfiguration){
			
			((IVlanConfiguration)destination).setDefaultTag(getDefaultTag());
			((IVlanConfiguration)destination).setDefault80211eEnabled(getDefault80211eEnabled());
			((IVlanConfiguration)destination).setDefault80211eCategoryIndex(getDefault80211eCategoryIndex());
			((IVlanConfiguration)destination).setDefault8021pPriority(getDefault8021pPriority());
			
			int vlanCnt = getVlanCount();
			((IVlanConfiguration)destination).setVlanCount(vlanCnt);
			
			for(int i=0; i<vlanCnt; i++){
				
				IVlanInfo	destVlanInfo  =	(IVlanInfo)((IVlanConfiguration)destination).getVlanInfoByIndex(i);
				if(destVlanInfo == null) {
					continue;
				}
				
				IVlanInfo	vlanInfo	  = (IVlanInfo)vlanInfoHolder.get(i);
				if(vlanInfo == null) {
					continue;
				}
				
				if(vlanInfo.copyTo(destVlanInfo) == false){
					return false;
				}
			}
			
			return true;
		}
		
		return false;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		int configChangedFlag;
		
		configChangedFlag	= 0;
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		IVlanConfiguration	iVlanConfigDest	= (IVlanConfiguration)destination;
	
		if(iVlanConfigDest.getDefaultTag() != getDefaultTag()) {
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;

		}
		if(iVlanConfigDest.getDefault80211eEnabled() != getDefault80211eEnabled()) {
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;

		}
		if(iVlanConfigDest.getDefault80211eCategoryIndex() != getDefault80211eCategoryIndex()) {
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;

		}
		if(iVlanConfigDest.getDefault8021pPriority() != getDefault8021pPriority()) {
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;

		}
				
		if(iVlanConfigDest.getVlanCount() !=	getVlanCount()) {
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		
		for(int i = 0; i<vlanInfoCount;i++) {
			IVlanInfo	srcVlanInfo	= (IVlanInfo)vlanInfoHolder.get(i);
			if(srcVlanInfo == null) {
				continue;
			}
			String		vlanName	= srcVlanInfo.getName();
			
			IVlanInfo	destVlanInfo  =	(IVlanInfo)((IVlanConfiguration)destination).getVlanInfoByName(vlanName);
			if(destVlanInfo == null) {
				continue;
			}
			
			srcVlanInfo.compare(destVlanInfo, statusHandler);
			configChangedFlag++;
		}
		
		if(configChangedFlag != vlanInfoCount) {
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		
		return IComparable.CONFIG_NOT_CHANGED;
	}

    public IVlanInfo addVlanInfo(String essid, short tag) {
        
        VLANInfo vlanInfo;
        
        /**
         * If there is an existing VLAN with the same essid or
         * with the same tag, we return null.
         */
        
        for(int i = 0; i < vlanInfoCount; i++) {
            
            vlanInfo = (VLANInfo)vlanInfoHolder.get(i);
            
            if(vlanInfo == null) {
                continue;
            }
            
            if(essid.equals(vlanInfo.getEssid()) 
            || vlanInfo.getTag() == tag) {
                return null;
            }
            
        }
        
        vlanInfo = new VLANInfo();
        vlanInfo.setName("vlan-" + Short.toString(tag));
        vlanInfoHolder.add(vlanInfo);
        vlanInfoCount++;
        
        return vlanInfo;
        
    }
    
    public boolean deleteVlanInfo(short tag) {
        
        IVlanInfo vlanInfo;
        
        for(int i = 0; i < vlanInfoCount; i++) {
            
            vlanInfo = (IVlanInfo)vlanInfoHolder.get(i);
            
            if(vlanInfo.getTag() == tag) {
                vlanInfoHolder.remove(i);
                vlanInfoCount--;
                return true;
            }
        }        
        
        return false;
    }
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IVlanConfiguration#addVlanInfo(String)
	 */
	public IVlanInfo addVlanInfo(String vlanName) {
		
		VLANInfo vlanInfo = null;
		
		if(vlanName == null) {
			return null;
		}
		
		for(int i=0;i<vlanInfoCount;i++) {
			vlanInfo = (VLANInfo) vlanInfoHolder.get(i);
			if(vlanInfo == null) {
				continue;
			}
			
			if(vlanName.equalsIgnoreCase(vlanInfo.getName()) == true) {
				return vlanInfo;
			}
		}
		
		vlanInfo = new VLANInfo();
		vlanInfo.setName(vlanName);
		vlanInfoHolder.add(vlanInfo);
		vlanInfoCount++;
		
		return vlanInfo;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IVlanConfiguration#deleteVlanInfo(String)
	 */
	public void deleteVlanInfo(String vlanName) {
		int index;
		
		if(vlanName == null) {
			return;
		}
		
		for(index = 0; index < vlanInfoCount; index++) {
			IVlanInfo vlanInfo = (IVlanInfo)vlanInfoHolder.get(index);
			if(vlanInfo == null) {
				continue;
			}
			if(vlanName.equals(vlanInfo.getName()) == true){
				vlanInfoHolder.remove(index);
				vlanInfoCount--;
				break;
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
		vlanInfoHolder.clear();
	}
}
