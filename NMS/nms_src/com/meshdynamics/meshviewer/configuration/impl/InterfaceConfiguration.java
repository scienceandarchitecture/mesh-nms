/**
 * MeshDynamics 
 * -------------- 
 * File     : InterfaceConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 02, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/
package com.meshdynamics.meshviewer.configuration.impl;

import java.util.Vector;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;

public class InterfaceConfiguration implements IInterfaceConfiguration {

	private Vector<IInterfaceInfo>	interfaceInfo; // array of interface info, size = no of interfaces
	
	public InterfaceConfiguration() {
		interfaceInfo	= new Vector<IInterfaceInfo>();
		//interfaceInfo.setSize(6);
	}
	
	public IInterfaceInfo getInterfaceByIndex(int index) { 
		return (IInterfaceInfo) interfaceInfo.get(index);
	}

	public IInterfaceInfo getInterfaceByName(String ifName) {
		
		for(int i=0;i<interfaceInfo.size();i++) {
			IInterfaceInfo ifInfo = (IInterfaceInfo) interfaceInfo.get(i);
			if(ifInfo == null)
				continue;
			if(ifInfo.getName().equalsIgnoreCase(ifName) == true)
				return ifInfo;
		}
		return null;
	}

	public int getInterfaceCount() {
		return interfaceInfo.size();
	}

	public void setInterfaceCount(int count) {
		interfaceInfo.clear();
		for(int i=0; i<count; i++) {
			IInterfaceInfo ifInfo = new InterfaceInfo();
			interfaceInfo.add(ifInfo);
		}
	}

	public boolean load(ConfigurationReader configReader) {
		
		
		while (configReader.getNextToken() == true){
		
			int currentTokenType	= configReader.getCurrentTokenType();
			
			if(currentTokenType	== IConfigurationIODefs.TOKEN_TYPE_HEADER){
			
				if(configReader.getCurrentHeader() == IConfigurationIODefs.CONFIG_HEADER_PORT_INFO_START){
					for(int i=0; i<interfaceInfo.size(); i++){
						IInterfaceInfo ifInfo = (IInterfaceInfo) interfaceInfo.get(i);
						ifInfo.load(configReader); 
					}
				}else if(configReader.getCurrentHeader() ==	IConfigurationIODefs.CONFIG_HEADER_PORT_CONFIG_END){
					return true;
				}
				
			}else if(currentTokenType == IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
				
				String tokenValue	= configReader.getCurrentTokenValue();
			
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PORT_COUNT){
				
					try{
						int portCnt	=	Integer.parseInt(tokenValue.trim());
						setInterfaceCount(portCnt);  
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(java.io.OutputStream)
	 */
	public boolean save(ConfigurationWriter configWriter) {
	
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_PORT_CONFIG_START);
		
		if(interfaceInfo.size() <= 0) {
			configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_PORT_CONFIG_END);
		}
			
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PORT_COUNT, Integer.toString(getInterfaceCount()));

		for(int i=0; i<interfaceInfo.size(); i++) {
			IInterfaceInfo ifInfo = (IInterfaceInfo) interfaceInfo.get(i);
			ifInfo.save(configWriter);
		}
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_PORT_CONFIG_END);
		return true;
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof IInterfaceConfiguration){
		
			((IInterfaceConfiguration)destination).setInterfaceCount(getInterfaceCount());
			
			for(int i=0; i<getInterfaceCount(); i++){
				
				IInterfaceInfo destInterfaceInfo 	= (IInterfaceInfo)((IInterfaceConfiguration)destination).getInterfaceByIndex(i);
				IInterfaceInfo srcInterfaceInfo 	= (IInterfaceInfo) interfaceInfo.get(i);

				if(srcInterfaceInfo.copyTo(destInterfaceInfo) == false) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {

		short	ret = IComparable.CONFIG_NOT_CHANGED;
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		IInterfaceConfiguration	iInterfaceConfig	= (IInterfaceConfiguration)destination;
		
		if(iInterfaceConfig.getInterfaceCount() != getInterfaceCount()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		
		for(int i=0; i<interfaceInfo.size(); i++) {
			
			IInterfaceInfo	srcInterfaceInfo	= (IInterfaceInfo)interfaceInfo.get(i);
			if(srcInterfaceInfo == null) {
				continue;
			}
			String ifName	= srcInterfaceInfo.getName();
			
			IInterfaceInfo 	dstInterfaceInfo 	= (IInterfaceInfo)((IInterfaceConfiguration)destination).getInterfaceByName(ifName);
			if(dstInterfaceInfo == null) {
				continue;
			}
					
			if(srcInterfaceInfo.compare(dstInterfaceInfo, statusHandler) == IComparable.CONFIG_CHANGED) {
			    ret = IComparable.CONFIG_CHANGED;
			}
		}
		
		return ret;
	}

	public IInterfaceInfo addInterfaceByName(String ifName) {
		
		IInterfaceInfo ifInfo = null;
		
		for(int i=0;i<interfaceInfo.size();i++) {
			ifInfo = (IInterfaceInfo) interfaceInfo.get(i);
			if(ifInfo == null) {
				continue;
			}
			
			if(ifName.equalsIgnoreCase(ifInfo.getName()) == true) {
				return ifInfo;
			}
		}
		
		ifInfo = new InterfaceInfo();
		ifInfo.setName(ifName);
		interfaceInfo.add(ifInfo);
		
		return ifInfo;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		interfaceInfo.clear();	
	}

}
