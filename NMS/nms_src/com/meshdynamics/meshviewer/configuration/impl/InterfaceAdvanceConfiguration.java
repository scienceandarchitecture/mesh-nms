package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IInterfaceAdvanceConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;

public class InterfaceAdvanceConfiguration implements
		IInterfaceAdvanceConfiguration {

	
	private byte channelBandwidth;
	private byte secondaryChannelPosition;
	private byte preamble;
	private byte guardInterval_20;
	private byte guardInterval_40;
	private byte guardInterval_80;
	/*private byte bandwidth_20;
	private byte bandwidth_40;
	private byte bandwidth_80;*/
	private byte frameAggregation;
	private byte MaxAMPDU;
	private byte MaxAMSDU;
	private byte txSTBC;
	private byte rxSTBC;
	private byte ldpc;
	private byte gfMode;
	private byte coexistence;
	private short  MaxMPDU;
	private byte  maxAMPDUEnabled;
	private String protocolName;
	private int    protocolid;
	
public InterfaceAdvanceConfiguration(){
	init();
}
	private void init() {
	
		channelBandwidth=1;
		secondaryChannelPosition=0;
		preamble=0;
		guardInterval_20=0;
		guardInterval_40=0;
		guardInterval_80=0;
		
		maxAMPDUEnabled=1;
		MaxAMPDU=64;
		MaxAMSDU=0;
		txSTBC=1;
		rxSTBC=1;
		ldpc=1;
		gfMode=0;
		coexistence=0;
		MaxMPDU=3895;
		protocolid=0;
}
	@Override
	public byte getChannelBandwidth() {
		return channelBandwidth;
	}

	@Override
	public void setChannelBandwidth(byte channel) {
		this.channelBandwidth=channel;		
	}

	@Override
	public byte getSecondaryChannelPosition() {
				return secondaryChannelPosition;
	}

	@Override
	public void setSecondaryChannelPosition(byte position) {
		this.secondaryChannelPosition=position;		
	}
	@Override
	public byte getPreamble() {
		  return preamble;
	}

	@Override
	public void setPreamble(byte preamble) {
		this.preamble=preamble;		
	}

	@Override
	public byte getGuardInterval_20() {
		return guardInterval_20;
	}

	@Override
	public void setGuardInterval_20(byte guardInterval_20) {
		this.guardInterval_20=guardInterval_20;		
	}

	@Override
	public byte getGuardInterval_40() {
		return guardInterval_40;
	}

	@Override
	public void setGuardInterval_40(byte guardInterval_40) {
		this.guardInterval_40=guardInterval_40;		
	}
	@Override
	public byte getFrameAggregation() {
			return frameAggregation;
	}

	@Override
	public void setFrameAggregation(byte frameAggregation) {
		this.frameAggregation=frameAggregation;		
	}

	@Override
	public byte getMaxAMPDU() {
		return MaxAMPDU;
	}

	@Override
	public void setMaxAMPDU(byte maxAMPDU) {
		this.MaxAMPDU=maxAMPDU;		
	}

	@Override
	public byte getMaxAMSDU() {
			return MaxAMSDU;
	}

	@Override
	public void setMaxAMSDU(byte maxAMSDU) {
		this.MaxAMSDU=maxAMSDU;		
	}

	@Override
	public byte getTxSTBC() {
		 return txSTBC;
	}

	@Override
	public void setTxSTBC(byte txSTBC) {
		this.txSTBC=txSTBC;		
	}

	@Override
	public byte getRxSTBC() {
			return rxSTBC;
	}

	@Override
	public void setRxSTBC(byte rxSTBC) {
		this.rxSTBC=rxSTBC;		
	}

	@Override
	public byte getLDPC() {
		 return ldpc;
	}

	@Override
	public void setLDPC(byte ldpc) {
		this.ldpc=ldpc;		
	}

	@Override
	public byte getGFMode() {
		return gfMode;
	}

	@Override
	public void setGFMode(byte gfMode) {
		this.gfMode=gfMode;		
	}

	@Override
	public byte getCoexistence() {
		return coexistence;
	}

	@Override
	public void setCoexistence(byte coexistence) {
		this.coexistence=coexistence;		
	}

	@Override
	public short getMaxMPDU() {
			return MaxMPDU;
	}

	@Override
	public void setMaxMPDU(short maxMPDU) {
		this.MaxMPDU=maxMPDU;
		
	}
	@Override
	public String getProtocalName() {
		return protocolName;
	}
	@Override
	public void setProtocalName(String protocol) {
		this.protocolName=protocol;
	}
	@Override
	public boolean load(ConfigurationReader configReader) {
		return false;
	}
	@Override
	public boolean save(ConfigurationWriter configWriter) {
		return false;
	}
	@Override
	public void clearData() {
		init();
		
	}
	@Override
	public boolean copyTo(ICopyable destination) {
		if(destination == null)
			return false;
		if(destination instanceof IInterfaceAdvanceConfiguration){
			((IInterfaceAdvanceConfiguration)destination).setChannelBandwidth(getChannelBandwidth());
			((IInterfaceAdvanceConfiguration)destination).setSecondaryChannelPosition(getSecondaryChannelPosition());
			((IInterfaceAdvanceConfiguration)destination).setPreamble(getPreamble());
			((IInterfaceAdvanceConfiguration)destination).setGuardInterval_20(getGuardInterval_20());
			((IInterfaceAdvanceConfiguration)destination).setGuardInterval_40(getGuardInterval_40());
			((IInterfaceAdvanceConfiguration)destination).setGuardInterval_80(getGuardInterval_80());
			((IInterfaceAdvanceConfiguration)destination).setFrameAggregation(getFrameAggregation());
			((IInterfaceAdvanceConfiguration)destination).setMaxAMPDU(getMaxAMPDU());
			((IInterfaceAdvanceConfiguration)destination).setMaxAMSDU(getMaxAMSDU());
			((IInterfaceAdvanceConfiguration)destination).setLDPC(getLDPC());
			((IInterfaceAdvanceConfiguration)destination).setTxSTBC(getTxSTBC());
			((IInterfaceAdvanceConfiguration)destination).setRxSTBC(getRxSTBC());
			((IInterfaceAdvanceConfiguration)destination).setGFMode(getGFMode());
			((IInterfaceAdvanceConfiguration)destination).setCoexistence(getCoexistence());
			((IInterfaceAdvanceConfiguration)destination).setMaxMPDU(getMaxMPDU());
			((IInterfaceAdvanceConfiguration)destination).setMaxAMPDUEnabled(getMaxAMPDUEnabled());
			((IInterfaceAdvanceConfiguration)destination).setProtocolID(getProtocolID());
		}
		return true;
	}
	@Override
	public short compare(IComparable destination,
			IConfigStatusHandler statusHandler) {
		
		 IInterfaceAdvanceConfiguration iIntrfaceAdvanceConfig	= (IInterfaceAdvanceConfiguration)destination;
		 if(iIntrfaceAdvanceConfig.getChannelBandwidth() != getChannelBandwidth()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO); //plane to add Advance settings of interface as configuration packet
		 return IComparable.CONFIG_CHANGED;
		 }
		 if(iIntrfaceAdvanceConfig.getSecondaryChannelPosition() != getSecondaryChannelPosition()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO); //plane to add Advance settings of interface as configuration packet
		 return IComparable.CONFIG_CHANGED;
		 } 
		 if(iIntrfaceAdvanceConfig.getGuardInterval_20() != getGuardInterval_20()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
			 }
		 if(iIntrfaceAdvanceConfig.getGuardInterval_40() != getGuardInterval_40()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
			 }
		 if(iIntrfaceAdvanceConfig.getGuardInterval_80() != getGuardInterval_80()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
			 }
		 if(iIntrfaceAdvanceConfig.getFrameAggregation() != getFrameAggregation()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
		 }
		 
		 if(iIntrfaceAdvanceConfig.getLDPC() != getLDPC()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
		 }
		 
		 if(iIntrfaceAdvanceConfig.getTxSTBC() != getTxSTBC()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
		 }
		 
		 if(iIntrfaceAdvanceConfig.getRxSTBC() != getRxSTBC()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
		 }
		 
		 if(iIntrfaceAdvanceConfig.getMaxAMPDU() != getMaxAMPDU()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
		 }
		 
		 if(iIntrfaceAdvanceConfig.getMaxAMSDU() != getMaxAMSDU()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
		 }
		 
		 if(iIntrfaceAdvanceConfig.getMaxMPDU() != getMaxMPDU()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
		 }
		 if(iIntrfaceAdvanceConfig.getCoexistence() != getCoexistence()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
		 }
		 if(iIntrfaceAdvanceConfig.getGFMode() != getGFMode()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
		 }
		 if(iIntrfaceAdvanceConfig.getMaxAMPDUEnabled() != getMaxAMPDUEnabled()){
			 statusHandler.advanceInfoChanged(true);
			 statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			 return IComparable.CONFIG_CHANGED;
		 }

		return IComparable.CONFIG_NOT_CHANGED;
	}
	@Override
	public int getProtocolID() {
		return protocolid;
	}
	@Override
	public void setProtocolID(int protocolid) {
		this.protocolid=protocolid;
	}
	@Override
	public byte getMaxAMPDUEnabled() {
		return maxAMPDUEnabled;
	}
	@Override
	public void setMaxAMPDUEnabled(byte maxAMPDUEnabled) {
		this.maxAMPDUEnabled=maxAMPDUEnabled;
		
	}
	@Override
	public byte getGuardInterval_80() {
		return guardInterval_80;
	}
	@Override
	public void setGuardInterval_80(byte guardInterval_80) {
		this.guardInterval_80 = guardInterval_80;		
	}

}
