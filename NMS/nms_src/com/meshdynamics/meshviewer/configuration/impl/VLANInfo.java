/**
 * MeshDynamics 
 * -------------- 
 * File     : VlanInfo.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 6  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 5  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 4  | Apr 26, 2007   | Ip Address copying fixed	      			  | Abhijit      |
 * ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/
 
package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.IpAddress;

public class VLANInfo implements IVlanInfo {


	//C80211eConfiguration	_8021eConfig;
	SecurityConfiguration	securityConfig;
	
	
	private short			_8021pPriority;
	private short			_80211ePriority;
	private short			_80211Enabled;
	
	private int				tag;
	
	private String 			essid;
	private String 			name;
	private IpAddress		vlanIpAddress;
	
	public VLANInfo(){
		init();
		securityConfig 	= new SecurityConfiguration();
	}
	
	private void init() {
		
		tag				= 0;
		essid			= "";
		name			= "";
		_8021pPriority = 0;
		vlanIpAddress	= new IpAddress();
		
	}
	
	public short get80211eCategoryIndex() {
		return _80211ePriority;
	}

	public short get80211eEnabled() {
		return _80211Enabled;
	}

	public short get8021pPriority() {
		return _8021pPriority;
	}

	public long getBeaconInterval() {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getEssid() {
		return essid;
	}

	public IpAddress getIpAddress() {
		return vlanIpAddress;
	}

	public String getName() {
		return name;
	}

	public long getRtsThreshold() {
		return 0;
	}

	public ISecurityConfiguration getSecurityConfiguration() {
		return securityConfig;
	}

	public short getServiceType() {
		return 0;
	}

	public int getTag() {
		return tag;
	}

	public short getTxPower() {
		return 0;
	}

	public long getTxRate() {
		return 0;
	}

	public long getfragThreshold() {
		return 0;
	}

	public void set80211eCategoryIndex(short val) {
		_80211ePriority = val;
	}

	public void set80211eEnabled(short val) {
		_80211Enabled = val;
	}

	public void set8021pPriority(short val) {
		_8021pPriority	= val;
	}

	public void setBeaconInterval(long becaonInterval) {
		// TODO Auto-generated method stub

	}

	public void setEssid(String essid) {
		this.essid = essid;
	}

	public void setFragThreshold(long val) {

	}

	public void setIpAddress(IpAddress ip) {
		if(ip == null) {
			return;
		}
		vlanIpAddress.setBytes(ip.getBytes());
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRtsThreshold(long val) {

	}

	public void setSecurityConfiguration(ISecurityConfiguration securityConfig) {
	    ISecurityConfiguration secConf = (ISecurityConfiguration)securityConfig;
	    secConf.copyTo(this.securityConfig);
	}

	public void setServiceType(short val) {

	}

	public void setTag(int tag) {
		this.tag = tag;
	}

	public void setTxPower(short val) {
	
	}

	public void setTxRate(long val) {
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		
		try {
			
			while (configReader.getNextToken() == true){
			
				int currentTokenType	=	configReader.getCurrentTokenType();
				
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
					
					int currentHeader   =   configReader.getCurrentHeader(); 
					
					if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_VLAN_INFO_END){
						return true;
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_SECURITY_CONFIG_START){
						securityConfig.load(configReader);
					}
			}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
				
				String tokenValue	=	configReader.getCurrentTokenValue();
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_NAME){
				    
					setName(tokenValue);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_TAG){
					try{
						int tag = Integer.parseInt(tokenValue.trim());
						setTag(tag);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_ESSID){
					setEssid(tokenValue);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_IP_ADDRESS){
					IpAddress ip =  new IpAddress();
					ip.setBytes(tokenValue);
					setIpAddress(ip);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_8021P_PRIORITY){
					try{
						short _802_11P = Short.parseShort(tokenValue.trim());
						set8021pPriority(_802_11P);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_80211E_ENABLED){
					try{
						short _80211Enabled = Short.parseShort(tokenValue.trim());
						set80211eEnabled(_80211Enabled);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_80211E_PRIORITY){
					try{
						short _802_11E = Short.parseShort(tokenValue.trim());
						set80211eCategoryIndex(_802_11E);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}	
				}
			}
		  }
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(java.io.OutputStream)
	 */
	public boolean save(ConfigurationWriter configWriter) {

		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_VLAN_INFO_START);

		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_NAME, getName());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_IP_ADDRESS, getIpAddress().toString());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_TAG, Integer.toString(getTag()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_8021P_PRIORITY, Short.toString(get8021pPriority()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_80211E_ENABLED, Short.toString(get80211eEnabled()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_80211E_PRIORITY,Short.toString(get80211eCategoryIndex()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_VLAN_INFO_ESSID,getEssid());
		
		securityConfig.save(configWriter);
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_VLAN_INFO_END);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof VLANInfo){
			
			((VLANInfo)destination).setName(getName());
			((VLANInfo)destination).setIpAddress(getIpAddress());
			((VLANInfo)destination).setTag(getTag());
			((VLANInfo)destination).set8021pPriority(get8021pPriority());
			((VLANInfo)destination).set80211eEnabled(get80211eEnabled());
			((VLANInfo)destination).set80211eCategoryIndex(get80211eCategoryIndex());
			((VLANInfo)destination).setEssid(getEssid());
			
			ISecurityConfiguration destSecurityConfigInfo 	 = (ISecurityConfiguration)((VLANInfo)destination).getSecurityConfiguration();
			
			if(securityConfig.copyTo(destSecurityConfigInfo) == false)
				return false;
			
			return true;
		}
		return false;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {

		short	securityConfigChanged;
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		IVlanInfo				iVlanInfoDest			= (IVlanInfo)destination;
		ISecurityConfiguration 	destSecurityConfigInfo 	= (ISecurityConfiguration)((VLANInfo)destination).getSecurityConfiguration();
		
		
		if((iVlanInfoDest.getName().equals(getName())) == false){
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if((iVlanInfoDest.getIpAddress().toString().equalsIgnoreCase(getIpAddress().toString())) == false){
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if(iVlanInfoDest.getTag() != getTag()){
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if(iVlanInfoDest.get8021pPriority() != get8021pPriority()){
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if(iVlanInfoDest.get80211eEnabled()	!= get80211eEnabled()){
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if(iVlanInfoDest.get80211eCategoryIndex() != get80211eCategoryIndex()){
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if(iVlanInfoDest.getEssid().equalsIgnoreCase(getEssid()) == false){
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		
		securityConfigChanged	= securityConfig.compare(destSecurityConfigInfo, statusHandler);
		if(securityConfigChanged == IComparable.CONFIG_CHANGED){
			statusHandler.packetChanged(PacketFactory.AP_VLAN_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		return IComparable.CONFIG_NOT_CHANGED;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */

	
	public void clearData() {
		init();
		securityConfig.clearData();
	}
	
}
