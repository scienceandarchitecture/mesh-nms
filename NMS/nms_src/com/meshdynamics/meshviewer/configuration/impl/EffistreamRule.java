/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamRule.java
 * Comments : 
 * Created  : Jul 08, 2007
 * Author   : Abhishek Patankar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author   |
 * ----------------------------------------------------------------------------------
 * |  0  | Jul 08, 2007 | Restructured & redesigned                       | Abhishek/|
 * |	 |				|												  | Imran    |	
 * ----------------------------------------------------------------------------------
 **********************************************************************************/


package com.meshdynamics.meshviewer.configuration.impl;

import java.util.Enumeration;
import java.util.Hashtable;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IEffistreamAction;
import com.meshdynamics.meshviewer.configuration.IEffistreamRule;
import com.meshdynamics.meshviewer.configuration.IEffistreamRuleCriteria;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.imcppackets.helpers.EffistreamHelper;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;

public class EffistreamRule implements IEffistreamRule{
	
	private short									criteriaId;
	private short									childCount;
	private IEffistreamRule							parent;
	private Hashtable<String, IEffistreamRule>		childrenHash;
	private boolean									isRoot;
	private IEffistreamAction						action;
	private IEffistreamRuleCriteria					value;
	
	private static int			counter = 0;
	public  static int			saveCounter = 0;	
	
	public EffistreamRule(EffistreamRule parentRule, short criteriaId) {
		this.parent			= parentRule;
		this.criteriaId		= criteriaId;
		this.value			= new EffistreamRuleCriteria(criteriaId);
		this.isRoot			= false;
		init();
		childrenHash		= new Hashtable<String, IEffistreamRule>();
	}
	
	private EffistreamRule(){
		this.parent			= null;
		this.criteriaId		= 0;
		this.value			= new EffistreamRuleCriteria(criteriaId);
		this.isRoot			= false;
		init();
		childrenHash		= new Hashtable<String, IEffistreamRule>();
	}

	private void init() {
		this.childCount		= 0;
		this.action			= null;
	}

	public IEffistreamRule		createRule(short criteriaId){
		return new EffistreamRule(this, criteriaId);
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamRule#addRuleByCriteriaId(short)
	 */
	public boolean addRule(IEffistreamRule	newRule) {
		String 			key;
				
		IEffistreamRuleCriteria	newValue	= (IEffistreamRuleCriteria)newRule.getCriteriaValue();
		key									= EffistreamHelper.generateKey(newValue);
		if(childrenHash.containsKey(key) == true) {
			return false;
		}
		childrenHash.put(key, newRule);		
		childCount++;
		return true;
	}
	

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamRule#deleteRuleByCriteriaId(short)
	 */
	public boolean deleteRule(String strIdentifier) {
		
		if(childrenHash.containsKey(strIdentifier)) {
			childrenHash.remove(strIdentifier);
			childCount--;
			return true;
		}else {
			Enumeration<String>	keys	= childrenHash.keys();
			while(keys.hasMoreElements()){
				IEffistreamRule childRule	= (IEffistreamRule) childrenHash.get(keys.nextElement()); 
				if(childRule.deleteRule(strIdentifier) == true){
					return true;
				}
			}
		}
		return false;
	}

	public IEffistreamAction addAction(int actionType) {
		action = new EffistreamAction(actionType);
		return action;
	}

	public void deleteAction() {
		if(action == null) {
			return;
		}
		action.clearData();
		action	= null;	
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamRule#hasAction()
	 */
	public IEffistreamAction getAction() {
		return action;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamRule#getChildCount()
	 */
	public short getChildCount() {
		return childCount;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamRule#getCriteriaId()
	 */
	public short getCriteriaId() {
		return criteriaId;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamRule#getCriteriaValue()
	 */
	public IEffistreamRuleCriteria getCriteriaValue() {
		return value;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.interfaces.IComparable#compare(com.meshdynamics.util.interfaces.IComparable, com.meshdynamics.meshviewer.mesh.IConfigStatusHandler)
	 */
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		short 			comparison;
		IEffistreamRule	dstEffiRule;
		String  		key; 
		
		comparison	= IComparable.CONFIG_NOT_CHANGED;
		
		dstEffiRule	= null;
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		if(destination instanceof IEffistreamRule) {
			dstEffiRule	= (IEffistreamRule)destination;
			
		}else {
			return IComparable.CONFIG_NULL;
		}
				
		if(dstEffiRule.getChildCount() != childCount) {
			statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(dstEffiRule.getCriteriaId() != criteriaId) {
			statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(value.compare((IComparable)dstEffiRule.getCriteriaValue(), statusHandler) == IComparable.CONFIG_CHANGED) {
			statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		
		Enumeration<String>	keys				= childrenHash.keys();
		
		while(keys.hasMoreElements()) {
			key									= (String)keys.nextElement();
			
			IEffistreamRule	srcEffiRuleChild	=(IEffistreamRule) childrenHash.get(key);
			if(srcEffiRuleChild	== null) {
				continue;
			}
			
			IEffistreamRule	dstEffiRuleChild	=(IEffistreamRule) dstEffiRule.getChild(key);
			if(dstEffiRuleChild == null ) {
				statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
				return IComparable.CONFIG_CHANGED;
			}
			
			if(childCount > 0) {
				comparison	= srcEffiRuleChild.compare(dstEffiRuleChild, statusHandler);
				if(comparison == IComparable.CONFIG_CHANGED) {
					statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
					return IComparable.CONFIG_CHANGED;
				}
			}
		}
		if(childCount == 0) {
			IEffistreamAction dstAction	= (IEffistreamAction)dstEffiRule.getAction();
			if(dstAction == null) {
				return IComparable.CONFIG_NULL;
			}
			comparison	= action.compare(dstAction, statusHandler);
			if(comparison == IComparable.CONFIG_CHANGED) {
				statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
				return IComparable.CONFIG_CHANGED;
			}
		}
		
		return IComparable.CONFIG_NOT_CHANGED;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.interfaces.ICopyable#copyTo(com.meshdynamics.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		String key;
		
		if(destination == null) {
			return false;
		}
		if(destination instanceof IEffistreamRule == false) {
			return false;
		}
		
		IEffistreamRule destEffistreamRule	= null;
		
		destEffistreamRule			=((IEffistreamRule)destination);
		IEffistreamRuleCriteria	dstValue	= (IEffistreamRuleCriteria)destEffistreamRule.getCriteriaValue();
		value.copyTo(dstValue);
			
		Enumeration<String>	keys			= childrenHash.keys();
		
		while(keys.hasMoreElements()) {
			key							= (String)keys.nextElement();
			IEffistreamRule	srcEffiRule	= (IEffistreamRule) childrenHash.get(key);
			if(srcEffiRule == null) {
				continue;
			}
			IEffistreamRule	dstEffiRule	= (IEffistreamRule)destEffistreamRule.createRule(srcEffiRule.getCriteriaId());
			srcEffiRule.copyTo(dstEffiRule);	
			destEffistreamRule.addRule(dstEffiRule);
			
		}
		if(childCount == 0) {
		
			if(action == null) {
				return false;
			}
			IEffistreamAction	dstAction	= (IEffistreamAction)destEffistreamRule.addAction(action.getActionType());
			action.copyTo(dstAction);				  
		}	
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		short 	count;
		int 	index;
		
		count	= 0;
		if(isRoot == true) {
			loadRulesRootConfiguration(configReader);
		}  
		
		try {
			
			while (configReader.getNextToken() == true){
			
				int currentTokenType	=	configReader.getCurrentTokenType();
				
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
					
					int currentHeader   =   configReader.getCurrentHeader(); 
					
					if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_EFFISTREAM_RULE_START){
						counter++;
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_EFFISTREAM_ACTION_START){
						IEffistreamAction	localAction	= (IEffistreamAction)addAction(IEffistreamAction.ACTION_TYPE_DEFAULT);
						localAction.load(configReader);
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_EFFISTREAM_RULE_END) {
						counter--;
						//if(counter == 0) {
							return true;
						//}
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_EFFISTREAM_CONFIG_END){
						return true;
					}
				}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
					
					String tokenValue	=	configReader.getCurrentTokenValue();
					if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_RULE_CHILD_COUNT){
						try{
							count = Short.parseShort(tokenValue.trim());
						    for(index = 0; index < count; index++) {
						    	EffistreamRule	tempRule	= new EffistreamRule();
								tempRule.load(configReader);
								IEffistreamRule	newRule	= (IEffistreamRule)createRule(tempRule.criteriaId);
								tempRule.copyTo(newRule);
								addRule(newRule);
							}
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_RULE_CRITERIA_ID){
						try{
							this.criteriaId = Short.parseShort(tokenValue.trim());
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_RULE_VALUE){
						value.setCriteriaId(criteriaId);
						value.load(configReader);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * 
	 */
	private void loadRulesRootConfiguration( ConfigurationReader configReader) {
		short 	count;
		int		index;
		
		count = 0;
		while (configReader.getNextToken() == true){
			int currentTokenType	=	configReader.getCurrentTokenType();
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
				String tokenValue	=	configReader.getCurrentTokenValue();
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_RULE_COUNT){
					count	= Short.parseShort(tokenValue.trim());
					break;
				}
			}
		}
		for(index = 0; index < count; index++) {
			EffistreamRule	tempRule	= new EffistreamRule();
			tempRule.load(configReader);
			IEffistreamRule newRule	= (IEffistreamRule)createRule(tempRule.criteriaId);
			tempRule.copyTo(newRule);
			addRule(newRule);
			
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		try {
			saveRule(configWriter, this);
			return true;
		}catch(Exception e) {
			return false;
		}
	}

	/**
	 * @param configWriter
	 * @param effiRule
	 */
	private void saveRule(ConfigurationWriter configWriter, IEffistreamRule rule) {
		
		String	ruleIdentifier;
		
		Enumeration<String>	enumChildrenIdentifiers	= rule.getChildrenIdentifiers();
		
		while(enumChildrenIdentifiers.hasMoreElements()) {
			
			ruleIdentifier				= (String)enumChildrenIdentifiers.nextElement();
			IEffistreamRule	effiRule	= (IEffistreamRule)rule.getChild(ruleIdentifier);
			saveCounter++;
			configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_EFFISTREAM_RULE_START,saveCounter);
					
			short criteriaId	= effiRule.getCriteriaId();
			configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_RULE_CRITERIA_ID,""+criteriaId,saveCounter);
			
			IEffistreamRuleCriteria	value = (IEffistreamRuleCriteria)effiRule.getCriteriaValue();
			value.save(configWriter);
			
			short count	= effiRule.getChildCount();
			configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_RULE_CHILD_COUNT, ""+count, saveCounter);
			
			if(count > 0) {				
				saveCounter++;
				saveRule(configWriter,effiRule);
			} else {
				IEffistreamAction	effiAction	= (IEffistreamAction)effiRule.getAction();
				effiAction.save(configWriter);
			}
			configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_EFFISTREAM_RULE_END, saveCounter);
			saveCounter--;
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
		childrenHash.clear();
		criteriaId	= 0;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamRule#setCriteriaValue(java.lang.Object)
	 */
	public void setCriteriaValue(IEffistreamRuleCriteria value) {
		value.copyTo(this.value);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamRule#getChildren()
	 */
	public Enumeration<String> getChildrenIdentifiers() {
		return childrenHash.keys();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamRule#isRoot()
	 */
	public boolean isRoot() {
		return isRoot;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamRule#setRoot(boolean)
	 */
	public void setRoot(boolean enable) {
		isRoot	= enable;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamRule#getParent()
	 */
	public IEffistreamRule getParent() {
		return parent;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamRule#getChild(java.lang.String)
	 */
	public IEffistreamRule getChild(String srcIdentifier) {
		return (IEffistreamRule)childrenHash.get(srcIdentifier);
	}
}
