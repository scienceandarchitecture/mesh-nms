/**
 * MeshDynamics 
 * -------------- 
 * File     : SecurityConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/
 
package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IPSKConfiguration;
import com.meshdynamics.meshviewer.configuration.IRadiusConfiguration;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.IWEPConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;

public class SecurityConfiguration implements ISecurityConfiguration {

	private PSKConfiguration		wpaPersonalConfig;
	private RadiusConfiguration		wpaEnterpriseConfig;
	private WEPConfiguration		wepConfig;

	private int						enabledSecurity;
	
	public SecurityConfiguration(){
		
		wpaPersonalConfig	=	new PSKConfiguration();
		wpaEnterpriseConfig =   new RadiusConfiguration();
		wepConfig			=   new WEPConfiguration();
		
	}
	
	private void init() {
		enabledSecurity		=   SECURITY_TYPE_NONE;
	}
	
	public int getEnabledSecurity() {
		return enabledSecurity;
	}

	public IPSKConfiguration getPSKConfiguration() {
		return wpaPersonalConfig;
	}

	public IRadiusConfiguration getRadiusConfiguration() {
		return wpaEnterpriseConfig;
	}

	public IWEPConfiguration getWEPConfiguration() {
		return (IWEPConfiguration) wepConfig;
	}

	public void setEnabledSecurity(int securityType) {
		enabledSecurity = securityType;
	}

	public void setPSKConfiguration(IPSKConfiguration config) {
	    IPSKConfiguration pskConf = (IPSKConfiguration)config;
	    pskConf.copyTo(wpaPersonalConfig);
	}

	public void setRadiusConfiguration(IRadiusConfiguration config) {
	    IRadiusConfiguration radiusConf = (IRadiusConfiguration)config;
	    radiusConf.copyTo(wpaEnterpriseConfig);
	}

	public void setWEPConfiguration(IWEPConfiguration config) {
	    IWEPConfiguration wepConf = (IWEPConfiguration)config;
	    wepConf.copyTo(wepConfig);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		
		while (configReader.getNextToken() == true){
			
			int currentTokenType	=	configReader.getCurrentTokenType();
			
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				
				if(configReader.getCurrentHeader()			==	IConfigurationIODefs.CONFIG_HEADER_SECURITY_CONFIG_END){
					return true;
				}else if(configReader.getCurrentHeader()	==	IConfigurationIODefs.CONFIG_HEADER_WEP_INFO_START){
					wepConfig.load(configReader);
					setWEPConfiguration(wepConfig);
				}else if(configReader.getCurrentHeader()	==	IConfigurationIODefs.CONFIG_HEADER_WPA_PERSONAL_INFO_START){
					wpaPersonalConfig.load(configReader);
					setPSKConfiguration(wpaPersonalConfig);
				}else if(configReader.getCurrentHeader()	==	IConfigurationIODefs.CONFIG_HEADER_WPA_ENTERPRISE_INFO_START){
					wpaEnterpriseConfig.load(configReader);
					setRadiusConfiguration(wpaEnterpriseConfig);
				}
			}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
			
				String tokenValue	=	configReader.getCurrentTokenValue();
				if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_TYPE){
					try{
						int val = Integer.parseInt(tokenValue.trim());
						if(val  == SECURITY_TYPE_NONE)
							setEnabledSecurity(SECURITY_TYPE_NONE);
						else if(val	==	SECURITY_TYPE_WEP)
							setEnabledSecurity(SECURITY_TYPE_WEP);
						else if(val	==	SECURITY_TYPE_WPA_PERSONAL)
							setEnabledSecurity(SECURITY_TYPE_WPA_PERSONAL);
						else if(val	==	SECURITY_TYPE_WPA_ENTERPRISE)
							setEnabledSecurity(SECURITY_TYPE_WPA_ENTERPRISE);
						
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_SECURITY_CONFIG_START);
					
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_TYPE, Integer.toString(getEnabledSecurity()));
		
		if(getEnabledSecurity() == SECURITY_TYPE_NONE)
			saveNoneInfo(configWriter);
		if(getEnabledSecurity() == SECURITY_TYPE_WEP)
			wepConfig.save(configWriter);
		if(getEnabledSecurity() == SECURITY_TYPE_WPA_PERSONAL)
			wpaPersonalConfig.save(configWriter);
		if(getEnabledSecurity() == SECURITY_TYPE_WPA_ENTERPRISE)
			wpaEnterpriseConfig.save(configWriter);
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_SECURITY_CONFIG_END);
		
		return true;
		
	}
	
	private void saveNoneInfo(ConfigurationWriter	configWriter){
		if(configWriter == null)
			return;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof ISecurityConfiguration){
			
			int enabledSecurity = getEnabledSecurity();
			((ISecurityConfiguration)destination).setEnabledSecurity(enabledSecurity);
			
			IWEPConfiguration 	 destWEPConfig	  =	 (IWEPConfiguration)((ISecurityConfiguration)destination).getWEPConfiguration();
			IRadiusConfiguration destRadiusConfig =  (IRadiusConfiguration)((ISecurityConfiguration)destination).getRadiusConfiguration();
			IPSKConfiguration    destPSKConfig    =  (IPSKConfiguration)((ISecurityConfiguration)destination).getPSKConfiguration();
			
			if(enabledSecurity  	== SECURITY_TYPE_NONE){
				return true;
			}else if(enabledSecurity == SECURITY_TYPE_WEP){
				if(wepConfig.copyTo(destWEPConfig) == false)
					return false;
			}else if(enabledSecurity == SECURITY_TYPE_WPA_PERSONAL){
				if(wpaPersonalConfig.copyTo(destPSKConfig) == false)
					return false;
			}else if(enabledSecurity == SECURITY_TYPE_WPA_ENTERPRISE){
				if(wpaEnterpriseConfig.copyTo(destRadiusConfig) == false)
					return false;
			}
			return true;	
		}
		return false;
		}
	
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		short securityConfigChanged = IComparable.CONFIG_NOT_CHANGED;
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		ISecurityConfiguration iSecurityConfig	= (ISecurityConfiguration)destination;
		IWEPConfiguration 	 destWEPConfig	  =	 (IWEPConfiguration)((ISecurityConfiguration)destination).getWEPConfiguration();
		IPSKConfiguration    destPSKConfig    =  (IPSKConfiguration)((ISecurityConfiguration)destination).getPSKConfiguration();
		IRadiusConfiguration destRadiusConfig =  (IRadiusConfiguration)((ISecurityConfiguration)destination).getRadiusConfiguration();
		
		if(iSecurityConfig.getEnabledSecurity()	!=	getEnabledSecurity()) {
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if((iSecurityConfig.getEnabledSecurity() == SECURITY_TYPE_NONE)	&&
				getEnabledSecurity() == SECURITY_TYPE_NONE ) {
			return CONFIG_NOT_CHANGED;
		}
				
		if(getEnabledSecurity() == SECURITY_TYPE_WEP)
			securityConfigChanged	= wepConfig.compare(destWEPConfig, statusHandler);
		if(getEnabledSecurity() == SECURITY_TYPE_WPA_PERSONAL)
			securityConfigChanged	= wpaPersonalConfig.compare(destPSKConfig, statusHandler);
		if(getEnabledSecurity() == SECURITY_TYPE_WPA_ENTERPRISE)
			securityConfigChanged	= wpaEnterpriseConfig.compare(destRadiusConfig, statusHandler);
		
		if(securityConfigChanged == IComparable.CONFIG_CHANGED){
			return IComparable.CONFIG_CHANGED;
		}else {
			return IComparable.CONFIG_NOT_CHANGED; 
		}
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
		
		wpaEnterpriseConfig.clearData();
		wepConfig.clearData();
		wpaPersonalConfig.clearData();
	}
}