/**
 * MeshDynamics 
 * -------------- 
 * File     : NetworkConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 02, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/
package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.INetworkConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.IpAddress;

public class NetworkConfiguration implements INetworkConfiguration {

	private IpAddress	gateWay;
	private String   	hostName;
	private IpAddress	subnetMask;
	private IpAddress	ipAddress;
	
	public NetworkConfiguration(){
		init();
	}
	
	private void init() {
		
		gateWay		=	new IpAddress();
		subnetMask	=	new IpAddress();
		ipAddress	=	new IpAddress();
		hostName	=	"";
	}
	public IpAddress getGateway() {
		return gateWay;
	}

	public String getHostName() {
		return hostName;
	}

	public IpAddress getIpAddress() {
		return ipAddress;
	}

	public IpAddress getSubnetMask() {
		return subnetMask;
	}

	public void setGateway(IpAddress gateway) {
		if(gateway == null) {
			return;
		}
		this.gateWay.setBytes(gateway.getBytes());
	}

	public void setHostName(String name) {
		this.hostName = name;
	}

	public void setIpAddress(IpAddress ipAddress) {
		if(ipAddress == null) {
			return;
		}
		this.ipAddress.setBytes(ipAddress.getBytes());
	}

	public void setSubnetMask(IpAddress subnet) {
		if(subnet == null) {
			return;
		}
		this.subnetMask.setBytes(subnet.getBytes());
	}

	public boolean load(ConfigurationReader configReader) {
		
		while (configReader.getNextToken() == true){
			
			int currentTokenType	=	configReader.getCurrentTokenType();
			
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				if(configReader.getCurrentHeader()==IConfigurationIODefs.CONFIG_HEADER_NETWORK_INFO_END)
					return true;
			}else if(currentTokenType	==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
				String tokenValue	=	configReader.getCurrentTokenValue();
				
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_HOST_NAME){
					setHostName(tokenValue);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_IP_ADDRESS){
					IpAddress ip = new IpAddress();
					ip.setBytes(tokenValue);
					setIpAddress(ip);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SUBNET_MASK){
					IpAddress subnet = new IpAddress();
					subnet.setBytes(tokenValue);
					setSubnetMask(subnet);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_GATEWAY_ADDRESS){
					IpAddress gateway  = new IpAddress();
					gateway.setBytes(tokenValue);
					setGateway(gateway);
				}
					
			}
		}
		return false;
	}

	public boolean save(ConfigurationWriter configWriter) {
	
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_NETWORK_INFO_START);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_HOST_NAME , getHostName());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_IP_ADDRESS, getIpAddress().toString());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SUBNET_MASK, getSubnetMask().toString());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_GATEWAY_ADDRESS, getGateway().toString());
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_NETWORK_INFO_END);
		
		return true;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof INetworkConfiguration){
			
			((INetworkConfiguration)destination).setGateway(getGateway());
			((INetworkConfiguration)destination).setHostName(getHostName());
			((INetworkConfiguration)destination).setIpAddress(getIpAddress());
			((INetworkConfiguration)destination).setSubnetMask(getSubnetMask());
		
			return true;
		}
		return false;
	}
	
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		if(destination == null)
			return IComparable.CONFIG_NULL;
		
		INetworkConfiguration	iNetConfigDest	= (INetworkConfiguration)destination;
		
		if((iNetConfigDest.getHostName().equalsIgnoreCase(getHostName())) == false) {
			statusHandler.packetChanged(PacketFactory.AP_IP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if((iNetConfigDest.getIpAddress().toString().equalsIgnoreCase(getIpAddress().toString())) == false) {
			statusHandler.packetChanged(PacketFactory.AP_IP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if((iNetConfigDest.getSubnetMask().toString().equalsIgnoreCase(getSubnetMask().toString())) == false) {
			statusHandler.packetChanged(PacketFactory.AP_IP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if((iNetConfigDest.getGateway().toString().equalsIgnoreCase(getGateway().toString())) == false) {
			statusHandler.packetChanged(PacketFactory.AP_IP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		return IComparable.CONFIG_NOT_CHANGED;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
	}


}