/**
 * MeshDynamics 
 * -------------- 
 * File     : C80211eCategoryConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.I80211eCategoryConfiguration;
import com.meshdynamics.meshviewer.configuration.I80211eCategoryInfo;
import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;

public class C80211eCategoryConfiguration implements I80211eCategoryConfiguration {

	private int							infoCount;
	private C80211eCategoryInfo[]		dot11eCategoryInfo;

	public C80211eCategoryConfiguration() {
		init();
	}
	
	private void init(){
		infoCount 			= 0;
		dot11eCategoryInfo	= null;
	}
	
	public I80211eCategoryInfo getCategoryInfo(int index) {
		return dot11eCategoryInfo[index];
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.I80211eCategoryConfiguration#getCategoryInfoByName(java.lang.String)
	 */
	public I80211eCategoryInfo getCategoryInfoByName(String categoryName) {
		
		int index;
		
		for(index = 0; index < infoCount; index++) {
		
			if(dot11eCategoryInfo[index] == null) {
				continue;
			}
			if(dot11eCategoryInfo[index].getName().equalsIgnoreCase(categoryName)) {
				return dot11eCategoryInfo[index];
			}
		}
		
		return null;
	}
	
	public int getCount() {
		return infoCount;
	}

	public void setCount(int count) {
		infoCount 			= count;
		dot11eCategoryInfo = new C80211eCategoryInfo[count];
		for(int i=0;i<count;i++) {
			dot11eCategoryInfo[i] = new C80211eCategoryInfo();
		}
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		
		try {
			
			while (configReader.getNextToken() == true){
			
				int currentTokenType	=	configReader.getCurrentTokenType();
				
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
					
					int currentHeader   =   configReader.getCurrentHeader(); 
					
					if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_80211E_CATEGORY_CONFIG_END){
						return true;
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_80211E_CATEGORY_INFO_START){
						for(int i=0; i<dot11eCategoryInfo.length; i++){
							dot11eCategoryInfo[i].load(configReader);
						}
					}
			}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
				
				String tokenValue	=	configReader.getCurrentTokenValue();
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_COUNT){
					try{
						int cnt = Integer.parseInt(tokenValue.trim());
						setCount(cnt);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}
			}
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_80211E_CATEGORY_CONFIG_START);
		
		if(dot11eCategoryInfo == null) {
			configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_80211E_CATEGORY_CONFIG_END);
				return false;			
		}
		
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_COUNT, Integer.toString(dot11eCategoryInfo.length));
		
		for(int i = 0; i < dot11eCategoryInfo.length; i++) {
			
			if(dot11eCategoryInfo[i] == null)
				continue;
			
			dot11eCategoryInfo[i].save(configWriter);
		}
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_80211E_CATEGORY_CONFIG_END);
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof I80211eCategoryConfiguration){
			
			int categoryCnt = getCount();
			((I80211eCategoryConfiguration)destination).setCount(categoryCnt);
			
			for(int i=0; i<categoryCnt; i++){
				
				I80211eCategoryInfo dest80211CategoryInfo = (I80211eCategoryInfo)((I80211eCategoryConfiguration)destination).getCategoryInfo(i);
				if(dot11eCategoryInfo[i].copyTo(dest80211CategoryInfo) == false)
					return false;
			}
			return true;
		}
		return false;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		int configChangedFlag;
		
		configChangedFlag	= 0;
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		I80211eCategoryConfiguration	i80211eCategyConfig	= (I80211eCategoryConfiguration)destination;
		
		if(i80211eCategyConfig.getCount() != getCount()) {
			statusHandler.packetChanged(PacketFactory.DOT11E_CATEGORY_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		
		if(dot11eCategoryInfo == null) {
			return IComparable.CONFIG_NULL;			
		}
		
		for(int i=0; i<infoCount; i++) {
			
			if(dot11eCategoryInfo[i] == null){
				continue;
			}
			String categoryName	= dot11eCategoryInfo[i].getName();
			
			I80211eCategoryInfo destDot11CategoryInfo = (I80211eCategoryInfo)((I80211eCategoryConfiguration)destination).getCategoryInfoByName(categoryName);
			if(destDot11CategoryInfo == null) {
				continue;
			}
			
			if(dot11eCategoryInfo[i].compare(destDot11CategoryInfo, statusHandler) == IComparable.CONFIG_CHANGED) {
			    return IComparable.CONFIG_CHANGED;
			}
			configChangedFlag++;
		}
		
		if(configChangedFlag != infoCount) {
			statusHandler.packetChanged(PacketFactory.DOT11E_CATEGORY_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		
		return IComparable.CONFIG_NOT_CHANGED;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
	}

	
	
}
