/*
 * Created on May 18, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.util.MacAddress;

/**
 * @author imran
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class DLSaturationAPInfo {

	private MacAddress	bssid;
	private String 		essid;
	private short 		signal;
	private long 		retryDuration;
	private long 		txDuration;
	
	public DLSaturationAPInfo() {
		bssid = new MacAddress();
		essid = "";
	}
	/**
	 * @return Returns the bssid.
	 */
	public MacAddress getBssid() {
		return bssid;
	}
	/**
	 * @param bssid The bssid to set.
	 */
	public void setBssid(MacAddress bssid) {
		this.bssid = bssid;
	}
	/**
	 * @return Returns the essid.
	 */
	public String getEssid() {
		return essid;
	}
	/**
	 * @param essid The essid to set.
	 */
	public void setEssid(String essid) {
		this.essid = essid;
	}
	/**
	 * @return Returns the retryDuration.
	 */
	public long getRetryDuration() {
		return retryDuration;
	}
	/**
	 * @param retryDuration The retryDuration to set.
	 */
	public void setRetryDuration(long retryDuration) {
		this.retryDuration = retryDuration;
	}
	/**
	 * @return Returns the signal.
	 */
	public short getSignal() {
		return signal;
	}
	/**
	 * @param signal The signal to set.
	 */
	public void setSignal(short signal) {
		this.signal = signal;
	}
	/**
	 * @return Returns the txDuration.
	 */
	public long getTxDuration() {
		return txDuration;
	}
	/**
	 * @param txDuration The txDuration to set.
	 */
	public void setTxDuration(long txDuration) {
		this.txDuration = txDuration;
	}
}
