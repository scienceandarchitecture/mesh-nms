/**
 * MeshDynamics 
 * -------------- 
 * File     : RadiusConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/
 
package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IRadiusConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.IpAddress;


public class RadiusConfiguration implements IRadiusConfiguration {

	private short		radiusCipherCCMP;
	private short   	radiusCipherTKIP;
	private short   	radiusMode;
	private short		wpaEnterpriseDecidesVlan;
	private int			radiusGroupKeyRenewal;
	
	private long 		radiusPort;

	private String	 	radiusKey;
	
	private IpAddress   serverIpAddress;
	
	public RadiusConfiguration(){
		init();
	}
	
	private void init() {
		
		this.radiusCipherCCMP			= 0;
		this.radiusCipherTKIP			= 0;
		this.radiusKey					= "";
		this.radiusMode					= 0;
		this.radiusGroupKeyRenewal		= 0;
		this.radiusPort					= 1812;
		this.serverIpAddress			= new IpAddress();
		this.wpaEnterpriseDecidesVlan 	= 0;
	}
	
	public short getCipherCCMP() {
		return radiusCipherCCMP;
	}

	public short getCipherTKIP() {
		return radiusCipherTKIP;
	}

	public int getGroupKeyRenewal() {
		return radiusGroupKeyRenewal;
	}

	public String getKey() {
		return radiusKey;
	}

	public short getMode() {
		return radiusMode;
	}

	public long getPort() {
		return radiusPort;
	}

	public IpAddress getServerIPAddress() {
		return serverIpAddress;
	}

	public void setCipherCCMP(short ccmp) {
		radiusCipherCCMP =	ccmp;
	}

	public void setCipherTKIP(short tkip) {
		radiusCipherTKIP = tkip;	
	}

	public void setGroupKeyRenewal(int grpKeyRenew) {
		radiusGroupKeyRenewal = grpKeyRenew; 
	}

	public void setKey(String key) {
		radiusKey = key;
	}

	public void setMode(short mode) {
		radiusMode = mode;
	}

	public void setPort(long port) {
		radiusPort = port;
	}

	public void setServerIPAddress(IpAddress ip) {
		serverIpAddress = ip;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IRadiusConfiguration#getWPAEnterpriseDecidesVlan()
	 */
	public short getWPAEnterpriseDecidesVlan() {
		return wpaEnterpriseDecidesVlan;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IRadiusConfiguration#setWPAEnterpriseDecidesVlan(short)
	 */
	public void setWPAEnterpriseDecidesVlan(short decision) {
		wpaEnterpriseDecidesVlan = decision;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		
		while (configReader.getNextToken() == true){
			
			int currentTokenType	=	configReader.getCurrentTokenType();
			
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				
				if(configReader.getCurrentHeader()			==	IConfigurationIODefs.CONFIG_HEADER_WPA_ENTERPRISE_INFO_END){
					return true;
				}
			}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
			
				String tokenValue	=	configReader.getCurrentTokenValue();
				
				 if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_CIPHER_CCMP){
					try{
						short cipherCCMP = Short.parseShort(tokenValue.trim());
						setCipherCCMP(cipherCCMP);
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_CIPHER_TKIP){
					try{
						short cipherTKIP = Short.parseShort(tokenValue.trim());
						setCipherTKIP(cipherTKIP); 
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_GROUP_KEY_RENEWAL){
					try{
						int grpKeyRenewal  = Integer.parseInt(tokenValue.trim());
						setGroupKeyRenewal(grpKeyRenewal);
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_DECIDES_VLAN){
					try{
						short value  = Short.parseShort(tokenValue.trim());
						setWPAEnterpriseDecidesVlan(value); 
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_KEY){
					setKey(tokenValue);
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_MODE){
					try{
						short mode = Short.parseShort(tokenValue.trim());
						setMode(mode);
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_SERVER_IP_ADDRESS){
					IpAddress ip  =	 new IpAddress();
					ip.setBytes(tokenValue.trim());
					setServerIPAddress(ip);
				}else if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_SERVER_PORT){
					try{
						long port  = Long.parseLong(tokenValue.trim());
						setPort(port);
					 }catch(Exception e){
						e.printStackTrace();
						return false;
					 }
				}
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */

	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_WPA_ENTERPRISE_INFO_START);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_CIPHER_CCMP, Short.toString(getCipherCCMP()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_CIPHER_TKIP, Short.toString(getCipherTKIP()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_GROUP_KEY_RENEWAL, Integer.toString(getGroupKeyRenewal()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_SERVER_IP_ADDRESS, getServerIPAddress().toString());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_KEY, getKey());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_MODE, Short.toString(getMode()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_SERVER_PORT, Long.toString(getPort()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SECURITY_WPA_ENTERPRISE_DECIDES_VLAN, Short.toString(getWPAEnterpriseDecidesVlan()));
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_WPA_ENTERPRISE_INFO_END);
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof IRadiusConfiguration){
			
			((IRadiusConfiguration)destination).setCipherCCMP(getCipherCCMP());
			((IRadiusConfiguration)destination).setCipherTKIP(getCipherTKIP());
			((IRadiusConfiguration)destination).setGroupKeyRenewal(getGroupKeyRenewal());
			((IRadiusConfiguration)destination).setServerIPAddress(getServerIPAddress());
			((IRadiusConfiguration)destination).setKey(getKey());
			((IRadiusConfiguration)destination).setMode(getMode());
			((IRadiusConfiguration)destination).setPort(getPort());
			((IRadiusConfiguration)destination).setWPAEnterpriseDecidesVlan(getWPAEnterpriseDecidesVlan());
		
			return true;
		}
		return false;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {

		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		IRadiusConfiguration	iWPAEnterpriseDest	=(IRadiusConfiguration)destination;
		
		if(iWPAEnterpriseDest.getCipherCCMP() != getCipherCCMP()) {
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if(iWPAEnterpriseDest.getCipherTKIP() != getCipherTKIP()) {
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if(iWPAEnterpriseDest.getGroupKeyRenewal() != getGroupKeyRenewal()) {
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if((iWPAEnterpriseDest.getServerIPAddress().toString().equals(getServerIPAddress().toString())) == false) {
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if((iWPAEnterpriseDest.getKey().equalsIgnoreCase(getKey())) == false){
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if(iWPAEnterpriseDest.getMode() != getMode()) {
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if(iWPAEnterpriseDest.getPort() != getPort()) {
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;

		}
		if(iWPAEnterpriseDest.getWPAEnterpriseDecidesVlan()	!= getWPAEnterpriseDecidesVlan()) {
			statusHandler.securityChanged(true);
			return IComparable.CONFIG_CHANGED;
		}
		return IComparable.CONFIG_NOT_CHANGED;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();	
	}


}
