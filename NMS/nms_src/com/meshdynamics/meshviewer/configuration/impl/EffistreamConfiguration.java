/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamConfiguration.java
 * Comments : 
 * Created  : Jul 09, 2007
 * Author   : Abhishek Patankar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author   |
 * ----------------------------------------------------------------------------------
 * |  0  |jul 10,2007   | Created                                         | Abhishek |
 * ----------------------------------------------------------------------------------*/
 
package com.meshdynamics.meshviewer.configuration.impl;

import java.util.Enumeration;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IEffistreamConfiguration;
import com.meshdynamics.meshviewer.configuration.IEffistreamRule;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;


public class EffistreamConfiguration implements IEffistreamConfiguration {

	private IEffistreamRule		rulesRoot;
	
	/**
	 * 
	 */
	public EffistreamConfiguration() {
		rulesRoot 			= new EffistreamRule(null,(short)0);
		rulesRoot.setRoot(true);
	}
	
	public IEffistreamRule getRulesRoot() {
		return rulesRoot;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		try {
			rulesRoot.load(configReader);
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_EFFISTREAM_CONFIG_START);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_RULE_COUNT,""+rulesRoot.getChildCount());
		rulesRoot.save(configWriter);
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_EFFISTREAM_CONFIG_END);
		return true;
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		rulesRoot.clearData();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.interfaces.ICopyable#copyTo(com.meshdynamics.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination){
		
		if(destination == null) {
			return false;
		}
		if(destination instanceof IEffistreamConfiguration) {
			
			IEffistreamRule destRulesRoot	= (IEffistreamRule)((IEffistreamConfiguration)destination).getRulesRoot();
			destRulesRoot.clearData();
			rulesRoot.copyTo(destRulesRoot);
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.interfaces.IComparable#compare(com.meshdynamics.util.interfaces.IComparable, com.meshdynamics.meshviewer.mesh.IConfigStatusHandler)
	 */
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		short	comparison;
		String 	ruleIdentifier;
		
		comparison	= IComparable.CONFIG_NOT_CHANGED;
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		IEffistreamRule	destRulesRoot	= (IEffistreamRule)((IEffistreamConfiguration)destination).getRulesRoot();
		
		if(destRulesRoot.getChildCount() != rulesRoot.getChildCount()) {
			statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		
		Enumeration<String>	enumChildrenIdentifiers	= rulesRoot.getChildrenIdentifiers();
		
		while(enumChildrenIdentifiers.hasMoreElements()) {
			ruleIdentifier						= (String)enumChildrenIdentifiers.nextElement();
			IEffistreamRule	srcEffiRuleChild	=(IEffistreamRule) rulesRoot.getChild(ruleIdentifier);
			if(srcEffiRuleChild == null) {
				continue;
			}
			
			IEffistreamRule	dstEffiRuleChild	=(IEffistreamRule) destRulesRoot.getChild(ruleIdentifier);
			if(dstEffiRuleChild == null) {
				statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
				return IComparable.CONFIG_CHANGED;
			}
			comparison					= srcEffiRuleChild.compare(dstEffiRuleChild, statusHandler);
			if(comparison == IComparable.CONFIG_CHANGED) {
				statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
				return IComparable.CONFIG_CHANGED;
			}
		}
					
		return IComparable.CONFIG_NOT_CHANGED;
		
	}
}
