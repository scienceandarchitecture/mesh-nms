/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamConfiguration.java
 * Comments : 
 * Created  : Jul 09, 2007
 * Author   : Imran Khan        
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author   |
 * ----------------------------------------------------------------------------------
 * |  0  |jul 10,2007   | Created                                         | Imran    |
 * ----------------------------------------------------------------------------------*/
 
package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IEffistreamRuleCriteria;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.imcppackets.helpers.EffistreamHelper;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;
import com.meshdynamics.util.Range;


public class EffistreamRuleCriteria implements IEffistreamRuleCriteria {

	private short 		criteriaId;
	private MacAddress	macAddr;
	private IpAddress	ipAddr;
	private Long		longValue;
	private Range		rangeValue;
	
	public EffistreamRuleCriteria(short criteriaId) {
		this.criteriaId	= criteriaId;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.interfaces.IComparable#compare(com.meshdynamics.util.interfaces.IComparable, com.meshdynamics.meshviewer.mesh.IConfigStatusHandler)
	 */
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		IEffistreamRuleCriteria	dstValue;
		dstValue	= null;
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		if(destination instanceof IEffistreamRuleCriteria) {
			dstValue	= (IEffistreamRuleCriteria)destination;
		}else {
			return IComparable.CONFIG_NULL;
		}
		switch(criteriaId) {
		
			case EffistreamHelper.ID_TYPE_ETH_TYPE:	
			case EffistreamHelper.ID_TYPE_IP_TOS:
			case EffistreamHelper.ID_TYPE_IP_DIFFSRV:
			case EffistreamHelper.ID_TYPE_IP_PROTO:
			case EffistreamHelper.ID_TYPE_RTP_VERSION:
			case EffistreamHelper.ID_TYPE_RTP_PAYLOAD:
				if(dstValue.getLongValue().equals(this.longValue) == false) {
					statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
					return IComparable.CONFIG_CHANGED;
				}
				break;		
				
			case EffistreamHelper.ID_TYPE_ETH_DST:
			case EffistreamHelper.ID_TYPE_ETH_SRC:
				if(this.macAddr.toString().equalsIgnoreCase(dstValue.getMacAddressValue().toString()) == false) {
					statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
					return IComparable.CONFIG_CHANGED;
				}
				break;
				
			case EffistreamHelper.ID_TYPE_IP_SRC:
			case EffistreamHelper.ID_TYPE_IP_DST:
				if(this.ipAddr.toString().equalsIgnoreCase(dstValue.getIPAddressValue().toString()) == false) {
					statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
					return IComparable.CONFIG_CHANGED;
				}
				break;
				
			case EffistreamHelper.ID_TYPE_UDP_SRC_PORT:
			case EffistreamHelper.ID_TYPE_UDP_DST_PORT:
			case EffistreamHelper.ID_TYPE_UDP_LENGTH:
			case EffistreamHelper.ID_TYPE_TCP_SRC_PORT:
			case EffistreamHelper.ID_TYPE_TCP_DST_PORT:
			case EffistreamHelper.ID_TYPE_TCP_LENGTH:
			case EffistreamHelper.ID_TYPE_RTP_LENGTH:
				Range	dstRange	= dstValue.getRangeValue();
				if(this.rangeValue.getLowerLimit() != dstRange.getLowerLimit()	||
						this.rangeValue.getUpperLimit() != dstRange.getUpperLimit()) {
					statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
					return IComparable.CONFIG_CHANGED;
				}
				break;
			}
		return IComparable.CONFIG_NOT_CHANGED;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.interfaces.ICopyable#copyTo(com.meshdynamics.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null) {
			return false;
		}
		if(destination instanceof IEffistreamRuleCriteria == false) {
			return false;
		}
		
		IEffistreamRuleCriteria dstValue	= null;
		
		dstValue			=((IEffistreamRuleCriteria)destination);
		switch(criteriaId) {
		
			case EffistreamHelper.ID_TYPE_ETH_TYPE:	
			case EffistreamHelper.ID_TYPE_IP_TOS:
			case EffistreamHelper.ID_TYPE_IP_DIFFSRV:
			case EffistreamHelper.ID_TYPE_IP_PROTO:
			case EffistreamHelper.ID_TYPE_RTP_VERSION:
			case EffistreamHelper.ID_TYPE_RTP_PAYLOAD:
				 dstValue.setLongValue(this.longValue.longValue());		
				 break;
				
			case EffistreamHelper.ID_TYPE_ETH_DST:
			case EffistreamHelper.ID_TYPE_ETH_SRC:
				 dstValue.setMacAddressValue(this.macAddr.getBytes());
				 break;
				
			case EffistreamHelper.ID_TYPE_IP_SRC:
			case EffistreamHelper.ID_TYPE_IP_DST:
				 dstValue.setIPAddressValue(ipAddr.getBytes());
				 break;
				
			case EffistreamHelper.ID_TYPE_UDP_SRC_PORT:
			case EffistreamHelper.ID_TYPE_UDP_DST_PORT:
			case EffistreamHelper.ID_TYPE_UDP_LENGTH:
			case EffistreamHelper.ID_TYPE_TCP_SRC_PORT:
			case EffistreamHelper.ID_TYPE_TCP_DST_PORT:
			case EffistreamHelper.ID_TYPE_TCP_LENGTH:
			case EffistreamHelper.ID_TYPE_RTP_LENGTH:
				 dstValue.setRangeValue(this.rangeValue.getLowerLimit(), this.rangeValue.getUpperLimit());
				 break;
			}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		try {
			 			 	
				int currentTokenType	=	configReader.getCurrentTokenType();
				
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
				
					String tokenValue	=	configReader.getCurrentTokenValue().trim();
					
					
						switch(criteriaId) {

						case EffistreamHelper.ID_TYPE_ETH_TYPE:	
						case EffistreamHelper.ID_TYPE_IP_TOS:
						case EffistreamHelper.ID_TYPE_IP_DIFFSRV:
						case EffistreamHelper.ID_TYPE_IP_PROTO:
						case EffistreamHelper.ID_TYPE_RTP_VERSION:
						case EffistreamHelper.ID_TYPE_RTP_PAYLOAD:
							 this.longValue	= new Long(tokenValue);
							 break;		
							
						case EffistreamHelper.ID_TYPE_ETH_DST:
						case EffistreamHelper.ID_TYPE_ETH_SRC:
							 this.macAddr	= new MacAddress();
							 this.macAddr.setBytes(tokenValue);
							 break;
							
						case EffistreamHelper.ID_TYPE_IP_SRC:
						case EffistreamHelper.ID_TYPE_IP_DST:
							 this.ipAddr	= new IpAddress();
							 this.ipAddr.setBytes(tokenValue);
							 break;
							
						case EffistreamHelper.ID_TYPE_UDP_SRC_PORT:
						case EffistreamHelper.ID_TYPE_UDP_DST_PORT:
						case EffistreamHelper.ID_TYPE_UDP_LENGTH:
						case EffistreamHelper.ID_TYPE_TCP_SRC_PORT:
						case EffistreamHelper.ID_TYPE_TCP_DST_PORT:
						case EffistreamHelper.ID_TYPE_TCP_LENGTH:
						case EffistreamHelper.ID_TYPE_RTP_LENGTH:
							 String[]	strRange;
							 strRange 			= (tokenValue.trim()).split(":");
							 this.rangeValue	= new Range();
							 this.rangeValue.setLowerLimit(Long.parseLong(strRange[0].trim()));
							 this.rangeValue.setUpperLimit(Long.parseLong(strRange[1].trim()));
							 break;
						}
					}	
				
				return true;
			
		}catch(Exception e) {
			return false;
		}
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		try {
			String 	strValue	= "";
			
			switch(criteriaId) {

			case EffistreamHelper.ID_TYPE_ETH_TYPE:	
			case EffistreamHelper.ID_TYPE_IP_TOS:
			case EffistreamHelper.ID_TYPE_IP_DIFFSRV:
			case EffistreamHelper.ID_TYPE_IP_PROTO:
			case EffistreamHelper.ID_TYPE_RTP_VERSION:
			case EffistreamHelper.ID_TYPE_RTP_PAYLOAD:
				strValue	= this.longValue.toString();
				break;		
				
			case EffistreamHelper.ID_TYPE_ETH_DST:
			case EffistreamHelper.ID_TYPE_ETH_SRC:
				strValue	= this.macAddr.toString();
				break;
				
			case EffistreamHelper.ID_TYPE_IP_SRC:
			case EffistreamHelper.ID_TYPE_IP_DST:
				strValue	= this.ipAddr.toString();
				break;
				
			case EffistreamHelper.ID_TYPE_UDP_SRC_PORT:
			case EffistreamHelper.ID_TYPE_UDP_DST_PORT:
			case EffistreamHelper.ID_TYPE_UDP_LENGTH:
			case EffistreamHelper.ID_TYPE_TCP_SRC_PORT:
			case EffistreamHelper.ID_TYPE_TCP_DST_PORT:
			case EffistreamHelper.ID_TYPE_TCP_LENGTH:
			case EffistreamHelper.ID_TYPE_RTP_LENGTH:
				strValue	= this.rangeValue.getLowerLimit() +" : "+
							  this.rangeValue.getUpperLimit();
				break;
			}
			configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_RULE_VALUE, strValue, EffistreamRule.saveCounter);
		}catch(Exception e) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamCriteria#getMacAddressValue()
	 */
	public MacAddress getMacAddressValue() {
		return macAddr;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamCriteria#setMacAddressValue(byte[])
	 */
	public void setMacAddressValue(byte[] macAddrBytes) {
		this.macAddr	= new MacAddress();
		this.macAddr.setBytes(macAddrBytes);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamCriteria#getIPAddressValue()
	 */
	public IpAddress getIPAddressValue() {
		return ipAddr;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamCriteria#setIPAddressValue(byte[])
	 */
	public void setIPAddressValue(byte[] ipAddrBytes) {
		this.ipAddr	= new IpAddress();
		this.ipAddr.setBytes(ipAddrBytes);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamCriteria#getLongValue()
	 */
	public Long getLongValue() {
		return longValue;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamCriteria#setLongValue(long)
	 */
	public void setLongValue(long longValue) {
		this.longValue = new Long(longValue);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamCriteria#getRangeValue()
	 */
	public Range getRangeValue() {
		return rangeValue;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamCriteria#setRangeValue(long, long)
	 */
	public void setRangeValue(long lowerLimit, long upperLimit) {
		this.rangeValue	= new Range();
		rangeValue.setLowerLimit(lowerLimit);
		rangeValue.setUpperLimit(upperLimit);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamCriteria#getCriteria()
	 */
	public int getCriteria() {
		return criteriaId;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamCriteria#setCriteriaId(short)
	 */
	public void setCriteriaId(short criteriaId) {
		this.criteriaId	= criteriaId;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		macAddr		= null;
		ipAddr		= null;
		longValue	= null;
		rangeValue	= null;
	}


}
