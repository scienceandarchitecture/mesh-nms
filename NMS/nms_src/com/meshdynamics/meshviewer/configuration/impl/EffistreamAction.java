/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamAction.java
 * Comments : 
 * Created  : Jul 9, 2007
 * Author   : Abhishek
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  | Jul 9, 2007 | bitrate added                                   |Abhishek|
 * --------------------------------------------------------------------------------
 * |  0  | Jul 9, 2007 | Created                                         |Abhishek|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IEffistreamAction;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;

public class EffistreamAction implements IEffistreamAction {

	private int 	actionType;
	private short 	dot11eCategory;
	private	short 	noAck;
	private	short 	dropPacket;		
	private	short	bitRate;
	private	short	queuedRetry;
	
	public EffistreamAction(short dot11eCategory, short dropPacket, short noAck, short bitRate,short queuedRetry) {
		this.dot11eCategory	= dot11eCategory;
		this.dropPacket		= dropPacket;
		this.noAck			= noAck;
		this.bitRate		= bitRate;
		this.queuedRetry	= queuedRetry;
	}
	
	public EffistreamAction() {
		init();
	}

	public EffistreamAction(int actionType) {
		this.actionType = actionType;
		init();
	}

	private void init(){
		this.dropPacket 	= 0;
		this.dot11eCategory = AC_NN;
		this.noAck 			= 0;
		this.queuedRetry	= 0;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamAction#getDot11eCategory()
	 */
	public short getDot11eCategory() {
		return dot11eCategory;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamAction#setDot11eCategory(short)
	 */
	public void setDot11eCategory(short dot11eCategory) {
		this.dot11eCategory	= dot11eCategory;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamAction#getDropPacket()
	 */
	public short getDropPacket() {
		return dropPacket;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamAction#setDropPacket(short)
	 */
	public void setDropPacket(short dropPacket) {
		this.dropPacket	= dropPacket;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamAction#getNoAck()
	 */
	public short getNoAck() {
		return noAck;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamAction#setNoAck(short)
	 */
	public void setNoAck(short noAck) {
		this.noAck	= noAck;
	}
	
	public short getQueuedRetry() {
		return queuedRetry;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamAction#setNoAck(short)
	 */
	public void setQueuedRetry(short queuedRetry) {
		this.queuedRetry = queuedRetry;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.interfaces.IComparable#compare(com.meshdynamics.util.interfaces.IComparable, com.meshdynamics.meshviewer.mesh.IConfigStatusHandler)
	 */
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		if(destination instanceof IEffistreamAction) {
			
			if(((IEffistreamAction)destination).getDot11eCategory()   != dot11eCategory) {
				statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
				return IComparable.CONFIG_CHANGED;
				
			}else if(((IEffistreamAction)destination).getDropPacket() != dropPacket) {
				statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
				return IComparable.CONFIG_CHANGED;
				
			}else if(((IEffistreamAction)destination).getNoAck()	  != noAck) {
				statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
				return IComparable.CONFIG_CHANGED;
				
			}else if(((IEffistreamAction)destination).getBitRate()	  != bitRate) {
				statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
				return IComparable.CONFIG_CHANGED;
				
			}else if(((IEffistreamAction)destination).getQueuedRetry() != queuedRetry) {
				statusHandler.packetChanged(PacketFactory.EFFISTREAM_CONFIG_PACKET);
				return IComparable.CONFIG_CHANGED;
			}
		}
		return IComparable.CONFIG_NOT_CHANGED;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.util.interfaces.ICopyable#copyTo(com.meshdynamics.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null) {
			return false;
		}
		if(destination instanceof IEffistreamAction) {
			
			((IEffistreamAction)destination).setDot11eCategory(dot11eCategory);
			((IEffistreamAction)destination).setDropPacket(dropPacket);
			((IEffistreamAction)destination).setNoAck(noAck);
			((IEffistreamAction)destination).setBitRate(bitRate);
			((IEffistreamAction)destination).setQueuedRetry(queuedRetry);
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		try {
			
			while (configReader.getNextToken() == true){
			
				int currentTokenType	=	configReader.getCurrentTokenType();
				
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
					
					int currentHeader   =   configReader.getCurrentHeader(); 
					
					if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_EFFISTREAM_ACTION_END){
						return true;
					}
				}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
					
					String tokenValue	=	configReader.getCurrentTokenValue();
					if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_ACTION_80211E_CATEGORY){
						try{
							short category = Short.parseShort(tokenValue.trim());
						    setDot11eCategory(category);						    
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_ACTION_DROP_PACKET){
						try{
							short dropPacket = Short.parseShort(tokenValue.trim());
						    setDropPacket(dropPacket);						    
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_ACTION_NO_ACK_POLICY){
						try{
							short noAck = Short.parseShort(tokenValue.trim());
						    setNoAck(noAck);   
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_ACTION_BIT_RATE){
						try{
							short rate = Short.parseShort(tokenValue.trim());
						    setBitRate(rate);   
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_ACTION_QUEUED_RETRY){
						try{
							short queuedRetry = Short.parseShort(tokenValue.trim());
						    setQueuedRetry(queuedRetry);   
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_EFFISTREAM_ACTION_START, EffistreamRule.saveCounter);

		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_ACTION_80211E_CATEGORY, ""+dot11eCategory, EffistreamRule.saveCounter);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_ACTION_NO_ACK_POLICY	 , ""+noAck, EffistreamRule.saveCounter);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_ACTION_DROP_PACKET	 , ""+dropPacket, EffistreamRule.saveCounter);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_ACTION_BIT_RATE		 , ""+bitRate, EffistreamRule.saveCounter);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_EFFISTREAM_ACTION_QUEUED_RETRY	 , ""+queuedRetry, EffistreamRule.saveCounter);
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_EFFISTREAM_ACTION_END, EffistreamRule.saveCounter);

		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamAction#getBitRate()
	 */
	public short getBitRate() {
		return bitRate;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamAction#setBitRate(short)
	 */
	public void setBitRate(short bitRate) {
		this.bitRate	= bitRate;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IEffistreamAction#getActionType()
	 */
	public int getActionType() {
		return actionType;
	}

}
