/**
 * MeshDynamics 
 * -------------- 
 * File     : MeshConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 02, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;


public class MeshConfiguration implements IMeshConfiguration {

	private long		beaconInterval;
	
	private short		dsTxPower;
	private long		dsTxRate;
	
	private short		etsiCertifiedOperation;
	private String		essid;
	private short		fccCertifiedOperation;
	private long 		fragThreshold;
	private long 		rtsThreshold;	
	
	/**Mesh Config Info*/
	
	private short[]     signalMap;
	private short       heartbeatInterval;
	private short       hopCost;
	private short		crThreshold;
	
	/**Mobility info*/
	
	private short		hbMissCount;
	private short		locationAwarenessScanInt;
	private short		MaxAllowableHops;
	private short		stayAwakeCnt;
	private short		bridgeAgeingTime;
	private	short		dynChannelAlloc;
	
	
	public MeshConfiguration() {
		init();
	}
	
	private void init() {
		
		beaconInterval			= 0;
		bridgeAgeingTime		= 0;
		hbMissCount				= 0;
		locationAwarenessScanInt= 0;
		MaxAllowableHops		= 0;
		stayAwakeCnt			= 0;
		dynChannelAlloc			= 0;
		
		dsTxPower				= 0;
		dsTxRate				= 0;
		etsiCertifiedOperation	= 0;
		essid					= "";
		fccCertifiedOperation	= 0;
		fragThreshold			= 0;
		rtsThreshold			= 0;
		
		signalMap				= null;
		heartbeatInterval		= 0;
		hopCost					= 0;
		crThreshold				= 0;
	
	}
	public long getBeaconInterval() {
		return beaconInterval;
	}

	public short getBridgeAgeingTime() {
		return bridgeAgeingTime;
	}

	public short getChangeResistanceThreshold() {
		return crThreshold;
	}

	public short getDSTxPower() {
		return dsTxPower;
	}

	public long getDSTxRate() {
		return dsTxRate;
	}

	public double getDeployDistance() {
		return (fragThreshold & 0xFFFF);
	}

	public byte getDeployDistanceUnit() {
		return (byte)((fragThreshold >> 28)& 0x3);
	}

	public short getDynamicChannelAllocation() {
		return dynChannelAlloc;
	}

	public short getETSICertifiedOperation() {
		return etsiCertifiedOperation;
	}

	public String getEssid() {
		return essid;
	}

	public short getFCCCertifiedOperation() {
		return fccCertifiedOperation;
	}

	public long getFragThreshold() {
		return fragThreshold;
	}

	public short getHeartbeatInterval() {
		return heartbeatInterval;
	}

	public short getHeartbeatMissCount() {
		return hbMissCount;
	}

	public short getHopCost() {
		return hopCost;
	}

	public short getLocationAwarenessScanInterval() {
		return locationAwarenessScanInt;
	}

	public short getMaxAllowableHops() {
		return MaxAllowableHops;
	}

	public short getMobilityMode() {
		return (short)((locationAwarenessScanInt) >> 7);
	}

	public short getMobilityIndex() {
		return (short)(locationAwarenessScanInt & 0x7F);
	}

	public long getRTSThreshold() {
		return rtsThreshold;
	}

	public short[] getSignalMap() {
		return signalMap;
	}

	public short getSpeed() {
		return (short)((fragThreshold >> 16) & 0xFFF);
	}

	public byte getSpeedUnit() {
		return (byte)((fragThreshold >> 30)& 0x3);
	}

	public short getStayAwakeCount() {
		return stayAwakeCnt;
	}

	public void setBeaconInterval(long value) {
		this.beaconInterval = value;
	}

	public void setBridgeAgeingTime(short bat) {
		bridgeAgeingTime	=	bat;
	}

	public void setChangeResistanceThreshold(short value) {
		this.crThreshold = value;
	}

	public void setDSTxPower(short txPower) {
		dsTxPower = txPower;
	}

	public void setDSTxRate(long rate) {
		dsTxRate = rate;	
	}

	public void setDynamicChannelAllocation(short set) {
		dynChannelAlloc	=	set;
	}

	public void setETSICertifiedOperation(short etsi) {
		etsiCertifiedOperation = etsi;
	}

	public void setEssid(String essid) {
		this.essid	=	essid;
	}

	public void setFCCCertifiedOperation(short fcc) {
		this.fccCertifiedOperation = fcc;
	}

	public void setFragThreshold(long value) {
		this.fragThreshold = value;
	}

	public void setHeartbeatInterval(short hbi) {
		this.heartbeatInterval = hbi;
	}

	public void setHeartbeatMissCount(short hbmc) {
		hbMissCount	=	hbmc;
	}

	public void setHopCost(short hopCost) {
		this.hopCost = hopCost;
	}

	public void setLocationAwarenessScanInterval(short lasi) {
		locationAwarenessScanInt	= lasi;
	}

	public void setMaxAllowableHops(short mah) {
		MaxAllowableHops	= mah;
	}

	public void setMobilityMode(short mode) {
		
		// resetting the index
		locationAwarenessScanInt	=  (short) ((mode == IMeshConfiguration.MOBILITY_MODE_MOBILE) ?
		locationAwarenessScanInt : (locationAwarenessScanInt  &= ~0x7F));
		
		// setting/resetting  1st bit of lasi
		locationAwarenessScanInt	=  (short) ((mode == IMeshConfiguration.MOBILITY_MODE_MOBILE) ?
		(locationAwarenessScanInt | 0x80) : (locationAwarenessScanInt &= ~0x80));
	}
	

	public void setRTSThreshold(long value) {
		this.rtsThreshold = value;
	}

	public void setSignalMap(short[] signalMap) {
		if(signalMap == null){
			return;
		}
		this.signalMap = new short[signalMap.length];
		for(int i=0; i<this.signalMap.length; i++){
			this.signalMap[i] = signalMap[i];
		}
	}

	public void setStayAwakeCount(short count) {
		stayAwakeCnt	=	count;
	}

	public void setMobilityIndex(short index){

		// Resetting the mobility index
		locationAwarenessScanInt  &= ~0x7F;

		// Setting the index to new value
		locationAwarenessScanInt |= (index & 0x7F);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(java.io.InputStream)
	 */
	
	public boolean load(ConfigurationReader configReader) {
		
		while (configReader.getNextToken() == true){
			
			int currentTokenType	=	configReader.getCurrentTokenType();
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				if(configReader.getCurrentHeader()==IConfigurationIODefs.CONFIG_HEADER_MESH_INFO_END){
					return true;
				}else if(configReader.getCurrentHeader()==IConfigurationIODefs.CONFIG_HEADER_MOBILITY_INFO_START){
					if(loadMobilityInfo(configReader)== false)
						return false;
				}
			}else if(currentTokenType	==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
			
				String tokenValue	=	configReader.getCurrentTokenValue();
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SIGNAL_MAP){
					
					if(!tokenValue.equals("")){
						String[] signalMapStr = tokenValue.split(",");
						if(signalMapStr.length > 0){
							if(signalMapStr.length == 1 && signalMapStr[0].equals("0")){
								setSignalMap(null);
							}else{
								short[]  signalMapList = new short[signalMapStr.length];
								for(int i=0;i<signalMapStr.length;i++) {
									if(signalMapStr[i].trim().equalsIgnoreCase(""))
										continue;
									signalMapList[i] = Short.parseShort(signalMapStr[i].trim());
								}
								setSignalMap(signalMapList);
							}
						}
				   }
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_HEARTBEAT_INTERVAL){
					try{
						short hbi  = Short.parseShort(tokenValue.trim());
						setHeartbeatInterval(hbi);
					}catch(Exception e){
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_HOP_COST){
					try{
						Short.parseShort(tokenValue.trim());
						/*** 
						 *  DO NOT LOAD HOPCOST yet
						 *  since, All the information related to 
						 *  DHCP,ADHOC flags are  Not Available
						 ***/
						//setHopCost(hopCost);
					}catch(Exception e){
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANGE_RESISTANCE){
					try{
						short changeResistance  = Short.parseShort(tokenValue.trim());
						setChangeResistanceThreshold(changeResistance);
					}catch(Exception e){
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_FRAGMENTATION_THRESHOLD)
				{
					try{
						 long fragTh = Long.parseLong(tokenValue.trim());
						 setFragThreshold(fragTh);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_GLOBAL_DYNAMIC_CHANNEL_ALLOC)
				{
					try{
						 short gdca = Short.parseShort(tokenValue.trim());
						 setDynamicChannelAllocation(gdca);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}
			}
		}
		return false;
	}
	
	

	private boolean loadMobilityInfo(ConfigurationReader configReader) {
		
		while (configReader.getNextToken() == true){
				
				int currentTokenType	=	configReader.getCurrentTokenType();
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
					if(configReader.getCurrentHeader()==IConfigurationIODefs.CONFIG_HEADER_MOBILITY_INFO_END){
						return true;
					}
				}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
					
					String tokenValue	=	configReader.getCurrentTokenValue();
					if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_MOBILITY_MODE){
						try{
							 short mode = Short.parseShort(tokenValue.trim());
							 setMobilityMode(mode);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_MOBILITY_INDEX){
						try{
							 short mobilityIndex = Short.parseShort(tokenValue.trim());
							 setMobilityIndex(mobilityIndex);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}
				}
		  }
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(java.io.OutputStream)
	 */
	public boolean save(ConfigurationWriter configWriter) {

		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_MESH_INFO_START);
		
		saveMeshConfig(configWriter);
		saveMobilityConfig(configWriter);
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_MESH_INFO_END);
		
		return true;
	}
	
	private void saveMeshConfig(ConfigurationWriter configWriter){
		
		if(configWriter == null)
			return ;
		
		short[] sigMap = getSignalMap();
		String strSigMap = "";
		
		if(sigMap == null) {
			return;
		}
		for(int i=0;i<sigMap.length;i++){
			strSigMap = strSigMap.concat(sigMap[i] + ",");
		}
		strSigMap = strSigMap.substring(0,strSigMap.length()-1);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SIGNAL_MAP, strSigMap);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_HEARTBEAT_INTERVAL, Short.toString(getHeartbeatInterval()));
		/*** 
		 *  DO NOT LOAD HOPCOST yet
		 *  since, All the information related to 
		 *  DHCP,ADHOC flags are  Not Available
		 ***/
		//configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_HOP_COST, Short.toString(getHopCost()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANGE_RESISTANCE, Short.toString(getChangeResistanceThreshold()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_FRAGMENTATION_THRESHOLD, Long.toString(getFragThreshold()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_GLOBAL_DYNAMIC_CHANNEL_ALLOC, Long.toString(getDynamicChannelAllocation()));
	}
	private void saveMobilityConfig(ConfigurationWriter configWriter){
	
		if(configWriter == null)
			return;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_MOBILITY_INFO_START);
		
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_MOBILITY_MODE, Integer.toString(getMobilityMode()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_MOBILITY_INDEX, Integer.toString(getMobilityIndex()));
		
		String 	strTemp	= "";
		String 	strUnit = "";
		
		if(getDeployDistanceUnit() == IMeshConfiguration.MOBILITY_DIST_FEET)
			strUnit	=	" feet";
		else if(getDeployDistanceUnit() == IMeshConfiguration.MOBILITY_DIST_METERS)
			strUnit	=	" meters";
		else if(getDeployDistanceUnit() == IMeshConfiguration.MOBILITY_DIST_MILES)
			strUnit	=	" miles";
		strTemp	=	Double.toString(getDeployDistance()) + strUnit;
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_MOBILITY_DISTANCE,strTemp);
		
		if(getSpeedUnit() == IMeshConfiguration.MOBILITY_SPEED_MPH)
			strUnit	=	" mph";
		else if(getSpeedUnit() == IMeshConfiguration.MOBILITY_SPEED_KMPH)
			strUnit	=	" kmph";
		strTemp	=	Integer.toString(getSpeed())+strUnit;
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_MOBILITY_SPEED, strTemp);
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_MOBILITY_INFO_END);
	
	}
	


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof IMeshConfiguration){
			
			IMeshConfiguration destConfig = (IMeshConfiguration)destination;
			/**Mesh Information */
			destConfig.setSignalMap(getSignalMap());
			destConfig.setHeartbeatInterval(getHeartbeatInterval());
			destConfig.setHopCost(getHopCost());
			destConfig.setChangeResistanceThreshold(getChangeResistanceThreshold());
			destConfig.setFragThreshold(getFragThreshold());
			destConfig.setDynamicChannelAllocation(getDynamicChannelAllocation());
			
			/**Mobility Information */
			destConfig.setMobilityMode(getMobilityMode());
			destConfig.setMobilityIndex(getMobilityIndex());
			
			return true;
		}
		return false;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
	    short ret;
	    short retFlag =  IComparable.CONFIG_NOT_CHANGED;
	    
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		IMeshConfiguration	iMeshConfig	= (IMeshConfiguration)destination;
		
		ret = compareMeshConfig(iMeshConfig, statusHandler );
		if(ret == IComparable.CONFIG_CHANGED){
		    retFlag = IComparable.CONFIG_CHANGED;
		}
		ret = compareMobilityConfig(iMeshConfig, statusHandler);
		if(ret == IComparable.CONFIG_CHANGED){
		    retFlag = IComparable.CONFIG_CHANGED;
		}
		
		return retFlag;
	}

	private short compareMeshConfig(IMeshConfiguration meshConfig, IConfigStatusHandler statusHandler) {
		
		if(meshConfig.getHeartbeatInterval() != getHeartbeatInterval()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if(meshConfig.getHopCost() != getHopCost()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			statusHandler.isHopCostChanged(true);
			return IComparable.CONFIG_CHANGED;
		}
		if(meshConfig.getChangeResistanceThreshold() != getChangeResistanceThreshold()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
	
		return IComparable.CONFIG_NOT_CHANGED;
	}
	
	private short compareMobilityConfig(IMeshConfiguration meshConfig, IConfigStatusHandler statusHandler) {
		
		if(meshConfig.getMobilityMode() != getMobilityMode()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if(meshConfig.getMobilityIndex() != getMobilityIndex()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if(meshConfig.getDeployDistance() != getDeployDistance()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if(	meshConfig.getDeployDistanceUnit() != getDeployDistanceUnit()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if(meshConfig.getSpeed() !=	getSpeed()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}
		if(meshConfig.getSpeedUnit() != getSpeedUnit()) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			return IComparable.CONFIG_CHANGED;
		}		
		return IComparable.CONFIG_NOT_CHANGED;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
	}

	@Override
	public boolean getP3MMode() {
		return ((hopCost & 0x02) != 0) ? true : false;
	}

	@Override
	public boolean getP3MModeInfraStartup() {
		return ((hopCost & 0x08) != 0) ? true : false;
	}

	@Override
	public boolean getP3MModeSectoredUse() {
		return ((hopCost & 0x40) != 0) ? true : false;
	}

	@Override
	public void setP3MMode(boolean set) {
		if(set == true)
			hopCost |= 0x02;
		else
			hopCost &= ~(0x02);
	}

	@Override
	public void setP3MModeInfraStartup(boolean set) {
		if(set == true)
			hopCost |= 0x08;
		else
			hopCost &= ~(0x08);
	}

	@Override
	public void setP3MModeSectoredUse(boolean set) {
		if(set == true)
			hopCost |= 0x40;
		else
			hopCost &= ~(0x40);
	}

	@Override
	public boolean getDHCPEnabled() {
		return ((hopCost & 0x10) == 0x10) ? true : false;
	}

	@Override
	public boolean getIGMPEnabled() {
		return ((hopCost & 0x01) == 0x01) ? true : false;
	}

	@Override
	public boolean getPBVEnabled() {
		return ((hopCost & 0x80) == 0x80) ? true : false;
	}

	@Override
	public void setIGMPEnabled(boolean enabled) {
		if(enabled == true)
			hopCost |= 0x01;
		else
			hopCost &= ~(0x01);
	}

}
