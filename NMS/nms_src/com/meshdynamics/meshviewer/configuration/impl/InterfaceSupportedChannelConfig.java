/**
 * MeshDynamics 
 * -------------- 
 * File     : InterfaceSupportedChannelConfig.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.meshviewer.configuration.impl;


import com.meshdynamics.meshviewer.configuration.IChannelInfo;
import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IInterfaceChannelConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;

public class InterfaceSupportedChannelConfig implements IInterfaceChannelConfiguration {

	private ChannelInfo[]		channelInfo;
	
	public InterfaceSupportedChannelConfig(){
		channelInfo = null;
	}
	
	public IChannelInfo getChannelInfo(int index) {
		return channelInfo[index];
	}

	public IChannelInfo getChannelInfoByChNumber(int chNumber) {
		int index;
		
		if(channelInfo == null) {
			return null;
		}
		
		for(index = 0; index < channelInfo.length; index++) {
			if(channelInfo[index].getChannelNumber() == chNumber){
				return channelInfo[index];
			}
		}
		
		return null;
	}
	
	public int getChannelInfoCount() {
		if(channelInfo == null)
			return 0;
		return channelInfo.length; 
	}

	public void setChannelInfoCount(int count) {
		channelInfo =	new ChannelInfo[count];
		for(int i=0; i<count; i++){
			channelInfo[i] = new ChannelInfo();
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		
		int i = 0;
		while (configReader.getNextToken() == true){
			
			int currentTokenType	=	configReader.getCurrentTokenType();
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				
				if(configReader.getCurrentHeader()			==	IConfigurationIODefs.CONFIG_HEADER_CHANNEL_INFO_START){
					channelInfo[i].load(configReader);
					i++;
				}else if(configReader.getCurrentHeader()	==	IConfigurationIODefs.CONFIG_HEADER_CHANNEL_CONFIG_END){
					return true;
				}
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(java.io.OutputStream)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_COUNT, Integer.toString(getChannelInfoCount()));
		
		if(channelInfo == null || channelInfo.length < 0)
			return false;
		
		for(int i=0; i<channelInfo.length; i++){
			if(channelInfo[i] == null)
				continue;
			channelInfo[i].save(configWriter);
		}
		return true;
	}

	public short getAntennaGain() {
		// TODO Auto-generated method stub
		return 0;
	}

	public short getAntennaPower() {
		// TODO Auto-generated method stub
		return 0;
	}

	public short getChannelBandwidth() {
		return 0;
	}

	public short getCtl() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setAntennaGain(short antGain) {
		// TODO Auto-generated method stub
		
	}

	public void setAntennaPower(short antPower) {
		// TODO Auto-generated method stub
		
	}

	public void setChannelBandwidth(short bandWidth) {
		// TODO Auto-generated method stub
		
	}

	public void setCtl(short ctl) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof InterfaceSupportedChannelConfig){
		
			int channelInfoCnt	=	getChannelInfoCount();
			((InterfaceSupportedChannelConfig)destination).setChannelInfoCount(channelInfoCnt);
			
			for(int i=0; i<channelInfoCnt; i++){
				
				IChannelInfo destChannelInfo =	(IChannelInfo)((InterfaceSupportedChannelConfig)destination).getChannelInfo(i);	
				if(channelInfo[i].copyTo(destChannelInfo) == false)
					return false;	
			}
			return true;
		}
		return false;
	}
	
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		return IComparable.CONFIG_NOT_CHANGED;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		channelInfo = null;
	}
}
