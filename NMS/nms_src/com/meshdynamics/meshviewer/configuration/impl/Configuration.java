/**
 * MeshDynamics 
 * -------------- 
 * File     : NewConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No |Date            |  Comment                                   | Author       |
 *  --------------------------------------------------------------------------------
 * | 10 |Nov  5, 2007    | isCountryCodeChanged flag added			  | Imran 		 |
 *  --------------------------------------------------------------------------------
 * | 9  |Jul 20, 2007    | compareGeneralInfo modified				  | Imran 		 |
 * ----------------------------------------------------------------------------------
 * | 8  | May 29, 2007   | fips changes added    					  | Imran        |
 * ----------------------------------------------------------------------------------
 * | 7  | May 29, 2007   | versionGreaterThanEqualTo added    		  | Imran        |
 * ----------------------------------------------------------------------------------
 * | 6  | May 29, 2007   | versionLessThanEqualTo added    			  | Imran        |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/
 
package com.meshdynamics.meshviewer.configuration.impl;


import com.meshdynamics.meshviewer.configuration.I80211eCategoryConfiguration;
import com.meshdynamics.meshviewer.configuration.IACLConfiguration;
import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IEffistreamConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;
import com.meshdynamics.meshviewer.configuration.INetworkConfiguration;
import com.meshdynamics.meshviewer.configuration.ISipConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.meshviewer.util.interfaces.IVersionInfo;
import com.meshdynamics.util.MacAddress;

public class Configuration implements IConfiguration, IVersionInfo {

	private int 							countryCode;
	private MacAddress						dsMacAddress;
	private String							description;
	private String 							model;
	private String							latitude;
	private String							longitude;
	private String 							nodeName;
	private int 							regDomain;
	private short 							firmwareMajorVersion;
	private short 							firmwareMinorVersion;
	private short 							firmwareVariantVersion;
	private String 							preferredParent;
	private	boolean							fipsEnabled;
	
	private ACLConfiguration 				aclConfiguration;
	private C80211eCategoryConfiguration	dot11eCategoryConfiguration;
	private InterfaceConfiguration			interfaceConfiguration;
	private MeshConfiguration				meshConfiguration;
	private NetworkConfiguration			networkConfiguration;
	private VLANConfiguration				vlanConfiguration;
	private EffistreamConfiguration			effistreamConfiguration;
	private SipConfiguration				sipConfiguration;
	
	private ConfigurationWriter				configWriter;
	private boolean isCountryCodeChanged;

	public Configuration() {
		
		super();
		
		init();
		
		aclConfiguration 				= new ACLConfiguration();
		dot11eCategoryConfiguration		= new C80211eCategoryConfiguration();
		interfaceConfiguration			= new InterfaceConfiguration();
		meshConfiguration				= new MeshConfiguration();
		networkConfiguration			= new NetworkConfiguration();
		vlanConfiguration				= new VLANConfiguration();
		effistreamConfiguration			= new EffistreamConfiguration();
		sipConfiguration				= new SipConfiguration();
		
	}

	private void init() {
		
		countryCode						= 0;
		dsMacAddress					= new MacAddress();
		description						= "";
		model							= "";
		latitude						= "";
		longitude						= "";
		nodeName						= "";
		regDomain						= 0;
		preferredParent					= "";
		firmwareMajorVersion			= 0;
		firmwareMinorVersion			= 0;
		firmwareVariantVersion			= 0;
		fipsEnabled						= false;
		isCountryCodeChanged			= false;
		
	}
	public I80211eCategoryConfiguration get80211eCategoryConfiguration() {
		return dot11eCategoryConfiguration;
	}

	public IACLConfiguration getACLConfiguration() {
		return aclConfiguration;
	}

	public IInterfaceConfiguration getInterfaceConfiguration() {
		return interfaceConfiguration;
	}

	public IMeshConfiguration getMeshConfiguration() {
		return meshConfiguration;
	}

	public INetworkConfiguration getNetworkConfiguration() {
		return networkConfiguration;
	}

	public IVlanConfiguration getVlanConfiguration() {
		return vlanConfiguration;
	}

	public IEffistreamConfiguration getEffistreamConfiguration() {
		return effistreamConfiguration;
	}

	public String getLatitude() {
		return latitude; 
	}

	public String getLongitude() {
		return longitude;
	}


	public int getCountryCode() {
		return countryCode;
	}

	public MacAddress getDSMacAddress() {
		return dsMacAddress;
	}

	public String getDescription() {
		return description;
	}

	public String getHardwareModel() {
		return model;
	}

	public String getNodeName() {
		return nodeName; 
	}

	public int getRegulatoryDomain() {
		return regDomain;
	}

	public void setCountryCode(int countryCode) {
		this.countryCode	= countryCode;
	}

	public void setDescription(String desc) {
		this.description	= desc;
	}

	public void setHardwareModel(String hardwareModel) {
		this.model	= hardwareModel;
	}

	public void setLatitude(String lat) {
		this.latitude	= lat;
	}

	public void setLongitude(String longt) {
		this.longitude	= longt;
	}

	public String getPreferedParent() {
		return preferredParent;
	}

	public void setPreferedParent(String prefParent) {
		this.preferredParent = prefParent;
	}

	public void setMacAddress(MacAddress macAddress) {
		this.dsMacAddress = macAddress;
	}

	public void setNodeName(String name) {
		this.nodeName = name;
	}

	public void setRegulatoryDomain(int regDomain) {
		this.regDomain	= regDomain;
		setFipsEnabled();
	}

	/**
	 * Loads Configuration from the file. File is provided by ConfigurationReader
	 */
	public boolean load(ConfigurationReader configReader) {
		
		try {
			while (configReader.getNextToken() == true){
				
				int currentTokenType	=	configReader.getCurrentTokenType();
				
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
					
					int currentHeader   =   configReader.getCurrentHeader();
					
					if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_MODEL_INFO_START){
						loadModelInfo(configReader);
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_VERSION_INFO_START){
						loadVersionInfo(configReader);
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_NODE_INFO_START){
						loadNodeInfo(configReader);
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_NETWORK_INFO_START){
						networkConfiguration.load(configReader);
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_MESH_INFO_START){
						meshConfiguration.load(configReader); 
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_PORT_CONFIG_START){
						interfaceConfiguration.load(configReader);
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_VLAN_CONFIG_START){
						vlanConfiguration.load(configReader); 
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_80211E_CATEGORY_CONFIG_START){
						dot11eCategoryConfiguration.load(configReader);
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_ACL_CONFIG_START){
						aclConfiguration.load(configReader); 
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_EFFISTREAM_CONFIG_START) {
						effistreamConfiguration.load(configReader);
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_SIP_CONFIG_START) {
						sipConfiguration.load(configReader);
					}
				}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
					
				}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_UNDEFINED){
					
				}	
			}
				
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}


	/***
	 * Loads node information. It consists of nodename, node description, latitude, longitude
	 * regulatory domain
	 */
	private void loadNodeInfo(ConfigurationReader configReader) {
		
		while (configReader.getNextToken() == true){
			
			int currentTokenType	=	configReader.getCurrentTokenType();
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				
				if(configReader.getCurrentHeader()==IConfigurationIODefs.CONFIG_HEADER_NODE_INFO_END)
					return;
			}else if(currentTokenType	==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
				
				String tokenValue	=	configReader.getCurrentTokenValue();
				
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_NODE_NAME){
					setNodeName(tokenValue);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_NODE_DESCRIPTION){
					setDescription(tokenValue);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_LATITUDE){
					setLatitude(tokenValue);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_LONGITUDE){
					setLongitude(tokenValue);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_REGULATORY_DOMAIN){
					try{
						int regDomain = Integer.parseInt(tokenValue.trim());
						setRegulatoryDomain(regDomain);
					}catch(Exception e){
						return;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_PREFERRED_PARENT){
					setPreferedParent(tokenValue);
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_COUNTRY_CODE){
					try{
						int code = Integer.parseInt(tokenValue.trim());
						setCountryCode(code);
					}catch(Exception e){
						return;
					}
				}
			}
		}
	}
	
	
	/**
	 * Loads software version information such as Software version
	 */
	private void loadVersionInfo(ConfigurationReader configReader) {
		
		while (configReader.getNextToken() == true){
			
			int currentTokenType		=	configReader.getCurrentTokenType();
			if(currentTokenType			==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				
				if(configReader.getCurrentHeader()==IConfigurationIODefs.CONFIG_HEADER_VERSION_INFO_END)
					return;
			}else if(currentTokenType	==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
			
				String tokenValue		=	configReader.getCurrentTokenValue();
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_SOFTWARE_VERSION){
					String[] tokens = tokenValue.split(".");
					if(tokens.length == 3) {
						setFirmwareMajorVersion(Short.parseShort(tokens[0]));
						setFirmwareMinorVersion(Short.parseShort(tokens[1]));
						setFirmwareVariantVersion(Short.parseShort(tokens[2]));
					}
				}
			}
		}
	}
	
	/**
	 * Loads model information. It consists of Hardware Model
	 */
	private void loadModelInfo(ConfigurationReader configReader) {
		
		while (configReader.getNextToken() == true){
			
			int currentTokenType	=	configReader.getCurrentTokenType();
		
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				
				if(configReader.getCurrentHeader()==IConfigurationIODefs.CONFIG_HEADER_MODEL_INFO_END)
					return;
		
			}else if(currentTokenType	==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
				
				String tokenValue	=	configReader.getCurrentTokenValue();
				if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_HARDWARE_MODEL){
					setHardwareModel(tokenValue);
				}
			}
		}
		
	}
	

	/**
	 * Saves configuration in to file. File is provided by ConfigurationWriter 
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		this.configWriter = configWriter;
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_MESHDYNAMICS);
		saveModelInfo();
		saveVersionInfo();
		saveNodeInfo();
		networkConfiguration.save(configWriter);
		meshConfiguration.save(configWriter);
		interfaceConfiguration.save(configWriter);
		vlanConfiguration.save(configWriter);
		dot11eCategoryConfiguration.save(configWriter);
		aclConfiguration.save(configWriter);
		effistreamConfiguration.save(configWriter);
		sipConfiguration.save(configWriter);
		return true;
		
	}
	
	/**
	 * Saves Model information in to file. It consists of Hardware Model 
	 */
	private void saveModelInfo() {
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_MODEL_INFO_START);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_HARDWARE_MODEL, getHardwareModel());
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_MODEL_INFO_END);
	}
	
	/**
	 * Saves version information in to file. It consists of software version information 
	 */
	private void saveVersionInfo() {
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_VERSION_INFO_START);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_SOFTWARE_VERSION, getFirmwareVersion());
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_VERSION_INFO_END);
	}
	
	/**
	 * Saves Node Information. It consists of nodename, node description, latitude, longitude
	 * regulatory domain
	 */
	private void saveNodeInfo() {
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_NODE_INFO_START);
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_NODE_NAME, getNodeName());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_NODE_DESCRIPTION, getDescription());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_LATITUDE, getLatitude());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_LONGITUDE, getLongitude());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_PREFERRED_PARENT, getPreferedParent());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_REGULATORY_DOMAIN, Integer.toString(getRegulatoryDomain()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_COUNTRY_CODE, Integer.toString(getCountryCode()));
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_NODE_INFO_END);
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	/***
	 * Copies destination to caller of function. destination is ICopyable type
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(!(destination instanceof IConfiguration))
			return false;
		
		IConfiguration					destConfig					= (IConfiguration)destination;
		IInterfaceConfiguration 		destInterfaceConfigInfo 	= destConfig.getInterfaceConfiguration();
		IMeshConfiguration      		destMeshConfiguration   	= destConfig.getMeshConfiguration();
		INetworkConfiguration			destNetworkConfigInfo   	= destConfig.getNetworkConfiguration();
		IVlanConfiguration				destVlanConfigInfo			= destConfig.getVlanConfiguration();
		I80211eCategoryConfiguration  	dest80211eConfigInfo  		= destConfig.get80211eCategoryConfiguration();
		IACLConfiguration  				destACLConfigInfo 			= destConfig.getACLConfiguration();
		IEffistreamConfiguration		destEffistreamConfigInfo	= destConfig.getEffistreamConfiguration();
		ISipConfiguration				destSipConfigInfo			= destConfig.getSipConfiguration();
		
		destConfig.setHardwareModel(model);
		destConfig.setCountryCode(countryCode);
		destConfig.setDescription(description);
		destConfig.setFirmwareMajorVersion(firmwareMajorVersion);
		destConfig.setFirmwareMinorVersion(firmwareMinorVersion);
		destConfig.setFirmwareVariantVersion(firmwareVariantVersion);
		destConfig.setLatitude(latitude);
		destConfig.setLongitude(longitude);
		destConfig.setMacAddress(dsMacAddress);
		destConfig.setNodeName(nodeName);
		destConfig.setPreferedParent(preferredParent);
		destConfig.setRegulatoryDomain(regDomain);
		
		if(interfaceConfiguration.copyTo(destInterfaceConfigInfo) 	 == false)
			return false;
		if(meshConfiguration.copyTo(destMeshConfiguration) 		  	 == false)
			return false;
		if(networkConfiguration.copyTo(destNetworkConfigInfo)     	 == false)
			return false;
		if(vlanConfiguration.copyTo(destVlanConfigInfo)			  	 == false)
			return false;
		if(dot11eCategoryConfiguration.copyTo(dest80211eConfigInfo)  == false)
			return false;
		if(aclConfiguration.copyTo(destACLConfigInfo) 				 == false)
			return false;
		if(effistreamConfiguration.copyTo(destEffistreamConfigInfo)	 == false)
			return false;
		if(sipConfiguration.copyTo(destSipConfigInfo)				 == false)
			return false;
			
		return true;
	}

	/***
	 * Compares destination with caller of function. destination is IComparable type.
	 */
	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
	    short 	retFlag = IComparable.CONFIG_NOT_CHANGED;
	    short	ret;
	    
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}

		if(!(destination instanceof IConfiguration))
			return IComparable.CONFIG_NULL;
		
		IConfiguration					destConfig					= (IConfiguration)destination;
		INetworkConfiguration			destNetworkConfigInfo   	= destConfig.getNetworkConfiguration();
		IMeshConfiguration      		destMeshConfiguration   	= destConfig.getMeshConfiguration();
		IInterfaceConfiguration 		destInterfaceConfigInfo 	= destConfig.getInterfaceConfiguration();
		IVlanConfiguration				destVlanConfigInfo			= destConfig.getVlanConfiguration();
		I80211eCategoryConfiguration  	dest80211eConfigInfo  		= destConfig.get80211eCategoryConfiguration();
		IACLConfiguration  				destACLConfigInfo 			= destConfig.getACLConfiguration();
		IEffistreamConfiguration		destEffistreamConfigInfo	= destConfig.getEffistreamConfiguration();
		ISipConfiguration  				destSipConfigInfo 			= destConfig.getSipConfiguration();
		
		ret = compareGeneralInfo(destination, statusHandler);
	
		if(ret == IComparable.CONFIG_CHANGED){
		    retFlag = IComparable.CONFIG_CHANGED;
		}
		ret = networkConfiguration.compare(destNetworkConfigInfo, statusHandler);
		if(ret == IComparable.CONFIG_CHANGED){
		    retFlag = IComparable.CONFIG_CHANGED;
		}
		ret = meshConfiguration.compare(destMeshConfiguration, statusHandler);
		if(ret == IComparable.CONFIG_CHANGED){
		    retFlag = IComparable.CONFIG_CHANGED;
		}
		ret = interfaceConfiguration.compare(destInterfaceConfigInfo, statusHandler);
		if(ret == IComparable.CONFIG_CHANGED){
		    retFlag = IComparable.CONFIG_CHANGED;
		}
		ret = vlanConfiguration.compare(destVlanConfigInfo, statusHandler);
		if(ret == IComparable.CONFIG_CHANGED){
		    retFlag = IComparable.CONFIG_CHANGED;
		}
		ret = dot11eCategoryConfiguration.compare(dest80211eConfigInfo, statusHandler);
		if(ret == IComparable.CONFIG_CHANGED){
		    retFlag = IComparable.CONFIG_CHANGED;
		}
		ret = aclConfiguration.compare(destACLConfigInfo, statusHandler);
		if(ret == IComparable.CONFIG_CHANGED){
		    retFlag = IComparable.CONFIG_CHANGED;
		}
		ret = effistreamConfiguration.compare(destEffistreamConfigInfo, statusHandler);
		if(ret == IComparable.CONFIG_CHANGED) {
		    retFlag = IComparable.CONFIG_CHANGED;
		}
		ret = sipConfiguration.compare(destSipConfigInfo, statusHandler);
		if(ret == IComparable.CONFIG_CHANGED) {
		    retFlag = IComparable.CONFIG_CHANGED;
		}
		
		return retFlag;
	}

	/**
	 *Compares General information like hardwaremodel, software version, nodename, node description, latitude, longitude
	 * regulatory domain 
	 */
	private short compareGeneralInfo(IComparable destination, IConfigStatusHandler statusHandler) {
		
		boolean configChangedFlag;
		configChangedFlag	= false;
		IConfiguration iConfigDest	= (IConfiguration)destination;
		
		if((iConfigDest.getHardwareModel().equalsIgnoreCase(getHardwareModel())) == false) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChangedFlag	= true;
		}
		if((iConfigDest.getFirmwareVersion().equalsIgnoreCase(getFirmwareVersion())) == false) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChangedFlag	= true;
		}
		if((iConfigDest.getNodeName().equalsIgnoreCase(getNodeName())) == false) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChangedFlag	= true;
		}
		if((iConfigDest.getDescription().equalsIgnoreCase(getDescription())) ==  false) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChangedFlag	= true;
		}
		if((iConfigDest.getLatitude().equalsIgnoreCase(getLatitude())) ==  false) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChangedFlag	= true;
		}
		if((iConfigDest.getLongitude().equalsIgnoreCase(getLongitude())) == false) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChangedFlag	= true;
		}
		if((iConfigDest.getPreferedParent().equalsIgnoreCase(getPreferedParent())) == false) {
			statusHandler.packetChanged(PacketFactory.AP_CONFIGURATION_INFO);
			configChangedFlag	= true;
		}
		if(iConfigDest.getRegulatoryDomain() != getRegulatoryDomain()) {
			statusHandler.packetChanged(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET);
			configChangedFlag	= true;
		}
		if(iConfigDest.getCountryCode() != getCountryCode()) {
			statusHandler.packetChanged(PacketFactory.AP_REG_DOMAIN_CCODE_INFO_PACKET);
			configChangedFlag	= true;
		}
		
		if(configChangedFlag == true) {
			return IComparable.CONFIG_CHANGED ;
		}
		
		return IComparable.CONFIG_NOT_CHANGED ;
	}
	
	private boolean versionLessThan(short majorVersion) {
		return (this.firmwareMajorVersion < majorVersion) ?	true : false;
	}

	private boolean versionEqualTo(short majorVersion) {
		return (this.firmwareMajorVersion == majorVersion) ?	true : false;		
	}

	private boolean versionGreaterThan(short majorVersion) {
		return (this.firmwareMajorVersion > majorVersion) ?	true : false;
	}

	private boolean versionLessThan(short majorVersion,short minorVersion) {

		if(versionGreaterThan(majorVersion) == true)
			return false;
		
		if(versionEqualTo(majorVersion) == true) {
			return (this.firmwareMinorVersion < minorVersion) ?
				true : false;
		}

		return true;
	}

	private boolean versionEqualTo(short majorVersion,short minorVersion) {

		if(versionEqualTo(majorVersion) == false)
			return false;
			
		return (this.firmwareMinorVersion != minorVersion) ?
			false : true;
		
	}

	private boolean versionGreaterThan(short majorVersion,short minorVersion) {

		if(versionLessThan(majorVersion) == true)
			return false;
		
		if(versionEqualTo(majorVersion) == true) {
			return (this.firmwareMinorVersion > minorVersion) ?
				true : false;
		}

		return true;
	}
	
	private short getCorrectedVariant(short variant) {
		return (short) ((variant < 10) ? (variant * 10) : variant);
	}
	
	public boolean versionEqualTo(short major, short minor, short variant) {
		
		if(versionEqualTo(major, minor) == false)
			return false;
		
		short srcVariant 	= this.firmwareVariantVersion;
		srcVariant 			= getCorrectedVariant(srcVariant); 
		variant 			= getCorrectedVariant(variant);
		
		return (srcVariant != variant) ? false : true;
		
	}

	public boolean versionGreaterThan(short major, short minor, short variant) {

		if(versionLessThan(major,minor) == true)
			return false;
		
		if(versionEqualTo(major,minor) == true) {

			short srcVariant 	= this.firmwareVariantVersion;
			srcVariant 			= getCorrectedVariant(srcVariant); 
			variant 			= getCorrectedVariant(variant);
			
			return (srcVariant > variant) ? true : false;
		}
		
		return true;
		
	}

	public boolean versionLessThan(short major, short minor, short variant) {

		if(versionGreaterThan(major,minor) == true)
			return false;
		
		if(versionEqualTo(major,minor) == true) {
			short srcVariant 	= this.firmwareVariantVersion;
			srcVariant 			= getCorrectedVariant(srcVariant); 
			variant 			= getCorrectedVariant(variant);
			
			return (srcVariant < variant) ? true : false;
		}
	
		return true;
		
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.util.interfaces.IVersionInfo#versionGreaterThanEqualTo(short, short, short)
	 */
	public boolean versionGreaterThanEqualTo(short major, short minor, short variant) {
	
		if(versionLessThanEqualTo(major,minor) == true)
			return false;
		
		if(versionEqualTo(major,minor) == true) {

			short srcVariant 	= this.firmwareVariantVersion;
			srcVariant 			= getCorrectedVariant(srcVariant); 
			variant 			= getCorrectedVariant(variant);
			
			return (srcVariant >= variant) ? true : false;
		}
		
		return true;
	}

	/**
	 * @param major
	 * @param minor
	 * @return
	 */
	private boolean versionLessThanEqualTo(short majorVersion, short minorVersion) {
		
		if(versionGreaterThanEqualTo(majorVersion) == true)
			return false;
		
		if(versionEqualTo(majorVersion) == true) {
			return (this.firmwareMinorVersion <= minorVersion) ?
				true : false;
		}

		return true;
	
	}

	/**
	 * @param majorVersion
	 * @return
	 */
	private boolean versionGreaterThanEqualTo(short majorVersion) {
		return (this.firmwareMajorVersion >= majorVersion) ?	true : false;
	}
	
	/* (non-Javadoc)
	 * @see com.meshdynamics.util.interfaces.IVersionInfo#versionLessThanEqualTo(short, short, short)
	 */
	public boolean versionLessThanEqualTo(short major, short minor, short variant) {
		if(versionGreaterThanEqualTo(major,minor) == true)
			return false;
		
		if(versionEqualTo(major,minor) == true) {
			short srcVariant 	= this.firmwareVariantVersion;
			srcVariant 			= getCorrectedVariant(srcVariant); 
			variant 			= getCorrectedVariant(variant);
			
			return (srcVariant <= variant) ? true : false;
		}
	
		return true;
	}

	/**
	 * @param major
	 * @param minor
	 * @return
	 */
	private boolean versionGreaterThanEqualTo(short majorVersion, short minorVersion) {
		
		if(versionLessThanEqualTo(majorVersion) == true)
			return false;
		
		if(versionEqualTo(majorVersion) == true) {
			return (this.firmwareMinorVersion >= minorVersion) ?
				true : false;
		}

		return true;
	}

	/**
	 * @param majorVersion
	 * @return
	 */
	private boolean versionLessThanEqualTo(short majorVersion) {
		return (this.firmwareMajorVersion <= majorVersion) ?	true : false;
	}

	public short getFirmwareMajorVersion() {
		return firmwareMajorVersion;
	}

	public void setFirmwareMajorVersion(short firmwareMajorVersion) {
		this.firmwareMajorVersion = firmwareMajorVersion;
	}

	public short getFirmwareMinorVersion() {
		return firmwareMinorVersion;
	}

	public void setFirmwareMinorVersion(short firmwareMinorVersion) {
		this.firmwareMinorVersion = firmwareMinorVersion;
	}

	public short getFirmwareVariantVersion() {
		return firmwareVariantVersion;
	}

	public void setFirmwareVariantVersion(short firmwareVariantVersion) {
		this.firmwareVariantVersion = firmwareVariantVersion;
	}

	public String getFirmwareVersion() {
		return 	firmwareMajorVersion + "." +
				firmwareMinorVersion + "." +
				firmwareVariantVersion;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IConfiguration#isFipsEnabled()
	 */
	public boolean isFipsEnabled() {
		if(versionLessThan(IVersionInfo.MAJOR_VERSION_2,
				IVersionInfo.MINOR_VERSION_5, IVersionInfo.VARIANT_VERSION_34) == true) {
			return false;
		}
		return fipsEnabled;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IConfiguration#setFipsEnabled()
	 */
	public void setFipsEnabled() {
		
		if(versionLessThan(IVersionInfo.MAJOR_VERSION_2,
				IVersionInfo.MINOR_VERSION_5, IVersionInfo.VARIANT_VERSION_34) == true) {
			fipsEnabled	= false;
			return;
		}
		
		fipsEnabled	= ((regDomain & Mesh.FIPS_BIT_SET) == Mesh.FIPS_BIT_SET) ?
						true : false;
	
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		
		init();
		
		dot11eCategoryConfiguration.clearData();
		interfaceConfiguration.clearData();
		meshConfiguration.clearData();
		networkConfiguration.clearData();
		vlanConfiguration.clearData();
		aclConfiguration.clearData();
		effistreamConfiguration.clearData();
		sipConfiguration.clearData();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IConfiguration#setCountryCodeChanged()
	 */
	public void setCountryCodeChanged(boolean changed) {
		isCountryCodeChanged	= changed;
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IConfiguration#isCountryCodeChanged()
	 */
	public boolean isCountryCodeChanged() {
		return isCountryCodeChanged;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IConfiguration#getSipConfiguration()
	 */
	public ISipConfiguration getSipConfiguration() {
		return sipConfiguration;
	}

	public boolean supportsSIP() {
		return (versionGreaterThanEqualTo(IVersionInfo.MAJOR_VERSION_2, IVersionInfo.MINOR_VERSION_5,IVersionInfo.VARIANT_VERSION_74) == true) ?
				true : false;
	}

	@Override
	public boolean supportsP3M() {
		return (versionGreaterThanEqualTo(IVersionInfo.MAJOR_VERSION_2, IVersionInfo.MINOR_VERSION_5,IVersionInfo.VARIANT_VERSION_74) == true) ?
				true : false;
	}
}
