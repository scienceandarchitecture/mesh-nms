/**
 * MeshDynamics 
 * -------------- 
 * File     : InterfaceCustomChannelConfig.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 *  ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *  ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/
package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IChannelInfo;
import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.IInterfaceChannelConfiguration;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;


public class InterfaceCustomChannelConfig implements
		IInterfaceChannelConfiguration {

	private ChannelInfo[]				channelInfo;
	private int							channelInfoCount;
	private short 						antennaGain;
	private short						antennaPower;
	private	short 						channelBandwidth;
	private short 						ctl;
	
	public InterfaceCustomChannelConfig(){
		init();
	}
	
	private void init(){
		
		channelInfo 	 = null;
		channelInfoCount = 0;
		antennaGain  	 = 0;
		antennaPower	 = 30;
		channelBandwidth = 20;
		ctl				 = 0;
		
	}
	public short getAntennaGain() {
		return antennaGain;
	}

	public short getAntennaPower() {
		return antennaPower;
	}

	public short getChannelBandwidth() {
		return channelBandwidth;
	}

	public IChannelInfo getChannelInfo(int index) {
		return channelInfo[index];
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IInterfaceChannelConfiguration#getChannelInfoByChNumber(int)
	 */
	public IChannelInfo getChannelInfoByChNumber(int chNumber) {
		int index;
		
		for(index = 0; index < channelInfoCount; index++) {
			if(channelInfo[index].getChannelNumber() == chNumber){
				return channelInfo[index];
			}
		}
		
		return null;
	}
	
	public int getChannelInfoCount() {
		return channelInfoCount;
	}

	public short getCtl() {
		return ctl;
	}

	public void setAntennaGain(short antGain) {
		this.antennaGain = antGain;
	}

	public void setAntennaPower(short antPower) {
		this.antennaPower = antPower;
	}

	public void setChannelBandwidth(short bandWidth) {
		this.channelBandwidth = bandWidth;
	}

	public void setChannelInfoCount(int count) {
		channelInfoCount =  count;
		channelInfo 	 =	new ChannelInfo[count];
		for(int i=0; i<count; i++){
			channelInfo[i]	=  new ChannelInfo(); 
		}
	}

	public void setCtl(short ctl) {
		this.ctl = ctl;
	}

	/**
	 * @param configReader
	 * 
	 */
private boolean loadCustomChannelInfo(ConfigurationReader configReader) {
		
		int i = 0;
		while (configReader.getNextToken() == true){
			
			int currentTokenType	=	configReader.getCurrentTokenType();
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				
				if(configReader.getCurrentHeader()			==	IConfigurationIODefs.CONFIG_HEADER_CHANNEL_INFO_START){
					channelInfo[i].load(configReader);
					i++;
				}else if(configReader.getCurrentHeader()	==	IConfigurationIODefs.CONFIG_HEADER_CUSTOM_CHANNEL_INFO_END){
					return true;
				}
		
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		
		while (configReader.getNextToken() == true){
			
			int currentTokenType	=	configReader.getCurrentTokenType();
			
			if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
				
				if(configReader.getCurrentHeader()			==	IConfigurationIODefs.CONFIG_HEADER_CUSTOM_CHANNEL_CONFIG_END){
					return true;
				}else if(configReader.getCurrentHeader()	==	IConfigurationIODefs.CONFIG_HEADER_CUSTOM_CHANNEL_INFO_START){
					loadCustomChannelInfo(configReader);
				}
			}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
			
				String tokenValue	=	configReader.getCurrentTokenValue();
				if(configReader.getCurrentToken() 		== IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_BANDWIDTH){
					try{
						short bandWidth	 =	Short.parseShort(tokenValue.trim());
						setChannelBandwidth(bandWidth);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_CONFORMANCE){
					try{
						short ctl	 =	Short.parseShort(tokenValue.trim());
						setCtl(ctl);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_MAX_POWER){
					try{
						short antPower	 =	Short.parseShort(tokenValue.trim());
						setAntennaPower(antPower);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_MAX_ANTENNA_GAIN){
					try{
						short antGain  	 =	Short.parseShort(tokenValue.trim());
						setAntennaGain(antGain);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_COUNT){
					try{
						short channelCnt  	 =	Short.parseShort(tokenValue.trim());
						setChannelInfoCount(channelCnt);
					}catch(Exception e){
						e.printStackTrace();
						return false;
					}
				}
			}
		}
	return false;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_BANDWIDTH,Short.toString(getChannelBandwidth()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_CONFORMANCE,Short.toString(getCtl()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_MAX_POWER,Short.toString(getAntennaPower()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_MAX_ANTENNA_GAIN,Short.toString(getAntennaGain()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_CHANNEL_CONFIG_COUNT,Integer.toString(getChannelInfoCount()));
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_CUSTOM_CHANNEL_INFO_START);
		if(channelInfo == null) {
		    configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_CUSTOM_CHANNEL_INFO_END);
			return true;
		}
		for(int i=0; i < channelInfo.length ; i++){
				
			if(channelInfo[i] == null)
				continue;
			
			channelInfo[i].save(configWriter);
		}
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_CUSTOM_CHANNEL_INFO_END);
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof InterfaceCustomChannelConfig){
			
			((InterfaceCustomChannelConfig)destination).setChannelBandwidth(getChannelBandwidth());
			((InterfaceCustomChannelConfig)destination).setCtl(getCtl());
			((InterfaceCustomChannelConfig)destination).setAntennaPower(getAntennaPower());
			((InterfaceCustomChannelConfig)destination).setAntennaGain(getAntennaGain());
			
			int channelCount	=	getChannelInfoCount();
			((InterfaceCustomChannelConfig)destination).setChannelInfoCount(channelCount);
			
			for(int i=0; i<channelCount; i++){
				IChannelInfo destChannelInfo = (IChannelInfo)((InterfaceCustomChannelConfig)destination).getChannelInfo(i);
				channelInfo[i].copyTo(destChannelInfo);
			}
			
			return true;
		}
		return false;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		int	configChangedFlag;
		
		configChangedFlag	= 0;
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		IInterfaceChannelConfiguration iIntrfaceChConfig	= (IInterfaceChannelConfiguration)destination;
		
		if(iIntrfaceChConfig.getChannelBandwidth() != getChannelBandwidth()) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iIntrfaceChConfig.getCtl() != getCtl()) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iIntrfaceChConfig.getAntennaPower() != getAntennaPower()) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iIntrfaceChConfig.getAntennaGain() != getAntennaGain()) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iIntrfaceChConfig.getChannelInfoCount() != getChannelInfoCount()) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		
		if(channelInfo == null || channelInfo.length < 0) {
			return IComparable.CONFIG_NULL;
		}
			
		for(int i=0; i < channelInfoCount ; i++) {
			
			if(channelInfo[i] == null) {
				continue; 
			}
			int chNumber	= channelInfo[i].getChannelNumber();
			
			IChannelInfo destChannelInfo = (IChannelInfo)((InterfaceCustomChannelConfig)destination).getChannelInfoByChNumber(chNumber);
			if(destChannelInfo == null) {
				continue;
			}
			
			channelInfo[i].compare(destChannelInfo, statusHandler);
			configChangedFlag++;
			
		}
		
		if(configChangedFlag != channelInfoCount) {
			statusHandler.packetChanged(PacketFactory.RF_CUSTOM_CHANNEL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		} 
		
		return IComparable.CONFIG_NOT_CHANGED;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
	}

	

}
