/**
 * MeshDynamics 
 * -------------- 
 * File     : ACLInfo.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 2  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *   ----------------------------------------------------------------------------------
 * | 3  | Feb 01, 2007   | save added                  	     		  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.meshviewer.configuration.impl;


import com.meshdynamics.meshviewer.configuration.IACLInfo;
import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.MacAddress;

public class ACLInfo implements IACLInfo {
	
	private short		_80211eCategoryIndex;
	private	short		allowEntry;
	private short		_80211eCategoryEnabled;
	private MacAddress	staAddress;
	private	int			vlanTag;
	
	public ACLInfo() {
		init();
	}
	
	private void init(){
		_80211eCategoryIndex 	= 0;
		allowEntry				= 0;
		_80211eCategoryEnabled	= 0;
		staAddress				= new MacAddress();
		vlanTag					= 0;
	}
	public short get80211eCategoryIndex() {
		return _80211eCategoryIndex;
	}

	public short getAllow() {
		return allowEntry;
	}

	public short getEnable80211eCategory() {
		return _80211eCategoryEnabled;
	}

	public MacAddress getStaAddress() {
		return staAddress;
	}

	public int getVLANTag() {
		return vlanTag;
	}

	public void set80211eCategoryIndex(short category) {
		this._80211eCategoryIndex	= category;
	}

	public void setAllow(short allow) {
		this.allowEntry	= allow;
	}

	public void setEnable80211eCategory(short enabled) {
		this._80211eCategoryEnabled	= enabled;
	}

	public void setStaAddress(MacAddress staAddress) {
		this.staAddress.setBytes(staAddress.getBytes());
	}

	public void setVLANTag(int tag) {
		this.vlanTag	= tag;
	}


	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		try {
			
			while (configReader.getNextToken() == true){
			
				int currentTokenType	=	configReader.getCurrentTokenType();
				
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
					
					int currentHeader   =   configReader.getCurrentHeader(); 
					
					if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_ACL_INFO_END){
						return true;
					}
				}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
	
					String tokenValue	=	configReader.getCurrentTokenValue();
					if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_ACL_80211E_CATEGORY){
						try{
							short acl80211CatIndex = Short.parseShort(tokenValue.trim());
							set80211eCategoryIndex(acl80211CatIndex);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_ACL_80211E_ENABLED) {
						try{
							short aclEnable = Short.parseShort(tokenValue.trim());
							setEnable80211eCategory(aclEnable);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_ACL_ALLOW_CONNECTION){
						try{
							short allow = Short.parseShort(tokenValue.trim());
							setAllow(allow);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_ACL_STA_ADDRESS){
						MacAddress	staMac	=	new MacAddress();
						staMac.setBytes(tokenValue.trim());
						setStaAddress(staMac);
						
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_ACL_VLAN_TAG){
						try{
							int tag	 = Integer.parseInt(tokenValue.trim());
							setVLANTag(tag);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
						
					}
				}
		}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_ACL_INFO_START);
		
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_ACL_STA_ADDRESS,getStaAddress().toString());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_ACL_VLAN_TAG,Integer.toString(getVLANTag()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_ACL_ALLOW_CONNECTION,Short.toString(getAllow()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_ACL_80211E_ENABLED,Short.toString(getEnable80211eCategory()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_ACL_80211E_CATEGORY,Short.toString(get80211eCategoryIndex()));
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_ACL_INFO_END);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof IACLInfo){
		
			((IACLInfo)destination).setStaAddress(getStaAddress());
			((IACLInfo)destination).setVLANTag(getVLANTag());
			((IACLInfo)destination).setAllow(getAllow());
			((IACLInfo)destination).setEnable80211eCategory(getEnable80211eCategory());
			((IACLInfo)destination).set80211eCategoryIndex(get80211eCategoryIndex());
		
			return true;
		}
		return false;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		if(destination == null)
			return IComparable.CONFIG_NULL;
		
		IACLInfo iAclInfoDest	= (IACLInfo)destination;
		
		if((iAclInfoDest.getStaAddress().toString().equalsIgnoreCase(getStaAddress().toString())) == false) {
			statusHandler.packetChanged(PacketFactory.ACL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iAclInfoDest.getVLANTag() !=	getVLANTag()) {
			statusHandler.packetChanged(PacketFactory.ACL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iAclInfoDest.getAllow() != getAllow()) {
			statusHandler.packetChanged(PacketFactory.ACL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(	iAclInfoDest.getEnable80211eCategory() != getEnable80211eCategory()) {
			statusHandler.packetChanged(PacketFactory.ACL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(iAclInfoDest.get80211eCategoryIndex() != get80211eCategoryIndex()) {
			statusHandler.packetChanged(PacketFactory.ACL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}

		return IComparable.CONFIG_NOT_CHANGED;
	}

	public boolean equals(Object obj) {
		
		if((obj instanceof IACLInfo) == false) {
			return false;
		}
		
		IACLInfo aclInfo = (IACLInfo) obj;
		
		if(aclInfo.get80211eCategoryIndex() != this._80211eCategoryIndex) {
			return false;
		}
		if(aclInfo.getAllow() != this.allowEntry) {
			return false;
		}
		if(aclInfo.getEnable80211eCategory() != this._80211eCategoryEnabled) {
			return false;
		}
		if(aclInfo.getStaAddress().toString().equalsIgnoreCase(this.staAddress.toString()) == false) {
			return false;
		}
		if(aclInfo.getVLANTag() != this.vlanTag) {
			return false;
		}

		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
	}

}
