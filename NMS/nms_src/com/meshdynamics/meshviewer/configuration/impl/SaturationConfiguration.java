/**
 * MeshDynamics 
 * -------------- 
 * File     : SaturationConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Imran 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Imran      |
 * ----------------------------------------------------------------------------------*/
 

package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.ISaturationInfo;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.MacAddress;


public class SaturationConfiguration implements ISaturationInfo{

	private String 						ifName;
	private int 						channelCount;
	private String 						lastUpdateTime;
	private DLSaturationChannelInfo[] 	channelInfo;	
	private boolean						infoAvailable;				
	/**
	 * 
	 */
	public SaturationConfiguration() {
		init();
	}
	
	private void init() {

		ifName			= "";
		channelCount	= 0;
		lastUpdateTime	= "";
		channelInfo		= null;
		infoAvailable	= false;
		
	}
	
	public boolean load(ConfigurationReader configReader) {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean save(ConfigurationWriter configWriter) {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean copyTo(ICopyable destination) {
		// TODO Auto-generated method stub
		return true;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		// TODO Auto-generated method stub
		return IComparable.CONFIG_NOT_CHANGED;
	}

	public String getIfName() {
		return ifName;
	}

	public void setIfName(String ifName) {
		this.ifName	= ifName;
	}
	
	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime	= lastUpdateTime;
	}
	
	public int getChannelCount() {
		return channelCount;
	}

	public void setChannelCount(int chCount) {
		int index;
		
		this.channelCount	= chCount;
		this.channelInfo	=  new DLSaturationChannelInfo[this.channelCount];
		for(index = 0; index < this.channelCount; index++) {
			channelInfo[index]	= new DLSaturationChannelInfo();
		}
		
	}
	
	public int getChannel(int index) {
		return channelInfo[index].getChannel();
	}

	public void setChannel(int channel, int chIndex) {
		channelInfo[chIndex].setChannel(channel);
	}

	public long getChannelTxDuration(int channel) {
		return channelInfo[channel].getTxDuration();
	}

	public void setChannelTxDuration(long txDuration,int chIndex) {
		channelInfo[chIndex].setTxDuration(txDuration);
	}

	public long getChannelTotalDuration(int channel) {
		return channelInfo[channel].getTotalDuration();
	}

	public void setChannelTotalDuration(long totalDuration, int chIndex) {
		channelInfo[chIndex].setTotalDuration(totalDuration);
	}

	public long getChannelRetryDuration(int channel) {
		return channelInfo[channel].getRetryDuration();
	}

	public void setChannelRetryDuration(long retryDuration,  int chIndex) {
		channelInfo[chIndex].setRetryDuration(retryDuration);
	}

	public short getChannelAvgSignal(int channel) {
		return channelInfo[channel].getAvgSignal();
	}

	public void setChannelAvgSignal(short avgSignal, int chIndex) {
		channelInfo[chIndex].setAvgSignal(avgSignal);
		
	}

	public short getChannelMaxSignal(int channel) {
		return channelInfo[channel].getMaxSignal();
	}

	public void setChannelMaxSignal(short maxSignal, int chIndex) {
		channelInfo[chIndex].setMaxSignal(maxSignal);
		
	}
	public int getChannelAPCount(int channel) {
		return channelInfo[channel].getApCount();
	}

	public void setChannelAPCount(short apCount, int chIndex) {
		channelInfo[chIndex].setApCount(apCount);
	}
	
	public MacAddress getChannelAPBssid(int channelIndex, int apIndex) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].getApInfo();
		return info[apIndex].getBssid();		
	}

	public void setChannelAPBssid(int channelIndex, int apIndex, MacAddress apBssId) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].getApInfo();
		info[apIndex].setBssid(apBssId);	
		
	}

	public String getChannelAPEssid(int channelIndex, int apIndex) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].getApInfo();
		return info[apIndex].getEssid();		
	}

	public void setChannelAPEssid(int channelIndex, int apIndex, String apEssId) {
		DLSaturationAPInfo[] info 	= channelInfo[channelIndex].getApInfo();
		info[apIndex].setEssid(apEssId);
	}

	public short getChannelAPSignal(int channelIndex, int apIndex) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].getApInfo();
		return info[apIndex].getSignal();		
	}

	public void setChannelAPSignal(int channelIndex, int apIndex,short apSignal) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].getApInfo();
		info[apIndex].setSignal(apSignal);	
	}

	public long getChannelAPTxDuration(int channelIndex, int apIndex) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].getApInfo();
		return info[apIndex].getTxDuration();		
	}

	public void setChannelAPTxDuration(int channelIndex, int apIndex, long txDuration) {
		DLSaturationAPInfo[] info 	= channelInfo[channelIndex].getApInfo();
		info[apIndex].setTxDuration(txDuration);	
	}

	public long getChannelAPRetryDuration(int channelIndex, int apIndex) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].getApInfo();
		return info[apIndex].getRetryDuration();		
	}


	public void setChannelAPRetryDuration(int channelIndex, int apIndex, long apRetryDuration) {
		DLSaturationAPInfo[] info = channelInfo[channelIndex].getApInfo();
		info[apIndex].setRetryDuration(apRetryDuration);	
	}

	public void setSaturationInfoReceived(){
		infoAvailable	= true;
	}
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISaturationInfo#isAvailable()
	 */
	public boolean isAvailable() {
		return infoAvailable;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
	}
}
