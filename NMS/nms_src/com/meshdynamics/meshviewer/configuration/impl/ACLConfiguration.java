/**
 * MeshDynamics 
 * -------------- 
 * File     : ACLConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 *   ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added  	                			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------
 */

package com.meshdynamics.meshviewer.configuration.impl;

import java.util.Vector;

import com.meshdynamics.meshviewer.configuration.IACLConfiguration;
import com.meshdynamics.meshviewer.configuration.IACLInfo;
import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.MacAddress;

public class ACLConfiguration implements IACLConfiguration {

	private Vector<IACLInfo>	aclInfoHolder;
	private	int					aclCount;
	
	public ACLConfiguration() {
		aclCount 	= 0;
		aclInfoHolder	= new Vector<IACLInfo>();
	}
	
	public IACLInfo getACLInfo(int index) {
		
		return (IACLInfo)aclInfoHolder.get(index);
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IACLConfiguration#getACLInfo(com.meshdynamics.util.MacAddress, int, short, short)
	 */
	public IACLInfo getACLInfo(IACLInfo	srcAclInfo) {
		int	index;
		
		for(index = 0; index < aclCount; index++) {
			IACLInfo	aclInfo	= (IACLInfo)aclInfoHolder.get(index);
			if((aclInfo.getStaAddress().toString().equals(srcAclInfo.
					getStaAddress().toString()))												&&
					(aclInfo.getVLANTag() == srcAclInfo.getVLANTag())							&&
					(aclInfo.getEnable80211eCategory() == srcAclInfo.getEnable80211eCategory())	&&
					(aclInfo.get80211eCategoryIndex() == srcAclInfo.get80211eCategoryIndex())	&&
					(aclInfo.getAllow() == srcAclInfo.getAllow())) {
				
				return aclInfo;
			}
		}
		return null;
	}
	
	public int getCount() {
		return aclCount;
	}

	public void setCount(int count) {
		aclCount	= count;
		for(int i=0; i<count; i++) {
			IACLInfo	aclInfo	= new ACLInfo();
			aclInfoHolder.add(i,aclInfo);
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(java.io.InputStream)
	 */
	public boolean load(ConfigurationReader configReader) {
		
		try {
			
			while (configReader.getNextToken() == true){
			
				int currentTokenType	=	configReader.getCurrentTokenType();
				
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
					
					int currentHeader   =   configReader.getCurrentHeader(); 
					
					if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_ACL_CONFIG_END){
						return true;
					}else if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_ACL_INFO_START){
						for(int i=0;i<aclCount; i++){
							IACLInfo	aclInfo	  =	new ACLInfo();
							aclInfo.load(configReader);
							aclInfoHolder.add(i, aclInfo);
						}
					}
				}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
					
					String tokenValue	=	configReader.getCurrentTokenValue();
					if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_ACL_COUNT){
						try{
							int cnt = Integer.parseInt(tokenValue.trim());
							setCount(cnt);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return false;
		
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(java.io.OutputStream)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_ACL_CONFIG_START);
		
		if(aclInfoHolder.size() <= 0){
			configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_ACL_CONFIG_END);
			return false;
		}
			
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_ACL_COUNT, Integer.toString(getCount()));
		/***temp comment**/
		for(int i=0;i<aclCount;i++){
			IACLInfo	aclInfo	= (IACLInfo)aclInfoHolder.get(i);
			if(aclInfo	==	null)
				continue;
			
			aclInfo.save(configWriter);
		}
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_ACL_CONFIG_END);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof IACLConfiguration){
			
			int aclCnt = getCount();
			((IACLConfiguration)destination).setCount(aclCnt);
			
			for(int i=0; i<aclCnt; i++){
				
				IACLInfo destACLInfo = (IACLInfo)((IACLConfiguration)destination).getACLInfo(i);
				if(destACLInfo == null){
					continue;
				}
				
				IACLInfo	aclInfo	  = (IACLInfo)aclInfoHolder.get(i);
				if(aclInfo == null) {
					continue;
				}
				
				if(aclInfo.copyTo(destACLInfo) == false)
					return false;
			}
			return true;
		}
		return false;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
	
		int configChangedFlag;
		
		configChangedFlag	= 0;
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		IACLConfiguration iACLConfigDest	= (IACLConfiguration)destination;
		
		if(iACLConfigDest.getCount() != getCount()) {
			statusHandler.packetChanged(PacketFactory.ACL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		
		if(aclInfoHolder.size() <= 0) {
			return IComparable.CONFIG_NULL;
		}
		
		for(int i=0; i<aclCount; i++) {
			IACLInfo	srcAclInfo	= (IACLInfo)aclInfoHolder.get(i);
			if(srcAclInfo == null){
				continue;
			}
			IACLInfo	dstAclInfo	= (IACLInfo)((IACLConfiguration)destination).getACLInfo(srcAclInfo);
			if(dstAclInfo == null) {
				statusHandler.packetChanged(PacketFactory.ACL_INFO_PACKET);
				return IComparable.CONFIG_CHANGED;
			}
			srcAclInfo.compare(dstAclInfo, statusHandler);
			configChangedFlag++;
		}
		
		if(configChangedFlag != aclCount) {
			statusHandler.packetChanged(PacketFactory.ACL_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		} /* if one acl entry is deleted and new is added then above condition 
			will get true and flag for aclConfiguration changed is set*/ 
		
		return IComparable.CONFIG_NOT_CHANGED;
		
	}
	

	public IACLInfo addAclInfo(MacAddress staAddress) {
		ACLInfo aclInfo = new ACLInfo();
		aclInfo.setStaAddress(staAddress);
		aclInfoHolder.add(aclInfo);
		aclCount++;
		return aclInfo;
	}

	public void deleteAclInfo(IACLInfo matchACLInfo) {
		if(matchACLInfo == null) {
			return;
		}
		
		for(int i=0;i<aclCount;i++) {
			
			ACLInfo aclInfo = (ACLInfo) aclInfoHolder.get(i);
			if(aclInfo.equals(matchACLInfo) == true) {
				aclInfoHolder.remove(i);
				aclCount--;
				return;
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IACLConfiguration#containsAclEntry(com.meshdynamics.meshviewer.configuration.IACLInfo)
	 */
	public boolean containsAclEntry(IACLInfo srcAclInfo) {
		
		int	index;
		
		for(index = 0; index < aclCount; index++) {
			IACLInfo	aclInfo	= (IACLInfo)aclInfoHolder.get(index);
			if((aclInfo.getStaAddress().toString().equals(srcAclInfo.
					getStaAddress().toString()))												&&
					(aclInfo.getVLANTag() == srcAclInfo.getVLANTag())							&&
					(aclInfo.getEnable80211eCategory() == srcAclInfo.getEnable80211eCategory())	&&
					(aclInfo.get80211eCategoryIndex() == srcAclInfo.get80211eCategoryIndex())	&&
					(aclInfo.getAllow() == srcAclInfo.getAllow())) {
				
				return true;
			}
			
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		aclCount 	= 0;
		aclInfoHolder.clear();
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.IACLConfiguration#containsAclEntryWithStaAddr(com.meshdynamics.util.MacAddress)
	 */
	public boolean containsAclEntryWithStaAddr(MacAddress staAddr) {
		int	index;
		
		for(index = 0; index < aclCount; index++) {
			IACLInfo	aclInfo	= (IACLInfo)aclInfoHolder.get(index);
			if(aclInfo == null) {
				continue;
			}
			if(aclInfo.getStaAddress().toString().equals(
					staAddr.toString())) {
				return true;
			}
			
		}
		return false;
	}
	//get ACL Info by ACL MACADDRESS
	public IACLInfo aclEntryWithStaAddr(MacAddress staAddr) {
		int	index;		
		for(index = 0; index < aclCount; index++) {
			IACLInfo	aclInfo	= (IACLInfo)aclInfoHolder.get(index);
			if(aclInfo == null) {
				continue;
			}
			if(aclInfo.getStaAddress().toString().equals(
					staAddr.toString())) {
				return aclInfo;
			}
			
		}
		return null;
	}
}
