/**
 * MeshDynamics 
 * -------------- 
 * File     : C80211eCategoryInfo.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 5  | Apr 27, 2007   | compare modified                			  | Imran        |
 *  ----------------------------------------------------------------------------------
 * | 4  | Mar 20, 2007   | compare added                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 3  | Mar 20, 2007   | CopyTo added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 2  | Feb 01, 2007   | save added	                  			  | Imran	     |
 * ----------------------------------------------------------------------------------
 * | 1  | Feb 01, 2007   | Loading added                  			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 * | 0  | Feb 01, 2007   | Created                        			  | Abhijit      |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.meshviewer.configuration.impl;

import com.meshdynamics.meshviewer.configuration.I80211eCategoryInfo;
import com.meshdynamics.meshviewer.configuration.IComparable;
import com.meshdynamics.meshviewer.configuration.ICopyable;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationReader;
import com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter;
import com.meshdynamics.meshviewer.configuration.io.IConfigurationIODefs;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;


public class C80211eCategoryInfo implements I80211eCategoryInfo {

	private int		acwMax;
	private int		acwMin;
	
	private long    burstTime;
	
	private short   disableBackOff;
	private short   aifsn;
	
	private String 	name;
	
	public C80211eCategoryInfo(){
		init();
	}
	
	private void init(){
		
		acwMax			= 0;
		acwMin			= 0;
		aifsn   		= 0;
		burstTime		= 0;
		disableBackOff  = 0;
		name			= "";	
	}
	public int getACWMax() {
		return acwMax;
	}

	public int getACWMin() {
		// TODO Auto-generated method stub
		return acwMin;
	}

	public short getAIFSN() {
		return aifsn ;
	}

	public long getBurstTime() {
		return burstTime;
	}

	public short getDisableBackoff() {
		return disableBackOff;
	}

	public String getName() {
		return name;
	}

	public void setACWMax(int aCWMax) {
		acwMax	= aCWMax;
	}

	public void setACWMin(int aCWMin) {
		acwMin = aCWMin; 
	}

	public void setAIFSN(short aifsn) {
		this.aifsn = aifsn;
	}

	public void setBurstTime(long burstTime) {
		this.burstTime = burstTime;
	}

	public void setDisableBackoff(short disableBackoff) {
		this.disableBackOff = disableBackoff;
	}

	public void setName(String categoryName) {
		this.name = categoryName;
	}

	
	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#load(com.meshdynamics.meshviewer.configuration.io.ConfigurationReader)
	 */
	public boolean load(ConfigurationReader configReader) {
		try {
			
			while (configReader.getNextToken() == true){
			
				int currentTokenType	=	configReader.getCurrentTokenType();
				
				if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_HEADER){
					
					int currentHeader   =   configReader.getCurrentHeader(); 
					
					if(currentHeader == IConfigurationIODefs.CONFIG_HEADER_80211E_CATEGORY_INFO_END){
						return true;
					}
				}else if(currentTokenType		==	IConfigurationIODefs.TOKEN_TYPE_KEY_VALUE){
					String tokenValue	=	configReader.getCurrentTokenValue();
					if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_ACWMAX){
						try{
							int acwMax = Integer.parseInt(tokenValue.trim());
							setACWMax(acwMax);
								
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_ACWMIN){
						try{
							int acwMin = Integer.parseInt(tokenValue.trim());
							setACWMin(acwMin);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_AIFSN){
						try{
							short aifsn = Short.parseShort(tokenValue.trim());
							setAIFSN(aifsn);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_BURST_TIME){
						try{
							long burstTime = Long.parseLong(tokenValue.trim());
							setBurstTime(burstTime);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_DISABLE_BACKOFF){
						try{
							short disableBackOff = Short.parseShort(tokenValue.trim());
							setDisableBackoff(disableBackOff);
						}catch(Exception e){
							e.printStackTrace();
							return false;
						}
						
					}else if(configReader.getCurrentToken() == IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_NAME){
						setName(tokenValue);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#save(com.meshdynamics.meshviewer.configuration.io.ConfigurationWriter)
	 */
	public boolean save(ConfigurationWriter configWriter) {
		
		if(configWriter == null)
			return false;
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_80211E_CATEGORY_INFO_START);
		
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_NAME, getName());
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_BURST_TIME, Long.toString(getBurstTime()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_ACWMIN, Integer.toString(getACWMin()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_ACWMAX, Integer.toString(getACWMax()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_AIFSN, Short.toString(getAIFSN()));
		configWriter.writeEntry(IConfigurationIODefs.CONFIG_KEY_80211E_CATEGORY_DISABLE_BACKOFF, Short.toString(getDisableBackoff()));
		
		configWriter.writeHeader(IConfigurationIODefs.CONFIG_HEADER_80211E_CATEGORY_INFO_END);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.util.interfaces.ICopyable#copyTo(com.meshdynamics.meshviewer.util.interfaces.ICopyable)
	 */
	public boolean copyTo(ICopyable destination) {
		
		if(destination == null)
			return false;
		
		if(destination instanceof I80211eCategoryInfo){
			
			((I80211eCategoryInfo)destination).setName(getName());
			((I80211eCategoryInfo)destination).setBurstTime(getBurstTime());
			((I80211eCategoryInfo)destination).setACWMin(getACWMin());
			((I80211eCategoryInfo)destination).setACWMax(getACWMax());
			((I80211eCategoryInfo)destination).setAIFSN(getAIFSN());
			((I80211eCategoryInfo)destination).setDisableBackoff(getDisableBackoff());
			
			return true;
		}
		return false;
	}

	public short compare(IComparable destination, IConfigStatusHandler statusHandler) {
		
		if(destination == null) {
			return IComparable.CONFIG_NULL;
		}
		
		I80211eCategoryInfo	i80211eCatgInfo	= (I80211eCategoryInfo)destination;
	
		if((i80211eCatgInfo.getName().equalsIgnoreCase(getName())) == false) {
			statusHandler.packetChanged(PacketFactory.DOT11E_CATEGORY_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(i80211eCatgInfo.getBurstTime() != getBurstTime()) {
			statusHandler.packetChanged(PacketFactory.DOT11E_CATEGORY_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(i80211eCatgInfo.getACWMin() != getACWMin()) {
			statusHandler.packetChanged(PacketFactory.DOT11E_CATEGORY_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(i80211eCatgInfo.getACWMax() != getACWMax()) {
			statusHandler.packetChanged(PacketFactory.DOT11E_CATEGORY_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(i80211eCatgInfo.getAIFSN() != getAIFSN()) {
			statusHandler.packetChanged(PacketFactory.DOT11E_CATEGORY_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		if(i80211eCatgInfo.getDisableBackoff() != getDisableBackoff()) {
			statusHandler.packetChanged(PacketFactory.DOT11E_CATEGORY_INFO_PACKET);
			return IComparable.CONFIG_CHANGED;
		}
		
		return IComparable.CONFIG_NOT_CHANGED;
	
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.configuration.ISerializableConfiguration#clearData()
	 */
	public void clearData() {
		init();
	}
	
}
