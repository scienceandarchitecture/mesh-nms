package com.meshdynamics.meshviewer.configuration;

import com.meshdynamics.util.IpAddress;

public interface IRadiusConfiguration extends ISerializableConfiguration, ICopyable, IComparable{
	
	public static final int WPA_PERSONAL_MODE_WPA	= 0;
	public static final int WPA_PERSONAL_MODE_WPA2	= 1;
	
	public short 		getCipherCCMP();
	public void 		setCipherCCMP(short ccmp);
	
	public short 		getCipherTKIP();
	public void 		setCipherTKIP(short tkip);
	
	public int 			getGroupKeyRenewal();
	public void			setGroupKeyRenewal(int grpKeyRenew);
	
	public IpAddress 	getServerIPAddress();
	public void		 	setServerIPAddress(IpAddress ip);
	
	public long 		getPort();
	public void 		setPort(long port);
	
	public String 		getKey();
	public void 		setKey(String key);
	
	public short 		getMode();
	public void 		setMode(short mode);	
	
	public short 		getWPAEnterpriseDecidesVlan();
	public void			setWPAEnterpriseDecidesVlan(short decision);
	
}
