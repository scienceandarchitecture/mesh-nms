/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ChannelListInfo.java
 * Comments : 
 * Created  : Apr 26, 2006
 * Author   : Mithil
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  2  |Jun 28, 2006 | ccode xml changes						  		 | Mithil |
 * -----------------------------------------------------------------------------------
 * |  1  |Jun 14, 2006 | All Models included	                         | Mithil |
 * --------------------------------------------------------------------------------
 * |  0  |Apr 26, 2006 | Created                                         | Mithil |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.util;



public class ChannelListInfo {
	
	private String		interfaceName;
	private String 		channels;
	private short[]		channelList;
	
	public ChannelListInfo(){
		interfaceName = new String();
		channels 	  = new String();
	}

	
	public short[] getChannels() {
		return channelList;
	}

	public String getStrChannels() {
		return channels;
	}

	public String getInterfaceName() {
		return interfaceName;
	}


	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}


	public void setChannels(String channels) {
		this.channels = channels;

		if(channels == null)
			return;
		if(channels.length() <= 0) 
			return;
		
        String strChannels[]	= channels.split(",");
		channelList				= new short[strChannels.length];
		for(int dl=0;dl<channelList.length;dl++){
			channelList[dl] = Short.parseShort(strChannels[dl]);
		}
		
	}
}
