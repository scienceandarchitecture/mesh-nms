/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamRule.java
 * Comments : 
 * Created  : Feb 23, 2007
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author   |
 * ----------------------------------------------------------------------------------
 * |  0  | Feb 23, 2007 | Created                                         |  Bindu   |
 * ----------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.util;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class EffistreamCriteria {
	private final static String		TAG_VALUE			= "value";
	private final static String		TAG_LABEL			= "label";

	public int 				matchId;	
	public Field[]			fields;
	
	public class Field {
		
		private final static String		SEPERATOR_COMMA		= ",";
		private final static String		SEPERATOR_HYPHEN	= "-";

		public class KeyValuePair {
			public String key;
			public String value;
			public KeyValuePair(String key, String value) {
				this.key	= key;
				this.value	= value;
			}
		};
		
		public String 			uiLabel;
		public KeyValuePair[]	kvPairs; 	//KeyValue Pairs
		
		public Field(Node xmlConfigNode) {
			
			String 		commaSeperatedPairs;
			String[] 	pairs;
			String[]	keyValuePair;
			
			uiLabel 			= xmlConfigNode.getAttributes().getNamedItem(TAG_LABEL).getNodeValue();
			commaSeperatedPairs	= xmlConfigNode.getAttributes().getNamedItem(TAG_VALUE).getNodeValue();
			pairs 				= commaSeperatedPairs.split(SEPERATOR_COMMA);
			
			kvPairs				= new KeyValuePair[pairs.length];
			
			for(int i = 0; i < pairs.length; i++) {
				 keyValuePair 	= pairs[i].split(SEPERATOR_HYPHEN);
				 kvPairs[i]		= new KeyValuePair(keyValuePair[0], keyValuePair[1]);
			}		
		}
		
	}
	
	public EffistreamCriteria(Node xmlConfigNode) {
		String 		strMatchId;
		int 		fieldsCount;
		int 		nodeListCount;
		int			fieldIndex;
		int 		i;
		NodeList 	xmlConfigNodeList;		
		
		fieldsCount = 0;
		fields 		= null;		
		strMatchId 	= xmlConfigNode.getAttributes().getNamedItem(TAG_VALUE).getNodeValue();
		matchId 	= Integer.parseInt(strMatchId);	
		
		/**
		 * The child nodes are not verified to be optionbox elements.
		 * This is OK right now since that is the only item expected.
		 * In future if more items are added at this level, the following
		 * code will need to be modified.
		 * -- Bindu
		 */
		
		xmlConfigNodeList 	= xmlConfigNode.getChildNodes();
		nodeListCount		= xmlConfigNodeList.getLength();
		
		if(nodeListCount <= 0)
			return;		
		
		for(i = 0; i < nodeListCount; i++) {
			
			Node xmlConfigChildNode = xmlConfigNodeList.item(i);
			
			if(xmlConfigChildNode.getNodeType() == Node.TEXT_NODE)
				continue;
			
			++fieldsCount;
		}
		
		fields 		= new Field[fieldsCount];
		fieldIndex 	= 0;
		
		for(i = 0; i < nodeListCount; i++) {
			
			Node xmlConfigChildNode = xmlConfigNodeList.item(i);
			
			if(xmlConfigChildNode.getNodeType() == Node.TEXT_NODE)
				continue;
						
			fields[fieldIndex] = new Field(xmlConfigChildNode);
			fieldIndex++;
		}		
	}
}
