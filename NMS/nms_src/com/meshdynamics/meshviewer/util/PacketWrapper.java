/**
 * MeshDynamics 
 * -------------- 
 * File     : PacketWrapper.java
 * Comments : 
 * Created  : Jun 29, 2006
 * Author   : Abhijit Ayarekar 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * |  0  |Jun 29, 2006  |  Created	                                  	 | Abhijit   |
 * ----------------------------------------------------------------------------------
 * 
 */

package com.meshdynamics.meshviewer.util;

public class PacketWrapper {

    public static final int PACKET_TYPE_IMCP		= 1;
    
    public static final int PACKET_FLAGS_NONE		= 1;
    public static final int PACKET_FLAGS_ENCRYPTED	= 2;
    
    private Object 	packet;
    private int 	packetType;
    private int 	packetFlags;
    
    /**
     * 
     */
    public PacketWrapper() {
        super();
        packet 		= null;
        packetType	= 0;
        packetFlags	= 1;
    }

    /**
     * @return Returns the packetType.
     */
    public int getPacketType() {
        return packetType;
    }
    /**
     * @param packetType The packetType to set.
     */
    public void setPacketType(int packetType) {
        this.packetType = packetType;
    }
    /**
     * @return Returns the packetFlags.
     */
    public int getPacketFlags() {
        return packetFlags;
    }
    /**
     * @param packetFlags The packetFlags to set.
     */
    public void setPacketFlags(int packetFlags) {
        this.packetFlags = packetFlags;
    }
    /**
     * @return Returns the packet.
     */
    public Object getPacket() {
        return packet;
    }
    /**
     * @param packet The packet to set.
     */
    public void setPacket(Object packet,int packetType,int packetFlags) {
        this.packet	 		= packet;
        this.packetFlags 	= packetFlags;
        this.packetType	 	= packetType;
    }
}
