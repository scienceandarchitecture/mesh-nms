/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : StaSignalEntry.java
 * Comments : Added for Heartbeat 2 packet
 * Created  : Oct 19, 2005
 * Author   : Mithil Wane
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 19, 2005 | Created                                         | Mithil |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.util;

import com.meshdynamics.util.MacAddress;

public class StaSignalEntry {
	public MacAddress			macAddress;
	public short 				signal;
	public long 			 	dwnlinkTransmitRate;
	public StaSignalEntry(){
		macAddress 			= new MacAddress();
		dwnlinkTransmitRate	= 0;	
		signal 				= 0;
	}
}
