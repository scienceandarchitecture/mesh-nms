/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : CustomChannelsData
 * Comments : Holds data for RF Editor
 * Created  : Dec 13, 2006
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Imran   |
 * --------------------------------------------------------------------------------
 * |  3  |Jan 23, 2007 | dataChangeFlag is added            			 |Abhishek  |
 * --------------------------------------------------------------------------------
 * |  2  |Dec 16, 2006 | Initialization added in constructor			 |Abhijit  |
 * --------------------------------------------------------------------------------
 * |  1  |Dec 14, 2006 | Added removeChannelInfo						 |Abhishek |
 * --------------------------------------------------------------------------------
 * |  0  |Dec 13, 2006 | Created                                         | Imran   |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.util;

import java.util.Vector;

import com.meshdynamics.meshviewer.mesh.Mesh;

public class CustomChannelsData {

	private int					interfaceIndex;	
	private short	 	 		channelBandwith;
	private short 		 		ctl;
	private boolean		 		dfs;
	private short 		 		antennaPower;
	private short		 		antennaGain;
	private Vector<CustomChannelInfo> channels ; 
	private boolean				dataChangeFlag;
	
	public CustomChannelsData() {
		super();
		channels 	 	= new Vector<CustomChannelInfo>();
		antennaGain  	=	0;
		antennaPower 	=  Mesh.DEFAULT_ANTENNA_POWER;
		channelBandwith = 20;
		channels.setSize(0);
		dataChangeFlag	= false;	
	}
	
	public short getChannelWidth() {
		return channelBandwith;
	}
	
	public void setChannelBandwith(short channelBandwith) {
		this.channelBandwith = (short)channelBandwith;
	}
	
	public short getCtl() {
		return ctl;
	}
	
	public void setCtl(short ctl) {
		this.ctl = ctl;
	}
	
	public boolean getDfs() {
		return dfs;
	}
	public void setDfs(boolean dfs) {
		this.dfs = dfs;
	}
	public short getAntennaPower() {
		return antennaPower;
	}
	
	public void setAntennaPower(short antennaPower) {
		this.antennaPower = antennaPower;
	}
	public short getAntennaGain() {
		return antennaGain;
	}
	
	public void setMaxAntennaGain(short antennaGain) {
		this.antennaGain = antennaGain;
	}
	
	public CustomChannelInfo getChannel(int index){
		return (CustomChannelInfo)channels.get(index);
	}
	
	public void setCustomChannelInfo(CustomChannelInfo customChInfo){
		if(customChInfo == null)
		{
			return;
		}
		channels.add(customChInfo);
	}
	
	/**
	 * @return Returns the interfaceIndex.
	 */
	
	public int getInterfaceIndex() {
		return interfaceIndex;
	}
	
	/**
	 * @param interfaceIndex The interfaceIndex to set.
	 */
	
	public void setInterfaceIndex(int interfaceIndex) {
		this.interfaceIndex = interfaceIndex;
	}
	
	/**
	 * @return Returns the channels.
	 */
	
	public int getChannelCount() {
		return channels.size();
	}
	
	/**
	 * @param channels The channels to set.
	 */
	
	public void setChannelCount(int count) {
		//this.channels.setSize(count);
	}
	
	/*
	 * 
	 */
	
	public CustomChannelInfo getChannelInfo(int index){
		if(index < 0 || index > channels.size())
			return null;
		
		return (CustomChannelInfo)channels.get(index); 
	}
	
	/*
	 * Adding channel information to vector
	 */
	
	public void setChannelInfo(int index, CustomChannelInfo channelInfo){
		if(index < 0 || index > channels.size())
			return;
		
		channels.add(channelInfo); 
	}
	
	public void removeChannelInfo(int chNo)
	{
		for(int i=0;i<channels.size();i++ ) {
			CustomChannelInfo obj = (CustomChannelInfo)channels.get(i);
			if(obj == null)
				return;
			if(obj.getChannelNo() == chNo) {
				channels.remove(i);
				break;
			}
		}
	}
	
	/**
	 * @return Returns the dataChangeFlag.
	 */
	public boolean isDataChangeFlag() {
		return dataChangeFlag;
	}
	/**
	 * @param dataChangeFlag The dataChangeFlag to set.
	 */
	public void setDataChangeFlag(boolean dataChangeFlag) {
		this.dataChangeFlag = dataChangeFlag;
	}
}