/*
 * Created on Apr 10, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer.util;

/**
 * @author Administrator
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CountryCodeComboHelper {

	private int					regDomain;
	private String				hardwareModel;
	private int					countryCode;
	private	boolean				countryCodeChanged;
	
	public CountryCodeComboHelper() {
		
		regDomain			= 0;
		hardwareModel		= "";
		countryCode			= 0;
		countryCodeChanged	= false;
	}
	
	/**
	 * @return Returns the countryCode.
	 */
	public int getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode The countryCode to set.
	 */
	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return Returns the hardwareModel.
	 */
	public String getHardwareModel() {
		return hardwareModel;
	}
	/**
	 * @param hardwareModel The hardwareModel to set.
	 */
	public void setHardwareModel(String hardwareModel) {
		this.hardwareModel = hardwareModel;
	}
	/**
	 * @return Returns the regDomain.
	 */
	public int getRegDomain() {
		return regDomain;
	}
	/**
	 * @param regDomain The regDomain to set.
	 */
	public void setRegDomain(int regDomain) {
		this.regDomain = regDomain;
	}
	public boolean isCountryCodeChanged() {
		return countryCodeChanged;
	}
	public void setCountryCodeChanged(boolean countryCodeChanged) {
		this.countryCodeChanged = countryCodeChanged;
	}
}
