
package com.meshdynamics.meshviewer.util.interfaces;

public interface IVersionInfo {

	public static final short	MAJOR_VERSION_2		= 2;
	
	public static final short	MINOR_VERSION_3		= 3;
	public static final short	MINOR_VERSION_4		= 4;
	public static final short	MINOR_VERSION_5		= 5;
	
	public static final short	VARIANT_VERSION_1	= 1;
	public static final short	VARIANT_VERSION_7	= 7;
	public static final short	VARIANT_VERSION_26	= 26;
	public static final short	VARIANT_VERSION_28	= 28;
	public static final short	VARIANT_VERSION_30	= 30;
	public static final short	VARIANT_VERSION_33	= 33;
	public static final short	VARIANT_VERSION_34	= 34;
	public static final short	VARIANT_VERSION_35	= 35;
	public static final short	VARIANT_VERSION_39	= 39;
	public static final short	VARIANT_VERSION_40	= 40;
	public static final short	VARIANT_VERSION_50	= 50;
	public static final short	VARIANT_VERSION_65	= 65;
	public static final short	VARIANT_VERSION_70	= 70;
	public static final short	VARIANT_VERSION_71	= 71;
	public static final short	VARIANT_VERSION_74	= 74;
	public static final short	VARIANT_VERSION_94	= 94;
	public static final short	VARIANT_VERSION_98	= 98;	

	public boolean versionGreaterThan		(short major,short minor,short variant);
	public boolean versionEqualTo			(short major,short minor,short variant);
	public boolean versionLessThan			(short major,short minor,short variant);
	public boolean versionGreaterThanEqualTo(short major,short minor,short variant);
	public boolean versionLessThanEqualTo	(short major,short minor,short variant);
	
	public boolean supportsSIP				();
	public boolean supportsP3M				();
}
