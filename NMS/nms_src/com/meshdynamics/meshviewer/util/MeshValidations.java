/**
 * MeshDynamics 
 * -------------- 
 * File     : MeshValidations.java
 * Comments : 
 * Created  : Feb 10, 2005
 * Author   : Sneha Puranam
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * |  8  |Mar 30, 2007   |  isLong modified for not accepting -0      | Imran        |
 * ----------------------------------------------------------------------------------
 * |  7  |Oct 17, 2006    |  Group Key renewal validations             | Imran        |
 * ----------------------------------------------------------------------------------
 * |  6  |Oct 11, 2006   | IP Address validations bug fix             |   Bindu      |
 * --------------------------------------------------------------------------------
 * |  5  |Jun 30, 2006   | MacAddress validations bug fix             |Abhijit       |
 * --------------------------------------------------------------------------------
 * |  4  |Jun 21, 2006   | MacAddress validations                     |Mithil        |
 * --------------------------------------------------------------------------------
 * |  3  |Jun 13, 2006   | MacAddress validations                     |Mithil        |
 * --------------------------------------------------------------------------------
 * |  2  |Aug 25, 2005   | validation isIntegerGreaterThanZero added  | Abhijit      |
 * ----------------------------------------------------------------------------------
 * |  1  |Apr 26, 2005   | validation corrected in isHex()            | Bindu        |
 * ----------------------------------------------------------------------------------
 * |  0  |Feb 10, 2005   | Created	                                  | Sneha        |
 * ----------------------------------------------------------------------------------
 */

package com.meshdynamics.meshviewer.util;

import java.util.StringTokenizer;


public class MeshValidations { 
	
	public static boolean isShort(String value){
		if(value.equals(""))
			return false;
		try{
			short temp = Short.parseShort(value);
			if(temp<0)
				return false;
		}catch(NumberFormatException e){
			return false;		
		}
		return true;
		
	}
	
	public static boolean isLong(String value) {
		if(value.equals(""))
			return false;
		if(value.equals("-0"))
			return false; 
		try{
			long temp = Long.parseLong(value);
			if(temp<0)
				return false;
		}catch(NumberFormatException e){
			return false;
					
		}
		return true;
	}
	
	public static boolean isInt(String value){
		if(value.equals(""))
			return false;
		if(value.equals("-0"))
			return false; 
		try{
			int temp = Integer.parseInt(value);
			if(temp<0)
				return false;
		}catch(NumberFormatException e){
			return false;
					
		}
		return true;
	}
	
	public static boolean isInt1(String value){
		if(value.equals(""))
			return false;
		try{
			Integer.parseInt(value);
		}catch(NumberFormatException e){
			return false;
					
		}
		return true;
	}
	

	public static boolean isMacAddress(String value){
		
	    if(value.equals(""))
			return false;
		
		if(value.length() != 17)
		    return false;
		
		StringTokenizer st = new StringTokenizer(value,":");
		if(st.countTokens()!=6)
			return false;
		else{
			while(st.hasMoreElements()){
				String tok = st.nextToken();
				if(tok.trim().length() != 2 || MeshValidations.isHex(tok.trim()) == false){
					return false;
				}
			}
		}
		return true;		
	}
	
	public static boolean isIpAddress(String value){
		if(value.equals(""))
			return false;
		StringTokenizer st = new StringTokenizer(value,".");
		
		if(st.countTokens()!=4)
			return false;
		while(st.hasMoreTokens()){
			try{
				String token=st.nextToken();
				if(token.equals("-0"))
					return false;
				int temp= (short)Integer.parseInt(token);
				if((temp<0) || (temp>255))
					return false;
			}catch(NumberFormatException e){
				return false;
	 		}
   		}
		return true;		
	}
	
	public static boolean istokenized(String value,String token,int no){
		if(value.equals(""))
			return false;
		StringTokenizer st = new StringTokenizer(value,token);
		if(st.countTokens()!=no)
			return false;
		return true;
	}
	
	public static boolean isString(String value){
		if(value.equals(""))
			return false;
		return true;
	}
	
	public static boolean isHex (String value){
		if(value.equals(""))
			return false;
		 for (int i = 0; i < value.length(); i++)
		    { 		       
		        char c = value.charAt(i);
		        
		        if( ( ( (c >= 'a') && (c <= 'f') )   ||
		        	  ( (c >= 'A') && (c <= 'F') ) ) || 
					  ( (c >= '0') && (c <= '9') ) )  
		        	continue;
		        else	
		        	return false;		        		       
		    }		    
		    return true;		
	}
	
	
	public static boolean isIntegerInRange(int num, int min, int max ){			    
		if(num>=min && num<=max)
			return true;
		return false;
	}
	
	
	public static boolean isAlphanumeric(String value){
		if(value.equals(""))
			return false;
		 for (int i = 0; i < value.length(); i++)
		    {  		       
		        char c = value.charAt(i);
		        
		        if( (((c >= 'a') && (c <= 'z')) ||((c >= 'A') && (c <= 'Z'))) || ((c >= '0') && (c <= '9')))  
		        	continue;
		        else
		        	return false;		          
		    }
		return true;
	}
	
	/**
	 * @param i
	 * @return
	 */
	public static boolean isIntegerGreaterThanZero(int num) {
		if(num > 0)
			return true;
		
		return false;
	}
}


