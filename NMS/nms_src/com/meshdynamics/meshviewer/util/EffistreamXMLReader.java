/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : EffistreamXMLReader.java
 * Comments : 
 * Created  : Feb 23, 2007
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author   |
 * ----------------------------------------------------------------------------------
 * |  0  | Feb 23, 2007 | Created                                         |  Bindu   |
 * ----------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.util;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class EffistreamXMLReader {
	public final static String		TAG_MATCH_ID		= "match_id";
	
	public EffistreamCriteria[]	topLevelCriterias;

	public EffistreamXMLReader(String fileName) {
		
		Document doc;
		
		topLevelCriterias	= null;
		
		doc					= parseXML(fileName);
		
		if(doc != null) 
			processXML(doc);
		
	}
	
	private Document parseXML(String fileName) {
		
		Document	doc;
		
		doc = null;
		
		try{
			DocumentBuilderFactory	dbf 		= DocumentBuilderFactory.newInstance();
			DocumentBuilder 		docBuilder 	= dbf.newDocumentBuilder();
			doc			 						= docBuilder.parse(new File(fileName));
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return doc;
	}
	
	private void processXML(Document doc) {
		
		int						listCount;
		NodeList				list;

		list 		= doc.getElementsByTagName(TAG_MATCH_ID);
		listCount 	= list.getLength();
		
		if(listCount <= 0) {
			return;
		}
		
		topLevelCriterias = new EffistreamCriteria[listCount];
		
		for(int i = 0; i < listCount; i++) {
			
			Node matchId = list.item(i);
			
			if(matchId == null) {
				continue;
			}
			
			topLevelCriterias[i] = new EffistreamCriteria(matchId);
			
		}	
	}	
}
