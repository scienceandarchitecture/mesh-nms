/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MACAddress.java
 * Comments : 
 * Created  : Sep 8, 2005
 * Author   : Abhijit Ayarekar
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |Dec 11,2006  | CheckerFlag is added to class StaEntry          |Abhishek|
 * --------------------------------------------------------------------------------
 * |  0  |Sep 8, 2005  | Created                                         | Abhijit|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.util;

import com.meshdynamics.util.DateTime;
import com.meshdynamics.util.MacAddress;



public class StaEntry {
	
	public MacAddress	macAddress;
	public String 		associationTime;
	public boolean      checkerFlag;
	
	public StaEntry(){
		macAddress 		= null;
		associationTime	= DateTime.getDateTime();
		checkerFlag		= false;	
		
	}

	
	/**
	 * @param checkerFlag The checkerFlag to set.
	 */
	public void setCheckerFlag(boolean checkerFlag) {
		this.checkerFlag = checkerFlag;
	}
	/**
	 * @return Returns the checkerFlag.
	 */
	public boolean isCheckerFlag() {
		return checkerFlag;
	}
}
