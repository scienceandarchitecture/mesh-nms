/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : NetworkSort.java
 * Comments : 
 * Created  : Jan 12, 2005
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  2  |May 28,2007  |heartbeat references removed             		 |Abhishek|
 * --------------------------------------------------------------------------------
 * |  1  |Apr 17, 2005 | ArrayIndex out of bounds sortAPNetwork cleared  | Mithil |
 * --------------------------------------------------------------------------------
 * |  0  |Jan 12, 2005 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.util;

import java.util.Iterator;
import java.util.Vector;

import com.meshdynamics.meshviewer.configuration.IRuntimeConfiguration;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;

public class NetworkSort {
    
    public static Vector<AccessPoint> sortAPNetwork(Vector<AccessPoint> aps) {
        Vector<AccessPoint> sortedAPs 	= new Vector<AccessPoint>();
        int 	i			= 0;
        while(!aps.isEmpty() && i < aps.size()) {
            AccessPoint ap = (AccessPoint)aps.get(i);
            if(ap.getStatus() == AccessPoint.STATUS_ALIVE) {
                if(ap.isRoot() == true) {
                    aps.remove(i);
                    getChildAPs(aps, ap, sortedAPs);
                    i = 0;
                } else {
                    i++;
                }
            }
        }
        return sortedAPs;
    }

    private static void getChildAPs(Vector<AccessPoint> aps, AccessPoint parentAP, Vector<AccessPoint> sortedAPs) {
       
    	Vector<AccessPoint> temp = new Vector<AccessPoint>();
        Iterator<AccessPoint> it = aps.iterator();
        while(it.hasNext())
            temp.add(it.next());
        
        it = temp.iterator();
        while(it.hasNext()) {
            AccessPoint 	ap 		= (AccessPoint)it.next();
            IRuntimeConfiguration runtimeConfig	= ap.getRuntimeApConfiguration();
    		if(runtimeConfig == null) {
    			return;
    		}
    		if(parentAP.containsAddress(runtimeConfig.getParentBssid()) == true) {
                aps.remove(ap);
                getChildAPs(aps, ap, sortedAPs);
            }
        }
        sortedAPs.add(parentAP);
    }
}
