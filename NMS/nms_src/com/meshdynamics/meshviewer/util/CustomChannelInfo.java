/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : CustomChannelInfo
 * Comments : Holds single channel from RF Editor
 * Created  : Dec 13, 2006
 * Author   : Imran Khan
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 *
 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Imran  |
 * --------------------------------------------------------------------------------
 * |  1  |jan 02, 2007 | Private channel flag added                      | Abhishek |
 * --------------------------------------------------------------------------------
 * |  0  |Dec 13, 2006 | Created                                         | Imran |
 * --------------------------------------------------------------------------------
 **********************************************************************************/


package com.meshdynamics.meshviewer.util;

public class CustomChannelInfo {
	
	private short 	channelNo;
	private int  	channelFrequency;
	private short	privateChannelFlag;
	
	public short getChannelNo() {
		return channelNo;
	}
	public void setChannelNo(short channelNo) {
		this.channelNo = channelNo;
	}
	public int getChannelFrequency() {
		return channelFrequency;
	}
	public void setChannelFrequency(int channelFrequency) {
		this.channelFrequency = channelFrequency;
	}
	
	public short getPrivateChannelFlag() {
		return privateChannelFlag;
	}
	
	public void setPrivateChannelFlag(short privateChannelFlag) {
		this.privateChannelFlag = privateChannelFlag;
	}
}
