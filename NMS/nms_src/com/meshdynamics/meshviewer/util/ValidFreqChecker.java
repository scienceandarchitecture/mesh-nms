/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : ValidFreqChecker.java
 * Comments : 
 * Created  : Oct 25, 2004
 * Author   : Abhishek
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |Nov 04, 2007 | updateValidFreq added                           | Abhishek  |
 * --------------------------------------------------------------------------------
 * |  0  |jan 04, 2007 | Created                                         | Abhishek  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.util;

import java.util.Vector;

import com.meshdynamics.meshviewer.mesh.Mesh;

public class ValidFreqChecker {
	
	private	Vector<String>	channelFrequency;
	
	public ValidFreqChecker(){
		channelFrequency = new Vector<String>(); 
	}
	
	public void freqChecker(int channelFreq, int endChannelFreq ){
	while(channelFreq < endChannelFreq){	
			if(channelFreq < 4800){
				
					if(((channelFreq - 2192) % 5) == 0) {
						channelFrequency.add(""+channelFreq);
					}else if(((channelFreq - 2224) % 5) == 0){
						channelFrequency.add(""+channelFreq);
					}else{
						
					}
			}else if(((channelFreq % 5) == 2) && (channelFreq <= 5435)) {
						channelFrequency.add(""+channelFreq);
					}else if ((channelFreq % 20) == 0 && channelFreq >= 5120) {
						channelFrequency.add(""+channelFreq);
					} else if ((channelFreq % 10) == 0) {
						channelFrequency.add(""+channelFreq);
					} else if ((channelFreq % 5) == 0) {
						channelFrequency.add(""+channelFreq);
				 }
			
			channelFreq ++;
	 }
	}
	public int getValidChannel(int index){
		int channel	= 0;
		String strChannel	=	(String)channelFrequency.get(index);
		try
		{
			channel			=   Integer.parseInt(strChannel.trim());
		}
		catch(Exception e){
			e.printStackTrace(); 
		}
		return channel;  
	}
	
	public int getValidChLength(){
		return channelFrequency.size();  
	}
	
	public boolean isValidFrequency(int channelFreq) {
		
		for(int i = 0; i < channelFrequency.size(); i++) {
			
			int freq	= Integer.parseInt((String) channelFrequency.get(i));
			if(freq == channelFreq) {
				return true;
			}
			
		}
		return false;	
	}
	
	public void updateValidFreq(short phySubType, short bandWidth) {
		
		if(phySubType == Mesh.PHY_SUB_TYPE_802_11_A){
			
			int lowerVal	=	4899 + (bandWidth/2);
			int upperVal	=	5999 - (bandWidth/2);
			freqChecker(lowerVal,upperVal);
			
		}
		else if(phySubType == Mesh.PHY_SUB_TYPE_802_11_B ||
				phySubType == Mesh.PHY_SUB_TYPE_802_11_G ||
				phySubType == Mesh.PHY_SUB_TYPE_802_11_B_G 
				){
			
			int lowerVal	=	2299 + (bandWidth/2);
			int upperVal	=	2499 - (bandWidth/2);
			freqChecker(lowerVal,upperVal);
		}
	}
	public static void main(String[] args) {
		
		ValidFreqChecker obj	=	new ValidFreqChecker();
		obj.freqChecker(2000,2999);
	}
}
