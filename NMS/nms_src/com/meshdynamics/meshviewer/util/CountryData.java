/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : CountryData.java
 * Comments : 
 * Created  : Nov 24, 2006
 * Author   : Imran 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * |  0  |Nov 24, 2006  |  Created	                                  | Imran     |
 * --------------------------------------------------------------------------------
 */



package com.meshdynamics.meshviewer.util;


public class CountryData {
	public  String 	countryName;
	public	int 	countryCode;
	public	int     regDomnFld;
	
	public CountryData(String cName, int cCode, int rdf){
		countryName = cName;
		countryCode = cCode;
		regDomnFld	= rdf;
	}
	
}
