/**********************************************************************************
 * MeshDynamics
 * --------------
 * File     : TransmitRateTable.java
 * Comments :
 * Created  : Jan 2, 2007
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  2  |Mar 15, 2007 | getPublicSafetyRateTable added                  |Abhishek|
 * --------------------------------------------------------------------------------
 * |  1  |Jan 04, 2007 | return types and arguments modified for all     |Imran   |
 * --------------------------------------------------------------------------------
 * |  0  |Jan 02, 2007 | Created                                         |Prachiti|
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.meshviewer.util;

import java.util.Iterator;
import java.util.Vector;

import com.meshdynamics.meshviewer.mesh.Mesh;



public class TransmitRateTable {

	/*TODO: add 900MHZ Rates also*/

	private static int txRate_ieee_a[] 			= {6,9,12,18,24,36,48,54};
	private static int txRate_ieee_bg[] 		= {1,2,5,6,9,11,12,18,24,36,48,54};
	private static int txRate_ieee_b[] 			= {1,2,5,11};
	private static int txRate_ieee_g[] 			= {6,9,12,18,24,36,48,54};
	private static int _20_short_gi_tx_rates[]  = {7, 14, 21, 28, 43, 57, 65};
    private static int _20_long_gi_tx_rates[]   = {6, 13, 19, 26, 39, 52, 58,65};

     private static int _40_short_gi_tx_rates[] = {15, 30, 45, 60, 90, 120, 135, 150};
     private static int _40_long_gi_tx_rates[]  = {13, 27, 40, 54, 81, 108, 121, 135};
      
    private static int _80_short_gi_tx_rates[]   = {32, 65, 97, 130, 195, 260, 292, 325, 390};
    private static int _80_long_gi_tx_rates[]    = {29, 58, 87, 117, 175, 234, 263, 292, 351};


	private static double RATE_FACTOR_QUARTER	= 0.25;
	private static double RATE_FACTOR_HALF		= 0.5;
	private static double RATE_FACTOR_DEFAULT	= 1;
	private static double RATE_FACTOR_DOUBLE	= 2;

	public static Vector<String> getDefaultRateTable(int phy_sub_type, int ifBandwidth){

		switch(phy_sub_type){
		case Mesh.PHY_SUB_TYPE_802_11_A :
			return fillTxrate(txRate_ieee_a,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_B :
			return fillTxrate(txRate_ieee_b,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_G :
			return fillTxrate(txRate_ieee_g,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_B_G:
			return fillTxrate(txRate_ieee_bg,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_24GHz_N :
			return fillTxrate(txRate_ieee_a,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_5GHz_N :
			return fillTxrate(txRate_ieee_a,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_AC :
			return fillTxrate(txRate_ieee_a,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_AN :
			return fillTxrate(txRate_ieee_a,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_ANAC :
			return fillTxrate(txRate_ieee_a,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_BGN:
			return fillTxrate(txRate_ieee_bg,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ :
			return fillTxrate(txRate_ieee_a,RATE_FACTOR_QUARTER);
		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ :
			return fillTxrate(txRate_ieee_a,RATE_FACTOR_HALF);
		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ :
			return fillTxrate(txRate_ieee_a,RATE_FACTOR_DEFAULT);
		}
		return null;
	}
//for advance Info
	public static Vector<String> getDefaultRateTable_AdvanceSettings(int phy_sub_type, int ifBandwidth,short guardInterval){

		switch(phy_sub_type){
		case Mesh.PHY_SUB_TYPE_802_11_A :
			return fillTxrate(txRate_ieee_a,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_B :
			return fillTxrate(txRate_ieee_b,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_G :
			return fillTxrate(txRate_ieee_g,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_B_G:
			return fillTxrate(txRate_ieee_bg,RATE_FACTOR_DEFAULT);
		case Mesh.PHY_SUB_TYPE_802_11_5GHz_N :
			if(ifBandwidth == 2){
		         if(guardInterval == 2)
			             return fillTxrate(_20_short_gi_tx_rates,RATE_FACTOR_DEFAULT);
			     else if(guardInterval == 0)
				        return fillTxrate(_20_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}else if(ifBandwidth == 1 || ifBandwidth == 0){
			    if(guardInterval == 2)
			          return fillTxrate(_40_short_gi_tx_rates,RATE_FACTOR_DEFAULT);			
			    else if(guardInterval == 0)
				      return fillTxrate(_40_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}
		case Mesh.PHY_SUB_TYPE_802_11_24GHz_N :
			if(ifBandwidth == 2){
		         if(guardInterval == 2)
			             return fillTxrate(_20_short_gi_tx_rates,RATE_FACTOR_DEFAULT);
			     else if(guardInterval == 0)
				        return fillTxrate(_20_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}else if(ifBandwidth == 1 || ifBandwidth == 0){
			    if(guardInterval == 2)
			          return fillTxrate(_40_short_gi_tx_rates,RATE_FACTOR_DEFAULT);			
			    else if(guardInterval == 0)
				      return fillTxrate(_40_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}
		case Mesh.PHY_SUB_TYPE_802_11_AC :
			if(ifBandwidth == 2){
		         if(guardInterval == 2)
			             return fillTxrate(_20_short_gi_tx_rates,RATE_FACTOR_DEFAULT);
			     else if(guardInterval == 0)
				        return fillTxrate(_20_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}else if(ifBandwidth == 1 || ifBandwidth == 0){
			    if(guardInterval == 2)
			          return fillTxrate(_40_short_gi_tx_rates,RATE_FACTOR_DEFAULT);			
			    else if(guardInterval == 0)
				      return fillTxrate(_40_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}else if(ifBandwidth == 3){
			  if(guardInterval == 2)
			          return fillTxrate(_80_short_gi_tx_rates,RATE_FACTOR_DEFAULT);
			  else if(guardInterval == 0)
				    return fillTxrate(_80_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}
		case Mesh.PHY_SUB_TYPE_802_11_AN :
			if(ifBandwidth == 2){
		         if(guardInterval == 2)
			             return fillTxrate(_20_short_gi_tx_rates,RATE_FACTOR_DEFAULT);
			     else if(guardInterval == 0)
				        return fillTxrate(_20_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}else if(ifBandwidth == 1 || ifBandwidth == 0){
			    if(guardInterval == 2)
			          return fillTxrate(_40_short_gi_tx_rates,RATE_FACTOR_DEFAULT);			
			    else if(guardInterval == 0)
				      return fillTxrate(_40_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}
		case Mesh.PHY_SUB_TYPE_802_11_ANAC :
			if(ifBandwidth == 2){
		         if(guardInterval == 2)
			             return fillTxrate(_20_short_gi_tx_rates,RATE_FACTOR_DEFAULT);
			     else if(guardInterval == 0)
				        return fillTxrate(_20_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}else if(ifBandwidth == 1 || ifBandwidth == 0){
			    if(guardInterval == 2)
			          return fillTxrate(_40_short_gi_tx_rates,RATE_FACTOR_DEFAULT);			
			    else if(guardInterval == 0)
				      return fillTxrate(_40_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}else if(ifBandwidth == 3){
			  if(guardInterval == 2)
			          return fillTxrate(_80_short_gi_tx_rates,RATE_FACTOR_DEFAULT);
			  else if(guardInterval == 0)
				    return fillTxrate(_80_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}
		case Mesh.PHY_SUB_TYPE_802_11_BGN:
			if(ifBandwidth == 2){
		         if(guardInterval == 2)
			             return fillTxrate(_20_short_gi_tx_rates,RATE_FACTOR_DEFAULT);
			     else if(guardInterval == 0)
				        return fillTxrate(_20_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}else if(ifBandwidth == 1 || ifBandwidth == 0){
			    if(guardInterval == 2)
			          return fillTxrate(_40_short_gi_tx_rates,RATE_FACTOR_DEFAULT);			
			    else if(guardInterval == 0)
				      return fillTxrate(_40_long_gi_tx_rates,RATE_FACTOR_DEFAULT);
			}
		}
			return null;
		}
	private static Vector<String> fillTxrate(int[] txRate, double rateFactor ){
		Vector<String> vctrTxRate = new Vector<String>(txRate.length);
		for(int i=0;i<txRate.length;i++){
			int rate = (int) (txRate[i]*rateFactor);
			if(rate <= 0)
				continue;
			vctrTxRate.add(""+rate);
		}
		return vctrTxRate;
	}

	public static Vector<String> getCustomRateTable(int phy_sub_type,int bandwidth){

		switch(phy_sub_type){
		case Mesh.PHY_SUB_TYPE_802_11_A :
			return _getCustomRateTable(txRate_ieee_a,bandwidth);
		case Mesh.PHY_SUB_TYPE_802_11_B :
			return _getCustomRateTable(txRate_ieee_b,bandwidth);
		case Mesh.PHY_SUB_TYPE_802_11_G :
			return _getCustomRateTable(txRate_ieee_g,bandwidth);
		case Mesh.PHY_SUB_TYPE_802_11_B_G:
			return _getCustomRateTable(txRate_ieee_bg,bandwidth);
		case Mesh.PHY_SUB_TYPE_802_11_5GHz_N:
			return _getCustomRateTable(txRate_ieee_a,bandwidth);
		case Mesh.PHY_SUB_TYPE_802_11_24GHz_N :
			return _getCustomRateTable(txRate_ieee_a,bandwidth);
		case Mesh.PHY_SUB_TYPE_802_11_AC :
			return _getCustomRateTable(txRate_ieee_a,bandwidth);
		case Mesh.PHY_SUB_TYPE_802_11_ANAC :
			return _getCustomRateTable(txRate_ieee_a,bandwidth);
		case Mesh.PHY_SUB_TYPE_802_11_BGN:
			return _getCustomRateTable(txRate_ieee_bg,bandwidth);
		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_5MHZ :
			return _getCustomRateTable(txRate_ieee_a,bandwidth);
		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_10MHZ :
			return _getCustomRateTable(txRate_ieee_a,bandwidth);
		case Mesh.PHY_SUB_TYPE_802_11_PUBLIC_SAFETY_20MHZ :
			return _getCustomRateTable(txRate_ieee_a,bandwidth);
			
		}
		return null;
	}

	private static Vector<String> _getCustomRateTable(int[] defaultRateTable,int bandwidth){

		double factor = 1;

		switch(bandwidth){
		case Mesh.CHANNEL_BANDWIDTH_5_MHZ:
			factor = RATE_FACTOR_QUARTER; /* Quarter Rate */
			break;
		case Mesh.CHANNEL_BANDWIDTH_10_MHZ:
			factor = RATE_FACTOR_HALF; /* Half Rate */
			break;
		case Mesh.CHANNEL_BANDWIDTH_20_MHZ:
			factor = RATE_FACTOR_DEFAULT;		/* Default Rate */
			break;
		case Mesh.CHANNEL_BANDWIDTH_40_MHZ:
			factor = RATE_FACTOR_DOUBLE; /* Double Rate */
			break;
		}

		Vector<String> vctrRateTable = new Vector<String>(defaultRateTable.length);

		for(int i=0;i<defaultRateTable.length;i++){
			if(!vctrRateTable.contains("" + (int)(defaultRateTable[i] * factor))){
				if((int)(defaultRateTable[i] * factor)!= 0)
					vctrRateTable.add("" + (int)(defaultRateTable[i] * factor));
			}
		}

		return vctrRateTable;
	}

	public static String[] getDefaultRateTableStr(int phy_sub_type, int ifBandwidth){

		String[] RateTableStr;

		Vector<String> defaultVctrRateTable = getDefaultRateTable(phy_sub_type, ifBandwidth);
		if(defaultVctrRateTable == null)
			return null;

		RateTableStr 				= new String[defaultVctrRateTable.size()];
		Iterator<String> it 		= defaultVctrRateTable.iterator();

		int count = 0;
		while(it.hasNext()){
			RateTableStr[count] = it.next()+ " Mbps";
			count++;
		}
		return RateTableStr;
	}
	//for Advance info
	public static String[] getDefaultRateTableStr_Advance(short phy_sub_type, short ifBandwidth,short guardInterval){

		String[] RateTableStr;

		Vector<String> defaultVctrRateTable = getDefaultRateTable_AdvanceSettings(phy_sub_type, ifBandwidth, guardInterval);
		if(defaultVctrRateTable == null)
			return null;

		RateTableStr 				= new String[defaultVctrRateTable.size()];
		Iterator<String> it 		= defaultVctrRateTable.iterator();

		int count = 0;
		while(it.hasNext()){
			RateTableStr[count] = it.next()+ " Mbps";
			count++;
		}
		return RateTableStr;
	}

	public static String[] getCustomRateTableStr(int phy_sub_type,int bandwidth){

		Vector<String> vctrRateTable 		= getCustomRateTable(phy_sub_type,bandwidth);
		if(vctrRateTable == null)
			return null;
		String[] RateTableStr 	= new String[vctrRateTable.size()];
		int count = 0;
		Iterator<String> it = vctrRateTable.iterator();
		while(it.hasNext()){
			RateTableStr[count] = it.next() + " Mbps";
			count++;
		}
		return RateTableStr;
	}

	public static void main(String args[]){

		String a[] = TransmitRateTable.getCustomRateTableStr(Mesh.PHY_SUB_TYPE_802_11_A,Mesh.CHANNEL_BANDWIDTH_5_MHZ);

		for(int i=0;i<a.length;i++ ){
			System.out.println(a[i]);
		}
	}
}
