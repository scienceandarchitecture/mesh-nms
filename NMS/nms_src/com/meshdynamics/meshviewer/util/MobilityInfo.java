/**
 * MeshDynamics 
 * -------------- 
 * File     : MobilityInfo.java
 * Comments : 
 * Created  : May 07, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 * ----------------------------------------------------------------------------------
 * | 0  | May 07, 2007   | created									  | Abhishek     |
 * ----------------------------------------------------------------------------------*/

package com.meshdynamics.meshviewer.util;

import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;


public class MobilityInfo {
    
    public static final int 	MOBILITY_SETUP_BASIC	= 1;
	public static final int 	MOBILITY_SETUP_ADVANCED	= 2;
	
	/**
	 * Speed Mph Values, distMileValues are the values from mobility.conf on board
	 * speedMphValues[0],distMileValues[0] are the values for Stationary mode
	 * */
	
	public static double speedMphValues[] = {0,61,64,68,72,76,81,87,94,102,111,122,136,153,174,204,244,306,408,612,1224};
	public static double distMileValues[] = {0,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17,0.17};
		 	
	public static long	calulateFragThresholdValue(short speed, double distance, byte speedUnit, byte distanceUnit, short mode){
		
		long fragThreshold = 0;
		
		if(mode == IMeshConfiguration.MOBILITY_MODE_STATIONARY) {
			fragThreshold = IMeshConfiguration.FRAG_THRESHOLD;
			return fragThreshold;
		}
		/*
		 * Frag Threshold = SpeedUnit (2 bits) + Dist Unit(2 bits) + Speed(12 bits) + Distance(16 bits) 
		 */

		if(distance < 1) {
			/**
			 * If Distance is less than 1 then 
			 * we have to convert it to a value in lower distance unit
			 */
			byte newDistanceUnit	= (byte)(distanceUnit - 1);
			if(distanceUnit < 0) {
				distanceUnit = IMeshConfiguration.MOBILITY_DIST_MILES;
			}
			distance		= getDistanceValue(distanceUnit,newDistanceUnit,distance);
			distanceUnit	= newDistanceUnit;
		}
		distance		= Math.round(distance);
		
		fragThreshold |= (speed << 16);
		fragThreshold |= (speedUnit << 30);
		fragThreshold |= (long)(distance);
		fragThreshold |= (distanceUnit << 28);
		
		return fragThreshold;
	}
	
	public static short calculateMobilityIndex(int mode, double distance,int distancUnit, double speed, int speedUnit) {
		
		double tDist	= 0;
		double tSpeed   = 0;
		
		if(distancUnit == IMeshConfiguration.MOBILITY_DIST_FEET) {
			tDist = 0.00018939 * distance;					
		} else if(distancUnit == IMeshConfiguration.MOBILITY_DIST_METERS) {
			tDist = 0.00062137 * distance;
		} else {
			tDist = distance;
		}
		
		tDist	= tDist / 2;
		
		if(speedUnit == IMeshConfiguration.MOBILITY_SPEED_KMPH) { 
			tSpeed 	= Math.round(speed/1.609344);
		} else {
			tSpeed 	= speed;
		}
		
		
		double	time  		= tDist/tSpeed;
		time  				= time * 60 * 60 * 1000;
		double 	index		= 20 - Math.floor(time/250) + 1;
		
		if(mode == IMeshConfiguration.MOBILITY_MODE_STATIONARY) {
			index = 0;
		}else if(mode == IMeshConfiguration.MOBILITY_MODE_MOBILE) {			
			
			if(index <= 0) {
				index = 1;
			}
			if(index > 20) {
				index = 20;
			}
		}
		
		return (short) index;
	}

	/**
	 * @param mobility_dist_miles
	 * @param mobility_dist_feet
	 * @param temp
	 */
	public static double getDistanceValue(int srcDistanceUnit, int destDistanceUnit, double value) {
		
		//Conversion from Miles to feet/meter
		
		if(srcDistanceUnit == IMeshConfiguration.MOBILITY_DIST_MILES){
			
			if(destDistanceUnit == IMeshConfiguration.MOBILITY_DIST_FEET){
				//1 mile = 5280 feet
				return (value * 5280);
			}else if(destDistanceUnit == IMeshConfiguration.MOBILITY_DIST_METERS){
				//1 mile = 1609.344 meter
				return (value * 1609.344);
			} 
		}else if(srcDistanceUnit == IMeshConfiguration.MOBILITY_DIST_METERS){
			
			if(destDistanceUnit == IMeshConfiguration.MOBILITY_DIST_FEET){
				//1 meter = 3.280 839 895 feet
				return (value * 3.280839895);
			} 
		}
		
		return 0;
	}
	
}
