package com.meshdynamics.meshviewer.util;

import com.meshdynamics.meshviewer.mesh.Mesh;

public class ProtocolEntry {
	

	//for back haul interfaces 
	private static String[] A_B={"802.11a"};
	private static String[] K_B={"802.11a","802.11n","802.11a/n"};
	private static String[] C_B={"802.11ac"};
	private static String[] L_B={"802.11a","802.11n","802.11ac","802.11a/n","802.11a/n/ac"};
	
	//for down link interfaces
	private static String[] B={"802.11b"};//only references purpose
	private static String[] G={"802.11g"};//only references purpose
	private static String[] I={"802.11b","802.11b/g"};
	private static String[] N_5GHZ={"802.11n"};
	private static String[] N_24GHz={"802.11n"};
	private static String[] J={"802.11b","802.11b/g","802.11n","802.11b/g/n"};
	public  static String[] default_list={};
	
	public static String[] getProtocolEntry(int phySubType, String interfaceName){
	String[] protocolArr=null;
		switch (phySubType) {
	case  Mesh.PHY_SUB_TYPE_802_11_A:
		  protocolArr =A_B;
		  break;
	case  Mesh.PHY_SUB_TYPE_802_11_5GHz_N: 		
	      protocolArr =N_5GHZ;
		  break;  
	case Mesh.PHY_SUB_TYPE_802_11_AN:
		protocolArr =K_B;
		break;
	case Mesh.PHY_SUB_TYPE_802_11_AC:
		protocolArr =C_B;
		break;
	case Mesh.PHY_SUB_TYPE_802_11_ANAC:
		protocolArr =L_B;
		break;
	case Mesh.PHY_SUB_TYPE_802_11_B:
		protocolArr =B;
		break;
	case Mesh.PHY_SUB_TYPE_802_11_G:
		protocolArr =G;
		break;
	case  Mesh.PHY_SUB_TYPE_802_11_B_G:
		protocolArr =I;
		break;
	case  Mesh.PHY_SUB_TYPE_802_11_BGN:
		protocolArr =J;
		break;
    case  Mesh.PHY_SUB_TYPE_802_11_24GHz_N: 		
	      protocolArr =N_24GHz;
		  break;  
	default:
		protocolArr=default_list;
		break;
	}
		return protocolArr;
	}
	
	//get subType
	public static short getSubType(String protocolName,String fre){
		short SubType=0;
		switch (protocolName) {
		case "802.11a":
			 SubType=Mesh.PHY_SUB_TYPE_802_11_A;
			 break;
        case "802.11n":
        	if(fre.equals("5G"))
        	   SubType=Mesh.PHY_SUB_TYPE_802_11_5GHz_N;
        	else
        	  SubType=Mesh.PHY_SUB_TYPE_802_11_24GHz_N;
			break;
        case "802.11ac":
        	SubType=Mesh.PHY_SUB_TYPE_802_11_AC;
        	break;
        case "802.11a/n":
        	SubType=Mesh.PHY_SUB_TYPE_802_11_AN;
        	break;
        case "802.11a/n/ac":
        	SubType=Mesh.PHY_SUB_TYPE_802_11_ANAC;
        	break;
        case "802.11b":
        	SubType=Mesh.PHY_SUB_TYPE_802_11_B;
        	break;
        case "802.11g":
        	SubType=Mesh.PHY_SUB_TYPE_802_11_G;
        	break;
        case "802.11b/g":
        	SubType=Mesh.PHY_SUB_TYPE_802_11_B_G;
        	break;
        case "802.11b/g/n":
        	SubType=Mesh.PHY_SUB_TYPE_802_11_BGN;
        	break;
		default:
			break; 
		}
		return SubType;
	}
	
	public static char getMaxSupportProtocol(String interfaceName,String model){
		char ch=0;
		switch (interfaceName) {
		case "wlan0":
			ch=model.charAt(0);
			break;
	    case "wlan1":	
	    	ch=model.charAt(1);
	    	break;
        case "wlan2":
        	ch=model.charAt(2);
        	break;
        case "wlan3":
        	ch=model.charAt(3);
        	break;
        case "wlan4":
        	ch=model.charAt(4);
        	break;
          case "wlan5":
        	  ch=model.charAt(5);
        	break;
          case "v_wlan0":
        	  ch=model.charAt(0);
        	break;	
        default:
			break;
		}
		return ch;
	}
	public static int getSubType_(char ch){
		
		short SubType=0;
		switch (ch) {
		case 'A':
			SubType=Mesh.PHY_SUB_TYPE_802_11_A;
			break;
        case 'N':	
        	SubType=Mesh.PHY_SUB_TYPE_802_11_5GHz_N;
			break;
        case 'C':
        	SubType=Mesh.PHY_SUB_TYPE_802_11_AC;
        	break;
        case 'K':
        	SubType=Mesh.PHY_SUB_TYPE_802_11_AN;
        	break;
        case 'L':
        	SubType=Mesh.PHY_SUB_TYPE_802_11_ANAC;
        	break;
        case 'B':
        	SubType=Mesh.PHY_SUB_TYPE_802_11_B;
        	break;
        case 'G':
        	SubType=Mesh.PHY_SUB_TYPE_802_11_G;
        	break;
        case 'I':
        	SubType=Mesh.PHY_SUB_TYPE_802_11_B_G;
        	break;
        case 'J':
        	SubType=Mesh.PHY_SUB_TYPE_802_11_BGN;
        	break;
		default:
			break;
		}
		return SubType;
	}
}
