/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : CountryCodeXMLReader.java
 * Comments : 
 * Created  : Apr 26, 2006
 * Author   : Mithil
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 ----------------------------------------------------------------------------------
 * |  15 |Mar 15, 2007 | modelname parsing enhancement fix	 			 |Abhijit |
 *  -------------------------------------------------------------------------------
 * |  14 |Mar 06, 2007 | specific modelname parsing enhanced 			 |Prachiti|
 *  -------------------------------------------------------------------------------
 * |  13 |Dec 12, 2006 | specific modelname parsing added 	 			 |Prachiti|
 *  -------------------------------------------------------------------------------
 * |  12 |Nov 24, 2006 | Country Code reading modified for RDF 			 | Imran  |
 *  -------------------------------------------------------------------------------
 * |  11 |Oct 13,2006  | Country codes are arranged in alphbetical order | Imran  |
 * --------------------------------------------------------------------------------
 * |  10 |Jun 30, 2006 | misc  changes							  		 |Prachiti|
 * --------------------------------------------------------------------------------
 * |  9  |Jun 28, 2006 | ccode xml changes						  		 | Mithil |
 * --------------------------------------------------------------------------------
 * |  8  |June 21, 2006| "D" ignored								     |Mithil  |
 * --------------------------------------------------------------------------------
 * |  7  |June 21, 2006| "X",and "-" ignored						     |Mithil  |
 * --------------------------------------------------------------------------------
 * |  6  |June 21, 2006| Model checking for channels				     |Mithil  |
 * --------------------------------------------------------------------------------
 * |  5  |June 19, 2006| DCA List problems when DCA List = 0		     |Mithil  |
 * --------------------------------------------------------------------------------
 * |  4  |Jun 15, 2006 | Country code reading	                         | Mithil |
 * --------------------------------------------------------------------------------
 * |  3  |Jun 14, 2006 | All Models included	                         | Mithil |
 * --------------------------------------------------------------------------------
 * |  2  |May 31, 2006 | Removed Unwanted prints                         | Mithil |
 * --------------------------------------------------------------------------------
 * |  1  |May 16, 2006 | Misc Changes                                    | Mithil |
 * --------------------------------------------------------------------------------
 * |  0  |Apr 26, 2006 | Created                                         | Mithil |
 * --------------------------------------------------------------------------------
 **********************************************************************************/
package com.meshdynamics.meshviewer.util;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

	
public class CountryCodeXMLReader {

	private final static String		TAG_NAME			= "name";
	private final static String		TAG_COUNTRY			= "country";
	private final static String		TAG_MODEL			= "model";
	private final static String		TAG_DCALIST			= "dca_list";
	private final static String		SEP_COMMA			= ",";
	private final static String		SEP_HYPHEN			= "-";
	
    private 						Document				doc;
	private	 						String 					fileName;
	private							NodeList				list;
	private 						Vector<ChannelListInfo> channelList = new Vector<ChannelListInfo>(); 
	
	public void setXmlFileName(String name){
		fileName = name;
	}
	
	public boolean createXmlDocument(){
		try{
			DocumentBuilderFactory	docFac 	= DocumentBuilderFactory.newInstance();
			DocumentBuilder builder 		= docFac.newDocumentBuilder();
			doc			 					= builder.parse(new File(fileName));
		}catch(Exception e){
			return false;
		}
		
		return true;
	}
	
	private boolean checkFullName(String hardwareModel){
		
		boolean isFullName 	    = false;
		if(doc == null)
			return false;
		
		list 					= doc.getElementsByTagName(TAG_MODEL);
		
		if(list == null)
			return false;
		
		if(list.getLength() <= 0){
			return false;
		}
		for(int i=0; i<list.getLength();i++) {
			Node model = list.item(i);
			
			if(model == null) {
				continue;
			}
			String name 	= model.getAttributes().getNamedItem(TAG_NAME).getNodeValue();
			//String country 	= model.getAttributes().getNamedItem(TAG_COUNTRY).getNodeValue();
			
			if(hardwareModel.equalsIgnoreCase(name)) {
				isFullName = true;
			}
		}
		return isFullName;
	}
	
	void parseCountryCodes(Hashtable<String, CountryData> countryCodes,String country) {
		
		String[] splitstr = country.split(SEP_COMMA);
		
		for(int j=0;j<splitstr.length;j++) {
			String[] 	temp 	= splitstr[j].split(SEP_HYPHEN);
			String 	cName		= temp[0];
			int		cCode		= Integer.parseInt(temp[1]);
			int		rdf			= Integer.parseInt(temp[2]);
			CountryData cData 	= new CountryData(cName,cCode,rdf);
			countryCodes.put(""+cCode,cData);
		}
	}
	
	public Hashtable<String, CountryData> readCountryCodesForModel(String hardwareModel){
		
		Hashtable<String, CountryData> 	countryCodes 	= new Hashtable<String, CountryData>();
		boolean		isFullName		= false;
		
		if(doc == null)
			return null;
		
		list 						= doc.getElementsByTagName(TAG_MODEL);
		
		if(list == null)
			return null;
		
		if(list.getLength() <= 0){
			return null;
		}
		if(checkFullName(hardwareModel) == true){
			isFullName = true;
		}
		
		for(int i=0; i<list.getLength();i++) {
			Node model = list.item(i);
			
			if(model == null) {
				continue;
			}
			String name 	= model.getAttributes().getNamedItem(TAG_NAME).getNodeValue();
			String country 	= model.getAttributes().getNamedItem(TAG_COUNTRY).getNodeValue();
			
			if(isFullName == true){ 
				if (hardwareModel.equalsIgnoreCase(name)){
					parseCountryCodes(countryCodes,country);
				}
			}else if(name.equalsIgnoreCase(hardwareModel.substring(0,hardwareModel.indexOf(SEP_HYPHEN)))) {
				parseCountryCodes(countryCodes,country);
			}
		}
		return countryCodes;
	}
	
	/**
	 * 
	 */
	public void readCountryCodeInformation(String hardwareModel,String countryName,int ccode) {
		
		boolean isFullName		= false;			
		channelList 			= new Vector<ChannelListInfo>();
		
		if(doc == null)
			return;
		
		list 					= doc.getElementsByTagName(TAG_MODEL);
		
		if(list == null)
			return;
		
		if(list.getLength() <= 0) {
			return;
		}
	
		if(checkFullName(hardwareModel) == true){
			isFullName = true;
		}
	
		for(int i=0;i < list.getLength();i++) {
			Node model = list.item(i);
			if(model == null){
				return;
			}
			String name 	= model.getAttributes().getNamedItem(TAG_NAME).getNodeValue();
			String country 	= model.getAttributes().getNamedItem(TAG_COUNTRY).getNodeValue();
			String ccodestr = countryName + SEP_HYPHEN + ccode;
			
			if(isFullName == true){ 
				if (hardwareModel.equalsIgnoreCase(name) && country.indexOf(ccodestr) >= 0)
					readModelInformation(model);
			}else if(name.equalsIgnoreCase(hardwareModel.substring(0,hardwareModel.indexOf(SEP_HYPHEN))) && 
					 country.indexOf(ccodestr) >= 0) {
				readModelInformation(model);
			}
		}
	}
	
	private void readModelInformation(Node model){
		
		NodeList intfList = model.getChildNodes();
		if(intfList.getLength() <= 0){
			return;
		}
		for (int j = 0; j < intfList.getLength(); j++) {
			Node intf = intfList.item(j);
			if(intf == null){
				continue;
			}
		    ChannelListInfo channelinfo = readInterfaceInformation(intf);
			if(channelinfo != null)
				channelList.add(channelinfo);
		}
	}
	
	private ChannelListInfo readInterfaceInformation(Node intf){
		if(intf.getNodeType() != Node.TEXT_NODE){
		    ChannelListInfo channelList = new ChannelListInfo();
			channelList.setInterfaceName(intf.getAttributes().getNamedItem(TAG_NAME).getNodeValue());
			channelList.setChannels(intf.getAttributes().getNamedItem(TAG_DCALIST).getNodeValue());
			//System.out.println(channelList.channels);
			//channelvec.add(channelList);
			return channelList;
		}
		
		return null;
	}

	/**
	 * @return Returns the channelList.
	 */
	public Vector<ChannelListInfo> getChannelList() {
		return channelList;
	}
	
	
	public static void main(String[] args) {
		CountryCodeXMLReader cc = new CountryCodeXMLReader();
		cc.setXmlFileName("D:/MESHVIEWER8.0-18-12-06-FIPS/Config/CountryCode.xml");
		cc.createXmlDocument();
		
		cc.readCountryCodeInformation("MD4350-AAIx","Default",0);
		Vector<ChannelListInfo> cv = cc.getChannelList();
		
		Enumeration<ChannelListInfo> keys 		= cv.elements();
		while(keys.hasMoreElements()){
			
			ChannelListInfo  hashKey 		= (ChannelListInfo)keys.nextElement();
			System.out.println(hashKey.getInterfaceName() + hashKey.getChannels());
		}
		
		
		/*
		Hashtable countryCodes 	= cc.readCountryCodesForModel("MD4350");
		Enumeration keys 		= countryCodes.keys();
		
		while(keys.hasMoreElements()){
			
			String hashKey 		= (String)keys.nextElement();
			CountryData cData 	= (CountryData) countryCodes.get(hashKey);
			System.out.println(cData.countryName + "~" + cData.countryCode);
		}*/
	}
}