/*
 * Created on Sep 30, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.meshviewer;

import java.util.Vector;

/**
 * @author Prachiti Gaikwad
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SystemLogger implements ILog {
	
	 private	Vector<String>	logInfo;
	 private	Vector<String>	logError;
	 private	Vector<String>	logDebug;
	 private	Vector<String>	logSystem;
	 
	 private 	static final int 		LOG_VECTOR_SIZE = 1000;
	 
	public SystemLogger(){
		
		logInfo 		= new Vector<String>(LOG_VECTOR_SIZE);
	  	logError		= new Vector<String>(LOG_VECTOR_SIZE);
	  	logDebug		= new Vector<String>(LOG_VECTOR_SIZE);
	  	logSystem		= new Vector<String>(LOG_VECTOR_SIZE);
	}
  	
	 
	public void logErrorMessage(String errorMsg) {
		
		if(errorMsg == null){
			return; 
		} 
		if(logError.size() < LOG_VECTOR_SIZE)
			logError.add(errorMsg);	
		else
			logError.removeAllElements(); 
	}
	public void logDebugMessage(String debugMsg) {
		
		if(debugMsg == null){
			return; 
		} 
		if(logDebug.size() < LOG_VECTOR_SIZE)
			logDebug.add(debugMsg);	
		else
			logDebug.removeAllElements();		
	}
	public void logInfoMessage(String infoMsg) {
		if(infoMsg == null){
			return; 
		} 
		if(logInfo.size() < LOG_VECTOR_SIZE)
			logInfo.add(infoMsg);	
		else
			logInfo.removeAllElements();
	}
	
	public Vector<String> getLogDebug() {
		return logDebug;
	}
	public Vector<String> getLogError() {
		return logError;
	}
	public Vector<String> getLogInfo() {
		return logInfo;
	}
	public Vector<String> getLogSystem() {
		return logSystem;
	}

	/* (non-Javadoc)
	 * @see com.meshdynamics.meshviewer.ILog#logSystemMessage(java.lang.String)
	 */
	public void logSystemMessage(String sysMsg) {
		if(sysMsg == null){
			return; 
		} 
		if(logSystem.size() < LOG_VECTOR_SIZE)
			logSystem.add(sysMsg);	
		else
			logSystem.removeAllElements();
	}
}