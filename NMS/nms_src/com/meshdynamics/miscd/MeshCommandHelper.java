package com.meshdynamics.miscd;

import java.net.InetAddress;

public class MeshCommandHelper {

	static {   
		System.loadLibrary("MeshCommandHelper");
	}
	
	public String executeCommand(byte[] ipAddress, String command) {
		
		try {
			if(InetAddress.getByAddress(ipAddress).isReachable(10000) == false)
				return "Node not reachable";
		} catch (Exception e) {
			return "Node not reachable";
		}
		
		return runCommand(ipAddress, command);
	}
	
	private native String runCommand(byte[] ipAddress, String command);
}
