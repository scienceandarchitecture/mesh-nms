package com.meshdynamics.hmacsha1;

public class HMAC_SHA1 {

	private static HMAC_SHA1 instance;
	
	static {
	    System.loadLibrary("HMAC_SHA1");
	    instance = null;
	}
	
	public static final int DIGEST_LENGTH	= 20;
	
	private HMAC_SHA1() {
	}
	
	public static HMAC_SHA1 getInstance() {
		if(instance == null)
			instance = new HMAC_SHA1();
		
		return instance;
	}
	
	public boolean generateHMAC(byte[] text, byte[] key, byte[] digest) {
	
		if(text == null || key == null || digest == null)
			return false;
		
		if(digest.length != DIGEST_LENGTH)
			return false;
		
		getDigest(text, text.length, key, key.length, digest);
		
		return true;
	}
	
	private native void getDigest(byte[] text, int text_len, byte[] key, int key_len, byte[] digest);
	
	public static void main(String[] args) {
		HMAC_SHA1.getInstance().generateHMAC(null, null, null);
	}
}
