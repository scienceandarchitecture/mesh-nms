/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : AESException.java
 * Comments : Exception class for Identifiying enc/decrypt errors
 * Created  : Oct 25, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 25, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.aes;

public class AESException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
     * Error while setting key
     */
    public static final int AES_ERROR_SET_KEY			= -1;
    /**
     * Error while encryption 
     */
    public static final int AES_ERROR_ENCR_FAILED 		= -2;
    /**
     * Invalid Key for decryption
     */
    public static final int AES_ERROR_INVALID_KEY 		= -3;
    public static final int AES_ERROR_UNKNOWN	 		= -4;
    public static final int AES_ERROR_KEY_NOT_FOUND 	= -5;
    public static final int AES_ERROR_NULL_ARGUMENT 	= -6;
    public static final int UUENCODE_ERROR				= -7;
    public static final int UUDECODE_ERROR				= -8;
    public static final int DATA_LENGTH_ERROR			= -9;

    
    public AESException(final String message) {
        super(message);
    }
    
    public int getErrorCode() {
        final String msg = getMessage();
        
        if(msg.equals("AES_KEY_ERROR"))
            return AES_ERROR_SET_KEY;
        else if(msg.equals("AES_ENCR_ERROR"))
            return AES_ERROR_ENCR_FAILED;
	    else if(msg.equals("INVALID_KEY_ERROR"))
	        return AES_ERROR_INVALID_KEY;
        else if(msg.equals("KEY_NOT_FOUND"))
            return AES_ERROR_KEY_NOT_FOUND;
        else if(msg.equals("NULL_ARGUMENT"))
            return AES_ERROR_NULL_ARGUMENT;
        else if(msg.equals("UUENCODE_ERROR"))
            return UUENCODE_ERROR;
        else if(msg.equals("UUDECODE_ERROR"))
            return UUDECODE_ERROR;
        else if(msg.equals("DATA_LENGTH_ERROR"))
            return DATA_LENGTH_ERROR;
                   
        
        return AES_ERROR_UNKNOWN;        
    }
}
