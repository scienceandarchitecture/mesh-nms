/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : AES.java
 * Comments : Exception class for Identifiying enc/decrypt errors
 * Created  : Sep 30, 2004
 * Author   : Prachiti Gaikwad
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ---------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author  |
 * ---------------------------------------------------------------------------------
 * |  1  |Jan 30, 2006 | Restructuring                                   | Mithil  |
 * ---------------------------------------------------------------------------------
 * |  0  |Sep 30, 2004 | Created                                         | Prachiti|
 * ---------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.aes;



public class AES {
	
	static {
		System.loadLibrary("aeslib");
	}
	public native byte[] aesdecodeKey(byte[] buffer, int text_key_len, byte[] mac_address)throws AESException;
	public native byte[] aesencodeKey(byte[] text_key,int text_key_len,byte[] mac_address)throws AESException;

	public native byte[] aesEncrypt(byte[] input,int input_length,byte[] key,int key_length )throws AESException ;		
	public native byte[] aesDecrypt(byte[] input,int input_length,byte[] key,int key_length )throws AESException;	
}
/*
class test {
	public static void main(String[] args){
		
		AES aes = new AES();
		
		String st;
		String text_key 	= "cybernetics!";
		
		final byte[] mac_address	= {0x00,0x02,0x6F, 0x09, 0x59, 0x27};
		String Text 		= "AES TEST TEXT FOR ENCRYPTION AND DECRYPTION";
		byte[] ret = null;
		
		
		try {
            ret = aes.aesencodeKey(text_key.getBytes(),text_key.length(),mac_address);
    		st = new String(ret);
    		System.out.println(st);
    		try {
                FileOutputStream fos = new FileOutputStream("Key.txt");
                fos.write(ret);
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            ret = new byte[500];
            FileInputStream fis = null;
            try {
                fis = new FileInputStream("Key.txt");
            } catch (FileNotFoundException e4) {
                e4.printStackTrace();
            }
            int count = 0;
            try {
                count = fis.read(ret);
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            System.out.println(new String(ret, 0, count));
    		ret = aes.aesdecodeKey(ret,count, mac_address);
    		st = new String(ret);
    		System.out.println("------------------> " + st);
    		System.out.println("------------------> " + text_key);
    		
        } catch (AESException e) {
            e.printStackTrace();
        }
		
		
		/*try {
			ret = aes.aesEncrypt(Text.getBytes(),Text.length(),text_key.getBytes(),text_key.length());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		st = new String(ret);
		System.out.println(st);
		String sst = new String(ret);
		byte[] rets;
		//ret = aes.aesDecrypt(sst.getBytes(),sst.length(),text_key.getBytes(),text_key.length());
		for(int i=0; i< 1000; i++) {
		try {
		    String ss = text_key1 + i;
			rets = aes.aesDecrypt(ret,ret.length,ss.getBytes(),ss.length());
			st = new String(rets);
			System.out.println(st);
			System.out.println(text_key1 + i);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		}
		
	}
	
}*/
