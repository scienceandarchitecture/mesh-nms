/*******************************************************************************
 * MeshDynamics -------------- 
 * File 	: Event.java 
 * Comments : 
 * Created 	: Dec 17, 2004 
 * Author 	: Anand Bhange 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc
 * 
 * File Revision History
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Dec 17, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.util;

public class Event {

	public static final int TIME_OUT 	= 1;
	public static final int NOTIFY 		= 2;
	public static final int ERROR 		= 3;

	private boolean notify = false;
	
	
	public Event() { 
	}

	public synchronized int WaitForSingleObject(int timeOut) {
		try { 
			wait(timeOut); 
		} catch (Exception e){ 
			return ERROR;
		}
		
		if(notify)
			return NOTIFY;
		else
			return TIME_OUT;		
	}

	public synchronized void setEvent() {
		notify = true;
		notify();
	}
	
	public synchronized void resetEvent() {
		notify();
		notify = false;
	}
}