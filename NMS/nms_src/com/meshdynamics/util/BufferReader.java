/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : BufferReader.java
 * Comments : 
 * Created  : Oct 13, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 13, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.util;

import com.meshdynamics.meshviewer.mesh.Mesh;

public class BufferReader {
    byte[] 	buffer;
    int 	pos;
    int 	length;
    
    public BufferReader(byte[] buf, int length) {
        buffer 			= buf;
        this.length 	= length;
        reset();
    }
    

    /**
     * Reset Buffer Read Pointer
     */
    public void reset() {
        pos = 0;
    }


    /**
     * @return MacAddress
     */
    public MacAddress readMacAddress() {
        byte[] mac = new byte[6];
        MacAddress macAddress = new MacAddress();
        int i=0;
		for(i=0;i<6;i++){
			mac[i] = buffer[pos++];	
		}
		macAddress.setBytes(mac);
		return macAddress;
    }


    /**
     * @return int
     */
    public long readInt() {
        long ret = 0;
        ret = Bytes.bytesToInt(buffer,pos);
		pos += 4;
		return ret;
    }


    /**
     * @return
     */
    public short readByte() {
        short ret = 0;
       	ret =  Bytes.byteToShort(buffer[pos]);
        pos += 1;
        return ret;
    }


    /**
     * @param i
     * @return
     */
    public byte[] getRemainingBytes() {
        int len = length-pos;
        byte[] buf = new byte[len];
        for(int i=0; i< len; i++) {
            buf[i] = buffer[pos++];
        }
        return buf;
    }
    
    /**
     * @param i
     * @return
     */
    public byte[] getBytes(int len) {
        byte[] buf = new byte[len];
        for(int i=0; i< len; i++) {
            buf[i] = buffer[pos++];
        }
        return buf;
    }


    /**
     * @return
     */
    public String readString() {
        try{
        	int len = buffer[pos++];
            String ret = new String(buffer, pos, len);
            pos += len;
            return ret;	
        } catch (Exception e){
        	Mesh.logException(e);
        	return "";
        }
    	
    }
    public String readString_value(int length) {
        try{
            String ret = new String(buffer, pos, length);
            pos += length;
            return ret;	
        } catch (Exception e){
        	Mesh.logException(e);
        	return "";
        }
    	
    }

    /**
     * @return
     */
    public int readShort() {
        int ret = 0;
        ret = Bytes.bytesToShort(buffer,pos);
		pos += 2;
		return ret;        
    }


	/**
	 * @return
	 */
	public IpAddress readIPAddress() {
		byte[] buf 			= 	new byte[4];
		IpAddress ipAddress = new IpAddress();
		for(int cnt =0;cnt<4;cnt++){
			buf[cnt] = buffer[pos];
			pos++;
 		}
		ipAddress.setBytes(buf);
		return ipAddress;
	}


	public byte[] getBuffer() {
		return buffer;
	}


	public int getLength() {
		return length;
	}

    
    
}
