
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : WEP.java
 * Comments : 
 * Created  : Mar 10, 2005
 * Author   : Bindu Khare
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date        |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  1  |Jan 30, 2006| Restructuring                                   | Mithil |
 * -----------------------------------------------------------------------------
 * |  0  |Mar 10, 2005| Created                                         | Bindu  |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.util;

public class WEP {
	
	static {
		System.loadLibrary("WEPKey");
	}
	
	public native int keyGenerate(String passPhrase, int passPhraseLength, int wepType, char[] data);	
	
}
