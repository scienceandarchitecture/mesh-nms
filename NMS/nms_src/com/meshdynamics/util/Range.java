/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Range.java
 * Comments : 
 * Created  : Feb 21, 2007
 * Author   : Bindu Parakala
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  | Date         |  Comment                                        | Author   |
 * ----------------------------------------------------------------------------------
 * |  0  | Feb 21, 2007 | Created                                         |  Bindu   |
 * ----------------------------------------------------------------------------------
 **********************************************************************************/


package com.meshdynamics.util;

public class Range {

	private long lowerLimit;
	private long upperLimit;
	
	public Range() {		
	}
	
	public Range(long lower, long upper) {
		upperLimit = upper;
		lowerLimit = lower;
	}
	
	public long getLowerLimit() {
		return lowerLimit;
	}
	
	public void setLowerLimit(long lowerLimit) {
		this.lowerLimit = lowerLimit;
	}
	
	public long getUpperLimit() {
		return upperLimit;
	}
	
	public void setUpperLimit(long upperLimit) {
		this.upperLimit = upperLimit;
	}

}
