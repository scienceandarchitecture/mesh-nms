/*
 * Semaphore.java
 *
 * Created on March 11, 2002, 8:00 PM
 */

package com.meshdynamics.util;

import  java.util.Hashtable;

/**
 * The Semaphore is an implementation of the
 * "semaphore" synchronization primitive.
 * @author  Sriram
 */

public class Semaphore {

    private int                _count;
    private int               _maxCount;
    private String            _name;
    private static Hashtable<String,SemaphoreReference>  _namedSemaphores = new Hashtable<String,SemaphoreReference>();

    private static class SemaphoreReference {

        Semaphore _semaphore;
        int       _count;

        public  SemaphoreReference() {
            _count      = 0;
            _semaphore  = null;
        }

        public synchronized int addRef() {
            return ++_count;
        }
        public synchronized int release() {
            --_count;
            if(_count == 0) {
                _semaphore = null;
            }
            return _count;
        }
    }

    public static Semaphore create(int initialCount,int maxCount,String name) throws Exception {
        if(name != null) {
            if(_namedSemaphores.get(name) != null) {
                throw new Exception("Semaphore already exists!");
            }
        }
        Semaphore s = new Semaphore(maxCount,initialCount,name);
        if(name != null) {
            s._name = name;
            SemaphoreReference sr = new SemaphoreReference();
            sr._semaphore = s;
            sr.addRef();
            _namedSemaphores.put(name,sr);
        }
        return  s;
    }

    public static Semaphore open(String name) {
        SemaphoreReference sr = (SemaphoreReference)_namedSemaphores.get(name);
        if(sr == null)
            return null;
        sr.addRef();
        return sr._semaphore;
    }

    public static void close(Semaphore s) {
        if(s._name != null) {
            SemaphoreReference sr = (SemaphoreReference)_namedSemaphores.get(s._name);
            if(sr != null) {
                if(sr.release() == 0) {
                    _namedSemaphores.remove(s._name);
                }
            }
        }
    }

    /*
     *  Creates a new instance of a <B>Semaphore Object</B>
     *  @param  count   The maximum count for the Semaphore.
     */

    private Semaphore(int maxCount,int initialCount,String name) {
        _count    = initialCount;
        _maxCount = maxCount;
        _name     = name;
    }

    /**
     * Decrements the semaphore usage count, and blocks the calling thread
     */

    public synchronized void down() {
        
        if(_count <= 0) {
            
            try {
                wait();
            } catch(InterruptedException e) {
                
            }                       
        }
        
        if(_count > 0) {
            --_count;
        }
    }
    
    public synchronized int down(long timeout) {
        
        if(_count <= 0) {
            
            try {
                wait(timeout);
            } catch(InterruptedException e) {
                
            }
            
            if(_count <= 0)
                return -1;
        }
        
        if(_count > 0) {
            --_count;
        }      
        
        return 0;
    }

    public synchronized void up() {
        
        if(_count < _maxCount) {
            
            ++_count;
            
            if(_count == 1) {
                notifyAll();
            }
            
        }
    }

}

