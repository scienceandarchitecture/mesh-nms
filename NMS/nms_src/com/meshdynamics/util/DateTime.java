/*
 * Created on Dec 14, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.meshdynamics.util;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author sneha
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 * 
 * 
 */
 /*
  * File Revision History 
  * --------------------------------------------------------------------------------
  * | No  |Date         |  Comment                                        | Author |
  * --------------------------------------------------------------------------------
  * | 4   |Feb 14, 2007  |getTimeDate added                                |Abhijit |
  *--------------------------------------------------------------------------------
  * | 3   |Feb 14, 2007  |getTimeSpan added                                |Abhijit |
  * --------------------------------------------------------------------------------
  * |  2  |Feb 14, 2006 | display Date and time differently				   | Mithil |
  * --------------------------------------------------------------------------------
  * |  1  |Jan 24, 2006 | Changed date Time Class for 12:45:1 to 12:45:01 | Mithil |
  * --------------------------------------------------------------------------------
  * |  0  |Oct 13, 2004 | Created                                         | Anand  |
  * -------------------------------------------------------------------------------- 
  * ********************************************************************************/
public class DateTime {

	private static Date 	dd 						= new Date();
	private static Format 	dateTimeFormatter 		= new SimpleDateFormat("MMM dd, HH:mm:ss");
	private static Format 	fileDateTimeFormatter 	= new SimpleDateFormat("MMMdd-HH-mm-ss");
	
	public static String getDateTime(){
		dd.setTime(System.currentTimeMillis());
		return dateTimeFormatter.format(dd);
		
	}

	public static String getFileDateTime() {
		dd.setTime(System.currentTimeMillis());
		return fileDateTimeFormatter.format(dd);
	}
	
	public static String getDateTime(long timeInMillis){
		dd.setTime(timeInMillis);
		return dateTimeFormatter.format(dd);
	}

	public static String getTimeSpan(long timeInMillis) {
		String strTimeSpan = "secs";
		
		long timeInSecs = timeInMillis / 1000;
		long mins		= timeInSecs / 60;
		long secs		= timeInSecs % 60;
		strTimeSpan		= secs + strTimeSpan;
		long hrs		= mins / 60;
		mins			= mins % 60;
		long days		= hrs / 24;
		hrs				= hrs % 24;
		
		if(mins > 0)
			strTimeSpan		= mins + "mins " + strTimeSpan;
		
		if(hrs > 0)
			strTimeSpan		= hrs + "hrs " + strTimeSpan;
		
		if(days > 0)
			strTimeSpan		= days + "days " + strTimeSpan;
		
		return strTimeSpan;
	}
}
