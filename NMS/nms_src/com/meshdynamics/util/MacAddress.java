/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MACAddress.java
 * Comments : 
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;



public class MacAddress {
	
	/**
	 * All the MeshDynamics MacAddresses starts with 00:12:ce:...
	 */
	private static String 	MeshDynamicsMacAddress		= "00:12:CE";
	
	static{
		resetAddress 		= new MacAddress((short)0);
		undefinedAddress 	= new MacAddress((short)255);
		tempAddress			= new MacAddress((short)0);
	}
	  
	public short[] macAddress;
	public static MacAddress resetAddress;
	public static MacAddress undefinedAddress;
	public static MacAddress tempAddress;
		
    public MacAddress() {
        macAddress = new short[6];
    }

    public MacAddress(short initialval) {
      macAddress = new short[6];
      for(int i=0;i<6;i++){
      	macAddress[i] = initialval;
      }
    }

    public MacAddress(byte[] bytes) {
        macAddress = new short[6];
        setBytes(bytes);
    }
    
    public int hashCode(){
       return macAddress[0] * 1 +
              macAddress[1] * 2 +
              macAddress[2] * 3 +
              macAddress[3] * 4 +
              macAddress[4] * 5 +
              macAddress[5] * 6;
     }

     public byte[] getBytes(){

        byte []bt =  new byte[6];

        for(int i=0;i<6;i++){
          bt[i] = (byte)macAddress[i];
        }

        return bt;
     }
      
     public int getBytes(byte[] buffer,int position){
    	
    	  if(buffer.length < position + MacAddress.GetLength())
    		  return 0;
    		
    	  for(int i=0;i<macAddress.length;i++){
    		  buffer[position + i] = (byte)macAddress[i]; 		
    	  }
    		
    	  return macAddress.length;
      }

      public int setBytes(String newAddress){

        int i=0,j=0;
        if(newAddress.length() != 17){
          return 1;
        }

        try{

              for(i=0;i<newAddress.length();i++){

                String s    = newAddress.substring(i,i+2);
                Integer it  = Integer.valueOf(s,16);

                macAddress[j++] = it.shortValue();
                i += 2;

              }
        }
        catch(Exception e){
            return 1;
        }

        return 0;
      }

     public int setBytes(byte[] newBytes){

          try{
              for(int i=0;i<6;i++){

                  if(newBytes[i] < 0){
                      macAddress[i]  = (short)(128 + (128 + newBytes[i]));
                  }
                  else{
                    macAddress[i] = newBytes[i];
                  }
              }
          }
          catch(Exception e){
            return 1;
          }

          return 0;
     }

     public void resetAddress(){

      for(int i=0;i<6;i++){
          macAddress[i] = 0;
        }
     }

/*     
     private int getHexValue(int val){

        int ret=9;

        if(val >= 0 && val <= 9){
          return val;
        }

        if(val >= 97 && val <= 102)
            ret += (val - 96);

        if(val >= 65 && val <= 70)
            ret += (val - 64);

        return ret;
      }
*/
     
     private String hexString(short value){

        String  ret;
        int     quotient;
        char    r;

        ret = "";  //TODO ANAND B LEVEL remove Hardcoding

        if(value < 0){
          value  = (short)(128 + (128 + value));
        }

        while(value > 0){

          quotient = value / 16;

          if(quotient > 9 && quotient < 16){

            r         = 'A';
            r         += (quotient-10);
            ret       += r;
            quotient  = 0;

          }
          else{
            ret += quotient;
          }

          value = (byte)(value % 16);

          if(value > 9 && value < 16){

            r = 'A';

            r += (value-10);
            ret += r;

          }
          else{
            ret += value;
            }
          value = 0;
        }
        return ret;
      }

      public String toString(){

        String ret;
        ret = ""; //$NON-NLS-1$

        for(int i=0;i<6;i++){

          if(macAddress[i] == 0){
              ret += "00";  //TODO ANAND B LEVEL remove Hardcoding
          }
          else{
              ret += hexString(macAddress[i]);
          }

          ret += ":";  //uTODO ANAND B LEVEL remove Hardcoding

        }

        return ret = ret.substring(0,ret.length()-1);
      }

     public boolean equals(Object obj){

        boolean equal         = true;
        MacAddress newAddress = (MacAddress)obj;

        for(int i=0;i<6;i++){
          if(macAddress[i] != newAddress.macAddress[i]){
            equal = false;
            break;
          }
        }
        return equal;
      }

      public int write(OutputStream out){
    	byte[] twoBytes = new byte[2];
    	int bytesWritten = 0;
    	
      	for(int i=0;i<macAddress.length;i++){
      		Bytes.shortToBytes(macAddress[i],twoBytes);	
      		try {
    			out.write(twoBytes);
    			bytesWritten += 2;
    		} catch (IOException e) {
    		//	Mesh.logException(e);   			
    		}
      	}
      	return bytesWritten;
      }

      public int read(InputStream in){
    	byte[] twoBytes = new byte[2];
    	int bytesRead = 0;
    	
    	for(int i=0;i<macAddress.length;i++){
    		
    		try {
    			in.read(twoBytes);
    			bytesRead += 2;
    			macAddress[i] = Bytes.bytesToShort(twoBytes);
    		} catch (Exception e) {
    			//Mesh.logException(e);
    		}
    	}
    	return bytesRead;
      }

      public static int GetLength(){
    	return 6;  
      }

      public static boolean isMeshDynamicsMacaddress(String macAddress){
      	if(macAddress == null) {
      		return false;
      	}
      	return macAddress.startsWith(MeshDynamicsMacAddress);
      }
      
}
