
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : MD5.java
 * Comments : 
 * Created  : Apr 21, 2005
 * Author   : Bindu Khare
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date           |  Comment                                     | Author |
 * -----------------------------------------------------------------------------
 * |  2  |Jan 30, 2006   | Restructuring					            | Mithil |
 * -----------------------------------------------------------------------------
 * |  1  |Apr 26, 2005   | changed String param to byte array           | Bindu   |
 * -----------------------------------------------------------------------------
 * |  0  |Apr 21, 2005   | Created                                      | Bindu   |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.util;



public class MD5 {
	static {
		System.loadLibrary("MessageDigest5");
	}

	public native int keyGenerate(byte[] buffer, int bufferLength, char[] data);

}
