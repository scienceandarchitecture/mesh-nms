/*
 * Mutex.java
 *
 * Created on March 11, 2002, 8:18 PM
 */

package com.meshdynamics.util;

import  java.util.Hashtable;

/**
 * The Mutex class implements the
 * "mutex" synchronization primitive.
 * @author  Sriram
 */

public class Mutex {

    private Semaphore         _semaphore;
    private String            _name;
    private static Hashtable<String,MutexReference>  _namedMutexes = new Hashtable<String,MutexReference>();

    public static final int STATE_SIGNALLED     = 1;
    public static final int STATE_NOT_SIGNALLED = 0;

    private static class MutexReference {
        
        Mutex _mutex;
        int   _count;
        
        public  MutexReference() {
            _count  = 0;
            _mutex  = null;
        }
        public synchronized int addRef() {
            return ++_count;
        }
        public synchronized int release() {
            --_count;
            if(_count == 0) {
                _mutex = null;
            }
            return _count;
        }
    }

    public static Mutex create(int initialState,String name) throws Exception {
        if(name != null) {
            if(_namedMutexes.get(name) != null) {
                throw new Exception("Mutex already exists!");
            }
        }
        Mutex m = new Mutex(initialState);
        if(name != null) {
            m._name = name;
            MutexReference mr = new MutexReference();
            mr._mutex = m;
            mr.addRef();
            _namedMutexes.put(name,mr);
        }
        return  m;
    }

    public static Mutex open(String name) {
        MutexReference mr = (MutexReference)_namedMutexes.get(name);
        if(mr == null)
            return null;
        mr.addRef();
        return mr._mutex;
    }

    public static void close(Mutex m) {
        if(m._name != null) {
            MutexReference mr = (MutexReference)_namedMutexes.get(m._name);
            if(mr != null) {
                if(mr.release() == 0) {
                    _namedMutexes.remove(m._name);
                }
            }
        }
    }


    /** Creates a new instance of Mutex */

    private Mutex(int initialState) throws Exception {
        _semaphore  = Semaphore.create(initialState,1,null);
    }

    public int capture(long timeout) {
        return _semaphore.down(timeout);
    }
    
    public void capture() {
        _semaphore.down();
    }

    public void release() {
        _semaphore.up();
    }

}
