/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : IPAddress.java
 * Comments : 
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  1  |Apr 26, 2005 | getNextIP function added                        | Anand  |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.StringTokenizer;


public class IpAddress {
	
	
	static{
		resetAddress 		= new IpAddress();
		tempAddress			= new IpAddress();
	}
	
	public static IpAddress resetAddress;
	public static IpAddress tempAddress;
	
	public static IpAddress getNextIP(IpAddress ip) {
		
		IpAddress newIp = new IpAddress();
		for(int i=0; i < 4; i++)
			newIp.ipAddress[i] = ip.ipAddress[i];
		
		newIp.ipAddress[3]++;
		if(newIp.ipAddress[3] > 255) {
			newIp.ipAddress[3] = 0;
			newIp.ipAddress[2]++;
			if(newIp.ipAddress[2] > 255) {
				newIp.ipAddress[2] = 0;
				newIp.ipAddress[1]++;
				if(newIp.ipAddress[1] > 255) {
					newIp.ipAddress[1] = 0;
					newIp.ipAddress[0]++;
					if(newIp.ipAddress[0] > 255) {
						newIp.ipAddress[0] = 0;
					}
				}
			}
		}
		return newIp;
	}
	
	
	public IpAddress() {
		ipAddress = new short[4];
	}
	
	public byte[] getBytes(){

		byte []bt =  new byte[4];

		for(int i=0;i<4;i++){
			bt[i] = (byte)ipAddress[i];
		}

		return bt;
	}

	public int getBytes(byte[] buffer,int position){
	
		if(buffer.length < position + IpAddress.GetLength())
			return 0;
		
		for(int i=0;i<ipAddress.length;i++){
			buffer[position + i] = (byte)ipAddress[i]; 		
		}
		
		return ipAddress.length;
	}

	public int setBytes(String newAddress){
		
		int i=0;
		
		if(newAddress.equals("")){
			return 1;
		}
		
		StringTokenizer st = new StringTokenizer(newAddress,".");
		if(st.countTokens() != 4){
			return 1;
		}
   
		while(st.hasMoreTokens()){
			try{
				ipAddress[i] = (short)Integer.parseInt(st.nextToken());
				i++;
			}
			catch(NumberFormatException e){
				return 1;
	 		}
   		}
   		
		return 0;
	}

	public int setBytes(byte[] newBytes){

		try{
			for(int i=0;i<4;i++){

				if(newBytes[i] < 0){
					ipAddress[i]  = (short)(128 + (128 + newBytes[i]));
				}
				else{
				  ipAddress[i] = newBytes[i];
				}
			}
		}
		catch(Exception e){
		  return 1;
		}

		return 0;
	}

	public void resetAddress(){

		for(int i=0;i<4;i++){
			ipAddress[i] = 0;
		}

	}


	public String toString(){

	  String ret;
	  ret = ""; //$NON-NLS-1$

	  for(int i=0;i<4;i++){

		if(ipAddress[i] == 0){
			ret += "0"; //$NON-NLS-1$
		}
		else{
			ret += ipAddress[i] + "";
		}

		ret += "."; //$NON-NLS-1$

	  }

	  return ret = ret.substring(0,ret.length()-1);
	}

	public boolean equals(Object obj){

		boolean equal         = true;
	  	IpAddress newAddress = (IpAddress)obj;

	  	for(int i=0;i<4;i++){
			if(ipAddress[i] != newAddress.ipAddress[i]){
		  		equal = false;
		 		break;
			}
		}

	  	return equal;
	}

	public int write(OutputStream out){
	  byte[] twoBytes = new byte[2];
	  int bytesWritten = 0;
	
	  for(int i=0;i<ipAddress.length;i++){
		  Bytes.shortToBytes(ipAddress[i],twoBytes);	
		  try {
			  out.write(twoBytes);
			  bytesWritten += 2;
		  } catch (IOException e) {
			  e.printStackTrace();
		  }
	  }
	  return bytesWritten;
	}

	public int read(InputStream in){
	  byte[] twoBytes = new byte[2];
	  int bytesRead = 0;
	
	  for(int i=0;i<ipAddress.length;i++){
		
		  try {
			  in.read(twoBytes);
			  bytesRead += 2;
			  ipAddress[i] = Bytes.bytesToShort(twoBytes);
		  } catch (IOException e) {
			  e.printStackTrace();
		  }
	  }
	  return bytesRead;
	}

	public static int GetLength(){
		return 4;
	}
	
	public short[] ipAddress;

}
