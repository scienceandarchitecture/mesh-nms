
/********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : RSN_PSK.java
 * Comments : 
 * Created  : Feb 24, 2005
 * Author   : Bindu Khare
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * -----------------------------------------------------------------------------
 * | No  |Date        |  Comment                                        | Author |
 * -----------------------------------------------------------------------------
 * |  1  |Jan 30, 2006   | Restructuring                                | Mithil |
 * -----------------------------------------------------------------------------
 * |  0  |Feb 24, 2005   | Created                                      | Bindu   |
 * -----------------------------------------------------------------------------
 ********************************************************************************/

package com.meshdynamics.util;



public class RSN_PSK {
	static {
		System.loadLibrary("RSN_PSK_Key");
	}
	
	public native  byte[] PSKKeyGenerate(String passPhrase, String ssid, int ssidLength);	
}
