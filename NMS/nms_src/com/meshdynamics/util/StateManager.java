package com.meshdynamics.util;



/**
 * @author abhishek
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class StateManager {

	private 		IStateEventHandler 	provider;
	private 		int 				state;
	private			int					maxCnt;
	
	public StateManager(IStateEventHandler provider, final int maxStateCnt, int initialState){
		state			=	initialState;
		this.provider	=	provider;
		maxCnt			=	maxStateCnt; 
	}
	public void init(){
		provider.init();
	}
	
	public void triggerState(int currentState){
		state	=	provider.stateFunction(currentState); 
	}
	
	public int getState() {
		return state;
	}
	public int getMaxCnt() {
		return maxCnt;
	}
}
