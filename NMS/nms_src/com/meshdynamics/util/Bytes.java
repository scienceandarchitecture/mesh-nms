/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : Bytes.java
 * Comments : 
 * Created  : Sep 28, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Sep 28, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.util;

public class Bytes {
    private static int MakePositive(int val){
    	
    	if(val < 0){
    		val = (256 + val);  	
    	}
    	
    	return val;
      }
      
      public static void shortToBytes(short s,byte[] b){

      	b[0]  = (byte)((s & 0x00FF));
        b[1]  = (byte)((s & 0xFF00) >> 8);
        
      }

      public static void shortToBytes(short s,byte[] b,int position){

    	b[position + 0]  = (byte)((s & 0x00FF));  	
    	b[position + 1]  = (byte)((s & 0xFF00) >> 8);
    	
      }

      public static void intToBytes(int i,byte[] b) {

        b[0]  = (byte)((i & 0x000000FF));
        b[1]  = (byte)((i & 0x0000FF00) >> 8);
        b[2]  = (byte)((i & 0x00FF0000) >> 16);
        b[3]  = (byte)((i & 0xFF000000) >> 24);

      }

      public static void intToBytes(int i,byte[] b,int position) {

    	b[position + 0]  = (byte)((i & 0x000000FF));
    	b[position + 1]  = (byte)((i & 0x0000FF00) >> 8);
    	b[position + 2]  = (byte)((i & 0x00FF0000) >> 16);
    	b[position + 3]  = (byte)((i & 0xFF000000) >> 24);
    	
      }

      public static void intToBytesHBO(int i,byte[] b) {

          b[3]  = (byte)((i & 0x000000FF));
          b[2]  = (byte)((i & 0x0000FF00) >> 8);
          b[1]  = (byte)((i & 0x00FF0000) >> 16);
          b[0]  = (byte)((i & 0xFF000000) >> 24);

        }
      
      public static final void longToBytes(long l,byte[] b) {

        b[0]  = (byte)((l & 0x00000000000000FFL) >> 0);
        b[1]  = (byte)((l & 0x000000000000FF00L) >> 8);
        b[2]  = (byte)((l & 0x0000000000FF0000L) >> 16);
        b[3]  = (byte)((l & 0x00000000FF000000L) >> 24);
        b[4]  = (byte)((l & 0x000000FF00000000L) >> 32);
        b[5]  = (byte)((l & 0x0000FF0000000000L) >> 40);
        b[6]  = (byte)((l & 0x00FF000000000000L) >> 48);
        b[7]  = (byte)((l & 0xFF00000000000000L) >> 56);

      }

      public static final void longToBytes(long l,byte[] b,int position) {

    	b[position + 0]  = (byte)((l & 0x00000000000000FFL) >> 0);
    	b[position + 1]  = (byte)((l & 0x000000000000FF00L) >> 8);
    	b[position + 2]  = (byte)((l & 0x0000000000FF0000L) >> 16);
    	b[position + 3]  = (byte)((l & 0x00000000FF000000L) >> 24);
    	b[position + 4]  = (byte)((l & 0x000000FF00000000L) >> 32);
    	b[position + 5]  = (byte)((l & 0x0000FF0000000000L) >> 40);
    	b[position + 6]  = (byte)((l & 0x00FF000000000000L) >> 48);
    	b[position + 7]  = (byte)((l & 0xFF00000000000000L) >> 56);

      }

      public static short byteToShort(byte b){

    	short s = b;
    	
    	s = (short) MakePositive(b);
     	
      	return s;
      }
      
      public static short bytesToShort(byte[] b){
    	
    	int i = 0;

    	i = 	MakePositive(b[0]);
    	i |= 	(MakePositive(b[1]) << 8);
    	
        return (short)i;
      }

      public static short bytesToShort(byte[] b,int position){
    	
    	int i = 0;

    	i = 	MakePositive(b[position + 0]);
    	i |= 	(MakePositive(b[position + 1]) << 8);

    	return (short)i;
      }

      public static int bytesToInt(byte[] b) {

        int i = 0;

    	i = i | MakePositive(b[0]);
    	i = i | (MakePositive(b[1]) << 8);
    	i = i | (MakePositive(b[2]) << 16);
        i = i | (MakePositive(b[3]) << 24);

        return i;
        
      }

      public static int bytesToIntHBO(byte[] b) {

          int i = 0;

          i = i | MakePositive(b[3]);
          i = i | (MakePositive(b[2]) << 8);
          i = i | (MakePositive(b[1]) << 16);
          i = i | (MakePositive(b[0]) << 24);

          return i;
          
        }
      
      public static int bytesToInt(byte[] b,int position) {

    	int i = 0;
    	
    	i =  i | MakePositive(b[position + 0]);
    	i =  i | (MakePositive(b[position + 1]) << 8);
    	i =  i | (MakePositive(b[position + 2]) << 16);
    	i =	 i | (MakePositive(b[position + 3]) << 24);
    	
    	return i;
    	
      }

      public static long bytesToLong(byte[] b) {

        int l = 0;

    	l = l | MakePositive(b[0]);
    	l = l | (MakePositive(b[1]) << 8);
    	l = l | (MakePositive(b[2]) << 16);
    	l = l | (MakePositive(b[3]) << 24);
    	l = l | (MakePositive(b[4]) << 32);
    	l = l | (MakePositive(b[5]) << 40);
    	l = l | (MakePositive(b[6]) << 48);	
    	l = l | (MakePositive(b[7]) << 56); 

        return l;
        
      }

      public static long bytesToLong(byte[] b,int position) {

    	int l = 0;

    	l = l | (MakePositive(b[position + 0]) << 0);
    	l = l | (MakePositive(b[position + 1]) << 8);
    	l = l | (MakePositive(b[position + 2]) << 16);
    	l = l | (MakePositive(b[position + 3]) << 24);
    	l = l | (MakePositive(b[position + 4]) << 32);
    	l = l | (MakePositive(b[position + 5]) << 40);
    	l = l | (MakePositive(b[position + 6]) << 48);
    	l = l | (MakePositive(b[position + 7]) << 56);

    	return l;
    	
      }
}
