/**
 * MeshDynamics 
 * -------------- 
 * File     : WEPConfiguration.java
 * Comments : 
 * Created  : Feb 01, 2007
 * Author   : Abhishek 
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * ----------------------------------------------------------------------------------
 * | No  |Date           |  Comment                                   | Author       |
 *  ----------------------------------------------------------------------------------
 * | 0  | Mar 01, 2007   | Created                        			  | Abhishek     |
 * ----------------------------------------------------------------------------------
 */
package com.meshdynamics.util;

public class HexHelper {

	public static short[] stringToHex(String str) {
		
		short[] ret = new short[str.length()/2];
		for(int i = 0, j = 0 ; i < str.length(); i = i+2, j++) {
			short totalValue;
			char token	= str.charAt(i);
			short val1	= getCharHex(token);			

			token 		= str.charAt(i+1);
			short val2  = getCharHex(token);
			
			totalValue 	= (short) ((short)(val1 * 16) + val2);
			ret[j] 		= totalValue;			
		}
		return ret;
	}
	
	public static  short getCharHex(char ch){
		switch(ch) {
			case 'a':
			case 'A':
				return 10;
			
			case 'b':
			case 'B':
				return 11;
				
			case 'c':
			case 'C':
				return 12;
				
			case 'd':
			case 'D':
				return 13;
	
			case 'e':
			case 'E':
				return 14;
				
			case 'f':
			case 'F':
				return 15;
			
			default:
				return Short.parseShort(String.valueOf(ch));
		}		
	}
	
	public static String toHex(short[] arrKey){
		
	    String key = new String();
	    key = "";
	    if(arrKey == null)
	    	return key;
        String hexDigits = "0123456789ABCDEF";
	    for (int i = 0; i < arrKey.length; i++) {
	    	int c = arrKey[i];
	    	if (c < 0) 
	    		c += 256;
	    	int hex1 = c & 0xF;
	    	int hex2 = c >> 4;			
	    	
	    	key += hexDigits.substring(hex2,hex2+1);
	    	key += hexDigits.substring(hex1,hex1+1);	    	
	    }	    
	    return key;
	}

	public static String toHexString(String s) {
		String key	= "";
		for(int i = 0; i < s.length(); i++) {
			int ch 	= (int)s.charAt(i) ;
			String hex = Integer.toHexString( ch & 0xFF);
			if(hex.length() == 1)
				hex = "0" + hex;
			key 	+= hex;	   
		}
	  	return key;
	  }
	
	public static boolean compareArray(short[] arr1, short[] arr2){
		
		if((arr1 == null) || (arr2 == null)) {
			return false;
		}
		
		if(arr1.length != arr2.length) {
			return false;
		}
			
		for(int i=0; i< arr1.length; i++) {
			if( arr1[i]!= arr2[i]){
				return false;
			}
		}
		
		return true;
	}
}
