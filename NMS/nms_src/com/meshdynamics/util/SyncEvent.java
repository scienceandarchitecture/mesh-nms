/*
 * Event.java
 *
 * Created on March 11, 2002, 8:26 PM
 */

package com.meshdynamics.util;

import  java.util.Hashtable;

/**
 * The Event class implements the
 * Event synchronization primitive.
 *
 * @author  Sriram
 */
public class SyncEvent {

    private Mutex             _mutex;
    private String            _name;
    private boolean           _autoReset;
    private static Hashtable<String,EventReference>  _namedEvents = new Hashtable<String,EventReference>();

    public static final int STATE_SIGNALLED     = 1;
    public static final int STATE_NOT_SIGNALLED = 0;

    private static class EventReference {
        SyncEvent     _event;
        int       _count;
        public  EventReference() {
            _count      = 0;
            _event  = null;
        }
        public synchronized int addRef() {
            return ++_count;
        }
        public synchronized int release() {
            --_count;
            if(_count == 0) {
                _event = null;
            }
            return _count;
        }
    };

    public static SyncEvent create(int initialState,boolean autoReset,String name) throws Exception {
        if(name != null) {
            if(_namedEvents.get(name) != null) {
                throw new Exception("Event already exists!");
            }
        }
        SyncEvent e = new SyncEvent(initialState,autoReset);
        if(name != null) {
            e._name = name;
            EventReference er = new EventReference();
            er._event = e;
            er.addRef();
            _namedEvents.put(name,er);
        }
        return  e;
    }

    public static SyncEvent open(String name) {
        EventReference er = (EventReference)_namedEvents.get(name);
        if(er == null)
            return null;
        er.addRef();
        return er._event;
    }

    public static void close(SyncEvent e) {
        if(e._name != null) {
            EventReference er = (EventReference)_namedEvents.get(e._name);
            if(er != null) {
                if(er.release() == 0) {
                    _namedEvents.remove(e._name);
                }
            }
        }
    }

    /** Creates a new instance of Event */

    private SyncEvent(int initialState,boolean autoReset) throws Exception {
        _mutex      = Mutex.create(initialState,null);
        _autoReset  = autoReset;
    }

    public void signalEvent() {
        _mutex.release();
    }

    public void resetEvent() {
        _mutex.release();
        _mutex.capture();
    }

    public int waitOn(long timeout) {
        
        int ret;
        
        ret = _mutex.capture(timeout);
        
        if(ret == 0 && _autoReset) {
            _mutex.release();
        }
        
        return ret;
    }
    
    public void waitOn() {
        _mutex.capture();
        if(_autoReset) {
            _mutex.release();
        }
    }
}
