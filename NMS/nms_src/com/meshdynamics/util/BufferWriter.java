/**********************************************************************************
 * MeshDynamics 
 * -------------- 
 * File     : BufferWriter.java
 * Comments : 
 * Created  : Oct 13, 2004
 * Author   : Anand Bhange
 * Copyright (c) Advanced Cybernetics Group, Inc & MeshDynamics, Inc

 * File Revision History 
 * --------------------------------------------------------------------------------
 * | No  |Date         |  Comment                                        | Author |
 * --------------------------------------------------------------------------------
 * |  0  |Oct 13, 2004 | Created                                         | Anand  |
 * --------------------------------------------------------------------------------
 **********************************************************************************/

package com.meshdynamics.util;

public class BufferWriter {

    byte[] 		buffer;
    int 		pos;
    
    /**
     * @param len
     */
    public BufferWriter(int len) {
        buffer = new byte[len];
        pos = 0;
    }
    
    public void reset() {
        pos = 0;
    }

    /**
     * @param c
     */
    public void writeByte(short c) {
        buffer[pos++] = (byte)c;
    }

    /**
     * @param meshId
     */
    public void writeString(String str) {
        int len = str.length();
        if(len > 255)
            len = 255;
        
        buffer[pos++] = (byte)len;
        byte[] buf = str.getBytes();
        for(int i=0; i < buf.length; i++) {
            buffer[pos++] = buf[i];
        }
    }

    /**
     * @return
     */
    public byte[] getBytes() {
        return buffer;
    }

    /**
     * @param dsMacAddress
     */
    public void writeMacAddress(MacAddress dsMacAddress) {
    	int i=0;
 		byte[] mac = dsMacAddress.getBytes();
    	for(i=0;i<6;i++){
 			buffer[pos] = mac[i];
 			pos++;
 		}
 	}

	/**
	 * @param rtsThreshold
	 */
	public void writeInt(long value) {
		byte[] bts = new byte[8];
        Bytes.longToBytes(value,bts,0);
        for(int i=0; i<4; i++) {
        	buffer[pos++] = bts[i];
        }
	}
	

	/**
	 * @param rtsThreshold
	 */
	public void writeShort(int value) {
		byte[] bts = new byte[4];
        Bytes.intToBytes(value,bts,0);
        for(int i=0; i<2; i++) {
        	buffer[pos++] = bts[i];
        }
	}


	/**
	 * @param ipAddress
	 * 
	 */
	public void writeIPAddress(IpAddress ipAddress) {
		byte[] ipBuff = ipAddress.getBytes();
		for(int index=0;index<ipBuff.length;index++){
			buffer[pos++] = ipBuff[index];
		}
	}

	/**
	 * @param bytes
	 */
	public void writeBytes(byte[] bytes) {
		//System.arraycopy(bytes, 0, buffer, pos, bytes.length);
		for(int i=0; i < bytes.length; i++) {
			buffer[pos++] = bytes[i];
		}
	}
	
}
