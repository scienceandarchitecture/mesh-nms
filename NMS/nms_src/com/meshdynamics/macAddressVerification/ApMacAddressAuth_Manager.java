package com.meshdynamics.macAddressVerification;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import javax.websocket.Session;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meshdynamics.meshviewer.imcppackets.MacAddressRequestResponsePacket;
import com.meshdynamics.meshviewer.imcppackets.PacketFactory;
import com.meshdynamics.meshviewer.imcppackets.helpers.IMCP_MacInfo;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.ApDataController;
import com.meshdynamics.meshviewer.mesh.ap.FirmwareRequest;
import com.meshdynamics.meshviewer.mesh.ap.IApFwUpdateListener;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.nmsui.MDBuildServer;
import com.meshdynamics.nmsui.MDManServer;
import com.meshdynamics.util.MacAddress;

public class ApMacAddressAuth_Manager implements IMacAddress_Config {
	public static final int macVerifiction = 614;
	public static final int waitingCount = 30;
	private String boardMac;
	private short rCount;
	private short vCount;
	private short appCount;
	private List<IMCP_MacInfo> raddr;
	private List<IMCP_MacInfo> vaddr;
	private List<IMCP_MacInfo> app_addr;

	private ApDataController dataController;
	private AccessPoint accessPoint;
	private FirmwareRequest firmwareRequest;
	private IConfigStatusHandler statusHandler;

	public ApMacAddressAuth_Manager(ApDataController dataController,
			AccessPoint ap) {
		this.dataController = dataController;
		this.accessPoint = ap;
		statusHandler = dataController.getConfigStatusHandler();
		firmwareRequest = new FirmwareRequest();
	}

	public void updateMacInfo(MacAddressRequestResponsePacket packet) {
		boardMac = packet.getDsMacAddress().toString();
		rCount = packet.getrCount();
		vCount = packet.getvCount();
		appCount = packet.getAppCount();
		raddr = packet.getRaddr();
		vaddr = packet.getVaddr();
		app_addr = packet.getApp_addr();
		MDManServer.readSettings();
		String server = MDManServer.server;
		int port = MDManServer.port;
		statusHandler
				.packetReceived(PacketFactory.IMCP_MAC_ADDR_REQUEST_RESPONSE);
		accessPoint.showMessage(accessPoint, "Device MAC verification",
				"successfull", "");
		/*
		 * if(pingHost(server,port,5000)){
		 * if(contactMDMan(boardMac,server,port))
		 * statusHandler.packetReceived(PacketFactory
		 * .IMCP_MAC_ADDR_REQUEST_RESPONSE); }else{
		 * accessPoint.showMessage(accessPoint, "Device MAC verification",
		 * "Network UNReachable,Check MDMan server", ""); }
		 */
	}

	/***
	 * 
	 * @param macConfig
	 * @return
	 */
	private boolean contactMDMan(String macConfig, String server, int port) {
		boolean flag = false;

		try {
			RegisterMac registerMac = new RegisterMac();
			registerMac.setBoardMacAddress(macConfig);
			registerMac.setrMacAddress(getMacInfo(getRaddr()));
			registerMac.setVlanMacAddress(getMacInfo(getVaddr()));
			registerMac.setAppMacAddress(getMacInfo(getApp_addr()));
			ObjectMapper mapper = new ObjectMapper();
			String jsonInString = mapper.writeValueAsString(registerMac);
			// get server details
			String generateUrl = "http://" + server + ":" + port
					+ "/MDManufacturer/macVerification/register";
			URL url = new URL(generateUrl);
			// open connection
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoOutput(true); // this is to enable writing
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			OutputStream out = (OutputStream) connection.getOutputStream();
			out.write(jsonInString.getBytes());
			out.flush();
			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				System.out.println("Failed : HTTP error code : "
						+ connection.getResponseCode());
				accessPoint.showMessage(
						accessPoint,
						"Device MAC verification",
						"Failed : HTTP error code : "
								+ connection.getResponseCode(), "");
				return flag;
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(connection.getInputStream())));

			String output;
			while ((output = br.readLine()) != null) {
				JSONObject jsonObject = (JSONObject) new JSONParser()
						.parse(output);
				ResponseEntity entity = dataConversion(jsonObject);
				if (entity.getResponseCode() == macVerifiction) {
					flag = true;
					accessPoint.showMessage(accessPoint,
							"Device MAC verification", "successfull", "");
					return flag;
				} else {
					flag = true;
					sendReboot();
				}
			}
			connection.disconnect();
		} catch (MalformedURLException e) {
			accessPoint.showMessage(accessPoint, "Device MAC verification",
					"Check MDMan Server Details", "");
			System.out.println(e.getMessage());
		} catch (IOException e) {
			accessPoint.showMessage(accessPoint, "Device MAC verification",
					"Check MDMan Server", "");
			System.out.println(e.getMessage());
		} catch (ParseException e) {
			accessPoint.showMessage(accessPoint, "Device MAC verification",
					e.getMessage(), "");
			System.out.println(e.getMessage());
		} catch (Exception e) {
			accessPoint.showMessage(accessPoint, "Device MAC verification",
					e.getMessage(), "");
			System.out.println(e.getMessage());
		}
		return flag;
	}

	private void sendReboot() {
		dataController.haltNode();
	}

	/***
	 * @param macAddress
	 * @return
	 */
	public synchronized boolean buildImage(String macAddress,
			IApFwUpdateListener listener, String deviceType, String version) {
		// open web socket connection
		String mdserver = MDBuildServer.server;
		int mdport = MDBuildServer.port;
		boolean serverStatus = pingHost(mdserver, mdport, 5000);
		if (!serverStatus) {
			listener.notifyUpdateStatus(Mesh.FW_BUILD_IMAGE_REQUEST,
					"MDBUILD Server Not reachable");
			return false;
		}
		Session session = WebSocketClient.socketOpen(firmwareRequest
				.getMacOfCurrentDevice());
		if (session == null) {
			listener.notifyUpdateStatus(Mesh.FW_BUILD_IMAGE_REQUEST,
					"Problem In Websocket Opening");
			return false;
		}
		List<String> authQueue = WebSocketClient.queue;
		String authKey;
		try {
			while (authQueue.isEmpty() == true) {
				Thread.sleep(2000);
			}
			authKey = authQueue.get(0);
			authQueue.clear();
		} catch (InterruptedException e1) {
			return false;
		}
		String[] authArr = authKey.split(":");
		MacAddress_Config macConfig = new MacAddress_Config();
		boolean flag = false;
		Status status = null;
		try {
			macConfig.setBoardMacAddress(macAddress);
			macConfig.setModelConf(accessPoint.getModel() + ".conf");
			macConfig.setTarget(deviceType);
			macConfig.setrMacAddress(getMacInfo(getRaddr()));
			macConfig.setVlanMacAddress(getMacInfo(getVaddr()));
			macConfig.setAppMacAddress(getMacInfo(getApp_addr()));
			macConfig.setVersion(version);
			status = firmwareRequest.buildImage(macConfig, authArr[1]);
			if (status != null) {
				if (status.getResponseCode() == 201) {
					listener.notifyUpdateStatus(Mesh.FW_BUILD_IMAGE_REQUEST,
							"Target Generating,wait for Response");
					flag = true;
					if (session != null) {
						try {
							int count = 0;
							while (authQueue.isEmpty() == true) {
								Thread.sleep(4000);
								count++;
								if (count == waitingCount)
									return false;
							}
							authQueue.clear();
						} catch (InterruptedException e1) {
							return false;
						}
						listener.notifyUpdateStatus(
								Mesh.FW_BUILD_IMAGE_REQUEST,
								"Download starting");
						firmwareRequest.downloadImage(
								(String) status.getResponseBody(),
								status.getSignedKey(), status.getUniqueId());

					}
					listener.notifyUpdateStatus(Mesh.FW_BUILD_IMAGE_REQUEST,
							"Firmware Image building...");
				} else if (status.getResponseCode() == 202) {
					listener.notifyUpdateStatus(Mesh.FW_BUILD_IMAGE_REQUEST,
							"Download starting");
					firmwareRequest.downloadImage(
							(String) status.getResponseBody(),
							status.getSignedKey(), status.getUniqueId());
				} else if (status.getResponseCode() == 501) {
					listener.notifyUpdateStatus(Mesh.FW_BUILD_IMAGE_REQUEST,
							status.getResponseMessage());
				} else {
					listener.notifyUpdateStatus(Mesh.FW_BUILD_IMAGE_REQUEST,
							"Build server Not reachable...");
				}
			}
		} catch (Exception e) {
			listener.notifyUpdateStatus(Mesh.FW_BUILD_IMAGE_REQUEST,
					"NMS problem...");
			e.printStackTrace();

		} finally {
			if (session != null) {
				try {
					session.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return flag;
	}

	private ResponseEntity dataConversion(JSONObject jsonObject) {
		ResponseEntity entity = new ResponseEntity();
		entity.setData((Object) jsonObject.get("data"));
		entity.setMessage((String) jsonObject.get("message"));
		entity.setResponseCode((int) (long) jsonObject.get("responseCode"));
		entity.setStatus((int) (long) jsonObject.get("status"));
		return entity;
	}

	@Override
	public String getBoardMac() {
		return boardMac;
	}

	@Override
	public void setBoardMac(String boardMac) {
		this.boardMac = boardMac;

	}

	@Override
	public short getRCount() {
		return rCount;
	}

	@Override
	public void setRCount(short rCount) {
		this.rCount = rCount;
	}

	@Override
	public short getVCount() {
		return vCount;
	}

	@Override
	public void setVCount(short vCount) {
		this.vCount = vCount;
	}

	@Override
	public short getAPPCount() {
		return appCount;
	}

	@Override
	public void setAPPCount(short appCount) {
		this.appCount = appCount;

	}

	@Override
	public void setraddr(List<IMCP_MacInfo> raddr) {
		this.raddr = raddr;
	}

	@Override
	public List<IMCP_MacInfo> getRaddr() {
		return raddr;
	}

	@Override
	public void setVaddr(List<IMCP_MacInfo> vaddr) {
		this.vaddr = vaddr;
	}

	@Override
	public List<IMCP_MacInfo> getVaddr() {
		return vaddr;
	}

	@Override
	public void setApp_addr(List<IMCP_MacInfo> app_addr) {
		this.app_addr = app_addr;
	}

	@Override
	public List<IMCP_MacInfo> getApp_addr() {
		return app_addr;
	}

	public List<String> getMacInfo(List<IMCP_MacInfo> macAddressList) {
		List<String> list = new ArrayList<String>();
		if (macAddressList != null && macAddressList.size() > 0) {
			for (IMCP_MacInfo macInfo : macAddressList) {
				list.add(macInfo.getMacAddress().toString());
			}
		}
		return list;
	}

	public static boolean pingHost(String host, int port, int timeout) {
		try (Socket socket = new Socket()) {
			socket.connect(new InetSocketAddress(host, port), timeout);
			return true;
		} catch (IOException e) {
			return false; // Either timeout or unreachable or failed DNS lookup.
		}
	}
}
