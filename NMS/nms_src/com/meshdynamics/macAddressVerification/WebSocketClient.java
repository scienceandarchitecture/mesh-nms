package com.meshdynamics.macAddressVerification;



import java.net.URI;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import com.meshdynamics.nmsui.MDBuildServer;


@ClientEndpoint
public class WebSocketClient{
	public static String buildUrl=null;
	public  static List<String> queue=new ArrayList<String>(1) ;
	private static String server;
	private static int    port;
	static{
		MDBuildServer.readSettings();
		server=MDBuildServer.server;
		port  =MDBuildServer.port; 
	}
	@OnMessage
	public void echoBinaryMessage(String message,Session session) {
		System.out.println("Received msg: " + message);
		queue.add(message);		
	}

	public static Session socketOpen(String clientMacAddress) {
	Session session = null;
     
		try {			
			final WebSocketContainer container = ContainerProvider.getWebSocketContainer();	
			System.out.println("Connecting to Web Socket server....");
			String url="ws://"+server+":"+port+"/MDBuild/build";
			session = container.connectToServer(WebSocketClient.class, URI.create(url));
			if(session.isOpen()){
				System.out.println("Connection Established! "+session.getId());	
			}
			session.getBasicRemote().sendText(clientMacAddress);
			} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			return null;

		} finally {
			/*if (session != null) {
				try {
					session.close();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}

			}*/

		}
		
		 
		return session;

	}

	public static ByteBuffer byteArrayToByteBuffer(byte[] bytes) {
	    final ByteBuffer ret = ByteBuffer.wrap(new byte[bytes.length]);
	    ret.put(bytes);
	    ret.flip();
	    return ret;	
	}


}