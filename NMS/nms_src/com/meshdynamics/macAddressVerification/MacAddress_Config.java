package com.meshdynamics.macAddressVerification;

import java.io.Serializable;
import java.util.List;

public class MacAddress_Config implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String       target;    
	private String       boardMacAddress;     
	private String       modelConf;     
	private List<String> rMacAddress; 
    private List<String> vlanMacAddress;    
   	private List<String> appMacAddress;     
	private String       version;    
    private String       clientMacAddress;
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getBoardMacAddress() {
		return boardMacAddress;
	}
	public void setBoardMacAddress(String boardMacAddress) {
		this.boardMacAddress = boardMacAddress;
	}
	public String getModelConf() {
		return modelConf;
	}
	public void setModelConf(String modelConf) {
		this.modelConf = modelConf;
	}
	public List<String> getrMacAddress() {
		return rMacAddress;
	}
	public void setrMacAddress(List<String> rMacAddress) {
		this.rMacAddress = rMacAddress;
	}
	public List<String> getVlanMacAddress() {
		return vlanMacAddress;
	}
	public void setVlanMacAddress(List<String> vlanMacAddress) {
		this.vlanMacAddress = vlanMacAddress;
	}
	public List<String> getAppMacAddress() {
		return appMacAddress;
	}
	public void setAppMacAddress(List<String> appMacAddress) {
		this.appMacAddress = appMacAddress;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getClientMacAddress() {
		return clientMacAddress;
	}
	public void setClientMacAddress(String clientMacAddress) {
		this.clientMacAddress = clientMacAddress;
	}
	
	
}
