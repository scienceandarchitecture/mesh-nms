package com.meshdynamics.macAddressVerification;

import java.util.List;

import com.meshdynamics.meshviewer.imcppackets.helpers.IMCP_MacInfo;

public interface IMacAddress_Config {
	public String getBoardMac();
	public void setBoardMac(String boardMac);
	public short getRCount();
	public void setRCount(short rCount);
	public short getVCount();
	public void setVCount(short vCount);
	public short getAPPCount();
	public void setAPPCount(short appCount);
	public void setraddr(List<IMCP_MacInfo>  raddr);
	public List<IMCP_MacInfo> getRaddr();
	public void setVaddr(List<IMCP_MacInfo> vaddr);
	public List<IMCP_MacInfo>  getVaddr();
	public void  setApp_addr(List<IMCP_MacInfo>  app_addr);
	public List<IMCP_MacInfo>  getApp_addr();
	}
