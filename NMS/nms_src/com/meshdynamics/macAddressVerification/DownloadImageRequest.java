package com.meshdynamics.macAddressVerification;

public class DownloadImageRequest {

	private       String         uniqueId;
	private       String         imageName;
	private       String         clientMacAddress;
		
	
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getClientMacAddress() {
		return clientMacAddress;
	}
	public void setClientMacAddress(String clientMacAddress) {
		this.clientMacAddress = clientMacAddress;
	}
	
	
	
}
