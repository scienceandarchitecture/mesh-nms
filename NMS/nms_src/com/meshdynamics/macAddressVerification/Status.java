package com.meshdynamics.macAddressVerification;

public class Status {
    private int     responseCode;	
	private String  responseMessage;	
	private Object  responseBody;	
	private String  uniqueId;
	private String  macAddressDigest;
	private String  signedKey;
	
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public Object getResponseBody() {
		return responseBody;
	}
	public void setResponseBody(Object responseBody) {
		this.responseBody = responseBody;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getMacAddressDigest() {
		return macAddressDigest;
	}
	public void setMacAddressDigest(String macAddressDigest) {
		this.macAddressDigest = macAddressDigest;
	}
	public String getSignedKey() {
		return signedKey;
	}
	public void setSignedKey(String signedKey) {
		this.signedKey = signedKey;
	}
	

}
