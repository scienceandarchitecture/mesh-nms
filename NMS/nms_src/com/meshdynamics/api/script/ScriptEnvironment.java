/*
 * Created Date: Oct 6, 2008
 * Author : Sriram
 */
package com.meshdynamics.api.script;

import java.io.Reader;

public interface ScriptEnvironment {    
    public void execute(String script) throws Exception;
    public void execute(Reader reader) throws Exception;
    public void defineVariable(String name, Object value) throws Exception;
    public Object getVariable(String name)  throws Exception;
    public Object invokeMethod(Object thisObject, String name, Object...args) throws Exception;
    public Object invokeFunction(String name, Object...args) throws Exception;
    public void importClass(String qualifiedClassName) throws Exception;    
}
