/*
 * Created Date: Oct 6, 2008
 * Author : Sriram
 */
package com.meshdynamics.api.script;

public interface ScriptExecEnvInit {    
    
    public void initialize(ScriptEnvironment env);
    
}
