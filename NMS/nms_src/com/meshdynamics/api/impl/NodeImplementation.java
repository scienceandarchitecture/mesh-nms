/*
 * Created Date: Jul 10, 2008
 * Author : Sri
 */
package com.meshdynamics.api.impl;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import com.meshdynamics.api.NMS;
import com.meshdynamics.api.NMS.ACLConfiguration;
import com.meshdynamics.api.NMS.ACLEntry;
import com.meshdynamics.api.NMS.ConnectedDevice;
import com.meshdynamics.api.NMS.EffistreamRule;
import com.meshdynamics.api.NMS.GeneralConfiguration;
import com.meshdynamics.api.NMS.InterfaceAdvanceInfo;
import com.meshdynamics.api.NMS.InterfaceConfiguration;
import com.meshdynamics.api.NMS.NeighborNode;
import com.meshdynamics.api.NMS.Network;
import com.meshdynamics.api.NMS.Node;
import com.meshdynamics.api.NMS.ObjectArray;
import com.meshdynamics.api.NMS.ShortArray;
import com.meshdynamics.api.NMS.VlanConfiguration;
import com.meshdynamics.api.NMS.WEPSecurity;
import com.meshdynamics.api.NMS.WPAEnterpriseSecurity;
import com.meshdynamics.api.NMS.WPAPersonalSecurity;
import com.meshdynamics.iperf.IperfListener;
import com.meshdynamics.meshviewer.configuration.IACLConfiguration;
import com.meshdynamics.meshviewer.configuration.IACLInfo;
import com.meshdynamics.meshviewer.configuration.IConfiguration;
import com.meshdynamics.meshviewer.configuration.IEffistreamAction;
import com.meshdynamics.meshviewer.configuration.IEffistreamConfiguration;
import com.meshdynamics.meshviewer.configuration.IEffistreamRule;
import com.meshdynamics.meshviewer.configuration.IEffistreamRuleCriteria;
import com.meshdynamics.meshviewer.configuration.IInterfaceAdvanceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceConfiguration;
import com.meshdynamics.meshviewer.configuration.IInterfaceInfo;
import com.meshdynamics.meshviewer.configuration.IMeshConfiguration;
import com.meshdynamics.meshviewer.configuration.INetworkConfiguration;
import com.meshdynamics.meshviewer.configuration.IPSKConfiguration;
import com.meshdynamics.meshviewer.configuration.IRadiusConfiguration;
import com.meshdynamics.meshviewer.configuration.ISecurityConfiguration;
import com.meshdynamics.meshviewer.configuration.IStaConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanConfiguration;
import com.meshdynamics.meshviewer.configuration.IVlanInfo;
import com.meshdynamics.meshviewer.configuration.IWEPConfiguration;
import com.meshdynamics.meshviewer.configuration.KnownAp;
import com.meshdynamics.meshviewer.configuration.impl.Configuration;
import com.meshdynamics.meshviewer.configuration.impl.PSKConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.RadiusConfiguration;
import com.meshdynamics.meshviewer.configuration.impl.WEPConfiguration;
import com.meshdynamics.meshviewer.mesh.IMessageHandler;
import com.meshdynamics.meshviewer.mesh.Mesh;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IApFwUpdateListener;
import com.meshdynamics.meshviewer.mesh.ap.ICommandExecutionListener;
import com.meshdynamics.meshviewer.mesh.ap.IConfigStatusHandler;
import com.meshdynamics.util.IpAddress;
import com.meshdynamics.util.MacAddress;
import com.meshdynamics.util.Range;
import com.meshdynamics.util.SyncEvent;

/**
 * @author Sri
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class NodeImplementation implements Node {
    
    NodeImplementation(Network network, AccessPoint accessPoint) {
        
        this.accessPoint    		= accessPoint;
        this.network        		= network;
        this.originalConfig 		= null;
        this.cmdResponseHandler		= null;
        this.iperfResponseHandler	= null;
    }
    
    public String getUnitMacAddress() {
        return accessPoint.getDSMacAddress().toUpperCase();
    }

    public long getHeartbeatSqnr() {
        return accessPoint.getRuntimeApConfiguration().getHeartbeat1SequenceNumber();
    }

    public boolean isMobile() {
        /**
         * TODO
         */
    	
        return false;
    }
    
    public boolean isRemote() {
        /**
         * TODO
         */
        return false;
    }

    public synchronized String runPerformanceTest(int recordCount, short type, short protocol, int udpBandWidth) {

        if(accessPoint.isLocal() == false && this.iperfResponseHandler == null)
        	this.iperfResponseHandler = new IperfResponseHandler();
    	
        String ret =  accessPoint.executePerformanceTest(recordCount, 
        													type,
        													protocol,
        													udpBandWidth,
        													this.iperfResponseHandler
        												);
        
        if(ret.equalsIgnoreCase("Executing command remotely.") == false)
        	return ret;
        
        return this.iperfResponseHandler.waitForResponse();
    	
    }

    public synchronized int upgradeFirmware(String firmwareFilePath) {

    	if(accessPoint.isLocal() == false && this.fwUpdateResponseHandler == null)
        	this.fwUpdateResponseHandler = new FwUpdateResponseHandler();
    	
    	int ret = accessPoint.updateFirmware(firmwareFilePath, fwUpdateResponseHandler);
    	
        if(ret == Mesh.FW_UPDATE_RETURN_SUCCESS_INIT_REMOTE_UPDATE) 
        	ret = this.fwUpdateResponseHandler.waitForResponse();
    	
        return (ret == Mesh.FW_UPDATE_RETURN_SUCCESS) ? 1 : 0;
        
    }
    

    public short getFreeRAM() {
        return (short)accessPoint.getRuntimeApConfiguration().getBoardFreeMemory();
    }

    public short getInputVoltage() {
        return (short)accessPoint.getRuntimeApConfiguration().getVoltage();
    }

    public short getTreeLinkRate() {
        /**
         * TODO
         */        
        return 0;
    }

    public short getHopCount() {
        return accessPoint.getRuntimeApConfiguration().getHopCount();
    }

    public short getCpuUsage() {
        return (short)accessPoint.getRuntimeApConfiguration().getBoardCPUUsage();
    }

    public short getTemperature() {
        return (short)accessPoint.getRuntimeApConfiguration().getBoardTemperature();
    }

    public int getParentDownlinkSignal() {
        
        AccessPoint         parentAp;
        MacAddress          macAddress;
        IStaConfiguration   config;
        
        parentAp    = accessPoint.getParentAp();
        
        macAddress  = new MacAddress();
        
        macAddress.setBytes(getUnitMacAddress());
        
        config  = parentAp.getRuntimeApConfiguration().getStaInformation(macAddress);
        
        return (int)config.getSignal();
        
    }

    public int getParentDownlinkTxBitRate() {
        
        AccessPoint         parentAp;
        MacAddress          macAddress;
        IStaConfiguration   config;
        
        parentAp    = accessPoint.getParentAp();
        
        macAddress  = new MacAddress();
        
        macAddress.setBytes(getUnitMacAddress());
        
        config  = parentAp.getRuntimeApConfiguration().getStaInformation(macAddress);
        
        return (int)config.getBitRate();
        
    }

    public String getParentBssid() {
        MacAddress bssid = accessPoint.getRuntimeApConfiguration().getParentBssid();
        if(bssid == null)
        	return "";
        return bssid.toString();
        
    }

    public short getFirmwareVersionMajor() {
        
        IConfiguration  apConfig;
        
        if(!accessPoint.canConfigure())
            return 0;
            
        apConfig = accessPoint.getConfiguration();
        
        return apConfig.getFirmwareMajorVersion();
    }

    public short getFirmwareVersionMinor() {
        
        IConfiguration  apConfig;
        
        if(!accessPoint.canConfigure())
            return 0;
            
        apConfig = accessPoint.getConfiguration();
        
        return apConfig.getFirmwareMinorVersion();
    }

    public short getFirmwareVersionVariant() {
        
        IConfiguration  apConfig;
        
        if(!accessPoint.canConfigure())
            return 0;
            
        apConfig = accessPoint.getConfiguration();
        
        return apConfig.getFirmwareVariantVersion();
    }

    public String getGpsCurrentLatitude() {        
        return accessPoint.getRuntimeApConfiguration().getLatitude();
    }

    public String getGpsCurrentLongitude() {        
        return accessPoint.getRuntimeApConfiguration().getLongitude();
    }
    
    public short getGpsAltitude() {
        return Short.parseShort(accessPoint.getRuntimeApConfiguration().getAltitude());
    }

    public short getGpsSpeed() {
        return Short.parseShort(accessPoint.getRuntimeApConfiguration().getSpeed());
    }    
    
    public Enumeration<NeighborNode> getNeighborNodes() {
        
        Enumeration<KnownAp> knownAps;
               
        knownAps    = accessPoint.getRuntimeApConfiguration().getKnownAPs();
        
        return new NeighborNodeEnumerator(knownAps);
        
    }

    public Enumeration<ConnectedDevice> getConnectedDevices() {
        
        Enumeration<IStaConfiguration>  staList;
        
        staList = accessPoint.getRuntimeApConfiguration().getStaList();

        return new ConnectedDeviceEnumerator(staList);        
    }

    public GeneralConfiguration getGeneralConfiguration() {
        
        GeneralConfiguration    config;
        IConfiguration          apConfig;
        IMeshConfiguration      meshConfig;
        INetworkConfiguration   netConfig;
        int                     regDomain;
        
        if(!accessPoint.canConfigure())
            return null;
                      
        config      = new GeneralConfiguration();        
        apConfig    = accessPoint.getConfiguration();
        meshConfig  = apConfig.getMeshConfiguration();
        netConfig   = apConfig.getNetworkConfiguration();
        
        config.nodeName                 = apConfig.getNodeName();
        config.nodeDescription          = apConfig.getDescription();
        config.gpsLatitude              = apConfig.getLatitude();
        config.gpsLongitude             = apConfig.getLongitude();
        
        config.model                    = apConfig.getHardwareModel();
                
        config.hostName                 = netConfig.getHostName();
        config.ipAddress                = netConfig.getIpAddress().toString();
        config.subnetMask               = netConfig.getSubnetMask().toString();
        config.gatewayIpAddress         = netConfig.getGateway().toString();
        
        config.dynamicChannelAllocation = meshConfig.getDynamicChannelAllocation();
        config.heartbeatInterval        = meshConfig.getHeartbeatInterval();
        config.mobilityMode             = meshConfig.getMobilityMode();
        config.options                  = meshConfig.getHopCost();
        
        config.preferredParent          = apConfig.getPreferedParent();
        
        config.countryCode              = apConfig.getCountryCode();
        
        regDomain                       = apConfig.getRegulatoryDomain();
        
        config.regulatoryDomain         = regDomain & 0x00FF;
        
        config.dfsRequired              = (short)((regDomain >> 8) & 0x01);
        
        return config;
    }

    public Enumeration<InterfaceConfiguration> getInterfaces() {
        
        IConfiguration          apConfig;
        IInterfaceConfiguration ifConfig;
     
        if(!accessPoint.canConfigure())
            return null;
        
        apConfig    = accessPoint.getConfiguration();
        ifConfig    = apConfig.getInterfaceConfiguration();
       
        return new InterfaceConfigurationEnumerator(ifConfig);
    }

    public Enumeration<VlanConfiguration> getVlans() {
        
        IConfiguration      apConfig;
        IVlanConfiguration  vlanConfig;
        
        if(!accessPoint.canConfigure())
            return null;
        
        apConfig    = accessPoint.getConfiguration();
        vlanConfig  = apConfig.getVlanConfiguration();     
                
        return new VlanConfigurationEnumerator(vlanConfig);
    }

    public InterfaceConfiguration getInterfaceConfigurationByName(String name) {
        
        IConfiguration          apConfig;
        IInterfaceConfiguration ifConfig;
        IInterfaceInfo        	ifInfo;
        
        if(!accessPoint.canConfigure())
            return null;
        
        apConfig    = accessPoint.getConfiguration();
        ifConfig    = apConfig.getInterfaceConfiguration();         
        ifInfo      = ifConfig.getInterfaceByName(name);
        
        if(ifInfo == null)
            return null;
                        
        return internalGetInterfaceConfiguration(ifInfo);
        
    }

    public VlanConfiguration getVlanConfigurationByTag(short tag) {
        
        IConfiguration      apConfig;
        IVlanConfiguration  vlanConfig;
        IVlanInfo         	vlanInfo;
        
        if(!accessPoint.canConfigure())
            return null;
        
        apConfig    = accessPoint.getConfiguration();
        vlanConfig  = apConfig.getVlanConfiguration();
        
        for(int i = 0; i < vlanConfig.getVlanCount(); i++) {
            
            vlanInfo = vlanConfig.getVlanInfoByIndex(i);
            
            if(vlanInfo.getTag() == tag) {
                return internalGetVlanConfiguration(vlanInfo);
            }
            
        }        
        
        return null;
    }
    
    public EffistreamRule getEffistreamRules() {
        
        IConfiguration              apConfig;
        IEffistreamConfiguration    effistreamConfig;
        IEffistreamRule           	rulesRoot;
        NMS.EffistreamRule          firstLevelRules;
        NMS.EffistreamRule          rootRule;
        
        if(!accessPoint.canConfigure())
            return null;
        
        apConfig            = accessPoint.getConfiguration();
        effistreamConfig    = apConfig.getEffistreamConfiguration();        
        rulesRoot           = effistreamConfig.getRulesRoot();        
        firstLevelRules     = internalGetEffistreamRules(rulesRoot);

        rootRule = new NMS.EffistreamRule();
        
        if(firstLevelRules != null)
        	rootRule.addChild(firstLevelRules);
        
        return rootRule;        
    }        
    
    public ACLConfiguration getACLConfiguration() {
    
        IConfiguration      apConfig;
        IACLConfiguration   aclConfig;
        
        if(!accessPoint.canConfigure())
            return null;

        apConfig    = accessPoint.getConfiguration();
        aclConfig   = apConfig.getACLConfiguration();        
        
        return internalGetACLConfiguration(aclConfig);
        
    }        

    public void reboot() {        
        accessPoint.reboot();        
    }

    public short rebootRequired() {                
        return (short) (accessPoint.getRuntimeApConfiguration().isRebootRequired() ? 1 : 0);
    }

    public int restoreDefaults() {
        
        accessPoint.restoreDefaultSettings();
        
        return 0;
    }    

    public synchronized String executeCommand(String command) {
    	
        if(accessPoint.isLocal() == false && this.cmdResponseHandler == null)
        	this.cmdResponseHandler = new CommandResponseHandler();
    	
        String ret =  accessPoint.executeCommand(command, this.cmdResponseHandler);
        if(ret.equalsIgnoreCase("Executing command remotely.") == false)
        	return ret;
        
        return this.cmdResponseHandler.waitForResponse();
        
    }
    
    public short isIpReachable() {
        
        InetAddress             address;
        IConfiguration          apConfig;
        INetworkConfiguration   netConfig;        
        
        if(!accessPoint.canConfigure())
            return 0;
        
        apConfig    = accessPoint.getConfiguration();
        netConfig   = apConfig.getNetworkConfiguration();
        try {
            address     = InetAddress.getByName(netConfig.getIpAddress().toString());
        } catch (UnknownHostException e) {
            return 0;
        }
        
        try {
            return (short)(address.isReachable(10000) ? 1 : 0);
        } catch (IOException e) {
        }
        
        return 0;        
    }    
    
    
    public int setGeneralConfiguration(GeneralConfiguration configuration) {
        
        IConfiguration          apConfig;
        Configuration           apOrigConfig;
        IMeshConfiguration      meshConfig;
        INetworkConfiguration   netConfig;
        IpAddress               ipAddress;
        int                     regDomain;
        
        if(!accessPoint.canConfigure())
            return -1;        
       
        apConfig          = accessPoint.getConfiguration();
       
        apOrigConfig      = null;
        
        if(originalConfig == null) {
            apOrigConfig    = new Configuration();        
            apConfig.copyTo(apOrigConfig);
        }
        
        meshConfig  = apConfig.getMeshConfiguration();
        netConfig   = apConfig.getNetworkConfiguration();
        
        apConfig.setNodeName(configuration.nodeName);
        apConfig.setDescription(configuration.nodeDescription);
        apConfig.setLatitude(configuration.gpsLatitude);
        apConfig.setLongitude(configuration.gpsLongitude);
                
        netConfig.setHostName(configuration.hostName);
        
        ipAddress   = new IpAddress();
        ipAddress.setBytes(configuration.ipAddress);        
        netConfig.setIpAddress(ipAddress);

        ipAddress   = new IpAddress();
        ipAddress.setBytes(configuration.subnetMask);        
        netConfig.setSubnetMask(ipAddress);
        
        ipAddress   = new IpAddress();
        ipAddress.setBytes(configuration.gatewayIpAddress);        
        netConfig.setGateway(ipAddress);

        meshConfig.setDynamicChannelAllocation(configuration.dynamicChannelAllocation);
        meshConfig.setHeartbeatInterval(configuration.heartbeatInterval);
        meshConfig.setMobilityMode(configuration.mobilityMode);
        meshConfig.setHopCost(configuration.options);
        
        apConfig.setPreferedParent(configuration.preferredParent);
        
        if(configuration.countryCode != NMS.COUNTRY_CODE_CUSTOM
        && configuration.regulatoryDomain != NMS.REG_DOMAIN_CODE_CUSTOM) {        
            
            apConfig.setCountryCode(configuration.countryCode);
            regDomain = apConfig.getRegulatoryDomain();
            regDomain |= configuration.regulatoryDomain;
            regDomain &= 0xFEFF;// resetting the DFS bit
            
            if(configuration.dfsRequired != 0) {
                regDomain |= 0x0100;
            }
            
            apConfig.setRegulatoryDomain(regDomain);

        }
        
        if(originalConfig == null && apOrigConfig != null) {
            return internalSendConfigurationAndWait(apOrigConfig,apConfig);
        }

        return 0;
    }    
    
    public int setInterfaceConfiguration(InterfaceConfiguration configuration) {
        
        Configuration           apOrigConfig;
        IConfiguration          apConfig;
        IInterfaceConfiguration ifConfig;
        IInterfaceInfo        ifInfo;
        
        if(!accessPoint.canConfigure())
            return -1;
        
        apConfig    = accessPoint.getConfiguration();        
        ifConfig    = apConfig.getInterfaceConfiguration();
                
        ifInfo      = ifConfig.getInterfaceByName(configuration.name);
        
        if(ifInfo == null)
            return -1;
        
        apOrigConfig  = null;
        
        if(originalConfig == null) {
            apOrigConfig    = new Configuration();        
            apConfig.copyTo(apOrigConfig);
        }       
        
        internalSetInterfaceConfiguration(ifInfo, configuration);
        
        if(originalConfig == null && apOrigConfig != null) {
            return internalSendConfigurationAndWait(apOrigConfig,apConfig);
        }

        return 0;
        
    }
    
    public int addVlan(VlanConfiguration configuration) {
        
        IConfiguration          apConfig;
        IVlanConfiguration      vlanConfig;
        Configuration           apOrigConfig;
        IVlanInfo             vlanInfo;
        
        if(!accessPoint.canConfigure())
            return -1;        
        
        apConfig    = accessPoint.getConfiguration();        
        vlanConfig  = apConfig.getVlanConfiguration();        
        
        apOrigConfig  = null;
        
        if(originalConfig == null) {
            apOrigConfig    = new Configuration();        
            apConfig.copyTo(apOrigConfig);
        }       
        
        vlanInfo    = vlanConfig.addVlanInfo(configuration.essid, configuration.tag);
        
        if(vlanInfo == null)
            return -1;        
         
        internalSetVlanConfiguration(vlanInfo, configuration);
        
        if(originalConfig == null && apOrigConfig != null) {
            return internalSendConfigurationAndWait(apOrigConfig,apConfig);
        }

        return 0;
        
    }
    
    public int removeVlan(short tag) {
        
        IConfiguration          apConfig;
        IVlanConfiguration      vlanConfig;
        Configuration           apOrigConfig;
        boolean                 status;
        
        if(!accessPoint.canConfigure())
            return -1;        
        
        apConfig        = accessPoint.getConfiguration();        
        vlanConfig      = apConfig.getVlanConfiguration();  
        
        apOrigConfig  = null;
        
        if(originalConfig == null) {
            apOrigConfig    = new Configuration();        
            apConfig.copyTo(apOrigConfig);
        }         
                
        status = vlanConfig.deleteVlanInfo(tag);
        
        if(!status)
            return -1;
        
        if(originalConfig == null && apOrigConfig != null) {
            return internalSendConfigurationAndWait(apOrigConfig,apConfig);
        }

        return 0;
    }
    
    public int setVlans(ObjectArray vlans) {
        
        IConfiguration              apConfig;
        Configuration               apOrigConfig;
        IVlanConfiguration          vlanConfig;
        Hashtable<String,Object>    essidHash;
        Hashtable<String,Object>    tagHash;
        VlanConfiguration           config;
        IVlanInfo                 vlanInfo;
        
        if(!accessPoint.canConfigure())
            return -1;      
        
        apConfig        = accessPoint.getConfiguration();        
        vlanConfig      = apConfig.getVlanConfiguration();     
        
        apOrigConfig  = null;
        
        if(originalConfig == null) {
            apOrigConfig    = new Configuration();        
            apConfig.copyTo(apOrigConfig);
        }  
        
        vlanConfig.setVlanCount(0);
        
        essidHash   = new Hashtable<String,Object>();
        tagHash     = new Hashtable<String,Object>();
        
        for(int i = 0; i < vlans.length(); i++) {
            
            config = (VlanConfiguration)vlans.get(i);
            
            if(essidHash.get(config.essid) != null)
                continue;
            
            if(tagHash.get(Short.toString(config.tag)) != null)
                continue;
            
            essidHash.put(config.essid, config);
            tagHash.put(Short.toString(config.tag),config);
            
            vlanInfo = vlanConfig.addVlanInfo(config.essid, config.tag);
            
            internalSetVlanConfiguration(vlanInfo, config);
            
        }
        
        essidHash.clear();
        tagHash.clear();
        
        if(originalConfig == null && apOrigConfig != null) {
            return internalSendConfigurationAndWait(apOrigConfig,apConfig);
        }

        return 0;
    }

    public int setVlanConfiguration(VlanConfiguration configuration) {

        IConfiguration          apConfig;
        IVlanConfiguration      vlanConfig;
        Configuration           apOrigConfig;
        IVlanInfo             vlanInfo;
        
        if(!accessPoint.canConfigure())
            return -1;        
        
        apConfig    = accessPoint.getConfiguration();        
        vlanConfig  = apConfig.getVlanConfiguration();
        vlanInfo    = null;
        
        for(int i = 0; i < vlanConfig.getVlanCount(); i++) {
            
            vlanInfo = vlanConfig.getVlanInfoByIndex(i);
            
            if(vlanInfo.getTag() == configuration.tag
            && vlanInfo.getEssid().equals(configuration.essid)) {
                break;
            }            
            
        }
        
        if(vlanInfo == null) {         
            return -1;
        } 
        
        apOrigConfig  = null;
        
        if(originalConfig == null) {
            apOrigConfig    = new Configuration();        
            apConfig.copyTo(apOrigConfig);
        }      
        
       internalSetVlanConfiguration(vlanInfo, configuration);
       
       if(originalConfig == null && apOrigConfig != null) {
           return internalSendConfigurationAndWait(apOrigConfig,apConfig);
       }
       
       return 0;
    }

    public int setEffistreamRules(EffistreamRule rules) {
        
        IConfiguration              apConfig;
        IEffistreamConfiguration    effistreamConfig;
        IEffistreamRule           rulesRoot;
        Enumeration<String>         enumChildIds;
        Vector<String>              childIds;
        IEffistreamRule           internalRule;
        Configuration               apOrigConfig;        
        
        if(!accessPoint.canConfigure())
            return -1;
        
        if(rules.matchId == NMS.EFFISTREAM_MATCH_IGNORE)
            rules = rules.firstChild;
                
        apConfig            = accessPoint.getConfiguration();
        
        apOrigConfig  = null;
        
        if(originalConfig == null) {
            apOrigConfig    = new Configuration();        
            apConfig.copyTo(apOrigConfig);
        }         
        
        effistreamConfig    = apConfig.getEffistreamConfiguration();  
        rulesRoot           = effistreamConfig.getRulesRoot();
        
        /**
         * Remove all existing rules by adding all child identifiers
         * to a temporary vector and then deleting it
         */
        
        enumChildIds    = rulesRoot.getChildrenIdentifiers();
        childIds        = new Vector<String>();
        
        while(enumChildIds.hasMoreElements()) {
            childIds.add(enumChildIds.nextElement());
        }
        
        for(int i = 0; i < childIds.size(); i++) {
            rulesRoot.deleteRule(childIds.get(i));
        }
        
        childIds = null;
        
        for(; rules != null; rules = rules.nextSibling) {            
            
            internalRule    = internalCreateInternalEffistreamRule(rules, rulesRoot);
            
            internalRecursiveProcessRule(rules, internalRule);
            
        }                
                
        if(originalConfig == null && apOrigConfig != null) {
            return internalSendConfigurationAndWait(apOrigConfig,apConfig);
        }
        
        return 0;
    }
    
    public int setACLConfiguration(ACLConfiguration configuration) {
        
        IConfiguration      apConfig;
        IACLConfiguration   aclConfig;
        Configuration       apOrigConfig;
        
        if(!accessPoint.canConfigure())
            return -1;

        apConfig        = accessPoint.getConfiguration();
        aclConfig       = apConfig.getACLConfiguration();        
        
        apOrigConfig  = null;
        
        if(originalConfig == null) {
            apOrigConfig    = new Configuration();        
            apConfig.copyTo(apOrigConfig);
        }  
        
        aclConfig.setCount(0);    
        
        internalSetACLConfiguration(aclConfig, configuration);
        
        if(originalConfig == null && apOrigConfig != null) {
            return internalSendConfigurationAndWait(apOrigConfig,apConfig);
        }
        
        return 0;
        
    }
    
    public String generateConfigMacro(String scriptLanguage) {
        
        NodeMacroScriptGenerator generator;
        
        generator = new NodeMacroScriptGenerator(this);
                       
        try {
            return generator.generate();
         } catch (Exception e) {
        	return null;
        }
        
    }    
    
    public int beginConfigurationUpdate() {
        
        IConfiguration apConfig;
        
        if(originalConfig != null) {
            return -1;
        }
        
        if(!accessPoint.canConfigure()) {
            originalConfig = null;
            return -2;
        }
                               
        apConfig        = accessPoint.getConfiguration();
        originalConfig  = new Configuration();
               
        apConfig.copyTo(originalConfig);        
    
        return 0;
    }

    public int cancelConfigurationUpdate() {
        originalConfig = null;
        return 0;
    }

    public int commitConfigurationUpdate() {
        
        IConfiguration  apConfig;
        int             ret;
        
        if(originalConfig == null) {
            return -1;
        }        
        
        apConfig = accessPoint.getConfiguration();
        
        ret = internalSendConfigurationAndWait(originalConfig,apConfig);
        
        originalConfig = null;
        
        return ret;
    }
    
       
    /**
     * Called to finalize the Node object by the NetworkImplementation class
     */
    
    void unInitialize() {
        System.err.println("Node: '" + getUnitMacAddress() + "' deleted");
        accessPoint = null;
    }
    
    
    
    /**
     * Converts an array of short's to array of byte's.
     * Called internally by the shortArrayToHexString function.
     */
        
    private byte[] shortToByteArray(short[] sArray) {
        
        byte[] bArray;
        
        bArray  = new byte[sArray.length];
        
        for(int i = 0; i < sArray.length; i++) {
            bArray[i] = (byte)sArray[i];
        }
        
        return bArray;
    }
    
    /**
     * Used internally to convert a short array from the MeshViewer internal strctures to
     * a hex string. This internal calls the shortToByteArray function and uses
     * the public NMS.bytesToHexString function.
     */
    
    private String shortArrayToHexString(short[] sArray) {
       return NMS.bytesToHexString(shortToByteArray(sArray));
    }    
    
    /**
     * Converts an array of bytes to an array of shorts.
     * Used internally by the hexStringToShortArray function
     */    
    
    private short[] byteToShortArray(byte[] bArray) {
        
        short[] sArray;
        
        sArray  = new short[bArray.length];
        
        for(int i = 0; i < bArray.length; i++) {
            sArray[i] =  (short)bArray[i];
            sArray[i] &= 0xFF;
        }
        
        return sArray;
    }
    
    /**
     * Used internally to convert a hexstring to an array of shorts.
     * The MeshViewer internal structures use an array of shorts.
     */       
    
    private short[] hexStringToShortArray(String hexString) {
        return byteToShortArray(NMS.hexStringToBytes(hexString));
    }
    
    /**
     * Used internally to create an internal MeshViewer IAPEffistreamRule object  
     * from the public  NMS.EffistreamRule object
     */
    
    private IEffistreamRule internalCreateInternalEffistreamRule(NMS.EffistreamRule rule,
                                                                   IEffistreamRule  internalParentRule) {

        IEffistreamRule           internalRule;
        IEffistreamRuleCriteria   criteria;
        
        internalRule    = internalParentRule.createRule(rule.matchId);
        criteria        = internalRule.getCriteriaValue();
        
        switch(rule.matchId) {
        case NMS.EFFISTREAM_MATCH_ETH_TYPE:
        case NMS.EFFISTREAM_MATCH_IP_TOS:
        case NMS.EFFISTREAM_MATCH_IP_DIFFSRV:
        case NMS.EFFISTREAM_MATCH_IP_PROTO:
        case NMS.EFFISTREAM_MATCH_RTP_VERSION:
        case NMS.EFFISTREAM_MATCH_RTP_PAYLOAD:
            criteria.setLongValue(Long.parseLong(rule.matchCriteria));
            break;
        case NMS.EFFISTREAM_MATCH_ETH_DST:
        case NMS.EFFISTREAM_MATCH_ETH_SRC:
            {
                MacAddress  macAddress;
                
                macAddress = new MacAddress();
                macAddress.setBytes(rule.matchCriteria);
                
                criteria.setMacAddressValue(macAddress.getBytes());                
            }
            break;       
        case NMS.EFFISTREAM_MATCH_IP_DST:        
        case NMS.EFFISTREAM_MATCH_IP_SRC:
            {
             
                IpAddress   ipAddress;
                
                ipAddress = new IpAddress();
                ipAddress.setBytes(rule.matchCriteria);
                
                criteria.setIPAddressValue(ipAddress.getBytes());                
            }
            break;
        case NMS.EFFISTREAM_MATCH_RTP_LENGTH:                
        case NMS.EFFISTREAM_MATCH_TCP_DST_PORT:
        case NMS.EFFISTREAM_MATCH_TCP_LENGTH:
        case NMS.EFFISTREAM_MATCH_TCP_SRC_PORT:
        case NMS.EFFISTREAM_MATCH_UDP_DST_PORT:
        case NMS.EFFISTREAM_MATCH_UDP_LENGTH:
        case NMS.EFFISTREAM_MATCH_UDP_SRC_PORT:
            {
                long            lowerLimit;
                long            upperLimit;
                StringTokenizer st;
                String          token;
                
                st          = new StringTokenizer(rule.matchCriteria,":");
                lowerLimit  = 0;
                upperLimit  = 0;
                
                try {
                    token       = st.nextToken();
                    lowerLimit  = Long.parseLong(token);
                    token       = st.nextToken();
                    upperLimit  = Long.parseLong(token);                    
                } catch(Exception e) {
                    
                }
                
                criteria.setRangeValue(lowerLimit, upperLimit);
                
            }    
        }
        
        internalParentRule.addRule(internalRule);
        
        return internalRule;
        
    }

    /**
     * Used internally to create a public NMS.EffistreamRule  
     * from the internal MeshViewer IAPEffistreamRule object
     */    
    
    private NMS.EffistreamRule internalCreateEffistreamRule(IEffistreamRule internalRule) {
        
        short                       matchId;
        String                      matchCriteria;
        IEffistreamRuleCriteria   criteria;
        Range                       rangeValue;

        criteria  = internalRule.getCriteriaValue();
               
        matchId         = internalRule.getCriteriaId();
        matchCriteria   = "0";
        
        switch(matchId) {
        case NMS.EFFISTREAM_MATCH_ETH_TYPE:
        case NMS.EFFISTREAM_MATCH_IP_TOS:
        case NMS.EFFISTREAM_MATCH_IP_DIFFSRV:
        case NMS.EFFISTREAM_MATCH_IP_PROTO:
        case NMS.EFFISTREAM_MATCH_RTP_VERSION:
        case NMS.EFFISTREAM_MATCH_RTP_PAYLOAD:
            matchCriteria = Long.toString(criteria.getLongValue());
            break;
        case NMS.EFFISTREAM_MATCH_ETH_DST:
        case NMS.EFFISTREAM_MATCH_ETH_SRC:
            matchCriteria = criteria.getMacAddressValue().toString();
            break;       
        case NMS.EFFISTREAM_MATCH_IP_DST:        
        case NMS.EFFISTREAM_MATCH_IP_SRC:
            matchCriteria = criteria.getIPAddressValue().toString();
            break;
        case NMS.EFFISTREAM_MATCH_RTP_LENGTH:                
        case NMS.EFFISTREAM_MATCH_TCP_DST_PORT:
        case NMS.EFFISTREAM_MATCH_TCP_LENGTH:
        case NMS.EFFISTREAM_MATCH_TCP_SRC_PORT:
        case NMS.EFFISTREAM_MATCH_UDP_DST_PORT:
        case NMS.EFFISTREAM_MATCH_UDP_LENGTH:
        case NMS.EFFISTREAM_MATCH_UDP_SRC_PORT:
            rangeValue  = criteria.getRangeValue();
            matchCriteria = Long.toString(rangeValue.getLowerLimit());
            matchCriteria += ":";
            matchCriteria = Long.toString(rangeValue.getUpperLimit());
        }      
        
        return new NMS.EffistreamRule(matchId,matchCriteria);
    }
    
    /**
     * Recursively processes a public NMS.EffistreamRule object 
     * and sets up values for the internal IAPEffistreamRule object
     */  
    
    private void internalRecursiveProcessRule(NMS.EffistreamRule rule,
                                              IEffistreamRule  internalRule) {
        
        NMS.EffistreamRule  childRule;
        IEffistreamRule   internalChildRule;
        
        if(rule.firstChild == null) {
            
            IEffistreamAction action;
            
            action = internalRule.addAction(IEffistreamAction.ACTION_TYPE_DEFAULT);           
            
            action.setBitRate(rule.actionBitRate);
            action.setDot11eCategory(rule.actionDot11eCategory);
            action.setDropPacket(rule.actionDropPacket);
            action.setNoAck(rule.actionNoAck);
            action.setQueuedRetry(rule.actionQueuedRetry);
            
            return;
        }
        
        for(childRule = rule.firstChild;
            childRule != null; 
            childRule = childRule.nextSibling) {
            
            internalChildRule = internalCreateInternalEffistreamRule(childRule,internalRule);
            
            internalRecursiveProcessRule(childRule,internalChildRule);
        }        
        
    }
    
    /**
     * Recursively processes an internal MeshViewer IAPEffistreamRule
     * and sets up values for the public  NMS.EffistreamRule object
     */      
     
    private void internalRecursiveProcessInternalRule(IEffistreamRule   internalRule,
                                                      NMS.EffistreamRule  rule) {
        
        Enumeration<String>         childIds;
        IEffistreamRule           internalChildRule;
        IEffistreamAction         action; 
        NMS.EffistreamRule          childRule;
                        
        if(internalRule.getChildCount() <= 0) {
            
            action                      = internalRule.getAction();
            
            rule.actionBitRate         = action.getBitRate();
            rule.actionDot11eCategory  = action.getDot11eCategory();
            rule.actionDropPacket      = action.getDropPacket();
            rule.actionNoAck           = action.getNoAck();
            rule.actionQueuedRetry     = action.getQueuedRetry();            

            return;
        }
        
        childIds = internalRule.getChildrenIdentifiers();

        while(childIds.hasMoreElements()) {

            internalChildRule  = internalRule.getChild(childIds.nextElement());            
            childRule          = internalCreateEffistreamRule(internalChildRule);
            
            rule.addChild(childRule);
            
            internalRecursiveProcessInternalRule(internalChildRule,childRule);
        }                
        
    }
    
    /**
     * Creates a public NMS.EffistreamRule object from the internal 
     * MeshViewer IAPEffistreamRule object from the ROOT.
     */
    
    private NMS.EffistreamRule internalGetEffistreamRules(IEffistreamRule rulesRoot) {
        
        NMS.EffistreamRule      firstLevelRules;
        NMS.EffistreamRule      lastRule;
        NMS.EffistreamRule      rule;
        Enumeration<String>     childIds;
        IEffistreamRule       child;
        
        firstLevelRules = null;
        lastRule        = null;
        
        if(rulesRoot.getChildCount() <= 0)
            return firstLevelRules;
        
        childIds    = rulesRoot.getChildrenIdentifiers();
        
        while(childIds.hasMoreElements()) {
            
            child   = rulesRoot.getChild(childIds.nextElement());        
            rule    = internalCreateEffistreamRule(child);
            
            if(lastRule != null) {
                lastRule.nextSibling    = rule;
            } else {
                firstLevelRules = rule;
            }
            
            lastRule = rule;
            
            internalRecursiveProcessInternalRule(child,rule);            
            
        }
                
        return firstLevelRules;
    }
    
    /**
     * Sets up the IAPSecurityConfiguration from a SecurityTypeInfo object
     */    
    
    private void internalSetSecurityConfig(ISecurityConfiguration apSecurityConfig,
                                           SecurityTypeInfo         sti) {
        
        switch(sti.securityType) {
        case NMS.SECURITY_TYPE_WEP_104:
        case NMS.SECURITY_TYPE_WEP_40:
            
            {   
                WEPConfiguration    wepConfig;
                WEPSecurity         securityInfo;
                
                securityInfo = (WEPSecurity)sti.securityInfo;
                
                apSecurityConfig.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_WEP);
                
                wepConfig = new WEPConfiguration();
                
                if(sti.securityType == NMS.SECURITY_TYPE_WEP_104) {
                    
                    wepConfig.setKeyStrength(WEPConfiguration.KEY_STRENGTH_128_BIT);
                    wepConfig.setKeyIndex((short) 0);
                    wepConfig.setKey(0,hexStringToShortArray((String)securityInfo.wepKeys.get(0)));
                    
                } else {
                    
                    wepConfig.setKeyStrength(WEPConfiguration.KEY_STRENGTH_64_BIT);
                    wepConfig.setKeyIndex(securityInfo.keyIndex);
                    
                    for(int i = 0; i < 4 && i < securityInfo.wepKeys.length(); i++) {
                        wepConfig.setKey(i,hexStringToShortArray((String)securityInfo.wepKeys.get(i)));
                    }
                }               
                
                apSecurityConfig.setWEPConfiguration(wepConfig);               
                
            }            
            break;
        case NMS.SECURITY_TYPE_WPA2_ENTERPRISE:
        case NMS.SECURITY_TYPE_WPA_ENTERPRISE:
            {
                WPAEnterpriseSecurity   securityInfo;
                RadiusConfiguration     radiusConfig;
                IpAddress               ipAddress;
                
                securityInfo    = (WPAEnterpriseSecurity)sti.securityInfo;
                
                apSecurityConfig.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_WPA_ENTERPRISE);
                
                radiusConfig    = new RadiusConfiguration();
                
                if(securityInfo.cipherType == NMS.CIPHER_CCMP) {
                    radiusConfig.setCipherCCMP((short) Mesh.ENABLED);
                    radiusConfig.setCipherTKIP(Mesh.DISABLED);
                } else {
                    radiusConfig.setCipherTKIP((short) Mesh.ENABLED);
                    radiusConfig.setCipherCCMP(Mesh.DISABLED);                    
                }
                
                radiusConfig.setKey(securityInfo.radiusServerSecret);                
                
                if(sti.securityType == NMS.SECURITY_TYPE_WPA2_ENTERPRISE) {
                    radiusConfig.setMode((short) IRadiusConfiguration.WPA_PERSONAL_MODE_WPA2);
                } else {
                    radiusConfig.setMode((short) IRadiusConfiguration.WPA_PERSONAL_MODE_WPA);
                }
                
                radiusConfig.setPort(securityInfo.radiusServerPort);
                
                ipAddress   = new IpAddress();
                ipAddress.setBytes(securityInfo.radiusServerIp);                
                radiusConfig.setServerIPAddress(ipAddress);
                
                apSecurityConfig.setRadiusConfiguration(radiusConfig);
                
            }
            break;
        case NMS.SECURITY_TYPE_WPA2_PERSONAL:
        case NMS.SECURITY_TYPE_WPA_PERSONAL:
            {
                
                WPAPersonalSecurity securityInfo;
                PSKConfiguration    pskConfig;
                
                securityInfo    = (WPAPersonalSecurity)sti.securityInfo;
                pskConfig       = new PSKConfiguration();
                
                apSecurityConfig.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_WPA_PERSONAL);
                                
                if(securityInfo.cipherType == NMS.CIPHER_CCMP) {
                    pskConfig.setPSKCipherCCMP((short) Mesh.ENABLED);
                    pskConfig.setPSKCipherTKIP(Mesh.DISABLED);
                } else {
                    pskConfig.setPSKCipherTKIP((short) Mesh.ENABLED);
                    pskConfig.setPSKCipherCCMP(Mesh.DISABLED);                    
                }
                
                pskConfig.setPSKKeyLength(32);                
                pskConfig.setPSKKey(hexStringToShortArray(securityInfo.preSharedKey));
                                
                if(sti.securityType == NMS.SECURITY_TYPE_WPA2_PERSONAL) {
                    pskConfig.setPSKMode(IPSKConfiguration.WPA_PERSONAL_MODE_WPA2);
                } else {
                    pskConfig.setPSKMode(IPSKConfiguration.WPA_PERSONAL_MODE_WPA);
                }
                
                pskConfig.setPSKPassPhrase("");
                
                apSecurityConfig.setPSKConfiguration(pskConfig);
                                
            }
            break;
        case NMS.SECURITY_TYPE_NONE:
        default:
            apSecurityConfig.setEnabledSecurity(ISecurityConfiguration.SECURITY_TYPE_NONE);
        }        
                
    }
    
    /**
     * Gets a SecurityTypeInfo from the internal MeshViewer IAPSecurityConfiguration object.
     */    
        
    private SecurityTypeInfo internalGetSecurityTypeInfo(ISecurityConfiguration apSecurityConfig) {
        
        SecurityTypeInfo sti;
        
        sti = new SecurityTypeInfo(NMS.SECURITY_TYPE_NONE,null);
        
        switch(apSecurityConfig.getEnabledSecurity()) {
        case ISecurityConfiguration.SECURITY_TYPE_WEP:
            {
                IWEPConfiguration wepConfig;
                WEPSecurity         wepSecurity;
            
                wepConfig   = apSecurityConfig.getWEPConfiguration();
                wepSecurity = new WEPSecurity();
                
                switch(wepConfig.getKeyStrength()) {
                case IWEPConfiguration.KEY_STRENGTH_128_BIT:                    
                    sti.securityType     = NMS.SECURITY_TYPE_WEP_104;                    
                    wepSecurity.keyIndex = 0;
                    wepSecurity.wepKeys  = new ObjectArray(1);                    
                    break;
                case IWEPConfiguration.KEY_STRENGTH_64_BIT:
                default:                    
                    sti.securityType      = NMS.SECURITY_TYPE_WEP_40;                    
                    wepSecurity.keyIndex  = wepConfig.getKeyIndex();
                    wepSecurity.wepKeys   = new ObjectArray(4);
                }
                
                for(int i = 0; i<wepSecurity.wepKeys.length(); i++) {
                    wepSecurity.wepKeys.set(i,shortArrayToHexString(wepConfig.getKey(i)));
                }
                
                sti.securityInfo = wepSecurity;
            }            
            break;
        case ISecurityConfiguration.SECURITY_TYPE_WPA_ENTERPRISE:
            {
                IRadiusConfiguration  radiusConfig;
                WPAEnterpriseSecurity   wpaSecurity;
                
                wpaSecurity     = new WPAEnterpriseSecurity();
                radiusConfig    = apSecurityConfig.getRadiusConfiguration();
                
                switch(radiusConfig.getMode()) {
                case IRadiusConfiguration.WPA_PERSONAL_MODE_WPA2:
                    sti.securityType = NMS.SECURITY_TYPE_WPA2_ENTERPRISE;
                    break;
                case IRadiusConfiguration.WPA_PERSONAL_MODE_WPA:
                default:
                    sti.securityType = NMS.SECURITY_TYPE_WPA_ENTERPRISE;
                }
                
                if(radiusConfig.getCipherCCMP() != 0)
                    wpaSecurity.cipherType = NMS.CIPHER_CCMP;
                else if(radiusConfig.getCipherTKIP() != 0)
                    wpaSecurity.cipherType = NMS.CIPHER_TKIP;
                else
                    wpaSecurity.cipherType = NMS.CIPHER_CCMP;
                
                wpaSecurity.radiusServerIp      = radiusConfig.getServerIPAddress().toString();
                wpaSecurity.radiusServerPort    = (short) radiusConfig.getPort();
                wpaSecurity.radiusServerSecret  = radiusConfig.getKey();
                
                sti.securityInfo = wpaSecurity;
            }
            break;
        case ISecurityConfiguration.SECURITY_TYPE_WPA_PERSONAL:
            {
                IPSKConfiguration pskConfig;
                WPAPersonalSecurity wpaSecurity;
                
                wpaSecurity = new WPAPersonalSecurity();
                pskConfig   = apSecurityConfig.getPSKConfiguration();
                
                switch(pskConfig.getPSKMode()) {
                case IPSKConfiguration.WPA_PERSONAL_MODE_WPA2:
                    sti.securityType    = NMS.SECURITY_TYPE_WPA2_PERSONAL;
                    break;
                case IPSKConfiguration.WPA_PERSONAL_MODE_WPA:
                default:
                    sti.securityType    = NMS.SECURITY_TYPE_WPA_PERSONAL;
                }
                
                if(pskConfig.getPSKCipherCCMP() != 0)
                    wpaSecurity.cipherType = NMS.CIPHER_CCMP;
                else if(pskConfig.getPSKCipherTKIP() != 0)
                    wpaSecurity.cipherType = NMS.CIPHER_TKIP;
                else
                    wpaSecurity.cipherType = NMS.CIPHER_CCMP;
                
                wpaSecurity.preSharedKey = shortArrayToHexString(pskConfig.getPSKKey());
                
                sti.securityInfo = wpaSecurity;
                
            }
            break;
        case ISecurityConfiguration.SECURITY_TYPE_NONE:
        default:
            sti.securityType = NMS.SECURITY_TYPE_NONE;
            sti.securityInfo = null;
        }    
        
        return sti;
    }
    
    /**
     * Sets up values into the internal MeshViewer IAPVlanInfo object 
     * from the public NMS.VlanConfiguration object
     */
    
    private void internalSetVlanConfiguration(IVlanInfo       info,
                                              VlanConfiguration config) {
        
        
        ISecurityConfiguration vlanSecurityConfig;
        
        vlanSecurityConfig = info.getSecurityConfiguration();
        
        info.set80211eCategoryIndex(config.dot11eCategory);
        info.set80211eEnabled(config.dot11eEnabled);
        info.set8021pPriority(config.dot1pPriority);
        info.setEssid(config.essid);
        info.setName(config.name);       
        
        internalSetSecurityConfig(vlanSecurityConfig,
                                  new SecurityTypeInfo(config.securityType,config.securityInfo));       
        
        info.setTag(config.tag);        
        
    }
    
    /**
     * Creates the public NMS.VlanConfiguration from the internal
     * MeshViewer IAPVlanInfo object
     */    
    
    private VlanConfiguration internalGetVlanConfiguration(IVlanInfo info) {
        
        VlanConfiguration           vlanConfig;
        ISecurityConfiguration    vlanSecurityConfig;
        SecurityTypeInfo            sti;
        
        vlanConfig = new VlanConfiguration();
        
        vlanConfig.dot11eCategory   = info.get80211eCategoryIndex();
        vlanConfig.dot11eEnabled    = info.get80211eEnabled();
        vlanConfig.dot1pPriority    = info.get8021pPriority();
        vlanConfig.essid            = info.getEssid();
        vlanConfig.name             = info.getName();
        
        vlanSecurityConfig          = info.getSecurityConfiguration();
        sti                         = internalGetSecurityTypeInfo(vlanSecurityConfig);
        
        vlanConfig.securityInfo     = sti.securityInfo;
        vlanConfig.securityType     = sti.securityType;
        
        vlanConfig.tag              = (short) info.getTag();
        
        return vlanConfig;
    }
    
    /**
     * Sets up the values for the internal MeshViewer IAPInterfaceInfo from
     * the public NMS.InterfaceConfiguration object
     */
    
    private void internalSetInterfaceConfiguration(IInterfaceInfo       info,
                                                   InterfaceConfiguration config) {
        
        ISecurityConfiguration    apSecurityConfig;
               
        info.setAckTimeout((short) config.ackTimeout);
        
        if(config.allowClientConnection != 0) {
            info.setServiceType(Mesh.IF_SERVICE_ALL);
        } else {
            info.setServiceType(Mesh.IF_SERVICE_BACKHAUL_ONLY);
        }
        
        info.setDcaListCount((short) config.dcaList.length());
        
        for(int i = 0; i < config.dcaList.length(); i++) {
            info.setDcaChannel(i, config.dcaList.get(i));
        }
        
        info.setDynamicChannelAlloc(config.dynamicChannelAllocation);
        info.setEssid(config.essid);
        info.setFragThreshold(config.fragThreshold);
        info.setEssidHidden(config.hideEssid);
        info.setChannel(config.manualChannel);
        info.setTxRate(config.maxTransmitRate);
        
        /**
         * Only allow medium sub-type to be changed between B/G/BG
         */
                
       /* switch(info.getMediumSubType()) {
        case Mesh.PHY_SUB_TYPE_802_11_B:
        case Mesh.PHY_SUB_TYPE_802_11_B_G:
        case Mesh.PHY_SUB_TYPE_802_11_G:
            if(config.phySubType == NMS.PHY_SUB_TYPE_802_11_B
            || config.phySubType == NMS.PHY_SUB_TYPE_802_11_BG
             || config.phySubType == NMS.PHY_SUB_TYPE_802_11_G) {
                info.setMediumSubType(config.phySubType);
            }
        } */
                      
        info.setMediumSubType(config.phySubType);
        System.out.println(config.phySubType);
        info.setRtsThreshold(config.rtsThreshold);
        apSecurityConfig    = info.getSecurityConfiguration();  
        
        internalSetSecurityConfig(apSecurityConfig,
                                  new SecurityTypeInfo(config.securityType,config.securityInfo));
        IInterfaceAdvanceConfiguration advanceInfo=info.getInterfaceAdvanceConfiguration();
        if(NMS.PHY_SUB_TYPE_802_11_N ==config.phySubType || 
				NMS.PHY_SUB_TYPE_802_11_AC ==config.phySubType ||
				NMS.PHY_SUB_TYPE_802_11_AN ==config.phySubType ||
				NMS.PHY_SUB_TYPE_802_11_ANAC ==config.phySubType ||
				NMS.PHY_SUB_TYPE_802_11_BGN ==config.phySubType
				){
        internalSetInterfaceAdvanceConfig(advanceInfo,(InterfaceAdvanceInfo)config.advanceInfo);
        }
        info.setTxPower((short) config.transmitPower);
        
    }
   /**
     * Creates a public NMS.InterfaceConfiguration object from the internal  
     * MeshViewer IAPInterfaceInfo object
     */  
    
    
    private InterfaceConfiguration internalGetInterfaceConfiguration(IInterfaceInfo info) {
        
        InterfaceConfiguration         interfaceConfig;      
        ISecurityConfiguration         apSecurityConfig;
        SecurityTypeInfo               sti;
        IConfiguration                 apConfig;
        IInterfaceConfiguration        ifConfig;
        IInterfaceAdvanceConfiguration advanceConfig;
        InterfaceAdvanceInfo           advance;
        
        interfaceConfig = new InterfaceConfiguration();
        
        interfaceConfig.ackTimeout  = info.getAckTimeout();
        
        switch(info.getServiceType()) {
        case Mesh.IF_SERVICE_ALL:
        case Mesh.IF_SERVICE_CLIENT_ONLY:
            interfaceConfig.allowClientConnection   = 1;
            break;
        default:
            interfaceConfig.allowClientConnection   = 0;
                
        }
        
        interfaceConfig.dcaList = new ShortArray(info.getDcaListCount());
        
        for(int i = 0; i < interfaceConfig.dcaList.length(); i++) {
            interfaceConfig.dcaList.set(i, info.getDcaChannel(i));
        }
        
        interfaceConfig.dynamicChannelAllocation    = info.getDynamicChannelAlloc();
        
        interfaceConfig.essid               = info.getEssid();
        interfaceConfig.fragThreshold       = (int) info.getFragThreshold();
        interfaceConfig.hideEssid           = info.getEssidHidden();
        interfaceConfig.macAddress          = info.getMacAddress().toString().toUpperCase();
        interfaceConfig.manualChannel       = info.getChannel();
        interfaceConfig.maxTransmitRate     = (int) info.getTxRate();
        interfaceConfig.name                = info.getName();
        interfaceConfig.operatingChannel    = info.getOperatingChannel();                    
        interfaceConfig.phySubType          = info.getMediumSubType();
        interfaceConfig.phyType             = info.getMediumType();
        interfaceConfig.rtsThreshold        = (int) info.getRtsThreshold();
        if(NMS.PHY_SUB_TYPE_802_11_N ==info.getMediumSubType()|| 
				NMS.PHY_SUB_TYPE_802_11_AC ==info.getMediumSubType()||
				NMS.PHY_SUB_TYPE_802_11_AN ==info.getMediumSubType()||
				NMS.PHY_SUB_TYPE_802_11_ANAC ==info.getMediumSubType()||
				NMS.PHY_SUB_TYPE_802_11_BGN ==info.getMediumSubType()
				){
        advanceConfig       = info.getInterfaceAdvanceConfiguration();
        advance             =  internalGetInterfaceAdvanceInfo(advanceConfig);
        interfaceConfig.advanceInfo = advance;
        }else{
        	 interfaceConfig.advanceInfo =null;        	 
        	 }
        apSecurityConfig    = info.getSecurityConfiguration();  
        sti                 = internalGetSecurityTypeInfo(apSecurityConfig);
        
        interfaceConfig.securityInfo    = sti.securityInfo;
        interfaceConfig.securityType    = sti.securityType;
        
        interfaceConfig.transmitPower = info.getTxPower();
        interfaceConfig.usageType     = info.getUsageType();
        
        
        
        apConfig    = accessPoint.getConfiguration();
        ifConfig    = apConfig.getInterfaceConfiguration();
        
        /**
         * Calculate the interface identifier by looping through
         * all interfaces and incrementing if usage type, phy type,
         * and phy sub type match.
         * The loop breaks when the current interface is detected.
         */
        
        interfaceConfig.identifier = 0;
        
        for(int i = 0; i < ifConfig.getInterfaceCount(); i++) {
            
            IInterfaceInfo    tempInfo;
            
            tempInfo = ifConfig.getInterfaceByIndex(i);
            
            if(interfaceConfig.name.equalsIgnoreCase(tempInfo.getName()))
                break;
            
            if(tempInfo.getUsageType() == interfaceConfig.usageType
            && tempInfo.getMediumType() == interfaceConfig.phyType) {
                
                short subType;
                
                subType = tempInfo.getMediumSubType();
                
                switch(interfaceConfig.phySubType) {
                case NMS.PHY_SUB_TYPE_802_11_B:
                case NMS.PHY_SUB_TYPE_802_11_BG:
                case NMS.PHY_SUB_TYPE_802_11_G:
                case NMS.PHY_SUB_TYPE_802_11_BGN:
                    if(subType == NMS.PHY_SUB_TYPE_802_11_B
                    || subType == NMS.PHY_SUB_TYPE_802_11_BG
                    || subType == NMS.PHY_SUB_TYPE_802_11_G
                    || subType == NMS.PHY_SUB_TYPE_802_11_BGN) {
                        ++interfaceConfig.identifier;
                    }
                    break;
                case NMS.PHY_SUB_TYPE_802_11_A:
                case NMS.PHY_SUB_TYPE_802_11_AC:
                case NMS.PHY_SUB_TYPE_802_11_AN:
                case NMS.PHY_SUB_TYPE_802_11_ANAC:
                	 if(subType == NMS.PHY_SUB_TYPE_802_11_A
                     || subType == NMS.PHY_SUB_TYPE_802_11_AC
                     || subType == NMS.PHY_SUB_TYPE_802_11_AN
                     || subType == NMS.PHY_SUB_TYPE_802_11_ANAC) {
                         ++interfaceConfig.identifier;
                     }
                     break;
                case  NMS.PHY_SUB_TYPE_802_11_N:
                	   ++interfaceConfig.identifier;
                	   break;
                default:
                    if(subType == interfaceConfig.phySubType)
                        ++interfaceConfig.identifier;
                }
            }
            
        }
                
        return interfaceConfig;        
    }
    
    private InterfaceAdvanceInfo internalGetInterfaceAdvanceInfo(IInterfaceAdvanceConfiguration advanceConfig) {
    	InterfaceAdvanceInfo advance=new InterfaceAdvanceInfo();
    	advance.channelBandwidth         = advanceConfig.getChannelBandwidth();
    	advance.secondaryChannelPosition = advanceConfig.getSecondaryChannelPosition();
    	advance.guardInterval_20         = advanceConfig.getGuardInterval_20();
    	advance.guardInterval_40         = advanceConfig.getGuardInterval_40();
        advance.guardInterval_80         = advanceConfig.getGuardInterval_80();
    	advance.ldpc                     = advanceConfig.getLDPC();
    	advance.rxSTBC                   = advanceConfig.getRxSTBC();
    	advance.txSTBC                   = advanceConfig.getTxSTBC();
    	advance.frameAggregation         = advanceConfig.getFrameAggregation();
    	advance.gfMode                   = advanceConfig.getGFMode();
    	advance.coexistence              = advanceConfig.getCoexistence();
    	advance.MaxAMPDU                 = advanceConfig.getMaxAMPDU();
    	advance.MaxAMSDU                 = advanceConfig.getMaxAMSDU();
    	advance.MaxMPDU                  = advanceConfig.getMaxMPDU();
    	advance.maxAMPDUEnabled          = advanceConfig.getMaxAMPDUEnabled();
    	
    	return advance;
		
	}

    private void internalSetInterfaceAdvanceConfig(IInterfaceAdvanceConfiguration advanceInfo,
			InterfaceAdvanceInfo interfaceAdvanceInfo) {
    	     advanceInfo.setChannelBandwidth(interfaceAdvanceInfo.channelBandwidth );
    	     advanceInfo.setSecondaryChannelPosition(interfaceAdvanceInfo.secondaryChannelPosition);
    	     advanceInfo.setGuardInterval_20(interfaceAdvanceInfo.guardInterval_20);
    	     advanceInfo.setGuardInterval_40(interfaceAdvanceInfo.guardInterval_40);
    	     advanceInfo.setGuardInterval_80(interfaceAdvanceInfo.guardInterval_80);
    	     advanceInfo.setLDPC(interfaceAdvanceInfo.ldpc);
    	     advanceInfo.setRxSTBC(interfaceAdvanceInfo.rxSTBC);
    	     advanceInfo.setTxSTBC(interfaceAdvanceInfo.txSTBC);
    	     advanceInfo.setFrameAggregation(interfaceAdvanceInfo.frameAggregation);
    	     advanceInfo.setGFMode(interfaceAdvanceInfo.gfMode);
    	     advanceInfo.setCoexistence(interfaceAdvanceInfo.coexistence);
    	     advanceInfo.setMaxAMPDU(interfaceAdvanceInfo.MaxAMPDU);
    	     advanceInfo.setMaxAMSDU(interfaceAdvanceInfo.MaxAMSDU);
    	     advanceInfo.setMaxMPDU(interfaceAdvanceInfo.MaxMPDU );
    	     advanceInfo.setMaxAMPDUEnabled(interfaceAdvanceInfo.maxAMPDUEnabled);
		
	}
	/**
     * Sets up the values for the internal MeshViewer IACLConfiguration from
     * the public NMS.ACLConfiguration object
     */    
    
    private void internalSetACLConfiguration(IACLConfiguration aclConfig,
                                             ACLConfiguration  configuration) {
        
        ACLEntry    entry;
        MacAddress  macAddress;
        IACLInfo  	info;
        
        for(int i = 0; i < configuration.entries.length(); i++) {
            
            entry = (ACLEntry)configuration.entries.get(i);
            
            if(entry.macAddress.equalsIgnoreCase(BROADCAST_MAC))
                continue;
            
            macAddress  = new MacAddress();
            macAddress.setBytes(entry.macAddress);
            info = aclConfig.addAclInfo(macAddress);
            
            info.set80211eCategoryIndex(entry.dot11eCategory);
            info.setAllow((short) (entry.block == 0 ? 1 : 0));
            info.setEnable80211eCategory(entry.dot11eEnabled);
            info.setVLANTag(entry.vlanTag);
            
        }
        
        if(configuration.whiteList != 0) {
            
            macAddress  = new MacAddress();
            macAddress.setBytes(BROADCAST_MAC);
            
            info = aclConfig.addAclInfo(macAddress);            
            info.setAllow((short) 0);
            info.setVLANTag(-1);
            info.setEnable80211eCategory((short) 0);
            info.set80211eCategoryIndex((short) 0);
        }
        
    }
        
    /**
     * Creates a public NMS.ACLConfiguration object from the internal  
     * MeshViewer IACLConfiguration object
     */  

    private ACLConfiguration internalGetACLConfiguration(IACLConfiguration aclConfig) {
        
        ACLConfiguration    configuration;
        ACLEntry            entry;
        IACLInfo          info;  
        String              staAddress;
        
        configuration           = new ACLConfiguration();
        
        configuration.whiteList = 0;
                
        for(int i = 0; i < aclConfig.getCount(); i++) {
            
            info        = aclConfig.getACLInfo(i);
            staAddress  = info.getStaAddress().toString();
            
            if(staAddress.equalsIgnoreCase(BROADCAST_MAC)) {
                if(info.getAllow() == 0) {
                    configuration.whiteList = 1;
                }
                continue;
            }
            
            entry   = new ACLEntry();      
            
            entry.block             = (short) ((info.getAllow() == 0) ? 1 : 0);
            entry.dot11eCategory    = info.get80211eCategoryIndex();
            entry.dot11eEnabled     = info.getEnable80211eCategory();
            entry.macAddress        = staAddress;
            entry.vlanTag           = (short)info.getVLANTag();
            
            configuration.entries.add(entry);
            
        }       
        
        return configuration;
    }    
    
    /**
     * Used internally as a common function to send changed
     * configuration packets and wait on the status.
     * @return 0 if successfully, negative number otherwise.
     */
    
    int internalSendConfigurationAndWait(Configuration  apOrigConfig,
                                         IConfiguration apConfig) {

        IConfigStatusHandler    statusHandler;

        statusHandler = accessPoint.getUpdateStatusHandler();        
        statusHandler.clearStatus();

        apOrigConfig.compare(apConfig,statusHandler);

        if(statusHandler.isAnyPacketChanged()) {

            MessageHandler  handler;
            int             ret;

            handler = new MessageHandler(statusHandler);            

            accessPoint.addMessageListener(handler);

            accessPoint.updateChangedConfiguration();

            ret = handler.waitForCompletion(15000);

            accessPoint.removeMessageListener(handler);

            return ret;

        }

        return 0;
    }    
    
    /**
     * Implements the internal MeshViewer IMessageHandler interface
     * and signals waiting threads when all configuration packets
     * are sent successfully.
     */
    
    private class MessageHandler implements IMessageHandler {
        
        MessageHandler(IConfigStatusHandler statusHandler) {
            
            this.statusHandler  = statusHandler;
            
            try {
                this.event = SyncEvent.create(SyncEvent.STATE_NOT_SIGNALLED, false, null);
            } catch (Exception e) {
               
            }
            
        }

        public void showMessage(IMessageSource msgSource,
                                String messageHeader,
                                String message,
                                String progress) {
            
            if(statusHandler.getPendingStatusCount() == 0) {
                event.signalEvent();
            }            
        }
        
        public int waitForCompletion(long timeout) {
            return event.waitOn(timeout);
        }
        
        private IConfigStatusHandler    statusHandler;
        private SyncEvent               event;
    }
    
    /**
     * Used internally to get the security type and
     * information from the internal MeshViewer structure
     */
        
    private static class SecurityTypeInfo {

        SecurityTypeInfo(short securityType, Object securityInfo) {
            this.securityType   = securityType;
            this.securityInfo   = securityInfo;
        }
        
        Object   securityInfo;
        short    securityType;
    }
    
    /**
     * Enumerates through the vlans returned by the 
     * MeshViewer internal IVlanConfiguration object and provides
     * the public NMS.VlanConfiguration to callers
     */
    
    private class VlanConfigurationEnumerator implements Enumeration<VlanConfiguration> {

        VlanConfigurationEnumerator(IVlanConfiguration vlanConfig) {
            this.vlanConfig = vlanConfig;
            this.vlanIndex  = 0;
        }
        
        public boolean hasMoreElements() {
            return (vlanIndex < vlanConfig.getVlanCount());
        }

        public VlanConfiguration nextElement() {
            
            IVlanInfo info;
            
            info = vlanConfig.getVlanInfoByIndex(vlanIndex++); 
                        
            return internalGetVlanConfiguration(info);
            
        }
        
        private IVlanConfiguration  vlanConfig;
        private int                 vlanIndex;
        
    }
    
    /**
     * Enumerates through the interfaces returned by the 
     * MeshViewer internal IInterfaceConfiguration object and provides
     * the public NMS.InterfaceConfiguration to callers
     */    
    
    private class InterfaceConfigurationEnumerator implements Enumeration<InterfaceConfiguration> {
        
        InterfaceConfigurationEnumerator(IInterfaceConfiguration ifConfig) {
            this.ifConfig   = ifConfig;
            this.ifIndex    = 0;
        }

        public boolean hasMoreElements() {
            return (ifIndex < ifConfig.getInterfaceCount());
        }

        public InterfaceConfiguration nextElement() {
            
            IInterfaceInfo    ifInfo;
            
            ifInfo  = ifConfig.getInterfaceByIndex(ifIndex++);
                                  
            return internalGetInterfaceConfiguration(ifInfo);
        }
        
        private IInterfaceConfiguration ifConfig;
        private int                     ifIndex;
        
    }
    
    /**
     * Enumerates through the internal IStaConfiguration enumeration object
     * and provides callers with the public NMS.ConnectedDevice object
     */
    
    private class ConnectedDeviceEnumerator implements Enumeration<ConnectedDevice> {
        
        public ConnectedDeviceEnumerator(Enumeration<IStaConfiguration> staList) {
            
            this.staList = staList;
            
        }

        public boolean hasMoreElements() {
            return staList.hasMoreElements();
        }

        public ConnectedDevice nextElement() {
            
            IStaConfiguration staInfo;
            
            staInfo = staList.nextElement();
            
            return new ConnectedDeviceImpl(staInfo);
            
        }
        
        private Enumeration<IStaConfiguration> staList;
        
    }
    
    /**
     * Enumerates through the internal MeshViewer KnownAp enumeration
     * and provides callers with the public NMS.NeighborNode object
     */
    
    private class NeighborNodeEnumerator implements Enumeration<NeighborNode> {
        
        public NeighborNodeEnumerator(Enumeration<KnownAp> knownAps) {            
            this.knownAps   = knownAps;
            
        }

        public boolean hasMoreElements() {
            return knownAps.hasMoreElements();
        }

        public NeighborNode nextElement() {
                        
            KnownAp             ap;
            NeighborNodeImpl   neighbor; 
            
            ap  = knownAps.nextElement();
            
            neighbor    = new NeighborNodeImpl(network,ap);            
                        
            return neighbor;
            
        }
        
        private Enumeration<KnownAp> knownAps;
        
    }

    private class CommandResponseHandler implements ICommandExecutionListener {
		
    	CommandResponseHandler() {
    		meshCommandEvent 	= null;
    		cmdResponse			= "";
    	}

    	String waitForResponse() {
       		try {
       			if(meshCommandEvent == null)
       				meshCommandEvent = SyncEvent.create(SyncEvent.STATE_NOT_SIGNALLED, 
       													false,
       													null
       													);
			} catch (Exception e) {
				meshCommandEvent = null;
				return "";
			}
    		
			cmdResponse = "";
			
			meshCommandEvent.waitOn();
			
			return cmdResponse;
    	}
    	
		@Override
		public void commandExecutionResponse(AccessPoint ap, String command, String responseStr) {
    		cmdResponse 		= responseStr;
    		if(meshCommandEvent != null)
    			meshCommandEvent.signalEvent();
		}
    	
    	private SyncEvent 	meshCommandEvent;
		private	String		cmdResponse;
    }

    private class IperfResponseHandler implements IperfListener {
		
    	IperfResponseHandler() {
    		iperfEvent 		= null;
    		iperfResponse	= "";
    	}

    	String waitForResponse() {
       		try {
       			if(iperfEvent == null)
       				iperfEvent = SyncEvent.create(SyncEvent.STATE_NOT_SIGNALLED, 
       													false,
       													null
       													);
			} catch (Exception e) {
				iperfEvent = null;
				return "";
			}
    		
			iperfResponse = "";
			
			iperfEvent.waitOn();
			
			return iperfResponse;
    	}
    	
		@Override
		public void appendResult(String input_line) {
		}

		@Override
		public void testEnded() {
		}

		@Override
		public void testResult(String output) {
    		iperfResponse 		= output;
    		if(iperfEvent != null)
    			iperfEvent.signalEvent();
		}

		@Override
		public void testStarted() {
		}
		
    	private SyncEvent 	iperfEvent;
		private	String		iperfResponse;
		
    }

    private class FwUpdateResponseHandler implements IApFwUpdateListener {

    	FwUpdateResponseHandler() {
    		fwUpdateEvent		= null;
    		fwUpdateResponse	= 0;
    	}

    	int waitForResponse() {
       		try {
       			if(fwUpdateEvent == null)
       				fwUpdateEvent = SyncEvent.create(SyncEvent.STATE_NOT_SIGNALLED, 
       													false,
       													null
       													);
			} catch (Exception e) {
				fwUpdateEvent = null;
				return Mesh.FW_UPDATE_RETURN_FAILED;
			}
    		
			fwUpdateResponse = 0;
			
			fwUpdateEvent.waitOn();
			
			return fwUpdateResponse;
    	}
    	
		@Override
		public void notifyApFileUpdate(int currentFileNo, int totalFileCount) {
		}
		
		@Override
		public void notifyUpdateStatus(int ret, String status) {
			fwUpdateResponse = ret;
			if(ret <= 1 && fwUpdateEvent != null)
				fwUpdateEvent.signalEvent();
		}

    	private SyncEvent 	fwUpdateEvent;
		private	int			fwUpdateResponse;
		
    }
    
    Configuration   			originalConfig;
    AccessPoint     			accessPoint;
    Network         			network;
    
    private CommandResponseHandler		cmdResponseHandler;
    private IperfResponseHandler		iperfResponseHandler;
    private FwUpdateResponseHandler		fwUpdateResponseHandler;
    
    private static final String BROADCAST_MAC = "FF:FF:FF:FF:FF:FF";

}
