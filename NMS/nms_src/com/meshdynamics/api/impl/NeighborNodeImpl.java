/*
 * Created Date: Jul 15, 2008
 * Author : Sri
 */
package com.meshdynamics.api.impl;

import java.util.Vector;

import com.meshdynamics.api.NMS.NeighborNode;
import com.meshdynamics.api.NMS.Network;
import com.meshdynamics.api.NMS.Node;
import com.meshdynamics.meshviewer.configuration.KnownAp;

public class NeighborNodeImpl implements NeighborNode {
    
    NeighborNodeImpl(Network network, KnownAp knownAp){       
        
        node   = network.getNodeByMacAddress(knownAp.macAddress.toString());
        
        infos  = new Vector<Info>();        
        
        for(int i = 0; i < knownAp.knownApInfo.size(); i++) {
            
            Info    info;
            
            info    = new Info(knownAp.knownApInfo.get(i).bitRate,
                               knownAp.knownApInfo.get(i).signal);
            
            infos.add(info);            
            
        }
                
    }

   
    public Node getNode() {
        return node;
    }

    public int getUplinkSignal() { return infos.get(0).signal; }

    public int getUplinkTxBitRate() { return infos.get(0).rate; }

    public int getDownlinkCount() { return infos.size(); }

    public int getUplinkSignal(int downlinkIndex) { return infos.get(downlinkIndex).signal; }

    public int getUplinkTxBitRate(int downlinkIndex) { return infos.get(downlinkIndex).rate; }
    
    private static class Info {
        
        public Info(int rate, int signal) {
            this.rate   = rate;
            this.signal = signal;
        }
        
        public int rate;
        public int signal;
    }
    
    private Node            node;
    private Vector<Info>    infos;

}
