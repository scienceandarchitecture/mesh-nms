/*
 * Created Date: Jul 17, 2008
 * Author : Sri
 */

package com.meshdynamics.api.impl;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Enumeration;

import com.meshdynamics.api.NMS;
import com.meshdynamics.api.NMS.GeneralConfiguration;
import com.meshdynamics.api.NMS.InterfaceConfiguration;
import com.meshdynamics.api.NMS.Node;
import com.meshdynamics.api.NMS.VlanConfiguration;

public class NodeMacroScriptGenerator {       
        
    public NodeMacroScriptGenerator(Node node) {        
        
        this.node           = node;
        this.outputStream   = new ByteArrayOutputStream();
        this.printStream    = new PrintStream(this.outputStream);
        
        this.uplinks        = new int[NMS.PHY_SUB_TYPE_802_11_PSF + 1];
        this.downlinks      = new int[NMS.PHY_SUB_TYPE_802_11_PSF + 1];
        this.scanners       = new int[NMS.PHY_SUB_TYPE_802_11_PSF + 1];        
        
        for(int i = 0; i < NMS.PHY_SUB_TYPE_802_11_PSF + 1; i++) {
            this.uplinks[i]     = 0;
            this.downlinks[i]   = 0;
            this.scanners[i]    = 0;
        }
        
    }    
    
    public String generate() throws Exception {
        
        InputStream resourceStream;  
        byte[]      templateBuffer;
        
        setupGlobals();
        
        setupInterfaces();
        
        setupVlans();
        
        setupAcl();     
        
        printStream.printf("\n\n");
        
        resourceStream  = NodeMacroScriptGenerator.class.getResourceAsStream("NodeMacroTemplate.rb");
        templateBuffer  = new byte[resourceStream.available()];
        resourceStream.read(templateBuffer);               
        outputStream.write(templateBuffer);            
        
        return outputStream.toString();
        
    }                

    protected void setupGlobals() {
        
        GeneralConfiguration genConfig;
        
        genConfig = node.getGeneralConfiguration();
    
        printStream.printf( "$effistreamXMLSpec = <<END_OF_XML_SPEC\n"
                          + "%s\n"
                          + "END_OF_XML_SPEC\n\n",
                           node.getEffistreamRules().toXmlSpec());
                
        printStream.printf( "$effistreamRules   = NMS::EffistreamRule::fromXmlSpec($effistreamXMLSpec)\n"
                          + "$interfaces        = NMS::ObjectArray.new()\n"
                          + "$vlans             = NMS::ObjectArray.new()\n"
                          + "$mobilityMode      = %d\n"
                          + "$options           = %d\n"
                          + "$heartbeatInterval = %d\n"
                          + "$countryCode       = %d\n"
                          + "$regDomain         = %d\n"
                          + "$dfsRequired       = %d\n",
                          genConfig.mobilityMode,
                          genConfig.options,
                          genConfig.heartbeatInterval,
                          genConfig.countryCode,
                          genConfig.regulatoryDomain,
                          genConfig.dfsRequired);
        
        
        
    }
    
    protected void setupInterfaces() {
        
        Enumeration<InterfaceConfiguration> interfaces;
        InterfaceConfiguration              configuration;
        short                               index;
        
        printStream.printf("\ndef initializeInterfaces()\n\n");        
        
        interfaces = node.getInterfaces();
        
        while(interfaces.hasMoreElements()) {
            
            configuration = interfaces.nextElement();
            
            /**
             * To make sure the phy-sub-type of ETHERNET interfaces 
             * is set to NMS.PHY_SUB_TYPE_IGNORE
             */
            
            if(configuration.phyType == NMS.PHY_TYPE_ETHERNET) {
                configuration.phySubType = NMS.PHY_SUB_TYPE_IGNORE;
            }
            
            index = configuration.phySubType; 
            
            /**
             * We set B,BG and G in the same bucket (i.e 2.4 GhHz)
             */
                            
            if(index == NMS.PHY_SUB_TYPE_802_11_B ||
            		index == NMS.PHY_SUB_TYPE_802_11_BG || 
            		index == NMS.PHY_SUB_TYPE_802_11_G ||
            		index == NMS.PHY_SUB_TYPE_802_11_BGN ){
            	if(configuration.usageType ==NMS.USAGE_TYPE_DOWNLINK)
                index = NMS.PHY_SUB_TYPE_802_11_B;
            }else if(index == NMS.PHY_SUB_TYPE_802_11_A ||
            		index == NMS.PHY_SUB_TYPE_802_11_AN ||
            		index == NMS.PHY_SUB_TYPE_802_11_AC || 
            		index == NMS.PHY_SUB_TYPE_802_11_ANAC){
            	  if(configuration.usageType ==NMS.USAGE_TYPE_UPLINK)
            		 index = NMS.PHY_SUB_TYPE_802_11_A;
            	  if(configuration.usageType ==NMS.USAGE_TYPE_DOWNLINK)
                    index = NMS.PHY_SUB_TYPE_802_11_B;
            }else if(index == NMS.PHY_SUB_TYPE_802_11_N){
            	    if(configuration.usageType ==NMS.USAGE_TYPE_UPLINK)
             		 index = NMS.PHY_SUB_TYPE_802_11_A;
             	     if(configuration.usageType ==NMS.USAGE_TYPE_DOWNLINK)
                     index = NMS.PHY_SUB_TYPE_802_11_B;
            }
            
            switch(configuration.usageType) {
            case NMS.USAGE_TYPE_UPLINK:
                ++uplinks[index];
                break;
            case NMS.USAGE_TYPE_DOWNLINK:
                ++downlinks[index];
                break;
            case NMS.USAGE_TYPE_SCANNER:
                ++scanners[index];
                break;                
            default:
                continue;
            }        
            
            printStream.printf("\tintFace = NMS::InterfaceConfiguration.new(<<END_OF_STRING\n"
                              + "%s\n"
                              + "END_OF_STRING\n)\n\t$interfaces.add(intFace)\n\n",
                              configuration.toString());            
            
        }
        
        printStream.printf("\nend\n\n" 
                         + "$uplinkCounts = [%d,%d,%d,%d,%d,%d,%d,%d]\n"
                         + "$downlinkCounts = [%d,%d,%d,%d,%d,%d,%d,%d]\n"
                         + "$scannerCounts = [%d,%d,%d,%d,%d,%d,%d,%d]\n",
                         uplinks[0],uplinks[1],uplinks[2],uplinks[3],uplinks[4],uplinks[5],uplinks[6],uplinks[7],
                         downlinks[0],downlinks[1],downlinks[2],downlinks[3],downlinks[4],downlinks[5],downlinks[6],downlinks[7],
                         scanners[0],scanners[1],scanners[2],scanners[3],scanners[4],scanners[5],scanners[6],scanners[7]);
                  
    }
    
    protected void setupVlans() {
        
        Enumeration<VlanConfiguration>  vlans;
        VlanConfiguration               configuration;
        
        printStream.printf("\ndef intializeVlans()\n\n");        

        vlans = node.getVlans();
        
        while(vlans.hasMoreElements()) {
            
            configuration = vlans.nextElement();
            
            printStream.printf("\tvlan = NMS::VlanConfiguration.new(<<END_OF_STRING\n"
                    + "%s\n"
                    + "END_OF_STRING\n)\n\t$vlans.add(vlan)\n\n",
                    configuration.toString());            
                        
        }
        
        
        printStream.printf("\nend\n\n");
        
    }
    
    protected void setupAcl() {
        
        printStream.printf("$aclConfig = NMS::ACLConfiguration.new(<<END_OF_STRING\n"
                          + "%s\n"
                          + "END_OF_STRING\n\n)",
                          node.getACLConfiguration().toString());
    }
                          
    
    /**
     * uplinks is an array indexed according to PHY-SUB-TYPE.
     * 
     * Each element in the array holds a count of uplinks of the PHY-SUB-TYPE 
     * 
     * e.g. uplinks[NMS.PHY_SUB_TYPE_802_11_A] returns the number of  
     * 802.11a uplink interfaces
     */
    
    protected int[]            uplinks;
    
    /**
     * downlinks is an array indexed according to PHY-SUB-TYPE.
     * 
     * Each element in the array holds a count of downlinks of the PHY-SUB-TYPE 
     * 
     * e.g. downlinks[NMS.PHY_SUB_TYPE_802_11_A] returns the number of  
     * 802.11a downlink interfaces
     */
    
    protected int[]            downlinks;

    /**
     * scanners is an array indexed according to PHY-SUB-TYPE.
     * 
     * Each element in the array holds a count of scanners of the PHY-SUB-TYPE 
     * 
     * e.g. scanners[NMS.PHY_SUB_TYPE_802_11_A] returns the number of  
     * 802.11a scanner interfaces
     */
    
    protected int[]            scanners;
    
    protected Node                    node;
    protected PrintStream             printStream;
    protected ByteArrayOutputStream   outputStream;

}
