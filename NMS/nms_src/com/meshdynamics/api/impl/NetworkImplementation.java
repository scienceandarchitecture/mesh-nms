/*
 * Created Date: Jul 10, 2008
 * Author : Sri
 */
package com.meshdynamics.api.impl;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.Vector;


import com.meshdynamics.api.NMS;
import com.meshdynamics.api.NMS.GeneralConfiguration;
import com.meshdynamics.api.NMS.Network;
import com.meshdynamics.api.NMS.Node;
import com.meshdynamics.api.NMS.NetworkListener;

import com.meshdynamics.meshviewer.mesh.INetworkListener;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;
import com.meshdynamics.util.MacAddress;
import com.meshdynamics.util.SyncEvent;

/**
 * @author Sri
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class NetworkImplementation implements Network,INetworkListener, IAPPropListener {
    
    NetworkImplementation(MeshNetwork meshNetwork) {
        
        this.meshNetwork        = meshNetwork;
        this.nodes              = new Hashtable<String, NodeImplementation>();
        this.listeners          = new Vector<NetworkListener>();
        this.eventList          = new LinkedList<EventInformation>();
        this.waitQueues         = new Vector<NodeWaitQueue>();
        
        try {
            this.queueEvent         = SyncEvent.create(SyncEvent.STATE_NOT_SIGNALLED, true, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        this.eventThread        = new EventThread();
        
        this.eventThread.start();
        
        meshNetwork.addNetworkListener(this);
    }                                    

    /**
     * Methods from the 'Network' interface
     */
    
    public Enumeration<Node> getNodes() {        
        return new NodesEnumerator(nodes);
    }

    public int deleteNode(Node node) {
        
        /**
         * TODO 
         */
        
        return 0;
    }

    public int addListener(NetworkListener listener) {
                
        listeners.add(listener);        
        
        return 0;
        
    }

    public int removeListener(NetworkListener listener) {
        
        if(listeners.remove(listener))
            return 0;
        
        return -1;
    }

    public Node getNodeByMacAddress(String macAddress) {
        return nodes.get(macAddress.toUpperCase());
    }


    public String getName() {
        return meshNetwork.getNwName();
    }

    public int waitForNodeDetect(String macAddresses, long timeout) {
        
        NodeWaitQueue   waitQueue;
        int             ret;
        
        waitQueue = new NodeWaitQueue(macAddresses);
        
        if(waitQueue.check()) {
            waitQueue = null;
            return 0;
        }            
        
        waitQueues.add(waitQueue);
        
        ret = waitQueue.waitOn(timeout);
        
        waitQueues.remove(waitQueue);
        
        waitQueue = null;
        
        return ret;
        
    }
    
    /**
     * Methods from the 'INetworkListener' interface
     */    
    
    public void accessPointAdded(AccessPoint accessPoint) {
        
        NodeImplementation  node;
        
        node    = new NodeImplementation(this,accessPoint);     
        
        nodes.put(accessPoint.getDSMacAddress().toUpperCase(), node);        
        
        accessPoint.addPropertyListener(this);
                
    }

    public void accessPointDeleted(AccessPoint accessPoint) {
        
        NodeImplementation  node;
        
        node    = nodes.get(accessPoint.getDSMacAddress().toUpperCase());        
        
        if(node == null)
            return;                
        
        /**
         * Remove the node from our list
         */
        
        nodes.remove(accessPoint.getDSMacAddress().toUpperCase());
        
        node.unInitialize();        
    }

    public void meshViewerStarted() {
        
        /**
         * TODO
         * It is not clear from an API point-of-view what the use of this message is.
         * Hence until we determine a need for API clients to know about this messsage
         * we won't implement
         */
        
    }

    public void meshViewerStopped() {
        
        /**
         * TODO
         * It is not clear from an API point-of-view what the use of this message is.
         * Hence until we determine a need for API clients to know about this messsage
         * we won't implement
         */        
        
    }

    public void notifyHealthAlert(int healthStatus, String meshId) {
        /**
         * TODO
         * It is not clear from an API point-of-view what the use of this message is.
         * Hence until we determine a need for API clients to know about this messsage
         * we won't implement
         */         
    }

    public void saturationInfoReceived(AccessPoint ap, String ifName) {
        /**
         * TODO
         * It is not clear from an API point-of-view what the use of this message is.
         * Hence until we determine a need for API clients to know about this messsage
         * we won't implement
         */        
    }

    /**
     * Methods from the 'IAPPropListener' interface
     */   
    
    public void apPropertyChanged(int filter, int subFilter, AccessPoint src) {
        
        NodeImplementation  node;
        int                 event;
        
        node    = nodes.get(src.getDSMacAddress().toUpperCase());           

        if(node == null) {            
            
            /**
             * Should not happen as accessPointAdded is called 
             * before apPropertyChanged
             */
            
            System.err.println("Network: Node '" + src.getDsMacAddress() + "' was not found");
                 
        }        
        
        /**
         * We process only Heartbeat2 here so that the AccessPoint
         * object is already updated with Heartbeat and Heartbeat2 
         * values.
         * 
         */
                
        switch(filter) {
        case IAPPropListener.MASK_HEARTBEAT2:
            event   = NMS.EVENT_NODE_HEARTBEAT;
            break;
        case IAPPropListener.MASK_AP_STATE:
            switch(subFilter) {
            case IAPPropListener.AP_STATE_DEAD:
                event   = NMS.EVENT_NODE_DEAD;
                break;
            case IAPPropListener.AP_STATE_SCANNING:
                event   = NMS.EVENT_NODE_SCAN;
                break;
            case IAPPropListener.AP_STATE_HEARTBEAT_MISSED:
                event   = NMS.EVENT_NODE_HEARTBEAT_MISS;
                break;
            default:
                return;
            }
            break;
        default:
            return;
        }
        
        if(event == NMS.EVENT_NODE_HEARTBEAT) {
            
            try {
                for(int i = 0; i < waitQueues.size(); i++) {
                    waitQueues.get(i).processNode(node);                
                }
            } catch(Exception e) {
                /** Silently ignore exception */
            }
        }
        
        synchronized(eventList) {
            eventList.addLast(new EventInformation(event,this,node));
        }
        
        queueEvent.signalEvent();
        
    }
    
    void unInitialize() {
        
        Enumeration<String> nodeIds;
        MacAddress          macAddress;
        AccessPoint         accessPoint;
        
        meshNetwork.removeNetworkListener(this);
        
        nodeIds     = nodes.keys();
        macAddress  = new MacAddress();
        
        while(nodeIds.hasMoreElements()) {
                       
            macAddress.setBytes(nodeIds.nextElement());
            
            accessPoint = meshNetwork.getAccessPoint(macAddress);
            
            if(accessPoint != null) {
                accessPoint.removePropertyListener(this);
            }            
        }
                
        eventThread.unInitialize();
                
        nodes.clear();                
        
        nodes       = null;
        eventThread = null;       
                
    }
    
    private static class EventInformation {
        
        EventInformation(int event, Network network, Node node) {
            this.event      = event;
            this.network    = network;
            this.node       = node;
        }
        
        public Network  network;
        public Node     node;
        public int      event;
    }
    
    private static class NodesEnumerator implements Enumeration<Node> {
        
        public NodesEnumerator(Hashtable<String,NodeImplementation> nodes) {
            this.nodes  = nodes;
            this.keys   = nodes.keys();
        }

        public boolean hasMoreElements() {
            return keys.hasMoreElements();
        }

        public Node nextElement() {
            
            String  key;
            
            key = keys.nextElement();
                        
            return nodes.get(key);
        }
        
        private Hashtable<String,NodeImplementation>    nodes;
        private Enumeration<String>                     keys;     
        
    }
    
    private class EventThread extends Thread {
        
        EventThread() {            
            quit    = false;
            
        }
        
        public void unInitialize() {
            
            quit    = true;
            
            queueEvent.signalEvent();            
        }
        
        public void run() {
            
            EventInformation    event;
            int                 i;
            
            System.err.println("Network: Event generator started for network '" + meshNetwork.getNwName() + "'");
            
            while(true) {
                
                queueEvent.resetEvent();
                queueEvent.waitOn();                

                if(quit)
                    break;              
                
                while(true) {
                    
                    synchronized(eventList) {
                        try {
                            event   = (EventInformation)eventList.removeFirst();
                        } catch(Exception e) {
                            event = null;
                        }
                    }                
                    
                    if(event == null)
                        break;                               
                    
                    for(i = 0; i < listeners.size(); i++) {
                        try {
                            listeners.get(i).onEvent(event.event, event.network, event.node);
                        } catch(Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                
            }
            
            eventList.clear();
            
            eventList   = null;

            SyncEvent.close(queueEvent);
            
            System.err.println("Network: Event generator stopped for network '" + meshNetwork.getNwName() + "'");
                        
            meshNetwork = null;            
            
        }
        
        private boolean quit;
        
    }
    
    private class NodeWaitQueue {
        
        NodeWaitQueue(String macAddresses) {
            
            StringTokenizer st;
            String          token;
            
            try {
                event = SyncEvent.create(SyncEvent.STATE_NOT_SIGNALLED, false, null);
            } catch (Exception e) {

            }
            
            macAddresses = macAddresses.toUpperCase();
            
            nodeList        = new Hashtable<String,String>();
            nodeDetectCount = 0;
            
            st = new StringTokenizer(macAddresses,",");
            
            while(st.hasMoreTokens()) {
                
                token = st.nextToken();
                
                nodeList.put(token,Integer.toString(0));
                
                ++nodeCount;                
            }
            
            initialize();
                        
        }
        
        void processNode(Node node) {
            
            GeneralConfiguration    config;
            String                  value;
            
            config = node.getGeneralConfiguration();
            
            if(config == null)
                return;
            
            value   = (String)nodeList.get(node.getUnitMacAddress());
            
            if(value != null) {
                
                if(Integer.parseInt(value) == 0) {
                    ++nodeDetectCount;
                    nodeList.put(node.getUnitMacAddress(),Integer.toString(1));
                }               
                
            }
            
            if(nodeDetectCount == nodeCount) {
                event.signalEvent();
            }
                        
            config = null;
            
        }
        
        int waitOn(long timeout) {
            return event.waitOn(timeout);
        }
        
        private void initialize() {
            
            Enumeration<Node>       nodes;
            Node                    node;
            String                  value;
            
            nodes = getNodes();
            
            while(nodes.hasMoreElements()) {
                
                node    = nodes.nextElement();
                value   = (String)nodeList.get(node.getUnitMacAddress());                
                
                if(value != null) {
                    
                    GeneralConfiguration gc;
                    
                    gc = node.getGeneralConfiguration();
                    
                    if(gc == null)
                        continue;
                    
                    nodeList.put(node.getUnitMacAddress(),Integer.toString(1));
                    ++nodeDetectCount;                    
                }
            }            
        }
        
        private boolean check() {            
            return (nodeDetectCount == nodeCount);
        }
        
        
        private Hashtable<String,String>   	nodeList;
        private int         				nodeDetectCount;
        private int         				nodeCount;
        private SyncEvent   				event;        
    }

    private SyncEvent                               queueEvent;
    private LinkedList<EventInformation>            eventList;
    private MeshNetwork                             meshNetwork;
    private Hashtable<String,NodeImplementation>    nodes;
    private Vector<NetworkListener>                 listeners;
    private Vector<NodeWaitQueue>                   waitQueues;
    private EventThread                             eventThread;

}
