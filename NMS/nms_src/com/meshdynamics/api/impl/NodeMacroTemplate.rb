
def setInterfaces(node)
 

  interfaces = node.getInterfaces()

  while interfaces.hasMoreElements()

    intFace = interfaces.nextElement()
    
    puts "\t\tConfiguring interface " + intFace.name + "\n"

    # Loop through source interfaces and find best match

    0.upto($interfaces.length() - 1) do |i|
     
      srcIntFace = $interfaces.get(i)

      # Skip interfaces with different usage type

      next if srcIntFace.usageType != intFace.usageType
      

      # Skip interfaces with mis-matched phySubTypes
      # The 2.4 GHz B/G/BG/BGN modes are considered equivalent
      
      if srcIntFace.phySubType == NMS::PHY_SUB_TYPE_802_11_B or
         srcIntFace.phySubType == NMS::PHY_SUB_TYPE_802_11_BG or
         srcIntFace.phySubType == NMS::PHY_SUB_TYPE_802_11_G  or
         srcIntFace.phySubType == NMS::PHY_SUB_TYPE_802_11_BGN then
          
          index = NMS::PHY_SUB_TYPE_802_11_B
          next if (intFace.phySubType != NMS::PHY_SUB_TYPE_802_11_B && intFace.phySubType != NMS::PHY_SUB_TYPE_802_11_G && intFace.phySubType != NMS::PHY_SUB_TYPE_802_11_BG && intFace.phySubType != NMS::PHY_SUB_TYPE_802_11_BGN)         
   
     elsif srcIntFace.phySubType == NMS::PHY_SUB_TYPE_802_11_A or
         srcIntFace.phySubType == NMS::PHY_SUB_TYPE_802_11_AN  or
         srcIntFace.phySubType == NMS::PHY_SUB_TYPE_802_11_AC  or
         srcIntFace.phySubType == NMS::PHY_SUB_TYPE_802_11_ANAC then         
         if intFace.usageType == NMS::USAGE_TYPE_UPLINK then 
        
         index = NMS::PHY_SUB_TYPE_802_11_A
         next if (intFace.phySubType != NMS::PHY_SUB_TYPE_802_11_A && intFace.phySubType != NMS::PHY_SUB_TYPE_802_11_AN && intFace.phySubType != NMS::PHY_SUB_TYPE_802_11_AC && intFace.phySubType != NMS::PHY_SUB_TYPE_802_11_ANAC)
         
         
         elsif intFace.usageType == NMS::USAGE_TYPE_DOWNLINK then
         
          index = NMS::PHY_SUB_TYPE_802_11_B
          next if (intFace.phySubType != NMS::PHY_SUB_TYPE_802_11_A && intFace.phySubType != NMS::PHY_SUB_TYPE_802_11_AN && intFace.phySubType != NMS::PHY_SUB_TYPE_802_11_AC && intFace.phySubType != NMS::PHY_SUB_TYPE_802_11_ANAC)
     
        end
        
      else
        
          index = srcIntFace.phySubType
          next if srcIntFace.phySubType != intFace.phySubType        
        
      end              

      # If there are more interfaces of the same usageType and phySubType
      # then skip this one
      
      if intFace.usageType == NMS::USAGE_TYPE_UPLINK then
        if intFace.identifier > srcIntFace.identifier then
          next if intFace.identifier < $uplinkCounts[index].to_i()  
        end        
      elsif intFace.usageType == NMS::USAGE_TYPE_DOWNLINK then
        if intFace.identifier > srcIntFace.identifier then
          next if intFace.identifier < $downlinkCounts[index].to_i()  
        end   
      elsif intFace.usageType == NMS::USAGE_TYPE_SCANNER then
        if intFace.identifier > srcIntFace.identifier then
          next if intFace.identifier < $scannerCounts[index].to_i()  
        end   
      end      
      
      # Set the name and macAddress and call setInterfaceConfiguration

      srcIntFace.name       = intFace.name
      srcIntFace.macAddress = intFace.macAddress

      ret = node.setInterfaceConfiguration(srcIntFace)

      if ret != 0 then
        $stdOut.println "\t\tCould not set configuration for interface " + srcIntFace.name + " (" + ret + ")."
      end

    end # loop on $interfaces.length() for source interfaces

  end # loop on interfaces.hasMoreElements() for destination interfaces

  return 0


end

def nmsConfigMacroInit(nms,network)

  initializeInterfaces()

  intializeVlans()

end

def nmsConfigMacroUninit(nms, network)

  # Nothing to do

end

def nmsConfigMacroExecute(nms, network, node)

  $stdOut.println "Configuring node " + node.getUnitMacAddress()
  
  node.beginConfigurationUpdate()

  genConfig = node.getGeneralConfiguration()
  
  genConfig.mobilityMode           = $mobilityMode.to_i()
  genConfig.options                = $options.to_i()
  genConfig.heartbeatInterval      = $heartbeatInterval.to_i()
  genConfig.countryCode            = $countryCode.to_i()
  genConfig.regulatoryDomain       = $regDomain.to_i()
  genConfig.dfsRequired            = $dfsRequired.to_i()
  
  $stdOut.println "\tSetting General configuration..."
  ret = node.setGeneralConfiguration(genConfig)
  if ret != 0 then
    $stdOut.println "\t\tGeneral configuration not set"
  end

  $stdOut.println "\tSetting interface configuration..."
  setInterfaces(node)

  $stdOut.println "\tSetting VLAN configuration..."
  ret = node.setVlans($vlans)
  if ret != 0 then
    $stdOut.println "\t\tVLAN configuration not set"
  end

  $stdOut.println "\tSetting ACL configuration..."
  ret = node.setACLConfiguration($aclConfig)
  if ret != 0 then
    $stdOut.println "\t\tACL configuration not set"
  end

  $stdOut.println "\tSetting Effistream configuration..."
  ret = node.setEffistreamRules($effistreamRules)
  if ret != 0 then
    $stdOut.println "\t\tEffistream configuration not set"
  end
  
  $stdOut.println "\tCommitting configuration..."  
  ret = node.commitConfigurationUpdate()
  if ret != 0 then
    $stdOut.println "\t\tConfiguration update was not successful"
  else
    $stdOut.println "\t\tConfiguration update successful"
  end
  
end

