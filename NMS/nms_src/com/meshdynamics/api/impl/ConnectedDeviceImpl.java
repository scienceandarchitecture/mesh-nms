/*
 * Created Date: Jul 15, 2008
 * Author : Sri
 */
package com.meshdynamics.api.impl;

import com.meshdynamics.api.NMS.ConnectedDevice;
import com.meshdynamics.meshviewer.configuration.IStaConfiguration;

public class ConnectedDeviceImpl implements ConnectedDevice {
    
    ConnectedDeviceImpl(IStaConfiguration staInfo) {
        
        macAddress  = staInfo.getStaMacAddress().toString().toUpperCase();
        rxSignal    = (int) staInfo.getSignal();
        bitRate     = (int) staInfo.getBitRate();        
    }

    public String getMacAddress() {
        return macAddress;
    }

    public int getRxSignal() {
        return rxSignal;
    }

    public int getTxBitRate() {
        return bitRate;
    }

    private String  macAddress;
    private int     rxSignal;
    private int     bitRate;
}
