/*
 * Created Date: Jul 11, 2008
 * Author : Sri
 */
package com.meshdynamics.api.impl;

import com.meshdynamics.api.NMS;
import com.meshdynamics.api.impl.NMSFactory;

public class NMSCreator implements NMSFactory.NMSCreator {

    public NMSCreator() {
        
    }
    
    public NMS create() {
        return new NMSImplementation();
    }
    
}
