/*
 * Created Date: Jul 10, 2008
 * Author : Sri
 */
package com.meshdynamics.api.impl;

import java.util.Enumeration;
import java.util.Vector;

import com.meshdynamics.api.NMS;
import com.meshdynamics.meshviewer.MeshViewer;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.mg.MGClient;

/**
 * @author Sri
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class NMSImplementation extends NMS {

    public NMSImplementation() {
        
        NetworkImplementation   defaultNetwork;
        
        meshViewer  = MeshViewer.getInstance();
        
        networks    = new Vector<NetworkImplementation>();
       
        
        defaultNetwork = new NetworkImplementation(
                                                    meshViewer.createMeshNetwork("default",
                                                                                 "cybernetics!",
                                                                                 MeshNetwork.NETWORK_REGULAR)
                                                   );
        
        networks.add(defaultNetwork);       
                
    }

    public int start() {
        meshViewer.startViewer();
        return 0;
    }

    public int stop() {
        meshViewer.stopViewer();
        return 0;
    }
    
    public int startMGClient(short     mode,
                             String    server,
                             int       port,
                             boolean   useSSL,
                             String    userName,
                             String    password,
                             boolean   ignoreLocalPackets) {
        
        int mgClientMode;
        
        switch(mode) {
        case NMS.MG_CLIENT_MODE_FORWARDER:
            mgClientMode = MGClient.MODE_FORWARDER;
            break;
        case NMS.MG_CLIENT_MODE_REMOTE_MANAGER:
            mgClientMode = MGClient.MODE_REMOTE_MANAGER;
            break;
        default:
            mgClientMode = MGClient.MODE_ETHERNET_BRIDGE;
        }
        
        return meshViewer.startMGClient(mgClientMode, server, port, useSSL, userName, password, ignoreLocalPackets);
    }
    

    public int stopMGClient() {
        
        return meshViewer.stopMGClient();
    }
    

    public Network openNetwork(String networkName, String networkKey, int networkType) {
        
        MeshNetwork             meshNetwork;
        NetworkImplementation   network;
        Network                 existingNetwork;                 
        
        existingNetwork = getNetworkByName(networkName);
        
        if(existingNetwork != null) {
            return existingNetwork;
        }
                
        network     = null;
        meshNetwork = null;
        
        switch(networkType) {
        case NMS.NETWORK_TYPE_REGULAR:
            meshNetwork = meshViewer.createMeshNetwork(networkName,networkKey,MeshNetwork.NETWORK_REGULAR); 
            break;
        case NMS.NETWORK_TYPE_FIPS_140_2:
            meshNetwork = meshViewer.createMeshNetwork(networkName,networkKey,MeshNetwork.NETWORK_FIPS_ENABLED);            
            break;
        }
        
        if(meshNetwork != null) {
            
            network = new NetworkImplementation(meshNetwork);
            
            networks.add(network);
        }
        
        return network;
    }

    public int closeNetwork(Network network) {
        
        NetworkImplementation   networkImpl;
        
        if(!(network instanceof NetworkImplementation)) {
            return -1;
        }       
        
        networkImpl = (NetworkImplementation)network;
        
        networks.remove(networkImpl);
        
        meshViewer.closeMeshNetwork(network.getName());
        
        networkImpl.unInitialize();              
        
        return 0;
        
    }
    
    public Network getNetworkByName(String networkName) {
        
        Network network;
        
        for(int i = 0; i < networks.size(); i++) {
            
            network = networks.get(i);
            
            if(networkName.equals(network.getName()))
                return network;
        }
        
        return null;
    }
    
    

    public Enumeration<Network> getOpenNetworks() {        
        return new NetworksEnumerator(networks);   
    }   
    
    public void stdOutPrintln(String str) {
        System.out.println(str);
    }
    
    public void stdErrPrintln(String str) {
        System.err.println(str);
    }
    
    protected void unInitialize() {
        
        NetworkImplementation network;
        
        for(int i = 0; i < networks.size(); i++) {
            
            network = networks.get(i);

            meshViewer.closeMeshNetwork(network.getName());
            
            network.unInitialize();            
        }
     
        networks.clear();
        
        meshViewer = null;
    }
    
    
    private static class NetworksEnumerator implements Enumeration<Network> {
        
        NetworksEnumerator(Vector<NetworkImplementation> networks) {
            
            this.networks   = networks;
            this.position   = 0;            
        }
                
        private Vector<NetworkImplementation>   networks;
        private int                             position;
        
        public boolean hasMoreElements() {
            
            if(position >= networks.size())
                return false;
            
            return true;
        }
        
        public Network nextElement() {
            return networks.get(position++);
        }    
        
    }

    protected MeshViewer                    meshViewer;
    protected Vector<NetworkImplementation> networks;    
               
}
