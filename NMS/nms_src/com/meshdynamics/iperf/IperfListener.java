package com.meshdynamics.iperf;

public interface IperfListener {

	public void testEnded();

	public void testStarted();

	public void appendResult(String input_line);

	public void testResult(String output);
	
}
