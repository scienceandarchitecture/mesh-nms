package com.meshdynamics.iperf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.meshdynamics.api.NMS;
import com.meshdynamics.util.IpAddress;

public class IperfHelper {
	private Process 	process;
	
    public void clearData() {
        if(process != null)
            process.destroy();
        
        process = null;
    }

	public String executeTest(IpAddress ipAddress, int recordCount, short type, short protocol, int udpBandWidth, IperfListener listener) {

		String iperfPath = System.getProperty("com.meshdynamics.iperf.path");
		if(iperfPath == null)
			iperfPath = System.getProperty("java.library.path");
		
		if(iperfPath == null)
			return "iperf not found.";
		
		StringBuffer iperfCommand = new StringBuffer();
		
		iperfCommand.append(iperfPath);
		iperfCommand.append("/iperf.exe -c ");  
		iperfCommand.append(ipAddress.toString());
		iperfCommand.append(" -i 1 -p 5001 -f a -L 5001 -t ");
		iperfCommand.append(recordCount);
		
        if(protocol == NMS.PERFORMANCE_PROTOCOL_TCP) {
    		iperfCommand.append(" -P 5 -l 8K");
        }else {
    		iperfCommand.append(" -u -P 2 -l 1470 -b ");
    		iperfCommand.append((double)udpBandWidth/2);
    		iperfCommand.append("M");
        }

        if(type == NMS.PERFORMANCE_TYPE_DUAL_INDIVIDUAL)
        	iperfCommand.append(" -r ");
        else if(type == NMS.PERFORMANCE_TYPE_DUAL_SIMULTANEOUS)
        	iperfCommand.append(" -d ");
        
        try {
            process = Runtime.getRuntime().exec(iperfCommand.toString());
          }
        catch(IOException e) {
            if(listener != null)
            	listener.testEnded();
            
            return e.getMessage();
        }
          
        BufferedReader input 	= new BufferedReader(new InputStreamReader(process.getInputStream()));
        BufferedReader errors 	= new BufferedReader(new InputStreamReader(process.getErrorStream()));
	
        if(listener != null)
        	listener.testStarted();

        String result 		= "";
        
        try {
            String inputLine 	= null;
            
            while((inputLine = input.readLine()) != null) {
                
                if(listener != null)
                	listener.appendResult(inputLine);
                
                result += inputLine + "\n";
                
            }
	
            String errorLine = null;
            while((errorLine = errors.readLine()) != null) {
            	if(listener != null)
            		listener.appendResult(errorLine + "\n");
                
            	result += errorLine + "\n";
            }
            
            if(listener != null) {
            	listener.appendResult("Done.\n");
            	listener.testEnded();
            }
            
            result += "Done.\n";
            
        } catch(IOException e) {
        	if(listener != null) {
	        	listener.appendResult("\nError in Iperf thread.\n");
	        	listener.testEnded();
        	}
        	result += "\nError in Iperf thread.\n";
        }
		
        return result;
	}
}
