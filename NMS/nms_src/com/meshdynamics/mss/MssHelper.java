package com.meshdynamics.mss;

import java.util.Enumeration;
import java.util.Hashtable;

public class MssHelper {
	
	private static int 				_mssId;
	
	@SuppressWarnings("unused")
	private String					mssId;
	private Hashtable<String, Msc>	hashMsc;
	
	static {
		_mssId = 1;
		System.loadLibrary("MSS");
	}
	
	public MssHelper() {
		mssId			= "mss" + _mssId++;
		hashMsc 		= new Hashtable<String, Msc>();
	}
	
	public native void		mssHelperInit			();
	public native void		mssHelperUninit			();
	
	public native void		setWorkingDirectory		(String dirPath);
	public native int 		load 					(String filePath);
	public native void 		executeMsc				(String mscId, IMssExecutionListener listener);
	
	
	public Enumeration<Msc> getMscEnumeration() {
		return hashMsc.elements();
	}
	
	protected Msc addMsc(String mscId) {
		
		Msc msc = hashMsc.get(mscId);
		
		if(msc != null)
			return msc;
		
		msc = new Msc(mscId);
		hashMsc.put(mscId, msc);
		
		return msc;
	}
	
}
