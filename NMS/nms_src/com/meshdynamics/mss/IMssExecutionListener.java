package com.meshdynamics.mss;


/**
 * @author AbhijitAyarekar
 *
 */
public interface IMssExecutionListener {

	public void 		executionStarted		(String mscId);
	public void 		executeClientStart		(String mscId, String strMacAddress);
	public void 		executionProgress		(String mscId, String strMacAddress, int mscEntryIndex, String outcome);
	public void 		executeClientEnd		(String mscId, String strMacAddress, boolean success);
	public void 		executionEnded			(String mscId, String outcome);
	
	public byte[] 		getIpAddress			(String strMacAddress);
	public byte[]		getHostIpAddress		();
	
}
