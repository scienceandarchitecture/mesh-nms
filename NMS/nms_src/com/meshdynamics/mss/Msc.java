package com.meshdynamics.mss;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.meshdynamics.util.MacAddress;

public class Msc {
	
	private String 							mscId;
	private Hashtable<String, MacAddress> 	hashMacAddress;
	private Vector<String>					lstScriptEntries;
	
	protected Msc(String mscId) {
		this.mscId 			= mscId;
		hashMacAddress 		= new Hashtable<String, MacAddress>();
		lstScriptEntries	= new Vector<String>();
	}
	
	public int getScriptEntryCount() {
		return lstScriptEntries.size();
	}
	
	public String getScriptEntryAt(int index) {
		return lstScriptEntries.elementAt(index);
	}
	
	public Enumeration<MacAddress> getMacAddressEnumeration() {
		return hashMacAddress.elements();
	}
	
	public String getId() {
		return mscId;
	}

	public boolean addMacAddress(byte[] macBytes) {
		if(MacAddress.tempAddress.setBytes(macBytes) != 0)
			return false;
		
		MacAddress address = hashMacAddress.get(MacAddress.tempAddress.toString());
		if(address != null)
			return true;
		
		address = new MacAddress();
		address.setBytes(macBytes);
		hashMacAddress.put(address.toString(), address);
		return true;
	}
	
	public boolean addScriptEntry(String entry) {
		lstScriptEntries.add(entry);
		return true;
	}
}
