package test;

import com.meshdynamics.meshviewer.IMeshViewerListener;
import com.meshdynamics.meshviewer.MeshViewer;
import com.meshdynamics.meshviewer.mesh.IMessageHandler;
import com.meshdynamics.meshviewer.mesh.INetworkListener;
import com.meshdynamics.meshviewer.mesh.MeshNetwork;
import com.meshdynamics.meshviewer.mesh.ap.AccessPoint;
import com.meshdynamics.meshviewer.mesh.ap.IAPPropListener;


public class Main implements IMeshViewerListener, INetworkListener, IAPPropListener, IMessageHandler {
	
	private MeshViewer 	meshViewer;
	private MeshNetwork	defaultNetwork;
	
	public void execute() {
		
		meshViewer = MeshViewer.getInstance();
		meshViewer.setMeshViewerListener(this);
		
		defaultNetwork = meshViewer.createMeshNetwork("default", "cybernetics!", MeshNetwork.NETWORK_REGULAR);
		if(defaultNetwork != null)
			defaultNetwork.addNetworkListener(this);
		
		if(meshViewer.isRunning() == false)
			meshViewer.startViewer();
		
	}
	
	public static void main(String[] args) {
		
		Main mainClass = new Main();
		mainClass.execute();
	}

	public void viewerStarted() {
		System.out.println("Viewer started");		
	}

	public void viewerStopped() {
		System.out.println("SMG stopped");
	}

	public void accessPointAdded(AccessPoint accessPoint) {
		accessPoint.addPropertyListener(this);
		accessPoint.addMessageListener(this);
		
		System.out.println("AP added " + accessPoint.getDSMacAddress());
		System.out.println("AP added ApName " + accessPoint.getApName());
		System.out.println("AP added v " + accessPoint.getFirmwareVersion());
		System.out.println("AP added Model " + accessPoint.getModel());
		System.out.println("AP added Name " + accessPoint.getName());
		System.out.println("AP added Model " + accessPoint.getNetworkId());
		System.out.println("AP update all configuration start.");
		accessPoint.updateAllConfiguration();
		
		System.out.println("AP update all configuration end.");
	}

	public void accessPointDeleted(AccessPoint accessPoint) {
		accessPoint.removeMessageListener(this);
		accessPoint.removePropertyListener(this);
		System.out.println("AP deleted " + accessPoint.getDSMacAddress());
		
	}

	public void meshViewerStarted() {
		System.out.println("Viewer started from network");
	}

	public void meshViewerStopped() {
		System.out.println("Viewer stopped from network");
	}

	public void notifyHealthAlert(int healthStatus, String meshId) {
		
	}

	public void saturationInfoReceived(AccessPoint ap, String ifName) {
		// TODO Auto-generated method stub
		
	}

	public void apPropertyChanged(int filter, int subFilter, AccessPoint src) {
		if(filter == IAPPropListener.MASK_HEARTBEAT ||
				filter == IAPPropListener.MASK_HEARTBEAT2)
			System.out.println("HB received from " + src.getDSMacAddress());
			
	}

	public void showMessage(IMessageSource msgSource, String messageHeader,
			String message, String progress) {
		System.out.println("MESSAGE : From " + msgSource.getSrcName());
		System.out.println("Header : " + messageHeader);
		System.out.println("Message : " + message);
		System.out.println("Progress : " + progress);
	}

	@Override
	public void meshGatewayStarted() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void meshGatewayStopped() {
		// TODO Auto-generated method stub
		
	}
}
