package com.meshdynamics.api;

import java.util.Enumeration;
import java.util.Vector;

/**
 * {@code NMSUI} is the primary class for extending the <i>Meshdynamics Network Viewer </i>.
 * <p> 
 * It is a singleton class defining classes, interfaces and constants to be used
 * for extending the Network Viewer<br>
 * <p>
 * When using NMSUI API from a scripting language, a global {@code NMSUI} object is defined by the name
 * {@code nmsui} ({@code $nmsui} in the case of Ruby).
 * <p>
 * When extending the  <i>Meshdynamics Network Viewer </i> using Java, clients need to call the static {@code getInstance()} method to obtain an
 * instance of this class.
 * 
 */

public abstract class NMSUI {   
    
    /**
     * The {@code Alert} class represents a network or system event that
     * needs the user's attention.
     * <p>
     * All alerts are displayed in the 'Alerts' tab of the <i>Meshdynamics Network Viewer</i>.
     */
	
	public static class Alert {
	    
	    /**
	     * Specifies that the {@code Alert} object contains information
	     * about a low priority network or system event.
	     * @see #level
	     */	
	    
		public static final int ALERT_LEVEL_LOW			= 1;
		
        /**
         * Specifies that the {@code Alert} object contains information
         * about a medium priority network or system event.
         * @see #level
         */ 		
		
		public static final int ALERT_LEVEL_MEDIUM		= 2;

        /**
         * Specifies that the {@code Alert} object contains information
         * about a high priority network or system event.
         * @see #level
         */         
		
		public static final int ALERT_LEVEL_HIGH		= 3;
		
		/**
		 * Constructs a {@code Alert} object
		 * @param level the alert level (can be one of {@code ALERT_LEVEL_LOW}, {@code ALERT_LEVEL_MEDIUM} or {@code ALERT_LEVEL_HIGH} 
		 * @param message the alert summary
		 * @param description the complete description for the alert
		 */
		
		public Alert(int level, String message, String description) {
			this.level 			= level;
			this.message		= message;
			this.description	= description;
		}
		
		/**
		 * The level or priority for the {@code Alert}.
		 * <p>
		 * This can be one of {@code ALERT_LEVEL_LOW}, {@code ALERT_LEVEL_MEDIUM} or {@code ALERT_LEVEL_HIGH}.
		 * @see #ALERT_LEVEL_HIGH
		 * @see #ALERT_LEVEL_MEDIUM
		 * @see #ALERT_LEVEL_LOW    
		 */
		
		public int level;
		
		/**
		 * A brief summary for the alert.
		 */
		
		public String message;
		
		/**
		 * Detailed description of the alert. 
		 */

		public String description;
		
	}

	/**
	 * Displays the information represented by the specified alert object 
	 * in the 'Alerts' tab of the <i>Meshdynamics Network Viewer</i>.
	 * @param alert the {@code Alert} object containing the information
	 * @see NMSUI.Alert
	 */
	
	public abstract void showAlert(Alert alert);
	
	/**
	 * The {@code Extension} interface defines methods that <i>Meshdynamics Network Viewer</i>
	 * extensions need to implement. 
	 */
	
	public static interface Extension {
		
	    /**
	     * Called when the <i>Meshdynamics Network Viewer</i> is started by the user.
	     */
	    
		public 	void	viewerStarted		();

        /**
         * Called when the <i>Meshdynamics Network Viewer</i> is stopped by the user.
         */		
		
		public 	void	viewerStopped		();
		
        /**
         * Called when the user opens a Network in the <i>Meshdynamics Network Viewer</i>.
         * 
         * @param networkName the name of the network being opened.
         */		
		
		public 	void	networkOpened		(String networkName);
		
        /**
         * Called when the user closes a Network in the <i>Meshdynamics Network Viewer</i>.
         * 
         * @param networkName the name of the network being opened.
         */ 
		
		public 	void 	networkClosed		(String networkName);
		
		/**
		 * Called when the user switches between open networks in the <i>Meshdynamics Network Viewer</i>.
		 * @param networkName the name of the current select network.
		 */
		
		public 	void 	networkSelected		(String networkName);
		
		/**
		 * Called when the user selects a node in the <i>Meshdynamics Network Viewer</i>.
		 * @param nodeId the MAC-address of the selected node.
		 */
		
		public 	void 	nodeSelected		(String networkName, String nodeId);
		
		/**
		 * Called when a new node is added in the <i>Meshdynamics Network Viewer</i>.
		 * @param nodeId the MAC-address of the newly added node.
		 */
		
		public 	void 	nodeAdded		(String networkName, String nodeId);

		/**
		 * Called when a new node is deleted from the <i>Meshdynamics Network Viewer</i>.
		 * @param nodeId the MAC-address of the deleted node.
		 */
		
		public 	void 	nodeDeleted		(String networkName, String nodeId);
		
		/**
		 * Called by the <i>Meshdynamics Network Viewer</i>  when the property view needs
		 * to be updated.
		 * <p>
		 * Extensions can create their own property sections and display custom properties
		 * using the {@link NMSUI#createPropertySection} and {@link NMSUI#addProperty} methods.
		 * <p>
		 * If an extension wants to explicitly update a property, it can call the 
		 * {@link NMSUI#requestPropertySectionUpdate} method.
		 * 
		 * @param sectionId the property section identifier.
		 * @param nodeId the node MAC-address
		 * @param propertyId the property identifier.
		 * @return the value of the specified property
		 */
		
		public 	String 	getPropertyValue	(String sectionId, String nodeId, String propertyId);		
		
	}

	/**
	 * Register's an extension with the <i>Meshdynamics Network Viewer</i>.
	 * <p>
	 * After an extention has been registered, custom user-interface elements
	 * like property sections, status tabs, etc can be created.
	 * 
	 * @param extension the extension to be registered
	 */
	
	public abstract void addExtension(Extension extension);

	/**
	 * Unregister's an extension with the <i>Meshdynamics Network Viewer</i>.
	 * @param extension the extension to unregistered
	 */
	
	public abstract void removeExtension(Extension extension);


	/**
	 * Creates a property section in the <i>Meshdynamics Network Viewer</i>'s property view.
	 * <p>
	 * A property section once created, can have a list of properties (name/value pairs).
	 * <p>
	 * Properties can be added to a property section using the {@link addProperty} method.
	 * 
	 * @param extension reference to a registered extension 
	 * @param sectionId a unique section identifier
	 * @param sectionText the section's display text
	 * @return  {@code 0} on success;
	 *          {@code -1} of the {@code extension} is not registered;
	 *          {@code -2} if the sectionId is in use by another extension;
	 *          {@code -3} if the sectionId was already registered by the {@code extension};
	 *          {@code -4} if an error occurs while creating the section
	 */
	
	public abstract int createPropertySection(Extension extension, String sectionId, String sectionText);

	/**
	 * Destroys the specified property section in the <i>Meshdynamics Network Viewer</i>'s property view.
	 * <p>
	 * The {@code sectionId} must refer to a property section created using {@link createPropertySection} method.
	 * 
	 * @param sectionId the section identifier
	 */
	
	public abstract void destroyPropertySection(String sectionId);

	/**
	 * Used by extensions to explicitly request an update for the specified section
	 * and node.
	 * <p>
	 * An Extension can use this method to request the <i>Meshdynamics Network Viewer</i>
	 * to call its {@link NMSUI.Extension#getPropertyValue} method.
	 * <p>
	 * The <i>Meshdynamics Network Viewer</i> shall adhere to the request and call 
	 * {@link NMSUI.Extension#getPropertyValue} only if the node specified by {@code nodeId}
	 * is the currently selected node.
	 * 
	 * @param sectionId the property section to be updated 
	 * @param nodeId the MAC-address of the node
	 */

	public abstract void requestPropertySectionUpdate(String sectionId, String nodeId);

	/**
	 * Adds a property to the specified property section.
	 * 
	 * @param sectionId the property section identifier
	 * @param propertyId the property identifier
	 * @param propertyText the display text for the property
	 * @return {@code 0} on success;
	 *         {@code -1} if the property section specifed by {@code sectionId} does not exist;
	 *         {@code -2} if an error occurs when adding the property
	 */
	
	public abstract int addProperty(String sectionId, String propertyId, String propertyText);

	/**
	 * Removes the specified property from the property section.
	 * 
	 * @param sectionId the property section identifier
	 * @param propertyId the property identifier
	 * 
	 */
	
	public abstract void removeProperty(String sectionId, String propertyId);

	
	/**
	 * The {@code StatusTabColumn} class defines the properties of
	 * a column in a <i>Meshdynamics Network Viewer</i> status tab.
	 */
	
	public static class StatusTabColumn {
	    
	    /**
	     * Specifies that the column should be left-aligned.
	     */
		
		public static final int COLUMN_ALIGN_LEFT 	= 0;
		
        /**
         * Specifies that the column should be right-aligned.
         */
		
		public static final int COLUMN_ALIGN_RIGHT 	= 1;
		
		/**
		 * Constructs the {@code StatusTabColumn} object.
		 * 
		 * @param name the column name
		 * @param align the column alignment (one of {@code COLUMN_ALIGN_LEFT},{@code COLUMN_ALIGN_RIGHT})
		 * @param width the column width
		 */
		
		private StatusTabColumn(String name, int align, int width) {
			this.name 	= name;
			this.align 	= align;
			this.width  = width;
		}
		
		/**
		 * The name of the column.
		 */
		
		public String 	name;
		
		/**
		 * The column alignment ({@code COLUMN_ALIGN_LEFT} or {@code COLUMN_ALIGN_RIGHT})
		 */
		
		public int		align;
		
		/**
		 * The width of the column.
		 */
		
		public int 		width;

	}
	
    /**
     * The {@code StatusTabHeader} class provides a collection of columns for 
     * a status tab in the <i>Meshdynamics Network Viewer</i>.
     */
	
	public static class StatusTabHeader {

        private Vector<StatusTabColumn> columns;	    
		
        /**
         * Constructs the {@code StatusTabHeader} object.
         */
        
		public StatusTabHeader() {
			columns = new Vector<StatusTabColumn>();
		}
		
		/**
		 * Adds a column to the status tab.
		 * 
		 * @param columnName the column heading
		 * @param align the column alignment ({@code StatusTabColumn.COLUMN_ALIGN_LEFT} or {@code StatusTabColumn.COLUMN_ALIGN_RIGHT})
		 * @param width the column width
		 */
		
		public void addColumn(String columnName, int align, int width) {
			columns.add(new StatusTabColumn(columnName, align, width));
		}
		
		/**
		 * Removes the specified column from the status tab.
		 * 
		 * @param columnName the column to remove
		 */
		
		public void removeColumn(String columnName) {
			for(StatusTabColumn column : columns) {
				if(column.name.equals(columnName) == true) {
					columns.remove(column);
					return;
				}
			}
		}
		
		/**
		 * Returns the number of columns in the status tab.
		 * 
		 * @return the number of columns
		 */

		public int getColumnCount() {
			return columns.size();
		}
		
		/**
		 * Retrieves the {@code StatusTabColumn} object for a given index.
		 * 
		 * @param index the column index
		 * @return {@link NMSUI#StatusTabColumn} object at {@code index}
		 */
		
		public StatusTabColumn getColumnAt(int index) {
			return columns.get(index);
		}
	}
	
	/**
	 * {@code StatusTab} interface defines methods for initialization
	 * and modification of custom status tabs in the <i>Meshdynamics Network Viewer</i>.
	 */
	
	public static interface StatusTab {
	    
	    /**
	     * Retrieves the name of the status tab.
	     * 
	     * @return the status tab name
	     */
		
	    public String getName();
		
	    /**
	     * Retrieves the {@code StatusTabHeader} object for the status tab.
	     * 
	     * @return {@code StatusTabHeader} object
	     * @see NMSUI#StatusTabHeader
	     */
	    
		public StatusTabHeader getHeader();
	}
	
	/**
	 * The {@code StatusTabRow} interface defines methods for initialization
	 * and modification of rows in a status tab. 
	 */
	
	public static interface StatusTabRow {
	    
	    /**
	     * Sets the display text for the specified column.
	     *  
	     * @param index the column index
	     * @param value the column text
	     * 
	     * @see NMSUI#updateStatusTabRow(com.meshdynamics.api.NMSUI.StatusTabRow)
	     */
	    
		public void setValueAt(int index, String value);
		
		/**
		 * Retrieves the reference to the {@code StatusTab} object
		 * 
		 * @return reference to the {@code StatusTab} object
		 */
		
		public StatusTab getStatusTab();
	}
	
	/**
	 * Creates a custom status tab in the <i>Meshdynamics Network Viewer</i>.
	 * 
	 * @param extension reference to a registered extension object
	 * @param tabName the unique name for the status tab
	 * @param columns the {@link NMSUI#StatusTabHeader} object specifying the column attributes for the tab
	 * 
	 * @return {@link NMSUI#StatusTab} object for the status tab
	 * @see  NMSUI#StatusTab
	 * @see NMSUI#StatusTabHeader
	 */
	
	public abstract StatusTab createStatusTab(Extension extension, String tabName, StatusTabHeader columns);
		
	/**
	 * Destroys the specified status tab in the <i>Meshdynamics Network Viewer</i>.
	 * @param statusTab
	 */
	
	public abstract void destroyStatusTab(StatusTab statusTab);
	
	/**
	 * Adds a row to the given {@code StatusTab}.
	 * 
	 * @param statusTab the {@code StatusTab} object 
	 * @return the {@code StatusTabRow} object representing the created row.
	 * 
	 * @see NMSUI#StatusTab
	 * @see NMSUI#StatusTabHeader
	 */
	
	public abstract StatusTabRow addStatusTabRow(StatusTab statusTab);
	
	/**
	 * Removes the given row from the status tab.
	 * 
	 * @param statusTabRow the {@code StatusTabRow} object representing the row
	 */
	
	public abstract void removeStatusTabRow(StatusTabRow statusTabRow);
	
	/**
	 * Requests the <i>Meshdynamics Network Viewer</i> to update the given status tab row.
	 * <p>
	 * Clients will first need to call the {@code setValueAt} method to change the columb values for a row.
	 *  
	 * @param statusTabRow the {@code StatusTabRow} object representing the row
	 * @see NMSUI.StatusTabRow#setValueAt(int, String)
	 */
	
	public abstract void updateStatusTabRow(StatusTabRow statusTabRow);
	
	/**
	 * The {@code NodeText} interface provides methods for initialization and modification
	 * of custom text on node widgets in the <i>Meshdynamics Network Viewer</i>.
	 * 
	 * @see NMSUI#createNodeText
	 */
	
	public static interface NodeText {
	    
	    /**
	     * Retrieves the unique text identifier for the custom node text.
	     * @return the unique text identifier for the custom node text.
	     */
	
		public String getTextId();
		
		/**
		 * Retrieves the node's MAC-address.
		 * 
		 * @return the MAC-address of the node.
		 */
		
		public String getNodeId();
		
		/**
		 * Sets the value of the custom node text.
		 * 
		 * @param text the display text.
		 */
		
		public void setText(String text);
		
	}

	/**
	 * Creates a custom text for the specified node in the <i>Meshdynamics Network Viewer</i>.
	 * <p>
	 * Custom text is displayed in the text display area of the node widget.
	 * 
	 * @param extension the reference to the registered extension
	 * @param nodeId the MAC-address of the node
	 * @param nodeTextId the unique identifier for the custom text
	 * 
	 * @return reference to a {@code NodeText} object representing the custom text.
	 * @see NMSUI.NodeText
	 */

	public abstract NodeText createNodeText(Extension extension, String nodeId, String nodeTextId);

	/**
	 * Destroys the specified custom node text.
	 * 
	 * @param nodeText the {@code NodeText} object representing the custom node text
	 * @see NMSUI.NodeText
	 */
	
	public abstract void destroyNodeText(NodeText nodeText);
	
	/**
	 * The {@code ContextMenuSection} interface is used as an identifer
	 * in the creation and modification of custom menus in a node's
	 * context menu.  
	 * <p>
	 * A context menu section consists of either child context menu sections
	 * or menu commands, both of which are represented by {@code ContextMenuSection}
	 * objects.
	 * 
	 * @see NMSUI#createContextMenuSection
	 * @see NMSUI#createMenuCommand
	 */

	public static interface ContextMenuSection {
	    		
		/**
		 * The display text for the context menu section.
		 * 
		 * @return the context menu section display text
		 */
		
		public String getText();
		
		/**
		 * Retrieves the parent {@code ContextMenuSection} object.
		 * 
		 * @return the parent {@code ContextMenuSection}
		 */
		
		public ContextMenuSection getParent();
		
		/**
		 * The display icon for the context menu section.
		 *  
		 * @return an opqaue object referencing a SWT {@code Image}. 
		 */
		
		public Object getIcon();
		
		/**
		 * Returns whether the context menu section represents a leaf-level command.
		 * 
		 * @return {@code true} if the context menu section represents a leaf-level command
		 */
		
		public boolean isCommand();
		
        /**
         * Retrieves the command identifier for the context menu section if it
         * represents a leaf-level command.
         * 
         * @return the context menu command identifier;
         *         or {@code null} if the object is not a leaf-level command
         * @see #isCommand()
         */
        
        public String getId();
		
	}

	/**
	 * Creates a custom context menu section in the <i>Meshdynamics Network Viewer</i>.
	 * 
	 * @param extension the reference to a registered extension
	 * @param parent the parent {@code ContextMenuSection} or {@code null} for a top-level section
	 * @param titleText the display text for the context menu section
	 * @param icon an opaque object representing a SWT {@code Image}.
	 * 
	 * @return a reference to the {@code ContextMenuSection} object for the created section
	 * @see NMSUI.ContextMenuSection
	 */
	
	public abstract ContextMenuSection  createContextMenuSection(Extension extension, ContextMenuSection parent, String titleText, Object icon);

	/**
	 * Destroys the specified context menu section or leaf-level command.
	 * <p>
	 * When called, this function recursively destroys all underlying {@code ContextMenuSection}'s and
	 * leaf-level commands.
	 * 
	 * @param menuSection the section to be destroyed
	 */
	
	public abstract void destroyContextMenuSection(ContextMenuSection menuSection);
	
    /**
     * The {@code MenuCommandHandler} interface defines methods for handling
     * cusutom menu commands from the node context menu in <i>Meshdynamics Network Viewer</i>.
     * 
     * @see NMSUI#createMenuCommand
     */
    
    public static interface MenuCommandHandler {
        
        /**
         * Called when the user selects the specified option in a node's context menu.
         * 
         * @param menuSection the reference to the {@code ContextMenuSection} representing the command
         * @param nodeId the MAC-address of the selected node 
         */
        
        void onCommand(ContextMenuSection menuSection, String networkName, String nodeId);
    }	

    /**
     * Creates a leaf-level command in the specified context menu section.
     * 
     * @param menuSection the {@code ContextMenuSection} in which the command is to be created.
     * @param commandId the identifier for the command to be handled
     * @param text the display text for the command
     * @param icon an opqaue object referencing a SWT {@code Image}
     * @param handler reference to a object implementing the {@code MenuCommandHandler} interface
     *  
     * @return {@code ContextMenuSection} object representing the leaf-level command
     * @see NMSUI.MenuCommandHandler
     * @see NMSUI.ContextMenuSection
     */

	public abstract ContextMenuSection createMenuCommand(ContextMenuSection menuSection, String commandId, String text, Object icon, MenuCommandHandler handler);

	/**
	 * Creates a property in nmsui context. This property can be shared amongst multiple plugins
	 * 
	 * @param name
	 * @param value
	 * @return true if successful
	 */
	public abstract boolean setProperty(String name, Object value);
	
	/**
	 * Returns value of named property in nmsui context.
	 * 
	 * @param name
	 * @return valid object if property exists. NULL otherwise
	 */
	public abstract Object getProperty(String name);

	/**
	 * Creates a property in nmsui context for given network.
	 * This property can be shared amongst multiple plugins
	 * 
	 * @param networkName 
	 * @param name
	 * @param value
	 * @return true if successful
	 */
	public abstract boolean setProperty(String networkName, String name, Object value);
	
	/**
	 * Returns value of named property in nmsui context.
	 * 
	 * @param networkName
	 * @param name
	 * @return valid object if property exists. NULL otherwise
	 */
	public abstract Object getProperty(String networkName, String name);

	/**
	 * Creates a property in nmsui context for node identified by nodeId in given network.
	 * This property can be shared amongst multiple plugins
	 * 
	 * @param networkName
	 * @param nodeId 
	 * @param name
	 * @param value
	 * @return true if successful
	 */
	public abstract boolean setProperty(String networkName, String nodeId, String name, Object value);
	
	/**
	 * Returns value of named property in nmsui context for node identified by nodeId in given network.
	 * 
	 * @param networkName
	 * @param nodeId
	 * @parma nodeName
	 * @param name
	 * @return valid object if property exists. NULL otherwise
	 */
	public abstract Object getProperty(String networkName, String nodeId, String name);

	public static final String NODE_ELEMENT_PROPERTY_LED1_COLOR = "led1_color";
	public static final String NODE_ELEMENT_PROPERTY_LED2_COLOR = "led2_color";
	public static final String NODE_ELEMENT_PROPERTY_LED1_LABEL = "led1_label";
	public static final String NODE_ELEMENT_PROPERTY_LED2_LABEL = "led2_label";

	public static final String NODE_ELEMENT_VALUE_COLOR_GRAY 	= "gray";
	public static final String NODE_ELEMENT_VALUE_COLOR_RED 	= "red";
	public static final String NODE_ELEMENT_VALUE_COLOR_YELLOW 	= "yellow";
	public static final String NODE_ELEMENT_VALUE_COLOR_GREEN 	= "green";
	public static final String NODE_ELEMENT_VALUE_COLOR_ORANGE 	= "orange";
	public static final String NODE_ELEMENT_VALUE_COLOR_BLUE 	= "blue";
	
	/**
	 * Sets a UI property in nmsui context for a node identified by nodeId in given network.
	 * This property is shared amongst multiple plugins
	 * 
	 * @param networkName
	 * @param nodeId 
	 * @param property
	 * @return
	 */
	public abstract void setNodeElementProperty(String networkName, String nodeId, String property, String value);

	/**
	 * Gets a UI property value in nmsui context for a node identified by nodeId in given network.
	 * This property is shared amongst multiple plugins
	 * 
	 * @param networkName
	 * @param nodeId 
	 * @param property
	 * @return valid string if property exists. NULL otherwise
	 */
	public abstract String getNodeElementProperty(String networkName, String nodeId, String property);
	
	/**
	 * Returns Enumeration of nodeId's currently group selected in nmsui.
	 * 
	 * @return Enumeration<String>
	 */
	public abstract Enumeration<String> getGroupSelection();
	
	
	/**
	 * Retrieves the current instance of the <i>Meshdynamics Network Viewer</i>.
	 * 
	 * @return the current instance of the {@code NMSUI} object 
	 */
	public static NMSUI getInstance() {
	    return singleton;
	}
	
	protected static NMSUI singleton = null; 
}
