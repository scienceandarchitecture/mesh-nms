
var _newGraphicPoints = new Array();
var _newGraphic;
var _isFirstPoint = true;
var _lineWidth = 6;
var _lineColor = "#ff0000";
var _lineOpacity = 0.7;

	
function AlertHello() {
	window.alert("Hello");
}

function Line(srcAp, dstAp) {
	    
	this.srcAp		= srcAp;
	this.dstAp		= dstAp;
	this.srcLatLng	= srcAp.marker.getLatLng();
	this.dstLatLng	= dstAp.marker.getLatLng();
	this.line			= new GPolyline([this.srcLatLng, this.dstLatLng], "#FF0000", 3, 1);
	gMap.addOverlay(this.line);
			
	this.remove = function() {
		gMap.removeOverlay(this.line);
	}
		
}

function AccessPoint(info, marker) {
	this.info		= new AccessPointInfo();
	this.marker		= marker; 
	this.line		= null;
	this.info.copyFrom(info);
}

AccessPoint.prototype.dispose = function() {
	delete this.info;
	delete this.marker;
	if(this.line != null) {
		this.removeLine();
	}
}

AccessPoint.prototype.addLine = function(parentAp) {
if(this.line != null) {
		this.removeLine();
	}

	this.line = new Line(this, parentAp);	
}

AccessPoint.prototype.removeLine = function() {
	if(this.line == null) {
		return;
	}
	
	this.line.remove();
	delete this.line;
}


AccessPoint.prototype.toString = function() {
    var ret = "[";
    for(var prop in this) {
		ret += prop + " = " + this[prop] + "\n";
    }
    return ret;
};
  
function AccessPointInfo() {
	this.id				= "";
	this.lat			= "";
	this.lng			= "";
	this.parentId			= "";
	this.name			= "";
	this.mobile			= "";
	this.typeChanged		= false;
}

AccessPointInfo.prototype.copyFrom = function(srcInfo) {

	this.typeChanged = false;
	if(this.parentId == "" && srcInfo.parentId != "" ||
		this.parentId != "" && srcInfo.parentId == "") {
		this.typeChanged = true;
	}
	
	this.id				= srcInfo.id;
	this.lat			= srcInfo.lat;
	this.lng			= srcInfo.lng;
	this.parentId			= srcInfo.parentId;
	this.name			= srcInfo.name;
	this.mobile			= srcInfo.mobile;
}

function CElementPos(elem) {
			 
	this.posLeft  = 0;
	this.posTop   = 0;
				 
	if(elem.tagName == "body") {
		return;
	}
				 
	if(elem.offsetParent) {
				 
		var oParentElementPos;
					 
		oParentElementPos = new CElementPos(elem.offsetParent);
					 
		this.posLeft = oParentElementPos.posLeft + elem.offsetLeft;
		this.posTop = oParentElementPos.posTop  + elem.offsetTop;
	}
}


 function drawCircle(point) {
    	
	var clickPointPixel 	= gMap.fromLatLngToDivPixel(point);
	var circleRadius 	= 125;
	
	alert(clickPointPixel);
	_newGraphicPoints 	= getCircleCoords(clickPointPixel.x,clickPointPixel.y,circleRadius,60);
	
	gMap.removeOverlay(_newGraphic);	   
    	_newGraphic 	= new GPolygon(_newGraphicPoints, _lineColor, _lineWidth,_lineOpacity);
    	gMap.addOverlay(_newGraphic);
		   
 }
	
	
 function getCircleCoords (centerX, centerY, r, numPoints) {

	var coords = new Array();
	var xCoords = new Array();
	var yCoords = new Array();
	var yDifs = new Array();

	var minX = centerX - r;
	var maxX = centerX + r;
	var increment = (maxX - minX) / numPoints;

	if(increment <= 0) return;

	var curX = minX;
	var curY = 0;
	var yDif = 0;

	var haveFirst = false;
		
	var i = 0;
	while ((curX <= maxX) || (i < 61)){
			
		curY = Math.sqrt(Math.pow(r,2) - Math.pow((curX - centerX),2))    + centerY;


		  if(isNaN(Math.sqrt(Math.pow(r,2) - Math.pow((curX - centerX),2)))) {
			 curY = centerY;
		  }


		yDif = 2*(curY - centerY);

		xCoords[i] = curX;
		yCoords[i] = curY;
		yDifs[i] = yDif;

		coords[i] = gMap.fromDivPixelToLatLng(new GPoint(curX,curY));

		curX += increment;
		i++;
	}
	var bottomY;
	for(k = xCoords.length-1; k > -1; k--){

		bottomY = yCoords[k] - yDifs[k];

		coords[i] = gMap.fromDivPixelToLatLng(new GPoint(xCoords[k],bottomY));

		i++;
	}
	return coords;
		
}

